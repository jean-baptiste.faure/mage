**En cours** : passer en allocation dynamique de mémoire pour s'affranchir des tailles prédéfinies en nombre de biefs, nombre de sections, etc.

**En cours** : prise en compte des dates calendaires au format ISO (AAAA-MM-JJ HH:MM:SS). Si seule la date de début de simulation T_inf est définie ainsi, les autres dates sont comptées à partir de T_inf.

NB : Pour récupérer une version exploitable, je conseille de prendre le dernier tag. Actuellement : État au 30 septembre 2021.
