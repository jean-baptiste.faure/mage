# ToDo liste pour Mage-8

__Note :__ _ce fichier est rédigé en utilisant la syntaxe_ [_markdown_](https://fr.wikipedia.org/wiki/Markdown)

## Général

* ~~Migrer vers la forge gitlab d'Irstea~~
* Régler la question de la licence


## Documentation (Mage-8_Documentation.odt)

Compléter la doc, en particulier sur les points suivants :

* implémentation de ISM : généralisation de la résolution au cas d'un réseau (chapitre 5.2)
* ~~implémentation du charriage~~
* description du fichier NET : ajouter la position relative des biefs aux jonctions, position utile pour ISM.


## Développement

### Charriage
* Ajouter le concept de couche active qui est la couche virtuelle d'épaisseur petite dans laquelle s'effectuent les opérations de mixage et démixage.
* ~~Ajouter la formule de capacité solide de Recking 2013.~~
* Permettre de définir une C.L. amont solide à l'équilibre, donc sans fournir de données de débit solide.
* ~~modif géométrie : corriger le calcul de la largeur active quand elle concerne aussi le lit majeur (ligne 929).~~
* (Priorité 2) Ajouter certaines options de dépôt-érosion proposées par RubarBE

### ISM
* intégrer le double-balayage modifié par YK pour le traitement d'un réseau _ramifié_ ;
* reconsidérer le traitement des confluences tel que proposé par YK :
    - il faut que ça marche quel que soit le nombre de biefs entrants ;
    - pas sur que la position relative des biefs aux jonctions soit vraiment utile pour ISM ;
* ISM + ouvrages : répartir les ouvrages dans les sous-sections ;
    - cela suppose d'ajouter une donnée dans SIN pour chaque ouvrage élémentaire : le n° de la sous-section dans laquelle il se trouve.
    - par défaut cette sous-section est le lit mineur
    - il faut pouvoir, dans une même section, résoudre St-Venant dans les lits majeurs et un ouvrage composite dans le lit mineur.
    - la contrainte « tout l'écoulement passe sur les ouvrages » n'est alors conservée qu'au niveau des sous-sections.

### Réseau maillé
* la méthode utilisée actuellement avec Debord ne peut pas fonctionner avec ISM parce que les bilans de débit aux nœuds doivent être faits différemment.
* essayer une approche itérative qui serait applicable aussi bien avec Debord qu'avec ISM et qui permettrait en plus de s'affranchir de la boîte noire Talweg (mage_blackbox_TAL.f90) dont le code est devenu incompréhensible.

### Formats de fichier
* Créer un fichier NUM alternatif avec :
    - un format clé = valeur
    - ce format permet d'ajouter aisément de nouvelles entrées
    - juste les entrées nécessaires et valides pour ISM
    - oublier les paramètres qu'on ne change plus depuis longtemps :
        + type d'itération
        + choix de la forme de l'équation de continuité (section vs. largeur)
        + type de test d'arrêt
        + état initial permanent qui est redondant avec la présence / absence sur fichier INI dans REP.
* Prise en compte des dates calendaires au format ISO (AAAA-MM-JJ HH:MM:SS). Reste à faire :
    - C.L. solides : si T_inf est une date calendaire, ajouter T_inf aux temps lus dans le fichier de C.L. comme c'est déjà fait pour les C.L. liquides.
    - C.L. (toutes) : permettre de définir des C.L. avec des dates calendaires. Condition nécessaire : que T_inf soit déjà une date calendaire.
    - dates calendaires dans les règles de régulation.

### Divers
* Passer tous les tableaux dimensionnés à IBsup, NOsup ou ISsup en allocation dynamique comme cela a été fait pour AdisTS

## Validation

* Ajouter des cas-tests automatiques pour le charriage. Pour cela il faut :
    - créer une routine de comparaison sur le modèle de mage_Compare_BIN.f90 pour comparer les fichiers ST avec couches sédimentaires
    - ajouter la comparaison des fichiers ST état final.
    - fait 1 cas-test (n°31) mais sans la comparaison des fichiers ST avec couches sédimentaires ; il faut d'autres cas-test.
