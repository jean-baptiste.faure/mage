# SECMA V 6.10 ENTREE: Bief_17.ST                          SORTIE: Bief_17.M                          
#  Z: INTERPOL LINEAIRE
#  D MOY SUR MAX DE BORD 1         -BORD 2         100.00   
     1     1     0    49     350.0000             
     350.0000       0.0000       3.3300    
     350.0000       0.0000       3.3300    
     350.0000       0.0000       3.3300    
     350.0000       0.0000       3.3300    
     350.0000       0.0000       3.3300    
     350.0000       0.0000       3.3300    
     350.0000       0.0000       3.3300    
     350.0000       0.0000       3.3300    
     350.0000       0.0000       3.3300    
     350.0000       0.0000       3.3300    
     350.0000       0.0000       3.3300    
     350.0000       0.0000       3.3300    
     350.0000       0.0000       3.3300 rg 
     350.0000       0.3747       2.6118    
     350.0000       0.9043       2.0718    
     350.0000       1.6675       1.8004    
     350.0000       2.4307       1.5291    
     350.0000       3.2110       1.3259    
     350.0000       4.0121       1.2057    
     350.0000       4.8131       1.0855    
     350.0000       5.6142       0.9654    
     350.0000       6.4213       0.9110    
     350.0000       7.2308       0.8840    
     350.0000       8.0404       0.8570    
     350.0000       8.8500       0.8300 fon
     350.0000       9.5004       0.8625    
     350.0000      10.1509       0.8950    
     350.0000      10.8013       0.9276    
     350.0000      11.4517       0.9601    
     350.0000      12.0908       1.0557    
     350.0000      12.7121       1.2510    
     350.0000      13.3334       1.4462    
     350.0000      13.9547       1.6415    
     350.0000      14.5760       1.8367    
     350.0000      15.1972       2.0320    
     350.0000      15.5435       2.5314    
     350.0000      15.8000       3.1300 rd 
     350.0000      15.8000       3.1300    
     350.0000      15.8000       3.1300    
     350.0000      15.8000       3.1300    
     350.0000      15.8000       3.1300    
     350.0000      15.8000       3.1300    
     350.0000      15.8000       3.1300    
     350.0000      15.8000       3.1300    
     350.0000      15.8000       3.1300    
     350.0000      15.8000       3.1300    
     350.0000      15.8000       3.1300    
     350.0000      15.8000       3.1300    
     350.0000      15.8000       3.1300    
     999.9990     999.9990       0.0000    
     2     1     1    49     262.5000             
     262.5000       0.0000       3.3800    
     262.5000       0.0000       3.3800    
     262.5000       0.0000       3.3800    
     262.5000       0.0000       3.3800    
     262.5000       0.0000       3.3800    
     262.5000       0.0000       3.3800    
     262.5000       0.0000       3.3800    
     262.5000       0.0000       3.3800    
     262.5000       0.0000       3.3800    
     262.5000       0.0000       3.3800    
     262.5000       0.0000       3.3800    
     262.5000       0.0000       3.3800    
     262.5000       0.0000       3.3800 rg 
     262.5000       0.3304       2.7081    
     262.5000       0.7770       2.1697    
     262.5000       1.4410       1.8825    
     262.5000       2.1530       1.6517    
     262.5000       2.8777       1.4720    
     262.5000       3.6180       1.3546    
     262.5000       4.3586       1.2385    
     262.5000       5.0997       1.1250    
     262.5000       5.8452       1.0608    
     262.5000       6.5926       1.0172    
     262.5000       7.3401       0.9736    
     262.5000       8.0875       0.9300 fon
     262.5000       8.8048       0.9544    
     262.5000       9.5220       0.9788    
     262.5000      10.2393       1.0032    
     262.5000      10.9555       1.0470    
     262.5000      11.6627       1.1453    
     262.5000      12.3566       1.3183    
     262.5000      13.0497       1.4968    
     262.5000      13.7418       1.6821    
     262.5000      14.4339       1.8674    
     262.5000      15.1260       2.0527    
     262.5000      15.4906       2.6208    
     262.5000      15.7750       3.2800 rd 
     262.5000      15.7750       3.2800    
     262.5000      15.7750       3.2800    
     262.5000      15.7750       3.2800    
     262.5000      15.7750       3.2800    
     262.5000      15.7750       3.2800    
     262.5000      15.7750       3.2800    
     262.5000      15.7750       3.2800    
     262.5000      15.7750       3.2800    
     262.5000      15.7750       3.2800    
     262.5000      15.7750       3.2800    
     262.5000      15.7750       3.2800    
     262.5000      15.7750       3.2800    
     999.9990     999.9990       0.0000    
     3     1     2    49     175.0000             
     175.0000       0.0000       3.4300    
     175.0000       0.0000       3.4300    
     175.0000       0.0000       3.4300    
     175.0000       0.0000       3.4300    
     175.0000       0.0000       3.4300    
     175.0000       0.0000       3.4300    
     175.0000       0.0000       3.4300    
     175.0000       0.0000       3.4300    
     175.0000       0.0000       3.4300    
     175.0000       0.0000       3.4300    
     175.0000       0.0000       3.4300    
     175.0000       0.0000       3.4300    
     175.0000       0.0000       3.4300 rg 
     175.0000       0.2861       2.8043    
     175.0000       0.6497       2.2676    
     175.0000       1.2145       1.9646    
     175.0000       1.8752       1.7743    
     175.0000       2.5444       1.6181    
     175.0000       3.2240       1.5035    
     175.0000       3.9041       1.3915    
     175.0000       4.5851       1.2847    
     175.0000       5.2692       1.2107    
     175.0000       5.9544       1.1505    
     175.0000       6.6397       1.0902    
     175.0000       7.3250       1.0300 fon
     175.0000       8.1091       1.0463    
     175.0000       8.8932       1.0625    
     175.0000       9.6774       1.0788    
     175.0000      10.4592       1.1339    
     175.0000      11.2346       1.2349    
     175.0000      12.0010       1.3857    
     175.0000      12.7659       1.5473    
     175.0000      13.5288       1.7227    
     175.0000      14.2918       1.8981    
     175.0000      15.0547       2.0734    
     175.0000      15.4378       2.7103    
     175.0000      15.7500       3.4300 rd 
     175.0000      15.7500       3.4300    
     175.0000      15.7500       3.4300    
     175.0000      15.7500       3.4300    
     175.0000      15.7500       3.4300    
     175.0000      15.7500       3.4300    
     175.0000      15.7500       3.4300    
     175.0000      15.7500       3.4300    
     175.0000      15.7500       3.4300    
     175.0000      15.7500       3.4300    
     175.0000      15.7500       3.4300    
     175.0000      15.7500       3.4300    
     175.0000      15.7500       3.4300    
     999.9990     999.9990       0.0000    
     4     1     3    49      87.5000             
      87.5000       0.0000       3.4800    
      87.5000       0.0000       3.4800    
      87.5000       0.0000       3.4800    
      87.5000       0.0000       3.4800    
      87.5000       0.0000       3.4800    
      87.5000       0.0000       3.4800    
      87.5000       0.0000       3.4800    
      87.5000       0.0000       3.4800    
      87.5000       0.0000       3.4800    
      87.5000       0.0000       3.4800    
      87.5000       0.0000       3.4800    
      87.5000       0.0000       3.4800    
      87.5000       0.0000       3.4800 rg 
      87.5000       0.2418       2.9005    
      87.5000       0.5224       2.3655    
      87.5000       0.9881       2.0467    
      87.5000       1.5975       1.8970    
      87.5000       2.2111       1.7643    
      87.5000       2.8300       1.6523    
      87.5000       3.4496       1.5445    
      87.5000       4.0706       1.4443    
      87.5000       4.6931       1.3606    
      87.5000       5.3162       1.2837    
      87.5000       5.9394       1.2069    
      87.5000       6.5625       1.1300 fon
      87.5000       7.4135       1.1381    
      87.5000       8.2644       1.1463    
      87.5000       9.1154       1.1544    
      87.5000       9.9630       1.2209    
      87.5000      10.8065       1.3245    
      87.5000      11.6455       1.4531    
      87.5000      12.4822       1.5979    
      87.5000      13.3159       1.7633    
      87.5000      14.1497       1.9288    
      87.5000      14.9834       2.0942    
      87.5000      15.3850       2.7997    
      87.5000      15.7250       3.5800 rd 
      87.5000      15.7250       3.5800    
      87.5000      15.7250       3.5800    
      87.5000      15.7250       3.5800    
      87.5000      15.7250       3.5800    
      87.5000      15.7250       3.5800    
      87.5000      15.7250       3.5800    
      87.5000      15.7250       3.5800    
      87.5000      15.7250       3.5800    
      87.5000      15.7250       3.5800    
      87.5000      15.7250       3.5800    
      87.5000      15.7250       3.5800    
      87.5000      15.7250       3.5800    
     999.9990     999.9990       0.0000    
     5     2     0    49       0.0000             
       0.0000       0.0000       3.5300    
       0.0000       0.0000       3.5300    
       0.0000       0.0000       3.5300    
       0.0000       0.0000       3.5300    
       0.0000       0.0000       3.5300    
       0.0000       0.0000       3.5300    
       0.0000       0.0000       3.5300    
       0.0000       0.0000       3.5300    
       0.0000       0.0000       3.5300    
       0.0000       0.0000       3.5300    
       0.0000       0.0000       3.5300    
       0.0000       0.0000       3.5300    
       0.0000       0.0000       3.5300 rg 
       0.0000       0.1975       2.9967    
       0.0000       0.3950       2.4634    
       0.0000       0.7616       2.1288    
       0.0000       1.3197       2.0196    
       0.0000       1.8778       1.9104    
       0.0000       2.4360       1.8012    
       0.0000       2.9951       1.6975    
       0.0000       3.5561       1.6040    
       0.0000       4.1171       1.5105    
       0.0000       4.6780       1.4170    
       0.0000       5.2390       1.3235    
       0.0000       5.8000       1.2300 fon
       0.0000       6.7178       1.2300    
       0.0000       7.6356       1.2300    
       0.0000       8.5534       1.2300    
       0.0000       9.4667       1.3078    
       0.0000      10.3783       1.4141    
       0.0000      11.2900       1.5205    
       0.0000      12.1985       1.6485    
       0.0000      13.1030       1.8040    
       0.0000      14.0076       1.9594    
       0.0000      14.9121       2.1149    
       0.0000      15.3321       2.8891    
       0.0000      15.7000       3.7300 rd 
       0.0000      15.7000       3.7300    
       0.0000      15.7000       3.7300    
       0.0000      15.7000       3.7300    
       0.0000      15.7000       3.7300    
       0.0000      15.7000       3.7300    
       0.0000      15.7000       3.7300    
       0.0000      15.7000       3.7300    
       0.0000      15.7000       3.7300    
       0.0000      15.7000       3.7300    
       0.0000      15.7000       3.7300    
       0.0000      15.7000       3.7300    
       0.0000      15.7000       3.7300    
     999.9990     999.9990       0.0000    
