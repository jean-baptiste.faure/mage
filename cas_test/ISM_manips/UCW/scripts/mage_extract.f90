program mage_extract
!écriture de résultats partiels sur H_computed
   implicit none
   integer,parameter :: long=kind(1.0d0)
   integer :: lin, lout, i, ios
   real(kind = long) :: x, z, zf, h
   character(len=250) :: ligne
   open(newunit=lin,file='Mage_fin.ini',form='formatted',status='old')
   open(newunit=lout,file='H_computed',form='formatted',status='unknown')
   !on saute l'entête
   do i = 1, 9
      read(lin,*)
   enddo
   do
      read(lin,'(a)',iostat=ios) ligne
      if (ios /= 0) exit
      if (len_trim(ligne) == 0) exit
      read(ligne(21:31),*) z
      read(ligne(32:41),*) x
      read(ligne(42:52),*) zf
      h = (z - zf) * 1000._long
      write(lout,*) x, h
   enddo
   close(lin)
   close(lout)
end program mage_extract
