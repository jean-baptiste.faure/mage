! compilation :
! gfortran -shared -o debitx.so debitx.f90

function debitx(z1,z2,p1,p2,p3,p4,p5) bind(c, name='debitx')
    use, intrinsic :: iso_c_binding, only: c_double
    real(kind=c_double) :: debitX
    real(kind=c_double),intent(in) :: z1,z2,p1,p2,p3,p4,p5
    print*," => debitX", z1,z2,p2
    debitx = 0.0
end function debitx
