
! methode de la cross entropy ou recherche d evenements rares
subroutine Cross_entropy(echantillon,ifechantillon)
use i_Parametres, only : rdp, np, issup
   use i_Consigne, only : nc, wmin, wmax, nst, nbok, nech,ifinsert,iflin,ifacp,ifhybride
   use i_Annealing, only : nb_simul,nacp,dth,temperature
   implicit none
! Prototype
!
! Variables locales
   integer:: n, nok, i,j, lu,ntest,ntot,indice
   real(kind=rdp) :: ww(np),mu(np,2),sigma(np,2),w_best(np,2),echelle,echantillon(:,:),w_opt(np,2)
   real(kind=rdp),allocatable ,dimension(:,:) :: valeurs
   real(kind=rdp),allocatable ,dimension(:,:) :: valeurs_p
   real(kind=rdp),allocatable,dimension(:) ::foo_min
   real(kind=rdp) :: foo, err_cal,foo_init,ecart
   character(len=1200) :: texte
   logical :: bstop, bpause,LHS
   logical :: ifechantillon
   
ntest=nech ! population a tester
nelite=nech/10
echelle=1
allocate (valeurs(nc,2*nelite))
allocate (foo_min(2*nelite))
!--------------------------------------------------------------------------
   write(*,*) 'Simulations pour la méthode d Entropie croisee'
   write(8,*) 'Simulations pour la méthode d Entropie croisee'
   write(*,*) 'nombre de tests : ', ntest, 'nombre de population elite', nelite 
   write(8,*) 'nombre de tests : ', ntest, 'nombre de population elite', nelite 
  LHS=.true.
 if (.not. ifechantillon) then
allocate (valeurs_p(int(echelle*ntest,kind=4),nc))
 call sampling(wmin(1:nc),wmax(1:nc),valeurs_p,int(echelle*ntest,4),LHS) 
   ntot = 1  ;  n = 0
 
  ! parametres initiaux
   ww(:) = (wmax + wmin)/2._rdp
   !ww(:,2) = (wmax + wmin)/2._rdp
    foo_min=Foo_surface(ww,err_cal)
    foo_init=foo_min(1)
    do i=1,2*nelite
    valeurs(1:nc,i)=ww(1:nc)
   end do
  ! moyennes des gaussiennes initiales
  mu(:,1) = (wmax + wmin)/2._rdp
    mu(:,2) = (wmax + wmin)/2._rdp
  !mu(:,2) = (wmax + wmin)/2._rdp
  
  !variance des gaussiennes initiales
  sigma(:,1)=(wmax-wmin)*(wmax-wmin)/20._rdp
    sigma(:,2)=(wmax-wmin)*(wmax-wmin)/20._rdp
  !sigma(:,2)=(wmax-wmin)*(wmax-wmin)/40._rdp
  else 
 ! do i=1,	nc
    mu(:,1) = (wmax + wmin)/2._rdp
    mu(:,2) = (wmax + wmin)/2._rdp
  !mu(:,2) = (wmax + wmin)/2._rdp
  
  !variance des gaussiennes initiales
  sigma(:,1)=(wmax-wmin)*(wmax-wmin)/20._rdp
    sigma(:,2)=(wmax-wmin)*(wmax-wmin)/20._rdp
  !sigma(:,2)=(wmax-wmin)*(wmax-wmin)/40._rdp
  end if
  do while (n<namel)
          nok=0
          n=n+1
          ntot=1
          do while (nok<ntest)
          	    
                  if(n==1) then
                  ww(nc+1:np)=0.0_rdp
                  if (ntot>echelle*ntest) then
                   call sampling(wmin(1:nc),wmax(1:nc),valeurs_p,int(echelle*ntest,4),LHS) 
                   ntot=1
                   end if
                  ww(1:nc)=valeurs_p(ntot,1:nc)
                  else
                  ww(nc+1:np)=0.0_rdp
                  if(mod(ntot,2)==0) then
                  call simulation_gauss(ww,mu(:,1),sigma(:,1))
                  else
                  call simulation_gauss(ww,mu(:,2),sigma(:,2))
                  end if
                  end if
                  foo=Foo_Surface(ww,err_cal)
                  ntot=ntot+1
                  if (err_cal*Coeff_Penalisation<1.0_rdp) then
                          nok=nok+1
                          write(*,*) 'nb it ',nok,foo
                          write(8,*) 'nb it ',nok,foo
                          if (foo<maxval(foo_min(1:nelite)) .and. foo>0.0_rdp) then
                                  indice=maxloc(foo_min(1:nelite),dim=1)
                                  valeurs(:  ,indice)=ww(1:nc)
                                  foo_min(indice)=foo
                               end if
                                  if (foo>minval(foo_min(nelite+1:2*nelite))) then
                                         indice=minloc(foo_min(nelite+1:2*nelite),dim=1)+nelite
                                         valeurs(:  ,indice)=ww(1:nc)
                                          foo_min(indice)=foo
                                  end if
                  end if
                  !Arrêt anticipé si on trouve un fichier stop, Pause si fichier pause
                  inquire(file='stop',exist=bstop)
                  if (bstop) then  !arrêt anticipé
                                 open(newunit=lu,file='stop',status='unknown')
                                 close(lu,status='delete')  !suppression du fichier 'stop'
                                 write(*,*) ' >>> interruption par l''utilisateur !!!'
                                 write(8,*) ' >>> interruption par l''utilisateur !!!'
                                 return
                                 endif
                         inquire(file='pause',exist=bpause)
                         if (bpause) call mise_en_veille()
                 end do
          
          do i=1,2*nelite
                  write(texte,*) valeurs(1:nst,i), min(9999.999,foo),min(9999.999,1000._rdp*err_cal)
                  write(*,*) n,' : ', foo_min(i), err_cal,(n-1)*nelite+i, real(n*ntest)/real(ntot)
                  write(8,*) n,' : ', foo_min(i), err_cal,(n-1)*nelite+i, real(n*ntest)/real(ntot)
                  write(7,*) n,' : ', texte(1:len_trim(texte)), (n-1)*nelite+i, real(n*ntest)/real(ntot)
          end do
          
          !Affichage parametres trouvés 
          do i=1,nc
                  do j=1,2
                  call moy_var(mu(i,j),sigma(i,j),valeurs(i,(j-1)*nelite+1:j*nelite))
                  end do
          end do
          write (8,*) 'moyenne'
          write (*,*) 'moyenne'
          do i=1,nc
                  write (8,*) mu(i,1), mu(i,2)
                  write (*,*) mu(i,1), mu(i,2)
          end do
          write (8,*) 'sigma'
          write (*,*) 'sigma'
          do i=1,nc
                  write (8,*) sigma(i,1), sigma(i,2)
                  write (*,*) sigma(i,1), sigma(i,2)
          end do
          ecart=(maxval(foo_min)-minval(foo_min))/foo_init*100.0_rdp
          write(*,*) 'surface min',minval(foo_min),'surface max',maxval(foo_min),ecart,nb_simul
          write(8,*) 'surface min',minval(foo_min),'surface max',maxval(foo_min),ecart,nb_simul
  end do
  

   
 write(*,*) ntot,' simulation best result ', ecart,' calage ratio : '  , real(n*ntest)/real(ntot)
  write(8,*) ntot,' simulation best result ', ecart,' calage ratio : '  , real(n*ntest)/real(ntot)
  w_opt=0.0_rdp
  indice=minloc(foo_min(1:nelite),dim=1)
  w_opt(1:nc,1)=valeurs(1:nc,indice)
   indice=maxloc(foo_min(nelite+1:2*nelite),dim=1)+nelite
   w_opt(1:nc,2)=valeurs(1:nc,indice)
   
     if(ifhybride) then
     if(ifacp) then
   dth=1-((1.0e-04_rdp/temperature)**(1.0_rdp/(nacp))) 
    else
    dth=1-((1.0e-04_rdp/temperature)**(1.0_rdp/( n_it_max*0.6))) 
    end if
    write(*,*) 'nouveau dth' , dth,nacp,1.0_rdp/(nacp),temperature
    ! initialisation du recuit
    sens = -1._rdp
     modify => modify_2c
     working_Area => flooded_Area
    call simulated_annealing(w_opt,w_opt,Foo_Ecart_amel,ifinsert,ifacp,iflin)
   end if
 end subroutine Cross_entropy
 
 
 
