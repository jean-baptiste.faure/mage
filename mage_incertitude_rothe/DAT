* ### Fichier généré automatiquement par la commande mage_sa -g ###
*
* NB : toute ligne commençant pas une * est un commentaire
*
* Nom de base du fichier REP des simulations de MAGE
basename =<à compléter>
* Nom du dossier de référence pour récupérer la simulation MAGE
dossier_simulation =ref_simulation
* Nom du dossier de référence pour récupérer la simulation de calage
dossier_calage =ref_calage
* Chemin complet du solveur MAGE à utiliser
solveur =~/bin/mage_install/7.y/mage
* Température initiale pour l'algorithme du Recuit-Simulé
temperature_initiale = 2.0
* Taux de décroissance de la température de l'algorithme du Recuit-Simulé
taux_decroissance_temperature = 0.005
nb_idem = 1
* Arrondi appliqué aux consignes ; 0.001 -> 3 décimales
arrondi_consigne = 0.001
* Échelle : facteur appliqué au rayon de variation admissible pour chaque consigne
*           Par défaut ce rayon est Wmax - Wmin
echelle = 1.
* Pénalisation : coefficient de pénalisation des contraintes ; valeur par défaut : 10^6
penalisation = 0.
* seuil_amelioration = amélioration minimale pour retenir une nouvelle estimation 
*                      dans l'algorithme du Recuit-Simulé ; valeur par défaut : 0
seuil_amelioration = 0.0
* nech : le nombre d echantillons consideré pour amorcer les methodes :
* krigeage sur solutions calees, krigeage sur le calage et la surface
 * et, entropie croisee sur solutions calées
nech=150
* namel : le nombre d iterations adaptatives pour le krigeage avec la methode Expected Improvement
* le nombre de répétitions pour l entropie croisée
namel=25
* nelite : le nombre de la population d elite pour l entropie croisee (valeur recommandée 5% nech)
* le nombre de solutions candidates potentielles à tester après un krigeage
nelite=25
* n_it_max : le budget de simulations accordé pour tester l incertitude
* methodes recommandées selon le nombre de simulations (dépend des contraintes de calage)
*1-500 : krigeage solutions calées adaptatif avec Expected Improvement
*500-3000 : Entropie Croisée, krigeage calage et surface inondee
*3000-15000 : méthode hybride : krigeage cal+surf et recuit simulé initialisé par le résultat
*15000-30000 : methode hybride ou recuit simple
*+ de 30000 : recuit simulé et éventuellment monte-carlo si peu de paramètres
n_it_max=15000
* nacp :  pour le recuit simulé nombre de simulations calées avant de passer en mode ACP
, blocage aux bords ou linéaire
nacp=250
* n_acp_fin : nombre de simulations pour finir mode acp blocage aux bords ou linéarités
n_acp_fin=500
* iflin : booléen gérant l exploitation des linéarité dans la fonction voisinage recuit simulé
iflin=0
* ifinsert : booléen recuit simulé blocage des solutions aux bords
ifinsert=0
* ifacp : booléen pour appliquer acp dans le recuit simulé
ifacp=1
* ifhybride : booléen pour appliquer le recuit simulé apres une methode
ifhybride=1
*
* Fonction de cout (1/0)  : maximisation de l'écart surface Max - surface Min ou des hauteurs max
surface_inondee = 1
*      surface_EcartMax -> méthode du recuit simulé
*      Monte-Carlo      -> génération pseudo-aléatoire de nbok simulation satisfaisant 
*                          les contraintes et le calage
*      LHSMontecarlo-> génération pseudo-aléatoire de nbok simulation satisfaisant 
*      krigeage -> krigeage sur simulations calées avec méthode Expected Improvement
*      krigeage_EI -> krigeage sur le calage et la surface inondee avec R 
*      Entropiecroisée -> méthode du recuit simulé
*      GA_pikaia -> algorithme génétique pikaia 
FOtype = surface_EcartMax
* nbok : Nombre de simulations pour Monte-Carlo
nbok = 1000
*
* Géométrie : rayon de variation admissible de la cote du fond ; valeur par défaut : 0
geometrie =0.
*
* Strickler : début de la liste des contraintes sur les stricklers, définies tronçon par tronçon
<strickler>
*  ib x_debut x_fin K1_min K1_max K2_min K2_max K1_1 K2_1 K1_2 K2_2
*  n° bief, Pk début tronçon Pk fin du tronçon, Strickler min et max du lit mineur
*                                               Strickler min et max du lit majeur
*  Strickler mineur et majeur de départ pour la consigne n°1
*  Strickler mineur et majeur de départ pour la consigne n°2
* exemple (Saar - 6 tronçons)
  1  500.0   1700.0      22. 28. 12. 18. 25. 15. 25. 15.
  1 1700.0   2900.0      22. 28. 12. 18. 25. 15. 25. 15.
  1 2900.0   4100.0      22. 28. 12. 18. 25. 15. 25. 15.
  1 4100.0   5300.0      22. 28. 12. 18. 25. 15. 25. 15.
  1 5300.0   6500.0      22. 28. 12. 18. 25. 15. 25. 15.
  1 6500.0   7600.0      22. 28. 12. 18. 25. 15. 25. 15.
* Strickler : fin de la liste
</strickler>
*
* Erreur calage : valeur maximale admissible
erreur_calage = 0.1
* coeff_calage_Z : poids de l'erreur en cote
coeff_calage_Z = 1.
* coeff_calage_Q : poids de l'erreur en débit
coeff_calage_Q = 0.
* Données de calage en Z : début de liste
<calage_z>
* n°_bief  Pk  Z_observé
* Exemple (Saar - 3 laisses de crue)
   1 7600.0 139.52
   1 4100.0 137.18 
   1 2300.0 135.39 
* Données de calage en Z : fin de liste
</calage_z>
*
* optionnel : Données de calage en débit : début de liste
<calage_q>
* n°_bief  Pk  Z_observé
* Données de calage en débit : fin de liste
</calage_q>
