!INITIALISATION des donnees routine tres importante
subroutine Init(filename)
!==============================================================================
!    Initialisation des paramètres du L.A.G., des consignes et des contraintes
!==============================================================================
   use i_Parametres, only : rdp, np
   use i_Consigne, only : Wmin, Wmax, W_ini, nc, ib, xmin, xmax, &
                        modele, basename, solveur, eps, norme, date_fin, &
                        inhib, scale, wtest, ntest, nbpro, nst, deltaZ, Coeff_Penalisation, &
                        laisse_crue, nb_obs_z, nb_obs_q, date_fin_calage, erreur_calage, &
                        Q_rep, cz, cq, datageo, nbok,nech,namel,nelite,n_it_max, ifinsert,ifacp,iflin
   use i_Annealing, only : t0, nequil, dth, fk_opt, FOtyp, unite, nb_simul, seuil_amelioration,nACP,n_acp_fin
   implicit none
! Prototype
   character (len=*), intent(in) :: filename
! variables locales
   real(kind=rdp) :: gw(np), n1, n2, n3, n4, n5, n6
   integer :: n, lu=1, nt, io_status=0, m
   character :: ligne*80
   integer:: test1,test2,test3,test4,test5
   logical :: k_open=.false., z_open=.false., n_open=.false., test_open=.false., calz_open=.false., calq_open=.false.
   integer, allocatable :: seed(:)
   character :: FOtype*20
   
! dossier de référence pour les simulations
   character(len=20) :: ref_sim='simulation'
! dossier de référence pour le calage
   character(len=20) :: ref_cal='calage'
!------------------------------------------------------------------------------
!Lecture du point dat
   !open(unit=lu, file='incertitude.dat', form='formatted', status='old')
   open(unit=lu, file=trim(filename), form='formatted', status='old')
!FIXME: gérer l'erreur dans le cas où le fichier n'existe pas
   nt = 0 ; ntest = 0 ; nc = 0
   eps = 0._rdp
   deltaZ = 0._rdp
   Coeff_Penalisation = 1.e+06_rdp
   cz = 1._rdp
   cq = 0._rdp
   seuil_amelioration = 0._rdp
   nb_simul = 0
   write(8,'(a)') '#####################################################################################'
   call print_Version(8)
   write(8,'(a)') '#####################################################################################'
   write(8,'(a)') ' '
   write(8,'(a)') 'Copie du fichier incertitude.dat'
   do while (io_status==0)
      read(lu,'(a)', iostat=io_status) ligne
      write(8,'(a)') ligne(1:len_trim(ligne))
      if (ligne(1:1) == '*') then
         cycle
      elseif (ligne(1:11) == '<strickler>' .or. ligne(1:11) == '<Strickler>') then
         k_open = .true.
      elseif (ligne(1:12) == '</strickler>' .or. ligne(1:12) == '</Strickler>') then
         k_open = .false.
      elseif (k_open) then !lecture d'un tronçon ; nc = nb consignes = nb de tronçons
         nt = nt + 1 ; nc = 2*nt
         read(ligne,*) ib(nt), xmin(nt), xmax(nt), wmin(nc-1), wmax(nc-1), wmin(nc), wmax(nc), &
                                                   w_ini(nc-1,1), w_ini(nc,1), w_ini(nc-1,2), w_ini(nc,2)
      elseif (ligne(1:20) == 'temperature_initiale') then
         n = scan(ligne,'=')
         read(ligne(n+1:),'(f10.3)') t0
      elseif (ligne(1:16) == 'arrondi_consigne') then
         n = scan(ligne,'=')
         read(ligne(n+1:),'(f10.3)') eps
      elseif (ligne(1:29) == 'taux_decroissance_temperature') then
         n = scan(ligne,'=')
         read(ligne(n+1:),'(f10.3)') dth
      elseif (ligne(1:7) == 'nb_idem') then
         n = scan(ligne,'=')
         read(ligne(n+1:),*) nequil
      elseif (ligne(1:4) == 'nbok') then
         n = scan(ligne,'=')
         read(ligne(n+1:),*) nbok
      elseif (ligne(1:6) == 'modele') then
         n = scan(ligne,'=')
         read(ligne(n+1:),'(a)') modele
      elseif (ligne(1:8) == 'basename') then
         n = scan(ligne,'=')
         read(ligne(n+1:),'(a)') basename
      elseif (ligne(1:14) == 'dossier_calage') then
         n = scan(ligne,'=')
         read(ligne(n+1:),'(a)') ref_cal
      elseif (ligne(1:18) == 'dossier_simulation') then
         n = scan(ligne,'=')
         read(ligne(n+1:),'(a)') ref_sim
      elseif (ligne(1:7) == 'solveur') then
         n = scan(ligne,'=')
         read(ligne(n+1:),'(a)') solveur
      elseif (ligne(1:5)  == 'norme') then
         n = scan(ligne,'=')
         read(ligne(n+1:),*) norme
      elseif (ligne(1:8)  == 'date_fin') then
         n = scan(ligne,'=')
         read(ligne(n+1:),*) date_fin
      elseif (ligne(1:7)  == 'echelle') then
         n = scan(ligne,'=')
         read(ligne(n+1:),*) scale
      elseif (ligne(1:4)=='nech') then
      n= scan(ligne,'=')
       read(ligne(n+1:),*) nech
           elseif (ligne(1:5)=='namel') then
      n= scan(ligne,'=')
       read(ligne(n+1:),*) namel
           elseif (ligne(1:6)=='nelite') then
      n= scan(ligne,'=')
       read(ligne(n+1:),*) nelite
            elseif (ligne(1:8)=='n_it_max') then
      n= scan(ligne,'=')
       read(ligne(n+1:),*) n_it_max
       elseif (ligne(1:4)=='nacp') then
      n= scan(ligne,'=')
       read(ligne(n+1:),*) nACP
       elseif (ligne(1:9)=='n_acp_fin') then
      n= scan(ligne,'=')
       read(ligne(n+1:),*) n_acp_fin
       elseif (ligne(1:8)=='ifinsert') then
      n= scan(ligne,'=')
       read(ligne(n+1:),*) test1
       if(test1==0) then
       	       ifinsert=.false.
       	       else
       	       ifinsert=.true.
       end if
           elseif (ligne(1:5)=='iflin') then
      n= scan(ligne,'=')
       read(ligne(n+1:),*) test2
       if(test2==0) then
       	       iflin=.false.
       	       else
       	       iflin=.true.
       end if
           elseif (ligne(1:5)=='ifacp') then
      n= scan(ligne,'=')
       read(ligne(n+1:),*) test3
       if(test3==0) then
       	       ifacp=.false.
       	       else
       	       ifacp=.true.
       end if
       elseif (ligne(1:9)=='ifhybride') then
      n= scan(ligne,'=')
       read(ligne(n+1:),*) test4
       if(test4==0) then
       	       ifhybride=.false.
       	       else
       	       ifhybride=.true.
       end if
       elseif (ligne(1:15)=='surface_inondee') then
      n= scan(ligne,'=')
       read(ligne(n+1:),*) test5
        if(test5==0) then
       	       working_Area => vertical_Area
       	       else
       	       working_Area => flooded_Area
       end if
      elseif (ligne(1:7)  == 'FOtype') then
         n = scan(ligne,'=')
         read(ligne(n+1:),'(a)') FOtype
         FOtype = ADJUSTL(FOtype)
         !test
         n = len_trim(FOtype)
         Select case (FOtype)
         case('surface_EcartMax')
         FOtyp = 0 ; unite = ' % '
         case('Monte-Carlo') 
         FOtyp = 8 ; unite = ' ha'
         case('LHSMonte-Carlo')
         FOtyp = 10 ; unite = ' ha'
         case('Entropiecroisee')
         FOtyp=9;unite='ha'
         case('krigeage_EI')
         FOtyp=11;unite='%'
         case('krigeage')
         FOtyp=12;unite='%'
         case('GA_pikaia')
         FOtyp=13;unite='%'
          case('test_random')
         FOtyp=14;unite='%'
         case default
            write(*,*) '>>>> Erreur > FOtype inconnu! : ',FOtype(1:n),' <<<<'
            stop 001
         end select
         
      elseif (ligne(1:6) == '<test>') then
         ntest = ntest+1 ; test_open = .true.
      elseif (ligne(1:7) == '</test>') then
         test_open = .false.
      elseif (test_open) then  !lecture des consignes de test a priori
         read(ligne,*) nt, wtest(2*nt-1,1,ntest),wtest(2*nt,1,ntest),&
                           wtest(2*nt-1,2,ntest),wtest(2*nt,2,ntest)
      elseif (ligne(1:9) == 'geometrie') then
         n = scan(ligne,'=')
         read(ligne(n+1:),*) deltaZ
      elseif (ligne(1:12) == 'penalisation') then
         n = scan(ligne,'=')
         read(ligne(n+1:),*) Coeff_Penalisation
      elseif (ligne(1:10) == '<calage_z>' .or. ligne(1:10) == '<Calage_Z>') then
         calz_open = .true.
         nb_obs_z = 0
      elseif (ligne(1:11) == '</calage_z>' .or. ligne(1:11) == '</Calage_Z>') then
         calz_open = .false.
      elseif (calz_open) then !lecture du jeu de laisses de crue
         nb_obs_z = nb_obs_z + 1
         read(ligne,*) laisse_crue(nb_obs_z)%ib, laisse_crue(nb_obs_z)%x, laisse_crue(nb_obs_z)%z
      elseif (ligne(1:10) == '<calage_q>' .or. ligne(1:10) == '<Calage_Q>') then
         calq_open = .true.
         nb_obs_q = 0
      elseif (ligne(1:11) == '</calage_q>' .or. ligne(1:11) == '</Calage_Q>') then
         calq_open = .false.
      elseif (calq_open) then !lecture du jeu de laisses de crue
         nb_obs_q = nb_obs_q + 1
         read(ligne,*) Q_rep(nb_obs_q)%ib, Q_rep(nb_obs_q)%x, Q_rep(nb_obs_q)%z
      elseif (ligne(1:11) == 'date_calage') then
         n = scan(ligne,'=')
         read(ligne(n+1:),*) date_fin_calage
      elseif (ligne(1:13) == 'erreur_calage') then
         n = scan(ligne,'=')
         read(ligne(n+1:),*) erreur_calage
      elseif (ligne(1:14) == 'coeff_calage_Z') then
         n = scan(ligne,'=')
         read(ligne(n+1:),*) cz
      elseif (ligne(1:14) == 'coeff_calage_Q') then
         n = scan(ligne,'=')
         read(ligne(n+1:),*) cq
      elseif (ligne(1:18) == 'seuil_amelioration') then
         n = scan(ligne,'=')
         read(ligne(n+1:),*) seuil_amelioration
         seuil_amelioration = -abs(seuil_amelioration) !Pour être sûr que seuil_amelioration soit négatif
      endif
   enddo
   if (nb_obs_q > 0) unite = '   '
   close(lu)
   nst = nc
   if (deltaZ > 0._rdp) then
      call lireTAL(ref_sim)
      nc = nst + nbpro
      do nt = nst+1, nc
         wmin(nt) = -deltaZ*0.5_rdp
         wmax(nt) = +deltaZ*0.5_rdp
         w_ini(nt,1) = 0._rdp
         w_ini(nt,2) = 0._rdp
      enddo
      open(newunit=lu,file=trim(outdir)//'/verif.tal',form='formatted',status='unknown')
      call exportTAL(lu,datageo)
      close (lu)
   endif

   inhib = 0
   do nt = 1, nc
      if (wmax(nt)-wmin(nt) > 0.0001*w_ini(nt,1)) inhib(nt)=1
   enddo

   write(*,'(2a)') ' Solveur : ',solveur
!   write(*,'(2a)') ' Modele : ',modele
   write(*,'(2a)') ' Basename : ', basename
   write(*,'(2a)') ' Donnees : '
   write(*,'(4x,a,i4)') ' Nc = ', nc
!   write(*,'(4x,a,i4)') ' Nd = ', nd
   write(*,'(4x,a,f8.2)') ' T0 = ', t0
   write(*,'(4x,a,f8.2)') ' Dth = ', dth
   write(*,'(4x,a,i4)') ' Nequil = ', nequil
   write(*,'(4x,a,es10.3)') ' Precision = ', eps
   write(*,'(4x,a,f6.3)') ' Echelle = ', scale
   write(*,'(4x,a,es10.3)') ' Coefficient de penalisation = ',Coeff_Penalisation
   write(*,'(4x,2a,i1)') ' FOtype = ', FOtype

   write(*,'(a)') ' Consignes Stricklers'
   write(8,'(a)') ' Consignes Stricklers'
   do n = 1, nst/2
      write(*,'(i2,2f10.2,6f8.3)') ib(n), xmin(n), xmax(n), wmin(2*n-1), w_ini(2*n-1,1), wmax(2*n-1), &
                                                            wmin(2*n), w_ini(2*n,1), wmax(2*n)
      write(8,'(i2,2f10.2,6f8.3)') ib(n), xmin(n), xmax(n), wmin(2*n-1), w_ini(2*n-1,1), wmax(2*n-1), &
                                                            wmin(2*n), w_ini(2*n,1), wmax(2*n)
   enddo
   if (ntest > 0) then
      write(8,'(a)') ' >>> Tests a priori :'
      do nt = 1, ntest
         write(8,'(a,i2,a)') ' Tests a priori n°',nt,' (Stricklers seuls) :'
         do n = 1, nst/2
            write(8,'(a,i2,3x,4f8.3)') ' Troncon ',n, wtest(2*n-1,1,nt),wtest(2*n,1,nt),&
                                                     wtest(2*n-1,2,nt),wtest(2*n,2,nt)
         enddo
      enddo
   endif
   write(*,*) 'Calage'
   write(*,*) 'date_fin_calage = ',date_fin_calage
   write(*,*) 'erreur_calage = ',erreur_calage
   write(8,*)
   write(8,*) 'Calage'
   write(8,*) 'date_fin_calage = ',date_fin_calage
   write(8,*) 'erreur_calage = ',erreur_calage
   write(8,*) 'Laisses de crue :'
   do n = 1, nb_obs_z
      write(*,*) laisse_crue(n)%ib, laisse_crue(n)%x, laisse_crue(n)%z
      write(8,*) laisse_crue(n)%ib, laisse_crue(n)%x, laisse_crue(n)%z
   enddo
   do n = 1, nb_obs_q
      write(*,*) Q_rep(n)%ib, Q_rep(n)%x, Q_rep(n)%z
      write(8,*) Q_rep(n)%ib, Q_rep(n)%x, Q_rep(n)%z
   enddo

! initialisation du générateur de nombres pseudo-aléatoires
   call random_seed (size = m)             ! sets size to m
   allocate (seed(m))
   do n = 1, m
      seed(n) = 10742456 * n*n
      !seed(n) = 1234 * n*n
   enddo
   call random_seed (put = seed (1 : m))   ! sets user seed
   !call random_seed()
   write(*,'(a,i3,6(2x,i10))') ' Random seed : ',m,(seed(n),n=1,6)
   write(*,'(18x,6(2x,i10))') (seed(n),n=7,m)
   write(8,*) 'Random seed : ',m,(seed(n),n=1,m)
   
!initialisation des dossiers de calcul pour les simulations
   if (FOtyp < 2 .or. FOtyp > 7) then
      !répertoire pour les simulations de calage
      write(*,*) 'Création des répertoires de calcul pour le solveur externe'
      write(*,*) '--> Création des répertoires pour le calage'
      call execute_command_line('rm -r ./0')
      call execute_command_line('rm -r ./1')
      call execute_command_line('mkdir 0')
      call execute_command_line('cp '//trim(ref_cal)//'/* 0/')
      call execute_command_line('mkdir 1')
      call execute_command_line('cp '//trim(ref_cal)//'/* 1/')
      write(*,*) '--> Création des répertoires pour les simulations'
      call execute_command_line('rm -r ./2')
      call execute_command_line('rm -r ./3')
      call execute_command_line('mkdir 2')
      call execute_command_line('cp '//trim(ref_sim)//'/* 2/')
      call execute_command_line('mkdir 3')
      call execute_command_line('cp '//trim(ref_sim)//'/* 3/')
      write(*,*) '--> Création du répertoire pour les résultats de calage de la solution moyenne'
      call execute_command_line('rm -r ./4')
      call execute_command_line('mkdir 4')
      call execute_command_line('cp '//trim(ref_cal)//'/* 4/')
      write(*,*) '--> Création du répertoires pour les résultats de la simulation moyenne'
      call execute_command_line('rm -r ./5')
      call execute_command_line('mkdir 5')
      call execute_command_line('cp '//trim(ref_sim)//'/* 5/')
end if
end subroutine init

