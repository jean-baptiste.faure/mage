

!Modules pour variables globales precision et consignes
INCLUDE 'initialisation.f90'

!module d'algebre lineaire (inversion, resolution de systeme...)
INCLUDE 'linalg.f90'

!module algorithme genetique PIKAIA
INCLUDE 'pikaia.f90'
        
!module pour le recuit simule pour avoir en memoire parametres optimaux et erreurs
module calage_error
use i_Parametres 
real(kind=rdp)::err_cal1,err_cal2,surf_mini,surf_maxi,ecart_courant,surf_ini,ec1,ec2
real(kind=rdp):: param_maxi(np),param_mini(np)
integer::ind_min,ind_max
end module calage_error


! Module pour sauvegarder les fichiers de geometrie et en faire un modele simplifie
module geom_val
use i_Parametres
use i_Consigne
implicit none
integer::new_nc
integer,allocatable::indice(:)
real(kind=rdp),allocatable::pk(:),cote_fond(:)
end module geom_val


!Module d'analyse composantes principales
module ACP
use i_Parametres
use i_Consigne ,only :nc  
!nombre de composantes retenues
integer :: indice
! composantes principales, et vecteurs associe courants
real(kind=rdp) ,allocatable :: comp(:,:),new_vect(:),old_vect(:), &
& center(:),scales(:),comp_max(:),comp_min(:),opt_vect(:),norme2(:)
real(kind=rdp) f_opt_ACP !resultat optimal obtenu avec acp
logical::ifbegin
contains

! Desallocation de tous les parametres associe au module
subroutine reset_ACP
use i_Parametres
ifbegin=.false.
if (allocated(comp)) deallocate(comp)
if (allocated(comp)) deallocate(comp)
if (allocated(new_vect)) deallocate(new_vect)
if (allocated(old_vect)) deallocate(old_vect)
if (allocated(opt_vect)) deallocate(opt_vect)
if (allocated(scales)) deallocate(scales)
if (allocated(center)) deallocate(center)
if (allocated(comp_max)) deallocate(comp_max)
if (allocated(norme2)) deallocate(norme2)
if (allocated(comp_min)) deallocate(comp_min)
end subroutine reset_ACP

end module ACP
 module bords
 use i_Parametres
use i_Consigne ,only :nc  
use calage_error
!nombre de composantes retenues
integer :: indice
! composantes principales, et vecteurs associe courants
logical,allocatable :: bool_bord_min(:),bool_bord_max(:)
real(kind=rdp),allocatable :: param_mini_acp(:),param_maxi_acp(:)

contains

subroutine reset_bord()
implicit none
if(allocated(bool_bord_min)) deallocate(bool_bord_min)
if(allocated(bool_bord_max)) deallocate(bool_bord_max)
if(allocated(param_mini_acp)) deallocate(param_mini_acp)
if(allocated(param_maxi_acp)) deallocate(param_maxi_acp)

end subroutine


subroutine bord_init(div)
 use i_Parametres
use i_Consigne ,only :nc,wmax,wmin
implicit none
integer :: i
real(kind=rdp) :: param_moy(nc),d1,d2,d3,e1,e2,e3,seuil(nc),div
call reset_bord()
allocate(bool_bord_min(nc),bool_bord_max(nc),param_mini_acp(nc),param_maxi_acp(nc))
param_moy=(wmin(1:nc)+wmax(1:nc))/2.0_rdp
param_maxi_acp=param_maxi(1:nc)
param_mini_acp=param_mini(1:nc)
seuil=(wmax(1:nc)-wmin(1:nc))/div
bool_bord_max=.false.
bool_bord_min=.false.
do i=1,nc
if(mod(i,1)==0) then
d3=seuil(i)/2.0_rdp
else 
d3=seuil(i)
end if
d1=abs(param_maxi_acp(i)-wmax(i))
d2=abs(param_maxi_acp(i)-wmin(i))
if (d1<d3 ) then
	bool_bord_max(i)=.true.
	param_maxi_acp(i)=wmax(i)
else if (d2<d3) then
	bool_bord_max(i)=.true.
	param_maxi_acp(i)=wmin(i)
else
	bool_bord_max(i)=.false.
end if

d1=abs(param_mini_acp(i)-wmax(i))
d2=abs(param_mini_acp(i)-wmin(i))
d3=seuil(i)!abs(param_mini_acp(i)-param_moy(i)) 
if (d1<d3 ) then
	bool_bord_min(i)=.true.
	param_mini_acp(i)=wmax(i)
else if (d2<d3) then
	bool_bord_min(i)=.true.
	param_mini_acp(i)=wmin(i)
else
	bool_bord_min(i)=.false.
end if
end do


end subroutine bord_init
 end module bords
!module gerant aleatoire : generation graine, nb aleatoire et fonction modification pour SA
INCLUDE 'random.f90'

!module recuit simule generaliser pour optimisation globale
INCLUDE 'recuit.f90'

!module optimisation : contient les methode relatives au krigeage avec critere expected improvement
INCLUDE 'module_optim.f90'

!Main program
program mage_SA

!Variables locales necessaires
INCLUDE 'param.f90'
   
!Gestion des parametres au terminal
INCLUDE 'terminal.f90'
   

   ! initialisation
   call cpu_time(debut) ; cpu_externe = 0._rdp ! demarrage du chrono
   call date_and_time(ddate,time,zone,values)
   tmp = ddate//'-'//time(1:6)
   !fichiers de resultat donnant historique de ce que fait le programme              
  ! entree 8 sortie de l execution du programme
   open(unit=8, file=trim(outdir)//'/sortie_'//tmp//'.txt', form='formatted', status='unknown')
   
   call init(datafile)  !initialisation
   write(*,*) 'test'
   write(*,*) ifinsert,ifacp,iflin
   !resultat sur le stricklers
  ! entree 7 sortie des differents stricklers
   open(unit=7, file=trim(outdir)//'/stricklers_'//tmp//'.txt', form='formatted', status='unknown')
   
   ! entree 9 sortie des differentes cotes du fond
   if (deltaZ > 0._rdp) open(unit=9, file=trim(outdir)//'/geometrie_'//tmp//'.txt', form='formatted', status='unknown')
    if (eps > 0._rdp) then
         modify => modify_2a
    else
        ! modify => modify_2b ancienne fonction modify : les deux surfaces changeaient en meme temps
        	modify => modify_2c
    endif
    temperature=t0
   select case (FOtyp)        
   case(0)       ! écart maximal sur des surfaces fonction de cout std
      sens = -1._rdp
      if (ntest > 0) then
         f_best = 0.
         do i = 1, ntest
            write(*,'(a,i2)') 'Test numero ',i
            w_opt(:,:) = wtest(:,:,i)
            fk_opt = Foo_Ecart_amel(w_opt,C_Plus) ; tmp = affiche(fk_opt,unite)
            if (C_Plus < 0.001) then
               tmp2 = ' calage OK'
            else
               tmp2 = ' calage KO'
            endif
            write(*,'(a,i2,2a)') ' Optimum a priori numero ',i,' = ',tmp//tmp2
            write(8,'(a,i2,2a)') ' Optimum a priori numero ',i,' = ',tmp//tmp2
         enddo
      endif
      call simulated_annealing(W_ini,W_opt,Foo_Ecart_amel,ifinsert,ifacp,iflin)    !algo du recuit simulé
   case(8)
      call Monte_Carlo(ifacp)                                  !Monte-Carlo classique
   case (9)
   ! algo entropie croisee
      call Cross_entropy(W_opt,.false.)
   case(10)
   ! algo Monte-carlo avec hypercube latin
      call Monte_carlo_surface()
   case(11)
   ! algo krigeage utilisant le critere de  l expected improvement
      call krigeage()
       
    case(12)
   !methode de krigeage avec 2 meta-modele pour le calage et pour la surface inondee
   !test si on integre la geometrie
     if(nc>nst) then
    	    ! krigeage parametre pour integrer la geometrie 
                call R_krigeage_geom()
    else
                  ! krigeage avec les stricklers uniquement
    	    call R_krigeage()
    end if
    case (13) 
    ! algorithme genetique pikaia
      call GA_pikaia()
            case(14) 
      !test du generateur random
     nc=2!test d equirepartition sur n dimensions
    call test_random()
   case default
      stop '>>>> Erreur : FOtype inconnu !!!!!<<<<'
   end select
   ! Affichage du resultat en sortie 8
INCLUDE 'resultat.f90'
   
   contains
 !=======================================================
          !Fonctions et subroutines
!=======================================================

!differentes fonctions de cout pour la surface inondees utilisee par recuit
INCLUDE 'fonction_cout.f90'

!Fonction qui lit les fichiers et .dat  pour initialiser les valeurs  
INCLUDE 'Init.f90'

! Methode d'optimisation considerees
INCLUDE 'optimisation.f90'

!Petites Fonctions pratiques (affichage, mise en forme ...)
INCLUDE 'utilitaire.f90'

!fonction pour verifier le calage
INCLUDE 'calage.f90'



!fonctions comprenant les modules lies aux meta-modeles
INCLUDE 'interpolation.f90'



end program mage_SA



