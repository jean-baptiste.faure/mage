
! Krigeage appelant routine de R
subroutine R_krigeage
use i_Parametres, only : rdp, np, issup
use i_Consigne
use i_Annealing,only : dth,temperature,nacp,n_acp_fin 
use linalg
use Genetic_algorithm
use recuit_simule1
use recuit_simule
use R_krig
use rand_num
implicit none

! Variables locales
integer :: n, k, nok, i,j,ntot,n_LHS,nb_amel,nb_ech,nb_points
real(kind=rdp) :: ww(np)
real(kind=rdp) ,allocatable :: param_LHS(:,:),ech(:,:)
real(kind=rdp) :: w_optim(np,2),w_init(np,2),test
real(kind=rdp) :: foo, err_cal,foo_min, foo_max,foo_ini
real(kind=rdp),allocatable::EI_max(:,:),EI_min(:,:)
logical:: testeur,LHS
real ::  ecart_max
real(kind=rdp)::x(2*nc),x_max(2*nc),w_maxi(nc,100),w_mini(nc,100),val_min(100),val_max(100)
real(kind=rdp) :: x_ini(2*nc),x_mini(nc),x_maxi(nc)
integer :: status,ind1,ind2
logical:: ifamel
   
!--------------------------------------------------------------------------
write(*,*) 'Simulations pour la méthode interpolation krigeage'
write(8,*) 'Simulations pour la méthode interpolation krigeage'
open(10,file='resultat.txt',access='append')
open(16,file='ech_krig.txt',status='replace')
write(10,*) (i,i=1,nc+3)


write(*,*) nech  
nok = 0  
n = 0
nb_ech=max(nech,nc+1)!
n_LHS=nb_ech
LHS=.true.
nb_amel=1
ifamel=.false.!nb_ech/8;
nb_points=2
val_min=0._rdp
val_max=0._rdp
w_mini=0._rdp
w_maxi=0._rdp



allocate(EI_max(nc,nb_points))
allocate(EI_min(nc,nb_points))
allocate(param_LHS(n_LHS,nc))
if (ifamel) then
allocate (ech(nc+2,nb_ech+nb_amel*2*nb_points))
else
allocate (ech(nc+2,nb_ech))
end if



!!! Echantillon de base  suivant un carre latin !!!!
!call plan_exp_ACP(param_LHS,n_LHS)

call sampling(wmin(1:nc),wmax(1:nc),param_LHS,n_LHS,LHS)
ww = (wmax + wmin)/2._rdp
! boucle tant que on n a pas le nombre d echantillon
do while (nok<nb_ech)
      n = n+1
      ntot=ntot+1
      foo = Foo_Surface_no_cal(ww,err_cal)
      if (n==1) then
                    foo_ini=foo
                    close(16)
                    open(16,file='ech_krig.txt',access='append')
                    end if
                    
                    nok = nok + 1
                    ! sauvegarde des donnees
                    err_cal=calage_ok(ww,'0')
                    ech(1:nc,nok)=ww(1:nc)
                    ech(nc+1,nok)=foo
                    ech(nc+2,nok)=err_cal*Coeff_Penalisation
                    
                    !ecriture des donnees
                    write (*,*)  'nb sim tot',n,' nb sim ',nok,' surface inondee ',real(ech(nc+1,nok)*0.0001_rdp,4),err_cal*1000000
                    write (8,*)  'nb sim tot',n,' nb sim ',nok,' surface inondee ',real(ech(nc+1,nok)*0.0001_rdp,4),err_cal*1000000
                    write(10,*) (ww(i),i=1,nc),foo,maxval(ech(nc+1,:)),minval(ech(nc+1,:))
                    write(16,*) (ww(i),i=1,nc),foo,err_cal*Coeff_Penalisation
               ! on passe aux parametres suivant
       ww(1:nc)=param_LHS(n,1:nc)
   enddo
   close(16)
  write (*,*) 'surface initiale ' , foo_ini
   write (8,*) 'surface initiale ' , foo_ini
   n=0
   
   ! Test si on applique Expected Improvement ou pas
   if(ifamel) then
   ! on repete jusqu au nombre d iteration souhaite
   do while(n<nb_amel)
   ! appel script R
   call execute_command_line("Rscript krig_R.R")
   ! ouverture des resultats donnant les nouveaux points à tester 
   open(17,file='EI_krig.txt')
   do j=1,nb_points
   read (17,*) (EI_min(i,j),i=1,nc)
   end do
   do j=1,nb_points
   read (17,*) (EI_max(i,j),i=1,nc)
   end do
   close(17)
   
   ! test des nouveaux points minimaux attendus
   do k=1,nb_points
   ww(1:nc)=EI_min(:,k)
   foo=Foo_Surface_no_cal(ww,err_cal)
   ! test de calage
      if (err_cal*Coeff_Penalisation< 1._rdp) then
                    nok = nok + 1
                    ech(1:nc,nok)=ww(1:nc)
                    err_cal=calage_ok(ww,'0')
                    ech(nc+1,nok)=foo
                    ech(nc+2,nok)=err_cal*Coeff_Penalisation
                    write (*,*)  'valeur_min',foo,'vrai min',minval(ech(nc+1,:),mask=ech(nc+1,:)>0._rdp,dim=1),'nb amel',n
                     write (8,*)  'valeur_min',foo,'vrai min',minval(ech(nc+1,:),mask=ech(nc+1,:)>0._rdp,dim=1),'nb amel',n
                    write(10,*) (ww(i),i=1,nc),foo,maxval(ech(nc+1,:),dim=1),minval(ech(nc+1,:),mask=ech(nc+1,:) &
                    & >0._rdp,dim=1)
                    write(16,*) (ww(i),i=1,nc),foo,err_cal

      end if
      end do
      
      ! test des points maximaux attendus
      do k=1,nb_points
   ww(1:nc)=EI_max(:,k)
   foo=Foo_Surface_no_cal(ww,err_cal)
   ! test de calage
      if (err_cal *Coeff_Penalisation< 1._rdp) then
                    nok = nok + 1
                     err_cal=calage_ok(ww,'0')
                    write (*,*)  'valeur_max',foo,'vrai min',maxval(ech(nc+1,:),dim=1),'nb amel',n
                     write (8,*)  'valeur_max',foo,'vrai min',maxval(ech(nc+1,:),dim=1),'nb amel',n
                    write(10,*) (ww(i),i=1,nc),foo,maxval(ech(nc+1,:),dim=1),minval(ech(nc+1,:),dim=1)
                    write(16,*) (ww(i),i=1,nc),foo,err_cal
                    ech(1:nc,nok)=ww(1:nc)
                    ech(nc+1,nok)=foo
                    ech(nc+2,nok)=err_cal*Coeff_Penalisation
      end if
      end do
      n=n+1
   end do
   end if
        ! appel script R pour obtenir les metamodele de calage et de surface finaux
         call execute_command_line("Rscript model_krig_final_R.R")
         ! Portage des meta-modeles en FORTRAN
         call init_krig(ech(1:nc,:),ech(nc+2,:),ech(nc+1,:))

! Verification de  la precision de l interpolation
do i=1,nb_ech
        foo=eval_surf(ech(1:nc,i))
        err_cal=eval_cal(ech(1:nc,i))
                write(*,*) 'valeur interpolee : ' ,foo, 'valeur' ,valeurs_surf(i),err_cal*1.0e-06_rdp
                write(8,*) 'valeur interpolee : ' ,foo, 'valeur' ,valeurs_surf(i) ,err_cal*1.0e-06_rdp
end do

!Recherche de l ecart maximum sur  l echantillon initial
ind1=minloc(valeurs_surf,mask=valeurs_cal<1.0_rdp,dim=1)
ind2=maxloc(valeurs_surf,mask=valeurs_cal<1.0_rdp,dim=1)
! sauvegarde parameters ecart initial
x(nc+1:2*nc)=ech(1:nc,ind2)
x(1:nc)=ech(1:nc,ind1)
x_ini=x
foo=eval_ecart(x)
write(*,*) 'ecart_initial',foo
write(8,*) 'ecart_initial',foo

! Calcul du maximum selon les deux metamodeles
! boucle sur le nombre de solutions potentielle desirees 
   do j=1,nelite
   x_max=x_ini
   ecart_max=eval_ecart(x_ini)
   foo_min=eval_surf(x_ini(1:nc))
   foo_max=eval_surf(x_ini(nc+1:2*nc))
   foo=foo_max-foo_min
   x_mini=x_ini(1:nc)
   x_maxi=x_ini(nc+1:2*nc)
   if (foo<0._rdp) then
              foo=foo_max
              foo_max=foo_min
              foo_min=foo
              x_mini=x_ini(nc+1:2*nc)   
              x_maxi=x_ini(1:nc)    
   end if

   call init_random_seed()
   
   !APPROCHE RECUIT SIMULE
   	   
   ! calcul du max
   call init_recuit(nb_param,wmin(1:nc),wmax(1:nc),x_maxi(1:nc),foo_max,-1._rdp,0.005_rdp)
   nequil=1
   call recuit_sa(x_maxi,eval_surf_cal)
   
   ! calcul du min
     call init_recuit1(nb_param,wmin(1:nc),wmax(1:nc),x_mini(1:nc),foo_min,1._rdp,0.005_rdp)
   nequil1=1
   call recuit_sa1(x_mini,eval_surf_cal_min)

   ! sauvegarde des resultats
       foo_max=eval_surf(x_maxi)
      foo_min=eval_surf(x_mini)  
                    val_max(j)=foo_max
                    val_min(j)=foo_min
                    w_mini(:,j)=x_mini
                    w_maxi(:,j)=x_maxi
                    
      foo=abs(foo_max-foo_min)/valeurs_surf(1)*100._rdp
     write(*,*) 'it', j,'vrai_ecart',foo,'foo_max',foo_max,'foo_min',foo_min
     write(8,*) 'it', j,'vrai_ecart',foo,'foo_max',foo_max,'foo_min',foo_min
     end do
     
     w_optim=0._rdp
     
     ! Test des solutions potentielles avec mage
     do j=1,nelite
     w_optim(1:nc,1)=w_maxi(:,j)
      w_optim(1:nc,2)=w_mini(:,j)
      ! appel de mage
      foo_max=Foo_Surface(w_optim(:,1),err_cal)
      foo_min=Foo_Surface(w_optim(:,2),err_cal)
      ! sauvegarde resultat simulation
      val_max(j)=foo_max
      val_min(j)=foo_min
      write(*,*) 'foo_max',foo_max,'foo_min',foo_min
       write(8,*) 'foo_max',foo_max,'foo_min',foo_min
     end do
     
     ! Recherche du meilleur resultat parmi l echantillon de base et les solutions potentielles
     w_init=0._rdp
     ind1=maxloc(val_max,dim=1)
     ind2=minloc(val_min,mask=val_min>0._rdp,dim=1)
     foo_max=maxval(val_max,dim=1)
     foo_min=minval(val_min,mask=val_min>0.0_rdp,dim=1)
     ! gestion des exceptions (cas ou les solutions potentielles ne sont pas calees)
     if(ind1>0 .and. foo_max>0.0_rdp) then
     	      w_init(1:nc,1)=w_maxi(:,ind1)
     else
        w_init(1:nc,1)=x_ini(nc+1:2*nc)
        foo_max=eval_surf(x_ini(nc+1:2*nc))
       end if
     if(ind2>0 .and. foo_min>0.0_rdp) then
      w_init(1:nc,2)=w_mini(:,ind2)
     else
      foo_min=eval_surf(x_ini(1:nc))
     w_init(1:nc,2)=x_ini(1:nc)
      end if
     foo=abs(foo_max-foo_min)/valeurs_surf(1)*100._rdp
     write(*,*) 'ecart_final',foo,'foo_max',foo_max,'foo_min',foo_min
      write(8,*) 'ecart_final',foo,'foo_max',foo_max,'foo_min',foo_min
     
      testeur=ifhybride ! booleen pour savoir si l on fait un recuit apres le krigeage
    if (testeur) then
     if (ifacp) then
      dth=1-((1.0e-04_rdp/temperature)**(1.0_rdp/(nacp))) 
    else
    dth=1-((1.0e-04_rdp/temperature)**(1.0_rdp/( n_it_max))) 
    end if
    write(*,*) 'nouveau dth' , dth
    ! initialisation du recuit
    w_optim=0._rdp
    sens = -1._rdp
     modify => modify_2c
     working_Area => flooded_Area
    call simulated_annealing(w_init,w_optim,Foo_Ecart_amel,ifinsert,ifacp,iflin)
    end if
end subroutine R_krigeage

