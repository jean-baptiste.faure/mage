
! Lit les composantes principales isssues de script R pour faire un plan d experience (hypercubbe latin)
subroutine plan_exp_ACP(echantillon,n)
use i_Parametres
implicit none
integer :: i,j
integer , intent(in) :: n
real(kind=rdp) :: echantillon(n,nc),ech(n,2) 
character(len=40) :: file1,file2,file3
real(kind=rdp) :: center(nc),scale(nc),comp(2*nc),comp1(nc),comp2(nc),w_min(2),w_max(2)
! nom des fichiers
file1='ACP_center_R.txt'
file2='ACP_scale_R.txt'
file3='ACP_comp_R.txt'

! lecture des fichiers
open(11,file=file1)
read(11,*) (center(i),i=1,nc)
write(*,*) (real(center(j),kind=4),j=1,nc)

open(12,file=file2)
read(12,*) (scale(i),i=1,nc)
write(*,*) (real(scale(j),kind=4),j=1,nc)
open(13,file=file3)
read(13,*) (comp(i),i=1,2*nc)
comp1=comp(1:nc)
comp2=comp(nc+1:2*nc)

w_min=-6._rdp
w_max=6._rdp
! tirage hypercube latin dans l espace des deux premieres comp principales
call sampling(w_min,w_max,ech,n,.true.)


! Recentre et remet a l echelle les donnees 
do i=1,n
echantillon(i,:)=ech(i,1)*comp1(:)+ech(i,2)*comp2(:)
end do
do i=1,nc
echantillon(:,i)=echantillon(:,i)*scale(i)+center(i)
end do

! assure que les parametres finaux sont bien dans l'espace initial
do i=1,nc
do j=1,n
echantillon(j,i)=max(echantillon(j,i),wmin(i))
echantillon(j,i)=min(echantillon(j,i),wmax(i))
end do
end do

end subroutine plan_exp_ACP



! Appel du script R realisant l acp à partir des donnees : echantillon et donne la correlation: correlation
! des parametres avec la surface inondee
subroutine corr(echantillon,correlation,n)
use i_Parametres
use i_Consigne, only  : Coeff_Penalisation
implicit none
integer::i,j,nb_ech,n
real(kind=rdp) :: echantillon(:,:),correlation(nc)
real(kind=rdp) ,allocatable:: Matrice_test(:,:)
nb_ech=n
allocate(Matrice_test(2*nb_ech,nc+2))
! reecriture de donnees 
!pour que 1 jeu de parametres donne 1 surface au lieu de 2 jeux donne  1 ecart
do j=1,nb_ech

        Matrice_test(2*j-1,1:nc)=echantillon(1:nc,j)
        Matrice_test(2*j-1,nc+1)=echantillon(2*nc+2,j)
         Matrice_test(2*j-1,nc+2)=echantillon(2*nc+1,j)
        Matrice_test(2*j,1:nc)=echantillon(nc+1:2*nc,j)
        Matrice_test(2*j,nc+1)=echantillon(2*nc+3,j)
         Matrice_test(2*j,nc+2)=echantillon(2*nc+4,j)
end do
! ecriture dans fichier
open(11,file='ACP.txt')
write(11,*) (i,i=1,nc+1)
do i=1,2*nb_ech
! ne prend en compte que les recultats cales
               if (Matrice_test(i,nc+2)*Coeff_Penalisation<1.0_rdp .and. Matrice_test(i,nc+1)>0.0_rdp ) &
               & write(11,*) (Matrice_test(i,j),j=1,nc+1)
end do
close(11)
! appel de R
call execute_command_line("Rscript testR.R")
! ecriture de la correlation
open(16,file='mat_cor_R.txt')
read (16,*) (correlation(i),i=1,nc)
close(16)
write(*,*) (real(correlation(i),kind=4),i=1,nc)
write(8,*) (real(correlation(i),kind=4),i=1,nc)
end subroutine corr


subroutine ACP_R_data(echantillon)
use i_Parametres
use i_Consigne, only  : Coeff_Penalisation
implicit none
integer::i,j,nb_ech,n
real(kind=rdp), intent(in) :: echantillon(:,:)
n=size(echantillon(:,1))

! ecriture dans fichier
open(11,file='ACP.txt')
write(11,*) (i,i=1,nc+1)
do i=1,n
                write(11,*) (echantillon(i,j),j=1,nc+1)
end do
close(11)
! appel de R
call execute_command_line("Rscript testR.R")

end subroutine ACP_R_data
subroutine modify_ecart(w_sa,w_new,ampli,indice)
!==============================================================================
!  routine de modification
!   sans arrondi
!==============================================================================
   use i_Parametres, only : rdp, np
   use i_Consigne, only : nc, inhib, wmin, wmax
   use rand_num
   use calage_error
   implicit none
! Prototype
   real(kind=rdp), intent(in) :: W_sa(np,2), ampli(np)
   real(kind=rdp), intent(out) :: W_new(np,2)
! Variables locales
   integer :: n,indice
!--------------------------------------------------------------------------
!!$omp parallel default(shared) private(n)
!   !$omp do schedule(dynamic)
   do n = 1, nc
      if( inhib(n) == 0 ) then
         w_new(n,indice) = w_sa(n,indice)
       else
         w_new(n,indice) = random(max(w_sa(n,indice)-ampli(n)/2._rdp,wmin(n)), &
         & min(w_sa(n,indice)+ampli(n)/2._rdp,wmax(n)))
      endif
   end do
!   !$omp end do
!!$omp end parallel
end subroutine modify_ecart

subroutine modify_geom(w_sa,w_new,ampli)
!==============================================================================
!  routine de modification
!   sans arrondi
!==============================================================================
   use i_Parametres, only : rdp, np
   use i_Consigne, only : inhib, wmin, wmax
   use rand_num
   use geom_val
   implicit none
! Prototype
   real(kind=rdp), intent(in) :: W_sa(np,2), ampli(np)
   real(kind=rdp), intent(out) :: W_new(np,2)
   real(kind=rdp) :: dummy(new_nc+nst,2)
! Variables locales
   integer :: n
!--------------------------------------------------------------------------
!!$omp parallel default(shared) private(n)
!   !$omp do schedule(dynamic)
   do n = 1, nst
      if( inhib(n) == 0 ) then
         dummy(n,1) = w_sa(n,1)
         dummy(n,2) = w_sa(n,2)
       else
         dummy(n,1) = random(max(w_sa(n,1)-ampli(n),wmin(n)),min(w_sa(n,1)+ampli(n),wmax(n)))
         dummy(n,2) = random(max(w_sa(n,2)-ampli(n),wmin(n)),min(w_sa(n,2)+ampli(n),wmax(n)))
      endif
   end do
!   !$omp end do
!!$omp end parallel
do n=nst+1,nst+new_nc
dummy(n-nst,1)=random(max(w_sa(indice(n),1)-ampli(n),wmin(n)), &
&min(w_sa(indice(n),1)+ampli(n),wmax(n)))
dummy(n-nst,2)=random(max(w_sa(indice(n),1)-ampli(n),wmin(n)), &
&min(w_sa(indice(n),1)+ampli(n),wmax(n)))
end do
call convert_geom(dummy(:,1),w_new(:,1))
call convert_geom(dummy(:,2),w_new(:,2))
end subroutine modify_geom


! Modification pour le recuit en tenant compte des linearites du probleme
subroutine modify_ACP_lin(w_sa,w_new,ampli,correl)
!==============================================================================
!  routine de modification
!   sans arrondi
!==============================================================================
   use i_Parametres, only : rdp, np
   use i_Consigne, only : nc, inhib, wmin, wmax
   use i_Annealing, only :temperature
   use rand_num
   implicit none
! Prototype
   real(kind=rdp), intent(in) :: W_sa(np,2), ampli(np),correl(nc)
   real(kind=rdp), intent(out) :: W_new(np,2)
   real(kind=rdp) :: a,b
! Variables locales
   integer :: n,j
!--------------------------------------------------------------------------

       w_new = w_sa  !initialisation globale avant modif de certaines composantes
     i = 1+mod(nb_eval,4)/2   !indice consigne 1 ou 2
      j = 1-mod(nb_eval,2)      !groupe Strickler mineur (1) ou Strickler majeur (0)
   do n = 1, nc
      if( inhib(n) == 0 ) cycle            !on ignore cette composante si inhib est nul              
      if(n <= nst .and. mod(n,2)/=j) cycle !ou selon que n est pair ou impair (alternativement) pour les stricklers
      a = w_sa(n,i)-ampli(n)
      b = w_sa(n,i)+ampli(n)
      w_new(n,i) = random(a,b)
      ! on bloque aux bords ici pour faciliter l'apparition de consignes
      ! calées sur les bords car les solutions sont souvent là
      w_new(n,i) = max (w_new(n,i),wmin(n))
      w_new(n,i) = min (w_new(n,i),wmax(n))
       if (abs(correl(n))>0.97_rdp) then
       if(correl(n)>0) w_new(n,1)=wmin(n);w_new(n,2)=wmax(n)!+random(-1._rdp,0._rdp)*temperature
       if(correl(n)<0) w_new(n,1)=wmax(n);w_new(n,2)=wmin(n)!+random(0._rdp,1._rdp)*temperature
       inhib(n)=0
       end if
   enddo
   


end subroutine modify_ACP_lin

! On transcrit tous les fichiers texte issus de la routine corr dans des variables fortran
subroutine init_ACP()
use i_Parametres
use ACP
use rand_num
implicit none
integer :: i,j
character(len=40) :: file1,file2,file3,file4,file5
real(kind=rdp) ,allocatable :: rdm1(:),rdm2(:)
! nom des fichiers
file1='ACP_center_R.txt'
file2='ACP_scale_R.txt'
file3='ACP_comp_R.txt'
file4='ACP_comp_max_R.txt'
file5='ACP_comp_min_R.txt'
! reset des variable acp en cas de repetition ou de nouvelles acp
call reset_ACP()


! ecriture des variables avec les fichiers
open(14,file='indice_R.txt')
read (14,*) indice
allocate (scales(nc),center(nc))
allocate (comp(nc,indice),comp_max(indice),comp_min(indice))
allocate (rdm1(indice),rdm2(indice))
open(11,file=file1)
read(11,*) (center(i),i=1,nc)

open(12,file=file2)
read(12,*) (scales(i),i=1,nc)
write(*,*) nc,indice
open(13,file=file3)
do j=1,indice
read(13,*) (comp(i,j),i=1,nc)
end do
open(15,file=file4)
read(15,*) (comp_max(i),i=1,indice)

open(16,file=file5)
read (16,*) (comp_min(i),i=1,indice)

close (16)
close (15)
close(14)
close(11)
close(12)
close(13)
allocate (norme2(indice))
norme2=0.0_rdp
do i=1,nc 
    do j=1,indice
    norme2(j)=norme2(j)+comp(i,j)**2.0_rdp
       end do
    end do

! initialisation d un vecteur aleatoire au cas ou
allocate (old_vect(2*indice),new_vect(2*indice),opt_vect(2*indice))


do i=1,indice
rdm1(i)=random(comp_min(i),comp_max(i))*1.1_rdp
rdm2(i)=random(comp_min(i),comp_max(i))*1.1_rdp
end do

old_vect(1:indice)=rdm1
old_vect(indice+1:2*indice)=rdm2
new_vect=old_vect
ifbegin=.false.
end subroutine init_ACP 

! routine pour convertir un element de l espace des composantes principales dans l espace initial
subroutine convert_ACP2(w_vrai,w_acp)
use i_Parametres
use ACP
use i_Consigne
implicit none
real(kind=rdp) w_vrai(np),w_acp(indice)
integer::i
w_vrai=0._rdp

! creation de la composante reelle centree et reduite
do i=1,indice
w_vrai(1:nc)=w_vrai(1:nc)+w_acp(i)*comp(:,i)
end do

! on recentre et remet a l echelle les composantes
do i=1,nc
w_vrai(i)=w_vrai(i)*scales(i)+center(i)
end do

! assure le respect des bornes du probleme
do i=1,nc

if (w_vrai(i)>wmax(i)) w_vrai(i)=wmax(i)
if (w_vrai(i)<wmin(i)) w_vrai(i)=wmin(i)

end do
end subroutine convert_ACP2

! routine pour convertir un element de l espace des composantes principales dans l espace initial
subroutine convert_ACP(w_vrai,w_acp)
use i_Parametres
use ACP
use i_Consigne
implicit none
real(kind=rdp) w_vrai(np),w_acp(indice)
integer::i
w_vrai=0._rdp

! creation de la composante reelle centree et reduite
do i=1,indice
w_vrai(1:nc)=w_vrai(1:nc)+w_acp(i)*comp(:,i)
end do

! on recentre et remet a l echelle les composantes
do i=1,nc
w_vrai(i)=w_vrai(i)*scales(i)+center(i)
end do

! assure le respect des bornes du probleme
do i=1,nc

if (w_vrai(i)>wmax(i)) then
w_vrai(1)=0.0_rdp
exit
end if
if (w_vrai(i)<wmin(i)) then
w_vrai(1)=0.0_rdp
exit
end if
end do
end subroutine convert_ACP


! fonction de voisinage pour le recuit simule mais sur les composantes principales trouvees
subroutine modify_ACP_SA(w_new)
use i_Parametres
use i_Annealing
use ACP
implicit none
integer::i
logical:: test
real(kind=rdp) rdm(indice),ampli(indice),w_new(np,2),comp_moy
ampli=(comp_max-comp_min)*0.25_rdp*temperature
test=.true.
! test si le recuit avec acp a deja commence
if(.not. (ifbegin)) then
! si c est la premiere fois pas d aleatoire on commence par new_vect aleatoire
do while (test)
do i=1,indice
new_vect(i)=random(comp_min(i),comp_max(i))
new_vect(indice+i)=random(comp_min(i),comp_max(i))
end do

! on converti les composantes principales dans l espace reel
call convert_ACP(w_new(1:np,1),new_vect(1:indice))
call convert_ACP(w_new(1:np,2),new_vect(indice+1:2*indice))
if (w_new(1,1)>0.0_rdp .and.  w_new(1,2)>0.0_rdp) test=.false.
end do
ifbegin=.true.
else
! sinon on tire aleatoirement dans une boule de rayon defini par ampli sur les composantes principales
do while (test)
do i=1,indice
!comp_moy=(comp_min(i)+comp_max(i))/2.0_rdp
!if(mod(nb_eval,2)==0) then
new_vect(i)=old_vect(i)+random(-ampli(i),ampli(i))
!new_vect(i)=min(1.5_rdp*comp_max(i)-comp_moy,new_vect(i))
!new_vect(i)=max(-comp_moy+1.5_rdp*comp_min(i),new_vect(i))
!new_vect(i+indice)=old_vect(i+indice)
!else
new_vect(indice+i)=old_vect(indice+i)+random(-ampli(i),ampli(i))
!new_vect(i+indice)=min(2*comp_max(i)-comp_moy,new_vect(i+indice))
!new_vect(i+indice)=max(-comp_moy+2*comp_min(i),new_vect(i+indice))
!new_vect(i)=old_vect(i)
!end if
end do

! on converti les composantes principales dans l espace reel
call convert_ACP(w_new(1:np,1),new_vect(1:indice))
call convert_ACP(w_new(1:np,2),new_vect(indice+1:2*indice))
if (w_new(1,1)>0.0_rdp .and.  w_new(1,2)>0.0_rdp) test=.false.
end do
end if

end subroutine modify_ACP_SA


! Routine pour projeter des parametres de l espace reel dans les composantes principales
subroutine projection_ACP(w_opt)
use i_Parametres
use ACP
implicit none
real(kind=rdp) :: w_opt(np,2),dummy(np,2)
integer::i,j
new_vect=0._rdp
dummy=w_opt

! centrage et reduction des donnees
do i=1,nc
dummy(i,1)=(dummy(i,1)-center(i))/scales(i)
dummy(i,2)=(dummy(i,2)-center(i))/scales(i)
end do
! application du produit scalaire composantes par composantes
do i=1,indice
    do j=1,nc
     new_vect(i)=new_vect(i)+comp(j,i)*dummy(j,1)
     new_vect(indice+i)=new_vect(indice+i)+comp(j,i)*dummy(j,2)
    end do
    new_vect(i)=new_vect(i)/norme2(i)
     new_vect(indice+i)=new_vect(indice+i)/norme2(i)
end do 
end subroutine projection_ACP


! fonction de voisinage du recuit sur les composantes principales mais en tirant juste aleatoirement
subroutine modify_ACP_random(w_new)
use i_Parametres
implicit none
integer :: i,j,indice
real(kind=rdp) :: w_new(np,2),w_1(nc),w_2(nc)
character(len=40) :: file1,file2,file3,file4,file5
real(kind=rdp) :: center(nc),scale(nc),w_min,w_max
real(kind=rdp) ,allocatable :: rdm1(:),rdm2(:),comp(:,:),comp_max(:),comp_min(:)
!lecture et definition des fichiers
file1='ACP_center_R.txt'
file2='ACP_scale_R.txt'
file3='ACP_comp_R.txt'
file4='ACP_comp_max_R.txt'
file5='ACP_comp_min_R.txt'
open(14,file='indice_R.txt')
read (14,*) indice
allocate (comp(nc,indice),comp_max(indice),comp_min(indice))
allocate (rdm1(indice),rdm2(indice))
open(11,file=file1)
read(11,*) (center(i),i=1,nc)

open(12,file=file2)
read(12,*) (scale(i),i=1,nc)

open(13,file=file3)
do j=1,indice
read(13,*) (comp(i,j),i=1,nc)
end do

open(15,file=file4)
read(15,*) (comp_max(i),i=1,indice)

open(16,file=file5)
read (16,*) (comp_min(i),i=1,indice)

close (16)
close (15)
close(14)
close(11)
close(12)
close(13)

! tirage aleatoire des composantes principales
w_min=-10._rdp
w_max=10._rdp
do i=1,indice
if(i==1) then
rdm1(i)=random(comp_min(i),comp_max(i))*1.1_rdp
rdm2(i)=random(comp_min(i),comp_max(i))*1.1_rdp
else
rdm1(i)=random(comp_min(i),comp_max(i))*1.1_rdp
rdm2(i)=random(comp_min(i),comp_max(i))*1.1_rdp    
end if
end do
w_1=0._rdp
w_2=0_rdp

do i=1,indice
w_1=w_1+rdm1(i)*comp(:,i)
w_2=w_2+rdm2(i)*comp(:,i)
end do


! on enleve centrage et reduction des donnees
do i=1,nc
w_1(i)=w_1(i)*scale(i)+center(i)
w_2(i)=w_2(i)*scale(i)+center(i)
end do

! allocation des nouveaux parametres
do i=1,nc
w_1(i)=max(w_1(i),wmin(i))
w_1(i)=min(w_1(i),wmax(i))
w_2(i)=max(w_2(i),wmin(i))
w_2(i)=min(w_2(i),wmax(i))

end do
w_new(1:nc,1)=w_1
w_new(1:nc,2)=w_2
end subroutine modify_ACP_random

