
subroutine GA_pikaia()
   use i_Parametres  !, only : rdp, np
   use i_Consigne, only : nc,n_it_max
   use Genetic_algorithm
   implicit none
   real :: ctrl(12), x(2*np), f
   integer :: status,rand_seed
   ! initialisation graine pour pikaia
   rand_seed=floor(random(1.0_rdp,100000._rdp))
   call rninit(rand_seed)
   ! parametre de controle pour pikaia
   
   ctrl = -1  
   ctrl(1) = 128  !population
   ctrl(2) = n_it_max/(128*4)  !nombre de generation
   ctrl(3) = 6  ! nombre de chiffre significatifs
   ctrl(10)=2 ! 1/2/3 Full generational replacement/
   !Steady-state-replace-random/Steady-state-replace-worst 
   ctrl(11)=1 !0/1 elitisme autorise (il faut ctrl(10) a 1 ou 2) 
   ctrl(12) = 2 ! affichage 0/1/2 sans/normal/verbose
   call pikaia(fff,2*nc,ctrl,x,f,STATUS)
   
end subroutine GA_pikaia

! fonction ecart qui correspond aux contraintes de pikaia
function fff(n,w) result(fn_val)

   use i_Parametres, only : rdp, np
   use i_Consigne, only : nc, wmax, wmin
   implicit none
   integer, intent(in)  :: n ! n doit etre pair
   real, intent(in)     :: w(:)
   real                 :: fn_val
   real (kind=rdp) :: ww(np,2), err_cal
   integer, save :: nap = 0
   ww=0.0_rdp
   nap = nap+1
   ww(1:nc,1) = w(1:nc)*(wmax(1:nc)-wmin(1:nc))+wmin(1:nc)
   ww(1:nc,2) = w(nc+1:2*nc)*(wmax(1:nc)-wmin(1:nc))+wmin(1:nc)
   !if (xlag(ww) < 0.0001_rdp) then
      fn_val = Foo_Ecart_amel(ww,err_cal)
   !else
   !   fn_val = -10.**30_rdp
   !endif
   write(*,*) ' Evaluation #',nap, ' pour pikaia : ',fn_val,' err_cal = ',err_cal
   write(8,*) nap,nb_simul, fn_val, err_cal
       write(7,*) nap,nb_simul, ww(1:nst,1), ww(1:nst,2), fn_val
end function fff
