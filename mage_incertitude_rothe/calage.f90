function calage_ok(W,tag)
!==============================================================================
!             Vérification du calage
!
!---Historique
!      08/06/2009 : création
!==============================================================================
   use i_Parametres, only : rdp
   use i_Consigne, only : erreur_calage

   implicit none
!prototype
   real(kind=rdp), intent(in) :: w(:)
   character(len=1), intent(in) :: tag
   real(kind=rdp) :: calage_ok
   
   type (Mage_results) :: resultats

   !print*,'Entrée calage_ok n°'//tag
   calage_ok = max(0._rdp , calage(w,tag,resultats)-erreur_calage)
end function calage_ok



function calage(W,tag,resultats)
!==============================================================================
!             Vérification du calage
!
!     Cette fonction évalue l'écart entre le résultat d'une simulation et les
!     données observées.
!     Les données de calage étant a priori différentes de celles concernées par
!     l'évaluation des incertitudes, calage() travaille dans un sous-répertoire
!     du répertoire courant.
!
!---Historique
!      17/07/2008 : création de calage_ok
!      26/05/2009 : modification pour renvoyer la valeur de l'erreur
!==============================================================================
   use i_Parametres, only : rdp, np, maxsim, issup
   use i_Consigne, only : nc, basename, ib, xmin, xmax, nst,deltaZ, &
                          laisse_crue, nb_obs_z, nb_obs_q, Q_rep, cz, cq, &
                          datageo, profil, nbpro
   use i_Annealing, only : FOtyp
   implicit none
!prototype
   real(kind=rdp), intent(in) :: w(:)
   character(len=1), intent(in) :: tag
   type (Mage_results), intent(out) :: resultats
   real(kind=rdp) :: calage
!variables locales
   integer :: i, lu, is, nbv
   integer :: lb
   character ::  REPname*40
   integer :: n
   real(kind=rdp) :: erreurZ, erreurQ
   type (profil) :: datamod(1000)

!--------------------------------------------------------------------------
   !print*,'Entrée calage n°'//tag
   if (maxval(w(1:nc)) < -9999._rdp) then
      calage = -9999._rdp
      return
   endif
   !print*,'1__calage n°'//tag
   lb = len_trim(basename)
   !--->exportation de la consigne
   open(newunit=lu,file=tag//'/'//basename(1:lb)//'.RUG',form='formatted',status='unknown')
   do i=1, nst/2
      write(lu,'(a,i3,6x,2f10.3,2f10.6)') 'K',ib(i),xmin(i),xmax(i),w(2*i-1),w(2*i)
   enddo
   close(lu)
   !print*,'2__calage n°'//tag
   if (deltaZ > 0._rdp) then
      do n = 1, nbpro
         nbv = datageo(n)%nbval
         datamod(n)%nbval = nbv
         datamod(n)%x = datageo(n)%x
         datamod(n)%zmoy = datageo(n)%zmoy
         datamod(n)%y(1:nbv) = datageo(n)%y(1:nbv)
         datamod(n)%z(1:nbv) = datageo(n)%z(1:nbv)+w(nst+n) ! on ne met à jour que les Z !!!
      enddo
      open(newunit=lu,file=tag//'/'//basename(1:lb)//'.TAL',form='formatted',status='unknown')
      call exportTAL(lu,datamod)
      close (lu)
   endif
!--->exécution de MAGE
   !print*,'3__calage n°'//tag
   REPname = basename(1:lb)//'.REP'
   !print*,'4__calage n°'//tag, REPname
   call solveur_externe(REPname,tag,resultats)
!--->vérification que la simulation n'a pas échoué
   !print*,'5__calage n°'//tag, REPname
   if (resultats%calcul_OK) then  !--->extraction des données de sortie et comparaison
      if (FOtyp < 6.or. FOtyp > 7) then  !vérification du calage : écart maximal
         erreurZ = -1.e50_rdp  ;  erreurQ = -1.e50_rdp
         do n = 1, nb_obs_z
            is = isect(laisse_crue(n)%ib, laisse_crue(n)%x, 0.01_rdp)
            erreurZ = max(erreurZ,abs(resultats%Z_max(is)-laisse_crue(n)%z))
         enddo
      else  if (FOtyp == 6) then !calage automatique sur les maxima 
                                 !moyenne quadratique des écarts entre valeurs calculées et valeurs observées
         erreurZ = 0._rdp 
         do n = 1, nb_obs_z
            is = isect(laisse_crue(n)%ib, laisse_crue(n)%x, 0.01_rdp)
            erreurZ = erreurZ + (resultats%Z_max(is)-laisse_crue(n)%z)**2
         enddo
         erreurZ = sqrt(erreurZ / real(nb_obs_z))
         erreurQ = 0._rdp
         if (nb_obs_q > 0) then
            do n = 1, nb_obs_q
               is = isect(Q_rep(n)%ib, Q_rep(n)%x, 0.01_rdp)
               erreurQ = erreurQ + (resultats%Qtot(is) - Q_rep(n)%z)**2
            enddo
            erreurQ = sqrt(erreurQ/real(nb_obs_q))
         endif
      else if (FOtyp == 7) then !calage automatique sur les valeurs de l'état final
         if (resultats%type_resultat /= 'FIN' .and. resultats%type_resultat /= 'END') then
            write(*,*) 'Type de resultat erroné : ',resultats%type_resultat
            stop 444
         endif
         erreurZ = 0._rdp 
         do n = 1, nb_obs_z
            is = isect(laisse_crue(n)%ib, laisse_crue(n)%x, 0.01_rdp)
            erreurZ = erreurZ + (resultats%Z_max(is)-laisse_crue(n)%z)**2
         enddo
         erreurZ = sqrt(erreurZ / real(nb_obs_z))
         ! Dans ce cas on cale sur la répartition des débits entre lit mineur et lit majeur
         ! Part de débit qui passe dans la plaine d'inondation exprimée en pourcentage du débit total
         ! Pour un calage sur l'état final en permanent, il n'y a pas d'autre choix pertinent pour le calage sur les débits
         erreurQ = 0._rdp
         if (nb_obs_q > 0) then
            do n = 1, nb_obs_q
               is = isect(Q_rep(n)%ib, Q_rep(n)%x, 0.01_rdp)
               erreurQ = erreurQ + (100._rdp*resultats%Qfp(is)/resultats%Qtot(is) - Q_rep(n)%z)**2
            enddo
            erreurQ = sqrt(erreurQ/real(nb_obs_q))
         endif
      endif
      calage = cz*abs(erreurZ) + cq*abs(erreurQ)
   else
      !échec de la simulation => foo = infinity
      calage = 1.e30_rdp
   endif
end function calage


function iSect(IB,X,Y)  !numéro de la section a abs x du bief numero ib
!==============================================================================
!   Cette fonction retourne le numero absolu de la section situee a
!       l'abscisse X du bief de numero IB dans .TAL
!   L'argument optionnel Y permet de définir la tolérance de correspondance
!   des abscisses. 
!   Si Y est absent on cherche une correspondance à 10**-5 près (en relatif)
!
!Historique des modifications
! 05/02/2007 : documentation
!==============================================================================
   use i_Parametres, only: rdp
   use dim_reelles, only: ibmax
   use geometrie_section
   implicit none
! -- le prototype --
   integer :: isect
   integer,intent(in) :: ib
   real(kind=rdp),intent(in) :: x
   real(kind=rdp),intent(in), optional :: y
! -- les variables --
   integer :: is, kb
!------------------------------------------------------------------------------
   if (ib>ibmax .or. ib<1) then
      write(6,'(a,i3,a)') ' >>>> ERREUR (fonction ISECT) : le bief ',ib, &
                          ' est inconnu sur ce réseau <<<<'
      stop 192
   endif
   kb=ibu(ib)
   if ( (x-xgeo(is1(kb)))*(x-xgeo(is2(kb)))  > 0._rdp) then
      continue                     !on sort si x n'est pas dans le bief ib
    else if (present(y)) then  !on cherche la section la plus proche
      isect = is1(kb)
      do is = is1(kb)+1, is2(kb)
         if ( abs(x-xgeo(is)) < abs(x-xgeo(isect)) ) isect = is
      enddo
      if (abs(xgeo(isect)-x) < y) return  !on accepte la section la plus proche si elle l'est assez
    else                 !on cherche une correspondance exacte
      do is=is1(kb),is2(kb)
         if ( abs(x-xgeo(is)) < 1._rdp) then
            isect=is
            return
         endif
      enddo
   endif
   write(0,'(a,f8.2,a,i3,a)') ' >>>> ERREUR (fonction ISECT)  : il n''y a pas de section a l''abscisse ', &
                              x,' dans le bief ',ib,' <<<<'
   stop 193
end function ISect
