! fonction qui calcule l ecart entre les deux surface inondees simulees avec les parametres de w
! Stockage des valeurs optimales dans calage_error
function Foo_Ecart_amel(w, C_Plus) 


   use i_Parametres, only : rdp, np, issup
   use i_Annealing, only : fk_opt, fk_ini, sens, Surf1, Surf2
   use i_Consigne, only : w_ini, wmax, wmin, Coeff_Penalisation
   use dim_reelles, only : ismax, ibmax
   use geometrie_section, only : is1, is2, xgeo
   use calage_error
   implicit none
!prototype
   real(kind=rdp), intent(in) :: w(np,2)
   real(kind=rdp), intent(out) :: C_Plus
   real(kind=rdp) :: Foo_Ecart_amel
!variables locales
   real(kind=rdp) :: funk, tmp
   integer :: i, j, k, ib
   real(kind=rdp) :: ww(np), foo1, foo2, ecart, long_net, h1, h2, hmax, hmin
   integer, save :: nap=0
   real(kind=rdp), save :: foo_ini
   character(len=2) :: OK
   character(len=1) :: tag
   type (Mage_results) :: resultats_1, resultats_2
!----------------------------------------------------------------------
!simulation initiale avec la moyenne
   if (nap == 0) then
      write(*,*) 'Simulation initiale avec les consignes moyennes'
      write(8,*) 'Simulation initiale avec les consignes moyennes'
      nap = 1
      ww = (wmax + wmin)/2._rdp
      foo_ini = working_Area(ww,'2',resultats)
      OK = 'KO'  ;  if (calage_ok(ww,'0') < 0.0001_rdp) OK = 'OK'
      write(*,'(a,f10.4,2a)') ' --> Surface initiale : ',foo_ini*0.0001,' ha, Calage : ',OK
      write(8,'(a,f10.4,2a)') ' --> Surface initiale : ',foo_ini*0.0001,' ha, Calage : ',OK
      surf_mini=foo_ini
      surf_maxi=foo_ini
      surf_ini=foo_ini
   endif
   
! PARALLELISATION pour les simulations de calage
   !$omp parallel 
   !$omp single private(tag,ww)
      !$omp task
         tag = '0' !; print*,' Appel calage_ok tag=',tag
         ww(:) = w(:,1) ; foo1 = calage_ok(ww,tag)
         err_cal1=foo1
      !$omp end task
      !$omp task
         tag = '1' !; print*,' Appel calage_ok tag=',tag
         ww(:) = w(:,2) ; foo2 = calage_ok(ww,tag)
         err_cal2=foo2
      !$omp end task
   !$omp end single
   !$omp end parallel
   C_Plus = distance_Domaine(w) + foo1 + foo2
   foo1 = 1.e+79_rdp ; foo2 = foo1
   
   if (Coeff_Penalisation*C_Plus < 1._rdp) then
   ! parallelisation pour les simulations sur l hydrogramme souhaite
      !$omp parallel private(tag,ww,resultats)
      !$omp single
         !$omp task
            tag = '2' !; print*,' Appel working_Area() tag=',tag
            ww = w(:,1) ; foo1 = working_Area(ww,tag,resultats)  ;  Surf1 = foo1*0.0001_rdp
         !$omp end task
         !$omp task
            tag = '3' !; print*,' Appel working_Area() tag=',tag
            ww = w(:,2) ; foo2 = working_Area(ww,tag,resultats)  ;  Surf2 = foo2*0.0001_rdp
         !$omp end task
      !$omp end single
      !$omp end parallel
                    
      if (abs(foo1) > 1.e+29_rdp .OR. abs(foo2) > 1.e+29_rdp) then  
         funk = sens * 9.999e+99_rdp          !échec d'au moins une des simulations
           ecart_courant=sens * 9.999e+99_rdp   
           
      else
         !ec1=min(foo1-surf_mini,foo2-surf_mini)/surf_mini*100._rdp
         !ec2=min(surf_maxi-foo1,surf_maxi-foo2)/surf_maxi*100._rdp
         !verification et changement si l une des surfaces devient un extremum sur toutes les simulations deja realisees
         if(foo1>surf_maxi)  then 
         surf_maxi=foo1;ind_max=1;param_maxi=w(:,1)
         end if
         if(foo2 >surf_maxi)  then
                  surf_maxi=foo2;ind_max=2;param_maxi=w(:,2)
                  end if
          if(foo1<surf_mini)  then 
          surf_mini=foo1;ind_min=1;param_mini=w(:,1)
          end if 
         if(foo2 <surf_mini)  then 
         surf_mini=foo2;ind_min=2;param_mini=w(:,2)
         end if
         ! calcul de l ecart
         ecart = abs(foo1-foo2)/(foo_ini)*100._rdp
         funk =(abs(foo1-foo2)/surf_ini*100._rdp) 
         ecart_courant=ecart + sens*Coeff_Penalisation*C_Plus
      endif
   else  !inutile de faire les simulations car les contraintes ne sont pas satisfaites
      !print*,'Contraintes non satisfaites'
      funk = sens * Coeff_Penalisation*C_Plus
        ecart_courant=sens * Coeff_Penalisation*C_Plus
      Surf1 = -1._rdp  ;  Surf2 = -1._rdp  ! valeurs arbitraires pour les distinguer dans le fichier csv
   endif
   Foo_Ecart_amel = funk
!------------------------------------------------------------------------------
end function Foo_Ecart_amel

! calcule l ecart entre les deux surfaces inondees sans verifier le calage
function Foo_Ecart_no_cal(w, C_Plus) ! FOtyp = 0,1,2,3
!==============================================================================
!       fonction à minimiser utilisée par SIMULATED_ANNEALING
!                utilise flooded_Area et ajoute les contraintes
!       cette fonction calcule un écart entre 2 simulations
!       plusieurs mesures de cet écart sont disponibles :
!       - différence relative des surfaces inondées (type 0)
!       - écart moyen entre les lignes d'eau enveloppe (type 1)
!       - écart maximal entre les lignes d'eau enveloppe (type 2)
!       - écart moyen entre les largeurs du lit majeur inondé (type 3)
!
!       La valeur de référence correspond toujours à la consigne médiane
!
!       Entrée :
!               W = consigne (vecteur)
!       Sortie :
!               Foo_Ecart : valeur de l'écart
!               C_Plus = poids des contraintes ; o si satisfaites
!
!---Historique
!      04/03/2004 : reprise depuis PEGASE (1994)
!      08/06/2009 : mise à jour de la description
!==============================================================================
   use i_Parametres, only : rdp, np, issup
   use i_Annealing, only : fk_opt, fk_ini, sens, Surf1, Surf2
   use i_Consigne, only : w_ini, wmax, wmin, Coeff_Penalisation
   use dim_reelles, only : ismax, ibmax
   use geometrie_section, only : is1, is2, xgeo
   implicit none
!prototype
   real(kind=rdp), intent(in) :: w(np,2)
   real(kind=rdp), intent(out) :: C_Plus
   real(kind=rdp) :: Foo_Ecart_no_cal
!variables locales
   real(kind=rdp) :: funk, tmp
   integer :: i, j, k, ib
   real(kind=rdp) :: ww(np), foo1, foo2, ecart, long_net, h1, h2, hmax, hmin
   integer, save :: nap=0
   real(kind=rdp), save :: foo_ini
   character(len=2) :: OK
   character(len=1) :: tag
   type (Mage_results) :: resultats_1, resultats_2
!----------------------------------------------------------------------
!simulation initiale avec la moyenne
   if (nap == 0) then
      write(*,*) 'Simulation initiale avec les consignes moyennes'
      write(8,*) 'Simulation initiale avec les consignes moyennes'
      nap = 1
      ww = (wmax + wmin)/2._rdp
      foo_ini = working_Area(ww,'2',resultats)
      OK = 'KO'  ;  if (calage_ok(ww,'0') < 0.0001_rdp) OK = 'OK'
      write(*,'(a,f10.4,2a)') ' --> Surface initiale : ',foo_ini*0.0001,' ha, Calage : ',OK
      write(8,'(a,f10.4,2a)') ' --> Surface initiale : ',foo_ini*0.0001,' ha, Calage : ',OK
   endif
   C_Plus =0._rdp
   foo1 = 1.e+79_rdp ; foo2 = foo1
   
      !$omp parallel private(tag,ww,resultats)
      !$omp single
         !$omp task
            tag = '2' !; print*,' Appel working_Area() tag=',tag
            ww = w(:,1) ; foo1 = working_Area(ww,tag,resultats)  ;  Surf1 = foo1*0.0001_rdp
         !$omp end task
         !$omp task
            tag = '3' !; print*,' Appel working_Area() tag=',tag
            ww = w(:,2) ; foo2 = working_Area(ww,tag,resultats)  ;  Surf2 = foo2*0.0001_rdp
         !$omp end task
      !$omp end single
      !$omp end parallel
                    
      !write(6,*) 'Surfaces : ',Surf1, Surf2
      if (abs(foo1) > 1.e+29_rdp .OR. abs(foo2) > 1.e+29_rdp) then  
         funk = sens * 9.999e+99_rdp          !échec d'au moins une des simulations
      else
         ecart = max(abs((foo1-foo2))+foo1-foo2,abs((foo2-foo1))+foo2-foo1 )/(2._rdp*foo_ini)*100._rdp
         funk = ecart + sens*Coeff_Penalisation*C_Plus
      endif
   Foo_Ecart_no_cal = funk
!------------------------------------------------------------------------------
end function Foo_Ecart_no_cal


! calcule la surface inondee et verifie le calage pour un jeu de parametres w
function Foo_Surface(w, C_Plus) 
!==============================================================================
!       Fonction qui calcule directement la surface inondée 
!       pour un input donné avec le poids des contraintes de calage
!
!       Entrée :
!               W = consigne (vecteur)
!       Sortie :
!               Foo_Ecart : valeur de l'écart
!               C_Plus = poids des contraintes ; o si satisfaites
!==============================================================================
   use i_Parametres, only : rdp, np, issup
   use i_Annealing, only : fk_opt, fk_ini, sens, Surf1, Surf2
   use i_Consigne, only : w_ini, wmax, wmin, Coeff_Penalisation
   use dim_reelles, only : ismax, ibmax
   use geometrie_section, only : is1, is2, xgeo
   implicit none
!prototype
   real(kind=rdp), intent(in) :: w(np)
   real(kind=rdp), intent(out) :: C_Plus
   real(kind=rdp) :: Foo_Surface
!variables locales
   real(kind=rdp) :: funk, tmp
   integer :: i, j, k, ib
   real(kind=rdp) :: ww(np), foo1, long_net, h1, h2, hmax, hmin
   integer, save :: nap=0
   real(kind=rdp), save :: foo_ini
   character(len=2) :: OK
   character(len=1) :: tag
   type (Mage_results) :: resultats_1, resultats_2
!-----------------------------------------------------------------------
!simulation initiale avec la moyenne
   if (nap == 0) then
      write(*,*) 'Simulation initiale avec les consignes moyennes'
      write(8,*) 'Simulation initiale avec les consignes moyennes'
      nap = 1
      ww = (wmax + wmin)/2._rdp
      foo_ini = working_Area(ww,'2',resultats)
      OK = 'KO'  ;  if (calage_ok(ww,'0') < 0.0001_rdp) OK = 'OK'
      write(*,'(a,f10.4,2a)') ' --> Surface initiale : ',foo_ini*0.0001,' ha, Calage : ',OK
      write(8,'(a,f10.4,2a)') ' --> Surface initiale : ',foo_ini*0.0001,' ha, Calage : ',OK
   endif

         tag = '0' !; print*,' Appel calage_ok tag=',tag
         ww(:) = w(:) ; foo1 = calage_ok(ww,tag)

         C_Plus = distance_Domaine_simple(w) + foo1 + foo2
         foo1 = 1.e+79_rdp ; 
   
   if (Coeff_Penalisation*C_Plus < 1._rdp) then
            tag = '2' !; print*,' Appel working_Area() tag=',tag
           foo1 = working_Area(w,tag,resultats)  ;  Surf1 = foo1*0.0001_rdp


      !write(6,*) 'Surfaces : ',Surf1, Surf2
      if (abs(foo1) > 1.e+29_rdp) then  
         funk = sens * 9.999e+99_rdp          !échec d'au moins une des simulations
      else
         funk = foo1 + sens*Coeff_Penalisation*C_Plus
      endif
   else  !inutile de faire les simulations car les contraintes ne sont pas satisfaites
      !print*,'Contraintes non satisfaites'
      funk = sens * Coeff_Penalisation*C_Plus
      Surf1 = -1._rdp  ;  Surf2 = -1._rdp  ! valeurs arbitraires pour les distinguer dans le fichier csv
   endif
   Foo_Surface = funk
!------------------------------------------------------------------------------
end function Foo_Surface


! calcule la surface inondee pour un  jeu de parametres w sans verifier le calage
function Foo_Surface_no_cal(w, C_Plus) 
!==============================================================================
!       Fonction qui calcule directement la surface inondée 
!       pour un input donné avec le poids des contraintes de calage
!
!       Entrée :
!               W = consigne (vecteur)
!       Sortie :
!               Foo_Ecart : valeur de l'écart
!               C_Plus = poids des contraintes ; o si satisfaites
!==============================================================================
   use i_Parametres, only : rdp, np, issup
   use i_Annealing, only : fk_opt, fk_ini, sens, Surf1, Surf2
   use i_Consigne, only : w_ini, wmax, wmin, Coeff_Penalisation
   use dim_reelles, only : ismax, ibmax
   use geometrie_section, only : is1, is2, xgeo
   implicit none
!prototype
   real(kind=rdp), intent(in) :: w(np)
   real(kind=rdp), intent(out) :: C_Plus
   real(kind=rdp) :: Foo_Surface_no_cal
!variables locales
   real(kind=rdp) :: funk, tmp
   integer :: i, j, k, ib
   real(kind=rdp) :: ww(np), foo1, long_net, h1, h2, hmax, hmin
   integer, save :: nap=0
   real(kind=rdp), save :: foo_ini
   character(len=2) :: OK
   character(len=1) :: tag
   type (Mage_results) :: resultats_1, resultats_2
!-----------------------------------------------------------------------
!simulation initiale avec la moyenne
   if (nap == 0) then
      write(*,*) 'Simulation initiale avec les consignes moyennes'
      write(8,*) 'Simulation initiale avec les consignes moyennes'
      nap = 1
      ww = (wmax + wmin)/2._rdp
      foo_ini = working_Area(ww,'2',resultats)
      OK = 'KO'  ;  if (calage_ok(ww,'0') < 0.0001_rdp) OK = 'OK'
      write(*,'(a,f10.4,2a)') ' --> Surface initiale : ',foo_ini*0.0001,' ha, Calage : ',OK
      write(8,'(a,f10.4,2a)') ' --> Surface initiale : ',foo_ini*0.0001,' ha, Calage : ',OK
   endif


         C_Plus = 0._rdp
         foo1 = 1.e+79_rdp ; 
   
            tag = '2' !; print*,' Appel working_Area() tag=',tag
           foo1 = working_Area(w,tag,resultats)  ;  Surf1 = foo1*0.0001_rdp


      !write(6,*) 'Surfaces : ',Surf1, Surf2
      if (abs(foo1) > 1.e+29_rdp) then  
         funk = sens * 9.999e+99_rdp          !échec d'au moins une des simulations
      else
         funk = foo1 + sens*Coeff_Penalisation*C_Plus
      endif
  
   Foo_Surface_no_cal = funk
!------------------------------------------------------------------------------
end function Foo_Surface_no_cal

function flooded_Area(w,tag,resultats)
!==============================================================================
!             FONCTION OBJECTIF A MINIMISER
!
!---Historique
!      10/03/2004 : création
!==============================================================================
   use i_Parametres, only : rdp, np, maxsim, issup
   use i_Consigne, only : nc, modele, basename, ib, xmin, xmax, w_ini, nst, deltaZ, &
                          datageo, nbpro, profil
   use dim_reelles, only : ismax, ibmax
   use geometrie_section, only : is1, is2
   implicit none
!prototype
   real(kind=rdp), intent(in) :: w(*)
   real(kind=rdp) :: flooded_Area
   type (Mage_results), intent(out) :: resultats
   character(len=1) :: tag
!variables locales
   integer :: i, lu, is, kb, nbv
   integer :: lb
   character :: fmt*40, REPname*40
   real(kind=rdp) :: surface, surface_mineur, Lmaj1, Lmaj2
   type (profil) :: datamod(1000)
   
!--------------------------------------------------------------------------
   lb = len_trim(basename)
   flooded_Area = 1.e+40_rdp

   !--->exportation de la consigne
   open(newunit=lu,file=tag//'/'//basename(1:lb)//'.RUG',form='formatted',status='unknown')
   do i=1, nst/2
      write(lu,'(a,i3,6x,2f10.3,2f10.6)') 'K',ib(i),xmin(i),xmax(i),w(2*i-1),w(2*i)
   enddo
   close (lu)
   if (deltaZ > 0._rdp) then
      do n = 1, nbpro
         nbv = datageo(n)%nbval
         datamod(n)%nbval = nbv
         datamod(n)%x = datageo(n)%x
         datamod(n)%zmoy = datageo(n)%zmoy
         datamod(n)%y(1:nbv) = datageo(n)%y(1:nbv)
         datamod(n)%z(1:nbv) = datageo(n)%z(1:nbv)+w(nst+n) ! on ne met à jour que les Z !!!
      enddo
      open(newunit=lu,file=tag//'/'//basename(1:lb)//'.TAL',form='formatted',status='unknown')
      call exportTAL(lu,datamod)
      close (lu)
   endif
!--->exécution de MAGE
   REPname = basename(1:lb)//'.REP'
   call solveur_externe(REPname,tag,resultats)
!--->vérification que la simulation n'a pas échoué
   if (resultats%calcul_OK) then  !--->extraction des données de sortie et comparaison
      surface = 0._rdp
      do kb = 1, ibmax
         Lmaj1 = max(0._rdp,resultats%largeur_totale(is1(kb))-resultats%largeur_mineur(is1(kb)))
         do is = is1(kb)+1, is2(kb)
            Lmaj2 = max(0._rdp,resultats%largeur_totale(is)-resultats%largeur_mineur(is))
            surface = surface + 0.5_rdp*abs(resultats%pm(is)-resultats%pm(is-1))*(Lmaj2 + Lmaj1)
            Lmaj1 = Lmaj2
         enddo
      enddo

      
      flooded_Area = surface  !surface en m²

   else
      !échec de la simulation => foo = infinity
      flooded_Area = 1.e+30_rdp
   endif

end function flooded_Area



function vertical_Area(w,tag,resultats)
!==============================================================================
!             FONCTION OBJECTIF A MINIMISER
!
!---Historique
!      10/03/2004 : création
!==============================================================================
   use i_Parametres, only : rdp, np, maxsim, issup
   use i_Consigne, only : nc, modele, basename, ib, xmin, xmax, w_ini, nst, deltaZ, &
                          datageo, nbpro, profil
   use dim_reelles, only : ismax, ibmax
   use geometrie_section, only : is1, is2
   implicit none
!prototype
   real(kind=rdp), intent(in) :: w(*)
   real(kind=rdp) :: vertical_Area
   type (Mage_results), intent(out) :: resultats
   character(len=1) :: tag
!variables locales
   integer :: i, lu, is, kb, nbv
   integer :: lb
   character :: fmt*40, REPname*40
   real(kind=rdp) :: surface, surface_mineur, Lmaj1, Lmaj2
   type (profil) :: datamod(1000)
   
!--------------------------------------------------------------------------
   lb = len_trim(basename)
   vertical_Area = 1.e+40_rdp

   !--->exportation de la consigne
   open(newunit=lu,file=tag//'/'//basename(1:lb)//'.RUG',form='formatted',status='unknown')
   do i=1, nst/2
      write(lu,'(a,i3,6x,2f10.3,2f10.6)') 'K',ib(i),xmin(i),xmax(i),w(2*i-1),w(2*i)
   enddo
   close (lu)
   if (deltaZ > 0._rdp) then
      do n = 1, nbpro
         nbv = datageo(n)%nbval
         datamod(n)%nbval = nbv
         datamod(n)%x = datageo(n)%x
         datamod(n)%zmoy = datageo(n)%zmoy
         datamod(n)%y(1:nbv) = datageo(n)%y(1:nbv)
         datamod(n)%z(1:nbv) = datageo(n)%z(1:nbv)+w(nst+n) ! on ne met à jour que les Z !!!
      enddo
      open(newunit=lu,file=tag//'/'//basename(1:lb)//'.TAL',form='formatted',status='unknown')
      call exportTAL(lu,datamod)
      close (lu)
   endif
!--->exécution de MAGE
   REPname = basename(1:lb)//'.REP'
   call solveur_externe(REPname,tag,resultats)
!--->vérification que la simulation n'a pas échoué
   if (resultats%calcul_OK) then  !--->extraction des données de sortie et comparaison
      surface = 0._rdp
      do kb = 1, ibmax
         Lmaj1 = resultats%Z_max(is1(kb)) - resultats%Zfd(is1(kb))
         do is = is1(kb)+1, is2(kb)
            Lmaj2 = resultats%Z_max(is) - resultats%Zfd(is)
            surface = surface + 0.5_rdp*abs(resultats%pm(is)-resultats%pm(is-1))*(Lmaj2 + Lmaj1)
            Lmaj1 = Lmaj2
         enddo
      enddo
   
      vertical_Area = surface  !surface en m²

   else
      !échec de la simulation => foo = infinity
      vertical_Area = 1.e+30_rdp
   endif

end function vertical_Area


! test si nouvelles valeurs bien dans le domaine 
function distance_Domaine(w)
!==============================================================================
!
!==============================================================================
   use i_Parametres, only : rdp, np
   use i_Consigne, only : Wmin, Wmax, nc
   use i_annealing, only : FOtyp
   implicit none
!prototype
   real(kind=rdp), intent(in) :: W(np,2)
   real(kind=rdp)  :: distance_Domaine
!variables locales
   integer :: n, i
   real(kind=rdp)  :: DD
!interfaces
   interface
      function calage(W)
         use i_Parametres, only : rdp
         real(kind=rdp), intent(in) :: w(*)
         real(kind=rdp) :: calage
      end function calage
   end interface

!------------------------------------------------------------------------------
   DD = 0._rdp  !initialisation à 0.
   ! contraintes sur les limites du domaine
   if (FOtyp > 1) then
      do n = 1, nc
         DD = DD + max(W(n,1)-Wmax(n),0._rdp) + max(Wmin(n)-W(n,1),0._rdp)
      enddo
   else
      do i = 1, 2
         do n = 1, nc
            DD = DD + max(W(n,i)-Wmax(n),0._rdp) + max(Wmin(n)-W(n,i),0._rdp)
         enddo
      enddo
   endif
   distance_Domaine = DD
end function distance_Domaine

!meme fonction que distance_domaine mais avec en entree un seul jeu de parametres 
function distance_Domaine_simple(w)
!==============================================================================
!
!==============================================================================
   use i_Parametres, only : rdp, np
   use i_Consigne, only : Wmin, Wmax, nc
   implicit none
!prototype
   real(kind=rdp), intent(in) :: W(np)
   real(kind=rdp)  :: distance_Domaine_simple
!variables locales
   integer :: n, i
   real(kind=rdp)  :: DD
!interfaces
   interface
      function calage(W)
         use i_Parametres, only : rdp
         real(kind=rdp), intent(in) :: w(*)
         real(kind=rdp) :: calage
      end function calage
   end interface

!------------------------------------------------------------------------------
   DD = 0._rdp  !initialisation à 0.
   ! contraintes sur les limites du domaine
      do n = 1, nc
         DD = DD + max(W(n)-Wmax(n),0._rdp) + max(Wmin(n)-W(n),0._rdp)
      enddo
   distance_Domaine_simple = DD
end function distance_Domaine_simple


! meme fonction que foo surface mais permet de choisir le dossier ou executer la simulation
! a utiliser lorsque l on parallelise 
function Foo_Surface_tag(w, C_Plus,tag1,tag2) 

   use i_Parametres, only : rdp, np, issup
   use i_Annealing, only : fk_opt, fk_ini, sens, Surf1, Surf2
   use i_Consigne, only : Coeff_Penalisation
   use dim_reelles, only : ismax, ibmax
   use geometrie_section, only : is1, is2
   implicit none
!prototype
   real(kind=rdp), intent(in) :: w(np)
   real(kind=rdp), intent(out) :: C_Plus
   real(kind=rdp) :: Foo_Surface_tag
!variables locales
   real(kind=rdp) :: funk, tmp
   integer :: i, j, k, ib
   real(kind=rdp) :: ww(np), foo1, long_net, h1, h2, hmax, hmin
   integer, save :: nap=0
   real(kind=rdp), save :: foo_ini
   character(len=2) :: OK
   character(len=1) :: tag1,tag2,tag
   type (Mage_results) :: resultats_1, resultats_2
!-----------------------------------------------------------------------
!simulation initiale avec la moyenne


         tag = tag1 !; print*,' Appel calage_ok tag=',tag
         ww(:) = w(:) ; foo1 = calage_ok(ww,tag)

         C_Plus = distance_Domaine_simple(w) + foo1 + foo2
         foo1 = 1.e+79_rdp ; 
   
   if (Coeff_Penalisation*C_Plus < 1._rdp) then
            tag = tag2 !; print*,' Appel working_Area() tag=',tag
           foo1 = working_Area(w,tag,resultats)  ;  Surf1 = foo1*0.0001_rdp


      !write(6,*) 'Surfaces : ',Surf1, Surf2
      if (abs(foo1) > 1.e+29_rdp) then  
         funk = sens * 9.999e+99_rdp          !échec d'au moins une des simulations
      else
         funk = foo1 + sens*Coeff_Penalisation*C_Plus
      endif
   else  !inutile de faire les simulations car les contraintes ne sont pas satisfaites
      !print*,'Contraintes non satisfaites'
      funk = sens * Coeff_Penalisation*C_Plus
      Surf1 = -1._rdp  ;  Surf2 = -1._rdp  ! valeurs arbitraires pour les distinguer dans le fichier csv
   endif
   Foo_Surface_tag = funk
!------------------------------------------------------------------------------
end function Foo_Surface_tag

! meme fonction que foo_surface_no_cal mais permet de choisir le dossier d execution a utiliser pour paralleliser
function Foo_Surface_tag_no_cal(w, C_Plus,tag1,tag2) 


   use i_Parametres, only : rdp, np, issup
   use i_Annealing, only : fk_opt, fk_ini, sens, Surf1, Surf2
   use i_Consigne, only : w_ini, wmax, wmin, Coeff_Penalisation
   use dim_reelles, only : ismax, ibmax
   use geometrie_section, only : is1, is2, xgeo
   implicit none
!prototype
   real(kind=rdp), intent(in) :: w(np)
   real(kind=rdp), intent(out) :: C_Plus
   real(kind=rdp) :: Foo_Surface_tag_no_cal
!variables locales
   real(kind=rdp) :: funk, tmp
   integer :: i, j, k, ib
   real(kind=rdp) :: ww(np), foo1, long_net, h1, h2, hmax, hmin
   integer, save :: nap=0
   real(kind=rdp), save :: foo_ini
   character(len=2) :: OK
   character(len=1) :: tag1,tag2,tag
   type (Mage_results) :: resultats_1, resultats_2



         C_Plus = 0._rdp
         foo1 = 1.e+79_rdp ; 
   
            tag = tag2 !; print*,' Appel working_Area() tag=',tag
           foo1 = working_Area(w,tag,resultats)  ;  Surf1 = foo1*0.0001_rdp


      !write(6,*) 'Surfaces : ',Surf1, Surf2
      if (abs(foo1) > 1.e+29_rdp) then  
         funk = sens * 9.999e+99_rdp          !échec d'au moins une des simulations
      else
         funk = foo1 + sens*Coeff_Penalisation*C_Plus
      endif
   Foo_Surface_tag_no_cal = funk
!------------------------------------------------------------------------------
end function Foo_Surface_tag_no_cal
