#!/bin/bash


#dossier des fichiers distances
 
#cd /home/rothe/bin/mage_install/master a changer si le dossier courant n est pas celui ou sont present les fichiers distances


echo 'Que voulez-vous tracer ? distance parcourue,augmentation optimum, evolution optimum(d/a/e)'
read param2

echo 'voulez-vous tracer en fonction du nombre de simulations totale ou reussies(t/r) ?'
read param


if [ "${param2}" = 'd' ] ; then
	valeur1=3
elif [ "${param2}" = 'a' ] ; then
	valeur1=4
else 
	valeur1=6
fi


if [ "${param}" = 'r' ] ; then
	valeur=2
else
	valeur=1
fi

#ecriture du fichier instruction gnuplot
rm plot.txt

echo 'unset key' >> plot.txt
echo 'plot(33.7)'>> plot.txt
echo 'replot(31)'>> plot.txt

a=*Distance*
echo $a > dummy

longueur=$(wc -w dummy)
echo $longueur > dummy1
long=$(awk '{print$1}'  ./dummy1)

rm dummy
rm dummy1
echo "le nombre de fichier distance est '$long' "

echo "Combien de fichier voulez-vous tracer ? (nb maximum : '$long')"

read chiffre
echo $chiffre
iteration=0
for i in *Distance*
do
difference=$(($long-$iteration))
if [ $difference -le $chiffre ] ; then
	echo "$i trace" 
	echo 'replot "'$i'" using '$valeur':'$valeur1' with lines'>> plot.txt \;
fi
((iteration++))
done


gnuplot plot.txt -persist

rm plot.txt
