#!/bin/bash


# si dossier des fichiers stricklers different du dossier courant rajouter  : cd /chemin dossier courant


#Question sur les variables
echo "Quel est le nombre de tronçons ?" 
read troncon
troncon=$((2*$troncon))
echo "les parametres disponible pour tracer sont :"
echo "1_nb evaluation"
echo "3-$(($troncon+2))_strickler surface1"
echo "$(($troncon+3))-$((2*$troncon+2))_strickler surface2"
echo "$((2*$troncon+3))_surface1"
echo "$((2*$troncon+4))_surface2"
echo "$((2*$troncon+5))_test"
echo "$((2*$troncon+6))_temperature"
echo "$((2*$troncon+7))_ecart"
echo 'Que voulez-vous tracer en abscisse ? '
read param1

echo 'Que voulez-tracer en ordonnée?'
read param2

if [ $param2 = $((2*$troncon+7)) ] ; then
param2='($'$((2*$troncon+3))'-$'$((2*$troncon+4))')*100/51.088'
fi

echo 'Voulez vous tracer des points ou des lignes ? (0/1)'
read style
echo $style
if [ $style = 1 ] ; then
pointer='with lines'
else
pointer='with points'
fi

#ecriture du fichier instruction gnuplot
rm plot2.txt
rm dummys
rm dummy1s

echo 'unset key' >> plot2.txt
echo 'plot(33.7)'>> plot2.txt
echo 'replot(31)'>> plot2.txt

a=*stricklers*
echo $a > dummys

longueur=$(wc -w dummys)
echo $longueur > dummy1s
long=$(awk '{print$1}'  ./dummy1s)

rm dummys
rm dummy1s
echo "le nombre de fichier strickler est '$long' "

echo "Combien de fichier voulez-vous tracer ? (nb maximum : '$long')"

read chiffre
echo $chiffre
iteration=0

#boucle sur les fichiers stricklers
for i in *stricklers*
do
difference=$(($long-$iteration))
if [ $difference -le $chiffre ] ; then
	echo "$i trace" 
	if [ $param2 = '($'$((2*$troncon+3))'-$'$((2*$troncon+4))')*100/51.088' ] ; then
	echo 'replot "'$i'" using '$param1':(abs('$param2')>1 ? abs('$param2'):1/0) '$pointer''>> plot2.txt \;
	else
	echo 'replot "'$i'" using '$param1':(abs($'$param2')>1 ? abs($'$param2'):1/0) '$pointer''>> plot2.txt \;
	fi
fi
((iteration++))
done


gnuplot plot2.txt -persist # laisse afficher le graphe


#rm plot2.txt si on veut supprimer le fichier gnuplot
