

program incertitude
!==============================================================================
!            OPTIMISATION
!
! 16/09/2005 : simplification des contraintes : la m�me pour les 2 versions d'une composante
!              wmin et wmax ont donc une seule dimension.
!==============================================================================
   use i_Parametres  !, only : rdp, np
   use i_Consigne, only : W_ini, nc, modele, wmin, wmax, wtest, ntest, nst
   use i_Annealing, only : iopt, nb_eval, fk_ini, fk_opt, unite, sens, FOtyp
   use parametres, only : issup
   implicit none
!Variables locales
   real(kind=rdp) :: w_opt(np,2), debut
   integer :: i
   integer values(8) ! valeurs renvoyees par la fonction date_and_time()
   character :: zone*5, ddate*8, time*10, tmp*15, tmp2*10
   real(kind=rdp) :: foo0, foo1, foo2, xxlag, ww(np), zmax(issup), Lmaj(issup)
   real(kind=rdp) :: Xx(issup)
   real(kind=rdp) :: f_best
   
   real :: ctrl(12), x(np), f
   integer :: status
!interfaces
  
      function Foo_Ecart(w,xxlag)
        use i_Parametres, only : rdp,np
         real(kind=rdp), intent(in) :: w(np,2)
         real(kind=rdp), intent(out) :: xxlag
         real(kind=rdp) :: Foo_Ecart
      end function Foo_Ecart
      
      function Foo_Calage(w,xxlag)
        use i_Parametres, only : rdp,np
         real(kind=rdp), intent(in) :: w(np,2)
         real(kind=rdp), intent(out) :: xxlag
         real(kind=rdp) :: Foo_Calage
      end function Foo_Calage
      
      function Foo_MinMax(w,xxlag)
        use i_Parametres, only : rdp,np
         real(kind=rdp), intent(in) :: w(np,2)
         real(kind=rdp), intent(out) :: xxlag
         real(kind=rdp) :: Foo_MinMax
      end function Foo_MinMax

      function foo_interne(w,zmax,Lmaj,xx)
         use i_Parametres, only : rdp
         real(kind=rdp), intent(in) :: w(*)
         real(kind=rdp), intent(out) :: zmax(*), Lmaj(*), xx(*)
         real(kind=rdp) :: foo_interne
      end function foo_interne

      subroutine m4b(debut,values)
         use i_Parametres, only : rdp
         integer, intent(in) :: values(8)
         real(kind=rdp), intent(in) :: debut
      end subroutine m4b

      function Affiche(x,chain)
         use i_Parametres, only : rdp
         real(kind=rdp), intent(in) :: x
         character(len=13) :: affiche
         character(len=3) :: chain
      end function affiche

      function fff(n, x) result(fn_val)
         implicit none
         integer, intent(in)  :: n
         real, intent(in)     :: x(:)
         real                 :: fn_val
      end function fff
      
   end interface
!------------------------------------------------------------------------------
   call cpu_time(debut)

   ! sauvegarde des valeurs de K utilis�es
   call date_and_time(ddate,time,zone,values)
   tmp = ddate//'-'//time(1:6)
   open(unit=7, file='stricklers_'//tmp//'.csv', form='formatted', status='unknown')
   open(unit=8, file='sortie_'//tmp//'.txt', form='formatted', status='unknown')
   open(unit=17, file='alea.csv',form='formatted',status='unknown')
   write(17,*) 'Liste des nombres pseudo-aleatoires'

   call init  !initialisation

   if (FOtyp == 0 .or. FOtyp == 1 .or. FOtyp == 2 .or. FOtyp == 3) then  !�cart
      sens = -1._rdp
      if (ntest > 0) then
         f_best = 0.
         do i = 1, ntest
            write(*,'(a,i2)') 'Test numero ',i
            w_opt(:,:) = wtest(:,:,i)
            fk_opt = Foo_MinMax(w_opt,xxlag) ; tmp = affiche(fk_opt,unite)
            if (xxlag < 0.001) then
               tmp2 = ' calage OK'
            else
               tmp2 = ' calage KO'
            endif
            write(*,'(a,i2,2a)') ' Optimum a priori n�',i,' = ',tmp//tmp2
            write(8,'(a,i2,2a)') ' Optimum a priori n�',i,' = ',tmp//tmp2
         enddo
      endif
      if (FOtyp > 0) then
         call simulated_annealing(W_ini,W_opt,Foo_Ecart)    !algo du recuit simul�
         ww = 0.5_rdp * (wmax+wmin) ; foo0 = foo_interne(ww,zmax,Lmaj,Xx)* 0.0001_rdp
         ww = w_opt(:,2) ; foo2 = foo_interne(ww,zmax,Lmaj,Xx)* 0.0001_rdp
         ww = w_opt(:,1) ; foo1 = foo_interne(ww,zmax,Lmaj,Xx)* 0.0001_rdp
      else
         call rninit(1234)
         ctrl = -1  ;  ctrl(1) = 40  ;  ctrl(2) = 100  ;  ctrl(3) = 4  ;  ctrl(12) = 1
         call pikaia(fff,2*nc,ctrl,x,f,STATUS)
         !Print the results
         write(*,*) ' status : ',status    ;  write(8,*) ' status : ',status
         write(*,*) '      x : ',x(1:2*nc) ;  write(8,*) '      x : ',x(1:2*nc)
         write(*,*) '      f : ',f         ;  write(8,*) '      f : ',f
         !write(*,'(a,6f9.5/10x,6f9.5)') '    ctrl : ',ctrl
         w_opt(1:nc,1) = x(1:nc)*(wmax-wmin)+wmin
         w_opt(1:nc,2) = x(nc+1:2*nc)*(wmax-wmin)+wmin
         fk_opt = f
      endif
   else if (FOtyp == 4) then  ! surface minimale
      sens = +1._rdp
      call simulated_annealing(W_ini,W_opt,Foo_MinMax)    !algo du recuit simul�
      ww = 0.5_rdp * (wmax+wmin) ; foo0 = foo_interne(ww,zmax,Lmaj,Xx)* 0.0001_rdp
      ww = w_opt(:,2) ; foo2 = foo_interne(ww,zmax,Lmaj,Xx)* 0.0001_rdp
      ww = w_opt(:,1) ; foo1 = foo_interne(ww,zmax,Lmaj,Xx)* 0.0001_rdp
   else if (FOtyp == 5) then  ! surface maximale
      sens = -1._rdp
      call simulated_annealing(W_ini,W_opt,Foo_MinMax)    !algo du recuit simul�
   else if (FOtyp >= 6) then  !calage
      sens = +1._rdp
      call simulated_annealing(W_ini,W_opt,Foo_Calage)    !algo du recuit simul�
   else
      stop '>>>> Erreur : FOtype inconnu <<<<'
   endif

   if (FOtyp < 6) then
      ww = 0.5_rdp * (wmax+wmin) ; foo0 = foo_interne(ww,zmax,Lmaj,Xx)* 0.0001_rdp
      ww = w_opt(:,2) ; foo2 = foo_interne(ww,zmax,Lmaj,Xx)* 0.0001_rdp
      ww = w_opt(:,1) ; foo1 = foo_interne(ww,zmax,Lmaj,Xx)* 0.0001_rdp
      write(*,'(a)') ' Solution : '
      write(*,'(12x,f10.4,a,7x,f10.4,a)') foo1,' ha',foo2,' ha'
      write(*,'(10x,4f10.6)') (w_opt(2*i+1,1),w_opt(2*i+2,1),w_opt(2*i+1,2),w_opt(2*i+2,2),i=0,nst/2-1)
      write(*,'(10x,2f10.6)') (w_opt(i,1),w_opt(i,2),i=nst+1,nc)
      write(8,'(a)') ' Solution : '
      write(8,'(12x,f10.4,a,7x,f10.4,a)') foo1,' ha',foo2,' ha'
      write(8,'(10x,4f10.6)') (w_opt(2*i+1,1),w_opt(2*i+2,1),w_opt(2*i+1,2),w_opt(2*i+2,2),i=0,nst/2-1)
      write(8,'(10x,2f10.6)') (w_opt(i,1),w_opt(i,2),i=nst+1,nc)
      tmp = affiche(fk_opt,unite)
      write(*,'(3a,i8.4,a,i8,a)') ' Optimum = ',tmp, ' a l''evaluation ',iopt,' en ',nb_eval,' evaluations'
      write(8,'(3a,i8.4,a,i8,a)') ' Optimum = ',tmp, ' a l''evaluation ',iopt,' en ',nb_eval,' evaluations'
      
      tmp = affiche(100._rdp*abs((foo1-foo2)/foo0),' %'//' ')
      write(*,'(a,f10.4,a,f10.4,2a)') ' Surface minimale = ',min(foo1,foo2),' ha ## Surface maximale = '&
                                      ,max(foo1,foo2),' ha ## Ecart / surface initiale = ',tmp
      write(8,'(a,f10.4,a,f10.4,2a)') ' Surface minimale = ',min(foo1,foo2),' ha ## Surface maximale = '&
                                      ,max(foo1,foo2),' ha ## Ecart / surface initiale = ',tmp
   else
      write(*,'(a)') ' Solution : '
      write(*,'(10x,2f10.6)') (w_opt(2*i+1,1),w_opt(2*i+2,1),i=0,nst/2-1)
      write(*,'(10x,f10.6)') (w_opt(i,1),i=nst+1,nc)
      write(8,'(a)') ' Solution : '
      write(8,'(10x,2f10.6)') (w_opt(2*i+1,1),w_opt(2*i+2,1),i=0,nst/2-1)
      write(8,'(10x,f10.6)') (w_opt(i,1),i=nst+1,nc)
      tmp = affiche(fk_opt,unite)
      write(*,'(3a,i8.4,a,i8,a)') ' Optimum = ',tmp, ' a l''evaluation ',iopt,' en ',nb_eval,' evaluations'
      write(8,'(3a,i8.4,a,i8,a)') ' Optimum = ',tmp, ' a l''evaluation ',iopt,' en ',nb_eval,' evaluations'
   endif
                                   
   call m4b(debut,values)
   close(7)
   close(8)
end program incertitude

!
function fff(n,w) result(fn_val)

   use i_Parametres, only : rdp, np
   use i_Consigne, only : nc, wmax, wmin
   implicit none
   integer, intent(in)  :: n ! n doit �tre pair
   real, intent(in)     :: w(:)
   real                 :: fn_val
   real (kind=rdp) :: ww(np,2), xxlag
   integer, save :: nap = 0

   interface
      function Foo_Ecart(w,xxlag)
        use i_Parametres, only : rdp,np
         real(kind=rdp), intent(in) :: w(np,2)
         real(kind=rdp), intent(out) :: xxlag
         real(kind=rdp) :: Foo_Ecart
      end function Foo_Ecart

      function xlag(w)
         use i_Parametres, only : rdp, np
         real(kind=rdp), intent(in) :: w(np,2)
         real(kind=rdp) :: xlag
      end function xlag
   end interface
   
   nap = nap+1
   ww(1:nc,1) = w(1:nc)*(wmax-wmin)+wmin
   ww(1:nc,2) = w(nc+1:2*nc)*(wmax-wmin)+wmin
   !if (xlag(ww) < 0.0001_rdp) then
      fn_val = real(Foo_Ecart(ww,xxlag))
   !else
   !   fn_val = -10.**30_rdp
   !endif
   write(*,*) ' Evaluation #',nap, ' pour pikaia : ',fn_val,' xxlag = ',xxlag
end function fff
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
function Foo_Ecart(w, xxlag)
!==============================================================================
!       fonction � minimiser utilis�e par SIMULATED_ANNEALING
!                utilise FOO_Interne et ajoute les contraintes
!       cette fonction calcule un �cart entre 2 simulations
!       plusieurs mesures de cet �cart sont disponibles :
!       - diff�rence relative des surfaces inond�es (type 0)
!       - �cart moyen entre les lignes d'eau enveloppe (type 1)
!       - �cart maximal entre les lignes d'eau enveloppe (type 2)
!       - �cart moyen entre les largeurs du lit majeur inond� (type 3)
!
!       La valeur de r�f�rence correspond toujours � la consigne m�diane
!
!       Entr�e :
!               W = consigne (vecteur)
!       Sortie :
!               Foo_Ecart : valeur de l'�cart
!               xxlag = poids des contraintes ; o si satisfaites
!
!---Historique
!      04/03/2004 : reprise depuis PEGASE (1994)
!      08/06/2009 : mise � jour de la description
!==============================================================================
   use i_Parametres, only : rdp, np
   use i_Annealing, only : fk_opt, fk_ini, FOtyp, sens
   use i_Consigne, only : w_ini, wmax, wmin, penal
   use parametres, only : issup
   use dim_reelles, only : ismax, ibmax
   use geometrie_section, only : is1, is2
   implicit none
!prototype
   real(kind=rdp), intent(in) :: w(np,2)
   real(kind=rdp), intent(out) :: xxlag
   real(kind=rdp) :: Foo_Ecart
!variables locales
   real(kind=rdp) :: funk   
   integer :: i, j, k
   real(kind=rdp) :: ww(np), foo1, foo2
   integer, save :: nap=0
   real(kind=rdp), save :: foo_ini
   real(kind=rdp) :: zmax1(issup), zmax2(issup), ecart
   real(kind=rdp) :: Lmaj1(issup), Lmaj2(issup), xx(issup)
   character(len=2) :: OK
!interfaces
   interface
      function foo_interne(w,zmax,Lmaj,xx)
         use i_Parametres, only : rdp
         real(kind=rdp), intent(in) :: w(*)
         real(kind=rdp), intent(out) :: zmax(*), Lmaj(*), xx(*)
         real(kind=rdp) :: foo_interne
      end function foo_interne

      function xlag(w)
         use i_Parametres, only : rdp, np
         real(kind=rdp), intent(in) :: w(np,2)
         real(kind=rdp) :: xlag
      end function xlag
      
      function calage_ok(W)
         use i_Parametres, only : rdp
         real(kind=rdp), intent(in) :: w(*)
         real(kind=rdp) :: calage_ok
      end function calage_ok

   end interface
!-----------------------------------------------------------------------
      if (nap == 0) then
         nap = 1
         ww = (wmax + wmin)/2._rdp 
         foo_ini = foo_interne(ww,zmax1,Lmaj1,Xx)
         OK = 'KO'  ;  if (calage_ok(ww) < 0.0001_rdp) OK = 'OK'
         write(*,'(a,f10.4,2a)') ' Surface initiale : ',foo_ini*0.0001,' ha, Calage : ',OK
         write(8,'(a,f10.4,2a)') ' Surface initiale : ',foo_ini*0.0001,' ha, Calage : ',OK
      endif
      ! poids des constraintes : domaine + calage
      xxlag = xlag(w) + calage_ok(w(:,1)) + calage_ok(w(:,2))
      
      if (xxlag < 1000._rdp) then 
         ww = w(:,1) ; foo1 = foo_interne(ww,zmax1,Lmaj1,Xx)
         ww = w(:,2) ; foo2 = foo_interne(ww,zmax2,Lmaj2,Xx)
         
         j = is1(ibmax) ; k = is2(ibmax)
   
         if (abs(foo1) > 1.e+29_rdp .OR. abs(foo2) > 1.e+29_rdp) then
            funk = -1000._rdp * fk_opt
         else
            ecart = 0._rdp
            if (FOtyp == 0) then  !FO surface
               ecart = 100._rdp*abs((foo1-foo2)/foo_ini) !ecart en %
               !ecart = 100._rdp*(foo1-foo2)/foo_ini !ecart en %
            elseif (FOtyp == 1) then !FO niveaux L1
               do i = j, k
                  ecart = ecart + abs(zmax1(i)-zmax2(i))
               enddo
               ecart = 1000._rdp * ecart / (k-j+1) !�cart en mm
            elseif (FOtyp == 2) then !FO niveaux L^infini
               do i = j, k
                  ecart = max(ecart,abs(zmax1(i)-zmax2(i)))
               enddo
               ecart = 1000._rdp*ecart
            elseif (FOtyp == 3) then !FO largeurs
               do i = j+1, k
                  ecart = ecart + abs(Lmaj1(i-1)+Lmaj1(i)-Lmaj2(i-1)-Lmaj2(i)) &
                                * abs(Xx(i)-Xx(i-1))
               enddo
               ecart = ecart / abs(Xx(k)-Xx(j)) * 0.5_rdp
            else
               stop '>>>> Erreur : FOtype inconnu <<<<'
            endif
            funk = ecart + sens*penal*xxlag
         endif
      else  !inutile de faire les simulations car les contraintes ne sont pas satisfaites
         funk = -1000._rdp * fk_opt
      endif
      Foo_Ecart = funk
!--------------------------------------------------------------------------
end function Foo_Ecart
!

