!Initialisation

!                                                A conserver module constantes précision 
module i_Parametres
!==============================================================================
!       Definition des dimensions maximales des tableaux, de constantes
!       numeriques et de divers constantes physiques et mathematiques.
!==============================================================================
   implicit none
! version
   character(len=21), parameter :: version_string = '1.0.1_d du 20/08/2014'
! precision des reels (kind)      4:simple 8:double
   integer, parameter :: rdp = kind(1.d0)
   integer, parameter :: sp = kind(1.)
! -- dimensions de tableaux
   integer, parameter :: np = 100, maxsim = 200
! nb max de sections ou points de calcul
   integer, parameter :: issup=15001
! nb max de biefs ou branches
   integer, parameter :: ibsup=150
end module i_Parametres

! Parametre recuit simule+ generale

module i_Annealing  ! données pour le recuit simulé
   use i_Parametres, only : rdp, np
   implicit none
   integer :: nequil  ! 
   ! sinon nequil est le nombre d'évaluations sans amélioration
   ! de l'optimum nécessaires avant une réduction de la température
   integer :: n_acp_fin,nACP
   real(kind=rdp) :: t0, temperature
   real(kind=rdp) :: dth ! taux de variation de la température

   real(kind=rdp) :: fk_ini     !valeur initiale de la fonction-coût
   real(kind=rdp) :: fk_opt     !meilleure valeur de la fonction-coût
   integer :: nb_eval   !nombre d'évaluation de la fonction-coût FUNK()
   integer :: nb_simul  !nombre de simulations réalisées (appels de solveur)
   integer :: iopt      !numéro de la meilleure évaluation de FUNK()

   real(kind=rdp) :: seuil_amelioration
   integer :: ndd !nombre de mesures utilisées
   integer :: FOtyp  !type de mesure utilisée pour la Fonction-Objectif
                     !0 = surface (défaut)
                     !1 = niveaux en L1
                     !2 = niveaux en L^infini
                     !3 = largeurs
   character :: unite*3
   real(kind=rdp) :: sens ! 1 pour minimisation et -1 pour maximisation
   real(kind=rdp) :: Surf1, Surf2
   real(kind=rdp) :: cpu_externe
end module i_Annealing

!Parametres generaux

module i_Consigne
   use i_Parametres, only : rdp, np
   integer :: nech,namel,nelite,n_it_max
   real(kind=rdp) :: Wmin(np)  !table des valeurs minimales par variable
   real(kind=rdp) :: Wmax(np)  !table des valeurs maximales par variable
   integer :: inhib(np)  ! indicateur d'inhibition des variables d'intervalle nul
   real(kind=rdp) :: W_ini(np,2) !table des valeurs initiales des variables à optimiser
   integer :: nc                !nombre de consignes = dimension du pb
   integer :: nst  !nombre de consignes Strickler : nst < ou = nc
   integer :: ib(np) !n° du bief du tronçon
   real(kind=rdp) :: xmin(np), xmax(np) !pk début et fin du tronçon
   real(kind=rdp) :: eps !precision de la ligne d'eau == arrondi du calcul de l'écart mesure-observation

   character :: modele*60, basename*8, solveur*40
   integer ::  norme
   integer :: date_fin !date de fin de simulation en minutes à rechercher
                       !pour vérifier que la simulation s'est bien terminée
   real(kind=rdp) :: scale !facteur d'echelle pour les calculs de consigne

   real(kind=rdp) :: Wtest(np,2,10) !table des consignes à tester a priori
   integer :: ntest  !nombre de consignes à tester a priori
   integer::logic_test
   logical::ifinsert,iflin,ifacp,ifhybride

   type profil
      real(kind=rdp) :: x, zmoy
      integer :: nbval
      real(kind=rdp) :: y(24), z(24)
   end type profil
   type (profil) :: datageo(1000)
   integer :: nbpro
   real(kind=rdp) :: deltaZ
   character(len=80) :: titre
   real(kind=rdp) :: Coeff_Penalisation

   !calage
   type data_obs
      integer :: ib          !numéro du bief
      real(kind=rdp) :: x    !pk de la laisse de crue
      real(kind=rdp) :: z    !niveau observé
   end type data_obs
   type (data_obs) :: laisse_crue(10) , Q_rep(10)
   integer :: nb_obs_z, nb_obs_q         !nombre effectif de laisses de crue
   integer :: date_fin_calage
   real(kind=rdp) :: erreur_calage, cz=1._rdp, cq=1._rdp

   !Glue
   integer :: nbok  ! nb d'évaluations à atteindre avec calage OK.

end module i_Consigne


! A conserver comme geometrie du systeme
module Geometrie_Section
   use i_parametres, only: ibsup, issup, rdp
   integer :: is1(ibsup) ! is1(ib) = indice de la premiere section du bief de
                         !           rang de calcul ib
   integer :: is2(ibsup) ! is2(ib) = indice de la derniere section du bief de
                         !           rang de calcul ib
   integer :: ibu(ibsup) ! ibu(kb) = rang de calcul du bief de numero KB dans .TAL

   real(kind=rdp) :: xgeo(issup)
end module Geometrie_Section


! A conserver tel quel geometrie reseau
module Dim_Reelles
   implicit none

   integer :: ISMax    ! nb de sections ou points de calcul
   integer :: IBMax    ! nb de biefs ou branches du réseau
end module Dim_Reelles

module i_kriging
implicit none

end module i_kriging
