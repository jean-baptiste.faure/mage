
!Krigeage integrant les parametres de geometrie
INCLUDE 'krig_geometrie.f90'

! Krigeage utilisant deux meta-modeles pour le calage et pour la surface inondee 
INCLUDE 'R_krigeage.f90'


!!!!!!!!!!!! Krigeage  ordinaire!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
subroutine krigeage

use i_Parametres, only : rdp, np, issup
use i_Consigne
use i_Annealing
use linalg
use kriging
implicit none

! Variables locales
integer :: n,  nok, i,nmax,j,nparam,indice,ntot,n_LHS,nb_amel
real(kind=rdp) :: ww(np)
real(kind=rdp) ,allocatable :: valeurs_max(:),valeurs_min(:),&
   & param_min(:,:),param_max(:,:),param_LHS(:,:),param_min_vrai(:,:), &
   & param_max_vrai(:,:)
real(kind=rdp) :: w_optim(np,2),w_init(np,2)
real(kind=rdp),allocatable::cov(:,:),inv(:,:)
real(kind=rdp) :: foo, err_cal,foo_min, foo_max,ecart,foo_ini
real(kind=rdp) :: foo_ini_max,foo_ini_min
logical:: testeur,LHS
   
!--------------------------------------------------------------------------
write(*,*) 'Simulations pour la méthode interpolation krigeage'
write(8,*) 'Simulations pour la méthode interpolation krigeage'
open(10,file='resultat.txt',access='append')
write(10,*) (i,i=1,nc+3)

!allocation nombre d echantillon, amelioration
nok = 0  
n = 0
nb_param=nc
nb_echantillon=max(nech,nc+1) ! nombre de simulations calees pour generer le meta modele 
n_LHS=2*nb_echantillon ! nb parametres produit par l hypercube latin
LHS=.true.;nb_amel=namel ! nombre de pas adaptatif de la methode expected improvement


! Allocation des vecteurs utiles
allocate (valeurs_max(nelite)) ! valeurs des solutions potentielles max du meta modele
allocate (valeurs_min(nelite)) ! valeurs des solutions potentielles max du meta modele
allocate(param_LHS(n_LHS,nc)) ! param issus de l hypercube latin
allocate (param_max(nc,nelite))! param des solutions potentielles max du meta modele
allocate (param_min(nc,nelite))! param des solutions potentielles max du meta modele
allocate (param_max_vrai(np,nelite))
allocate (param_min_vrai(np,nelite))
allocate (echantillon(nc,nb_echantillon)) ! echantillon genere par toutes les simu calees
allocate (echantillon_bis(nc,nb_echantillon-1))
allocate (valeurs(nb_echantillon)) ! valeurs genere par toutes les simu calees
allocate(valeurs_bis(nb_echantillon-1))
allocate(coeff_bis(nb_echantillon))
allocate(coeff(nb_echantillon+1)) ! coefficients pour evaluer le meta-modele
allocate(ksi(2*nc)) ! hyper-parametres du meta modele
allocate(ksi_bis(2*nc))
allocate (cov(nb_echantillon,nb_echantillon)) ! matrice de covariance du meta-modele
allocate (inv(nb_echantillon,nb_echantillon)) ! inverse de la matrice de covariance du meta-modele



! creation de l echantillon pour generer le pool de solutions calees
call sampling(wmin(1:nc),wmax(1:nc),param_LHS,n_LHS,LHS)
! simulation initiale
ww = (wmax + wmin)/2._rdp

!boucle principale
do while (nok<nb_echantillon)
      n = n+1
      ntot=ntot+1
      foo = Foo_Surface(ww,err_cal)
      if (n==1) foo_ini=foo ! simulation initiale prise en compte
      ! test de calage
      if (err_cal*Coeff_Penalisation< 1.0_rdp) then
                    nok = nok + 1
                    valeurs(nok)=foo
                    ! stockage et affichage des resultats
                    write (*,*)  'nb sim tot',ntot,' nb sim ',nok,' surface inondee ',real(valeurs(nok)*0.0001_rdp,4),err_cal
                    write (8,*)  'nb sim tot',ntot,' nb sim ',nok,' surface inondee ',real(valeurs(nok)*0.0001_rdp,4),err_cal
                    write(10,*) (ww(i),i=1,nc),foo,maxval(valeurs,dim=1),minval(valeurs,dim=1)
                    echantillon(:,nok)=ww(1:nc)
      end if
      if(mod(n,n_LHS)==0) then
      	  ! si pas assez de parametres generes on retire un hypercube
               call sampling(wmin(1:nc),wmax(1:nc),param_LHS,n_LHS,LHS)
               n=n+1
               ! on rentre les nouveaux parametres a tester 
               ww(1:nc)=param_LHS(mod(n,n_LHS),1:nb_param)
               else
               !on rentre les nouveaux parametres a tester 
               ww(1:nc)=param_LHS(mod(n,n_LHS),1:nb_param)
               end if
   enddo
   
   !affichage solution initiale 
  write (*,*) 'surface initiale ' , foo_ini
   write (8,*) 'surface initiale ' , foo_ini
   
! Creation du meta modele 
! initialisation hyperparametres	
ksi=1._rdp
ksi_bis=1._rdp

ksi(1:nb_param)=2._rdp
ksi(nb_param+1:2*nb_param)=1._rdp
!!! optimisation des hyperparametres
call optim_log_vrai()
!!!! Calcul des coeff nécessaires à l evaluation du meta modele
call coeff_kriging()

!! test  si interpolation bien exacte!! 
do i=1,nb_echantillon
        foo=eval(echantillon(:,i))
        !if (mod(i,nb_echantillon/10)==0)
                write(*,*) 'valeur interpolee : ' ,foo, 'valeur' ,valeurs(i) 
        !if (mod(i,nb_echantillon/10)==0) 
                write(8,*) 'valeur interpolee : ' ,foo, 'valeur' ,valeurs(i) 
end do

! suavegarde des valeurs extremales de l echantillon de base
write (*,*) 'valeur moyenne',coeff(nb_echantillon+1)
write (8,*) 'valeur moyenne',coeff(nb_echantillon+1)
   foo_ini_max=maxval(valeurs)                                           
   foo_ini_min=minval(valeurs)
!!!!! Raffinement de l'echantillon de base par le maximum de l'expected improvement avec namel pas!!!! 
call amelioration_adapt2(nb_amel)
!!! on recalcule les coefficient pour  l evaluation
call coeff_kriging()

! test de  d interpolation exacte pour verifier le meta modele
do i=1,nb_echantillon
        foo=eval(echantillon(:,i))
        if (mod(i,nb_echantillon/10)==0) write(*,*) 'valeur interpolee : ' ,foo, 'valeur' ,valeurs(i) 
        if (mod(i,nb_echantillon/10)==0) write(8,*) 'valeur interpolee : ' ,foo, 'valeur' ,valeurs(i) 
end do

!!!!! MAXIMISATION DU SURROGATE MODEL
!!!! Lecture du maximum dans les valeurs apres creation du meta modele
foo_min=coeff(nb_echantillon+1)
foo_max=0
valeurs_max=minval(valeurs)
valeurs_min=maxval(valeurs)

!initialisation des vecteurs de parametres 
do i=1,nelite
param_max(:,i)=(wmin(1:nc)+wmax(1:nc))/2._rdp
param_min(:,i)=(wmin(1:nc)+wmax(1:nc))/2._rdp
param_max_vrai(:,i)=wmax
param_min_vrai(:,i)=wmin
end do

!!Maximisation du meta modele par recuit simule  : on trouve candidats potentiels
call  max_meta(nelite,valeurs_max,param_max,valeurs_min,param_min)

! sauvegarde des resultats
valeurs_max(1)=maxval(valeurs,dim=1)
valeurs_min(1)=minval(valeurs,dim=1)
indice=maxloc(valeurs,dim=1)
param_max(:,1)=echantillon(:,indice)
indice=minloc(valeurs,dim=1)
param_min(:,1)=echantillon(:,indice)
  param_max_vrai(1:nc,:)=param_max
  param_min_vrai(1:nc,:)=param_min

   !   ecriture et exploitation des resultats
   	   
   ! resultats avant meta modele
   write(*,*) 'simulation initiale'
    write(8,*) 'simulation initiale'
   foo_max=maxval(valeurs)                                           
   foo_min=minval(valeurs)
   write(*,*) 'Surf min' ,real(foo_ini_min,kind=4),'Surf max',real(foo_ini_max,kind=4)
   write(8,*) 'Surf min' ,real(foo_ini_min,kind=4),'Surf max',real(foo_ini_max,kind=4)
   ecart=(foo_ini_max-foo_ini_min)/foo_ini*100._rdp
   write(*,*) 'ecart simulation initiale',real(ecart,kind=4),'%'
   
   ! resultats des candidats potentiels de la maximisation du meta modele
   write(*,*) 'modele approche'
   write(8,*) 'ecart simulation initiale',real(ecart,kind=4),'%'
   write(8,*) 'modele approche'
   do i=1,nelite
           write(*,*) 'min',real(valeurs_min(i),kind=4),'max',real(valeurs_max(i),kind=4)
           write(8,*) 'min',real(valeurs_min(i),kind=4),'max',real(valeurs_max(i),kind=4)
   end do
   foo_max=maxval(valeurs_max)                                           
   foo_min=minval(valeurs_min,mask=valeurs_min>0._rdp)
   write(*,*) 'Surf min' ,real(foo_min,kind=4),'Surf max',real(foo_max,kind=4)
   write(8,*) 'Surf min' ,real(foo_min,kind=4),'Surf max',real(foo_max,kind=4)
   ecart=(foo_max-foo_min)/foo_ini*100._rdp
   
   ! resultat des simulations avec les candidats potentiels
   write(*,*) 'ecart modele',real(ecart,kind=4),'%'
   write(*,*) 'simulation dirigée'
    write(8,*) 'ecart modele',real(ecart,kind=4),'%'
   write(8,*) 'simulation dirigée'
   do i=1,nelite
           !$omp parallel 
                            !$omp single
                                          !$omp task
                                                        foo_min=Foo_Surface_tag(param_min_vrai(:,i),err_cal,'0','2')
                                          !$omp end task
                                          !$omp task
                                                        foo_max=Foo_Surface_tag(param_max_vrai(:,i),foo,'1','3')
                                         !$omp end task
                            !$omp end single
                        !$omp end parallel
           valeurs_min(i)=foo_min
           valeurs_max(i)=foo_max
           write(*,*) 'min',real(foo_min,kind=4),'max',real(foo_max,kind=4)
           write(8,*) 'min',real(foo_min,kind=4),'max',real(foo_max,kind=4)
           if(foo_min>1.e-06_rdp) then
           write(10,*) (param_min(j,i),j=1,nc),foo_min,maxval(valeurs,dim=1),minval(valeurs,dim=1)
           end if
           if (foo_max>1.e-06_rdp) write(10,*) (param_max(j,i),j=1,nc),foo_max, &
           & maxval(valeurs,dim=1),minval(valeurs,dim=1)
   end do
   
   
   ! Ecriture et exploitation des resultats finaux 
   foo_max=maxval(valeurs_max)
   foo_min=minval(valeurs_min,mask=valeurs_min>0._rdp)
   ecart=(foo_max-foo_min)/foo_ini*100._rdp
   write(*,*) 'Surf min' ,real(foo_min,kind=4),'Surf max',real(foo_max,kind=4)
    write(*,*) 'ecart simulation dirigee',real(ecart,kind=4),'%'
    write(*,*) 'nombre de simulations',ntot+nelite+2*nb_amel
    write(*,*) 'nombre evaluation',nok+nelite+2*nb_amel
    write(8,*) 'Surf min' ,real(foo_min,kind=4),'Surf max',real(foo_max,kind=4)
    write(8,*) 'ecart simulation dirigee',real(ecart,kind=4),'%'
    write(8,*) 'nombre de simulations',ntot+nelite+2*nb_amel
    write(8,*) 'nombre evaluation',nok+nelite+2*nb_amel
    open (12,file='opt_max.txt',position='append')
    indice=maxloc(valeurs_max,dim=1)
    write(12,*) (param_max(j,indice),j=1,nc),foo_max
    indice=minloc(valeurs_min,mask=valeurs_min>0._rdp,dim=1)
    open(13,file='opt_min.txt',position='append')
    write(13,*) (param_min(j,indice),j=1,nc),foo_min
      call reset_krig()
      open(14,file='optimum.txt',access='append')
      indice=maxloc(valeurs_max,dim=1)
       write(14,*) (param_max(j,indice),j=1,nc),foo_max,ecart
       indice=minloc(valeurs_min,mask=valeurs_min>0._rdp,dim=1)
       write(14,*) (param_min(j,indice),j=1,nc),foo_min,ecart
       
      ! booleen pour savoir si on continue sur un recuit simule avec la solution optimale trouvee 
    testeur=ifhybride
    if (testeur) then
   !initialisation pour le recuit
    w_init=0._rdp
    w_optim=0._rdp
    indice=maxloc(valeurs_max,dim=1)
    w_init(1:nc,1)=param_max(:,indice)
    indice=minloc(valeurs_min,mask=valeurs_min>0._rdp,dim=1)
    w_init(1:nc,2)=param_min(:,indice)
    sens = -1._rdp
         modify => modify_2c
      working_Area => flooded_Area
              if (ifacp) then
      dth=1-((1.0e-04_rdp/temperature)**(1.0_rdp/(nacp))) 
    else
    dth=1-((1.0e-04_rdp/temperature)**(1.0_rdp/( n_it_max))) 
    end if
    write(*,*) 'nouveau dth' , dth
      ! recuit
    call simulated_annealing(w_init,w_optim,Foo_Ecart_amel,ifinsert,ifacp,iflin)
    end if
end subroutine krigeage


! routine pour la methode adaptative de expected Improvement
subroutine amelioration_adapt2(n)
use  calage_error, only : param_mini,param_maxi
use rand_num
use kriging
use i_Parametres
use i_Consigne, only : nc, wmin, wmax, nst, nbok
use Genetic_algorithm
implicit none
!initialisation
integer:: i,k,n,iteration,n_standstill1,n_standstill2
real (kind=rdp):: param_min(nb_param),param_max(nb_param) ! changer en real 4 si pikaia
real(kind=rdp)::param_min_vrai(np),param_max_vrai(np),ww(nb_param),foo
real(kind=rdp) :: foo1,foo2,err_cal1,err_cal2,val1,val2,ww1(nb_param),ww2(nb_param),diff(np)
 real :: ctrl(12),ctrl2(12),f,f1
integer:: ind_max,ind_min
 integer :: status1,status,seed,seed1
logical :: no_new_point1,no_new_point2
iteration=0
param_min_vrai=wmin
param_max_vrai=wmax
n_standstill1=1
n_standstill2=1
 call init_random_seed()
 ! boucle principale
do while(iteration < n)
write(*,*) 'iteration : ' ,iteration
write(8,*) 'iteration : ' ,iteration
        no_new_point1=.true.
        no_new_point2=.true.
        ! lot de parametres initiaux pour la recherche maximum EI
        	ww1(1:nc)=param_maxi(1:nc)
        	ww2(1:nc)=param_mini(1:nc)
        	diff=(wmax-wmin)/5.0_rdp
              do k = 1, nc
              ww1(k)=+random(wmin(k),wmax(k))
               !ww1(k) = max (ww1(k),wmin(k))
               !ww1(k) = min (ww1(k),wmax(k))
                ww2(k)=random(wmin(k),wmax(k))
               ! ww2(k) = max (ww2(k),wmin(k))
               ! ww2(k) = min (ww2(k),wmax(k))
                           ! ww(k) = random(0.99_rdp*wmin(k),1.01_rdp*wmax(k))
                     
                    end do
             
                !$omp parallel 
                            !$omp single
                                          !$omp task
                                          	  ! test pour recherche maximum EI pour ameliorer le maximum
                                          	! boucle de repetition tant que pas de resultat cale
                                                        do while (no_new_point1 .and. n_standstill1<6)
                                                                val1=Expect_min(ww1)
                                                                !max par recuit simule
                                                                call init_recuit(nb_param,wmin(1:nc),wmax(1:nc), &
                                                                &ww1,val1,-1._rdp,0.001_rdp)
                                                                nequil=1
                                                                call recuit_sa(param_min,Expect_min)
                                                                param_min_vrai(1:nc)=param_min
                                                                foo1=foo_Surface_tag(param_min_vrai,err_cal1,'0','2')
                                                                !test de calage
                                                         if(err_cal1*Coeff_Penalisation< 1.0_rdp) then
                                                                        no_new_point1=.false.
                                                                        n_standstill1=1
                                                        else
                                                        	! ecriture  du resultat
                                                                      foo1=eval(param_min)
                                                                      write(*,*) 'min rate' ,foo1,'err',err_cal1
                                                                      write(8,*) 'min rate' ,foo1,'err',err_cal1
                                                                      n_standstill1=n_standstill1+1
                                                        end if
                                                        end do
                                          !$omp end task
                                          !$omp task
                                          	    ! test pour recherche maximum EI pour ameliorer le minimum
                                          	! boucle de repetition tant que pas de resultat cale
                                                        do while (no_new_point2 .and. n_standstill2<6)
                                                                  val2=Expect_max(ww2)
                                                                  !max par recuit simule
                                                                  call init_recuit1(nb_param,wmin,wmax,ww2,val2,-1._rdp,0.001_rdp)
                                                                  nequil1=1
                                                                  call recuit_sa1(param_max,Expect_max)
                                                                  param_max_vrai(1:nc)=param_max
                                                                  foo2=foo_Surface_tag(param_max_vrai,err_cal2,'1','3')
                                                                  !test de calage 
                                                          if(err_cal2*Coeff_Penalisation< 1.0_rdp) then
                                                                  no_new_point2=.false.
                                                                  n_standstill2=1
                                                          else
                                                          	  !ecriture resultat
                                                                          foo2=eval(param_max)
                                                                  write(*,*) 'max rate' ,foo2,'err',err_cal2
                                                                  write(8,*) 'max rate' ,foo2,'err',err_cal2
                                                                  n_standstill2=n_standstill2+1
                                                          end if
                                                          end do
                                         !$omp end task
                            !$omp end single
                        !$omp end parallel
                        	
        ! test si il y solution calee pour l amelioration du maximum
        if(no_new_point1) then
        ! si pas de solution calee on prend celle initiale ce qui va pousser les recherches ailleurs
         if (err_cal2*Coeff_Penalisation>5.0e04_rdp) then
        foo=(maxval(valeurs,dim=1)+minval(valeurs,dim=1))/2
        ! ajout du point dans le meta modele + affichage
        call add_point(param_min_vrai(1:nc),foo)
        write(*,*) 'nouveau point CL' , real(foo,kind=4),'vrai min',minval(valeurs,dim=1),err_cal1
        write(8,*) 'nouveau point CL' , real(foo,kind=4),'vrai min',minval(valeurs,dim=1),err_cal1
        end if
        n_standstill1=1
        else
        	! ajout du point dans le meta modele + affichage
        call add_point(param_min_vrai(1:nc),foo1)
              write(*,*) 'nouveau point min' , real(foo1,kind=4),'vrai min',minval(valeurs,dim=1),err_cal1
              write(8,*) 'nouveau point min' , real(foo1,kind=4),'vrai min',minval(valeurs,dim=1),err_cal1
              write(10,*) (param_min(i),i=1,nc),foo1,maxval(valeurs,dim=1),minval(valeurs,dim=1)
        end if
        
             ! test si il y solution calee pour l amelioration du maximum
        if(no_new_point2) then
        ! si pas de solution calee on prend celle initiale ce qui va pousser les recherches ailleurs
       if (err_cal2*Coeff_Penalisation>5.0e04_rdp) then
       foo=(maxval(valeurs,dim=1)+minval(valeurs,dim=1))/2
                ! ajout du point dans le meta modele + affichage
        call add_point(param_max_vrai(1:nc),foo)
        write(*,*) 'nouveau point CL' , real(foo,kind=4),'vrai max',maxval(valeurs,dim=1),err_cal2
        write(8,*) 'nouveau point CL' , real(foo,kind=4),'vrai max',maxval(valeurs,dim=1),err_cal2
        end if
        n_standstill2=1
        else
        	        ! ajout du point dans le meta modele + affichage
        call add_point(param_max_vrai(1:nc),foo2)
              write(*,*) 'nouveau point max' , real(foo2,kind=4),'vrai max',maxval(valeurs,dim=1),err_cal2
              write(8,*) 'nouveau point max' , real(foo2,kind=4),'vrai max',maxval(valeurs,dim=1),err_cal2
              write(10,*) (param_max(i),i=1,nc),foo2,maxval(valeurs,dim=1),minval(valeurs,dim=1)
        end if
        ! optimisation des hyperparametres avant de recommencer pour l iteration suivante
        call optim_log_vrai()
        iteration=iteration+1
        end do
end subroutine amelioration_adapt2


