
!routine lisant le fichier de geometrie pk et cote fond en colonne 
!et calculant les points de rupture de pente
subroutine identify_geom
use geom_val
use i_Parametres, only : rdp, np, issup
use i_Consigne
implicit none
integer::i,indice_dummy(nc-nst),n_section
real(kind=rdp)::derive_courant,derive_apres,derive_globale,test
if (.not. allocated(pk)) allocate(pk(nc-nst))
if (.not. allocated(cote_fond) ) allocate(cote_fond(nc-nst))
n_section=nc-nst
open(22,file='red_geom.txt')
read(22,*) (pk(i),i=1,n_section)
read(22,*) (cote_fond(i),i=1,n_section)
close(22)
new_nc=1
indice_dummy(1)=1
! calcul de la derive globale
derive_globale=(cote_fond(n_section)-cote_fond(1))/(pk(n_section)-pk(1))
! calcul des rupture de pente (changement de derive gd devant la pente globale
do i=1,n_section-2
  derive_courant=(cote_fond(i+1)-cote_fond(i))/(pk(i+1)-pk(i))
    derive_apres=(cote_fond(i+2)-cote_fond(i+1))/(pk(i+2)-pk(i+1))
  test=abs(derive_courant-derive_apres)/abs(derive_globale)
  if(test>0.5) then
    new_nc=new_nc+1
    indice_dummy(new_nc)=i+1
   end if
end do
indice_dummy(new_nc+1)=n_section
new_nc=new_nc+1
if (.not. allocated(indice)) allocate(indice(new_nc))
indice=indice_dummy(1:new_nc)
write(*,*) 'identification_geo'
write(*,*) (indice_dummy(i),i=1,new_nc)
end subroutine identify_geom

! conversion des param geometriques simplifies en parametres reels 
subroutine convert_geom(param_red,vrai_param)
use geom_val
use i_Parametres, only : rdp, np, issup
use i_Consigne
implicit none
real(kind=rdp)::param_red(nst+new_nc),vrai_param(nc),a,b
integer :: i,j
vrai_param(nst+1:nc)=cote_fond
do i=1,new_nc
vrai_param(nst+indice(i))=cote_fond(indice(i))+param_red(i+nst)
!if (indice(i) <1) write(*,*) nst,indice,param_red,vrai_param
end do
vrai_param(1:nst)=param_red(1:nst)

do j=1,new_nc-1
if(indice(j+1)-indice(j)>1) then 
a=(vrai_param(indice (j+1)+nst)-vrai_param(indice(j)+nst))/(pk(indice (j+1))-pk(indice(j)))
b=vrai_param(indice(j)+nst)-a*pk(indice(j))
vrai_param(indice(j)+1+nst:indice(j+1)-1+nst)=pk(indice(j)+1:indice(j+1)-1)*a+b
 end if
end do
vrai_param(nst+1:nc)=cote_fond-vrai_param(nst+1:nc) 
end subroutine convert_geom

! krigeage avec 2 metamodeles integrant les parametres de geometrie
subroutine R_krigeage_geom
use i_Parametres, only : rdp, np, issup
use i_Consigne
use i_Annealing,only : dth,temperature,nacp
use linalg
use Genetic_algorithm
use recuit_simule1
use recuit_simule
use R_krig
use rand_num
use geom_val
implicit none

! Variables locales
integer :: n, k, nok, i,j,ntot,n_LHS,nb_amel,nb_ech,nb_points
real(kind=rdp) :: ww(np)
real(kind=rdp) ,allocatable :: param_LHS(:,:),ech(:,:)
real(kind=rdp) :: w_optim(np,2),w_init(np,2),test
real(kind=rdp) ::  foo, err_cal,foo_min, foo_max,foo_ini
real(kind=rdp),allocatable::EI_max(:,:),EI_min(:,:)
logical:: testeur,LHS
real ::  ecart_max
real(kind=rdp)::w_maxi(nc,100),w_mini(nc,100),val_min(100),val_max(100)
real(kind=rdp),allocatable :: x_ini(:),x_mini(:),x_maxi(:),param_red(:),x(:),x_max(:)
integer :: ind1,ind2,new_n
logical:: ifamel
   
!--------------------------------------------------------------------------
write(*,*) 'Simulations pour la méthode interpolation krigeage'
write(8,*) 'Simulations pour la méthode interpolation krigeage'
open(10,file='resultat.txt',access='append')
open(16,file='ech_krig.txt',status='replace')
write(10,*) (i,i=1,nc+3)
  ! call reset_krig()
call identify_geom()
new_n=nst+new_nc
nok = 0  ;  n = 0;nb_ech=max(nech,new_n)!
n_LHS=nb_ech;LHS=.true.;nb_amel=1;ifamel=.false.!nb_ech/8;
nb_points=2
val_min=0._rdp
val_max=0._rdp
w_mini=0._rdp
w_maxi=0._rdp
allocate(x_ini(2*new_n),x_mini(2*new_n),x_maxi(2*new_n),param_red(new_n),x(2*new_n),x_max(2*new_n))


allocate(EI_max(nc,nb_points))
allocate(EI_min(nc,nb_points))
allocate(param_LHS(n_LHS,new_n))
if (ifamel) then
allocate (ech(new_n+2,nb_ech+nb_amel*2*nb_points))
else
allocate (ech(new_n+2,nb_ech))
end if



!!! Echantillon de base  !!!!
!call plan_exp_ACP(param_LHS,n_LHS)
call sampling(wmin(1:new_n),wmax(1:new_n),param_LHS,n_LHS,LHS)
ww = (wmax + wmin)/2._rdp
param_red=ww(1:new_n)
do while (nok<nb_ech .and. n<n_LHS)
      n = n+1
      ntot=ntot+1
      foo = Foo_Surface_no_cal(ww,err_cal)
      if (n==1) then
                    foo_ini=foo
                    close(16)
                    open(16,file='ech_krig.txt',access='append')
                    end if
      if (err_cal*Coeff_Penalisation < 1.0_rdp) then
                    nok = nok + 1
                    err_cal=calage_ok(ww,'0')
                    ech(1:new_n,nok)=param_red
                    ech(new_n+1,nok)=foo
                    ech(new_n+2,nok)=err_cal*Coeff_Penalisation
                    write (*,*)  'nb sim tot',n,' nb sim ',nok,' surface inondee ', &
                    & real(ech(new_n+1,nok)*0.0001_rdp,4),err_cal*Coeff_Penalisation
                    write (8,*)  'nb sim tot',n,' nb sim ',nok,' surface inondee ', &
                    & real(ech(new_n+1,nok)*0.0001_rdp,4),err_cal*Coeff_Penalisation
                    write(10,*) (ww(i),i=1,nc),foo,maxval(ech(new_n+1,:)),minval(ech(new_n+1,:))
                    write(16,*) (param_red(i),i=1,new_n),foo,err_cal*Coeff_Penalisation
               
      end if
       param_red(1:new_n)=param_LHS(n,1:new_n)
       call convert_geom(param_red,ww)
   enddo
   
  write (*,*) 'surface initiale ' , foo_ini
   write (8,*) 'surface initiale ' , foo_ini
   n=0
   if(ifamel) then
   do while(n<nb_amel)
   call execute_command_line("Rscript krig_R.R")
   open(17,file='EI_krig.txt')
   do j=1,nb_points
   read (17,*) (EI_min(i,j),i=1,nc)
   end do
   do j=1,nb_points
   read (17,*) (EI_max(i,j),i=1,nc)
   end do
   close(17)
   do k=1,nb_points
   ww(1:nc)=EI_min(:,k)
   foo=Foo_Surface_no_cal(ww,err_cal)
      if (err_cal < 1._rdp) then
                    nok = nok + 1
                    ech(1:nc,nok)=ww(1:nc)
                    err_cal=calage_ok(ww,'0')
                    ech(nc+1,nok)=foo
                    ech(nc+2,nok)=err_cal*Coeff_Penalisation
                    write (*,*)  'valeur_min',foo,'vrai min',minval(ech(nc+1,:),mask=ech(nc+1,:)>0._rdp,dim=1),'nb amel',n
                     write (8,*)  'valeur_min',foo,'vrai min',minval(ech(nc+1,:),mask=ech(nc+1,:)>0._rdp,dim=1),'nb amel',n
                    write(10,*) (ww(i),i=1,nc),foo,maxval(ech(nc+1,:),dim=1),minval(ech(nc+1,:),mask=ech(nc+1,:) &
                    & >0._rdp,dim=1)
                    write(16,*) (ww(i),i=1,nc),foo,err_cal

      end if
      end do
      do k=1,nb_points
   ww(1:nc)=EI_max(:,k)
   foo=Foo_Surface_no_cal(ww,err_cal)
      if (err_cal < 1._rdp) then
                    nok = nok + 1
                     err_cal=calage_ok(ww,'0')
                    write (*,*)  'valeur_max',foo,'vrai min',maxval(ech(nc+1,:),dim=1),'nb amel',n
                     write (8,*)  'valeur_max',foo,'vrai min',maxval(ech(nc+1,:),dim=1),'nb amel',n
                    write(10,*) (ww(i),i=1,nc),foo,maxval(ech(nc+1,:),dim=1),minval(ech(nc+1,:),dim=1)
                    write(16,*) (ww(i),i=1,nc),foo,err_cal
                    ech(1:nc,nok)=ww(1:nc)
                    ech(nc+1,nok)=foo
                    ech(nc+2,nok)=err_cal*Coeff_Penalisation
      end if
      end do
      n=n+1
   end do
   end if
         call execute_command_line("Rscript model_krig_final_R.R")
         call init_krig(ech(1:new_n,:),ech(new_n+2,:),ech(new_n+1,:))
        ! write(*,*) covar(ech(1:,i
        
   do i=1,nb_ech
        foo=eval_surf_cal(ech(1:new_n,i))
        err_cal=eval_cal(ech(1:new_n,i))
        !if (mod(i,nb_echantillon/10)==0)
                !write(*,*) 'valeur interpolee : ' ,err_cal, 'valeur' ,valeurs_cal(i)
                write(*,*) 'valeur interpolee : ' ,foo, 'valeur' ,valeurs_surf(i),err_cal
        !if (mod(i,nb_echantillon/10)==0) 
                write(8,*) 'valeur interpolee : ' ,foo, 'valeur' ,valeurs_surf(i) ,err_cal
end do
 x(1:new_n)=ech(1:new_n,1)
 write(*,*) 'test_ecart'
         test=0._rdp
   do i=1,nb_ech
           if (eval_cal(ech(1:new_n,i))<1.0_rdp) then
           do j=i,nb_ech
           x(new_n+1:2*new_n)=ech(1:new_n,i)
           x(1:new_n)=ech(1:new_n,j)
        foo=eval_ecart(x)
        if (foo>test) then
                test=foo
                ind1=i
                ind2=j
               end if
               end do
               end if
end do
x(new_n+1:2*new_n)=ech(1:new_n,ind2)
x(1:new_n)=ech(1:new_n,ind1)
x_ini=x
!x_ini(1:new_n)=(wmin(1:new_n)+wmax(1:new_n))/2._rdp
!x_ini(new_n+1:2*new_n)=(wmin(1:new_n)+wmax(1:new_n))/2._rdp
write(*,*) 'ecart_initial',test
write(8,*) 'ecart_initial',test
   !!!! calcul du max
   !wmini(1:nc)=wmin(1:nc)
   !wmini(nc+1:2*nc)=wmin(1:nc)
   !wmaxi(1:nc)=wmax(1:nc)
   !wmaxi(nc+1:2*nc)=wmax(1:nc)
   !call init_recuit1(nb_param,wmini,wmaxi,x,foo,-1._rdp,0.001_rdp)
   !nequil1=20
   !call recuit_sa1(x,eval_ecart)
   !foo=eval_ecart(x)
   do j=1,nelite
  ! if(j==1) then
   x_max=x_ini
   ecart_max=eval_ecart(x_ini)
   foo_min=eval_surf(x_ini(1:new_n))
   foo_max=eval_surf(x_ini(new_n+1:2*new_n))
   foo=foo_max-foo_min
   x_mini=x_ini(1:new_n)
   x_maxi=x_ini(new_n+1:2*new_n)
   if (foo<0._rdp) then
              foo=foo_max
              foo_max=foo_min
              foo_min=foo
              x_mini=x_ini(new_n+1:2*new_n)   
              x_maxi=x_ini(1:new_n)   
              x_ini(1:new_n) =x_mini
              x_ini(new_n+1:2*new_n)=x_maxi
              
   end if
              
   !else
   !ind1=int(random(1._rdp,nb_ech*1._rdp))
   !ind2=int(random(1._rdp,nb_ech*1._rdp))
   ! x_max(1:nc)=ech(1:nc,ind1)
   ! x_max(nc+1:2*nc)=ech(1:nc,ind2)
   !end if
   call init_random_seed()
   !write(*,*) 'iteration',j
   !write(*,*) 'ecart_ini',ecart_max
              !APPROCHE RECUIT SIMULE
   call init_recuit(nb_param,wmin(1:new_n),wmax(1:new_n),x_maxi(1:new_n),foo_max,-1._rdp,0.005_rdp)
   nequil=1
   call recuit_sa(x_maxi,eval_surf_cal)
     call init_recuit1(nb_param,wmin(1:new_n),wmax(1:new_n),x_mini(1:new_n),foo_min,1._rdp,0.005_rdp)
     nequil1=1
    call recuit_sa1(x_mini,eval_surf_cal_min)
   w_optim=0._rdp
    ! foo_max=Foo_Surface(w_optim(:,1),err_cal)
      !foo_min=Foo_Surface(w_optim(:,2),err_cal)
       foo_max=eval_surf(x_maxi)
      foo_min=eval_surf(x_mini)  
                    val_max(j)=foo_max
                    val_min(j)=foo_min
                    call convert_geom(x_mini,w_mini(:,j))
                    call convert_geom(x_maxi,w_maxi(:,j))
      !if (foo_min>0._rdp .and. foo_max>0._rdp) then
      foo=abs(foo_max-foo_min)/valeurs_surf(1)*100._rdp
      !else
     !foo=0._rdp
     !end if
     write(*,*) 'it', j,'vrai_ecart',foo,'foo_max',foo_max,'foo_min',foo_min
     write(8,*) 'it', j,'vrai_ecart',foo,'foo_max',foo_max,'foo_min',foo_min
     end do
     val_max=0._rdp
     val_min=0._rdp
     do j=1,nelite
     w_optim(1:nc,1)=w_maxi(:,j)
      w_optim(1:nc,2)=w_mini(:,j)
      foo_max=Foo_Surface(w_optim(:,1),err_cal)
      foo_min=Foo_Surface(w_optim(:,2),err_cal)
      val_max(j)=foo_max
      val_min(j)=foo_min
      write(*,*) 'foo_max',foo_max,'foo_min',foo_min
       write(8,*) 'foo_max',foo_max,'foo_min',foo_min
     end do
     ind1=maxloc(val_max,dim=1)
     ind2=minloc(val_min,mask=val_min>0._rdp,dim=1)
     foo_max=maxval(val_max,dim=1)
     foo_min=minval(val_min,mask=val_min>0._rdp,dim=1)
      if(ind1>0 .and. foo_max>0.0_rdp) then
     	      w_init(1:nc,1)=w_maxi(:,ind1)
     else
          foo_max=eval_surf(x_ini(new_n+1:2*new_n))
      call convert_geom(x_ini(new_n+1:2*new_n),w_init(:,1))
       end if
     if(ind2>0 .and. foo_min>0.0_rdp) then
      w_init(1:nc,2)=w_mini(:,ind2)
     else
      foo_min=eval_surf(x_ini(1:new_n))
      call convert_geom(x_ini(1:new_n),w_init(:,2))
      end if
    
     foo=abs(foo_max-foo_min)/valeurs_surf(1)*100._rdp
     write(*,*) 'ecart_final',foo,'foo_max',foo_max,'foo_min',foo_min
      write(8,*) 'ecart_final',foo,'foo_max',foo_max,'foo_min',foo_min
          testeur=ifhybride
    if (testeur) then

    w_optim=0._rdp
    sens = -1._rdp
         modify => modify_2c
      working_Area => flooded_Area
    call simulated_annealing(w_init,w_optim,Foo_Ecart_amel,ifinsert,ifacp,iflin)
    end if
end subroutine R_krigeage_geom

