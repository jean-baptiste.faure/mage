!!!!!!Fonctions hypercubes latins
 subroutine sampling(wMin,wMax,wTest,n,normal_sampling)
 use i_Parametres
 implicit none 
 integer::i,j,k,n
 real(kind=rdp) :: wMin(:),wMax(:),wTest(:,:)
 integer,allocatable  :: LHS(:,:)
 
 logical :: normal_sampling
 if (normal_sampling) then
          k=size(wMin)
 else
          k=2*size(wMin)
 end if
 

 allocate (LHS(n,k))
 ! construction de la matrice de permutation du Latin hypercube
 call laths(LHS,n,k)
 if (normal_sampling) then 
         do j=1,k
                do i=1,n
                        if (wMin(j)/=wMax(j)) then
                                wTest(i,j)=(LHS(i,j)-1._rdp+random(0._rdp,1._rdp))/real(n,kind=rdp) &
                                &*(wMax(j)-wMin(j))+wMin(j)
                        else
                                wTest(i,j)=wMin(j)
                        end if
                end do
        end do
else
        do j=1,k
                do i=1,n
                        if (wMin(modulo(j-1,k/2)+1)/=wMax(modulo(j-1,k/2)+1) )then
                                wTest(i,j)=(LHS(i,j)-1._rdp+random(0._rdp,1._rdp))/real(n,kind=rdp) &
                                & *(wMax(modulo(j-1,k/2)+1)-wMin(modulo(j-1,k/2)+1))+wMin(modulo(j-1,k/2)+1)
                        else
                                wTest(i,j)=wMin(modulo(j-1,k/2)+1)
                        end if
                end do
        end do
end if
deallocate (LHS)
end subroutine sampling

subroutine laths(L,n,k)
use i_Parametres
implicit none
integer::n,k,maxit,g,nb_courant,nb
integer::i,j,maxi,temp
integer::L(n,k),L_temp(n,k)
maxit=5000
g=0
if(n>500) maxit=10
!amelioration du carre latin par statistique
do while(g<maxit)

g=g+1
if(g==1) then
 do j=1,k
         do i=1,n
                 L_temp(i,j)=i
         end do 
 end do
 end if
 do j=1,k
       ! write(*,*) 'test shuffle before' , (L_temp(i,j),i=1,n)
         call shuffle(L_temp(:,j))
         !write(*,*) 'test shuffle after' , (L_temp(i,j),i=1,n)
end do
if (n>10000) then
        L=L_temp
        exit
else
       if (g==1) then
        temp=distance_tot(L_temp,nb)
        else
        temp=distance_tot(L_temp,nb_courant)
        end if
end if
if (g==1) then
        maxi=temp
        L=L_temp
        nb_courant=nb
end if
if(temp>maxi) then
        maxi=temp
        L=L_temp
        nb=nb_courant
else if (temp==maxi .and. nb_courant<nb) then
        L=L_temp
        nb=nb_courant
end if
if(mod(g,max(maxit/4,1))==0) write(*,*) 'production hypercube : ',g ,'critere',real(maxi,kind=4),nb
end do
write(*,*) 'production hypercube finie!'
! amelioration de l'hypercube latin tres long column swap  algorithm!!!!!
!write (*,*) 'boucle it=',g,'max atteint:',mini
!do while(g<maxit  )
!        do i=1,k
!                 mini=distance_tot(L)
!                 indice1=1
!                 indice2=1
!                 do m=1,n-1
!                         do j=m+1,n
!                                dummy=L(1:n,i)
!                                call swap(L(1:n,i),m,j)
!                                temp=distance_tot(L)
!                                if (temp>mini) then
!                                        indice1=j
!                                        indice2=m
!                                        mini=temp
!                                end if
!                                L(1:n,i)=dummy
!                        end do
!                end do
!                call swap(L(:,i),indice1,indice2)
!                write (*,*) 'colonne=',i,'max atteint:',mini                
!                end do
!        g=g+1
!        write (*,*) 'boucle it=',g,'max atteint:',mini
!end do
end subroutine laths
 
subroutine swap(vector,i,j)
 use i_Parametres
 implicit none
integer:: vector(:)
 integer:: i,j,dummy
 dummy=vector(i)
 vector(i)=vector(j)
 vector(j)=dummy
 end subroutine swap
 
 function distance_euclide(vector1,vector2)
 use i_Parametres
 implicit none 
 integer::vector1(:),vector2(:),distance_euclide
 integer:: i
 distance_euclide=0
do i =1,size(vector1)
        distance_euclide=distance_euclide+(vector1(i)-vector2(i))*(vector1(i)-vector2(i))
end do
        
end function distance_euclide

function distance_tot(LHS,nb)
use i_Parametres
implicit none 
integer::LHS(:,:),distance_tot,tmp,nb
integer:: m,n,i,j
m=size(LHS(:,1))
n=size(LHS(1,:))
distance_tot=distance_euclide(LHS(1,:),LHS(2,:))
nb=1
do i=1,m-1
        do j=i+1,m
        tmp=distance_euclide(LHS(i,:),LHS(j,:))
        if (tmp<distance_tot) then
        	distance_tot=tmp
        	nb=1
        else if (distance_tot==tmp) then
        	nb=nb+1
        end if
        end do
end do
end function distance_tot

subroutine shuffle(vector)
use i_Parametres
use rand_num
implicit none
integer::vector(:),n,i,j,k
integer,allocatable :: temp(:)
n=size(vector)
allocate (temp(n))
temp=vector
j=0
do while (j<n )
        i=nint(random(1._rdp,real(n-j,kind=rdp)))
        k=temp(i)
        temp(1:n-j)=pack(temp,temp /= k)
        vector(j+1)=k
        j=j+1
end do
deallocate (temp)
end subroutine shuffle
