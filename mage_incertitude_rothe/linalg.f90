! module pour toutes les fonctionnalite liees a l algebre lineaire
module linalg
use i_Parametres
use i_Consigne
implicit none

contains

! resolution de systeme par le pivot de gauss
function resolution_syst(A,B)
use i_Parametres 
implicit none 
integer::n
real(kind=rdp):: A(:,:),B(:)
real(kind=rdp)::resolution_syst(size(A(:,1))),test(size(A(:,1))+1,size(A(:,1)))
n=size(A(:,1))
! remplissage des donnnees
test(1:n,1:n)=A
test(n+1,:)=B
!pivot de gauss
call pivot_gauss(test)
! remplissage des resultats
resolution_syst=test(n+1,:)
end function resolution_syst


! inversion pour une matrice symetrique definie positive par cholesky
function inversion_cholesky(A)
use i_Parametres
implicit none
real(kind=rdp)::A(:,:)
real(kind=rdp) :: inversion_cholesky(size(A(1,:)),size(A(1,:))) ,s(size(A(1,:))), tableau_test(size(A(1,:)),size(A(1,:)))
integer ::i,j,n
n=size(A(1,:))
tableau_test(1:n,1:n)=A
s=0._rdp
! decomposition de cholesky (matrice L avec A=tLL)
call Cholesky(tableau_test(1:n,1:n))
! transposition de L 
do j=1,n
     do i=1,n 
 tableau_test(i,j)=tableau_test(j,i)
     end do 
 end do
 ! sauvegarde de l inverse de la diagonale de L
do i=1,n
     s(i)=1._rdp/tableau_test(i,i)
end do
! calcul de l inverse de L
do j=n,1,-1
     do i=j,1,-1
        if (i==j) then 
        inversion_cholesky(i,j) = s(i) - dot_product(tableau_test(i,i+1:n),inversion_cholesky(i+1:n,j)) 
        else
        inversion_cholesky(i,j) =  - dot_product(tableau_test(i,i+1:n),inversion_cholesky(i+1:n,j))
        end if
        inversion_cholesky(i,j) =  inversion_cholesky(i,j)/tableau_test(i,i) 
        inversion_cholesky(j,i) = inversion_cholesky(i,j) 
    end do
end do

end function inversion_cholesky


! inversion de cholesky et calcul du determinant de la matrice en prime
function inversion_cholesky_det(A,det)
use i_Parametres
implicit none
real(kind=rdp)::A(:,:),det
real(kind=rdp):: inversion_cholesky_det(size(A(1,:)),size(A(1,:))),s(size(A(1,:))),  tableau_test(size(A(1,:)),size(A(1,:)))
integer ::i,j,n
n=size(A(1,:))
tableau_test(1:n,1:n)=A
s=0._rdp
! decomposition de cholesky
call Cholesky(tableau_test(1:n,1:n))
!initialisation du determinant 
det=1._rdp
!transposition de L
do j=1,n
         do i=1,n 
 tableau_test(i,j)=tableau_test(j,i)
         end do 
 end do
 ! calcul du determinant
do i=1,n
        s(i)=1._rdp/tableau_test(i,i)
        det=det*tableau_test(i,i)**2
end do
! calcul de l inverse
do j=n,1,-1
    do i=j,1,-1
        if(i==j) then
        inversion_cholesky_det(i,j) = s(i) - dot_product(tableau_test(i,i+1:n),inversion_cholesky_det(i+1:n,j)) 
        else
        inversion_cholesky_det(i,j) = - dot_product(tableau_test(i,i+1:n),inversion_cholesky_det(i+1:n,j))         
        end if
        inversion_cholesky_det(i,j) =  inversion_cholesky_det(i,j)/tableau_test(i,i) 
        inversion_cholesky_det(j,i) = inversion_cholesky_det(i,j) 
    end do
end do
end function inversion_cholesky_det


! resolution de systeme pour une matrice triangulaire inferieure (AX=B)
function resolution_forward(A,B)
use i_Parametres 
implicit none 
integer::i,j,n
real(kind=rdp):: A(:,:),B(:)
real(kind=rdp)::resolution_forward(size(A(:,1)))
n=size(A(:,1))
resolution_forward=0._rdp
do j=1,n
resolution_forward(j)=B(j)/A(j,j)
        do i=j+1,n
         B(i)=B(i)-A(i,j)*resolution_forward(j)
        end do
end do
end function resolution_forward

! resolution de systeme pour une matrice triangulaire superieure
function resolution_backward(A,B)
use i_Parametres 
implicit none 
integer::i,j,n
real(kind=rdp):: A(:,:),B(:)
real(kind=rdp)::resolution_backward(size(A(:,1)))
n=size(A(:,1))
do j=n,1,-1
resolution_backward(j)=B(j)/A(j,j)
        do i=1,j-1
        B(i)=B(i)-A(i,j)*resolution_backward(j)
        end do
end do
end function resolution_backward


! resolution de systeme  AX=B via la methode de cholesky
function resolution_cholesky(A,B)
use i_Parametres 
implicit none 
integer::n
real(kind=rdp):: A(:,:),B(:)
real(kind=rdp) ::resolution_cholesky(size(A(:,1))),test(size(A(:,1)),size(A(:,1))), &
& test_b(size(A(:,1))),res(size(A(:,1)))
! saisie des donnees
n=size(A(:,1))
test(1:n,1:n)=A
test_b=B
! decomposition de cholesky (A=tLL)
call Cholesky(test)
! resolution de LY=B
res=resolution_forward(test,test_b)
! resolution de tLX=Y donc de AX=B
resolution_cholesky=resolution_backward(transpose(test),res)
end function resolution_cholesky

! calcul seulement du determinant de cholesky
function determinant_cholesky(A)
use i_Parametres
implicit none
integer:: i,n
real(kind=rdp)::A(:,:),determinant_cholesky
real(kind=rdp)::mat(size(A(:,1)),size(A(:,1)))
n=size(A(:,1))
mat=A
determinant_cholesky=1._rdp
! decomposition cholesky
call Cholesky(mat)
!calcul du determinant
do i=1,n
        determinant_cholesky=determinant_cholesky*(mat(i,i)**2)
end do 
end function determinant_cholesky


! calcul de la distance euclidienne entre deux vecteurs
function dist_euclide(vector1,vector2)
use i_Parametres
implicit none
real(kind=rdp) :: vector1(:),vector2(:),dist_euclide
dist_euclide=sum((vector1-vector2)*(vector1-vector2))
end function


! routine faisant le pivot de gauss
subroutine pivot_gauss(A)
use i_Parametres
implicit none
integer:: i,j,n,m,indice
real(kind=rdp)::A(:,:)
real(kind=rdp)::dummy(size(A(:,1)))
m=size(A(1,:))
n=size(A(:,1))
do i=1,m
        indice=maxloc(A(i,i:m),dim=1)+i-1
        dummy=A(:,indice)
        A(:,indice)=A(:,i)
        A(:,i)=dummy
        A(:,i)=A(:,i)/A(i,i)
                        !omp parallel  DO 
                                do j=1,m
                                        if (i/=j) A(:,j)=A(:,j)-A(i,j)*A(:,i)
                                end do        
                        !omp END  parallel DO
end do
end subroutine pivot_gauss




! decomposition de cholesky
subroutine Cholesky(A)
use i_Parametres
implicit none
integer:: j,n
real(kind=rdp) :: A(:,:)
n=size(A(:,1))
do j = 1,n
! calcul termes diagonaux
A(j,j) = sqrt(A(j,j) - dot_product(A(j,1:j-1),A(j,1:j-1)))
! calcul terme non diagonaux
if (j < n) A(j+1:n,j) = (A(j+1:n,j) - matmul(A(j+1:n,1:j-1),A(j,1:j-1))) / A(j,j)
end do 
end subroutine Cholesky
        
! affiche une matrice sur le terminal
subroutine Affiche_mat(A)
use i_Parametres
implicit none
integer:: i,j,n,m
real(kind=rdp) :: A(:,:)
n=size(A(:,1))
m=size(A(1,:))
do i=1,n
write(*,*) (real(A(i,j),kind=4),j=1,m)
end do
end subroutine Affiche_mat


! calcul du detreminant avec le pivot de gauss
function determinant(A)
use i_Parametres
implicit none
integer:: i,j,n,m,indice
real(kind=rdp)::A(:,:),determinant
real(kind=rdp),allocatable::dummy(:),mat(:,:)
m=size(A(1,:))
n=size(A(:,1))
allocate(dummy(n))
allocate(mat(n,n))
mat=A
determinant=1._rdp
do i=1,m
        indice=maxloc(mat(i,i:m),dim=1)+i-1
        dummy=mat(:,indice)
        mat(:,indice)=mat(:,i)
        mat(:,i)=dummy
        if (i/=indice) then
        determinant=-1._rdp*determinant*mat(i,i)
        else
        determinant=determinant*mat(i,i)
        end if
        mat(:,i)=mat(:,i)/mat(i,i)
        !omp parallel  DO 
                do j=1,m
                        if (i/=j) mat(:,j)=mat(:,j)-mat(i,j)*mat(:,i)
                end do        
        !omp END  parallel DO
end do
deallocate(dummy)
deallocate(mat)
end function determinant


! inverse A avec le pivot de gauss
function inversion(A)
use i_Parametres
implicit none
real(kind=rdp)::A(:,:)
real(kind=rdp),allocatable:: inversion(:,:),dummy(:,:),test(:)
integer ::i,j,n
n=size(A(1,:))
allocate (inversion(n,n))
allocate (dummy(2*n,n))
allocate (test(n))
dummy=0;
dummy(1:n,1:n)=A
do j=1,n
        do i=1,n
                if (i==j) then
                        inversion(i,j)=1._rdp
                else
                        inversion(i,j)=0._rdp
                end if
        end do
end do
dummy(n+1:2*n,1:n)=inversion
call pivot_gauss(dummy)
inversion=dummy(n+1:2*n,1:n)
deallocate(dummy)
deallocate(test)
end function inversion

! calcule la variance et la moyenne d un vecteur de valeurs
 subroutine moy_var2(mu,sigma,echantillon)
 use i_Parametres
 implicit none
 real(kind=rdp):: mu,sigma,echantillon(:)
 real(kind=rdp) ,allocatable:: temp(:)
 integer :: nb
 nb =size(echantillon)
 allocate(temp(nb))
 mu=sum(echantillon(:))/real(nb)
 temp=echantillon-mu
 temp=temp*temp
 sigma=sum(temp)/real(nb)
 end subroutine moy_var2

 
 ! execute le script de l acp mais version obsolete suaf pour montecarlo
subroutine ACP(echantillon)
use i_Parametres
implicit none
integer::i,j,nb_ech
real(kind=rdp) :: echantillon(:,:)
real(kind=rdp)  Matrice_test(size(echantillon(:,1)),nc+1)
nb_ech=size(echantillon(:,1))
Matrice_test(:,1:nc+1)=echantillon 
open(11,file='ACP.txt')
write(11,*) (i,i=1,nc+1)
do i=1,nb_ech
               write(11,*) (Matrice_test(i,j),j=1,nc+1)
end do
call execute_command_line("Rscript testR.R")
end subroutine ACP


! tests de fonctions produit scalaire et multiplication de matrice pour comparer fonction intrinseques
function scalar_prod(v1,v2)
use i_Parametres
implicit none
integer::n,i
real(kind=rdp)::v1(:),v2(:),scalar_prod
n=size(v1)
scalar_prod=0._rdp
do i=1,n
scalar_prod=scalar_prod+v1(i)*v2(i)
end do
end function scalar_prod

function mul_vec_mat(vec,mat,n)
use i_Parametres
implicit none
real(kind=rdp),intent(in) :: vec(:),mat(:,:)
integer:: m,i,j
integer, intent(in)::n
real(kind=rdp)::mul_vec_mat(n)
m=size(vec)
mul_vec_mat=0._rdp
do j=1,n
        do i=1,m
        mul_vec_mat(j)=mul_vec_mat(j)+mat(i,j)*vec(i)
        end do
end do
end function mul_vec_mat

end  module linalg
