!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - Irstea - 1989-2014                          #
!# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
!# Licence : propriétaire jusqu'à nouvel ordre                                #
!#                                                                            #
!#                                                                            #
!##############################################################################
module i_Parametres
!==============================================================================
!       Definition des dimensions maximales des tableaux, de constantes
!       numeriques et de divers constantes physiques et mathematiques.
!==============================================================================
   implicit none
! version
   character(len=21), parameter :: version_string = '1.0.1_d du 20/08/2014'
! precision des reels (kind)      4:simple 8:double
   integer, parameter :: rdp = kind(1.d0)
   integer, parameter :: sp = kind(1.)
! -- dimensions de tableaux
   integer, parameter :: np = 100, maxsim = 200
! nb max de sections ou points de calcul
   integer, parameter :: issup=15001
! nb max de biefs ou branches
   integer, parameter :: ibsup=150

end module i_Parametres
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
module i_Annealing  ! données pour le recuit simulé
   use i_Parametres, only : rdp, np
   implicit none
   integer :: nequil  ! si nequil = 0 on réduit la température après chaque évaluation
                      ! sinon nequil est le nombre d'évaluations sans amélioration
                      ! de l'optimum nécessaires avant une réduction de la température
   real(kind=rdp) :: t0, temperature
   real(kind=rdp) :: dth ! taux de variation de la température

   real(kind=rdp) :: fk_ini     !valeur initiale de la fonction-coût
   real(kind=rdp) :: fk_opt     !meilleure valeur de la fonction-coût
   integer :: nb_eval   !nombre d'évaluation de la fonction-coût FUNK()
   integer :: nb_simul  !nombre de simulations réalisées (appels de solveur)
   integer :: iopt      !numéro de la meilleure évaluation de FUNK()

   real(kind=rdp) :: seuil_amelioration
   integer :: ndd !nombre de mesures utilisées
   integer :: FOtyp  !type de mesure utilisée pour la Fonction-Objectif
                     !0 = surface (défaut)
                     !1 = niveaux en L1
                     !2 = niveaux en L^infini
                     !3 = largeurs
   character :: unite*3
   real(kind=rdp) :: sens ! 1 pour minimisation et -1 pour maximisation
   real(kind=rdp) :: Surf1, Surf2
   real(kind=rdp) :: cpu_externe
end module i_Annealing
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
module i_Consigne
   use i_Parametres, only : rdp, np
   real(kind=rdp) :: Wmin(np)  !table des valeurs minimales par variable
   real(kind=rdp) :: Wmax(np)  !table des valeurs maximales par variable
   integer :: inhib(np)  ! indicateur d'inhibition des variables d'intervalle nul
   real(kind=rdp) :: W_ini(np,2) !table des valeurs initiales des variables à optimiser
   integer :: nc                !nombre de consignes = dimension du pb
   integer :: nst  !nombre de consignes Strickler : nst < ou = nc
   integer :: ib(np) !n° du bief du tronçon
   real(kind=rdp) :: xmin(np), xmax(np) !pk début et fin du tronçon
   real(kind=rdp) :: eps !precision de la ligne d'eau == arrondi du calcul de l'écart mesure-observation

   character :: modele*60, basename*8, solveur*40
   integer ::  norme
   integer :: date_fin !date de fin de simulation en minutes à rechercher
                       !pour vérifier que la simulation s'est bien terminée
   real(kind=rdp) :: scale !facteur d'echelle pour les calculs de consigne

   real(kind=rdp) :: Wtest(np,2,10) !table des consignes à tester a priori
   integer :: ntest  !nombre de consignes à tester a priori

   type profil
      real(kind=rdp) :: x, zmoy
      integer :: nbval
      real(kind=rdp) :: y(24), z(24)
   end type profil
   type (profil) :: datageo(1000)
   integer :: nbpro
   real(kind=rdp) :: deltaZ
   character(len=80) :: titre
   real(kind=rdp) :: Coeff_Penalisation

   !calage
   type data_obs
      integer :: ib          !numéro du bief
      real(kind=rdp) :: x    !pk de la laisse de crue
      real(kind=rdp) :: z    !niveau observé
   end type data_obs
   type (data_obs) :: laisse_crue(10) , Q_rep(10)
   integer :: nb_obs_z, nb_obs_q         !nombre effectif de laisses de crue
   integer :: date_fin_calage
   real(kind=rdp) :: erreur_calage, cz=1._rdp, cq=1._rdp

   !Glue
   integer :: nbok  ! nb d'évaluations à atteindre avec calage OK.

end module i_Consigne
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
module Geometrie_Section
   use i_parametres, only: ibsup, issup, rdp
   integer :: is1(ibsup) ! is1(ib) = indice de la premiere section du bief de
                         !           rang de calcul ib
   integer :: is2(ibsup) ! is2(ib) = indice de la derniere section du bief de
                         !           rang de calcul ib
   integer :: ibu(ibsup) ! ibu(kb) = rang de calcul du bief de numero KB dans .TAL

   real(kind=rdp) :: xgeo(issup)
end module Geometrie_Section
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
module Dim_Reelles
   implicit none

   integer :: ISMax    ! nb de sections ou points de calcul
   integer :: IBMax    ! nb de biefs ou branches du réseau
end module Dim_Reelles
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!


program mage_SA
!==============================================================================
!            OPTIMISATION
!
! 16/09/2005 : simplification des contraintes : la même pour les 2 versions d'une composante
!              wmin et wmax ont donc une seule dimension.
!==============================================================================
   use omp_lib
   use i_Parametres 
   use i_Consigne  
   use i_Annealing, only : iopt, nb_eval, nb_simul, fk_ini, fk_opt, unite, sens, FOtyp, &
                           cpu_externe
   use Geometrie_Section, only : xgeo
   implicit none
!Variables locales
   real(kind=rdp) :: w_opt(np,2), debut, fin
   integer :: i, n, is
   integer values(8) ! valeurs renvoyees par la fonction date_and_time()
   character :: zone*5, ddate*8, time*10, tmp*15, tmp2*10
   real(kind=rdp) :: foo0, foo1, foo2, C_Plus, ww(np), zmax(issup), Lmaj(issup)
   real(kind=rdp) :: Xx(issup)
   real(kind=rdp) :: f_best
   real(kind=rdp) :: Z0_max(10), Z1_max(10), Z2_max(10), err0, err1, err2
   character(len=1) :: tag, tag0
   !variable pour gérer les arguments de la ligne de commande
   character(len=60) :: argmnt
   integer :: iarg
   logical :: bexist
    
   character(len=20) :: outdir='.' !nom du sous-dossier pour le stockage des fichiers de résultats, 
   character(len=20) :: datafile   !nom du fichier de paramètres
   
   character(len=90), parameter :: Garantie1 = 'Mage_SA est fourni sans garantie '&
                           //'d''aucune sorte dans les limites prévues par la loi'
   character(len=80), parameter :: Garantie2 = 'Les résultats obtenus sont de '&
                                  //'la seule responsabilité de l''utilisateur'
   character(len=43), parameter :: entame = 'Mage_SA (Analyse de Sensibilité), version  '

! types dérivés
   type Mage_results
      logical :: calcul_OK
      integer :: date_fin
      character (len=3) :: type_resultat=''
      real(kind=rdp) :: pm(issup), zfd(issup), largeur_totale(issup)
      real(kind=rdp) :: largeur_mineur(issup),Z_max(issup)
      real(kind=rdp) :: qtot(issup), qfp(issup)  !fp : flood plaine
   end type Mage_results
   type (Mage_results) :: resultats
   
   procedure(flooded_Area), pointer :: working_Area => null()
   procedure(modify_1a), pointer :: modify => null()
   
!------------------------------------------------------------------------------
   iarg = 1 ; call get_command_argument(iarg,argmnt)
   do
      if (argmnt(1:2) == '--') then
         write(6,'(a)') entame//version_string
         write(6,'(a)') 'STOP : argument invalide : '//trim(argmnt)
         stop
      else if (argmnt(1:2) == '-v') then
         call print_Version(6)  ;  stop
      else if (argmnt(1:2) == '-h') then 
         write(6,'(a)') entame//version_string
         call print_Help()  ;  stop
      else if (argmnt(1:3) == '-g=' ) then 
         !création d'un fichier de paramètres avec les valeurs par défaut
         write(6,'(a)') entame//version_string
         write(6,'(a)') 'Création d''un fichier de paramètres avec les valeurs par défaut'
         call create_DATfile(trim(argmnt(4:)),.false.)
         stop
      else if (argmnt(1:3) == '-G=') then 
         !création d'un fichier de paramètres _commenté_ avec les valeurs par défaut
         write(6,'(a)') entame//version_string
         write(6,'(a)') 'Création d''un fichier de paramètres commenté avec les valeurs par défaut'
         call create_DATfile(trim(argmnt(4:)),.true.)
         stop
      else if (argmnt(1:3) == '-o=') then 
         !répertoire de stockage des fichiers de résultats
         outdir = trim(argmnt(4:))
         inquire(file='./'//trim(outdir),exist=bexist)
         if (.not.bexist) call execute_command_line('mkdir ./'//trim(outdir))
         iarg = iarg+1 ; call get_command_argument(iarg,argmnt) ;
      else if (argmnt(1:1) == '-' ) then
         write(6,'(a)') entame//version_string
         write(6,'(a)') 'STOP : argument de la ligne de commande inconnu : '//trim(argmnt)
         stop
      else if (len_trim(argmnt) > 0) then  !fichier de paramètres
         datafile = trim(argmnt)
         write(6,'(a)') entame//version_string
         write(6,'(a)') ' '
         exit
      else
         write(6,'(a)') entame//version_string
         stop ': Il manque le nom du fichier de paramètres'
      endif
   enddo
   

   ! initialisation
   call cpu_time(debut) ; cpu_externe = 0._rdp
   call date_and_time(ddate,time,zone,values)
   tmp = ddate//'-'//time(1:6)
   open(unit=8, file=trim(outdir)//'/sortie_'//tmp//'.txt', form='formatted', status='unknown')
   
   call init(datafile)  !initialisation
   
   open(unit=7, file=trim(outdir)//'/stricklers_'//tmp//'.csv', form='formatted', status='unknown')
   if (deltaZ > 0._rdp) open(unit=9, file=trim(outdir)//'/geometrie_'//tmp//'.csv', form='formatted', status='unknown')
         
   

   if (FOtyp < 2) then       ! écart maximal sur des surfaces
      sens = -1._rdp
      if (ntest > 0) then
         f_best = 0.
         do i = 1, ntest
            write(*,'(a,i2)') 'Test numero ',i
            w_opt(:,:) = wtest(:,:,i)
            fk_opt = Foo_Ecart(w_opt,C_Plus) ; tmp = affiche(fk_opt,unite)
            if (C_Plus < 0.001) then
               tmp2 = ' calage OK'
            else
               tmp2 = ' calage KO'
            endif
            write(*,'(a,i2,2a)') ' Optimum a priori numero ',i,' = ',tmp//tmp2
            write(8,'(a,i2,2a)') ' Optimum a priori numero ',i,' = ',tmp//tmp2
         enddo
      endif
      if (eps > 0._rdp) then
         modify => modify_2a
      else
         modify => modify_2c
      endif
      call simulated_annealing(W_ini,W_opt,Foo_Ecart)    !algo du recuit simulé
      
   else if (FOtyp < 4) then  ! surface minimale
      sens = +1._rdp
      if (eps > 0._rdp) then
         modify => modify_1a
      else
         modify => modify_1b
      endif
      call simulated_annealing(W_ini,W_opt,Foo_MinMax)    !algo du recuit simulé
      
   else if (FOtyp < 6) then  ! surface maximale
      sens = -1._rdp
      if (eps > 0._rdp) then
         modify => modify_1a
      else
         modify => modify_1b
      endif
      call simulated_annealing(W_ini,W_opt,Foo_MinMax)    !algo du recuit simulé
      
   else if (FOtyp < 8) then  !calage
      sens = +1._rdp
      if (eps > 0._rdp) then
         modify => modify_1a
      else
         modify => modify_1b
      endif
      call simulated_annealing(W_ini,W_opt,Foo_Calage)    !algo du recuit simulé
      
   else if (FOtyp == 8) then
      call Monte_Carlo()                                  !Monte-Carlo
   else
      stop '>>>> Erreur : FOtype inconnu <<<<'
   endif

   !Présentation des résultats
   if (FOtyp < 2) then
      tag = '5' ; tag0 = '4'  !solution moyenne
      ww = 0.5_rdp * (wmax+wmin) ; foo0 = working_Area(ww,tag,resultats)* 0.0001_rdp
      err0 = calage(ww,tag0,resultats)
      do n = 1, nb_obs_z
         is = iSect(laisse_crue(n)%ib, laisse_crue(n)%x, 0.01_rdp)
         Z0_max(n) = resultats%Z_max(is)
      enddo
      tag = '2' ; tag0 = '0'  !solution N°1      
      ww = w_opt(:,1) ; foo1 = working_Area(ww,tag,resultats)* 0.0001_rdp
      err1 = calage(ww,tag0,resultats)
      do n = 1, nb_obs_z
         is = iSect(laisse_crue(n)%ib, laisse_crue(n)%x, 0.01_rdp)
         Z1_max(n) = resultats%Z_max(is)
      enddo
      tag = '3' ; tag0 = '1'  !solution N°2      
      ww = w_opt(:,2) ; foo2 = working_Area(ww,tag,resultats)* 0.0001_rdp
      err2 = calage(ww,tag0,resultats)
      do n = 1, nb_obs_z
         is = iSect(laisse_crue(n)%ib, laisse_crue(n)%x, 0.01_rdp)
         Z2_max(n) = resultats%Z_max(is)
      enddo
      write(*,'(a)') ' Solution : '
      write(*,'(12x,f10.4,a,7x,f10.4,a)') foo1,' ha',foo2,' ha'
      write(*,'(10x,4f10.3)') (w_opt(2*i+1,1),w_opt(2*i+2,1),w_opt(2*i+1,2),w_opt(2*i+2,2),i=0,nst/2-1)
      !write(*,'(10x,2f10.3)') (w_opt(i,1),w_opt(i,2),i=nst+1,nc)
      write(*,'(a)') ' Consignes geometrie : voir le fichier sortie'
      write(8,'(a)') ' Solution : '
      write(8,'(12x,f10.4,a,7x,f10.4,a)') foo1,' ha',foo2,' ha'
      write(8,'(a)') ' Coefficients de Strickler'
      write(8,'(10x,4f10.3)') (w_opt(2*i+1,1),w_opt(2*i+2,1),w_opt(2*i+1,2),w_opt(2*i+2,2),i=0,nst/2-1)
      write(8,'(a)') ' Perturbations de la cote du fond de la géométrie'
      do i = nst+1, nc
         write(8,'(a,f10.3,f10.3,10x,f10.3)') 'Pm = ',xgeo(i-nst),w_opt(i,1),w_opt(i,2)
      enddo
      tmp = affiche(fk_opt,unite)
      write(*,'(3a,i5,a,2(i5,a))') ' Optimum = ',tmp, ' a l''evaluation ',iopt,' en ',nb_eval, &
                                  ' evaluations et ',nb_simul,' simulations'
      write(8,'(3a,i5,a,2(i5,a))') ' Optimum = ',tmp, ' a l''evaluation ',iopt,' en ',nb_eval, &
                                  ' evaluations et ',nb_simul,' simulations'

      tmp = affiche(100._rdp*abs((foo1-foo2)/foo0),' %'//' ')
      write(*,'(a,f10.4,a)') ' Surface minimale = ',min(foo1,foo2),' ha'
      write(*,'(a,f10.4,a)') ' Surface maximale = ',max(foo1,foo2),' ha'
      write(*,'(2a)') ' Ecart / surface initiale = ',tmp
      write(8,'(a,f10.4,a)') ' Surface minimale = ',min(foo1,foo2),' ha'
      write(8,'(a,f10.4,a)') ' Surface maximale = ',max(foo1,foo2),' ha'
      write(8,'(2a)') ' Ecart / surface initiale = ',tmp

      write(*,'(a)') ' Verification du calage'
      write(*,'(a,3(f10.4,a))') ' Observations      ',foo1,' ha      ',foo0,' ha      ',foo2,' ha'
      write(8,'(a)') ' Verification du calage'
      write(8,'(a,3(f10.4,a))') ' Observations      ',foo1,' ha      ',foo0,' ha      ',foo2,' ha'
      do n = 1, nb_obs_z
         write(*,'(4(f10.3,a))') laisse_crue(n)%z,'         ', Z1_max(n),'         ', Z0_max(n),'         ', Z2_max(n)
         write(8,'(4(f10.3,a))') laisse_crue(n)%z,'         ', Z1_max(n),'         ', Z0_max(n),'         ', Z2_max(n)
      enddo
      write(*,'(18x,f10.4,2(9x,f10.4))') err1, err0, err2
      write(8,'(18x,f10.4,2(9x,f10.4))') err1, err0, err2
   else if (FOtyp < 6) then
      tag = '2' ; tag0 = '0'  !solution moyenne
      ww = 0.5_rdp * (wmax+wmin) ; foo0 = working_Area(ww,tag,resultats)* 0.0001_rdp
      err0 = calage(ww,tag0,resultats)
      do n = 1, nb_obs_z
         is = iSect(laisse_crue(n)%ib, laisse_crue(n)%x, 0.01_rdp)
         Z0_max(n) = resultats%Z_max(is)
      enddo
      tag = '3' ; tag0 = '1'  !solution
      ww = w_opt(:,1) ; foo1 = working_Area(ww,tag,resultats)* 0.0001_rdp
      err1 = calage(ww,tag0,resultats)
      do n = 1, nb_obs_z
         is = iSect(laisse_crue(n)%ib, laisse_crue(n)%x, 0.01_rdp)
         Z1_max(n) = resultats%Z_max(is)
      enddo
      write(*,'(a)') ' Solution : '
      write(*,'(12x,f10.4,a)') foo1,' ha'
      write(*,'(10x,2f10.3)') (w_opt(2*i+1,1),w_opt(2*i+2,1),i=0,nst/2-1)
      write(*,'(a)') ' Consignes geometrie : voir le fichier sortie'
      write(8,'(a)') ' Solution : '
      write(8,'(12x,f10.4,a)') foo1,' ha'
      write(8,'(a)') ' Coefficients de Strickler'
      write(8,'(10x,2f10.3)') (w_opt(2*i+1,1),w_opt(2*i+2,1),i=0,nst/2-1)
      write(8,'(a)') ' Perturbations de la cote du fond de la géométrie'
      do i = nst+1, nc
         write(8,'(a,f10.3,f10.3)') 'Pm = ',xgeo(i-nst),w_opt(i,1)
      enddo
      tmp = affiche(fk_opt,unite)
      write(*,'(3a,i5,a,2(i5,a))') ' Optimum = ',tmp, ' a l''evaluation ',iopt,' en ',nb_eval, &
                                  ' evaluations et ',nb_simul,' simulations'
      write(8,'(3a,i5,a,2(i5,a))') ' Optimum = ',tmp, ' a l''evaluation ',iopt,' en ',nb_eval, &
                                  ' evaluations et ',nb_simul,' simulations'

      tmp = affiche(100._rdp*abs((foo1-foo0)/foo0),' %'//' ')
      if (FOtyp == 2 .or. FOtyp == 3) write(*,'(a,f10.4,a)') ' Surface minimale = ',foo1,' ha'
      if (FOtyp == 4 .or. FOtyp == 5) write(*,'(a,f10.4,a)') ' Surface maximale = ',foo1,' ha'
      write(*,'(2a)') ' Ecart / surface initiale = ',tmp
      if (FOtyp == 2 .or. FOtyp == 3) write(8,'(a,f10.4,a)') ' Surface minimale = ',foo1,' ha'
      if (FOtyp == 4 .or. FOtyp == 5) write(8,'(a,f10.4,a)') ' Surface maximale = ',foo1,' ha'
      write(8,'(2a)') ' Ecart / surface initiale = ',tmp

      write(*,'(a)') ' Verification du calage'
      write(*,'(a,3(f10.4,a))') ' Observations      ',foo1,' ha      ',foo0,' ha'
      write(8,'(a)') ' Verification du calage'
      write(8,'(a,3(f10.4,a))') ' Observations      ',foo1,' ha      ',foo0,' ha'
      do n = 1, nb_obs_z
         write(*,'(3(f10.3,a))') laisse_crue(n)%z,'         ', Z1_max(n),'         ', Z0_max(n)
         write(8,'(3(f10.3,a))') laisse_crue(n)%z,'         ', Z1_max(n),'         ', Z0_max(n)
      enddo
      write(*,'(18x,f10.4,9x,f10.4)') err1, err0
      write(8,'(18x,f10.4,9x,f10.4)') err1, err0
   else if (FOtyp < 8) then
      write(*,'(a)') ' Solution : '
      write(*,'(10x,2f10.4)') (w_opt(2*i+1,1),w_opt(2*i+2,1),i=0,nst/2-1)
      write(*,'(10x,f10.4)') (w_opt(i,1),i=nst+1,nc)
      write(8,'(a)') ' Solution : '
      write(8,'(10x,2f10.6)') (w_opt(2*i+1,1),w_opt(2*i+2,1),i=0,nst/2-1)
      write(8,'(10x,f10.6)') (w_opt(i,1),i=nst+1,nc)
      
      ww = w_opt(:,1)
      err1 = calage(ww,'0',resultats)
      do n = 1, nb_obs_z
         is = iSect(laisse_crue(n)%ib, laisse_crue(n)%x, 0.01_rdp)
         Z1_max(n) = resultats%Z_max(is)
      enddo
      do n = 1, nb_obs_q
         is = iSect(Q_rep(n)%ib, Q_rep(n)%x, 0.01_rdp)
         if (FOtyp == 6) then
            Z2_max(n) = resultats%Qtot(is)
         else if (FOtyp ==7) then
            Z2_max(n) = 100._rdp*resultats%Qfp(is)/resultats%Qtot(is)
         else
            stop 445
         endif
      enddo
      do n = 1, max(nb_obs_z,nb_obs_q)
         if (n <= min(nb_obs_z,nb_obs_q)) then
            write(6,'((a3,i2,3f10.3),(a4,i2,3f10.3))') &
                     'Z ',laisse_crue(n)%ib, laisse_crue(n)%x, laisse_crue(n)%z, z1_max(n), &
                   '  Q ',Q_rep(n)%ib, Q_rep(n)%x, Q_rep(n)%z, z2_max(n)
         else if (n <= nb_obs_q) then
            write(6,'(35x,a4,i2,3f10.3)') '  Q ',Q_rep(n)%ib, Q_rep(n)%x, Q_rep(n)%z, z2_max(n)
         else if (n <= nb_obs_z) then
            write(6,'(a3,i2,3f10.3)') 'Z ',laisse_crue(n)%ib, laisse_crue(n)%x, laisse_crue(n)%z, z1_max(n)
         endif                                   
      enddo
      tmp = affiche(fk_opt,unite)
      write(*,'(3a,i4,a,2(i4,a))') ' Optimum = ',tmp, ' a l''evaluation ',iopt,' en ',nb_eval, &
                                  ' evaluations et ',nb_simul,' simulations'
      write(8,'(3a,i4,a,2(i4,a))') ' Optimum = ',tmp, ' a l''evaluation ',iopt,' en ',nb_eval, &
                                  ' evaluations et ',nb_simul,' simulations'
   else
      continue
   endif
   write(*,*)

   call cpu_time(fin)
   call M4b(debut,fin,values)
   write(*,*)
   close(7)
   close(8)
   
   contains
   

function Foo_Ecart(w, C_Plus)
!==============================================================================
!       fonction à minimiser utilisée par SIMULATED_ANNEALING
!                utilise flooded_Area et ajoute les contraintes
!       cette fonction calcule un écart entre 2 simulations
!       plusieurs mesures de cet écart sont disponibles :
!       - différence relative des surfaces inondées (type 0)
!       - écart moyen entre les lignes d'eau enveloppe (type 1)
!       - écart maximal entre les lignes d'eau enveloppe (type 2)
!       - écart moyen entre les largeurs du lit majeur inondé (type 3)
!
!       La valeur de référence correspond toujours à la consigne médiane
!
!       Entrée :
!               W = consigne (vecteur)
!       Sortie :
!               Foo_Ecart : valeur de l'écart
!               C_Plus = poids des contraintes ; o si satisfaites
!
!---Historique
!      04/03/2004 : reprise depuis PEGASE (1994)
!      08/06/2009 : mise à jour de la description
!==============================================================================
   use i_Parametres, only : rdp, np, issup
   use i_Annealing, only : fk_opt, fk_ini, sens, Surf1, Surf2
   use i_Consigne, only : w_ini, wmax, wmin, Coeff_Penalisation
   use dim_reelles, only : ismax, ibmax
   use geometrie_section, only : is1, is2, xgeo
   implicit none
!prototype
   real(kind=rdp), intent(in) :: w(np,2)
   real(kind=rdp), intent(out) :: C_Plus
   real(kind=rdp) :: Foo_Ecart
!variables locales
   real(kind=rdp) :: funk, tmp
   integer :: i, j, k, ib
   real(kind=rdp) :: ww(np), foo1, foo2, ecart, long_net, h1, h2, hmax, hmin
   integer, save :: nap=0
   real(kind=rdp), save :: foo_ini
   character(len=2) :: OK
   character(len=1) :: tag
   type (Mage_results) :: resultats_1, resultats_2
!-----------------------------------------------------------------------
   if (nap == 0) then
      write(*,*) 'Simulation initiale avec les consignes moyennes'
      write(8,*) 'Simulation initiale avec les consignes moyennes'
      nap = 1
      ww = (wmax + wmin)/2._rdp
      foo_ini = working_Area(ww,'2',resultats)
      OK = 'KO'  ;  if (calage_ok(ww,'0') < 0.0001_rdp) OK = 'OK'
      write(*,'(a,f10.4,2a)') ' --> Surface initiale : ',foo_ini*0.0001,' ha, Calage : ',OK
      write(8,'(a,f10.4,2a)') ' --> Surface initiale : ',foo_ini*0.0001,' ha, Calage : ',OK
   endif

   !$omp parallel 
   !$omp single private(tag,ww)
      !$omp task
         tag = '0' !; print*,' Appel calage_ok tag=',tag
         ww(:) = w(:,1) ; foo1 = calage_ok(ww,tag)
      !$omp end task
      !$omp task
         tag = '1' !; print*,' Appel calage_ok tag=',tag
         ww(:) = w(:,2) ; foo2 = calage_ok(ww,tag)
      !$omp end task
   !$omp end single
   !$omp end parallel
   C_Plus = distance_Domaine(w) + foo1 + foo2
   foo1 = 1.e+79_rdp ; foo2 = foo1
   
   if (Coeff_Penalisation*C_Plus < 1._rdp) then
      !$omp parallel private(tag,ww,resultats)
      !$omp single
         !$omp task
            tag = '2' !; print*,' Appel working_Area() tag=',tag
            ww = w(:,1) ; foo1 = working_Area(ww,tag,resultats)  ;  Surf1 = foo1*0.0001_rdp
         !$omp end task
         !$omp task
            tag = '3' !; print*,' Appel working_Area() tag=',tag
            ww = w(:,2) ; foo2 = working_Area(ww,tag,resultats)  ;  Surf2 = foo2*0.0001_rdp
         !$omp end task
      !$omp end single
      !$omp end parallel

      !write(6,*) 'Surfaces : ',Surf1, Surf2
      if (abs(foo1) > 1.e+29_rdp .OR. abs(foo2) > 1.e+29_rdp) then  
         funk = sens * 9.999e+99_rdp          !échec d'au moins une des simulations
      else
         ecart = 100._rdp*abs((foo1-foo2)/foo_ini)     !ecart en %
         funk = ecart + sens*Coeff_Penalisation*C_Plus
      endif
   else  !inutile de faire les simulations car les contraintes ne sont pas satisfaites
      !print*,'Contraintes non satisfaites'
      funk = sens * Coeff_Penalisation*C_Plus
      Surf1 = -1._rdp  ;  Surf2 = -1._rdp  ! valeurs arbitraires pour les distinguer dans le fichier csv
   endif
   Foo_Ecart = funk
!------------------------------------------------------------------------------
end function Foo_Ecart


function Foo_Calage(w, C_Plus)
!==============================================================================
!       fonction à minimiser utilisée par SIMULATED_ANNEALING
!           Cette fonction est dédiée au calage
!
!       Entrée :
!               W = consigne (vecteur)
!       Sortie :
!               Foo_Calage : écart en mm par rapport aux observations
!               C_Plus = poids des constraintes ; = 0 si satisfaites
!
!---Historique
!      08/06/2009 : création
!==============================================================================
   use i_Parametres, only : rdp!, np, issup
   use i_Annealing, only : sens
   use i_Consigne, only : Coeff_Penalisation
   implicit none
!prototype
   real(kind=rdp), intent(in) :: w(np,2)
   real(kind=rdp), intent(out) :: C_Plus
   real(kind=rdp) :: Foo_Calage
!variables locales
   real(kind=rdp) :: ww(np), foo1, funk
   type (Mage_results) :: resultats
 
!-----------------------------------------------------------------------
   !poids des contraintes : domaine
   C_Plus = distance_Domaine(w)

   if (C_Plus < 0.0001_rdp) then
      ww = w(:,1) ; foo1 = 1000._rdp * calage(ww,'0',resultats)

      if (abs(foo1) > 1.e+29_rdp) then ! échec de la simulation
         funk = sens * 9.999e+99_rdp
      else
         funk = foo1 + sens*Coeff_Penalisation*C_Plus
      endif
   else  !inutile de faire les simulations car les contraintes ne sont pas satisfaites
      !funk = sens * 9.999e+99_rdp
      funk = sens*Coeff_Penalisation*C_Plus
   endif
   Foo_Calage = funk
end function Foo_Calage



function Foo_MinMax(w, C_Plus)
!==============================================================================
!       fonction à minimiser utilisée par SIMULATED_ANNEALING
!                utilise flooded_Area et ajoute les contraintes
!
!       Cette fonction est dédiée à la recherche d'une surface inondée
!       maximale ou minimale
!
!       Entrée :
!               W = consigne (vecteur)
!       Sortie :
!               Foo_MinMax : écart en % de la surface par rapport à la
!                            surface de référence (consigne médiane)
!               C_Plus = poids des contraintes ; = 0 si satisfaites
!
!---Historique
!      08/06/2009 : création
!==============================================================================
   use i_Parametres, only : rdp, np, issup
   use i_Annealing, only : fk_opt, fk_ini, sens, Surf1, Surf2
   use i_Consigne, only : w_ini, wmax, wmin, Coeff_Penalisation
   use dim_reelles, only : ismax, ibmax
   use geometrie_section, only : is1, is2
   implicit none
!prototype
   real(kind=rdp), intent(in) :: w(np,2)
   real(kind=rdp), intent(out) :: C_Plus
   real(kind=rdp) :: Foo_MinMax
!variables locales
   integer :: i, j, k
   real(kind=rdp) :: ww(np), foo1, funk, ecart
   integer, save :: nap=0
   real(kind=rdp), save :: foo_ini
   character(len=2) :: OK
   type (Mage_results) :: resultats

!-----------------------------------------------------------------------
   if (nap == 0) then
      write(*,*) 'Simulation initiale avec les consignes moyennes'
      write(8,*) 'Simulation initiale avec les consignes moyennes'
      nap = 1
      ww = (wmax + wmin)/2._rdp
      foo_ini = working_Area(ww,'2',resultats)
      OK = 'KO'  ;  if (calage_ok(ww,'0') < 0.0001_rdp) OK = 'OK'
      Surf1 = foo_ini*0.0001
      write(*,'(a,f10.4,2a)') ' Surface initiale : ',Surf1,' ha, Calage : ',OK
      write(8,'(a,f10.4,2a)') ' Surface initiale : ',Surf1,' ha, Calage : ',OK
      Surf2 = -1.
   endif

   !poids des contraintes : domaine + calage
   C_Plus = distance_Domaine(w) + calage_ok(w(:,1),'0')

   if (C_Plus < 0.0001_rdp) then
      ww = w(:,1) ; foo1 = working_Area(ww,'2',resultats)

      if (abs(foo1) > 1.e+29_rdp) then  !échec de la simulation
         funk = sens*9.999e+99_rdp
      else
         ecart = 100._rdp*(foo1-foo_ini)/foo_ini !ecart en %
         funk = ecart + sens*Coeff_Penalisation*C_Plus
         Surf1 = foo1*0.0001
      endif
   else  !inutile de faire les simulations car les contraintes ne sont pas satisfaites
      !funk = sens * 9.999e+99_rdp
      funk = sens*Coeff_Penalisation*C_Plus
      Surf1 = -1.  ! valeur arbitraire pour la distinguer dans le fichier csv
   endif
   Foo_MinMax = funk
end function Foo_MinMax



function flooded_Area(w,tag,resultats)
!==============================================================================
!             FONCTION OBJECTIF A MINIMISER
!
!---Historique
!      10/03/2004 : création
!==============================================================================
   use i_Parametres, only : rdp, np, maxsim, issup
   use i_Consigne, only : nc, modele, basename, ib, xmin, xmax, w_ini, nst, deltaZ, &
                          datageo, nbpro, profil
   use dim_reelles, only : ismax, ibmax
   use geometrie_section, only : is1, is2
   implicit none
!prototype
   real(kind=rdp), intent(in) :: w(*)
   real(kind=rdp) :: flooded_Area
   type (Mage_results), intent(out) :: resultats
   character(len=1) :: tag
!variables locales
   integer :: i, lu, is, kb, nbv
   integer :: lb
   character :: fmt*40, REPname*40
   real(kind=rdp) :: surface, surface_mineur, Lmaj1, Lmaj2
   type (profil) :: datamod(1000)
   
!--------------------------------------------------------------------------
   lb = len_trim(basename)
   flooded_Area = 1.e+40_rdp

   !--->exportation de la consigne
   open(newunit=lu,file=tag//'/'//basename(1:lb)//'.RUG',form='formatted',status='unknown')
   do i=1, nst/2
      write(lu,'(a,i3,6x,2f10.3,2f10.6)') 'K',ib(i),xmin(i),xmax(i),w(2*i-1),w(2*i)
   enddo
   close (lu)
   if (deltaZ > 0._rdp) then
      do n = 1, nbpro
         nbv = datageo(n)%nbval
         datamod(n)%nbval = nbv
         datamod(n)%x = datageo(n)%x
         datamod(n)%zmoy = datageo(n)%zmoy
         datamod(n)%y(1:nbv) = datageo(n)%y(1:nbv)
         datamod(n)%z(1:nbv) = datageo(n)%z(1:nbv)+w(nst+n) ! on ne met à jour que les Z !!!
      enddo
      open(newunit=lu,file=tag//'/'//basename(1:lb)//'.TAL',form='formatted',status='unknown')
      call exportTAL(lu,datamod)
      close (lu)
   endif
!--->exécution de MAGE
   REPname = basename(1:lb)//'.REP'
   call solveur_externe(REPname,tag,resultats)
!--->vérification que la simulation n'a pas échoué
   if (resultats%calcul_OK) then  !--->extraction des données de sortie et comparaison
      surface = 0._rdp
      do kb = 1, ibmax
         Lmaj1 = max(0._rdp,resultats%largeur_totale(is1(kb))-resultats%largeur_mineur(is1(kb)))
         do is = is1(kb)+1, is2(kb)
            Lmaj2 = max(0._rdp,resultats%largeur_totale(is)-resultats%largeur_mineur(is))
            surface = surface + 0.5_rdp*abs(resultats%pm(is)-resultats%pm(is-1))*(Lmaj2 + Lmaj1)
            Lmaj1 = Lmaj2
         enddo
      enddo
   
!      if (abs(surface) < 0.1_rdp) then
!         write(6,*) 'Pb surface : ',surface,ibmax,ismax,resultats%date_fin,is1(ibmax), is2(ibmax)
!         write(6,*) minval(resultats%largeur_totale(1:ismax)),maxval(resultats%largeur_totale(1:ismax))
!         write(6,*) minval(resultats%largeur_mineur(1:ismax)),maxval(resultats%largeur_mineur(1:ismax))
!         write(6,*) minval(resultats%pm(1:ismax)),maxval(resultats%pm(1:ismax))
!      endif
      
      flooded_Area = surface  !surface en m²

   else
      !échec de la simulation => foo = infinity
      flooded_Area = 1.e+30_rdp
   endif

end function flooded_Area



function vertical_Area(w,tag,resultats)
!==============================================================================
!             FONCTION OBJECTIF A MINIMISER
!
!---Historique
!      10/03/2004 : création
!==============================================================================
   use i_Parametres, only : rdp, np, maxsim, issup
   use i_Consigne, only : nc, modele, basename, ib, xmin, xmax, w_ini, nst, deltaZ, &
                          datageo, nbpro, profil
   use dim_reelles, only : ismax, ibmax
   use geometrie_section, only : is1, is2
   implicit none
!prototype
   real(kind=rdp), intent(in) :: w(*)
   real(kind=rdp) :: vertical_Area
   type (Mage_results), intent(out) :: resultats
   character(len=1) :: tag
!variables locales
   integer :: i, lu, is, kb, nbv
   integer :: lb
   character :: fmt*40, REPname*40
   real(kind=rdp) :: surface, surface_mineur, Lmaj1, Lmaj2
   type (profil) :: datamod(1000)
   
!--------------------------------------------------------------------------
   lb = len_trim(basename)
   vertical_Area = 1.e+40_rdp

   !--->exportation de la consigne
   open(newunit=lu,file=tag//'/'//basename(1:lb)//'.RUG',form='formatted',status='unknown')
   do i=1, nst/2
      write(lu,'(a,i3,6x,2f10.3,2f10.6)') 'K',ib(i),xmin(i),xmax(i),w(2*i-1),w(2*i)
   enddo
   close (lu)
   if (deltaZ > 0._rdp) then
      do n = 1, nbpro
         nbv = datageo(n)%nbval
         datamod(n)%nbval = nbv
         datamod(n)%x = datageo(n)%x
         datamod(n)%zmoy = datageo(n)%zmoy
         datamod(n)%y(1:nbv) = datageo(n)%y(1:nbv)
         datamod(n)%z(1:nbv) = datageo(n)%z(1:nbv)+w(nst+n) ! on ne met à jour que les Z !!!
      enddo
      open(newunit=lu,file=tag//'/'//basename(1:lb)//'.TAL',form='formatted',status='unknown')
      call exportTAL(lu,datamod)
      close (lu)
   endif
!--->exécution de MAGE
   REPname = basename(1:lb)//'.REP'
   call solveur_externe(REPname,tag,resultats)
!--->vérification que la simulation n'a pas échoué
   if (resultats%calcul_OK) then  !--->extraction des données de sortie et comparaison
      surface = 0._rdp
      do kb = 1, ibmax
         Lmaj1 = resultats%Z_max(is1(kb)) - resultats%Zfd(is1(kb))
         do is = is1(kb)+1, is2(kb)
            Lmaj2 = resultats%Z_max(is) - resultats%Zfd(is)
            surface = surface + 0.5_rdp*abs(resultats%pm(is)-resultats%pm(is-1))*(Lmaj2 + Lmaj1)
            Lmaj1 = Lmaj2
         enddo
      enddo
   
      vertical_Area = surface  !surface en m²

   else
      !échec de la simulation => foo = infinity
      vertical_Area = 1.e+30_rdp
   endif

end function vertical_Area



function distance_Domaine(w)
!==============================================================================
!
!==============================================================================
   use i_Parametres, only : rdp, np
   use i_Consigne, only : Wmin, Wmax, nc
   use i_annealing, only : FOtyp
   implicit none
!prototype
   real(kind=rdp), intent(in) :: W(np,2)
   real(kind=rdp)  :: distance_Domaine
!variables locales
   integer :: n, i
   real(kind=rdp)  :: DD
!interfaces
   interface
      function calage(W)
         use i_Parametres, only : rdp
         real(kind=rdp), intent(in) :: w(*)
         real(kind=rdp) :: calage
      end function calage
   end interface

!------------------------------------------------------------------------------
   DD = 0._rdp  !initialisation à 0.
   ! contraintes sur les limites du domaine
   if (FOtyp > 1) then
      do n = 1, nc
         DD = DD + max(W(n,1)-Wmax(n),0._rdp) + max(Wmin(n)-W(n,1),0._rdp)
      enddo
   else
      do i = 1, 2
         do n = 1, nc
            DD = DD + max(W(n,i)-Wmax(n),0._rdp) + max(Wmin(n)-W(n,i),0._rdp)
         enddo
      enddo
   endif
   distance_Domaine = DD
end function distance_Domaine



subroutine Init(filename)
!==============================================================================
!    Initialisation des paramètres du L.A.G., des consignes et des contraintes
!==============================================================================
   use i_Parametres, only : rdp, np
   use i_Consigne, only : Wmin, Wmax, W_ini, nc, ib, xmin, xmax, &
                        modele, basename, solveur, eps, norme, date_fin, &
                        inhib, scale, wtest, ntest, nbpro, nst, deltaZ, Coeff_Penalisation, &
                        laisse_crue, nb_obs_z, nb_obs_q, date_fin_calage, erreur_calage, &
                        Q_rep, cz, cq, datageo, nbok
   use i_Annealing, only : t0, nequil, dth, fk_opt, FOtyp, unite, nb_simul, seuil_amelioration
   implicit none
! Prototype
   character (len=*), intent(in) :: filename
! variables locales
   real(kind=rdp) :: gw(np), n1, n2, n3, n4, n5, n6, c_p
   integer :: n, lu=1, nt, io_status=0, m
   character :: ligne*80
   logical :: k_open=.false., z_open=.false., n_open=.false., test_open=.false., calz_open=.false., calq_open=.false.
   integer, allocatable :: seed(:)
   character :: FOtype*20
   
! dossier de référence pour les simulations
   character(len=20) :: ref_sim='simulation'
! dossier de référence pour le calage
   character(len=20) :: ref_cal='calage'
!------------------------------------------------------------------------------
   !open(unit=lu, file='incertitude.dat', form='formatted', status='old')
   open(unit=lu, file=trim(filename), form='formatted', status='old')
!FIXME: gérer l'erreur dans le cas où le fichier n'existe pas
   nt = 0 ; ntest = 0 ; nc = 0
   eps = 0._rdp
   deltaZ = 0._rdp
   Coeff_Penalisation = 1.e+06_rdp
   cz = 1._rdp
   cq = 0._rdp
   seuil_amelioration = 0._rdp
   nb_simul = 0
   write(8,'(a)') '#####################################################################################'
   call print_Version(8)
   write(8,'(a)') '#####################################################################################'
   write(8,'(a)') ' '
   write(8,'(a)') 'Copie du fichier incertitude.dat'
   do while (io_status==0)
      read(lu,'(a)', iostat=io_status) ligne
      write(8,'(a)') ligne(1:len_trim(ligne))
      if (ligne(1:1) == '*') then
         cycle
      elseif (ligne(1:11) == '<strickler>' .or. ligne(1:11) == '<Strickler>') then
         k_open = .true.
      elseif (ligne(1:12) == '</strickler>' .or. ligne(1:12) == '</Strickler>') then
         k_open = .false.
      elseif (k_open) then !lecture d'un tronçon ; nc = nb consignes = nb de tronçons
         nt = nt + 1 ; nc = 2*nt
         read(ligne,*) ib(nt), xmin(nt), xmax(nt), wmin(nc-1), wmax(nc-1), wmin(nc), wmax(nc), &
                                                   w_ini(nc-1,1), w_ini(nc,1), w_ini(nc-1,2), w_ini(nc,2)
      elseif (ligne(1:20) == 'temperature_initiale') then
         n = scan(ligne,'=')
         read(ligne(n+1:),'(f10.3)') t0
      elseif (ligne(1:16) == 'arrondi_consigne') then
         n = scan(ligne,'=')
         read(ligne(n+1:),'(f10.3)') eps
      elseif (ligne(1:29) == 'taux_decroissance_temperature') then
         n = scan(ligne,'=')
         read(ligne(n+1:),'(f10.3)') dth
      elseif (ligne(1:7) == 'nb_idem') then
         n = scan(ligne,'=')
         read(ligne(n+1:),*) nequil
      elseif (ligne(1:4) == 'nbok') then
         n = scan(ligne,'=')
         read(ligne(n+1:),*) nbok
      elseif (ligne(1:6) == 'modele') then
         n = scan(ligne,'=')
         read(ligne(n+1:),'(a)') modele
      elseif (ligne(1:8) == 'basename') then
         n = scan(ligne,'=')
         read(ligne(n+1:),'(a)') basename
      elseif (ligne(1:14) == 'dossier_calage') then
         n = scan(ligne,'=')
         read(ligne(n+1:),'(a)') ref_cal
      elseif (ligne(1:18) == 'dossier_simulation') then
         n = scan(ligne,'=')
         read(ligne(n+1:),'(a)') ref_sim
      elseif (ligne(1:7) == 'solveur') then
         n = scan(ligne,'=')
         read(ligne(n+1:),'(a)') solveur
      elseif (ligne(1:5)  == 'norme') then
         n = scan(ligne,'=')
         read(ligne(n+1:),*) norme
      elseif (ligne(1:8)  == 'date_fin') then
         n = scan(ligne,'=')
         read(ligne(n+1:),*) date_fin
      elseif (ligne(1:7)  == 'echelle') then
         n = scan(ligne,'=')
         read(ligne(n+1:),*) scale
      elseif (ligne(1:7)  == 'FOtype') then
         n = scan(ligne,'=')
         read(ligne(n+1:),'(a)') FOtype
         FOtype = ADJUSTL(FOtype)
         n = len_trim(FOtype)
         if (FOtype(1:n) == 'surface_EcartMax')  then
            FOtyp = 0 ; unite = ' % '
            working_Area => flooded_Area
         elseif (FOtype(1:n) == 'enveloppe_EcartMax')  then
            FOtyp = 1 ; unite = ' % '
            working_Area => vertical_Area
         elseif (FOtype(1:n) == 'surface_Min')  then
            FOtyp = 2 ; unite = ' % '
            working_Area => flooded_Area
         elseif (FOtype(1:n) == 'enveloppe_Min')  then
            FOtyp = 3 ; unite = ' % '
            working_Area => vertical_Area
         elseif (FOtype(1:n) == 'surface_Max')  then
            FOtyp = 4 ; unite = ' % '
            working_Area => flooded_Area
         elseif (FOtype(1:n) == 'enveloppe_Max')  then
            FOtyp = 5 ; unite = ' % '
            working_Area => vertical_Area
         elseif (FOtype(1:n) == 'calage_ENV')  then
            FOtyp = 6 ; unite = ' mm' !; type_resultat = 'ENV'
         elseif (FOtype(1:n) == 'calage_FIN')  then
            FOtyp = 7 ; unite = ' mm' !; type_resultat = 'FIN'
         elseif (FOtype(1:n) == 'calage')  then
            FOtyp = 6 ; unite = ' mm' !; type_resultat = 'ENV'
         elseif (FOtype(1:n) == 'Monte-Carlo')  then
            FOtyp = 8 ; unite = ' ha'
         else
            write(*,*) '>>>> Erreur > FOtype inconnu : ',FOtype(1:n),' <<<<'
            stop 001
         endif
      elseif (ligne(1:6) == '<test>') then
         ntest = ntest+1 ; test_open = .true.
      elseif (ligne(1:7) == '</test>') then
         test_open = .false.
      elseif (test_open) then  !lecture des consignes de test a priori
         read(ligne,*) nt, wtest(2*nt-1,1,ntest),wtest(2*nt,1,ntest),&
                           wtest(2*nt-1,2,ntest),wtest(2*nt,2,ntest)
      elseif (ligne(1:9) == 'geometrie') then
         n = scan(ligne,'=')
         read(ligne(n+1:),*) deltaZ
      elseif (ligne(1:12) == 'penalisation') then
         n = scan(ligne,'=')
         read(ligne(n+1:),*) C_P
         Coeff_Penalisation = C_P * Coeff_Penalisation
      elseif (ligne(1:10) == '<calage_z>' .or. ligne(1:10) == '<Calage_Z>') then
         calz_open = .true.
         nb_obs_z = 0
      elseif (ligne(1:11) == '</calage_z>' .or. ligne(1:11) == '</Calage_Z>') then
         calz_open = .false.
      elseif (calz_open) then !lecture du jeu de laisses de crue
         nb_obs_z = nb_obs_z + 1
         read(ligne,*) laisse_crue(nb_obs_z)%ib, laisse_crue(nb_obs_z)%x, laisse_crue(nb_obs_z)%z
      elseif (ligne(1:10) == '<calage_q>' .or. ligne(1:10) == '<Calage_Q>') then
         calq_open = .true.
         nb_obs_q = 0
      elseif (ligne(1:11) == '</calage_q>' .or. ligne(1:11) == '</Calage_Q>') then
         calq_open = .false.
      elseif (calq_open) then !lecture du jeu de laisses de crue
         nb_obs_q = nb_obs_q + 1
         read(ligne,*) Q_rep(nb_obs_q)%ib, Q_rep(nb_obs_q)%x, Q_rep(nb_obs_q)%z
      elseif (ligne(1:11) == 'date_calage') then
         n = scan(ligne,'=')
         read(ligne(n+1:),*) date_fin_calage
      elseif (ligne(1:13) == 'erreur_calage') then
         n = scan(ligne,'=')
         read(ligne(n+1:),*) erreur_calage
      elseif (ligne(1:14) == 'coeff_calage_Z') then
         n = scan(ligne,'=')
         read(ligne(n+1:),*) cz
      elseif (ligne(1:14) == 'coeff_calage_Q') then
         n = scan(ligne,'=')
         read(ligne(n+1:),*) cq
      elseif (ligne(1:18) == 'seuil_amelioration') then
         n = scan(ligne,'=')
         read(ligne(n+1:),*) seuil_amelioration
         seuil_amelioration = -abs(seuil_amelioration) !Pour être sûr que seuil_amelioration soit négatif
      endif
   enddo
   if (nb_obs_q > 0) unite = '   '
   close(lu)
   nst = nc
   if (deltaZ > 0._rdp) then
      call lireTAL(ref_sim)
      nc = nst + nbpro
      do nt = nst+1, nc
         wmin(nt) = -deltaZ*0.5_rdp
         wmax(nt) = +deltaZ*0.5_rdp
         w_ini(nt,1) = 0._rdp
         w_ini(nt,2) = 0._rdp
      enddo
      open(newunit=lu,file=trim(outdir)//'/verif.tal',form='formatted',status='unknown')
      call exportTAL(lu,datageo)
      close (lu)
   endif

   inhib = 0
   do nt = 1, nc
      if (wmax(nt)-wmin(nt) > 0.0001*w_ini(nt,1)) inhib(nt)=1
   enddo

   write(*,'(2a)') ' Solveur : ',solveur
!   write(*,'(2a)') ' Modele : ',modele
   write(*,'(2a)') ' Basename : ', basename
   write(*,'(2a)') ' Donnees : '
   write(*,'(4x,a,i4)') ' Nc = ', nc
!   write(*,'(4x,a,i4)') ' Nd = ', nd
   write(*,'(4x,a,f8.2)') ' T0 = ', t0
   write(*,'(4x,a,f8.2)') ' Dth = ', dth
   write(*,'(4x,a,i4)') ' Nequil = ', nequil
   write(*,'(4x,a,es10.3)') ' Precision = ', eps
   write(*,'(4x,a,f6.3)') ' Echelle = ', scale
   write(*,'(4x,a,es10.3)') ' Coefficient de penalisation = ',Coeff_Penalisation
   write(*,'(4x,2a,i1)') ' FOtype = ', FOtype

   write(*,'(a)') ' Consignes Stricklers'
   write(8,'(a)') ' Consignes Stricklers'
   do n = 1, nst/2
      write(*,'(i2,2f10.2,6f8.3)') ib(n), xmin(n), xmax(n), wmin(2*n-1), w_ini(2*n-1,1), wmax(2*n-1), &
                                                            wmin(2*n), w_ini(2*n,1), wmax(2*n)
      write(8,'(i2,2f10.2,6f8.3)') ib(n), xmin(n), xmax(n), wmin(2*n-1), w_ini(2*n-1,1), wmax(2*n-1), &
                                                            wmin(2*n), w_ini(2*n,1), wmax(2*n)
   enddo
   if (ntest > 0) then
      write(8,'(a)') ' >>> Tests a priori :'
      do nt = 1, ntest
         write(8,'(a,i2,a)') ' Tests a priori n°',nt,' (Stricklers seuls) :'
         do n = 1, nst/2
            write(8,'(a,i2,3x,4f8.3)') ' Troncon ',n, wtest(2*n-1,1,nt),wtest(2*n,1,nt),&
                                                     wtest(2*n-1,2,nt),wtest(2*n,2,nt)
         enddo
      enddo
   endif
   write(*,*) 'Calage'
   write(*,*) 'date_fin_calage = ',date_fin_calage
   write(*,*) 'erreur_calage = ',erreur_calage
   write(8,*)
   write(8,*) 'Calage'
   write(8,*) 'date_fin_calage = ',date_fin_calage
   write(8,*) 'erreur_calage = ',erreur_calage
   write(8,*) 'Laisses de crue :'
   do n = 1, nb_obs_z
      write(*,*) laisse_crue(n)%ib, laisse_crue(n)%x, laisse_crue(n)%z
      write(8,*) laisse_crue(n)%ib, laisse_crue(n)%x, laisse_crue(n)%z
   enddo
   do n = 1, nb_obs_q
      write(*,*) Q_rep(n)%ib, Q_rep(n)%x, Q_rep(n)%z
      write(8,*) Q_rep(n)%ib, Q_rep(n)%x, Q_rep(n)%z
   enddo

! initialisation du générateur de nombres pseudo-aléatoires
   call random_seed (size = m)             ! gets size in m
   allocate (seed(m))
   !do n = 1, m
   !   seed(n) = 10742456 * n*n
   !   !seed(n) = 1234 * n*n
   !enddo
   !call random_seed (put = seed (1 : m))   ! sets user seed
   call init_random_seed()
   call random_seed (get = seed (1 : m))   ! gets the current seed
   write(*,'(a,i3,6(2x,i10))') ' Random seed : ',m,(seed(n),n=1,6)
   write(*,'(18x,6(2x,i10))') (seed(n),n=7,m)
   write(8,*) 'Random seed : ',m,(seed(n),n=1,m)
   
!initialisation des dossiers de calcul pour les simulations
   if (FOtyp < 2 .or. FOtyp == 8) then
      !répertoire pour les simulations de calage
      write(*,*) 'Création des répertoires de calcul pour le solveur externe'
      write(*,*) '--> Création des répertoires pour le calage'
      call execute_command_line('rm -r ./0')
      call execute_command_line('rm -r ./1')
      call execute_command_line('mkdir 0')
      call execute_command_line('cp '//trim(ref_cal)//'/* 0/')
      call execute_command_line('mkdir 1')
      call execute_command_line('cp '//trim(ref_cal)//'/* 1/')
      write(*,*) '--> Création des répertoires pour les simulations'
      call execute_command_line('rm -r ./2')
      call execute_command_line('rm -r ./3')
      call execute_command_line('mkdir 2')
      call execute_command_line('cp '//trim(ref_sim)//'/* 2/')
      call execute_command_line('mkdir 3')
      call execute_command_line('cp '//trim(ref_sim)//'/* 3/')
      write(*,*) '--> Création du répertoire pour les résultats de calage de la solution moyenne'
      call execute_command_line('rm -r ./4')
      call execute_command_line('mkdir 4')
      call execute_command_line('cp '//trim(ref_cal)//'/* 4/')
      write(*,*) '--> Création du répertoires pour les résultats de la simulation moyenne'
      call execute_command_line('rm -r ./5')
      call execute_command_line('mkdir 5')
      call execute_command_line('cp '//trim(ref_sim)//'/* 5/')
   else if (FOtyp < 6) then
      !répertoire pour les simulations de calage
      write(*,*) 'Création des répertoires de calcul pour le solveur externe'
      write(*,*) '--> Création des répertoires pour le calage'
      call execute_command_line('rm -r ./0')
      call execute_command_line('rm -r ./1')
      call execute_command_line('mkdir 0')
      call execute_command_line('cp '//trim(ref_cal)//'/* 0/')
      call execute_command_line('mkdir 1')
      call execute_command_line('cp '//trim(ref_cal)//'/* 1/')
      write(*,*) '--> Création des répertoires pour les simulations'
      call execute_command_line('rm -r ./2')
      call execute_command_line('rm -r ./3')
      call execute_command_line('mkdir 2')
      call execute_command_line('cp '//trim(ref_sim)//'/* 2/')
      call execute_command_line('mkdir 3')
      call execute_command_line('cp '//trim(ref_sim)//'/* 3/')
   else if (FOtyp < 8) then
      !répertoire pour les simulations de calage
      write(*,*) 'Création des répertoires de calcul pour le solveur externe'
      write(*,*) '--> Création des répertoires pour le calage'
      call execute_command_line('rm -r ./0')
      call execute_command_line('mkdir 0')
      call execute_command_line('cp '//trim(ref_cal)//'/* 0/')
   endif

end subroutine init



subroutine Simulated_Annealing(W_ini,W_opt, funk)
!==============================================================================
!         ALGORITHME DE MINIMISATION PAR LA METHODE DU RECUIT-SIMULE
!   source : Numerical Recipes (livre)
!
! si sens > 0 minimisation, sinon maximisation
!==============================================================================
   use i_Parametres, only : rdp, np
   use i_Annealing, only : t0, nequil, fk_ini, fk_opt, dth, temperature, &
                         nb_eval, nb_simul, iopt, unite, sens, Surf1, Surf2, &
                         seuil_amelioration
   use i_Consigne, only : nc, wmin, wmax, eps, inhib, scale, nst, &
                          Coeff_Penalisation, deltaZ
   implicit none
! Prototype
   real(kind=rdp), intent(in) :: W_ini(np,2)
   real(kind=rdp), intent(out) :: W_opt(np,2)
! variables locales
   logical :: arret, bool, bstop, bpause
   integer :: idem, nl, n, i, k, j, nb_iter, nbvar, nn, n_testOK, ntot, nequil0, n_test
   real(kind=rdp) :: tmin
   real(kind=rdp) :: W_new(np,2), W_sa(np,2), rdom, W_i(np,2), fk(10)
   real(kind=rdp) :: fk_new, fk_sa !valeurs intermédiaires de la fonction-coût
   real(kind=rdp) :: test, C_Plus
   real(kind=rdp) :: ampli(np), ampli0(np)
   real(kind=rdp) :: alpha, nmax, beta, gamma
   character :: decision*7, ctmp1*13, ctmp2*13, ctmp3*13, MinMax*6

! interfaces
   interface
      function funk(w,C_Plus)
         use i_Parametres, only : rdp,np
         real(kind=rdp), intent(in) :: w(np,2)
         real(kind=rdp), intent(out) :: C_Plus
         real(kind=rdp) :: funk
      end function funk
   end interface
   
!---------------------------------------------------------------------------
   write(*,*) 'Simulations pour l''algorithme du Recuit Simulé'
   write(8,*) 'Simulations pour l''algorithme du Recuit Simulé'

   ! initialisation
   nbvar = sum(inhib(1:nc))
   temperature = t0                 !initialisation de la température
   ampli0 = 0.5*scale*(wmax-wmin)   !amplitude maximale des modifs de consignes
   ampli = ampli0
   nb_iter = 0                     !nombre d'itérations sur la température
   nb_eval = 0
   MinMax = ' Min ='  ;  if (sens < 0) MinMax = ' Max ='
   n_testOK = 0 ; ntot = 0 ; n_test = 0
   nequil0 = nequil

   ! paramètres pour le calcul de la température
   alpha = 0.001_rdp  ;  nmax = real(t0/dth,rdp)  ;  beta = -Log(alpha)/Log(nmax)

   W_opt = W_ini ; W_sa = W_ini
   fk_ini = funk(w_sa,C_Plus) ; fk_opt = fk_ini ; fk_sa = fk_ini
   ctmp1 = affiche(fk_ini,unite) ; ctmp2 = affiche(fk_opt,unite) ; ctmp3 = affiche(fk_sa,unite)
   decision = '       '

   write(*,'(2(a,i5),7a,f8.4,a)') '#',nb_eval,' (',nb_simul,') :',ctmp1,' Actuel = ',ctmp3,&
           MinMax,ctmp2,' T = ',t0, decision
   write(8,'(2(a,i5),7a,f8.4,a,es10.3)') '#',nb_eval,' (',nb_simul,') :',ctmp1,' Actuel = ',ctmp3,&
           MinMax,ctmp2,' T = ',t0, decision, C_plus

   tmin = 0.01_rdp*t0
   do while (temperature > 0._rdp)     !boucle sur la température
      nb_iter = 0
      arret = .false.
      idem = 0  !nb d'évaluations sans amélioration de l'optimum
      ntot = 0  !;  n_testOK = 0
      ampli = ampli0 * max(0.1_rdp,temperature / t0)

      !if (nequil > 0) w_sa = w_opt
      do while (.not. arret)   !boucle à température fixée
         !modification aléatoire des consignes et nouvelle évaluation de la fonction-coût
         call modify(w_sa,w_new,ampli)
         fk_new = funk(w_new,C_Plus)  ;  nb_eval = nb_eval+1  ;  ntot = ntot+1

         !tests pour savoir si on garde ou non la nouvelle consigne
         if (sens*(fk_new-fk_opt) < seuil_amelioration) then !NB: seuil_amelioration est négatif
         !>>>l'optimum est amélioré ; sauvegarde et on garde la nouvelle consigne
            idem = 0        !remise à 0 du compteur de non-améliorations
            iopt = nb_eval
            fk_opt = fk_new  ;  w_opt = w_new     !mise à jour de l'optimum
            fk_sa = fk_new   ;  w_sa = w_new      !on garde la nouvelle consigne
            decision = ' ##### '
            nb_iter = nb_iter+1
         elseif (sens*(fk_new-fk_sa) < seuil_amelioration) then  !NB: seuil_amelioration est négatif
         !>>>la consigne est meilleure que la précédente : on la garde
         !>>>mais l'optimum n'a pas changé, seulement fk_sa
            fk_sa = fk_new  ;  w_sa = w_new
            decision = ' +++++ '
            nb_iter = nb_iter+1
         elseif (C_Plus*Coeff_Penalisation > 1._rdp) then   !>>>on rejette la nouvelle consigne car les contraintes ne sont pas satisfaites
            decision = ' ----- '
            !on ne tient pas compte de ce cas pour réduire la température
         else
         !>>>on garde peut-être la nouvelle consigne mais l'optimum ne change pas
            !calcul du critère de test
            n_test = n_test+1
            !test = exp(-sens*(fk_new-fk_sa)/(fk_sa*temperature))
            !!test = exp(-sens*(fk_new-fk_sa)/temperature)
            test = exp(-sens*(fk_new-fk_opt)/temperature)
            !tirage aléatoire
            call random_number(rdom)
            if (test > rdom) then   !>>>on garde la nouvelle consigne
               fk_sa = fk_new  ;  w_sa = w_new
               !l'optimum n'a pas changé, seulement fk_sa
               decision = ' ===== '
               n_testOK = n_testOK+1
               nb_iter = nb_iter+1
            else                    !>>>on rejette la nouvelle consigne
               decision = ' --+-- '
               idem = idem + 1
            endif
         endif

         !Listing et affichage sur la console
         ctmp1 = affiche(fk_new,unite) ; ctmp2 = affiche(fk_opt,unite) ; ctmp3 = affiche(fk_sa,unite)
         !console
         write(*,'(2(a,i5),7a,f8.4,a,1x,f5.3,2i4)') '#',nb_eval,' (',nb_simul,') :',ctmp1,' Actuel = ',ctmp3,&
                 MinMax,ctmp2,' T = ',temperature,decision,real(n_testOK)/real(n_test),ntot,idem
         !fichier txt sortie
         write(8,'(2(a,i5),7a,f8.4,a,2es10.3,1x,f5.3,i4)') '#',nb_eval,' (',nb_simul,') :',ctmp1,' Actuel = ',ctmp3,&
                 MinMax,ctmp2,' T = ',temperature,decision, C_plus,minval(ampli(1:nc)), real(n_testOK)/real(n_test),ntot
         !fichier csv strickler
         if (sens < 0.) then
            test = max(-99.999,fk_new)
         else
            test = min(99.999,fk_new)
         endif
         write(7,'(i5,a,100f10.3)') nb_eval, decision, w_new(1:nst,1), w_new(1:nst,2), &
                                     Surf1, Surf2, test, temperature
         if (deltaZ > 0._rdp) then
            !fichier csv géométrie
            write(9,'(i5,a,1000f10.3)') nb_eval, decision, w_new(nst+1:nc,1), w_new(nst+1:nc,2), &
                                        Surf1, Surf2, test, temperature
         endif

         !Arrêt anticipé si on trouve un fichier stop, Pause si fichier pause
         inquire(file='stop',exist=bstop)
         if (bstop) then  !arrêt anticipé
            open(unit=50,file='stop',status='unknown')
            close(50,status='delete')  !suppression du fichier 'stop'
            write(*,*) ' >>> interruption par l''utilisateur !!!'
            write(8,*) ' >>> interruption par l''utilisateur !!!'
            return
         endif
         inquire(file='pause',exist=bpause)
         if (bpause) call mise_en_veille()

         !test pour changement de température : 
         !      si pas d'amélioration
         ! OU   si pas d'itération
         ! OU   si nombre total d'itérations à température donnée dépassé
         arret = (nb_iter > nequil) .OR. (idem > nequil) .OR. (nequil == 0)! .OR. (ntot > 4*nequil)

      enddo
      if (temperature > tmin) then
         temperature = temperature*(1._rdp-dth) !décroissance exponentielle
      else
         temperature = temperature - dth*tmin
      endif
   enddo
   fk_opt = funk(w_opt,C_Plus)  !pour que la dernière simulation corresponde à l'optimum

   write(*,'(a,g14.6)') ' Sortie de Simulated_Annealing a la temperature de ',temperature
   write(8,'(a,g14.6)') ' Sortie de Simulated_Annealing a la temperature de ',temperature
end subroutine Simulated_Annealing



subroutine M4b(debut,fin,values0)
!==============================================================================
!--categorie : IHM
!
!                   Fin du comptage du temps de calcul
!        et ecriture du nombre total d'iterations et pas de temps
!==============================================================================
   use i_Parametres, only: rdp
   use i_Annealing, only: cpu_externe
   implicit none

! -- Prototype --
   integer,intent(in) :: Values0(8)
   real(kind=rdp), intent(in) :: debut, fin

! -- Variables --
   integer :: Values(8)
   integer :: Annee0,Mois0,Jour0,Heure0,Minute0,Seconde0,Centieme0
   integer :: Annee1,Mois1,Jour1,Heure1,Minute1,Seconde1,Centieme1
   integer :: Heure, Minute, Seconde
   integer :: Heure_cpu, Minute_cpu, Seconde_cpu, mille_cpu
   integer :: TimeSec0,TimeSec1, Duree, Duree_cpu
   character(len=8) :: DDate
   character(len=10) :: Time
   character(len=5) :: Zone
!------------------------------------------------------------------------------
   annee0=values0(1)
   mois0=values0(2)
   jour0=values0(3)
   heure0=values0(5)
   minute0=values0(6)
   seconde0=values0(7)
   centieme0=values0(8)
!date de la fin de la simulation
   call date_and_time(ddate,time,zone,values)
   annee1=values(1)
   mois1=values(2)
   jour1=values(3)
   heure1=values(5)
   minute1=values(6)
   seconde1=values(7)
   centieme1=values(8)
!---duree du calcul
   heure=heure1+24*(jour1-jour0)  ! changement de jour eventuel
   timesec0=seconde0+60*minute0+3600*heure0
   timesec1=seconde1+60*minute1+3600*heure
   duree=timesec1-timesec0
   if (centieme1-centieme0 > 50) duree = duree+1
   heure = duree/3600
   duree = duree - heure*3600
   minute = duree/60
   seconde = duree - minute*60
   !call cpu_time(fin)
   if (debut < 0. .or. fin < 0.) then
      duree_cpu = -9999.
      mille_cpu = -999
   else
      duree_cpu = int(fin-debut+cpu_externe)
      mille_cpu = 1000*(fin-debut+cpu_externe-duree_cpu)
   endif
   heure_cpu=duree_cpu/3600
   duree_cpu=duree_cpu-3600*heure_cpu
   minute_cpu=duree_cpu/60
   seconde_cpu=duree_cpu-60*minute_cpu
   write(*,1030) jour0,mois0,annee0,heure0,minute0,seconde0,heure,minute,seconde,&
                 heure_cpu,minute_cpu,seconde_cpu,mille_cpu
   write(8,1030) jour0,mois0,annee0,heure0,minute0,seconde0,heure,minute,seconde,&
                 heure_cpu,minute_cpu,seconde_cpu,mille_cpu
   !write(*,*) 'CPU_Externe = ',cpu_externe
!------------------------------------------------------------------------
 1030 format(1X,'Date debut simulation : ',I2.2,'/',I2.2,'/',I4, &
                ' (',I2.2,':',I2.2,':',I2.2,')', &
                ' Duree : ',I2.2,':',I2.2,':',I2.2, &
                ' CPU total : ',I2.2,':',I2.2,':',I2.2,',',I3.3)
!------------------------------------------------------------------------------
end subroutine M4b



subroutine Mise_en_veille()
   logical reprise
   character :: csv*60,txt*60

   open(unit=50,file='pause',status='unknown')
   close(50,status='delete')
   inquire(unit=7,name=csv) ; close(7)
   inquire(unit=8,name=txt) ; close(8)
   do
      call sleep(3)
      inquire(file='go',exist=reprise)
      open(unit=50,file='go',status='unknown')
      close(50,status='delete')
      if (reprise) then
         open(unit=7, file=trim(csv), form='formatted', status='old',position='append')
         open(unit=8, file=trim(txt), form='formatted', status='old',position='append')
         exit
      endif
   enddo
end subroutine mise_en_veille



subroutine LireTAL(ref_sim)
! lecture d'un fichier TAL monobief
   use i_Parametres, only: rdp
   use i_Consigne, only : datageo, titre, basename, nbpro
   implicit none
   ! Prototype
   character(len=*),intent(in) :: ref_sim
   ! Variables locales
   character(len=12) :: filename
   integer :: lu=2, io_status=0, npro, nl, nlmax = 2, npt, i, lb
   character(len=80) :: ligne, ligrep(30)

   lb = len_trim(basename)
   nl = 1
   ! lecture du fichier TAL dans le dossier de référence des simulations
   open(unit=lu, file=trim(ref_sim)//'/'//basename(1:lb)//'.REP', form='formatted', status='old')
   do while (io_status == 0)
      read(lu,'(a)', iostat=io_status) ligrep(nl)
      if (ligrep(nl)(1:3) == 'TAL' .OR. ligrep(nl)(1:3) == 'tal') then
         filename = ligrep(nl)(5:len_trim(ligrep(nl)))
         ligrep(nl)(5:16) = basename(1:lb)//'.TAL'
      endif
      nl = nl+1
   enddo
   close(lu)
   write(*,'(2a)') ' Fichier TAL : ',trim(ref_sim)//'/'//filename
   write(8,'(2a)') ' Fichier TAL : ',trim(ref_sim)//'/'//filename
   open(unit=lu, file=trim(ref_sim)//'/'//filename, form='formatted', status='old')
   npro = 0 ; nl = 0
   read(lu,'(a)', iostat=io_status) ligne
   do while (io_status == 0)
      if (ligne(1:1) == '*') then
         continue
      elseif (ligne(1:1) == '#') then
         titre = ligne
         !write(*,*) 'titre : ',titre
         nl = 0
      elseif (nl == 0) then  !nouveau profil / ligne d'abscisses
         npro =  npro+1
         npt = len_trim(ligne)/6 -1
         if (ligne(1:1) == '+') then
            nlmax = 4
            if (npt < 12) stop 301
         else
            nlmax = 2
         endif
         !write(*,*) '>>>> ',ligne(1:1),nlmax
         read(ligne(2:),'(13f6.0)') datageo(npro)%x,(datageo(npro)%y(i),i=1,npt)
         nl = 1
         datageo(npro)%nbval = npt
      elseif (nl == 1) then
         npt = len_trim(ligne)/6 -1
         if (nlmax == 2) then  ! ligne de cotes
            read(ligne(2:),'(13f6.0)') datageo(npro)%zmoy,(datageo(npro)%z(i),i=1,npt)
            nl = 0
         else                  ! ligne d'abscisse (2ème)
            read(ligne(8:),'(12f6.0)') (datageo(npro)%y(i),i=datageo(npro)%nbval+1,datageo(npro)%nbval+npt)
            nl = 2
         endif
      elseif (nl == 2) then
         npt = len_trim(ligne)/6 -1
         if (nlmax == 2) then
            stop 302
         else                  ! ligne de cotes (1ère)
            read(ligne(2:),'(13f6.0)') datageo(npro)%zmoy,(datageo(npro)%z(i),i=1,npt)
         endif
         nl = 3
      elseif (nl == 3) then
         npt = len_trim(ligne)/6 -1
         if (nlmax == 2) then
            stop 303
         else                  ! ligne de cotes (2ème)
            read(ligne(8:),'(12f6.0)') (datageo(npro)%z(i),i=datageo(npro)%nbval+1,datageo(npro)%nbval+npt)
         endif
         datageo(npro)%nbval = datageo(npro)%nbval+npt
         nl = 0
         !write(*,*) 'nbval = ',datageo(npro)%nbval, npro
      endif
      read(lu,'(a)', iostat=io_status) ligne
   enddo
   close(lu)
   nbpro = npro
end subroutine LireTAL



subroutine ExportTAL(lu,datageo)
   use i_Parametres, only : rdp
   use i_Consigne, only : nbpro, titre, profil
   implicit none
   !character(len=*) :: filename
   type (profil) :: datageo(*)
   character(len=80) :: ligne
   integer :: nl, lu

   !open(newunit=lu,file=filename,status='unknown',form='formatted')
   write(lu,'(a)') titre
   do nl = 1, nbpro
      write(lu,'(a,i3,a,f8.2,i3,a)') '* Profil ',nl,' : Pk = ',datageo(nl)%x,datageo(nl)%nbval,' points'
      if (datageo(nl)%nbval > 12) then
         call ligneTAL(ligne,'+',datageo(nl)%x,datageo(nl)%y(1:12),12)
         write(lu,'(a)') ligne
         call ligneTAL(ligne,' ',99999._rdp,datageo(nl)%y(13:datageo(nl)%nbval),datageo(nl)%nbval-12)
         write(lu,'(a)') ligne
         call ligneTAL(ligne,' ',datageo(nl)%zmoy,datageo(nl)%z(1:12),12)
         write(lu,'(a)') ligne
         call ligneTAL(ligne,' ',99999._rdp,datageo(nl)%z(13:datageo(nl)%nbval),datageo(nl)%nbval-12)
         write(lu,'(a)') ligne
      else
         call ligneTAL(ligne,' ',datageo(nl)%x,datageo(nl)%y(1:datageo(nl)%nbval),datageo(nl)%nbval)
         write(lu,'(a)') ligne
         call ligneTAL(ligne,' ',datageo(nl)%zmoy,datageo(nl)%z(1:datageo(nl)%nbval),datageo(nl)%nbval)
         write(lu,'(a)') ligne
      endif
   enddo
   !close(lu)
end subroutine exportTAL



subroutine LigneTAL(ligne,a,z,y,n)
   use i_Parametres, only : rdp
   implicit none
   character(len=80) :: ligne
   character(len=1) :: a
   real(kind=rdp) :: z,y(*)
   integer :: n
   integer :: i, k

   do i = 1, 80
      ligne(i:i) = ' '
   enddo
   ligne(1:1) = a(1:1)
   k = 2
   if (z > 9999._rdp) then
      ligne(k:k+5) = '      '
   elseif (z >= 1000._rdp) then
      write(ligne(k:k+5),'(f6.1)') z
   else
      write(ligne(k:k+5),'(f6.2)') z
   endif
   do i = 1, n
      k = k+6
      if (y(i) >= 1000._rdp .OR. y(i) <= -100._rdp) then
         write(ligne(k:k+5),'(f6.1)') y(i)
      else
         write(ligne(k:k+5),'(f6.2)') y(i)
      endif
   enddo

end subroutine ligneTAL



! subroutine ModifTAL(w)
!    use i_Parametres, only : rdp
!    use i_Consigne, only : datageo, nbpro, basename, profil
!    implicit none
!    integer :: n, nbv
!    integer, save :: nap=0, lb
!    character(len=12), save :: filename
!    type (profil), save :: datamod(1000)
!    real(kind=rdp) :: w(*)

!    if (nap < 1) then
!       datamod(1:nbpro) = datageo(1:nbpro) ! nécessaire pour initialiser datamod
!       nap = 2
!       lb = len_trim(basename)
!       filename = basename(1:lb)//'.TAL'
!    endif
!    do n = 1, nbpro
!       nbv = datageo(n)%nbval
!       datamod(n)%z(1:nbv) = datageo(n)%z(1:nbv)+w(n) ! on ne met à jour que les Z !!!
!    enddo
!    call exportTAL(filename,datamod)
! end subroutine modifTAL



function calage_ok(W,tag)
!==============================================================================
!             Vérification du calage
!
!---Historique
!      08/06/2009 : création
!==============================================================================
   use i_Parametres, only : rdp
   use i_Consigne, only : erreur_calage

   implicit none
!prototype
   real(kind=rdp), intent(in) :: w(:)
   character(len=1), intent(in) :: tag
   real(kind=rdp) :: calage_ok
   
   type (Mage_results) :: resultats

   !print*,'Entrée calage_ok n°'//tag
   calage_ok = max(0._rdp , calage(w,tag,resultats)-erreur_calage)
end function calage_ok



function calage(W,tag,resultats)
!==============================================================================
!             Vérification du calage
!
!     Cette fonction évalue l'écart entre le résultat d'une simulation et les
!     données observées.
!     Les données de calage étant a priori différentes de celles concernées par
!     l'évaluation des incertitudes, calage() travaille dans un sous-répertoire
!     du répertoire courant.
!
!---Historique
!      17/07/2008 : création de calage_ok
!      26/05/2009 : modification pour renvoyer la valeur de l'erreur
!==============================================================================
   use i_Parametres, only : rdp, np, maxsim, issup
   use i_Consigne, only : nc, modele, basename, ib, xmin, xmax, nst,deltaZ, &
                          laisse_crue, nb_obs_z, nb_obs_q, Q_rep, cz, cq, &
                          datageo, profil, nbpro
   use i_Annealing, only : nb_eval, FOtyp
   use dim_reelles, only : ismax, ibmax
   use geometrie_section, only : is1, is2
   implicit none
!prototype
   real(kind=rdp), intent(in) :: w(:)
   character(len=1), intent(in) :: tag
   type (Mage_results), intent(out) :: resultats
   real(kind=rdp) :: calage
!variables locales
   integer :: i, lu, is, nbv
   integer :: lb
   character :: fmt*40, REPname*40
   integer :: n
   real(kind=rdp) :: erreurZ, erreurQ
   type (profil) :: datamod(1000)

!--------------------------------------------------------------------------
   !print*,'Entrée calage n°'//tag
   if (maxval(w(1:nc)) < -9999._rdp) then
      calage = -9999._rdp
      return
   endif
   !print*,'1__calage n°'//tag
   lb = len_trim(basename)
   !--->exportation de la consigne
   open(newunit=lu,file=tag//'/'//basename(1:lb)//'.RUG',form='formatted',status='unknown')
   do i=1, nst/2
      write(lu,'(a,i3,6x,2f10.3,2f10.6)') 'K',ib(i),xmin(i),xmax(i),w(2*i-1),w(2*i)
   enddo
   close(lu)
   !print*,'2__calage n°'//tag
   if (deltaZ > 0._rdp) then
      do n = 1, nbpro
         nbv = datageo(n)%nbval
         datamod(n)%nbval = nbv
         datamod(n)%x = datageo(n)%x
         datamod(n)%zmoy = datageo(n)%zmoy
         datamod(n)%y(1:nbv) = datageo(n)%y(1:nbv)
         datamod(n)%z(1:nbv) = datageo(n)%z(1:nbv)+w(nst+n) ! on ne met à jour que les Z !!!
      enddo
      open(newunit=lu,file=tag//'/'//basename(1:lb)//'.TAL',form='formatted',status='unknown')
      call exportTAL(lu,datamod)
      close (lu)
   endif
!--->exécution de MAGE
   !print*,'3__calage n°'//tag
   REPname = basename(1:lb)//'.REP'
   !print*,'4__calage n°'//tag, REPname
   call solveur_externe(REPname,tag,resultats)
!--->vérification que la simulation n'a pas échoué
   !print*,'5__calage n°'//tag, REPname
   if (resultats%calcul_OK) then  !--->extraction des données de sortie et comparaison
      if (FOtyp < 6.or. FOtyp > 7) then  !vérification du calage : écart maximal
         erreurZ = -1.e50_rdp  ;  erreurQ = -1.e50_rdp
         do n = 1, nb_obs_z
            is = iSect(laisse_crue(n)%ib, laisse_crue(n)%x, 0.01_rdp)
            erreurZ = max(erreurZ,abs(resultats%Z_max(is)-laisse_crue(n)%z))
         enddo
      else  if (FOtyp == 6) then !calage automatique sur les maxima 
                                 !moyenne quadratique des écarts entre valeurs calculées et valeurs observées
         erreurZ = 0._rdp 
         do n = 1, nb_obs_z
            is = iSect(laisse_crue(n)%ib, laisse_crue(n)%x, 0.01_rdp)
            erreurZ = erreurZ + (resultats%Z_max(is)-laisse_crue(n)%z)**2
         enddo
         erreurZ = sqrt(erreurZ / real(nb_obs_z))
         erreurQ = 0._rdp
         if (nb_obs_q > 0) then
            do n = 1, nb_obs_q
               is = iSect(Q_rep(n)%ib, Q_rep(n)%x, 0.01_rdp)
               erreurQ = erreurQ + (resultats%Qtot(is) - Q_rep(n)%z)**2
            enddo
            erreurQ = sqrt(erreurQ/real(nb_obs_q))
         endif
      else if (FOtyp == 7) then !calage automatique sur les valeurs de l'état final
         if (resultats%type_resultat /= 'FIN' .and. resultats%type_resultat /= 'END') then
            write(*,*) 'Type de resultat erroné : ',resultats%type_resultat
            stop 444
         endif
         erreurZ = 0._rdp 
         do n = 1, nb_obs_z
            is = iSect(laisse_crue(n)%ib, laisse_crue(n)%x, 0.01_rdp)
            erreurZ = erreurZ + (resultats%Z_max(is)-laisse_crue(n)%z)**2
         enddo
         erreurZ = sqrt(erreurZ / real(nb_obs_z))
         ! Dans ce cas on cale sur la répartition des débits entre lit mineur et lit majeur
         ! Part de débit qui passe dans la plaine d'inondation exprimée en pourcentage du débit total
         ! Pour un calage sur l'état final en permanent, il n'y a pas d'autre choix pertinent pour le calage sur les débits
         erreurQ = 0._rdp
         if (nb_obs_q > 0) then
            do n = 1, nb_obs_q
               is = iSect(Q_rep(n)%ib, Q_rep(n)%x, 0.01_rdp)
               erreurQ = erreurQ + (100._rdp*resultats%Qfp(is)/resultats%Qtot(is) - Q_rep(n)%z)**2
            enddo
            erreurQ = sqrt(erreurQ/real(nb_obs_q))
         endif
      endif
      calage = cz*abs(erreurZ) + cq*abs(erreurQ)
   else
      !échec de la simulation => foo = infinity
      calage = 1.e30_rdp
   endif
end function calage



subroutine modify_1a(w_sa,w_new,ampli)
!==============================================================================
!  routine de modification
!   avec arrondi
!==============================================================================
   use i_Parametres, only : rdp, np
   use i_Consigne, only : nc, eps, inhib, wmin, wmax
   implicit none
! Prototype
   real(kind=rdp), intent(in) :: W_sa(np,2), ampli(np)
   real(kind=rdp), intent(out) :: W_new(np,2)
! Variables locales
   integer :: n
   
!--------------------------------------------------------------------------
   do n = 1, nc
      if( inhib(n) == 0 ) then
         w_new(n,1) = w_sa(n,1)
      else
         !modifications sans corrélation entre tronçons
         ! on bloque aux bords ici pour faciliter l'apparition de consignes
         ! calées sur les bords car les solutions sont souvent là
         w_new(n,1) = random(max(w_sa(n,1)-ampli(n),wmin(n)),min(w_sa(n,1)+ampli(n),wmax(n)))
         ! arrondi à 1/eps décimales
         w_new(n,1) = eps*anint(w_new(n,1)/eps)
      endif
   enddo
end subroutine modify_1a


subroutine modify_1b(w_sa,w_new,ampli)
!==============================================================================
!  routine de modification
!   sans arrondi
!==============================================================================
   use i_Parametres, only : rdp, np
   use i_Consigne, only : nc, inhib, wmin, wmax
   implicit none
! Prototype
   real(kind=rdp), intent(in) :: W_sa(np,2), ampli(np)
   real(kind=rdp), intent(out) :: W_new(np,2)
! Variables locales
   integer :: n
   
!--------------------------------------------------------------------------
   do n = 1, nc
      if( inhib(n) == 0 ) then
         w_new(n,1) = w_sa(n,1)
      else
      !modifications sans corrélation entre tronçons
         w_new(n,1) = random(max(w_sa(n,1)-ampli(n),wmin(n)),min(w_sa(n,1)+ampli(n),wmax(n)))
      endif
   enddo
end subroutine modify_1b



subroutine modify_2a(w_sa,w_new,ampli)
!==============================================================================
!  routine de modification
!   avec arrondi
!==============================================================================
   use i_Parametres, only : rdp, np
   use i_Consigne, only : nc, eps, inhib, wmin, wmax
   implicit none
! Prototype
   real(kind=rdp), intent(in) :: W_sa(np,2), ampli(np)
   real(kind=rdp), intent(out) :: W_new(np,2)
! Variables locales
   integer :: n
!--------------------------------------------------------------------------
!!$omp parallel default(shared) private(n)
!   !$omp do schedule(dynamic)    
   do n = 1, nc
      if( inhib(n) == 0 ) then
         w_new(n,1) = w_sa(n,1)
         w_new(n,2) = w_sa(n,2)
      else
         w_new(n,1) = random(max(w_sa(n,1)-ampli(n),wmin(n)),min(w_sa(n,1)+ampli(n),wmax(n)))
         w_new(n,1) = eps*anint(w_new(n,1)/eps)
         w_new(n,2) = random(max(w_sa(n,2)-ampli(n),wmin(n)),min(w_sa(n,2)+ampli(n),wmax(n)))
         w_new(n,1) = eps*anint(w_new(n,1)/eps)
      endif
   end do
!   !$omp end do
!!$omp end parallel
end subroutine modify_2a



subroutine modify_2b(w_sa,w_new,ampli)
!==============================================================================
!  routine de modification
!   sans arrondi
!==============================================================================
   use i_Parametres, only : rdp, np
   use i_Consigne, only : nc, inhib, wmin, wmax
   use i_Annealing, only : t0, nb_eval
   implicit none
! Prototype
   real(kind=rdp), intent(in) :: W_sa(np,2), ampli(np)
   real(kind=rdp), intent(out) :: W_new(np,2)
! Variables locales
   integer :: n
!--------------------------------------------------------------------------
!!$omp parallel default(shared) private(n)
!   !$omp do schedule(dynamic)
   do n = 1, nc
      if( inhib(n) == 0 ) then
         w_new(n,1) = w_sa(n,1)
         w_new(n,2) = w_sa(n,2)
      else
         w_new(n,1) = random(max(w_sa(n,1)-ampli(n),wmin(n)),min(w_sa(n,1)+ampli(n),wmax(n)))
         w_new(n,2) = random(max(w_sa(n,2)-ampli(n),wmin(n)),min(w_sa(n,2)+ampli(n),wmax(n)))
      endif
   end do
!   !$omp end do
!!$omp end parallel
end subroutine modify_2b


subroutine modify_2c(w_sa,w_new,ampli)
!==============================================================================
!  routine de modification
!==============================================================================
   use i_Parametres, only : rdp, np, sp
   use i_Consigne, only : nc, eps, inhib, scale, nst, wmin, wmax
   use i_Annealing, only : t0, FOtyp, nb_eval
   implicit none
! Prototype
   real(kind=rdp), intent(in) :: W_sa(np,2), ampli(np)
   real(kind=rdp), intent(out) :: W_new(np,2)
! Variables locales
   integer :: n, i, k, j
   real(kind=rdp) :: a, b
!--------------------------------------------------------------------------   
   w_new = w_sa  !initialisation globale avant modif de certaines composantes
   if (FOtyp < 2) then
      i = 1+mod(nb_eval,4)/2    !indice consigne 1 ou 2
      j = 1-mod(nb_eval,2)      !groupe Strickler mineur (1) ou Strickler majeur (0)
   else
      i = 1  !calage : on ne modifie que la 1ère consigne
      j = mod(nb_eval,2)
   endif
   do n = 1, nc
      if( inhib(n) == 0 ) cycle            !on ignore cette composante si inhib est nul              
      if(n <= nst .and. mod(n,2)/=j) cycle !ou selon que n est pair ou impair (alternativement) pour les stricklers
      a = w_sa(n,i)-ampli(n)
      b = w_sa(n,i)+ampli(n)
      w_new(n,i) = random(a,b)
      ! on bloque aux bords ici pour faciliter l'apparition de consignes
      ! calées sur les bords car les solutions sont souvent là
      w_new(n,i) = max (w_new(n,i),wmin(n))
      w_new(n,i) = min (w_new(n,i),wmax(n))
   enddo
end subroutine modify_2c


subroutine Monte_Carlo()
!==============================================================================
! Réalisation de nbok évaluations de flooded_Area()
! On tire des simulations au hasard dans le domaine acceptable [wmin ; wmax]
! autant de fois que nécessaire pour en avoir nbok qui satisfont le calage
!==============================================================================
   use i_Parametres, only : rdp, np, issup
   use i_Consigne, only : nc, wmin, wmax, nst, nbok
   implicit none
! Prototype
!
! Variables locales
   integer :: n, k, nok, i, lu
   real(kind=rdp) :: ww(np,2)
   real(kind=rdp) :: a, b, foo, err_cal
   character(len=1200) :: texte
   logical :: bstop, bpause

!--------------------------------------------------------------------------
   write(*,*) 'Simulations pour la méthode de Monte-Carlo'
   write(8,*) 'Simulations pour la méthode de Monte-Carlo'
   nok = 0  ;  n = 0
   ww(:,1) = (wmax + wmin)/2._rdp
   ww(:,2) = (wmax + wmin)/2._rdp
   do while (nok < nbok .and. n < 100000)
      n = n+1
      foo = Foo_Ecart(ww,err_cal)
      if (err_cal < 1.e-06_rdp) nok = nok + 1
      write(texte,'(100(2x,f8.3))',decimal='comma') ww(1:nst,1:2), min(9999.999,foo), min(9999.999,1000._rdp*err_cal)
      write(*,'(i5,a,2(2x,f8.3),5x,i4,f8.3)',decimal='comma') n,' : ', foo, err_cal, nok, real(nok)/real(n)
      write(8,'(i5,a,2(2x,f8.3),5x,i4,f8.3)',decimal='comma') n,' : ', foo, err_cal, nok, real(nok)/real(n)
      write(7,'(i5,a,a,5x,i4,f8.3)',decimal='comma') n,' : ', texte(1:len_trim(texte)), nok, real(nok)/real(n)
      do i = 1, 2
         do k = 1, nc
            ww(k,i) = random(0.99_rdp*wmin(k),1.01_rdp*wmax(k))
            ww(k,i) = max (ww(k,i),wmin(k))
            ww(k,i) = min (ww(k,i),wmax(k))
         enddo
      enddo
      !Arrêt anticipé si on trouve un fichier stop, Pause si fichier pause
      inquire(file='stop',exist=bstop)
      if (bstop) then  !arrêt anticipé
         open(newunit=lu,file='stop',status='unknown')
         close(lu,status='delete')  !suppression du fichier 'stop'
         write(*,*) ' >>> interruption par l''utilisateur !!!'
         write(8,*) ' >>> interruption par l''utilisateur !!!'
         return
      endif
      inquire(file='pause',exist=bpause)
      if (bpause) call mise_en_veille()
   enddo
end subroutine Monte_Carlo



subroutine Solveur_externe(REPname,tag,resultats)
   use omp_lib
   use i_Parametres, only : rdp
   use Dim_Reelles, only : ismax, ibmax
   use Geometrie_Section, only : is1, is2, ibu, xgeo
   use i_Annealing, only : nb_simul, cpu_externe, FOtyp
   use i_Consigne, only : solveur
   implicit none
! Prototype
   character(len=40), intent(in) :: REPname
   character(len=1), intent(in) :: tag
   type (Mage_results), intent(out) :: resultats
!   
   logical :: silent, bexist
   integer :: is, lu, ib, flag
   character(len=90) :: commande
   real(kind=rdp) :: cpu_solveur

   !print*,'Entrée solveur_externe(), tag=',tag
   commande = trim(solveur)//' -d='//tag//' -s -l'//' '//trim(REPname)
   if (FOtyp == 7) commande = trim(solveur)//' -d='//tag//' -s -l=FIN'//' '//trim(REPname)
   !write(6,*) 'Commande solveur = ',trim(commande),'###','  tag = ',tag
   call execute_command_line(commande)
   
   !print*,'Test si le fichier Mage_Results existe pour la simulation ',tag
   inquire(file='./'//tag//'/'//'Mage_Results',exist=bexist)
   if (bexist) then
      !print*,' Le fichier Mage_Results existe pour la simulation ',tag
      open(newunit=lu,file=tag//'/'//'Mage_Results',form='unformatted',status='old')
         read (lu) flag
         resultats%calcul_OK = (flag .GT. 0)
         !write(6,*) 'Tag = ',tag,' Flag = ',flag,'  ',resultats%calcul_OK
         read(lu) resultats%date_fin, ibmax, ismax, resultats%type_resultat, cpu_solveur
         !write(6,*) 'Tag = ',tag,' Type Résultats = ',resultats%type_resultat
         do ib = 1, ibmax
            read(lu) is1(ib),is2(ib), ibu(ib)
         enddo
         do is = 1, ismax
            read(lu) resultats%pm(is), resultats%zfd(is), resultats%largeur_totale(is),&
                     resultats%largeur_mineur(is), resultats%Z_max(is), resultats%qtot(is),&
                     resultats%qfp(is)
         enddo
      close(lu,status='delete')
   else
      resultats%calcul_OK = .false.
      resultats%date_fin = -99999
      cpu_solveur = 0.
   endif
   nb_simul = nb_simul+1
   if (nb_simul == 1) xgeo = resultats%pm
   cpu_externe = cpu_externe + cpu_solveur
      
end subroutine solveur_externe



function iSect(IB,X,Y)  !numéro
!==============================================================================
!   Cette fonction retourne le numero absolu de la section situee a
!       l'abscisse X du bief de numero IB dans .TAL
!   L'argument optionnel Y permet de définir la tolérance de correspondance
!   des abscisses. 
!   Si Y est absent on cherche une correspondance à 10**-5 près (en relatif)
!
!Historique des modifications
! 05/02/2007 : documentation
!==============================================================================
   use i_Parametres, only: rdp
   use dim_reelles, only: ibmax
   use geometrie_section
   implicit none
! -- le prototype --
   integer :: isect
   integer,intent(in) :: ib
   real(kind=rdp),intent(in) :: x
   real(kind=rdp),intent(in), optional :: y
! -- les variables --
   integer :: is, kb
!------------------------------------------------------------------------------
   if (ib>ibmax .or. ib<1) then
      write(6,'(a,i3,a)') ' >>>> ERREUR (fonction ISECT) : le bief ',ib, &
                          ' est inconnu sur ce réseau <<<<'
      stop 192
   endif
   kb=ibu(ib)
   if ( (x-xgeo(is1(kb)))*(x-xgeo(is2(kb)))  > 0._rdp) then
      continue                     !on sort si x n'est pas dans le bief ib
    else if (present(y)) then  !on cherche la section la plus proche
      isect = is1(kb)
      do is = is1(kb)+1, is2(kb)
         if ( abs(x-xgeo(is)) < abs(x-xgeo(isect)) ) isect = is
      enddo
      if (abs(xgeo(isect)-x) < y) return  !on accepte la section la plus proche si elle l'est assez
    else                 !on cherche une correspondance exacte
      do is=is1(kb),is2(kb)
         if ( abs(x-xgeo(is)) < 1._rdp) then
            isect=is
            return
         endif
      enddo
   endif
   write(0,'(a,f8.2,a,i3,a)') ' >>>> ERREUR (fonction ISECT)  : il n''y a pas de section a l''abscisse ', &
                              x,' dans le bief ',ib,' <<<<'
   stop 193
end function ISect



function random(a,b)
! générateur de nombres pseudo-aléatoires de distribution uniforme sur [a,b]
   use i_Parametres, only : rdp
!prototype
   real(kind=rdp), intent(in) :: a, b
   real(kind=rdp) :: random
!variables locales
   real(kind=rdp) :: rdom
!---------------------------------------------------------
   if (b > a) then
      call random_number(rdom)
      random = (b-a) * rdom + a
      if (random < a .or. random > b) stop 665
   else
      stop 666
   endif
end function random



function affiche(x,chain)
   use i_Parametres, only : rdp
!prototype
   real(kind=rdp), intent(in) :: x
   character(len=3) :: chain
   character(len=13) :: affiche
!---------------------------------------------------------
   if(x>=10000._rdp .OR. x<=-1000._rdp) then
      write(affiche,'(es10.3,a)') x,chain
   else
      write(affiche,'(f10.5,a)') x,chain
   endif
end function Affiche



subroutine print_Help()
!==============================================================================
!     affichage des options de la ligne de commande   
!==============================================================================
   
   write(*,*) ' '
   write(*,*) ' >>> Options de la ligne de commande de Mage_SA'
   write(*,*) ' Syntaxe : mage_sa [options] DATfile pour lancer une simulation avec DATfile ',&
               'comme fichier de paramètres numériques'
   write(*,*) '           mage_sa -v pour afficher le numéro de version'
   write(*,*) '           mage_sa -h pour afficher cette aide'
   write(*,*) ' Options disponibles :'
   write(*,*) '    -o=outDir avec outDir le sous-dossier du dossier courant où seront stockés ',&
               'les fichiers de résulats.'
   write(*,*) '         Si outDir n''existe pas il est créé.'
   write(*,*) '         Si outDir n''est pas spécifié c''est le répertoire courant' 
   write(*,*) '    -g=DATfile génère un fichier DATfile avec les paramètres numériques par défaut'
   write(*,*) '    -G=DATfile génère un fichier DATfile avec les paramètres numériques par défaut'
   write(*,*) '               et une description de ces paramètres fournie sous forme de commentaires'
   write(*,*) ' '
   write(*,*) ' >>> En cours de calcul :'
   write(*,*) ' Pour arrêter proprement les calculs : créer un fichier nommé stop dans le dossier de travail'
   write(*,*) ' Cela peut se faire avec la commande suivante exécutée dans un autre terminal : echo 0 > stop'
   write(*,*) ' Pour suspendre les calculs : créer un fichier nommé pause dans le dossier de travail'
   write(*,*) ' Pour reprendre les calculs suspendus : créer un fichier nommé go dans le dossier de travail'
   write(*,*) ' Ces fichiers sont détruits une fois lus par Mage_SA.'

end subroutine print_Help



subroutine create_DATfile(filename,blong)
!==============================================================================
!   Génération d'un fichier de paramètres avec les valeurs par défaut 
!==============================================================================
   ! prototype
   character(len=*), intent(in) :: filename
   logical, intent(in) :: blong
   ! variables locales
   integer :: lu

   open(newunit=lu,file=trim(filename), form='formatted', status='new')
   write(lu,'(a)') '* ### Fichier généré automatiquement par la commande mage_sa -g ###'
   write(lu,'(a)') '*'
   write(lu,'(a)') '* NB : toute ligne commençant pas une * est un commentaire'
   write(lu,'(a)') '*'
   if (blong) write(lu,'(a)') '* Nom de base du fichier REP des simulations de MAGE'
   write(lu,'(a)') 'basename =<à compléter>'
   if (blong) write(lu,'(a)') '* Nom du dossier de référence pour récupérer la simulation MAGE'
   write(lu,'(a)') 'dossier_simulation =ref_simulation'
   if (blong) write(lu,'(a)') '* Nom du dossier de référence pour récupérer la simulation de calage'
   write(lu,'(a)') 'dossier_calage =ref_calage'
   if (blong) write(lu,'(a)') '* Chemin complet du solveur MAGE à utiliser'
   write(lu,'(a)') 'solveur =~/bin/mage_install/7.y/mage'
   if (blong) write(lu,'(a)') '* Température initiale pour l''algorithme du Recuit-Simulé'
   write(lu,'(a)') 'temperature_initiale = 1.0'
   if (blong) write(lu,'(a)') '* Taux de décroissance de la température de l''algorithme du Recuit-Simulé'
   write(lu,'(a)') 'taux_decroissance_temperature = 0.05'
   write(lu,'(a)') 'nb_idem = 10'
   if (blong) write(lu,'(a)') '* Arrondi appliqué aux consignes ; 0.001 -> 3 décimales'
   write(lu,'(a)') 'arrondi_consigne = 0.001'
   if (blong) write(lu,'(a)') '* Échelle : facteur appliqué au rayon de variation admissible pour chaque consigne'
   if (blong) write(lu,'(a)') '*           Par défaut ce rayon est Wmax - Wmin'
   write(lu,'(a)') 'echelle = 1.'
   if (blong) write(lu,'(a)') '* Pénalisation : coefficient de pénalisation des contraintes ; valeur par défaut : 10^6'
   write(lu,'(a)') 'penalisation = 0.'
   if (blong) write(lu,'(a)') '* seuil_amelioration = amélioration minimale pour retenir une nouvelle estimation '
   if (blong) write(lu,'(a)') '*                      dans l''algorithme du Recuit-Simulé ; valeur par défaut : 0'
   if (blong) write(lu,'(a)') 'seuil_amelioration = 0.0'

   if (blong) write(lu,'(a)') '*'
   if (blong) write(lu,'(a)') '* Type de fonction-objectif ; les valeurs possibles sont :'
   if (blong) write(lu,'(a)') '*      surface_EcartMax -> maximisation de l''écart surface Max - surface Min'
   if (blong) write(lu,'(a)') '*      surface_Min      -> recherche de la surface minimale'
   if (blong) write(lu,'(a)') '*      surface_Max      -> recherche de la surface maximale'
   if (blong) write(lu,'(a)') '*      calage_ENV       -> calage sur des niveaux et débits max observés'
   if (blong) write(lu,'(a)') '*      calage_FIN       -> calage sur des niveaux et débits (répartition mineur/majeur)'
   if (blong) write(lu,'(a)') '*                          d''état final observés'
   if (blong) write(lu,'(a)') '*      Monte-Carlo      -> génération pseudo-aléatoire de nbok simulation satisfaisant '
   if (blong) write(lu,'(a)') '*                          les contraintes et le calage'
   write(lu,'(a)') 'FOtype = surface_EcartMax'
   if (blong) write(lu,'(a)') '* nbok : Nombre de simulations pour Monte-Carlo'
   write(lu,'(a)') 'nbok = 1000'
   write(lu,'(a)') '*'
   if (blong) write(lu,'(a)') '* Géométrie : rayon de variation admissible de la cote du fond ; valeur par défaut : 0'
   write(lu,'(a)') 'geometrie =0.'
   write(lu,'(a)') '*'
   if (blong) write(lu,'(a)') '* Strickler : début de la liste des contraintes sur les stricklers, définies tronçon par tronçon'
   write(lu,'(a)') '<strickler>'
   write(lu,'(a)') '*  ib x_debut x_fin K1_min K1_max K2_min K2_max K1_1 K2_1 K1_2 K2_2'
   if (blong) write(lu,'(a)') '*  n° bief, Pk début tronçon Pk fin du tronçon, Strickler min et max du lit mineur'
   if (blong) write(lu,'(a)') '*                                               Strickler min et max du lit majeur'
   if (blong) write(lu,'(a)') '*  Strickler mineur et majeur de départ pour la consigne n°1'
   if (blong) write(lu,'(a)') '*  Strickler mineur et majeur de départ pour la consigne n°2'
   if (blong) write(lu,'(a)') '* exemple (Saar - 6 tronçons)'
   if (blong) write(lu,'(a)') '  1  500.0   1700.0      22. 28. 12. 18. 25. 15. 25. 15.'
   write(lu,'(a)') '  1 1700.0   2900.0      22. 28. 12. 18. 25. 15. 25. 15.'
   write(lu,'(a)') '  1 2900.0   4100.0      22. 28. 12. 18. 25. 15. 25. 15.'
   write(lu,'(a)') '  1 4100.0   5300.0      22. 28. 12. 18. 25. 15. 25. 15.'
   write(lu,'(a)') '  1 5300.0   6500.0      22. 28. 12. 18. 25. 15. 25. 15.'
   write(lu,'(a)') '  1 6500.0   7600.0      22. 28. 12. 18. 25. 15. 25. 15.'
   if (blong) write(lu,'(a)') '* Strickler : fin de la liste'
   write(lu,'(a)') '</strickler>'   
   write(lu,'(a)') '*'
   if (blong) write(lu,'(a)') '* Erreur calage : valeur maximale admissible'
   write(lu,'(a)') 'erreur_calage = 0.1'
   if (blong) write(lu,'(a)') '* coeff_calage_Z : poids de l''erreur en cote'
   write(lu,'(a)') 'coeff_calage_Z = 1.'
   if (blong) write(lu,'(a)') '* coeff_calage_Q : poids de l''erreur en débit'
   write(lu,'(a)') 'coeff_calage_Q = 0.'
   if (blong) write(lu,'(a)') '* Données de calage en Z : début de liste'
   write(lu,'(a)') '<calage_Z>'
   write(lu,'(a)') '* n°_bief  Pk  Z_observé'
   if (blong) write(lu,'(a)') '* Exemple (Saar - 3 laisses de crue)'
   if (blong) write(lu,'(a)') '   1 7600.0 139.52' 
   if (blong) write(lu,'(a)') '   1 4100.0 137.18 '
   if (blong) write(lu,'(a)') '   1 2300.0 135.39 '
   if (blong) write(lu,'(a)') '* Données de calage en Z : fin de liste'
   write(lu,'(a)') '</calage_z>'
   write(lu,'(a)') '*'
   if (blong) write(lu,'(a)') '* optionnel : Données de calage en débit : début de liste'
   write(lu,'(a)') '<calage_q>'
   if (blong) write(lu,'(a)') '* n°_bief  Pk  Z_observé'
   if (blong) write(lu,'(a)') '* Données de calage en débit : fin de liste'
   write(lu,'(a)') '</calage_q>'
end subroutine create_DATfile


subroutine print_Version(lu)
!==============================================================================
!   Écriture des informations de version sur l'unité logique lu
!==============================================================================
   ! prototype
   integer, intent(in) :: lu

   write(lu,'(a)') 'Programme Mage_SA (Analyse de Sensibilité pour MAGE)'
   write(lu,'(a)') 'Version '//version_string//' pour GNU/Linux'
   write(lu,'(a)') 'Auteur : Jean-Baptiste FAURE - Irstea - 2005-2014'
   write(lu,'(a)') 'Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200'
   write(lu,'(a)') ' '
   write(lu,'(a)')  trim(garantie1)
   write(lu,'(a)')  trim(garantie2)
   write(lu,'(a)') ' '

end subroutine print_Version
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine init_random_seed()
   use iso_fortran_env, only: int64
   implicit none
   integer, allocatable :: seed(:)
   integer :: i, n, un, istat, dt(8), pid
   integer(int64) :: t
   
   call random_seed(size = n)
   allocate(seed(n))

   call system_clock(t)
   if (t == 0) then
      call date_and_time(values=dt)
      t = (dt(1) - 1970) * 365_int64 * 24 * 60 * 60 * 1000 &
           + dt(2) * 31_int64 * 24 * 60 * 60 * 1000 &
           + dt(3) * 24_int64 * 60 * 60 * 1000 &
           + dt(5) * 60 * 60 * 1000 &
           + dt(6) * 60 * 1000 + dt(7) * 1000 &
           + dt(8)
   end if
   pid = getpid()
   t = ieor(t, int(pid, kind(t)))
   do i = 1, n
      seed(i) = lcg(t)
   end do
   call random_seed(put=seed)
end subroutine init_random_seed
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
function lcg(s)
! This simple PRNG might not be good enough for real work, but is
! sufficient for seeding a better PRNG.
   use iso_fortran_env, only: int64
   implicit none
   integer :: lcg
   integer(int64) :: s
   if (s == 0) then
     s = 104729
   else
     s = mod(s, 4294967296_int64)
   end if
   s = mod(s * 279470273_int64, 4294967291_int64)
   lcg = int(mod(s, int(huge(0), int64)), kind(0))
end function lcg

end program mage_SA



