##############################################################################
#                                                                            #
#                           PROGRAMME MAGE                                   #
#                                                                            #
# Auteur : Jean-Baptiste FAURE - Irstea - 1989-2013                          #
# Dépôt APP : en cours                                                       #
# Licence : propriétaire jusqu'à nouvel ordre                                #
#                                                                            #
#                                                                            #
##############################################################################
#######################################################################
#                                                                     #
#                              Makefile                               #
#                                                                     #
#######################################################################

.SUFFIXES: .o .f90 .f .for

#---------------------------------------------------------------------#
# PROGRAM NAME
#---------------------------------------------------------------------#
PROG_NAME	= Main

#---------------------------------------------------------------------#
# Directories
#---------------------------------------------------------------------#
machine := $(shell uname -n)
WORKDIR  =$(PWD)
EXEDIR   =$(HOME)/bin/mage_install/master
ifeq ($(machine),LYP1120)
	base = $(HOME)#initialement $(HOME)
else
	base =$(HOME)/travail/local
endif
BINDIR  =$(base)/sbin/omp
tmp	=$(base)/sbin

geom = 120x25
#---------------------------------------------------------------------#
# Source and Objects files
#---------------------------------------------------------------------#

EXECUTABLE	= $(EXEDIR)/$(PROG_NAME)

#---------------------------------------------------------------------#
# CPP compiler and Flags
#---------------------------------------------------------------------#
FC	= gfortran
parallel = openmp

ifeq ($(parallel),openmp)
# NB: utiliser ulimit -s 500000 avant d'exécuter mage_sa
#FFLAGS	= -fopenmp -O3 -fimplicit-none -march=native -fcheck=all
FFLAGS	= -fopenmp -Ofast -ftree-parallelize-loops=2 -fimplicit-none -march=core2 #-g  -fbacktrace  -fcheck=all  -Wall  #-fno-range-check   #pg pour efficacite -pg #-fcheck=bounds
LFLAGS	= -fopenmp -march=core2 #-pg # ajouter -pg pour etudier efficacite
else
FFLAGS	= -O3 -fimplicit-none -march=native  #-fcheck=all -fno-automatic   #pour test efficacite-pg -fno-range-check
LFLAGS	= -march=native  # pour test efficacite -pg
endif
#module dirs
MFLAGS	= -J$(BINDIR)/



#---------------------------------------------------------------------#
# rules of compilation
#---------------------------------------------------------------------#
$(BINDIR)/$(PROG_NAME).o: $(PROG_NAME).f90 mage_sa.mak
	@echo "########################################"
	@echo "##  compiling $< ..."
	$(FC) $(FFLAGS) $(MFLAGS) -o $@ -c $<
	@echo "##"
	@echo

#---------------------------------------------------------------------#
# rule of linkage
#---------------------------------------------------------------------#
$(EXECUTABLE):  $(BINDIR)/$(PROG_NAME).o mage_sa.mak
	@echo
	@echo "########################################"
	@echo "## creating executable..."
	$(FC) -o $(EXECUTABLE) $(LFLAGS) $(OBJETS) $(BINDIR)/$(PROG_NAME).o
	@echo "########################################"
	@echo "## creation of executable succeeded ! ##"
	@echo "########################################"
	-rm $(BINDIR)/*.o
	-rm $(BINDIR)/*.mod
	@echo "all cleaned"
	@echo

#---------------------------------------------------------------------#
# Build all
#---------------------------------------------------------------------#
build: $(EXECUTABLE) mage_sa.mak
all: rebuild run

#---------------------------------------------------------------------#
# Rebuild all
#---------------------------------------------------------------------#
rebuild: clean build
#---------------------------------------------------------------------#
# clean
#---------------------------------------------------------------------#
clean:
	
#---------------------------------------------------------------------#
# run 
#---------------------------------------------------------------------#
define generateScript
@echo export MAGE_LIC=$(HOME)/.mage/ >>tmp1
@echo export GFORTRAN_UNBUFFERED_ALL=true >>tmp1
@echo nice -0 $(EXECUTABLE) >> tmp1
@echo echo Appuyer sur la touche Entrée pour fermer ce terminal >> tmp1
@echo read tmp >> tmp1

mv tmp1  $(tmp)/run_$(PROG_NAME)
chmod +x $(tmp)/run_$(PROG_NAME)
-rm tmp?
endef

run: build
	$(generateScript)
	#konsole --noclose -e $(tmp)/run_$(PROG_NAME) &
	gnome-terminal --title="Incertitudes & Calage" --geometry $(geom) -e $(tmp)/run_$(PROG_NAME) 

	
calage: build
	@echo \#!/bin/sh >tmp1
	@echo cd $(base)/Mage_Test/Donnees/Proust/calage >>tmp1
	@echo cp cv6simfr_ref.REP cv6simfr.REP  >>tmp1
	$(generateScript)
	gnome-terminal --title="Incertitudes & Calage" --geometry $(geom) -e $(tmp)/run_$(PROG_NAME) 

saar: build
	@echo \#!/bin/sh >tmp1
	@echo cd $(base)/Mage_Test/Donnees/Saar/incertitude >>tmp1
	#@echo cp Saar_ref.REP Saar.REP  >>tmp1
	@echo cp Saar_ref.TAL Saar.TAL  >>tmp1
	@echo cp Saar_ref.TAL ./0/Saar.TAL >>tmp1
	@echo cp Saar_ref.TAL ./1/Saar.TAL >>tmp1
	@echo cp Saar_ref.TAL ./2/Saar.TAL >>tmp1
	@echo cp Saar_ref.TAL ./3/Saar.TAL >>tmp1
	$(generateScript)
	gnome-terminal --title="Incertitudes & Calage" --geometry $(geom) -e $(tmp)/run_$(PROG_NAME) 

saar2: build
	@echo \#!/bin/sh >tmp1
	@echo cd $(base)/Mage_Test/Donnees/Saar/saarLong >>tmp1
	#@echo cp Saarlong_Ref.REP Saarlong.REP  >>tmp1
	@echo cp Saarlong_Ref.TAL Saarlong.TAL  >>tmp1
	@echo cp Saarlong_Ref.TAL ./0/Saarlong.TAL  >>tmp1
	$(generateScript)
	gnome-terminal --title="Incertitudes & Calage" --geometry $(geom) -e $(tmp)/run_$(PROG_NAME) 

demoAM: build
	@echo \#!/bin/sh >tmp1
	@echo cd $(base)/Mage_Test/Donnees/Demo_AM0/incertitudes >>tmp1
	@echo cp DemoAM_ref.TAL DemoAM.TAL  >>tmp1
	@echo cp DemoAM_ref.TAL ./0/DemoAM.TAL >>tmp1
	$(generateScript)
	gnome-terminal --title="Incertitudes & Calage" --geometry $(geom) -e $(tmp)/run_$(PROG_NAME) 

