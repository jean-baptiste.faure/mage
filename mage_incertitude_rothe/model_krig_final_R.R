# nom du fichier ou mettre le resultat du terminal de R
sink("krig_final_R.txt")
# mettre le fichier dans le dossier de sortie et des données de mage_sa 
#(car lit ech_krig.txt  fichier cree en cours d execution)
resultat <- read.table("ech_krig.txt", header=FALSE, quote="\"")

dim=length(resultat[1,])-1
# verifier que ces deux packages sont bien dans la version de R sinon soit les installer soit faire un lien en debut de ficher avec une 
# bibliotheque qui les contient
require('DiceKriging')
require('DiceOptim')

for (i in 1:(dim-1))
{
  if (abs(max(resultat[,i])-min(resultat[,i]))>1)
  {
    valmin[i]=floor(min(resultat[,i]))  
    valmax[i]=ceiling(max(resultat[,i]))     
  }
  else
  {
    valmin[i]=min(resultat[,i])  
    valmax[i]=max(resultat[,i])   
  }
}
# modele de krigeage  de surface inondee
model1<-km(~1, design=resultat[,1:dim-1], response=resultat[,dim],control=list(pop.size=1000,max.generations=100), covtype="powexp")
#modele de krigeage d erreur de calage
model2<-km(~1, design=resultat[,1:dim-1], response=resultat[,dim+1],control=list(pop.size=1000,max.generations=100), covtype="powexp")


#sauvegarde resultats
covar_mat=covMatrix(model1@covariance, X=model1@X, noise.var=model1@noise.var)
coeff_surf=ginv(model1@T)%*%model1@z
coeff_cal=ginv(model2@T)%*%model2@z
res_cal=coef(model2)
res_surf=coef(model1)
ksi_surf=cbind(res_surf$shape,res_surf$range)
ksi_cal=cbind(res_cal$shape,res_cal$range)
moy_var_cal=c(res_cal$trend,res_cal$sd2)
moy_var_surf=c(res_surf$trend,res_surf$sd2)


#ecriture des resultats
write(coeff_surf,file='coeff_surf.txt',append=FALSE) # coeff fonction evaluation modele surface
write(coeff_cal,file='coeff_cal.txt',append=FALSE) #coeff fonction evaluation modele surface
write(ksi_surf,file='ksi_surf.txt',append=FALSE) #hyperparametres modele surface
write(ksi_cal,file='ksi_cal.txt',append=FALSE) #hyperparametres modele calage
write(moy_var_cal,file='moy_var_cal.txt',append=FALSE) #moyenne et variance erreur calage
write(moy_var_surf,file='moy_var_surf.txt',append=FALSE) #moyenne et variance surface inondee


sink()
