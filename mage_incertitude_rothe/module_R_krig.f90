module R_krig
use i_Parametres
use rand_num
use linalg
implicit none
integer::nb_param, nb_echantillon
! parametres a importer de R suaf echantillon 
real(kind=rdp),allocatable :: coeff_cal(:),echantillon(:,:), valeurs_cal(:),ksi_cal(:)
real(kind=rdp),allocatable :: coeff_surf(:), valeurs_surf(:),ksi_surf(:)
real(kind=rdp) :: sigma_cal,mu_cal,sigma_surf,mu_surf
contains

! routine pour desallouer les variables
subroutine reset_krig
use i_Parametres
implicit none
nb_param=0
nb_echantillon=0
if(  allocated(coeff_cal)) deallocate(coeff_cal)
if (allocated(echantillon)) deallocate(echantillon)
if( allocated(valeurs_cal))deallocate(valeurs_cal)
if( allocated(ksi_cal))deallocate(ksi_cal)
if( allocated(ksi_surf))deallocate(ksi_surf)
if( allocated(coeff_surf))deallocate(coeff_surf)
if( allocated(valeurs_surf))deallocate(valeurs_surf)
end subroutine reset_krig

! Portage des variables de R en Fortran
subroutine init_krig(ech,val_cal,val_surf)
use i_Parametres
implicit none
real(kind=rdp) :: ech(:,:),val_cal(:),val_surf(:)
call reset_krig()
nb_echantillon=size(val_cal,dim=1)
nb_param=size(ech(:,1))
! allocation selon le nombre echantillno
allocate(coeff_cal(nb_echantillon))
allocate(echantillon(nb_param,nb_echantillon))
allocate(valeurs_cal(nb_echantillon))
allocate(ksi_cal(2*nb_param))
allocate(ksi_surf(2*nb_param))
allocate(coeff_surf(nb_echantillon))
allocate(valeurs_surf(nb_echantillon))
echantillon=ech(1:nb_param,:)
valeurs_surf=val_surf
valeurs_cal=val_cal
coeff_surf=0._rdp
coeff_cal=0._rdp
! lecture fichiers textes
open(23,file='moy_var_cal.txt')
read(23,*) mu_cal,sigma_cal
close(23)
open(18,file='moy_var_surf.txt')
read(18,*) mu_surf,sigma_surf
close(18)
open(19,file='coeff_cal.txt')
read(19,*) coeff_cal
close(19)
open(20,file='coeff_surf.txt')
read(20,*) coeff_surf
close(20)
open(21,file='ksi_cal.txt')
read(21,*) ksi_cal
close(21)
open(22,file='ksi_surf.txt')
read(22,*) ksi_surf
close(22)
end subroutine init_krig

subroutine init_krig2(ech,val_cal,val_surf)
use i_Parametres
implicit none
real(kind=rdp) :: ech(:,:),val_cal(:),val_surf(:)
call reset_krig()
nb_echantillon=size(val_cal,dim=1)
nb_param=size(ech(:,1))
! allocation selon le nombre echantillno
allocate(coeff_cal(nb_echantillon))
allocate(echantillon(nb_param,nb_echantillon))
allocate(valeurs_cal(nb_echantillon))
allocate(ksi_cal(2*nb_param))
allocate(ksi_surf(2*nb_param))
allocate(coeff_surf(nb_echantillon))
allocate(valeurs_surf(nb_echantillon))
echantillon=ech(1:nb_param,:)
valeurs_surf=val_surf
valeurs_cal=val_cal
coeff_surf=0._rdp
coeff_cal=0._rdp
! lecture fichiers textes
!open(23,file='moy_var_cal.txt')
!read(23,*) mu_cal,sigma_cal
!close(23)
open(18,file='moy_var_surf.txt')
read(18,*) mu_surf,sigma_surf
close(18)
!open(19,file='coeff_cal.txt')
!read(19,*) coeff_cal
!close(19)
open(20,file='coeff_surf.txt')
read(20,*) coeff_surf
close(20)
!open(21,file='ksi_cal.txt')
!read(21,*) ksi_cal
!close(21)
open(22,file='ksi_surf.txt')
read(22,*) ksi_surf
close(22)
end subroutine init_krig2
! fonction evaluation du meta modele de calage
function eval_cal(param)
use i_Parametres
implicit none 
real(kind=rdp) :: eval_cal
real(kind=rdp) ,intent(in) :: param(:)
integer ::i
eval_cal=0._rdp

do i=1,nb_echantillon
        eval_cal=eval_cal+coeff_cal(i)*covar(echantillon(:,i),param,ksi_cal)*sigma_cal
end do

eval_cal=eval_cal+mu_cal
end function eval_cal

! fonction evaluation du metamodele de surface inondee
function eval_surf(param)
use i_Parametres
implicit none 
real(kind=rdp) :: eval_surf
real(kind=rdp) ,intent(in) :: param(:)
integer ::i,n
eval_surf=0._rdp

do i=1,nb_echantillon
              
        eval_surf=eval_surf+coeff_surf(i)*covar(echantillon(:,i),param,ksi_surf)*sigma_surf
end do

eval_surf=eval_surf+mu_surf
end function eval_surf

! fonction de covariance utilisee pour les evaluations
function covar(x,y,xi)
use i_Parametres
implicit none
integer::i
real(kind=rdp):: x(:),y(:),covar,xi(:)
covar=0._rdp
do i=1,size(x)
        if (xi(nb_param+i)>0.0_rdp .and. x(i)>0.0_rdp) then
        covar=covar-((abs(x(i)-y(i))/xi(nb_param+i))**xi(i))
        end if
end do
covar=exp(covar)
end function covar

! fonction evaluation de la surface calee (comme foo surface mais en metamodele)
function eval_surf_cal(ww)
use i_Parametres
implicit none
real(kind=rdp),intent(in)::ww(:)
real(kind=rdp)::err_cal
real(kind=rdp)::eval_surf_cal
err_cal=eval_cal(ww(1:nb_param))
if (err_cal<1._rdp)  then
        eval_surf_cal=eval_surf(ww(1:nb_param))
else
        eval_surf_cal=0._rdp
end if
end function eval_surf_cal

! fonction evaluation de la surface comme eval_surf_cal
! mais est utilisee par le recuit pour trouver la valeur minimale non nulle !
function eval_surf_cal_min(ww)
use i_Parametres
implicit none
real(kind=rdp),intent(in)::ww(:)
real(kind=rdp)::err_cal
real(kind=rdp)::eval_surf_cal_min
err_cal=eval_cal(ww(1:nb_param))
if (err_cal<1._rdp)  then
        eval_surf_cal_min=eval_surf(ww(1:nb_param))
else
         !si solution pas calee on met une valeur sup et pas 0
        eval_surf_cal_min=10*valeurs_surf(1)
end if
end function eval_surf_cal_min

! fonction evaluation de la surface comme eval_surf_cal
! mais est utilisee par le recuit pour trouver la valeur minimale non nulle !
function eval_surf_min(ww)
use i_Parametres
implicit none
real(kind=rdp),intent(in)::ww(:)
real(kind=rdp)::err_cal
real(kind=rdp)::eval_surf_min

        eval_surf_min=eval_surf(ww(1:nb_param))

end function eval_surf_min

! fonction prenant deux jeux de parametres et donne l ecart des surfaces (comme foo ecart en metamodele)
function eval_ecart(ww)
use i_Parametres
implicit none
real(kind=rdp),intent(in)::ww(:)
real(kind=rdp)::foo1,foo2,err_cal1,err_cal2
real(kind=rdp)::eval_ecart
err_cal1=eval_cal(ww(1:nb_param))
err_cal2=eval_cal(ww(nb_param+1:2*nb_param))
if (err_cal1<1._rdp .and. err_cal2<1._rdp) then
        foo1=eval_surf(ww(1:nb_param))
        foo2=eval_surf(ww(nb_param+1:2*nb_param))
        eval_ecart=abs(foo1-foo2)/(valeurs_surf(1))*100._rdp
else
        eval_ecart=0.0
end if
end function eval_ecart


end module R_krig
