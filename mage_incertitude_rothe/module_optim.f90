
! module utilise pour le krigeage genere a partir de solutions calee uniquement
module kriging

use i_Parametres
use recuit_simule
use recuit_simule1
use rand_num
use linalg
implicit none
! variables du module
integer::nb_param, nb_echantillon
real(kind=rdp), parameter :: accuracy = 0.01_rdp
real(kind=rdp) ::EI_min,EI_max,EI_min_temp,EI_max_temp
real(kind=rdp),allocatable :: coeff(:),echantillon(:,:), valeurs(:),ksi(:),correlation(:,:),invcor(:,:)
real(kind=rdp),allocatable :: coeff_bis(:),echantillon_bis(:,:), valeurs_bis(:),ksi_bis(:)
real(kind=rdp),allocatable :: correlation_bis(:,:),invcor_bis(:,:)
real(kind=rdp) :: sigma,mu,sigma_bis,mu_bis,det_cor
contains

! routine pour rajouter un point dans le meta modele
subroutine add_point(param,valeur)
use i_Parametres
implicit none
real(kind=rdp) :: valeur,param(nb_param),param_test
integer :: i
! booleen pour tester si le point existe deja
!car le modele n admet pas de doublon au niveau de l 'echantillon initial (matrice non inversible ...)
logical:: exist_point
exist_point=.false.
!test si le point est deje dans l echantillon
do i=1,nb_echantillon
param_test=dist_euclide(echantillon(:,i),param)
if (param_test<1.e-15_rdp) then
exist_point=.true.
continue
end if
end do

if (.not. exist_point) then
!reset des parametres de krigeage bis (utilisation param bis en intermediaire avant de generer les nouveaux 
deallocate(coeff_bis,echantillon_bis,valeurs_bis)
! reallocation des param bis en augmentant le nombre d echantillons 
allocate(coeff_bis(nb_echantillon+1),valeurs_bis(nb_echantillon),echantillon_bis(nb_param,nb_echantillon))
!transfert des parametres dummy
coeff_bis=coeff
valeurs_bis=valeurs
echantillon_bis=echantillon
nb_echantillon=nb_echantillon+1
! integration du nouveaux points dans les nouveaux param realloues
deallocate(coeff,echantillon,valeurs)
allocate(coeff(nb_echantillon+1),valeurs(nb_echantillon),echantillon(nb_param,nb_echantillon))
valeurs(1:nb_echantillon-1)=valeurs_bis
valeurs(nb_echantillon)=valeur
echantillon(:,1:nb_echantillon-1)=echantillon_bis
echantillon(:,nb_echantillon)=param

! calcul des coefficients pour la fonction evaluation du modele 
call coeff_kriging()
else 
! message  d erreur si le point existe deja 
write(*,*) 'Le point fait deja partie de l echantillon'
write(8,*) 'le point fait deja partie de l echantillon'
end if
end subroutine add_point


! fonction calculant l erreur de validation croisee (LOO leave one out error)
! c est l erreur faite entre la valeur d un point du  metamodele par rapport a son interpolation 
! si l on calibre le meta modele sans le point
function erreur_cross_validation(ksi_test)
use i_Parametres
implicit none
integer::j
real(kind=rdp)::erreur_cross_validation
real(kind=rdp),intent(in) :: ksi_test(:)
! somme des loo en recalibrant un meta modele avec les param bis et comparaison avec la vraie valeur
ksi_bis=ksi_test
erreur_cross_validation=0._rdp
valeurs_bis=valeurs(1:nb_echantillon-1)
! boucle sur le nombre d echantillno
        do j=1,nb_echantillon
               ! different traitement  selon position dans le vecteur
                if (j==1) then
                        valeurs_bis=valeurs(2:nb_echantillon)
                        echantillon_bis=echantillon(:,2:nb_echantillon)
                   
                else if (j==nb_echantillon) then
                        valeurs_bis(1:j-1)=valeurs(1:j-1)
                        echantillon_bis(:,1:j-1)=echantillon(:,1:j-1)
                else
                        valeurs_bis(1:j-1)=valeurs(1:j-1)
                        valeurs_bis(j:nb_echantillon-1)=valeurs(j+1:nb_echantillon)
                        echantillon_bis(:,j:nb_echantillon-1)=echantillon(:,j+1:nb_echantillon)
                        echantillon_bis(:,1:j-1)=echantillon(:,1:j-1)
                end if
                ! calcul des coefficients evaluation
                   call coeff_kriging_bis()
                   ! calcul de l erreur pour le point j
                   erreur_cross_validation=erreur_cross_validation+abs(eval_bis(echantillon(:,j))-valeurs(j))
        end do
end function erreur_cross_validation





! routine pour desallouer les variables
subroutine reset_krig
use i_Parametres
implicit none
nb_param=0
nb_echantillon=0
if(  allocated(coeff)) deallocate(coeff)
if (allocated(echantillon)) deallocate(echantillon)
if( allocated(valeurs))deallocate(valeurs)
if( allocated(ksi_bis))deallocate(ksi_bis)
if( allocated(ksi))deallocate(ksi)
if( allocated(coeff_bis))deallocate(coeff_bis)
if( allocated(echantillon_bis))deallocate(echantillon_bis)
if( allocated(valeurs_bis))deallocate(valeurs_bis)
if ( allocated(correlation)) deallocate (correlation)
if (allocated(invcor)) deallocate (invcor)
end subroutine reset_krig

! routine calculant les coefficients pour la fonction evaluation du meta modele
subroutine coeff_kriging()
use i_Parametres
use linalg
implicit none
integer:: i
real(kind=rdp) :: vec1(nb_echantillon),vec2(nb_echantillon)
real(kind=rdp)::A(nb_echantillon+1,nb_echantillon+1),B(nb_echantillon+1)
! creation de la matrice de covariance  pour resolution du systeme
call matrice_cor()
! calcul de la moyenne et du parametre sigma de la covariance 
vec2=1._rdp
mu=dot_product(matmul(vec2,invcor),valeurs)/(dot_product(matmul(vec2,invcor),vec2))
vec1=valeurs-mu
sigma=1._rdp/nb_echantillon*dot_product(matmul(vec1,invcor),vec1)

! Creation du systeme a resoudre
! vecteur des valeurs simulees (second membre du systeme)
do i=1,nb_echantillon
        B(i)=valeurs(i)
end do
 
! matrice de covariance + parametre moyen	
A(1:nb_echantillon,1:nb_echantillon)=sigma*correlation
A(nb_echantillon+1,:)=1._rdp
A(:,nb_echantillon+1)=1._rdp
A(nb_echantillon+1,nb_echantillon+1)=0._rdp
B(nb_echantillon+1)=0._rdp

! resolution du systeme Ax=B donnant les coefficients  (dernier coefficient  moyenne )
coeff=0._rdp
coeff=resolution_syst(A,B)

end subroutine coeff_kriging
 

! routine calculant la matrice de covariance 
subroutine matrice_cor()
use i_Parametres
implicit none
integer:: i,j
! deallocation des variables de covariance
if (allocated(correlation)) deallocate(correlation)
allocate (correlation(nb_echantillon,nb_echantillon))
! remplissage de la matrice avec l echantillon et la fonction covariance
do j=1,nb_echantillon
        do i=j,nb_echantillon
                correlation(i,j)=covar(echantillon(:,i),echantillon(:,j),ksi)
                correlation(j,i)=correlation(i,j)
        end do
end do
! allocation et calcul de l'inverse de la matrice de covariance 
if (allocated(invcor)) deallocate(invcor)
allocate (invcor(nb_echantillon,nb_echantillon))
invcor=inversion_cholesky_det(correlation,det_cor)
end subroutine matrice_cor

! meme fonction que matrice_cor mais pour les parametres bis
subroutine matrice_cor_bis()
use i_Parametres
implicit none
integer:: i,j
if (allocated(correlation_bis)) deallocate(correlation_bis)
allocate (correlation_bis(nb_echantillon-1,nb_echantillon-1))
do i=1,nb_echantillon-1
        do j=i,nb_echantillon-1
                correlation_bis(i,j)=covar(echantillon_bis(:,i),echantillon_bis(:,j),ksi)
                correlation_bis(j,i)=correlation_bis(i,j)
        end do
end do
if (allocated(invcor_bis)) deallocate(invcor_bis)
allocate (invcor_bis(nb_echantillon-1,nb_echantillon-1))
invcor_bis=inversion(correlation_bis)
end subroutine matrice_cor_bis


! maximisation et minimisation du meta modele par recuit simule
subroutine max_meta(n_elite,val_max,ech_max,val_min,ech_min)
use i_Parametres
use i_Consigne
use recuit_simule
use Genetic_algorithm
implicit none 
integer, intent(in) :: n_elite
integer :: i,indice,j
real (kind=rdp) :: w_opt1(nb_param),w_opt2(nb_param)
real(kind=rdp) ::val1,val2, val_max(:),ech_max(:,:),val_min(:),ech_min(:,:),&
& w_maxi(nb_param),w_mini(nb_param)
call init_random_seed()
! initialisation avec les parametres max courant
 !indice=maxloc(valeurs,dim=1)
 !w_maxi=echantillon(:,indice)
 !val1=valeurs(indice)
 !indice=minloc(valeurs,dim=1)
 !w_mini=echantillon(:,indice)
 ! val2=valeurs(indice)

! boucle sur le nombre de solutions potentielles desirees
do i=1,n_elite

!initialisation aleatoire
do j=1,nb_param
        if (wmax(j)-wmin(j)> 1.e-15_rdp) then
                w_mini(j)=random(wmin(j),wmax(j))
                w_maxi(j)=random(wmin(j),wmax(j))
        else
                w_mini(j)=wmin(j)
                w_maxi(j)=wmin(j)
        end if
end do
val1=eval(w_maxi)
val2=eval(w_mini)

!$omp parallel 
                           !$omp single
                                         !$omp task
                                         	 	! recuit pour maximiser
                                                       nequil=1
                                                       call init_recuit(nb_param,wmin,wmax,w_maxi,val1,-1._rdp,accuracy)
                                                       call recuit_sa(w_opt1,eval)
                                                       ech_max(:,i)=w_opt1
                                                       val_max(i)=fk_opt
                                         !$omp end task
                                         !$omp task
                                         	 	!recuit pour minimiser
                                                       nequil1=1
                                                      call init_recuit1(nb_param,wmin,wmax,w_mini,val2,1._rdp,accuracy)
                                                      call recuit_sa1(w_opt2,eval)        
                                                      ech_min(:,i)=w_opt2
                                                      val_min(i)=fk_opt1
                                        !$omp end task
                           !$omp end single
!$omp end parallel

!affichage
write(*,*) 'Min',fk_opt1,'Max',fk_opt
end do

end subroutine max_meta

!fonction de calcul de l expected improvement pour trouver le minimum
function Expect_min(param)
use i_Parametres
use rand_num
implicit none
real (kind=rdp), intent(in) ::param(:)
real(kind=rdp) :: f_min,u1,s_courant,f_courant,Expect_min,pond
f_min=minval(valeurs,dim=1)
pond=0.5_rdp
f_courant=eval(param)
s_courant=ecart_type(param) 
u1=(f_min-f_courant)/s_courant
!Expect_min=pond*(f_min-f_courant)*repart_normale(u1)+(1._rdp-pond)*densite_normale(u1)*s_courant
!Expect_min=-f_courant+s_courant
Expect_min=-f_courant+max(f_courant-f_min+pond*s_courant,real(0.0,kind=rdp))**(1.2_rdp)
end function Expect_min

! fonction de calcul de l expected improvement pour trouver le maximum
function Expect_max(param)
use i_Parametres
use rand_num
implicit none
real (kind=rdp), intent(in) ::param(:)
real(kind=rdp) :: f_max,u1,s_courant,f_courant,Expect_max,pond
pond=0.5_rdp
f_max=maxval(valeurs,dim=1)
f_courant=eval(param)
s_courant=ecart_type(param)
u1=(f_courant-f_max)/s_courant
!Expect_max=pond*(f_courant-f_max)*repart_normale(u1)+(1._rdp-pond)*densite_normale(u1)*s_courant
!Expect_max=f_courant+s_courant
Expect_max=f_courant+max(-f_courant+f_max+pond*s_courant,real(0.0,kind=rdp))**(1.2_rdp)
end function Expect_max


!Routine non utilisee qui calcule l EI pour trouver le min et le max simultanement
! pas interessant car recherche independante
subroutine expected_improvement(param)
use i_Parametres
use rand_num
implicit none
real (kind=rdp) ::param(nb_param),f_min,u1,u2,f_max,s_courant,f_courant,terme1
 call init_random_seed()
f_max=maxval(valeurs,dim=1)
f_min=minval(valeurs,dim=1)
f_courant=eval(param)
s_courant=ecart_type(param)
u1=(f_min-f_courant)/s_courant
u2=(f_courant-f_max)/s_courant
terme1=repart_normale(u1)
EI_min_temp=(f_min-f_courant)*terme1+densite_normale(u1)*s_courant
EI_max_temp=(f_courant-f_max)*repart_normale(u2)+densite_normale(u2)*s_courant
if (EI_min_temp<0._rdp .or. isnan(EI_min_temp)) then
        write(*,*) 'fmin',f_min,'f_courant',f_courant,'s_courant',s_courant
        write(*,*) 'u1',u1,'terme1',repart_normale(u1),'terme2',densite_normale(u1)
              stop
end if
if (EI_max_temp<0._rdp .or. isnan(EI_max_temp)) then
        write(*,*) 'fmin',f_max,'f_courant',f_courant,'s_courant',s_courant
        write(*,*) 'u1',u2,'terme1',repart_normale(u2),'terme2',densite_normale(u2)
              stop
end if
end subroutine expected_improvement


! fonction calculant l estimation de variance du metamodele en un point
function ecart_type(x)
use i_Parametres
implicit none
integer:: i
real(kind=rdp) :: r(nb_echantillon),identite(nb_echantillon),x(nb_param),ecart_type
real(kind=rdp) :: terme1,terme2,terme3
do i=1,nb_echantillon
r(i)=covar(echantillon(:,i),x,ksi)
end do
identite=1._rdp
terme1=dot_product(matmul(r,invcor),identite)
terme2=dot_product(matmul(identite,invcor),identite)
terme3=dot_product(matmul(r,invcor),r)
ecart_type=sqrt(sigma*(1.0_rdp-terme3+(1.0_rdp-terme1)**2/terme2))
!ecart_type=sqrt(sigma*(1._rdp+(1._rdp-terme3)**2/terme2))
end function ecart_type


! meme routine que coeff_kriging mais pour les parametres bis
subroutine coeff_kriging_bis()
use i_Parametres
use linalg
implicit none
integer:: i
real(kind=rdp) :: vec1(nb_echantillon-1),vec2(nb_echantillon-1)
real(kind=rdp),allocatable::A(:,:),B(:)
! creation de la matrice pour resolution du systeme
allocate (A(nb_echantillon,nb_echantillon))
allocate (B(nb_echantillon))
call matrice_cor_bis()
vec2=1._rdp
mu_bis=dot_product(matmul(vec2,invcor_bis),valeurs)/(dot_product(matmul(vec2,invcor_bis),vec2))
vec1=valeurs-mu_bis
sigma_bis=1._rdp/nb_echantillon*dot_product(matmul(vec1,invcor_bis),vec1)
do i=1,nb_echantillon-1
        B(i)=valeurs_bis(i)
end do
A(1:nb_echantillon-1,1:nb_echantillon-1)=sigma_bis*correlation_bis
A(nb_echantillon,:)=1._rdp
A(:,nb_echantillon)=1._rdp
A(nb_echantillon,nb_echantillon)=0._rdp
B(nb_echantillon)=0._rdp

! resolution du systeme Ax=B

coeff_bis=0._rdp

coeff_bis=resolution_syst(A,B)

deallocate (A)
deallocate(B)
end subroutine coeff_kriging_bis


! fonction evaluation du metamodele
function eval(param)
use i_Parametres
implicit none 
real(kind=rdp) :: eval
real(kind=rdp) ,intent(in) :: param(:)
integer ::i
eval=0._rdp

do i=1,nb_echantillon
              
        eval=eval+coeff(i)*covar(echantillon(:,i),param,ksi)*sigma
end do

eval=eval+coeff(nb_echantillon+1)
end function eval



! fonction evaluation avec les param bis
function eval_bis(param)
use i_Parametres
implicit none 
real(kind=rdp) :: param(nb_param),eval_bis
integer ::i,n
eval_bis=0._rdp
do i=1,nb_echantillon-1
        eval_bis=eval_bis+coeff_bis(i)*covar(echantillon_bis(:,i),param,ksi_bis)*sigma_bis
end do
eval_bis=eval_bis+coeff_bis(nb_echantillon)
end function eval_bis


! fonction de covariance retenue entre les jeux de parametres x y pour les hyperparametres egaux a xi
!fonction retenue exponentielle anisotrope( la puiss et les coefficients varient selon chaque direction) 
function covar(x,y,xi)
use i_Parametres
implicit none
integer::i
real(kind=rdp):: x(:),y(:),covar,xi(:)
covar=0._rdp
do i=1,size(x)
        covar=covar+(abs(x(i)-y(i))**xi(i))*xi(nb_param+i)
end do
covar=exp(-1._rdp*covar)
end function covar

! fonction qui calcule la log vraisemblance du meta modele pour un jeu d hyperparametres ksi
function log_vraisemblance(ksi_test)
use i_Parametres
implicit none
real(kind=rdp)::log_vraisemblance,vec1(nb_echantillon),vec2(nb_echantillon)
real(kind=rdp),intent(in) ::ksi_test(:)
ksi=ksi_test
! calcul matrice de covariance relatif a ksi
call matrice_cor()
vec2=1._rdp
mu=dot_product(matmul(vec2,invcor),valeurs)/(dot_product(matmul(vec2,invcor),vec2))
vec1=valeurs-mu
sigma=1._rdp/nb_echantillon*dot_product(matmul(vec1,invcor),vec1)
! cette valeur est en fait -1*log_vraisemblance
log_vraisemblance=nb_echantillon*log(sigma)+log(det_cor)
end function log_vraisemblance

! routine optimisant les hyperparametres en calculant le maximum de vraisemblance
subroutine optim_log_vrai()
use i_Parametres
use recuit_simule
implicit none
real(kind=rdp) ::w_min(2*nb_param),w_max(2*nb_param), &
 & w_init(2*nb_param),val
 real(kind=rdp) :: w_opt(2*nb_param)
 call init_random_seed()
 !initialisation des hyperparametres pour le recuit
w_init(1:nb_param)=2._rdp
w_init(nb_param+1:2*nb_param)=1._rdp
val=log_vraisemblance(w_init)
w_min(1:nb_param)=0.01_rdp
w_min(nb_param+1:2*nb_param)=0.0000001_rdp
w_max(1:nb_param)=2._rdp
w_max(nb_param+1:2*nb_param)=0.5_rdp
! recuit simule qui minimise la fonction log_vraisemblance (mais qui maximise bien la log vraisemblance)
call init_recuit(2*nb_param,w_min,w_max,w_init,val,1._rdp,accuracy)
nequil=1
call recuit_sa(w_opt,log_vraisemblance)
ksi=w_opt
! on recalcule les parametres de covariance avec les hyperparametres trouves
call matrice_cor()
end subroutine optim_log_vrai

!routine optimisant les hyperparametres avec la validation croisee (LOO error)
! mais on retiendra plutot le max de vraisemblance moins long a calculer
subroutine krig_cross_validation(nb_iteration)
use i_Parametres
implicit none
real(kind=rdp) ::test,erreur
integer::i,j,nb_iteration
call init_random_seed()
ksi(1:nb_param)=2._rdp
ksi(nb_param+1:2*nb_param)=1._rdp
ksi_bis=ksi
!!!! Optimisation de ksi pour le max de vraisemblance ou validation croisée 
do i=0,nb_iteration!nparam

!Logvraisemblance
        test=log_vraisemblance(ksi)
        if (isnan(test)) write(*,*) 'test1', test
        if (i==0 .or. isnan(erreur)) then 
                erreur=test
                ksi_bis=ksi
                write(*,*) 'log_vrai_ini', test
        end if
        if (test<erreur ) then
                erreur=test
                ksi_bis=ksi
        end if
        do j=1,nb_param
                ksi(j)=random(1._rdp,2._rdp)
        end do
        do j=nb_param+1,2*nb_param
                ksi(j)=random(0.000001_rdp,1._rdp)
        end do
end do
ksi=ksi_bis
call matrice_cor()
write(*,*) 'log_vrai_fin',erreur
end subroutine krig_cross_validation



end module kriging

!Module pour le krigeage utilisant deux metamodele pour le calage et la surface inondee
INCLUDE 'module_R_krig.f90'

