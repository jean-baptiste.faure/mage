subroutine Monte_Carlo(siacp)
!==============================================================================
! Réalisation de nbok évaluations de flooded_Area()
! On tire des simulations au hasard dans le domaine acceptable [wmin ; wmax]
! autant de fois que nécessaire pour en avoir nbok qui satisfont le calage
!==============================================================================
   use i_Parametres, only : rdp, np, issup
   use rand_num
   use calage_error
   use ACP
   use i_Consigne, only : nc, wmin, wmax, nst, nbok,n_it_max
   implicit none
! Prototype
!
! Variables locales
   integer :: n, k, nok, i, lu
   real(kind=rdp) :: ww(np,2)
   real(kind=rdp) :: a, b, foo, err_cal,foo_max
   character(len=1200) :: texte
   logical :: bstop, bpause,siacp

!--------------------------------------------------------------------------
   write(*,*) 'Simulations pour la méthode de Monte-Carlo'
   write(8,*) 'Simulations pour la méthode de Monte-Carlo'
   write(*,*) 'test1'
   nok = 0  ;  n = 0
   foo_max=0
   ww(:,1) = (wmax + wmin)/2._rdp
   ww(:,2) = (wmax + wmin)/2._rdp
   if(siacp) call init_acp()
   do while ( n <n_it_max)
      n = n+1
      foo = Foo_Ecart_amel(ww,err_cal)
      foo_max=(surf_maxi-surf_mini)/surf_ini*100._rdp
      if (err_cal*Coeff_Penalisation < 1.0_rdp) then 
      nok = nok + 1
      !if(foo>foo_max) foo_max=foo
      end if
      write(texte,'(100(2x,f8.3))',decimal='point') ww(1:nst,1:2), min(9999.999,foo), min(9999.999,1000._rdp*err_cal)
      write(*,'(i5,a,2(2x,f8.3),5x,i4,f8.3,f8.3)',decimal='point') n,' : ', foo, err_cal, nok, real(nok)/real(n),foo_max
      write(8,'(i5,a,2(2x,f8.3),5x,i4,f8.3,f8.3)',decimal='point') n,' : ', foo, err_cal, nok, real(nok)/real(n),foo_max
      write(7,'(i5,a,a,5x,i4,f8.3,f8.3)',decimal='point') n,' : ', texte(1:len_trim(texte)), nok, real(nok)/real(n),foo_max
      if(siacp) then
     call modify_ACP_random(ww)
      else
      	      do i=1,2
         do k = 1, nc
           ww(k,i) = random(0.99_rdp*wmin(k),1.01_rdp*wmax(k))
            ww(k,i) = max (ww(k,i),wmin(k))
            ww(k,i) = min (ww(k,i),wmax(k))
           ! ww(k,i)=alea_distribution_cafe(wmin(k),wmax(k),4)
         enddo
         end do
         end if
      !Arrêt anticipé si on trouve un fichier stop, Pause si fichier pause
      inquire(file='stop',exist=bstop)
      if (bstop) then  !arrêt anticipé
         open(newunit=lu,file='stop',status='unknown')
         close(lu,status='delete')  !suppression du fichier 'stop'
         write(*,*) ' >>> interruption par l''utilisateur !!!'
         write(8,*) ' >>> interruption par l''utilisateur !!!'
         return
      endif
      inquire(file='pause',exist=bpause)
      if (bpause) call mise_en_veille()
   enddo
end subroutine Monte_Carlo


! on fait egalement un tirage mais en utilisant un hypercube latin et on travaille sur la surface 
! et non sur l ecart de surface (pas de perte d information)
subroutine Monte_carlo_surface
use i_Parametres, only : rdp, np, issup
use i_Consigne, only : nc, wmin, wmax, n_it_max
use linalg
use ACP   
use kriging
   implicit none
! Prototype
! Variables locales
   integer :: n,  nok, nmax,n_barriere,n_effec,n_faux_pos,n_faux_neg,n_vire,n_maxi
   real(kind=rdp)::echelle
   real(kind=rdp) :: moy,var,v_sup,v_min,ww_vrai(np)
   real(kind=rdp),allocatable ,dimension(:,:) :: valeurs_p,test_val
   real(kind=rdp),allocatable :: foo_sample1(:),echantillon_p1(:,:),ech(:,:),echantillon_p(:,:),foo_sample(:)
   real(kind=rdp) :: foo, err_cal,foo_best ,test,foo_initiale
   logical ::  LHS,ifkmeans
n_maxi=n_it_max
nmax=min(n_it_max/10,1000)
echelle=1
allocate (valeurs_p(int(echelle*nmax,kind=4),nc))
allocate (test_val(int(echelle*n_maxi,kind=4),2))
allocate (foo_sample1(n_maxi))
allocate (echantillon_p1(n_maxi,nc+2))
allocate (ech(nc+2,int(echelle*n_maxi,kind=4)))
!--------------------------------------------------------------------------
   write(*,*) 'Simulations Monte-carlo surface hypercube latin'
   write(8,*) 'Simulations Monte-carlo  surface hypercube latin '
   write(*,*) 'nombre de tests : ', nmax 
   write(8,*) 'nombre de tests : ', nmax
   open(10,file='monte_carlo.txt')
   n = 1
   n_barriere=100
   foo_best=0 
   nok=0
   n_vire=0
   n_faux_pos=0
   n_faux_neg=0
   n_effec=1
   foo_initiale=Foo_surface((wmin+wmax)/2._rdp,err_cal)
   v_min=foo_initiale
   v_sup=0._rdp
  LHS=.true.
  ifkmeans=.false.
  ww_vrai=wmin
!call plan_exp_ACP(valeurs_p,int(echelle*nmax,kind=4))

if(LHS) call sampling(wmin(1:nc),wmax(1:nc),valeurs_p,int(echelle*nmax,4),LHS) 
 do while ( n_effec+nok<n_maxi)
          if (LHS) then
          ! si on depasse 
          if(mod(n,nmax)==0 .and. n>1) then
          call sampling(wmin(1:nc),wmax(1:nc),valeurs_p,int(echelle*nmax,4),LHS) 
          end if
          ww_vrai(1:nc)=valeurs_p(mod(n,nmax)+1,:)
          else 
          do i=1,nc
          ww_vrai(i)=random(wmin(i),wmax(i))
          end do
          end if
          test=0._rdp
          if(n>=100 .and. ifkmeans)  test=K_means(ech(1:nc,1:n_effec-1),ech(nc+1,1:n_effec-1),valeurs_p(mod(n,nmax)+1,:),1)
          if(n<100 .or. test<1.e-03_rdp) then
                          foo=Foo_Surface(ww_vrai,err_cal)
                            ech(1:nc,n_effec)=ww_vrai(1:nc)
                            ech(nc+1,n_effec)=err_cal
                            n_effec=n_effec+1
          if (err_cal*Coeff_penalisation<1.0_rdp) then 
                  nok=nok+1
                  foo_sample1(nok)=foo
                  echantillon_p1(nok,nc+1)=foo
                  echantillon_p1(nok,1:nc)=ww_vrai(1:nc)
                  write(10,*)  real(foo,kind=4), err_cal, (ww_vrai(i),i=1,nc)
                  if(foo>v_sup) v_sup=foo
                  if(foo<v_min) v_min=foo
          else
          if(n>100) n_faux_pos=n_faux_pos+1 
          end if
          write(*,*) n_effec,' ( ',n_effec+nok,')', real(foo,kind=4), nok, &
          &real(nok)/real(n_effec),real(v_sup,kind=4),real(v_min,kind=4)
          write(8,*) n_effec,' ( ',n_effec+nok,')', real(foo,kind=4), err_cal, nok,&
          &real(nok)/real(n_effec),real(v_sup,kind=4),real(v_min,kind=4)
          else
          !if(err_cal<1.e-06_rdp) n_faux_neg=n_faux_neg+1
          n_vire=n_vire+1
          !write(*,*) 'rate',test
          end if
          n=n+1
  end do
  allocate(foo_sample(nok))
  foo_sample=foo_sample1(1:nok)
  allocate(echantillon_p(nok,nc+2))
  echantillon_p=echantillon_p1(1:nok,1:nc+2)
  call moy_var(moy,var,foo_sample)
  
  v_sup=moy+1.96_rdp*sqrt(var)/sqrt(real(nok)-1)
  v_min=moy-1.96_rdp*sqrt(var)/sqrt(real(nok)-1)
  
write(*,*) ntest,' moyenne : ', moy,' variance :',var,' valeur sup : '&
&,v_sup,' valeur min : ',v_min, ' ratio calage : ',real(nok)/real(n_effec)
write(*,*) 'ecart intervalle confiance', real((v_sup-v_min)/moy*100._rdp,kind=4),'%'
write(8,*) ntest,' meilleur resultat : ', foo_best,' moyenne : ', moy,' variance :',&
&var,' valeur sup : ',v_sup,' valeur min : ',v_min, ' ratio calage : ',real(nok)/real(ntest)  
write(8,*) 'ecart intervalle confiance', real((v_sup-v_min)/foo_initiale*100._rdp,kind=4),'%'
v_sup=maxval(foo_sample,dim=1)
v_min=minval(foo_sample,dim=1)
write(*,*) 's_min', real(v_min,kind=4), 's_max', real(v_sup,kind=4),'ecart',&
 & real((v_sup-v_min)/moy*100._rdp,kind=4)
write(8,*) 's_min', real(v_min,kind=4), 's_max', real(v_sup,kind=4),'ecart',&
 & real((v_sup-v_min)/moy*100._rdp,kind=4)
 ! fait une ACP
 
 call ACP_R_data(echantillon_p(:,1:nc+1))
  call init_ACP()
  ! call fin_sa(100,indice)
end subroutine Monte_carlo_surface

! Fait une methode de K means pour tester le calage
! on prend les n points deja calcules les plus proches dans l espaces des parametres
! on decide de caler par ponderation
function K_means(ech,calage,xtest,n_test)
use i_Parametres
use linalg
implicit none
integer :: n_test,indice,ind_proche(n_test)
real(kind=rdp) :: xtest(:),ech(:,:),calage(:),K_means,dummy,ech_proche(n_test),somme,test
n=size(ech(1,:))
dummy=dist_euclide(xtest,ech(:,1))
indice=1
somme=0._rdp
K_means=0._rdp
if (n_test==1) then
! on prend juste le point le plus proche
do i=2,n
somme=dist_euclide(xtest,ech(:,i))
if(somme<dummy) then
        dummy=somme
        indice=i
end if        
end do                
! le calage est assimile a celui du point le plus proche
K_means=calage(indice)
else
!on prend les ntest plus proches 	
do i=1,n_test
ind_proche(i)=i
ech_proche(i)=dist_euclide(xtest,ech(:,i))
end do
do i=n_test+1,n
test=dist_euclide(xtest,ech(:,i))
if (test<maxval(ech_proche)) then
indice=maxloc(ech_proche,dim=1)
ind_proche(indice)=i
ech_proche(indice)=test
end if
end do
indice=minloc(ech_proche,dim=1)
! on pondere la valeur trouvee : le point le plus proche est le plus determinant
do i=1,n_test
if (calage(ind_proche(i))>1.0e-06_rdp)  then 
        if (i==indice) then 
        ! point le plus proche grande influence
        dummy=0.5
        else 
        ! point moins proches peu influence
        dummy=0.5_rdp/(n_test-1)
        end if
else 
dummy=0
end if
K_means=K_means+dummy
end do
!if (test<dummy) then
!        indice=i
!        test=dummy
!        end if
!K_means=calage(i)
if (K_means >0.5_rdp)  then
K_means=1._rdp
else
K_means=0._rdp
end if
end if
end function K_means

! fonction K means sur le calgae mais pour etre integree dans le recuit simule
! augmente bien le taux de calage mais restreint trop la fonction pour trouver un optimum
function K_means_recuit(ech,calage1,calage2,xtest)
use i_Parametres
use linalg
implicit none
integer :: indice1,indice2,i
real(kind=rdp) :: xtest(:),ech(:,:),calage1(:),K_means_recuit,dummy1,somme,test1,&
& calage2(:),dummy2,test2
n=size(ech(1,:))
dummy1=dist_euclide(xtest(1:nc),ech(1:nc,1))
dummy2=dist_euclide(xtest(nc+1:2*nc),ech(1:nc,1))

indice1=1
indice2=1
somme=0._rdp
K_means_recuit=0._rdp
do i=2,n
test1=dist_euclide(xtest(1:nc),ech(1:nc,i))
test2=dist_euclide(xtest(nc+1:2*nc),ech(1:nc,i))
if(test1<dummy1) then 
indice1=i
dummy1=test1
end if
if(test2<dummy2)  then
indice2=i
dummy2=test2
end if
end do
do i=1,n
test1=dist_euclide(xtest(1:nc),ech(nc+1:2*nc,i))
test2=dist_euclide(xtest(nc+1:2*nc),ech(nc+1:2*nc,i))
if(test1<dummy1) then 
indice1=i+n
dummy1=test1
end if
if(test2<dummy2) then
indice2=i+n
dummy2=test2
end if
end do
if (indice1>n) then
        K_means_recuit=K_means_recuit+calage2(indice1-n)
        else
        K_means_recuit=K_means_recuit+calage1(indice1)
end if
if (indice2>n .and. K_means_recuit<1.0e-06_rdp) then
        K_means_recuit=K_means_recuit+calage2(indice2-n)
        else
        K_means_recuit=K_means_recuit+calage1(indice2)
end if
end function K_means_recuit

!fonction donnant un resultat avec une densite de proba dense sur les 'bords' wmin et wmax 
 function alea_distribution_cafe(wmin,wmax,degre)
 use i_Parametres
 implicit none
 integer::degre
 real(kind=rdp)::alea_distribution_cafe,wmin,wmax,temp,a,moy,y
 moy=(wmin+wmax)/2._rdp
 y=random(0._rdp,1._rdp)
 a=real((degre+1),kind=rdp)*2**degre/((wmax-wmin)**(degre+1))
 temp=real(degre,kind=rdp)/a*y+((wmin-wmax)/2)**(degre+1)
 if (temp<0._rdp) then
alea_distribution_cafe=-1._rdp*(-temp)**(1._rdp/(degre+1))+(wmax+wmin)/2._rdp
 else
alea_distribution_cafe=(temp)**(1._rdp/(degre+1))+(wmax+wmin)/2._rdp
 end if

end function alea_distribution_cafe

subroutine test_random
use i_Parametres, only : rdp, np, issup
use i_Consigne, only : nc, wmin, wmax, n_it_max
use rand_num
   implicit none
! Prototype
! Variables locales
   integer :: n,  nok, nmax,i,n_maxi,k,l,s
   character(100) :: file1
   character(20) :: seed
   real(kind=rdp) :: ww_vrai(nc),w_min(nc),w_max(nc)
   real(kind=rdp),allocatable ,dimension(:,:) :: valeurs_p,test_val
n_maxi=n_it_max
nmax=n_maxi!min(n_it_max,300)
n=1
w_min=0.0_rdp
w_max=1.0_rdp

allocate (valeurs_p(nmax,nc))
!test random latin Hypercube
 write(*,*) 'test random hypercube latin'
call sampling(w_min(1:nc),w_max(1:nc),valeurs_p,nmax,.true.) 
do n=1,nmax
open (11,file='test_random_lhs.txt')
       ww_vrai(1:nc)=valeurs_p(n,:)
         write(11,*) n,(ww_vrai(i),i=1,nc) 
  end do
  close(11)
  !test random avec modulo
  write(*,*) 'test random modulo'
l=int(random(1.0_rdp,20.0_rdp))
 s=2**l-1
call init_seed_ran0(s)
write(seed,*) l

!file1='test_random_modulo'//trim(adjustl(seed))//'.txt'
file1='test_random_modulo.txt'	
write(*,*) file1
 open (12,file=trim(file1))
  !call init_random_seed()
  do i=1,n_maxi
  do k=1,nc
   ww_vrai(k)=ran0()
  end do
  write(12,*) i,(ww_vrai(k),k=1,nc) 
  end do
  close(12)
  !test random Mersenne twister
   write(*,*) 'test random Mersenne twister'
  open (13,file='test_random_MT.txt')
  do i=1,n_maxi
  do k=1,nc
   ww_vrai(k)=random(w_min(k),w_max(k))
  end do
  write(13,*) i ,(ww_vrai(k),k=1,nc) 
  end do
  close(13)
end subroutine test_random
