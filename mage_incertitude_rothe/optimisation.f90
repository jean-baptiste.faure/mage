INCLUDE 'montecarlo.f90'

INCLUDE 'Cross_entropy.f90'

INCLUDE 'latin_hypercubes.f90'

INCLUDE 'algo_genetique.f90'

INCLUDE 'acp.f90'

subroutine Solveur_externe(REPname,tag,resultats)
   use omp_lib
   use i_Parametres, only : rdp
   use Dim_Reelles, only : ismax, ibmax
   use Geometrie_Section, only : is1, is2, ibu, xgeo
   use i_Annealing, only : nb_simul, cpu_externe, FOtyp
   use i_Consigne, only : solveur
   implicit none
! Prototype
   character(len=40), intent(in) :: REPname
   character(len=1), intent(in) :: tag
   type (Mage_results), intent(out) :: resultats
!   
   logical ::  bexist
   integer :: is, lu, ib, flag
   character(len=90) :: commande
   real(kind=rdp) :: cpu_solveur

   !print*,'Entrée solveur_externe(), tag=',tag
   commande = trim(solveur)//' -d='//tag//' -s -l'//' '//trim(REPname)
   if (FOtyp == 7) commande = trim(solveur)//' -d='//tag//' -s -l=FIN'//' '//trim(REPname)
   !write(6,*) 'Commande solveur = ',trim(commande),'###','  tag = ',tag
   call execute_command_line(commande)
   
   !print*,'Test si le fichier Mage_Results existe pour la simulation ',tag
   inquire(file='./'//tag//'/'//'Mage_Results',exist=bexist)
   if (bexist) then
      !print*,' Le fichier Mage_Results existe pour la simulation ',tag
      open(newunit=lu,file=tag//'/'//'Mage_Results',form='unformatted',status='old')
         read (lu) flag
         resultats%calcul_OK = (flag .GT. 0)
         !write(6,*) 'Tag = ',tag,' Flag = ',flag,'  ',resultats%calcul_OK
         read(lu) resultats%date_fin, ibmax, ismax, resultats%type_resultat, cpu_solveur
         !write(6,*) 'Tag = ',tag,' Type Résultats = ',resultats%type_resultat
         do ib = 1, ibmax
            read(lu) is1(ib),is2(ib), ibu(ib)
         enddo
         do is = 1, ismax
            read(lu) resultats%pm(is), resultats%zfd(is), resultats%largeur_totale(is),&
                     resultats%largeur_mineur(is), resultats%Z_max(is), resultats%qtot(is),&
                     resultats%qfp(is)
         enddo
      close(lu,status='delete')
   else
      resultats%calcul_OK = .false.
      resultats%date_fin = -99999
      cpu_solveur = 0.
   endif
   nb_simul = nb_simul+1
   if (nb_simul == 1) xgeo = resultats%pm
   cpu_externe = cpu_externe + cpu_solveur
      
end subroutine solveur_externe




function change_tableau(echantillon)
use i_Parametres
implicit none 
integer :: i
real(kind=rdp) :: echantillon(:,:),change_tableau(nc+1,2*size(echantillon(1,:)))
do i=1,size(echantillon(1,:))
change_tableau(1:nc,2*(i-1)+1)=echantillon(1:nc,i)
change_tableau(nc+1,2*(i-1)+1)=echantillon(2*nc+2,i)*10000
change_tableau(1:nc,2*i)=echantillon(nc+1:2*nc,i)
change_tableau(nc+1,2*i)=echantillon(2*nc+3,i)*10000
end do
end function change_tableau





 






!!!!!!! Fonction simulant un jeu de parametres gaussien suivant N(mu,sigma)
 subroutine simulation_gauss(w,mu,sigma)
 use i_Parametres
 use rand_num
 use i_Consigne, only : nc, wmin, wmax, nst, nbok
 implicit none
 real(kind=rdp) :: w(:),sigma(:),mu(:),u,v,pi
 integer :: i,n
 call init_random_seed()
 pi=atan(1._rdp)*4._rdp
 n=size(w(:))
         do i=1,n
                 u=random(0._rdp,1._rdp)
                 v=random(0._rdp,1._rdp)
                 if (u>0._rdp .or. sigma(i)>0._rdp) then
                         w(i)=sqrt(-2*log(u))*cos(2*pi*v)*sigma(i)+mu(i)
                         w(i)=min(wmax(i),w(i))
                         w(i)=max(wmin(i),w(i))
                         else 
                         w(i)=mu(i)
                 end if
         end do
 
 end subroutine simulation_gauss
 
 !routine calculant la moyenne mu et la variance sigma d un vecteur de valeurs quelconque 
 subroutine moy_var(mu,sigma,echantillon)
 use i_Parametres
 implicit none
 real(kind=rdp) :: mu,sigma,echantillon(:)
 real(kind=rdp),allocatable:: temp(:)
 integer :: nb
 nb =size(echantillon)
 allocate(temp(nb))
 mu=sum(echantillon(:))/real(nb)
 temp=echantillon-mu
 temp=temp*temp
 sigma=sum(temp)/real(nb)
 end subroutine moy_var
 
subroutine Simulated_Annealing(W_initial,W_opt, funk, sikrig,siacp,silin)
!==============================================================================
!         ALGORITHME DE MINIMISATION PAR LA METHODE DU RECUIT-SIMULE
!   source : Numerical Recipes (livre)
!
! si sens > 0 minimisation, sinon maximisation
!==============================================================================
   use i_Parametres, only : rdp, np
   use i_Annealing, only : t0, nequil, fk_ini, fk_opt, dth, temperature, &
                         nb_eval, nb_simul, iopt, unite, sens, Surf1, Surf2, &
                         seuil_amelioration,nACP,n_acp_fin
   use i_Consigne, only : nc, wmin, wmax, inhib, scale, nst, deltaZ,n_it_max
   use calage_error
   use R_krig
   use ACP
   use bords  ,only :  bord_init
   implicit none
! Prototype
   real(kind=rdp), intent(in) :: W_initial(np,2)
   real(kind=rdp), intent(out) :: W_opt(np,2)
! variables locales
   logical :: arret, bstop, bpause
   integer :: idem,  i,nb_iter, nbvar, nko, ntot, nequil0,n_effec,n_ACP,n_ok
   integer :: n_decal,n_maxi,nstill,nstop,nb_eval_ini
   real(kind=rdp) :: tmin,Tseuil,distance,augmentation,distance2
   real(kind=rdp) :: W_new(np,2), W_sa(np,2), rdom,w_comp(2*nc),correl(nc)
   real(kind=rdp) :: fk_new, fk_sa !valeurs intermédiaires de la fonction-coût
   real(kind=rdp) :: test, C_Plus,test_cal,foo
   real(kind=rdp) :: ampli(np), ampli0(np),ampli_seuil
   real(kind=rdp) :: alpha, nmax, beta,acp_ech(100,2)
   character :: decision*7, ctmp1*13, ctmp2*13, ctmp3*13, MinMax*6
real(kind=rdp),allocatable :: echantillonsa(:,:)
   integer :: n_rate,n_insert,echelle
   logical :: sikrig,siacp,silin,ACP_test,fin_ACP,double_loop

! interfaces
   interface
      function funk(w,C_Plus)
         use i_Parametres, only : rdp,np
         real(kind=rdp), intent(in) :: w(np,2)
         real(kind=rdp), intent(out) :: C_Plus
         real(kind=rdp) :: funk
      end function funk
   end interface
   
!---------------------------------------------------------------------------
   write(*,*) 'Simulations pour l''algorithme du Recuit Simulé'
   write(8,*) 'Simulations pour l''algorithme du Recuit Simulé'
   open(25,file='Distance_'//tmp//'.txt')
   ! initialisation
    call init_random_seed()
    n_maxi=n_it_max
    n_insert=50000 
   nbvar = sum(inhib(1:nc))
   if(temperature<0.00001_rdp) then
   temperature = t0                 !initialisation de la température
   end if
   ampli0 = 0.5*scale*(wmax-wmin)   !amplitude maximale des modifs de consignes
   ampli = ampli0
   nb_iter = 0 
   n_rate=0
   ampli_seuil=0.1_rdp
   double_loop=.false.
   fin_ACP=.true.
   ACP_test=.true.
   nstill=0
   Tseuil=0.1
   nb_eval_ini=nb_simul/4
   n_decal=0
   ntot=0!nombre d'itérations sur la température
   nb_eval = 0
   n_ACP=nACP
   echelle=n_acp_fin!n_ACP+n_ACP/2
   n_effec=1
   MinMax = ' Min ='  ;  if (sens < 0) MinMax = ' Max ='
   n_ok = 1 ; ntot = 0;nko=1;
   nequil0 = nequil
   tmin = 0.01_rdp*t0
   if(nc>nst) call identify_geom()
   allocate (echantillonsa(2*nc+4,n_maxi))
   ! paramètres pour le calcul de la température
   nmax = real(t0/dth,rdp) 
   W_opt = W_initial ; W_sa = W_initial
   fk_ini = funk(w_sa,C_Plus) ; fk_opt = fk_ini ; fk_sa = fk_ini
      if (ifacp) then
      dth=1-((1.0e-04_rdp*T0/temperature)**(1.0_rdp/(nacp*0.25_rdp))) 
    else
    dth=1-((1.0e-04_rdp*T0/temperature)**(1.0_rdp/( (n_it_max)*0.25_rdp))) 
    end if
    write(*,*) 'new dth',dth
   call init_random_seed()
   do while (temperature/T0 > 1.e-04_rdp .and. nb_simul<n_maxi)     !boucle sur la température
      nb_iter = 0
      arret = .false.
      idem = 0  !nb d'évaluations sans amélioration de l'optimum
      ntot=0
      test_cal=0._rdp
      ampli = ampli0 * max(ampli_seuil,temperature / t0)
    !ampli = ampli0 *temperature 

      do while (.not. arret)   !boucle à température fixée
         !modification aléatoire des consignes et nouvelle évaluation de la fonction-coût
         	
       if ((siacp .or. sikrig .or. silin ) .and. (nb_simul>n_ACP+4 .or. ifbegin) .and. fin_ACP ) then
                  !if(silin) call modify_ACP1(w_sa,w_new,ampli,correl)
                  !if (siacp) call modify_ACP2(w_new)
                  if(nb_simul>=echelle) then
                  	  
                  	           w_new(:,1)=param_mini
                                       w_new(:,2)=param_maxi
                  	  fin_ACP=.false.
                  	  if (siacp) then 
                  	  write(*,*) 'fin_ACP'
                  	  write(8,*) 'fin_ACP'
                  	  elseif (sikrig) then
                  	   write(*,*) 'fin_bords'
                  	  write(8,*) 'fin_bords'	
                  	  else
                  	     write(*,*) 'fin_lin'
                  	  write(8,*) 'fin_lin'	  
                  	 end if
                  	  !call bord_init(40.0_rdp)
                  	  T0=0.1_rdp
                  	  temperature=T0/5._rdp
                  	  ampli_seuil=0.05_rdp
                  	  dth=1-(1.0e-04_rdp*T0/temperature)**(1.0_rdp/((n_it_max-nb_simul)*0.25_rdp))
                              write(*,*) 'nouveau dth' ,dth
                  	  else
                if (siacp) then	
               !if(mod(n_ok,100)==0) then
               !call corr(echantillonsa,correl,n_effec-1)
               !call init_ACP()
               !end if
               call modify_ACP_SA(w_new)
               !call modify_ACP_random(w_new)
               elseif (sikrig) then
               call modify_bord(w_sa,w_new,ampli)
               else
              if( mod(n_ok,500)==0) then
              call corr(echantillonsa,correl,n_effec-1)
              end if
               call modify_ACP_lin(w_sa,w_new,ampli,correl)
               end if
                  end if 
                   
        else
        	!if( fin_ACP) then
                call modify(w_sa,w_new,ampli)
                !else 
                 !call modify_2b(w_sa,w_new,ampli)	
                 !end if
       end if
         if(n_effec==1) w_new=w_initial
         w_comp(1:nc)=w_new(1:nc,1)
         w_comp(nc+1:2*nc)=w_new(1:nc,2)
          
         if(nb_simul>=n_ACP .and. (siacp .or. sikrig .or. silin )  .and. fin_ACP .and. ACP_test) then
         
         if (siacp) then
         call corr(echantillonsa,correl,n_effec-1)
         call init_ACP()
         call modify_ACP_SA(w_new)
         write(*,*) 'debut_ACP'
          write(8,*) 'debut_ACP'
        elseif (sikrig) then
         call bord_init(5.0_rdp)
            w_new(:,1)=param_mini(:)
         w_new(:,2)=param_maxi(:)
         w_sa=w_new
         write(*,*) 'debut_bords'
          write(8,*) 'debut_bords'
          else
          call corr(echantillonsa,correl,n_effec-1)
         w_new(:,1)=param_mini(:)
         w_new(:,2)=param_maxi(:)
         w_sa=w_new
           call modify_ACP_lin(w_sa,w_new,ampli,correl)
            write(*,*) 'debut_lin'
          write(8,*) 'debut_lin'
         end if
      
          
         f_opt_ACP=funk(w_new,C_plus)
         if(C_plus*Coeff_Penalisation>1._rdp) then 
         f_opt_ACP=0._rdp
         end if
         fk_opt=f_opt_ACP
         w_opt=w_new
         T0=0.1_rdp
          temperature=T0/2.0_rdp
         dth=1.0_rdp-((1.0e-04_rdp*T0/temperature)**(1.0_rdp/((echelle-n_ACP)*0.25)))
         write(*,*) 'nouveau dth',dth
         ACP_test=.false.
         
          
         end if
         
          n_effec=n_effec+1
          fk_new = funk(w_new,C_Plus)  ;  nb_eval = nb_eval+1  ;  ntot = ntot+1
         echantillonsa(2*nc+1,n_effec)=err_cal1
         echantillonsa(2*nc+4,n_effec)=err_cal2
         echantillonsa(1:2*nc,n_effec)=w_comp
         echantillonsa(2*nc+2,n_effec)=Surf1
         echantillonsa(2*nc+3,n_effec)=Surf2
          
          if(C_plus*Coeff_Penalisation<1.0_rdp  ) then
          n_ok=n_ok+1
          distance=0._rdp
          distance2=0._rdp
            do i=1,np
             distance=distance+(w_opt(i,1)-w_new(i,1))**2+(w_opt(i,2)-w_new(i,2))**2
            distance2=distance2+(w_opt(i,2)-w_new(i,1))**2+(w_opt(i,1)-w_new(i,2))**2
            end do
            distance=sqrt(min(distance,distance2))
            if (fk_opt>0.0001_rdp) then
            augmentation=max((fk_new-fk_opt)/fk_opt*100._rdp,0._rdp)
            else
            	augmentation=0._rdp  
            	end if
            	distance2=(surf_maxi-surf_mini)/surf_ini*100._rdp
            	if (sens*(fk_new-fk_opt) < seuil_amelioration) then
            	  test =0._rdp 
            	  else 
            	 test=exp(-sens*(fk_new-fk_opt)/(temperature)) 
            	 end if
            write(25,*) nb_simul,n_ok,distance,augmentation,fk_opt,distance2,test
          if (siacp .and. fin_ACP .and. ifbegin .and. abs(Surf1-Surf2)/surf_ini*100._rdp>f_opt_ACP) then
          	  opt_vect=new_vect
          	  f_opt_ACP=abs((Surf1-Surf2)/surf_ini)*100._rdp
          	  end if
        end if
        if (sens*(fk_new-fk_opt) < seuil_amelioration) then 
        ! if (sens*(fk_new-fk_opt) < seuil_amelioration) then !NB: seuil_amelioration est négatif
         !>>>l'optimum est amélioré ; sauvegarde et on garde la nouvelle consigne
            idem = 0        !remise à 0 du compteur de non-améliorations
            iopt = nb_eval
            
            fk_opt = fk_new  ;  w_opt = w_new  !mise à jour de l'optimum
            fk_sa = fk_new   ;  w_sa = w_new      !on garde la nouvelle consigne
             if(siacp .and. ifbegin .and. fin_ACP) old_vect=new_vect
            decision = ' ##### '
            nb_iter = nb_iter+1
         elseif (sens*(fk_new-fk_sa) < seuil_amelioration) then  !NB: seuil_amelioration est négatif
         !>>>la consigne est meilleure que la précédente : on la garde
         !>>>mais l'optimum n'a pas changé, seulement fk_sa
            !idem = idem + 1
             fk_sa = fk_new  ;  w_sa = w_new
            if(siacp .and. ifbegin .and. fin_ACP) old_vect=new_vect
            	    
            decision = ' +++++ '
                nb_iter = nb_iter+1
          elseif (C_Plus *Coeff_Penalisation> 1.0_rdp) then   !>>>on rejette la nouvelle consigne car les contraintes ne sont pas satisfaites
            decision = ' ----- '
         else
         !>>>on garde peut-être la nouvelle consigne mais l'optimum ne change pas
            !calcul du critère de test
              test = exp(-sens*(fk_new-fk_opt)/temperature)
            !tirage aléatoire
            rdom= random(0._rdp,1._rdp)
            if (test > rdom) then   !>>>on garde la nouvelle consigne
               fk_sa = fk_new  ;  w_sa = w_new
               if(siacp .and. ifbegin .and. fin_ACP ) old_vect=new_vect
               !l'optimum n'a pas changé, seulement fk_sa
               decision = ' ===== '
               !idem = idem + 1
               	          nb_iter = nb_iter+1
            else             !>>>on rejette la nouvelle consigne
               decision = ' ----- '
               idem = idem + 1
             !     nb_iter = nb_iter+1
            endif
         endif

         !Listing et affichage sur la console
        ctmp1 = affiche(ecart_courant,unite) 
        ctmp2 = affiche((surf_maxi-surf_mini)/surf_ini*100._rdp,unite) 
        ctmp3 = affiche(fk_sa,unite)
         write(*,'(2(a,i5),7a,f12.10,a,1X,f5.3,i5)') '#',n_effec+nb_eval_ini,' (',n_ok,') :',ctmp1,' Actuel = ',ctmp3,&
                 MinMax,ctmp2,' T = ',temperature/T0,decision, real(n_ok)/real(n_effec),nb_simul
         !fichier txt sortie
         write(8,'(2(a,i5),7a,f12.8,a,2es10.3,1X,f5.3,i5)') '#',n_effec+nb_eval_ini,' (',n_ok,') :',ctmp1,' Actuel = ',ctmp3,&
                 MinMax,ctmp2,' T = ',temperature/T0,decision, C_plus,minval(ampli(1:nc)), real(n_ok)/real(n_effec),nb_simul
         !fichier csv strickler
         if (sens < 0.) then
            test = max(-99.999,fk_new)
         else
            test = min(99.999,fk_new)
         endif
          write(7,'(i5,a,100f10.3)')  n_effec+nb_eval_ini, decision, w_new(1:nst,1), w_new(1:nst,2), Surf1, Surf2, &
                                    test, temperature
         if (deltaZ > 0._rdp) then
            !fichier csv géométrie
            write(9,'(i5,a,1000f10.6)') n_effec+nb_eval_ini, decision, w_new(nst+1:nc,1), w_new(nst+1:nc,2), Surf1, Surf2, &
                                        test, temperature/T0
         endif
       
         !Arrêt anticipé si on trouve un fichier stop, Pause si fichier pause
         inquire(file='stop',exist=bstop)
         if (bstop) then  !arrêt anticipé
            open(unit=50,file='stop',status='unknown')
            close(50,status='delete')  !suppression du fichier 'stop'
            write(*,*) ' >>> interruption par l''utilisateur !!!'
            write(8,*) ' >>> interruption par l''utilisateur !!!'
            return
         endif
         inquire(file='pause',exist=bpause)
         if (bpause) call mise_en_veille()

         !test pour changement de température : 
         !      si pas d'amélioration
         ! OU   si pas d'itération
         ! OU   si nombre total d'itérations à température donnée dépassé
         arret = (nb_iter > nequil) .OR. (idem > nequil) .OR. (nequil == 0)! .OR. (ntot > 4*nequil)
         !arret = (idem > nequil) .OR. (nequil == 0) .OR. (ntot > nstop)
      enddo
        !     if (temperature > tmin) then
         temperature = temperature*(1._rdp-dth) !décroissance exponentielle
      !else
         if (double_loop) then
         	 double_loop=.false.
         	 temperature=t0/10._rdp
         	 dth=0.005
         	 w_sa(:,1)=param_mini
         	 w_sa(:,2)=param_maxi
         	 fk_sa=(surf_maxi-surf_mini)/surf_ini*100._rdp
         end if
      !   temperature = temperature - dth*tmin
      !endif

   enddo
   w_opt(:,1)=param_mini
   w_opt(:,2)=param_maxi
   fk_opt = funk(w_opt,C_Plus)  !pour que la dernière simulation corresponde à l'optimum

   write(*,'(a,g14.6)') ' Sortie de Simulated_Annealing a la temperature de ',temperature
   write(8,'(a,g14.6)') ' Sortie de Simulated_Annealing a la temperature de ',temperature
   write(*,*) fk_opt,n_effec,n_ok,nb_simul
   write(*,*) Surf1
   write(*,*) (real(w_opt(i,1),kind=4),i=1,nc)
   write(*,*) Surf2
   write(*,*) (real(w_opt(i,2),kind=4),i=1,nc)
   write(8,*) fk_opt,n_effec,n_ok,nb_simul
   write(8,*) Surf1
   write(8,*) (real(w_opt(i,1),kind=4),i=1,nc)
   write(8,*) Surf2
   write(8,*) (real(w_opt(i,2),kind=4),i=1,nc)
   open(14,file='resultat_recuit.txt',access='append')
   write(14,*) (w_opt(i,1),i=1,nc),Surf1,fk_opt,n_effec,n_ok
   write(14,*) (w_opt(i,2),i=1,nc),Surf2,fk_opt,n_effec,n_ok
   call corr(echantillonsa,correl,n_effec-1)
    ! call init_ACP()
   !call fin_sa(100,indice)
   write(*,*) 'test resultat'
   write(*,*) real(wmin(1:nc),kind=4)
    write(*,*) real(wmax(1:nc),kind=4)
   ifhybride=.false.
   ! test de rebouclage sur krigeage
  w_new(:,1)=wmax
  w_new(:,2)=wmin
   do i=1,nc
   if(param_maxi(i)==wmin(i) .or. param_maxi(i)==wmax(i)) then
   	   wmax(i)=param_maxi(i)
   	   wmin(i)=wmax(i)
   end if
   end do

   !call R_krigeage()
   wmax=w_new(:,1)
   wmin=w_new(:,2)
   do i=1,nc
   if(param_mini(i)==wmin(i) .or. param_mini(i)==wmax(i)) then
   	   wmax(i)=param_mini(i)
   	   wmin(i)=wmax(i)
   end if
   end do
   !call R_krigeage()
end subroutine Simulated_Annealing

! routine pour faire un krigeage sur les axes trouves par l acp
subroutine fin_sa(n_kr,n_dim)
use recuit_simule1
use recuit_simule
use R_krig
use ACP
use i_Parametres
use i_Consigne
implicit none 
integer, intent(in) :: n_kr,n_dim
  real(kind=rdp) :: W_new(np,2), W_sa(np,2), rdom,w_comp(2*nc),correl(nc)
   real(kind=rdp) :: fk_new, fk_sa !valeurs intermédiaires de la fonction-coût
   real(kind=rdp) :: test, C_Plus,test_cal,foo,x_mini(n_dim),x_maxi(n_dim),foo_min,foo_max
   real(kind=rdp) :: ampli(np), ampli0(np),foo_test(n_kr),err_cal_test(n_kr)
   real(kind=rdp) :: alpha, nmax, beta,acp_ech(n_kr,n_dim),acp2_ech(n_dim,n_kr),val(n_dim,n_kr)
   integer :: i,j,k
   
        comp_min=1.2_rdp*comp_min
         comp_max=1.2_rdp*comp_max
 k=0
   open(21,file='acp_sampling.txt')
do while (k<n_kr)
call sampling(comp_min(1:n_dim),comp_max(1:n_dim),acp_ech,n_kr,.true.)

  do i=1,n_dim
  acp2_ech(i,:)=acp_ech(:,i)
  end do
    do i=1,n_kr
    if(k>=n_kr) exit
     call convert_ACP(w_new(:,1),acp2_ech(1:n_dim,i))
     if (w_new(1,1)>0.0_rdp) then
    k=k+1
    val(:,k)=acp2_ech(:,i)
   foo=Foo_Surface_no_cal(w_new(:,1),C_plus)
   foo_test(k)=foo
   err_cal_test(k)=calage_ok(w_new(:,1),'0')*Coeff_Penalisation
   write(21,*) (val(j,k),j=1,n_dim),foo,err_cal_test(k)
    write(*,*) (val(j,k),j=1,n_dim),foo,err_cal_test(k)
    end if
   end do
   end do
   close(21)
   
   
   
           call execute_command_line("Rscript model_krig_ACP.R")
         ! Portage des meta-modeles en FORTRAN
         call init_krig(val(1:n_dim,:),err_cal_test,foo_test)
     ! test krigeage
    do i=1,n_kr
    foo=eval_surf(val(1:n_dim,i))
    C_plus=eval_cal(val(1:n_dim,i))
    write(*,*) 'vrai surf',foo_test(i),'modele',foo,'vrai err',err_cal_test(i),'modele',C_plus
    end do
    do i=1,100
     x_mini=val(:,i)
    x_maxi=x_mini
    foo_max=foo_test(i)
    foo_min=foo_max
    !calcul max
    	    
    call init_recuit(nb_param,comp_min,comp_max,x_maxi(1:n_dim),foo_max,-1._rdp,0.005_rdp)
   nequil=1
   call recuit_sa(x_maxi,eval_surf_cal)
    call convert_ACP2(w_new(:,1),x_maxi)
    foo_max=foo_Surface(w_new(:,1),C_plus)
   ! calcul du min
  
     call init_recuit1(nb_param,comp_min,comp_max,x_mini(1:n_dim),foo_min,1._rdp,0.005_rdp)
   nequil1=1
   call recuit_sa1(x_mini,eval_surf_cal_min)
    call convert_ACP2(w_new(:,2),x_mini)
     foo_min=foo_Surface(w_new(:,2),C_plus)
      write(*,*) 'mini :',foo_min,'maxi :' ,foo_max
      write(8,*) 'mini :',foo_min,'maxi :' ,foo_max
    end do
    
   
end subroutine fin_sa

subroutine modify_bord(w_sa,w_new,ampli)
use i_Parametres, only : rdp, np, sp
   use i_Consigne, only : nc, eps, inhib, scale, nst, wmin, wmax
   use i_Annealing, only : t0, FOtyp, nb_eval
   use bords
   implicit none
! Prototype
   real(kind=rdp), intent(out) :: W_new(np,2)
   real(kind=rdp), intent(in) :: W_sa(np,2), ampli(np)
! Variables locales
   integer :: n, i, k, j
   real(kind=rdp) :: a, b
!--------------------------------------------------------------------------   
   w_new = w_sa  !initialisation globale avant modif de certaines composantes
     i = 1+mod(nb_eval,4)/2   !indice consigne 1 ou 2
      j = 1-mod(nb_eval,2)      !groupe Strickler mineur (1) ou Strickler majeur (0)
   do n = 1, nc
      if( inhib(n) == 0 ) cycle            !on ignore cette composante si inhib est nul              
     if(n <= nst .and. mod(n,2)==j) then!ou selon que n est pair ou impair (alternativement) pour les stricklers
      a = w_sa(n,i)-ampli(n)
      b = w_sa(n,i)+ampli(n)
      w_new(n,i) = random(a,b)
      ! on bloque aux bords ici pour faciliter l'apparition de consignes
      ! calées sur les bords car les solutions sont souvent là
      w_new(n,i) = max (w_new(n,i),wmin(n))
      w_new(n,i) = min (w_new(n,i),wmax(n))
      end if
     if (  mod(n,2)==0 .and. bool_bord_min(n) ) w_new(n,1)=param_mini_acp(n)
      if (mod(n,2)==0 .and. bool_bord_max(n) ) w_new(n,2)=param_maxi_acp(n)
     !if( mod(n,2)==0 .and.  bool_bord_max(n) .and. bool_bord_min(n)) inhib(n)=0

   enddo



end subroutine modify_bord

