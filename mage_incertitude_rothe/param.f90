use omp_lib 
   use i_Parametres 
   use rand_num
   use i_Consigne  
   use i_Annealing, only : iopt, nb_eval, nb_simul, fk_ini, fk_opt, unite, sens, FOtyp, &
                           cpu_externe,temperature,t0
   use Geometrie_Section, only : xgeo
   implicit none
!Variables locales
   real(kind=rdp) :: w_opt(np,2), debut, fin
   integer :: i, n, is
   integer values(8) ! valeurs renvoyees par la fonction date_and_time()
   character :: zone*5, ddate*8, time*10, tmp*15, tmp2*10
   real(kind=rdp) :: foo0, foo1, foo2, C_Plus, ww(np), zmax(issup), Lmaj(issup)
   real(kind=rdp) :: Xx(issup)
   real(kind=rdp) :: f_best
   real(kind=rdp) :: Z0_max(10), Z1_max(10), Z2_max(10), err0, err1, err2
   character(len=1) :: tag, tag0
   !variable pour gérer les arguments de la ligne de commande
   character(len=60) :: argmnt
   integer :: iarg
   logical :: bexist
   character(len=20) :: outdir='.' !nom du sous-dossier pour le stockage des fichiers de résultats, 
   character(len=20) :: datafile   !nom du fichier de paramètres
   character(len=90), parameter :: Garantie1 = 'Mage_SA est fourni sans garantie '&
                           //'d''aucune sorte dans les limites prévues par la loi'
   character(len=80), parameter :: Garantie2 = 'Les résultats obtenus sont de '&
                                  //'la seule responsabilité de l''utilisateur'
   character(len=43), parameter :: entame = 'Mage_SA (Analyse de Sensibilite), version  '

! types dérivés
   type Mage_results
      logical :: calcul_OK
      integer :: date_fin
      character (len=3) :: type_resultat=''
      real(kind=rdp) :: pm(issup), zfd(issup), largeur_totale(issup)
      real(kind=rdp) :: largeur_mineur(issup),Z_max(issup)
      real(kind=rdp) :: qtot(issup), qfp(issup)  !fp : flood plaine
   end type Mage_results
   type (Mage_results) :: resultats
   
   procedure(flooded_Area), pointer :: working_Area => null()
   procedure(modify_1a), pointer :: modify => null()
   call init_random_seed()
