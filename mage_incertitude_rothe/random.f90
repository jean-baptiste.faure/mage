module mt19937
implicit none
integer,private :: N, N1, M, MATA, UMASK, LMASK, TMASKB, TMASKC
parameter( &
& N = 624, &
& N1 = 625, &
& M = 397, &
& MATA = -1727483681, &
!& UMASK = -2147483648, &
& LMASK = 2147483647, &
& TMASKB = -1658038656, &
& TMASKC = -272236544 &
& )
integer,private :: mti = N1, mt(0:N-1), mag01(0:1) = (/0, MATA/)
 
contains
 
subroutine sgrnd(seed)
integer,intent(in) :: seed
!
! setting initial seeds to mt[N] using
! the generator Line 25 of Table 1 in
! [KNUTH 1981, The Art of Computer Programming
! Vol. 2 (2nd Ed.), pp102]
!
mt(0) = iand(seed, -1)
do mti = 1, N - 1
mt(mti) = iand(69069 * mt(mti - 1), -1)
end do
end subroutine sgrnd
 
real(8) function grnd()
integer :: y, kk
UMASK=-2147483647
UMASK=UMASK-1
if(mti >= N) then
! generate N words at one time
if(mti == N + 1) then
! if sgrnd() has not been called,
call sgrnd(4357)
! a default initial seed is used
endif
 
do kk = 0, N - M - 1
y = ior(iand(mt(kk), UMASK), iand(mt(kk + 1), LMASK))
mt(kk) = ieor(ieor(mt(kk + M), ishft(y, -1)), mag01(iand(y, 1)))
end do
 
do kk = N - M, N - 2
y = ior(iand(mt(kk), UMASK), iand(mt(kk + 1), LMASK))
mt(kk) = ieor(ieor(mt(kk + (M - N)), ishft(y, -1)), mag01(iand(y, 1)))
end do
 
y = ior(iand(mt(N - 1), UMASK), iand(mt(0), LMASK))
mt(N - 1) = ieor(ieor(mt(M - 1), ishft(y, -1)), mag01(iand(y, 1)))
mti = 0
endif
 
y = mt(mti)
mti = mti + 1
y = ieor(y, ishft(y, -11))
y = ieor(y, iand(ishft(y, 7), TMASKB))
y = ieor(y, iand(ishft(y, 15), TMASKC))
y = ieor(y, ishft(y, -18))
 
if(y < 0) then
grnd = (dble(y) + 2.0d0 ** 32) / (2.0d0 ** 32)
else
grnd = dble(y) / (2.0d0 ** 32)
endif
end function grnd
 
end module mt19937 



!module contenant les fonctions de voisinages pour le recuit et de generation alea
Module rand_num
use i_Parametres
use mt19937
use i_Consigne
implicit none
integer :: iseed

contains

!modification aleatoire pour tester les parametres
subroutine modify_1a(w_sa,w_new,ampli)
!==============================================================================
!  routine de modification
!   avec arrondi
!==============================================================================
   use i_Parametres, only : rdp, np
   use i_Consigne, only : nc, eps, inhib, wmin, wmax
   implicit none
! Prototype
   real(kind=rdp), intent(in) :: W_sa(np,2), ampli(np)
   real(kind=rdp), intent(out) :: W_new(np,2)
! Variables locales
   integer :: n
   
!--------------------------------------------------------------------------
   do n = 1, nc
      if( inhib(n) == 0 ) then
         w_new(n,1) = w_sa(n,1)
      else
         !modifications sans corrélation entre tronçons
         ! on bloque aux bords ici pour faciliter l'apparition de consignes
         ! calées sur les bords car les solutions sont souvent là
         w_new(n,1) = random(max(w_sa(n,1)-ampli(n),wmin(n)),min(w_sa(n,1)+ampli(n),wmax(n)))
         ! arrondi à 1/eps décimales
         w_new(n,1) = eps*anint(w_new(n,1)/eps)
      endif
   enddo
end subroutine modify_1a


!Modification des parametres tests  aleatoire
subroutine modify_1b(w_sa,w_new,ampli)
!==============================================================================
!  routine de modification
!   sans arrondi
!==============================================================================
   use i_Parametres, only : rdp, np
   use i_Consigne, only : nc, inhib, wmin, wmax
   implicit none
! Prototype
   real(kind=rdp), intent(in) :: W_sa(np,2), ampli(np)
   real(kind=rdp), intent(out) :: W_new(np,2)
! Variables locales
   integer :: n
   
!--------------------------------------------------------------------------
   do n = 1, nc
      if( inhib(n) == 0 ) then
         w_new(n,1) = w_sa(n,1)
      else
      !modifications sans corrélation entre tronçons
         w_new(n,1) = random(max(w_sa(n,1)-ampli(n),wmin(n)),min(w_sa(n,1)+ampli(n),wmax(n)))
      endif
   enddo
end subroutine modify_1b



subroutine modify_2a(w_sa,w_new,ampli)
!==============================================================================
!  routine de modification
!   avec arrondi
!==============================================================================
   use i_Parametres, only : rdp, np
   use i_Consigne, only : nc, eps, inhib, wmin, wmax
   implicit none
! Prototype
   real(kind=rdp), intent(in) :: W_sa(np,2), ampli(np)
   real(kind=rdp), intent(out) :: W_new(np,2)
! Variables locales
   integer :: n
!--------------------------------------------------------------------------
!!$omp parallel default(shared) private(n)
!   !$omp do schedule(dynamic)    
   do n = 1, nc
      if( inhib(n) == 0 ) then
         w_new(n,1) = w_sa(n,1)
         w_new(n,2) = w_sa(n,2)
      else
         w_new(n,1) = random(max(w_sa(n,1)-ampli(n),wmin(n)),min(w_sa(n,1)+ampli(n),wmax(n)))
         w_new(n,1) = eps*anint(w_new(n,1)/eps)
         w_new(n,2) = random(max(w_sa(n,2)-ampli(n),wmin(n)),min(w_sa(n,2)+ampli(n),wmax(n)))
         w_new(n,1) = eps*anint(w_new(n,1)/eps)
      endif
   end do
!   !$omp end do
!!$omp end parallel
end subroutine modify_2a



subroutine modify_2b(w_sa,w_new,ampli)
!==============================================================================
!  routine de modification
!   sans arrondi
!==============================================================================
   use i_Parametres, only : rdp, np
   use i_Consigne, only : nc, inhib, wmin, wmax
   implicit none
! Prototype
   real(kind=rdp), intent(in) :: W_sa(np,2), ampli(np)
   real(kind=rdp), intent(out) :: W_new(np,2)
! Variables locales
   integer :: n
!--------------------------------------------------------------------------
!!$omp parallel default(shared) private(n)
!   !$omp do schedule(dynamic)
   do n = 1, nc
      if( inhib(n) == 0 ) then
         w_new(n,1) = w_sa(n,1)
         w_new(n,2) = w_sa(n,2)
      else
         w_new(n,1) = random(max(w_sa(n,1)-ampli(n),wmin(n)),min(w_sa(n,1)+ampli(n),wmax(n)))
         w_new(n,2) = random(max(w_sa(n,2)-ampli(n),wmin(n)),min(w_sa(n,2)+ampli(n),wmax(n)))
      endif
   end do
!   !$omp end do
!!$omp end parallel
end subroutine modify_2b


subroutine modify_2c(w_sa,w_new,ampli)
!==============================================================================
!  routine de modification avec modif  en cycle S1-Lmajeur=>S1-Lmineur=>S2-Lmineur=>S2-Lmineur
!==============================================================================
   use i_Parametres, only : rdp, np, sp
   use i_Consigne, only : nc, eps, inhib, scale, nst, wmin, wmax
   use i_Annealing, only : t0, FOtyp, nb_eval
   implicit none
! Prototype
   real(kind=rdp), intent(in) :: W_sa(np,2), ampli(np)
   real(kind=rdp), intent(out) :: W_new(np,2)
! Variables locales
   integer :: n, i, k, j
   real(kind=rdp) :: a, b
!--------------------------------------------------------------------------   
   w_new = w_sa  !initialisation globale avant modif de certaines composantes
     i = 1+mod(nb_eval,4)/2   !indice consigne 1 ou 2
      j = 1-mod(nb_eval,2)      !groupe Strickler mineur (1) ou Strickler majeur (0)
   do n = 1, nc
      if( inhib(n) == 0 ) cycle            !on ignore cette composante si inhib est nul              
      if(n <= nst .and. mod(n,2)/=j) cycle !ou selon que n est pair ou impair (alternativement) pour les stricklers
      a = w_sa(n,i)-ampli(n)
      b = w_sa(n,i)+ampli(n)
      w_new(n,i) = random(a,b)
      ! on bloque aux bords ici pour faciliter l'apparition de consignes
      ! calées sur les bords car les solutions sont souvent là
      w_new(n,i) = max (w_new(n,i),wmin(n))
      w_new(n,i) = min (w_new(n,i),wmax(n))
   enddo
end subroutine modify_2c


function random(a,b)
! générateur de nombres pseudo-aléatoires de distribution uniforme sur [a,b]
   use i_Parametres, only : rdp
   implicit none
!prototype
   real(kind=rdp), intent(in) :: a, b
   real(kind=rdp) :: random
!variables locales
   real(kind=rdp) :: rdom
!---------------------------------------------------------
   if (b > a) then
      !call random_number(rdom)
     rdom=grnd()
      random = (b-a) * rdom + a
      if (random < a .or. random > b) stop 665
   else if (b==a) then 
      random=a
   else
      write(*,*) a,b,random
      stop 666
   endif
end function random


function random2(a,b)
! générateur de nombres pseudo-aléatoires de distribution uniforme sur [a,b]
   use i_Parametres, only : rdp
   implicit none
!prototype
   real(kind=rdp), intent(in) :: a, b
   real(kind=rdp) :: random2
!variables locales
   real(kind=rdp) :: rdom
!---------------------------------------------------------
   if (b > a) then
      call random_number(rdom)
      random2 = (b-a) * rdom + a
      if (random2 < a .or. random2 > b) stop 665
   else if (b==a) then 
      random2=a
   else
      write(*,*) a,b,random2
      stop 666
   endif
end function random2

!Routine pour initialiser la graine indexee sur la date de la machine
        subroutine init_random_seed()
            use i_Parametres, only : rdp
            use iso_fortran_env, only: int64
            implicit none
            integer, allocatable :: seed(:)
            integer :: i, n, dt(8), pid
            integer(int64) :: t
          
            call random_seed(size = n)
            allocate(seed(n))
            
               call system_clock(t)
               if (t == 0) then
                  call date_and_time(values=dt)
                  t = (dt(1) - 1970) * 365_int64 * 24 * 60 * 60 * 1000 &
                       + dt(2) * 31_int64 * 24 * 60 * 60 * 1000 &
                       + dt(3) * 24_int64 * 60 * 60 * 1000 &
                       + dt(5) * 60 * 60 * 1000 &
                       + dt(6) * 60 * 1000 + dt(7) * 1000 &
                       + dt(8)
               end if
               pid = getpid()
               t = ieor(t, int(pid, kind(t)))
               do i = 1, n
                  seed(i) = lcg(t)
               end do
            !call random_seed(put=seed)   
            	   call sgrnd(seed(1))
          contains
            function lcg(s)
              integer :: lcg
              integer(int64) :: s
              if (s == 0) then
                 s = 104729
              else
                 s = mod(s, 4294967296_int64)
              end if
              s = mod(s * 279470273_int64, 4294967291_int64)
              lcg = int(mod(s, int(huge(0), int64)), kind(0))
            end function lcg
           
          end subroutine init_random_seed

! Donne la fonction de densité d'une loi N(0,1)
function densite_normale(x)
use i_Parametres
implicit none
real(kind=rdp) pi,densite_normale,x
pi=4._rdp*atan(1._rdp)
densite_normale=1._rdp/sqrt(2._rdp*pi)*exp(-x**2/2._rdp)
end function densite_normale


!Fonction de repartition de la fonction normale (approximee sinon integrale trop lourde à calculer)
function repart_normale(x)
use i_Parametres
implicit none
real(kind=rdp) repart_normale,x
real(kind=rdp) u,Z,b1,b2,b3,b4,b5,t,t2,t4
repart_normale=0._rdp

 !fonction de répartition de la loi normale centrée réduite
   !    (= probabilité qu'une variable aléatoire distribuée selon
     !  cette loi soit inférieure à x)
      ! formule simplifiée proposée par Abramovitz & Stegun dans le livre
      ! "Handbook of Mathematical Functions" (erreur < 7.5e-8)
    u = abs(x) !# car la formule n'est valable que pour x>=0
 
    Z = densite_normale(u)!# ordonnée de la LNCR pour l'absisse u
    b1 = 0.319381530
    b2 = -0.356563782
    b3 = 1.781477937
    b4 = -1.821255978
    b5 = 1.330274429
 
    t = 1/(1+0.2316419*u)
    t2 = t*t
    t4 = t2*t2
 
    repart_normale = 1-Z*(b1*t + b2*t2 + b3*t2*t + b4*t4 + b5*t4*t)
 
    if (x<0._rdp) repart_normale = 1.0-repart_normale 
end function repart_normale

FUNCTION ran0() RESULT(fn_val)
!=====================================================================
!  "Minimal standard" pseudo-random number generator of Park and Miller.
!  Returns a uniform random deviate r s.t. 0 < r < 1.0.
!  Set seed to any non-zero integer value to initialize a sequence, then do
!  not change seed between calls for successive deviates in the sequence.

!  References:
!     Park, S. and Miller, K., "Random Number Generators: Good Ones
!        are Hard to Find", Comm. ACM 31, 1192-1201 (Oct. 1988)
!     Park, S. and Miller, K., in "Remarks on Choosing and Implementing
!        Random Number Generators", Comm. ACM 36 No. 7, 105-110 (July 1993)
!=====================================================================
! *** Declaration section ***

!     Output:
REAL(kind=rdp) :: fn_val

!     Constants:

INTEGER, PARAMETER  :: a = 65539, m = 2147483647, q = 44488, r = 2531011

REAL(kind=rdp), PARAMETER :: scale = 1./m, eps = 1.2E-7, rnmx = 1. - eps

!     Local:
INTEGER  :: j

! *** Executable section ***
j = (a*iseed)/m
iseed = a*iseed-j*m
IF (iseed < 0) iseed = iseed + m
fn_val = MIN(iseed*scale, rnmx)

RETURN
END FUNCTION ran0


subroutine init_seed_ran0(seed)
implicit none
integer, intent(in) :: seed
iseed=seed
end subroutine init_seed_ran0
end module rand_num
