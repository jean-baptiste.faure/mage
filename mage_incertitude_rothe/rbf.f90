subroutine Interp
use i_Parametres, only : rdp, np, issup
use i_Consigne, only : nc, wmin, wmax, nst, nbok
use linalg
implicit none
! Variables locales
   integer :: n, k, nok, i, lu,ntest,nmax,pas,j
   real(kind=rdp) :: ww(np),w_max(np),w_min(np),epsilon,epsilon_opt,erreur,test
   real(kind=rdp),allocatable::parametres(:,:),valeurs(:),coeff(:),coeff_opt(:),coeff_temp(:)
   real(kind=rdp),allocatable::parametres_temp(:,:),valeurs_temp(:),matrice(:,:),inverse(:,:)
   real(kind=rdp) :: a, b, foo, err_cal,foo_min, foo_max,ecart,foo_ini,delta
   character(len=1200) :: texte
   logical :: bstop, bpause
   !real(kind=rdp),allocatable :: matrice(:,:),inverse(:,:)
   logical:: testeur
!--------------------------------------------------------------------------
   write(*,*) 'Simulations pour la méthode interpolation rbf'
   write(8,*) 'Simulations pour la méthode interpolation rbf'
   nok = 0  ;  n = 0; ntest=10;nmax=10000
   allocate (parametres(np,ntest))
   allocate (valeurs(ntest))
   allocate(coeff(ntest+1))
   allocate(coeff_opt(ntest+1))
   testeur=.true.
   
  
if(testeur) then
   ww = (wmax + wmin)/2._rdp
   do while (nok<ntest)
      n = n+1
      foo = Foo_Surface(ww,err_cal)
      if (n==1) foo_ini=foo
      if (err_cal < 1.e-06_rdp) then
      nok = nok + 1
      valeurs(nok)=foo
      write (*,*)  ' nb sim ',nok,' surface inondee ',valeurs(nok)*0.0001_rdp
      parametres(:,nok)=ww
      end if
      ! valeur des parametres aleatoires
         do k = 1, nc
            ww(k) = random(0.99_rdp*wmin(k),1.01_rdp*wmax(k))
            ww(k) = max (ww(k),wmin(k))
            ww(k) = min (ww(k),wmax(k))
         enddo
      
      
      !Arrêt anticipé si on trouve un fichier stop, Pause si fichier pause
      inquire(file='stop',exist=bstop)
      if (bstop) then  !arrêt anticipé
         open(newunit=lu,file='stop',status='unknown')
         close(lu,status='delete')  !suppression du fichier 'stop'
         write(*,*) ' >>> interruption par l''utilisateur !!!'
         write(8,*) ' >>> interruption par l''utilisateur !!!'
         return
      endif
      inquire(file='pause',exist=bpause)
      if (bpause) call mise_en_veille()
   enddo
      !foo_min=foo_ini
  write (*,*) 'surface initiale ' , foo_ini
   !boucle pour epsilon optimal avec l'erreur leave-one out
   allocate (coeff_temp(ntest))
   allocate(valeurs_temp(ntest-1))
   allocate(parametres_temp(np,ntest-1))
   pas=100
   n=1
   delta=1._rdp
   testeur=.false.
   if (testeur) then
   !!!!!Optimisation du parametre epsilon
   !pour chaque epsilon on calcule pour chaque cas test la valeur extrapolee du modele
   ! issu de l'apprentissage de tous les cas sauf le courant et l'erreur est ma vraie valeur contre
   ! l'extrapolee
   do while(n<pas .and. delta>0._rdp)
           !creation des coeff rbf
           epsilon=real((n-1),kind=rdp)/(pas-1)+0.5_rdp
           !coeff=coeff_RBF_interpolation(parametres,valeurs,epsilon)
              test=0_rdp
              if(n>1) delta=erreur
           !test
           do i=1,ntest
                   if (i==1) then
                           valeurs_temp(i:ntest-1)=valeurs(i+1:ntest)
                           parametres_temp(:,i:ntest-1)=parametres(:,i+1:ntest)
                   else if (i==ntest) then
                           valeurs_temp(1:i-1)=valeurs(1:i-1)
                           parametres_temp(:,1:i-1)=parametres(:,1:i-1)
                   else
                           valeurs_temp(1:i-1)=valeurs(1:i-1)
                           valeurs_temp(i:ntest-1)=valeurs(i+1:ntest)
                           parametres_temp(:,i:ntest-1)=parametres(:,i+1:ntest)
                           parametres_temp(:,1:i-1)=parametres(:,1:i-1)
                   end if
                   coeff_temp=coeff_RBF_interpolation(parametres_temp,valeurs_temp,epsilon)
                   ww=parametres(:,i)
                   !write(*,*) abs(RBF_value(ww,coeff_temp,parametres_temp,epsilon)-valeurs(i))
                   test=test+abs(RBF_value(ww,coeff_temp,parametres_temp,epsilon)-valeurs(i))
                   write(*,*) 'iteration test',i
           end do
           if (n==1) then
           erreur=test
           epsilon_opt=epsilon
           end if
           write(*,*) 'iteration epsilon',n,'epsilon',epsilon,'erreur',test
           if(test<erreur) then
                   erreur=test
                   epsilon_opt=epsilon
           end if
           if (n>1)  delta=erreur-test
           n=n+1
   end do
    write (*,*) 'erreur optimale : ',erreur, 'epsilon optimal', epsilon_opt
   coeff_opt=coeff_RBF_interpolation(parametres,valeurs,epsilon_opt)
   else
   epsilon_opt=0.5_rdp
   coeff_opt=coeff_RBF_interpolation(parametres,valeurs,epsilon_opt)
   end if
   do i=1,ntest
                   ww=parametres(:,i)
                   test=RBF_value(ww,coeff_opt,parametres,epsilon_opt)
                    write(*,*) ' valeur ', valeurs(i), ' valeur interpolee ', test , ' ecart ', (valeurs(i)-test)/valeurs(i) 
   end do
   foo_min=foo_ini
  write (*,*) 'surface initiale ' , foo_ini
   foo_max=0.
   ! Calcul du maximum et du minimum de la fonction + ecart
   ! Methode statistique
   do n=1,nmax
           do k = 1, nc
                            ww(k) = random(0.99_rdp*wmin(k),1.01_rdp*wmax(k))
                            ww(k) = max (ww(k),wmin(k))
                            ww(k) = min (ww(k),wmax(k))
                    enddo
                    foo=RBF_value(ww,coeff_opt,parametres,epsilon_opt)
                    !write (*,*) ' iteration : ', n , 'valeur :' , foo 
                    if(foo>foo_max)  then 
                    foo_max=foo
                    w_max=ww
                    end if
                    if(foo<foo_min) then
                    foo_min=foo
                    w_min=ww
                    end if
   end do
   ! Methode recuit simule
   ! Methode cross entropy
   ! Resultat 
   foo=Foo_Surface(w_min,err_cal)
   write(*,*) 'simulation',foo,'modele approche',foo_min, 'calage',err_cal
   foo=Foo_Surface(w_max,err_cal)
   write(*,*) 'simulation',foo,'modele approche',foo_max, 'calage',err_cal
   ecart=((foo_max-foo_min)/foo_ini)*100_rdp
   
   
   write(*,*) foo_max,foo_min
   write(*,*) ' Nombre de simulations effectuees : ' &
   ,ntest, ' Nombre valeurs avec le modele simplifie ', nmax, &
   ' valeur maximale ' , real(foo_max*0.0001_rdp,kind=4), ' valeur minimale ', real(foo_min*0.0001_rdp,kind=4)
    write(*,*) ' ecart obtenu ', real(ecart,kind=4), '%'   
    write(8,*) ' Nombre de simulations effectuees : ',&
    ntest, 'Nombre valeurs avec le modele simplifie', nmax, &
   ' valeur maximale ' , foo_max*0.0001_rdp, ' valeur minimale ', foo_min*0.0001_rdp
   write(8,*) ' ecart obtenu ', ecart, '%'  
   end if
end subroutine Interp


function coeff_RBF_interpolation(echantillon,valeurs,epsilon)
use linalg
use i_Parametres
implicit none
integer:: i,j,k,l,n
real(kind=rdp) :: echantillon(:,:),valeurs(:),dummy,epsilon
real(kind=rdp),allocatable::coeff_RBF_interpolation(:),A(:,:),B(:)
! creation de la matrice pour resolution du systeme
n=size(echantillon(1,:))
allocate (coeff_RBF_interpolation(n+1))
allocate (A(n+1,n+1))
allocate (B(n+1))

do i=1,n
        B(i)=valeurs(i)
        do j=1,n
                dummy=sqrt(dist_euclide(echantillon(:,i),echantillon(:,j)))
                A(i,j)=rbf_gauss(dummy,epsilon)
        end do
end do
A(n+1,:)=1._rdp
A(:,n+1)=1._rdp
A(n+1,n+1)=0._rdp
B(n+1)=0._rdp
! resolution du systeme Ax=B
coeff_RBF_interpolation=resolution_syst(A,B)
end function coeff_RBF_interpolation

function RBF_value(param,coeff,echantillon,epsilon)
use linalg
use i_Parametres
implicit none
integer::i,j,k,degre
real(kind=rdp)::param(:),coeff(:),echantillon(:,:),RBF_value,dummy,epsilon
n=size(echantillon(1,:))
RBF_value=0
do i=1,n
        dummy=sqrt(dist_euclide(param,echantillon(:,i)))
        RBF_value=RBF_value+coeff(i)*rbf_gauss(dummy,epsilon)
end do
RBF_value=RBF_value+coeff(n+1)
!test erreur
end function RBF_value

function rbf_gauss(x,epsilon)
use i_Parametres
implicit none
real (kind=rdp) rbf_gauss,x,epsilon
rbf_gauss=exp(-x**2*epsilon)
end function rbf_gauss

function rbf_rad(x,c)
use i_Parametres
implicit none
real (kind=rdp) rbf_rad,x,c
rbf_rad=sqrt(x*x+c*c)
end function rbf_rad



