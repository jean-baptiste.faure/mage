
! module pour effectuer le recuit en methode d optimisation generale
	
! prend en parametres :
! une fonction de type f(w) et le parametre optimal a stocker
	
! On doit l initialiser avec init_recuit prenant :
!nbparam, le nombre de paramtres
!w_min, bornes inf des param
! w_max, bornes sup des param
! p_ini,f_ini, parametres et valeurs initiales 
! min_ou_max 1/-1 pour minimiser/maximiser
!temp, le taux de decroissance de la temperature
	
module recuit_simule
use i_Parametres
use rand_num
implicit none
integer  :: nb_parametres

integer :: nequil  ! 
! sinon nequil est le nombre d'évaluations sans amélioration
! de l'optimum nécessaires avant une réduction de la température
real(kind=rdp) :: T0, T
real(kind=rdp) :: dth0 ! taux de variation de la température
real(kind=rdp) :: fk_ini     !valeur initiale de la fonction-coût
real(kind=rdp) :: fk_opt     !meilleure valeur de la fonction-coût
real(kind=rdp) , allocatable :: param_opt(:),param_ini(:),borne_sup(:),borne_inf(:),amplitude(:)

integer :: nb_eval   !nombre d'évaluation de la fonction-coût FUNK()
integer :: nb_simul  !nombre de simulations réalisées (appels de solveur)
integer :: iopt      !numéro de la meilleure évaluation de FUNK()
logical:: affichage
real(kind=rdp) :: seuil_amelioration

character :: unite*3
real(kind=rdp) :: min_max ! 1 pour minimisation et -1 pour maximisation

contains

! fonction pour desallouer si on refait un recuit
subroutine reset_recuit()
implicit none

if (allocated(borne_sup)) deallocate (borne_sup)
if (allocated(borne_inf)) deallocate (borne_inf)
if (allocated(param_opt)) deallocate (param_opt)
if (allocated(param_ini)) deallocate (param_ini)
if (allocated(amplitude)) deallocate (amplitude)
end subroutine reset_recuit

! initialisation d un recuit avec les bornes, la decroissance ...
subroutine init_recuit(nbparam,w_min,w_max,p_ini,f_ini,min_ou_max,temp)
use i_Parametres
implicit none 
integer :: nbparam
real(kind=rdp) :: w_min(:),w_max(:),p_ini(:)
real(kind=rdp) :: f_ini,min_ou_max,temp
call reset_recuit()
min_max=min_ou_max
T0=2._rdp
dth0=temp
T=T0
affichage=.false. ! booleen pour afficher tous le processus ou seulement le resultat
nequil=1
nb_parametres=nbparam
allocate(borne_sup(nb_parametres),borne_inf(nb_parametres),param_opt(nb_parametres), &
& param_ini(nb_parametres),amplitude(nb_parametres))
borne_sup=w_max
borne_inf=w_min
param_opt=p_ini
fk_ini=f_ini
fk_opt=fk_ini
param_ini=p_ini
amplitude=(w_max-w_min)/2._rdp
end subroutine init_recuit

! routine du recuit faite sur le meme modele que celle utilisee directement
subroutine recuit_sa(W_optimal,funk)
use rand_num
implicit none
! Prototype
 
   real(kind=rdp), intent(out) :: W_optimal(nb_parametres)
! variables locales
   integer :: idem, nb_iter, nko, ntot, nequil0
   real(kind=rdp) :: tmin
   real(kind=rdp) :: W_new(nb_parametres), W_sa(nb_parametres), rdom
   real(kind=rdp) :: fk_new, fk_sa !valeurs intermédiaires de la fonction-coût
   real(kind=rdp) :: test
   real(kind=rdp) :: alpha, nmax, beta
   character :: decision*7 ,MinMax*6
logical:: arret

! interfaces
   interface
      function funk(w)
         use i_Parametres, only : rdp,np
         real(kind=rdp), intent(in) :: w(:)
         real(kind=rdp) :: funk
      end function funk
   end interface
   
!---------------------------------------------------------------------------
   ! initialisation
   T= T0                 !initialisation de la température
   nb_iter = 0                     !nombre d'itérations sur la température
   nb_eval = 0
   MinMax = ' Min ='  ;  if (min_max< 0) MinMax = ' Max ='
   nko = 0 ; ntot = 0
   nequil0 = nequil
   ! paramètres pour le calcul de la température
   alpha = 0.001_rdp  ;  nmax = real(T0/dth0,rdp)  ;  beta = -Log(alpha)/Log(nmax)
   W_optimal=param_ini ; W_sa = param_ini
   fk_sa = fk_ini
   tmin = 0.01_rdp*T0
   do while (T > 0._rdp)     !boucle sur la température
      nb_iter = 0
      idem = 0  !nb d'évaluations sans amélioration de l'optimum
      ntot = 0  !;  nko = 0
      amplitude =(borne_sup-borne_inf)*0.5_rdp*max( T / T0,0.1_rdp)
      arret=.false.
      if (nequil > 0) w_sa = w_optimal
      do while (.not. arret)   !boucle à température fixée
         call modif(w_sa,w_new,amplitude)
         fk_new = funk(w_new)  ;  nb_eval = nb_eval+1  ;  ntot = ntot+1
         if (min_max*(fk_new-fk_opt) < seuil_amelioration) then !NB: seuil_amelioration est négatif
            idem = 0        !remise à 0 du compteur de non-améliorations
            iopt = nb_eval
            fk_opt = fk_new  ;  w_optimal = w_new  !mise à jour de l'optimum
            fk_sa = fk_new   ;  w_sa = w_new      !on garde la nouvelle consigne
            decision = ' ##### '
         elseif (min_max*(fk_new-fk_sa) < seuil_amelioration) then  
            fk_sa = fk_new  ;  w_sa = w_new
            decision = ' +++++ '
            nb_iter=nb_iter+1
            else
            test = exp(- min_max*(fk_new-fk_opt)/(T))
            call random_number(rdom)
            if (test > rdom) then   !>>>on garde la nouvelle consigne
               fk_sa = fk_new  ;  w_sa = w_new
               !l'optimum n'a pas changé, seulement fk_sa
               decision = ' ===== '
               nko = nko+1
               nb_iter=nb_iter+1
               !idem = idem + 1
            else             !>>>on rejette la nouvelle consigne
               decision = ' ----- '
               idem = idem + 1
            endif
         endif
         if (affichage) then
         write(*,'(f30.10,a,f30.10,a,f30.10)') fk_ini,' Actuel = ',fk_sa,&
                 MinMax,fk_opt
         !fichier txt sortie
   write(8,'(f30.10,a,f30.10,a,f30.10)') fk_ini,' Actuel = ',fk_sa,&
                 MinMax,fk_opt
        end if
        
              arret = (nb_iter > nequil) .OR. (idem > nequil) .OR. (nequil == 0)
      enddo
      if (T > tmin) then
         T = T*(1._rdp-dth0) !décroissance geometrique
      else
         T = T - dth0*tmin
      endif
   enddo
   fk_opt = funk(w_optimal)  !pour que la dernière simulation corresponde à l'optimum
   write(*,'(f30.10,a,f30.10,a,f30.10)') fk_ini,' Actuel = ',fk_sa,&
                 MinMax,fk_opt
         !fichier txt sortie
   write(8,'(f30.10,a,f30.10,a,f30.10)') fk_ini,' Actuel = ',fk_sa,&
                 MinMax,fk_opt
end subroutine recuit_sa


! fonction de voisinage pour le recuit tire nouveaux paramtres  w_new dans une boule definie par ampli 
! autour de w_sa
subroutine modif(w_sa,w_new,ampli)
!==============================================================================
!  routine de modification
!   sans arrondi
!==============================================================================
   use i_Parametres, only : rdp
   use rand_num
   implicit none
! Prototype
   real(kind=rdp), intent(in) :: W_sa(:), ampli(:)
   real(kind=rdp), intent(out) :: W_new(:)
! Variables locales
   integer :: n
!--------------------------------------------------------------------------
!!$omp parallel default(shared) private(n)
!   !$omp do schedule(dynamic)
   do n = 1, nb_parametres
      if( ampli(n) > 0._rdp ) then
         w_new(n) = random(w_sa(n)-ampli(n),w_sa(n)+ampli(n))
         w_new(n)=max(w_new(n),borne_inf(n))
         w_new(n)=min(w_new(n),borne_sup(n))
      else
         w_new(n) = w_sa(n)
      endif
   end do
!   !$omp end do
!!$omp end parallel
end subroutine modif
end module recuit_simule


! meme module que precedemment avec nom differents pour pouvoir paralleliser 
! l utilisation est la meme sauf que le nom des fonctions est maintenant suivi de 1 
module recuit_simule1
use i_Parametres
use rand_num
implicit none
integer  :: nb_parametres1

integer :: nequil1  ! 
! sinon nequil est le nombre d'évaluations sans amélioration
! de l'optimum nécessaires avant une réduction de la température
real(kind=rdp) :: T01, T1
real(kind=rdp) :: dth1 ! taux de variation de la température
real(kind=rdp) :: fk_ini1     !valeur initiale de la fonction-coût
real(kind=rdp) :: fk_opt1     !meilleure valeur de la fonction-coût
real(kind=rdp) , allocatable :: param_opt1(:),param_ini1(:),borne_sup1(:),borne_inf1(:) &
& ,amplitude1(:)

integer :: nb_eval1   !nombre d'évaluation de la fonction-coût FUNK()
integer :: nb_simul1  !nombre de simulations réalisées (appels de solveur)
integer :: iopt1      !numéro de la meilleure évaluation de FUNK()
logical:: affichage1
real(kind=rdp) :: seuil_amelioration

character :: unite*3
real(kind=rdp) :: min_max1 ! 1 pour minimisation et -1 pour maximisation

contains

subroutine reset_recuit1()
implicit none

if (allocated(borne_sup1)) deallocate (borne_sup1)
if (allocated(borne_inf1)) deallocate (borne_inf1)
if (allocated(param_opt1)) deallocate (param_opt1)
if (allocated(param_ini1)) deallocate (param_ini1)
if (allocated(amplitude1)) deallocate (amplitude1)
end subroutine reset_recuit1

subroutine init_recuit1(nbparam,w_min,w_max,p_ini,f_ini,min_ou_max,temp)
use i_Parametres
implicit none 
integer :: nbparam
real(kind=rdp) :: w_min(:),w_max(:),p_ini(:)
real(kind=rdp) :: f_ini,min_ou_max,temp
call reset_recuit1()
min_max1=min_ou_max
T01=2._rdp
dth1=temp
T1=T01
affichage1=.false.
nequil1=1
!seuil_amelioration=0.0005_rdp
nb_parametres1=nbparam
allocate(borne_sup1(nb_parametres1),borne_inf1(nb_parametres1),param_opt1(nb_parametres1), &
& param_ini1(nb_parametres1),amplitude1(nb_parametres1))
borne_sup1=w_max
borne_inf1=w_min
param_opt1=p_ini
fk_ini1=f_ini
fk_opt1=fk_ini1
param_ini1=p_ini
amplitude1=(w_max-w_min)/2._rdp
end subroutine init_recuit1


subroutine recuit_sa1(W_optimal,funk)
use rand_num
implicit none
! Prototype
 
   real(kind=rdp), intent(out) :: W_optimal(nb_parametres1)
! variables locales
   integer :: idem,  nb_iter,nko, ntot, nequil0,n_test
   real(kind=rdp) :: tmin
   real(kind=rdp) :: W_new(nb_parametres1), W_sa(nb_parametres1), rdom
   real(kind=rdp) :: fk_new, fk_sa !valeurs intermédiaires de la fonction-coût
   real(kind=rdp) :: test
   real(kind=rdp) :: alpha, nmax, beta
   character :: decision*7, MinMax*6
logical:: arret

! interfaces
   interface
      function funk(w)
         use i_Parametres, only : rdp,np
         real(kind=rdp), intent(in) :: w(:)
         real(kind=rdp) :: funk
      end function funk
   end interface
   
!---------------------------------------------------------------------------
   ! initialisation
   T1= T01                 !initialisation de la température
   nb_iter = 0                     !nombre d'itérations sur la température
   nb_eval1 = 0
   MinMax = ' Min ='  ;  if (min_max1< 0) MinMax = ' Max ='
   nko = 0 ; ntot = 0
   nequil0 = nequil1
   ! paramètres pour le calcul de la température
   alpha = 0.001_rdp  ;  nmax = real(T01/dth1,rdp)  ;  beta = -Log(alpha)/Log(nmax)
   W_optimal=param_ini1 ; W_sa = param_ini1
   fk_sa = fk_ini1
   n_test=0
   tmin = 0.01_rdp*T01
   do while (T1 > 0._rdp)     !boucle sur la température
      idem = 0  !nb d'évaluations sans amélioration de l'optimum
      ntot = 0  !;  nko = 0
      amplitude1 = (borne_sup1-borne_inf1)*0.5_rdp* max(T1 / T01,0.1)
      arret=.false.
      if (nequil1 > 0) w_sa = w_optimal
      do while (.not. arret)   !boucle à température fixée
         call modif1(w_sa,w_new,amplitude1)
         fk_new = funk(w_new)  ;  nb_eval1 = nb_eval1+1  ;  ntot = ntot+1
         if (min_max1*(fk_new-fk_opt1) < seuil_amelioration) then !NB: seuil_amelioration est négatif
            idem = 0        !remise à 0 du compteur de non-améliorations
            iopt1 = nb_eval1
            fk_opt1 = fk_new  ;  w_optimal = w_new  !mise à jour de l'optimum
            fk_sa = fk_new   ;  w_sa = w_new      !on garde la nouvelle consigne
            decision = ' ##### '
            nb_iter=nb_iter+1
         elseif (min_max1*(fk_new-fk_sa) < seuil_amelioration) then  
            fk_sa = fk_new  ;  w_sa = w_new
            decision = ' +++++ '
            nb_iter=nb_iter+1
         else
         n_test=n_test+1
            test = exp(- min_max1*(fk_new-fk_opt1)/(T1))
            call random_number(rdom)
            if (test > rdom) then   !>>>on garde la nouvelle consigne
               fk_sa = fk_new  ;  w_sa = w_new
               !l'optimum n'a pas changé, seulement fk_sa
               decision = ' ===== '
               nko = nko+1
               nb_iter=nb_iter+1
               !idem = idem + 1
            else             !>>>on rejette la nouvelle consigne
               decision = ' ----- '
               idem = idem + 1
            endif
         endif
         if (affichage1) then
         write(*,'(f30.10,a,f30.10,a,f30.10)') fk_ini1,' Actuel = ',fk_sa,&
                 MinMax,fk_opt1
         !fichier txt sortie
   write(8,'(f30.10,a,f30.10,a,f30.10)') fk_ini1,' Actuel = ',fk_sa,&
                 MinMax,fk_opt1
        end if
        
     arret = (nb_iter > nequil0) .OR. (idem > nequil0) .OR. (nequil0 == 0)
      enddo
      if (T1 > tmin) then
         T1 = T1*(1._rdp-dth1) !décroissance exponentielle
         ! T1 = T1-dth1!changement
      else
         T1 = T1- dth1*tmin
      endif
   enddo
   fk_opt1 = funk(w_optimal)  !pour que la dernière simulation corresponde à l'optimum
   write(*,'(f30.10,a,f30.10,a,f30.10)') fk_ini1,' Actuel = ',fk_sa,&
                 MinMax,fk_opt1
         !fichier txt sortie
   write(8,'(f30.10,a,f30.10,a,f30.10)') fk_ini1,' Actuel = ',fk_sa,&
                 MinMax,fk_opt1
end subroutine recuit_sa1

subroutine modif1(w_sa,w_new,ampli)
!==============================================================================
!  routine de modification
!   sans arrondi
!==============================================================================
   use i_Parametres, only : rdp
   use rand_num
   implicit none
! Prototype
   real(kind=rdp), intent(in) :: W_sa(:), ampli(:)
   real(kind=rdp), intent(out) :: W_new(:)
! Variables locales
   integer :: n
!--------------------------------------------------------------------------
!!$omp parallel default(shared) private(n)
!   !$omp do schedule(dynamic)
   do n = 1, nb_parametres1
      if( ampli(n) > 0._rdp ) then
         w_new(n) = random(max(w_sa(n)-ampli(n),borne_inf1(n)),min(w_sa(n)+ampli(n), &
         &borne_sup1(n)))
      else
         w_new(n) = w_sa(n)
      endif
   end do
!   !$omp end do
!!$omp end parallel
end subroutine modif1



end module recuit_simule1


