!Présentation des résultats
   if (FOtyp < 2) then
      tag = '5' ; tag0 = '4'  !solution moyenne
      ww = 0.5_rdp * (wmax+wmin) ; foo0 = working_Area(ww,tag,resultats)* 0.0001_rdp
      err0 = calage(ww,tag0,resultats)
      do n = 1, nb_obs_z
         is = isect(laisse_crue(n)%ib, laisse_crue(n)%x, 0.01_rdp)
         Z0_max(n) = resultats%Z_max(is)
      enddo
      tag = '2' ; tag0 = '0'  !solution N°1      
      ww = w_opt(:,1) ; foo1 = working_Area(ww,tag,resultats)* 0.0001_rdp
      err1 = calage(ww,tag0,resultats)
      do n = 1, nb_obs_z
         is = isect(laisse_crue(n)%ib, laisse_crue(n)%x, 0.01_rdp)
         Z1_max(n) = resultats%Z_max(is)
      enddo
      tag = '3' ; tag0 = '1'  !solution N°2      
      ww = w_opt(:,2) ; foo2 = working_Area(ww,tag,resultats)* 0.0001_rdp
      err2 = calage(ww,tag0,resultats)
      do n = 1, nb_obs_z
         is = isect(laisse_crue(n)%ib, laisse_crue(n)%x, 0.01_rdp)
         Z2_max(n) = resultats%Z_max(is)
      enddo
      ! affichage sur le terminal
                    
      write(*,'(a)') ' Solution : '
      write(*,'(12x,f10.4,a,7x,f10.4,a)') foo1,' ha',foo2,' ha'
      write(*,'(10x,4f10.3)') (w_opt(2*i+1,1),w_opt(2*i+2,1),w_opt(2*i+1,2),w_opt(2*i+2,2),i=0,nst/2-1)
      !write(*,'(10x,2f10.3)') (w_opt(i,1),w_opt(i,2),i=nst+1,nc)
      write(*,'(a)') ' Consignes geometrie : voir le fichier sortie'
      
      ! affichage sur la sortie copiant l execution    
      write(8,'(a)') ' Solution : '
      write(8,'(12x,f10.4,a,7x,f10.4,a)') foo1,' ha',foo2,' ha'
      write(8,'(a)') ' Coefficients de Strickler'
      write(8,'(10x,4f10.3)') (w_opt(2*i+1,1),w_opt(2*i+2,1),w_opt(2*i+1,2),w_opt(2*i+2,2),i=0,nst/2-1)
      write(8,'(a)') ' Perturbations de la cote du fond de la géométrie'
      do i = nst+1, nc
         write(8,'(a,f10.3,f10.3,10x,f10.3)') 'Pm = ',xgeo(i-nst),w_opt(i,1),w_opt(i,2)
      enddo
      tmp = affiche(fk_opt,unite)
      write(*,'(3a,i5,a,2(i5,a))') ' Optimum = ',tmp, ' a l''evaluation ',iopt,' en ',nb_eval, &
                                  ' evaluations et ',nb_simul,' simulations'
      write(8,'(3a,i5,a,2(i5,a))') ' Optimum = ',tmp, ' a l''evaluation ',iopt,' en ',nb_eval, &
                                  ' evaluations et ',nb_simul,' simulations'

      tmp = affiche(100._rdp*abs((foo1-foo2)/foo0),' %'//' ')
      write(*,'(a,f10.4,a)') ' Surface minimale = ',min(foo1,foo2),' ha'
      write(*,'(a,f10.4,a)') ' Surface maximale = ',max(foo1,foo2),' ha'
      write(*,'(2a)') ' Ecart / surface initiale = ',tmp
      write(8,'(a,f10.4,a)') ' Surface minimale = ',min(foo1,foo2),' ha'
      write(8,'(a,f10.4,a)') ' Surface maximale = ',max(foo1,foo2),' ha'
      write(8,'(2a)') ' Ecart / surface initiale = ',tmp

      write(*,'(a)') ' Verification du calage'
      write(*,'(a,3(f10.4,a))') ' Observations      ',foo1,' ha      ',foo0,' ha      ',foo2,' ha'
      write(8,'(a)') ' Verification du calage'
      write(8,'(a,3(f10.4,a))') ' Observations      ',foo1,' ha      ',foo0,' ha      ',foo2,' ha'
      do n = 1, nb_obs_z
         write(*,'(4(f10.3,a))') laisse_crue(n)%z,'         ', Z1_max(n),'         ', Z0_max(n),'         ', Z2_max(n)
         write(8,'(4(f10.3,a))') laisse_crue(n)%z,'         ', Z1_max(n),'         ', Z0_max(n),'         ', Z2_max(n)
      enddo
      write(*,'(18x,f10.4,2(9x,f10.4))') err1, err0, err2
      write(8,'(18x,f10.4,2(9x,f10.4))') err1, err0, err2
   else if (FOtyp < 6) then
      tag = '2' ; tag0 = '0'  !solution moyenne
      ww = 0.5_rdp * (wmax+wmin) ; foo0 = working_Area(ww,tag,resultats)* 0.0001_rdp
      err0 = calage(ww,tag0,resultats)
      do n = 1, nb_obs_z
         is = isect(laisse_crue(n)%ib, laisse_crue(n)%x, 0.01_rdp)
         Z0_max(n) = resultats%Z_max(is)
      enddo
      tag = '3' ; tag0 = '1'  !solution
      ww = w_opt(:,1) ; foo1 = working_Area(ww,tag,resultats)* 0.0001_rdp
      err1 = calage(ww,tag0,resultats)
      do n = 1, nb_obs_z
         is = isect(laisse_crue(n)%ib, laisse_crue(n)%x, 0.01_rdp)
         Z1_max(n) = resultats%Z_max(is)
      enddo
      write(*,'(a)') ' Solution : '
      write(*,'(12x,f10.4,a)') foo1,' ha'
      write(*,'(10x,2f10.3)') (w_opt(2*i+1,1),w_opt(2*i+2,1),i=0,nst/2-1)
      write(*,'(a)') ' Consignes geometrie : voir le fichier sortie'
      write(8,'(a)') ' Solution : '
      write(8,'(12x,f10.4,a)') foo1,' ha'
      write(8,'(a)') ' Coefficients de Strickler'
      write(8,'(10x,2f10.3)') (w_opt(2*i+1,1),w_opt(2*i+2,1),i=0,nst/2-1)
      write(8,'(a)') ' Perturbations de la cote du fond de la géométrie'
      do i = nst+1, nc
         write(8,'(a,f10.3,f10.3)') 'Pm = ',xgeo(i-nst),w_opt(i,1)
      enddo
      tmp = affiche(fk_opt,unite)
      write(*,'(3a,i5,a,2(i5,a))') ' Optimum = ',tmp, ' a l''evaluation ',iopt,' en ',nb_eval, &
                                  ' evaluations et ',nb_simul,' simulations'
      write(8,'(3a,i5,a,2(i5,a))') ' Optimum = ',tmp, ' a l''evaluation ',iopt,' en ',nb_eval, &
                                  ' evaluations et ',nb_simul,' simulations'

      tmp = affiche(100._rdp*abs((foo1-foo0)/foo0),' %'//' ')
      if (FOtyp == 2 .or. FOtyp == 3) write(*,'(a,f10.4,a)') ' Surface minimale = ',foo1,' ha'
      if (FOtyp == 4 .or. FOtyp == 5) write(*,'(a,f10.4,a)') ' Surface maximale = ',foo1,' ha'
      write(*,'(2a)') ' Ecart / surface initiale = ',tmp
      if (FOtyp == 2 .or. FOtyp == 3) write(8,'(a,f10.4,a)') ' Surface minimale = ',foo1,' ha'
      if (FOtyp == 4 .or. FOtyp == 5) write(8,'(a,f10.4,a)') ' Surface maximale = ',foo1,' ha'
      write(8,'(2a)') ' Ecart / surface initiale = ',tmp

      write(*,'(a)') ' Verification du calage'
      write(*,'(a,3(f10.4,a))') ' Observations      ',foo1,' ha      ',foo0,' ha'
      write(8,'(a)') ' Verification du calage'
      write(8,'(a,3(f10.4,a))') ' Observations      ',foo1,' ha      ',foo0,' ha'
      do n = 1, nb_obs_z
         write(*,'(3(f10.3,a))') laisse_crue(n)%z,'         ', Z1_max(n),'         ', Z0_max(n)
         write(8,'(3(f10.3,a))') laisse_crue(n)%z,'         ', Z1_max(n),'         ', Z0_max(n)
      enddo
      write(*,'(18x,f10.4,9x,f10.4)') err1, err0
      write(8,'(18x,f10.4,9x,f10.4)') err1, err0
   else if (FOtyp < 8) then
      write(*,'(a)') ' Solution : '
      write(*,'(10x,2f10.4)') (w_opt(2*i+1,1),w_opt(2*i+2,1),i=0,nst/2-1)
      write(*,'(10x,f10.4)') (w_opt(i,1),i=nst+1,nc)
      write(8,'(a)') ' Solution : '
      write(8,'(10x,2f10.6)') (w_opt(2*i+1,1),w_opt(2*i+2,1),i=0,nst/2-1)
      write(8,'(10x,f10.6)') (w_opt(i,1),i=nst+1,nc)
      
      ww = w_opt(:,1)
      err1 = calage(ww,'0',resultats)
      do n = 1, nb_obs_z
         is = isect(laisse_crue(n)%ib, laisse_crue(n)%x, 0.01_rdp)
         Z1_max(n) = resultats%Z_max(is)
      enddo
      do n = 1, nb_obs_q
         is = isect(Q_rep(n)%ib, Q_rep(n)%x, 0.01_rdp)
         if (FOtyp == 6) then
            Z2_max(n) = resultats%Qtot(is)
         else if (FOtyp ==7) then
            Z2_max(n) = 100._rdp*resultats%Qfp(is)/resultats%Qtot(is)
         else
            stop 445
         endif
      enddo
      do n = 1, max(nb_obs_z,nb_obs_q)
         if (n <= min(nb_obs_z,nb_obs_q)) then
            write(6,'((a3,i2,3f10.3),(a4,i2,3f10.3))') &
                     'Z ',laisse_crue(n)%ib, laisse_crue(n)%x, laisse_crue(n)%z, z1_max(n), &
                   '  Q ',Q_rep(n)%ib, Q_rep(n)%x, Q_rep(n)%z, z2_max(n)
         else if (n <= nb_obs_q) then
            write(6,'(35x,a4,i2,3f10.3)') '  Q ',Q_rep(n)%ib, Q_rep(n)%x, Q_rep(n)%z, z2_max(n)
         else if (n <= nb_obs_z) then
            write(6,'(a3,i2,3f10.3)') 'Z ',laisse_crue(n)%ib, laisse_crue(n)%x, laisse_crue(n)%z, z1_max(n)
         endif                                   
      enddo
      tmp = affiche(fk_opt,unite)
      write(*,'(3a,i4,a,2(i4,a))') ' Optimum = ',tmp, ' a l''evaluation ',iopt,' en ',nb_eval, &
                                  ' evaluations et ',nb_simul,' simulations'
      write(8,'(3a,i4,a,2(i4,a))') ' Optimum = ',tmp, ' a l''evaluation ',iopt,' en ',nb_eval, &
                                  ' evaluations et ',nb_simul,' simulations'
   else
      continue
   endif
   write(*,*)

   call cpu_time(fin)
   call M4b(debut,fin,values)
   write(*,*)
   close(7)
   close(8)
   
