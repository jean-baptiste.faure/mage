#!/bin/bash

#script pour compiler et executer programme incertitude de mage

#dossier pour compiler (mettre le chemin ou les fichiers fortran sont presents) 
cd /home/rothe/Bureau/mage/mage_sa_parallele

#compilation
make -f mage_sa_debug.mak build

#dossier execution
cd /home/rothe/bin/mage_install/master

echo "Combien d'execution du programme voulez-vous? (1 minimum)"
read nombre
if [ $nombre -lt 1 ] ; then
	nombre=1
fi
#execution du programme
for i in `seq 1 $nombre`;
do
echo "execution $i "
# a changer si on execute pas dans le meme dossier que l executable
./Main saar_sa.dat
done



gprof ./Main gmon.out > debug.txt
#fin du script
