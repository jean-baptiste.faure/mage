!Gestion de la commande passee au terminal
   iarg = 1 ; call get_command_argument(iarg,argmnt)   ! lit la première ligne de commande du terminal
   do
      if (argmnt(1:2) == '--') then
         write(6,'(a)') entame//version_string
         write(6,'(a)') 'STOP : argument invalide : '//trim(argmnt)
         stop
      else if (argmnt(1:2) == '-v') then
         call print_Version(6)  ;  stop
      else if (argmnt(1:2) == '-h') then 
         write(6,'(a)') entame//version_string
         call print_Help()  ;  stop
      else if (argmnt(1:3) == '-g=' ) then 
         !création d'un fichier de paramètres avec les valeurs par défaut
         write(6,'(a)') entame//version_string
         write(6,'(a)') 'Création d''un fichier de paramètres avec les valeurs par défaut'
         call create_DATfile(trim(argmnt(4:)),.false.)
         stop
      else if (argmnt(1:3) == '-G=') then 
         !création d'un fichier de paramètres _commenté_ avec les valeurs par défaut
         write(6,'(a)') entame//version_string
         write(6,'(a)') 'Création d''un fichier de paramètres commenté avec les valeurs par défaut'
         call create_DATfile(trim(argmnt(4:)),.true.)
         stop
      else if (argmnt(1:3) == '-o=') then 
         !répertoire de stockage des fichiers de résultats
         outdir = trim(argmnt(4:))
         inquire(file='./'//trim(outdir),exist=bexist)
         if (.not.bexist) call execute_command_line('mkdir ./'//trim(outdir))
         iarg = iarg+1 ; call get_command_argument(iarg,argmnt) ;
      else if (argmnt(1:1) == '-' ) then
         write(6,'(a)') entame//version_string
         write(6,'(a)') 'STOP : argument de la ligne de commande inconnu : '//trim(argmnt)
         stop
      else if (len_trim(argmnt) > 0) then  !fichier de paramètres
         datafile = trim(argmnt)
         write(6,'(a)') entame//version_string
         write(6,'(a)') ' '
         exit
      else
         write(6,'(a)') entame//version_string
         stop ': Il manque le nom du fichier de paramètres'
      endif
   enddo
