#!/bin/bash


#fichier pour générer les saar .dat pour 7 laisses de crues (mais on peut en faire plus, valable pour n laisses)
#nécessite un fichier combinaison.txt qui regroupe toutes les nombre de 0 à 2^n codés en binaire 
#sur R simple à générer fonction bincombin(n) donne la matrice directement  (attention au cas 000000... l'enlever pour plus de sûreté)

while read line #boucle sur chaque ligne du fichier combinaison.txt

do

somme=0
echo $line > dummy
num=$(wc -w <dummy)
for i in $(seq 1 $num); do
	long=$(awk '{print$'"$i"'}'  ./dummy)
	echo $long
	if [ "$i" = "1" ] ; then
		b=${b}${long}
	else
		b=${b}${long}
	fi
somme=$(($somme+$long))
done




#nom indexé par la combinaison en binaire (nom+le nombre de laisse de crue + combinaison binaire) 
name="saar_laisse$somme$b.txt"
unset b

#recopie du saar.dat à utiliser
echo "basename =Saar" >> $name
echo "solveur =/home/rothe/bin/mage_install/master/mage" >> $name
echo "dossier_calage =ref_calage" >> $name
echo "dossier_simulation =ref_simulation" >> $name
echo "temperature_initiale = 2." >> $name
echo "taux_decroissance_temperature = 0.0025" >> $name
echo "nb_idem = 1" >> $name
echo "arrondi_consigne = 0.0" >> $name
echo "echelle = 1.1" >> $name
echo "penalisation = 1000." >> $name
echo "seuil_amelioration = 0.0001" >> $name
echo "*" >> $name
echo "FOtype = surface_EcartMax" >> $name
echo "*FOtype = enveloppe_EcartMax" >> $name
echo "*FOtype = calage_FIN" >> $name
echo "*FOtype = Monte-Carlo" >> $name
echo "*FOtype = krigeage" >> $name
echo "*FOtype = RBFGAUSS" >> $name
echo "*FOtype = LHSMonte-Carlo" >> $name
echo "*FOtype = Entropiecroisee" >> $name
echo "nbok = 1000" >> $name
echo "*" >> $name
echo "*geometrie = 0.1" >> $name
echo "*" >> $name
echo "<strickler>" >> $name
echo   "1  500.0   1700.0      22. 28. 12. 18. 25. 15. 25. 15." >> $name
echo   "1 1700.0   2900.0      22. 28. 12. 18. 25. 15. 25. 15." >> $name
echo  "1 2900.0   4100.0      22. 28. 12. 18. 25. 15. 25. 15." >> $name
echo  "1 4100.0   5300.0      22. 28. 12. 18. 25. 15. 25. 15." >> $name
echo  "1 5300.0   6500.0      22. 28. 12. 18. 25. 15. 25. 15." >> $name
echo  "1 6500.0   7600.0      22. 28. 12. 18. 25. 15. 25. 15." >> $name
echo "</strickler>" >> $name
echo "*" >> $name

echo "erreur_calage = 0.10" >> $name
echo "coeff_calage_Z = 1." >> $name
echo "coeff_calage_Q = 0." >> $name
echo "<calage_z>" >> $name
echo "* observations vraies" >> $name
#boucle pour définir les  laisses de crues 
for i in $(seq 1 $num); do
long=$(awk '{print$'"$i"'}'  ./dummy) #extraction de la i eme valeur en binaire 
if [ "$long" = "1" ] ; then #  si i eme valeur laisse  activée
 	if [ "$i" = "1" ] ; then 
 	echo " 1   7600.0000000000000        139.58000000000001 " >>$name #ecriture de la première position de laisse de crue
 	elif [ "$i" = "2" ] ; then #pour considérer n laisses de crue continuer de faire des elif jusqu'à atteindre le cas n
 	echo " 1   6500.0000000000000        139.02000000000001 " >>$name
 	elif [ "$i" = "3" ] ; then
 	echo " 1   5300.0000000000000        138.38000000000000 " >>$name
 	elif [ "$i" = "4" ] ; then
 	echo " 1   4100.0000000000000        137.13000000000000" >>$name
 	elif [ "$i" = "5" ] ; then
 	echo " 1   3200.0000000000000        136.46000000000001 " >>$name
 	elif [ "$i" = "6" ] ; then
 	echo " 1   2300.0000000000000        135.44999999999999 " >>$name
 	else
 	echo " 1   1200.0000000000000        134.58000000000001  " >>$name
 	fi
fi
done
  

echo "</calage_z>" >> $name
rm dummy
  
done < "combinaison.txt"




