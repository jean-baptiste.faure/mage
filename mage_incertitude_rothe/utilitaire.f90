subroutine M4b(debut,fin,values0)
!==============================================================================
!--categorie : IHM
!
!                   Fin du comptage du temps de calcul
!        et ecriture du nombre total d'iterations et pas de temps
!==============================================================================
   use i_Parametres, only: rdp
   use i_Annealing, only: cpu_externe
   implicit none

! -- Prototype --
   integer,intent(in) :: Values0(8)
   real(kind=rdp), intent(in) :: debut, fin

! -- Variables --
   integer :: Values(8)
   integer :: Annee0,Mois0,Jour0,Heure0,Minute0,Seconde0,Centieme0
   integer :: Annee1,Mois1,Jour1,Heure1,Minute1,Seconde1,Centieme1
   integer :: Heure, Minute, Seconde
   integer :: Heure_cpu, Minute_cpu, Seconde_cpu, mille_cpu
   integer :: TimeSec0,TimeSec1, Duree, Duree_cpu
   character(len=8) :: DDate
   character(len=10) :: Time
   character(len=5) :: Zone
!------------------------------------------------------------------------------
   annee0=values0(1)
   mois0=values0(2)
   jour0=values0(3)
   heure0=values0(5)
   minute0=values0(6)
   seconde0=values0(7)
   centieme0=values0(8)
!date de la fin de la simulation
   call date_and_time(ddate,time,zone,values)
   annee1=values(1)
   mois1=values(2)
   jour1=values(3)
   heure1=values(5)
   minute1=values(6)
   seconde1=values(7)
   centieme1=values(8)
!---duree du calcul
   heure=heure1+24*(jour1-jour0)  ! changement de jour eventuel
   timesec0=seconde0+60*minute0+3600*heure0
   timesec1=seconde1+60*minute1+3600*heure
   duree=timesec1-timesec0
   if (centieme1-centieme0 > 50) duree = duree+1
   heure = duree/3600
   duree = duree - heure*3600
   minute = duree/60
   seconde = duree - minute*60
   !call cpu_time(fin)
   if (debut < 0. .or. fin < 0.) then
      duree_cpu = -9999.
      mille_cpu = -999
   else
      duree_cpu = int(fin-debut+cpu_externe)
      mille_cpu = 1000*(fin-debut+cpu_externe-duree_cpu)
   endif
   heure_cpu=duree_cpu/3600
   duree_cpu=duree_cpu-3600*heure_cpu
   minute_cpu=duree_cpu/60
   seconde_cpu=duree_cpu-60*minute_cpu
   write(*,1030) jour0,mois0,annee0,heure0,minute0,seconde0,heure,minute,seconde,&
                 heure_cpu,minute_cpu,seconde_cpu,mille_cpu
   write(8,1030) jour0,mois0,annee0,heure0,minute0,seconde0,heure,minute,seconde,&
                 heure_cpu,minute_cpu,seconde_cpu,mille_cpu
   !write(*,*) 'CPU_Externe = ',cpu_externe
!------------------------------------------------------------------------
 1030 format(1X,'Date debut simulation : ',I2.2,'/',I2.2,'/',I4, &
                ' (',I2.2,':',I2.2,':',I2.2,')', &
                ' Duree : ',I2.2,':',I2.2,':',I2.2, &
                ' CPU total : ',I2.2,':',I2.2,':',I2.2,',',I3.3)
!------------------------------------------------------------------------------
end subroutine M4b



subroutine Mise_en_veille()
   logical reprise
   character :: csv*60,txt*60

   open(unit=50,file='pause',status='unknown')
   close(50,status='delete')
   inquire(unit=7,name=csv) ; close(7)
   inquire(unit=8,name=txt) ; close(8)
   do
      call sleep(3)
      inquire(file='go',exist=reprise)
      open(unit=50,file='go',status='unknown')
      close(50,status='delete')
      if (reprise) then
         open(unit=7, file=trim(csv), form='formatted', status='old',position='append')
         open(unit=8, file=trim(txt), form='formatted', status='old',position='append')
         exit
      endif
   enddo
end subroutine mise_en_veille



subroutine LireTAL(ref_sim)
! lecture d'un fichier TAL monobief
   use i_Parametres, only: rdp
   use i_Consigne, only : datageo, titre, basename, nbpro
   implicit none
   ! Prototype
   character(len=*),intent(in) :: ref_sim
   ! Variables locales
   character(len=12) :: filename
   integer :: lu=2, io_status=0, npro, nl, nlmax = 2, npt, i, lb
   character(len=80) :: ligne, ligrep(30)

   lb = len_trim(basename)
   nl = 1
   ! lecture du fichier TAL dans le dossier de référence des simulations
   open(unit=lu, file=trim(ref_sim)//'/'//basename(1:lb)//'.REP', form='formatted', status='old')
   do while (io_status == 0)
      read(lu,'(a)', iostat=io_status) ligrep(nl)
      if (ligrep(nl)(1:3) == 'TAL' .OR. ligrep(nl)(1:3) == 'tal') then
         filename = ligrep(nl)(5:len_trim(ligrep(nl)))
         ligrep(nl)(5:16) = basename(1:lb)//'.TAL'
      endif
      nl = nl+1
   enddo
   close(lu)
   write(*,'(2a)') ' Fichier TAL : ',trim(ref_sim)//'/'//filename
   write(8,'(2a)') ' Fichier TAL : ',trim(ref_sim)//'/'//filename
   open(unit=lu, file=trim(ref_sim)//'/'//filename, form='formatted', status='old')
   npro = 0 ; nl = 0
   read(lu,'(a)', iostat=io_status) ligne
   do while (io_status == 0)
      if (ligne(1:1) == '*') then
         continue
      elseif (ligne(1:1) == '#') then
         titre = ligne
         !write(*,*) 'titre : ',titre
         nl = 0
      elseif (nl == 0) then  !nouveau profil / ligne d'abscisses
         npro =  npro+1
         npt = len_trim(ligne)/6 -1
         if (ligne(1:1) == '+') then
            nlmax = 4
            if (npt < 12) stop 301
         else
            nlmax = 2
         endif
         !write(*,*) '>>>> ',ligne(1:1),nlmax
         read(ligne(2:),'(13f6.0)') datageo(npro)%x,(datageo(npro)%y(i),i=1,npt)
         nl = 1
         datageo(npro)%nbval = npt
      elseif (nl == 1) then
         npt = len_trim(ligne)/6 -1
         if (nlmax == 2) then  ! ligne de cotes
            read(ligne(2:),'(13f6.0)') datageo(npro)%zmoy,(datageo(npro)%z(i),i=1,npt)
            nl = 0
         else                  ! ligne d'abscisse (2ème)
            read(ligne(8:),'(12f6.0)') (datageo(npro)%y(i),i=datageo(npro)%nbval+1,datageo(npro)%nbval+npt)
            nl = 2
         endif
      elseif (nl == 2) then
         npt = len_trim(ligne)/6 -1
         if (nlmax == 2) then
            stop 302
         else                  ! ligne de cotes (1ère)
            read(ligne(2:),'(13f6.0)') datageo(npro)%zmoy,(datageo(npro)%z(i),i=1,npt)
         endif
         nl = 3
      elseif (nl == 3) then
         npt = len_trim(ligne)/6 -1
         if (nlmax == 2) then
            stop 303
         else                  ! ligne de cotes (2ème)
            read(ligne(8:),'(12f6.0)') (datageo(npro)%z(i),i=datageo(npro)%nbval+1,datageo(npro)%nbval+npt)
         endif
         datageo(npro)%nbval = datageo(npro)%nbval+npt
         nl = 0
         !write(*,*) 'nbval = ',datageo(npro)%nbval, npro
      endif
      read(lu,'(a)', iostat=io_status) ligne
   enddo
   close(lu)
   nbpro = npro
end subroutine LireTAL



subroutine ExportTAL(lu,datageo)
   use i_Parametres, only : rdp
   use i_Consigne, only : nbpro, titre, profil
   implicit none
   !character(len=*) :: filename
   type (profil) :: datageo(*)
   character(len=80) :: ligne
   integer :: nl, lu

   !open(newunit=lu,file=filename,status='unknown',form='formatted')
   write(lu,'(a)') titre
   do nl = 1, nbpro
      write(lu,'(a,i3,a,f8.2,i3,a)') '* Profil ',nl,' : Pk = ',datageo(nl)%x,datageo(nl)%nbval,' points'
      if (datageo(nl)%nbval > 12) then
         call ligneTAL(ligne,'+',datageo(nl)%x,datageo(nl)%y(1:12),12)
         write(lu,'(a)') ligne
         call ligneTAL(ligne,' ',99999._rdp,datageo(nl)%y(13:datageo(nl)%nbval),datageo(nl)%nbval-12)
         write(lu,'(a)') ligne
         call ligneTAL(ligne,' ',datageo(nl)%zmoy,datageo(nl)%z(1:12),12)
         write(lu,'(a)') ligne
         call ligneTAL(ligne,' ',99999._rdp,datageo(nl)%z(13:datageo(nl)%nbval),datageo(nl)%nbval-12)
         write(lu,'(a)') ligne
      else
         call ligneTAL(ligne,' ',datageo(nl)%x,datageo(nl)%y(1:datageo(nl)%nbval),datageo(nl)%nbval)
         write(lu,'(a)') ligne
         call ligneTAL(ligne,' ',datageo(nl)%zmoy,datageo(nl)%z(1:datageo(nl)%nbval),datageo(nl)%nbval)
         write(lu,'(a)') ligne
      endif
   enddo
   !close(lu)
end subroutine exportTAL



subroutine LigneTAL(ligne,a,z,y,n)
   use i_Parametres, only : rdp
   implicit none
   character(len=80) :: ligne
   character(len=1) :: a
   real(kind=rdp) :: z,y(*)
   integer :: n
   integer :: i, k

   do i = 1, 80
      ligne(i:i) = ' '
   enddo
   ligne(1:1) = a(1:1)
   k = 2
   if (z > 9999._rdp) then
      ligne(k:k+5) = '      '
   elseif (z >= 1000._rdp) then
      write(ligne(k:k+5),'(f6.1)') z
   else
      write(ligne(k:k+5),'(f6.2)') z
   endif
   do i = 1, n
      k = k+6
      if (y(i) >= 1000._rdp .OR. y(i) <= -100._rdp) then
         write(ligne(k:k+5),'(f6.1)') y(i)
      else
         write(ligne(k:k+5),'(f6.2)') y(i)
      endif
   enddo

end subroutine ligneTAL

function affiche(x,chain)
   use i_Parametres, only : rdp
!prototype
   real(kind=rdp), intent(in) :: x
   character(len=3) :: chain
   character(len=13) :: affiche
!---------------------------------------------------------
   if(x>=10000._rdp .OR. x<=-1000._rdp) then
      write(affiche,'(es10.3,a)') x,chain
   else
      write(affiche,'(f10.5,a)') x,chain
   endif
end function Affiche

subroutine Affiche_mat(A)
use i_Parametres
implicit none
integer:: i,j,n,m
real(kind=rdp) :: A(:,:)
n=size(A(:,1))
m=size(A(1,:))
do i=1,n
write(*,*) (real(A(i,j),kind=4),j=1,m)
end do
end subroutine Affiche_mat

subroutine print_Help()
!==============================================================================
!     affichage des options de la ligne de commande   
!==============================================================================
   
   write(*,*) ' '
   write(*,*) ' >>> Options de la ligne de commande de Mage_SA'
   write(*,*) ' Syntaxe : mage_sa [options] DATfile pour lancer une simulation avec DATfile ',&
               'comme fichier de paramètres numériques'
   write(*,*) '           mage_sa -v pour afficher le numéro de version'
   write(*,*) '           mage_sa -h pour afficher cette aide'
   write(*,*) ' Options disponibles :'
   write(*,*) '    -o=outDir avec outDir le sous-dossier du dossier courant où seront stockés ',&
               'les fichiers de résulats.'
   write(*,*) '         Si outDir n''existe pas il est créé.'
   write(*,*) '         Si outDir n''est pas spécifié c''est le répertoire courant' 
   write(*,*) '    -g=DATfile génère un fichier DATfile avec les paramètres numériques par défaut'
   write(*,*) '    -G=DATfile génère un fichier DATfile avec les paramètres numériques par défaut'
   write(*,*) '               et une description de ces paramètres fournie sous forme de commentaires'
   write(*,*) ' '
   write(*,*) ' >>> En cours de calcul :'
   write(*,*) ' Pour arrêter proprement les calculs : créer un fichier nommé stop dans le dossier de travail'
   write(*,*) ' Cela peut se faire avec la commande suivante exécutée dans un autre terminal : echo 0 > stop'
   write(*,*) ' Pour suspendre les calculs : créer un fichier nommé pause dans le dossier de travail'
   write(*,*) ' Pour reprendre les calculs suspendus : créer un fichier nommé go dans le dossier de travail'
   write(*,*) ' Ces fichiers sont détruits une fois lus par Mage_SA.'

end subroutine print_Help



subroutine create_DATfile(filename,blong)
!==============================================================================
!   Génération d'un fichier de paramètres avec les valeurs par défaut 
!==============================================================================
   ! prototype
   character(len=*), intent(in) :: filename
   logical, intent(in) :: blong
   ! variables locales
   integer :: lu

   open(newunit=lu,file=trim(filename), form='formatted', status='new')
   write(lu,'(a)') '* ### Fichier généré automatiquement par la commande mage_sa -g ###'
   write(lu,'(a)') '*'
   write(lu,'(a)') '* NB : toute ligne commençant pas une * est un commentaire'
   write(lu,'(a)') '*'
   if (blong) write(lu,'(a)') '* Nom de base du fichier REP des simulations de MAGE'
   write(lu,'(a)') 'basename =<à compléter>'
   if (blong) write(lu,'(a)') '* Nom du dossier de référence pour récupérer la simulation MAGE'
   write(lu,'(a)') 'dossier_simulation =ref_simulation'
   if (blong) write(lu,'(a)') '* Nom du dossier de référence pour récupérer la simulation de calage'
   write(lu,'(a)') 'dossier_calage =ref_calage'
   if (blong) write(lu,'(a)') '* Chemin complet du solveur MAGE à utiliser'
   write(lu,'(a)') 'solveur =~/bin/mage_install/7.y/mage'
   if (blong) write(lu,'(a)') '* Température initiale pour l''algorithme du Recuit-Simulé'
   write(lu,'(a)') 'temperature_initiale = 2.0'
   if (blong) write(lu,'(a)') '* Taux de décroissance de la température de l''algorithme du Recuit-Simulé'
   write(lu,'(a)') 'taux_decroissance_temperature = 0.005'
   write(lu,'(a)') 'nb_idem = 1'
   if (blong) write(lu,'(a)') '* Échelle : facteur appliqué au rayon de variation admissible pour chaque consigne'
   if (blong) write(lu,'(a)') '*           Par défaut ce rayon est Wmax - Wmin'
   write(lu,'(a)') 'echelle = 1.'
   if (blong) write(lu,'(a)') '* Pénalisation : coefficient de pénalisation des contraintes ; valeur par défaut : 10^6', &
   & ' si 0 on ne tient pas compte des contraintes' 
   write(lu,'(a)') '*penalisation = 1000.'
   if (blong) write(lu,'(a)') '* seuil_amelioration = amélioration minimale pour retenir une nouvelle estimation '
   if (blong) write(lu,'(a)') '*                      dans l''algorithme du Recuit-Simulé ; valeur par défaut : 0'
   if (blong) write(lu,'(a)') 'seuil_amelioration = 0.0'
   if (blong) write(lu,'(a)') '* nech : le nombre d echantillons consideré pour amorcer les methodes :'
   if (blong) write(lu,'(a)') '* krigeage sur solutions calees, krigeage sur le calage et la surface'
   if (blong) write(lu,'(a)') ' * et, entropie croisee sur solutions calées'
   write(lu,'(a)') 'nech=200'
   if (blong) write(lu,'(a)') '* namel : le nombre d iterations adaptatives pour le krigeage avec la methode Expected Improvement'
   if (blong) write(lu,'(a)') '* le nombre de répétitions pour l entropie croisée'
   write(lu,'(a)') 'namel=25'
   if (blong) write(lu,'(a)') '* nelite : le nombre de la population d elite pour l entropie croisee (valeur recommandée 5% nech)'
   if (blong) write(lu,'(a)') '* le nombre de solutions candidates potentielles à tester après un krigeage'
   write(lu,'(a)') 'nelite=25'
   if (blong) write(lu,'(a)') '* n_it_max : le budget de simulations accordé pour tester l incertitude'
   if (blong) write(lu,'(a)') '* methodes recommandées selon le nombre de simulations (dépend des contraintes de calage)'
   if (blong) write(lu,'(a)') '*1-500 : krigeage solutions calées adaptatif avec Expected Improvement'
   if (blong) write(lu,'(a)') '*500-3000 : Entropie Croisée, krigeage calage et surface inondee'
   if (blong) write(lu,'(a)') '*3000-15000 : méthode hybride : krigeage cal+surf et recuit simulé initialisé par le résultat'
   if (blong) write(lu,'(a)') '*15000-30000 : methode hybride ou recuit simple'
   if (blong) write(lu,'(a)') '*+ de 30000 : recuit simulé et éventuellment monte-carlo si peu de paramètres'
   write(lu,'(a)') 'n_it_max=15000'
   if (blong) write(lu,'(a)') '* nacp :  pour le recuit simulé nombre de simulations calées avant de passer en mode ACP' &
   & ,', blocage aux bords ou linéaire'
   write(lu,'(a)') 'nacp=2000'
   if (blong) write(lu,'(a)') '* n_acp_fin : nombre de simulations pour finir mode acp blocage aux bords ou linéarités' 
   write(lu,'(a)') 'n_acp_fin=4000'
   if (blong) write(lu,'(a)') '* iflin : booléen gérant l exploitation des linéarité dans la fonction voisinage recuit simulé'
   write(lu,'(a)') 'iflin=0'
   if (blong) write(lu,'(a)') '* ifinsert : booléen recuit simulé blocage des solutions aux bords'
   write(lu,'(a)') 'ifinsert=0'
   if (blong) write(lu,'(a)') '* ifacp : booléen pour appliquer acp dans le recuit simulé'
   write(lu,'(a)') 'ifacp=1'
   if (blong) write(lu,'(a)') '* ifhybride : booléen pour appliquer le recuit simulé apres une methode'
   write(lu,'(a)') 'ifhybride=1'
   if (blong) write(lu,'(a)') '*'
   if (blong) write(lu,'(a)') '* Fonction de cout (1/0)  : maximisation de l''écart surface Max - surface Min ou des hauteurs max'
   write(lu,'(a)') 'surface_inondee = 1'
   if (blong) write(lu,'(a)') '*      surface_EcartMax -> méthode du recuit simulé'
   if (blong) write(lu,'(a)') '*      Monte-Carlo      -> génération pseudo-aléatoire de nbok simulation satisfaisant '
   if (blong) write(lu,'(a)') '*                          les contraintes et le calage'
   if (blong) write(lu,'(a)') '*      LHSMontecarlo-> génération pseudo-aléatoire de nbok simulation satisfaisant '
   if (blong) write(lu,'(a)') '*      krigeage -> krigeage sur simulations calées avec méthode Expected Improvement'
   if (blong) write(lu,'(a)') '*      krigeage_EI -> krigeage sur le calage et la surface inondee avec R '
   if (blong) write(lu,'(a)') '*      Entropiecroisée -> méthode du recuit simulé'
   if (blong) write(lu,'(a)') '*      GA_pikaia -> algorithme génétique pikaia '
   write(lu,'(a)') '*FOtype = surface_EcartMax'
   write(lu,'(a)')   '*FOtype = GA_pikaia'
  write(lu,'(a)') '*FOtype = Monte-Carlo'
  write(lu,'(a)') '*FOtype = krigeage'
  write(lu,'(a)') '*FOtype = krigeage_EI'
  write(lu,'(a)') '*FOtype = LHSMonte-Carlo'
  write(lu,'(a)') '*FOtype = Entropiecroisee'
   if (blong) write(lu,'(a)') '* nbok : Nombre de simulations pour Monte-Carlo'
   write(lu,'(a)') 'nbok = 1000'
   write(lu,'(a)') '*'
   if (blong) write(lu,'(a)') '* Géométrie : rayon de variation admissible de la cote du fond ; valeur par défaut : 0'
   write(lu,'(a)') 'geometrie =0.'
   write(lu,'(a)') '*'
   if (blong) write(lu,'(a)') '* Strickler : début de la liste des contraintes sur les stricklers, définies tronçon par tronçon'
   write(lu,'(a)') '<strickler>'
   write(lu,'(a)') '*  ib x_debut x_fin K1_min K1_max K2_min K2_max K1_1 K2_1 K1_2 K2_2'
   if (blong) write(lu,'(a)') '*  n° bief, Pk début tronçon Pk fin du tronçon, Strickler min et max du lit mineur'
   if (blong) write(lu,'(a)') '*                                               Strickler min et max du lit majeur'
   if (blong) write(lu,'(a)') '*  Strickler mineur et majeur de départ pour la consigne n°1'
   if (blong) write(lu,'(a)') '*  Strickler mineur et majeur de départ pour la consigne n°2'
   if (blong) write(lu,'(a)') '* exemple (Saar - 6 tronçons)'
   if (blong) write(lu,'(a)') '  1  500.0   1700.0      22. 28. 12. 18. 25. 15. 25. 15.'
   write(lu,'(a)') '  1 1700.0   2900.0      22. 28. 12. 18. 25. 15. 25. 15.'
   write(lu,'(a)') '  1 2900.0   4100.0      22. 28. 12. 18. 25. 15. 25. 15.'
   write(lu,'(a)') '  1 4100.0   5300.0      22. 28. 12. 18. 25. 15. 25. 15.'
   write(lu,'(a)') '  1 5300.0   6500.0      22. 28. 12. 18. 25. 15. 25. 15.'
   write(lu,'(a)') '  1 6500.0   7600.0      22. 28. 12. 18. 25. 15. 25. 15.'
   if (blong) write(lu,'(a)') '* Strickler : fin de la liste'
   write(lu,'(a)') '</strickler>'   
   write(lu,'(a)') '*'
   if (blong) write(lu,'(a)') '* Erreur calage : valeur maximale admissible'
   write(lu,'(a)') 'erreur_calage = 0.1'
   if (blong) write(lu,'(a)') '* coeff_calage_Z : poids de l''erreur en cote'
   write(lu,'(a)') 'coeff_calage_Z = 1.'
   if (blong) write(lu,'(a)') '* coeff_calage_Q : poids de l''erreur en débit'
   write(lu,'(a)') 'coeff_calage_Q = 0.'
   if (blong) write(lu,'(a)') '* Données de calage en Z : début de liste'
   write(lu,'(a)') '<calage_z>'
   write(lu,'(a)') '* n°_bief  Pk  Z_observé'
   if (blong) write(lu,'(a)') '* Exemple (Saar - 3 laisses de crue)'
   if (blong) write(lu,'(a)') '   1 7600.0 139.52' 
   if (blong) write(lu,'(a)') '   1 4100.0 137.18 '
   if (blong) write(lu,'(a)') '   1 2300.0 135.39 '
   if (blong) write(lu,'(a)') '* Données de calage en Z : fin de liste'
   write(lu,'(a)') '</calage_z>'
   write(lu,'(a)') '*'
   if (blong) write(lu,'(a)') '* optionnel : Données de calage en débit : début de liste'
   write(lu,'(a)') '<calage_q>'
   if (blong) write(lu,'(a)') '* n°_bief  Pk  Z_observé'
   if (blong) write(lu,'(a)') '* Données de calage en débit : fin de liste'
   write(lu,'(a)') '</calage_q>'
end subroutine create_DATfile


subroutine print_Version(lu)
!==============================================================================
!   Écriture des informations de version sur l'unité logique lu
!==============================================================================
   ! prototype
   integer, intent(in) :: lu

   write(lu,'(a)') 'Programme Mage_SA (Analyse de Sensibilité pour MAGE)'
   write(lu,'(a)') 'Version '//version_string//' pour GNU/Linux'
   write(lu,'(a)') 'Auteur : Jean-Baptiste FAURE - Irstea - 2005-2014'
   write(lu,'(a)') 'Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200'
   write(lu,'(a)') ' '
   write(lu,'(a)')  trim(garantie1)
   write(lu,'(a)')  trim(garantie2)
   write(lu,'(a)') ' '

end subroutine print_Version
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
