!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - INRAE - 1989-2020                           #
!# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################


! TODO: reprogrammer le stockage des données d'apports latéraux ; remplacer le fichier à accés direct etude_qla par un tableau
! TODO: Reprogrammer sous forme modulaire la discrétisation : Boussinesq, Froude, Manning-Strickler
! TODO: Documenter l'utilisation de PARAM pour l'initialisation en permament
! TODO: relecture HYD en cours de simulation (cf relecture NUM et VAR)
! TODO: relecture LAT en cours de simulation (cf relecture NUM et VAR)



! NOTE: Unités Logiques utilisées par MAGE :
!        Fichiers de sortie TRA, BIN et ERR      : 1, 2 et 3
!        Fichiers internes et accés direct       : 91
!        Fichier copie sortie écran              : 9
!
!##############################################################################
program Mage
!==============================================================================
!                              Programme principal
!     Appelle
!     - le module Inimage() de lecture des fichiers de données et d'initialisation
!       dont le calcul en permanent
!     - le module Analyse_Geo() d'analyse de la géométrie
!     - le module Exemage() de réalisation des itérations temporelles
!
!==============================================================================
   use Parametres, only: long, zero, un, version_number, Authors, depotAPP, &
                         garantie1, garantie2, i_debord, lname, &
                         toutpetit, toutpetit_default, Signal_interruption, slash, &
                         precision_alti, precision_alti_default
   use, intrinsic :: iso_fortran_env, only: output_unit, input_unit, error_unit
   use iso_c_binding, only: c_null_char

   use Mage_Utilitaires, only: capitalize
   use Data_Num_Fixes, only: encodage, fp_model, nb_cpu, silent, long_BIN, dtmelissa, with_melissa
   use objet_section, only: verbose, is_LC, LC_type
   use TopoGeometrie, only: is_ISM, with_charriage, export_ST_final, la_topo
   use StVenant_ISM, only: V_interface, V_interface_upstream, V_interface_weighted, &
                           weight_Vint, V_interface_downstream, V_interface_biggest, V_interface_yen
   use Tests_Unitaires, only: alltests
   use Licence, only: print_license
   use Mage_Results, only: save_Mage_Results, type_resultat
   use charriage, only: dump_contraintes_locales
#ifdef with_timers
   use mage_timers
#endif /* with_timers */
   implicit none

#ifdef mpi
   include "mpif.h"
#ifdef melissa
   include "melissa_api.f90" ! melissa requires MPI
#endif /* melissa */
#endif /* mpi */

! -- Variables locales
   character(len=lname) :: REPname, argmnt
   logical :: linked
   integer :: iarg, xbackup, nzero, ios
   character(len=3) :: tmp
   character(len=180) :: cmdline
   include 'buildID.fi'
   external ask_stop
   character(len=1) :: answer, wine
   integer :: err_code, check_fp
#ifdef mpi
   integer :: comm, me, statinfo, np
   integer(kind=MPI_ADDRESS_KIND) :: appnum
#endif /* mpi */

#ifdef with_timers
   real(kind=long) :: start_t, end_t, start_t2, end_t2
#endif /* with_timers */

#ifdef with_timers
   call cpu_time(start_t)
   call cpu_time(start_t2)
#endif /* with_timers */
#ifdef mpi
   call mpi_init(statinfo)
#endif /* mpi */

!---Routine de lecture et vérification des données
   ! Valeurs par défaut
   silent = .false.
   long_BIN = .true.
   encodage = 'UTF-8'
   linked = .false.
   fp_model = i_debord
   nb_cpu = 1
   weight_Vint = -99._long
   xbackup = -1 !si ≥ 0 archivage des données et résultats texte en fin de simulation
   toutpetit = toutpetit_default
   is_LC = .false.
   LC_type = 1
   with_charriage = 0
   check_fp = 0
   precision_alti = precision_alti_default

!---Définition du séparateur de fichier en fonction de l'OS
#ifdef linux
   slash = '/'
#endif
#ifdef windows
   slash = '\'
#endif
#ifndef linux
#ifndef windows
   stop 'OS inconnu'
#endif
#endif
   !Si l'OS est MS-Windows et si la variable d'environnement mage_wine est définie et vaut 1
   !alors on change slash pour '/' de façon à pouvoir utiliser des données Linux.
   call get_environment_variable('MAGE_WINE',wine)
#ifdef windows
   if (wine == '1') slash = '/'
#endif

   ! on essaye d'abord avec la ligne de commande
   call get_command(cmdline)
   with_melissa = .false.
   dtmelissa = zero

   iarg = 1 ; call get_command_argument(iarg,argmnt)
   if (len_trim(argmnt) > 0) then  !il y a des arguments sur la ligne de commande
      if (argmnt(1:2) == '-v') then
         silent = .true.
#ifdef linux
         write(output_unit,'(3a)') 'MAGE ',version_number(1:len_trim(version_number)),' pour Linux'
#endif
#ifdef windows
         write(output_unit,'(3a)') 'MAGE ',version_number(1:len_trim(version_number)),' pour Windows'
#endif
         write(output_unit,'(2a)') 'Dernier correctif : ',trim(commitID)
         write(output_unit,'(a)')  trim(dateCommit)
         write(output_unit,'(2a)') 'Date de compilation : ',trim(buildID)
         write(output_unit,'(2a)') 'Auteur : ',trim(Authors)
         write(output_unit,'(2a)') 'Dépôt APP : ',trim(depotAPP)
         call print_license(output_unit)
         stop
      else if (argmnt(1:2) == '-h') then
         call help(6) ; stop
      else if (argmnt(1:2) == '-t') then
         call alltests ; stop
      else if (argmnt(1:2) == '--') then
         write(output_unit,*) '>>> Erreur : argument de la ligne de commande invalide'
         call help(6) ; stop
      ! les arguments suivants peuvent être dans n'importe quel ordre
      else
         do
            if (argmnt(1:3) == '-s ') then
               silent = .true.
               iarg = iarg+1 ; call get_command_argument(iarg,argmnt)
            else if (argmnt(1:3) == '-r ') then
               long_BIN = .false.
               iarg = iarg+1 ; call get_command_argument(iarg,argmnt)
            else if (argmnt(1:3) == '-D=') then
               ! NOTE: chdir() est une extension GNU de gfortran
               call chdir(trim(argmnt(4:)))
               iarg = iarg+1 ; call get_command_argument(iarg,argmnt)
            else if (argmnt(1:3) == '-i=') then
               read(argmnt(4:),*) fp_model
               is_ISM = fp_model /= i_debord
               iarg = iarg+1 ; call get_command_argument(iarg,argmnt)
            else if (argmnt(1:3) == '-w=') then
               read(argmnt(4:),*) weight_Vint
               if (weight_Vint < zero .or. weight_Vint > un) then
                  write(output_unit,*) '>>>> option -w erronée : coefficient en dehors de la plage [0 , 1]'
                  stop 'Arrêt pour option erronée'
               endif
               V_interface => V_interface_weighted
               iarg = iarg+1 ; call get_command_argument(iarg,argmnt)
            else if (argmnt(1:3) == '-u=') then
               read(argmnt(4:),*) weight_Vint
               if (weight_Vint < -un .or. weight_Vint > un) then
                  write(output_unit,*) '>>>> option -u erronée : coefficient en dehors de la plage [-1,+1]'
                  stop 'Arrêt pour option erronée'
               endif
               V_interface => V_interface_upstream
               iarg = iarg+1 ; call get_command_argument(iarg,argmnt)
            else if (argmnt(1:3) == '-d=') then
               read(argmnt(4:),*) weight_Vint
               if (weight_Vint < -un .or. weight_Vint > un) then
                  write(output_unit,*) '>>>> option -d erronée : coefficient en dehors de la plage [-1,+1]'
                  stop
               endif
               V_interface => V_interface_downstream
               iarg = iarg+1 ; call get_command_argument(iarg,argmnt)
            else if (argmnt(1:3) == '-b=') then
               read(argmnt(4:),*) weight_Vint
               if (weight_Vint < -un .or. weight_Vint > un) then
                  write(output_unit,*) '>>>> option -b erronée : coefficient en dehors de la plage [-1,+1]'
                  stop 'Arrêt pour option erronée'
               endif
               V_interface => V_interface_biggest
               iarg = iarg+1 ; call get_command_argument(iarg,argmnt)
            else if (argmnt(1:3) == '-y ') then
               V_interface => V_interface_yen
               weight_Vint = zero
               iarg = iarg+1 ; call get_command_argument(iarg,argmnt)
            else if (argmnt(1:5) == '-cpu=') then
               read(argmnt(6:),*) nb_cpu
               iarg = iarg+1 ; call get_command_argument(iarg,argmnt)
            else if (argmnt(1:5) == '-eps=') then
               read(argmnt(6:),*) nzero
               toutpetit = 10._long**(-nzero)
               iarg = iarg+1 ; call get_command_argument(iarg,argmnt)
            else if (argmnt(1:4) == '-pa=') then
               read(argmnt(5:),*) precision_alti
               iarg = iarg+1 ; call get_command_argument(iarg,argmnt)
            else if (argmnt(1:3) == '-l ') then
               linked = .true.
               if (len_trim(argmnt)>2 .and. argmnt(3:3)=='=') then
                  tmp = trim(argmnt(4:6))
                  call capitalize(tmp,type_resultat)
               else
                  type_resultat='ENV'
               endif
               iarg = iarg+1 ; call get_command_argument(iarg,argmnt)
            else if (argmnt(1:3) == '-z ') then
               xbackup = 3
               iarg = iarg+1 ; call get_command_argument(iarg,argmnt)
            else if (argmnt(1:3) == '-z=') then
               read(argmnt(4:4),'(i1)',iostat=ios) xbackup
               if (ios /= 0) xbackup = -1
               iarg = iarg+1 ; call get_command_argument(iarg,argmnt)
            else if (argmnt(1:4) == '-LC=') then
               !si renseigné alors on utilise les variantes Largeur-Cote des routines largeur(), perimetre() et section()
               is_LC = .true.
               read(argmnt(5:5),'(i1)',iostat=ios) LC_type
               if (ios /= 0 .or. LC_type > 2 .or. LC_type <0) then
                   write(error_unit,'(a)') ' >>>> Erreur : les valeurs possibles pour l''option -LC sont 0, 1 ou 2'
                  stop 'Arrêt pour option erronée'
               endif

               iarg = iarg+1 ; call get_command_argument(iarg,argmnt)
            else if (argmnt(1:3) == '-c=') then
               read(argmnt(4:),*) with_charriage
               if (with_charriage /= 0 .and. with_charriage /= 1 .and. &
                   with_charriage /= 2 .and. with_charriage /= 3) then
                  write(error_unit,'(a)') ' >>>> Erreur : les valeurs possibles pour l''option -c sont 0, 1, 2 ou 3'
                  stop 'Arrêt pour option erronée'
               endif
              iarg = iarg+1 ; call get_command_argument(iarg,argmnt)
            else if (argmnt(1:4) == '-fp=') then
               read(argmnt(5:),*) check_fp
               if (check_fp < 0 .or. check_fp > 3) then
                  write(error_unit,*) '>>> Erreur : les valeurs possibles pour l''option -fp sont 0, 1, 2 ou 3'
                  stop 'Arrêt pour option erronée'
               endif
              iarg = iarg+1 ; call get_command_argument(iarg,argmnt)
            else if (argmnt(1:1) == '-') then
               write(output_unit,'(a)') ' >>>> Erreur : argument de la ligne de commande inconnu : ',trim(argmnt),' est ignoré'
               write(output_unit,'(a)') '      Pour continuer, taper C ou c, autre chose pour arrêter'
               read(input_unit,*) answer
               if (answer(1:1) == 'C' .or. answer(1:1) == 'c') then
                  iarg = iarg+1 ; call get_command_argument(iarg,argmnt)
               else
                  stop 'Arrêt pour option erronée'
               endif
            else
               REPname = trim(argmnt)
               exit
            endif
         enddo
      endif

   else
      write(output_unit,'(a)') 'Nom du fichier REPertoire'
      read(input_unit,'(a)') REPname
      if (len_trim(REPname) == 0) then
         write(output_unit,'(a)') ' >>>> Erreur : il manque le nom du fichier REP'
         write(output_unit,'(a)') '      tapez Entrée pour continuer'
         read(input_unit,*)
         call help(output_unit) ; stop
      endif

   endif
   verbose = .not.silent

#ifdef fake_mage7
   check_fp = 1
   is_LC = .true.
   LC_type = 0
   with_charriage = 0
#endif /* fake_mage7 */

   !Vérification qu'on a bien une formule pour la vitesse à l'interface si ISM
   if (fp_model /= i_debord .and. weight_Vint < -2._long) then
      write(output_unit,'(a)') ' >>>> Erreur : il manque la formule pour la vitesse à l''interface pour ISM'
      write(output_unit,'(a)') '               vous avez le choix entre les options w, u, d, b et y'
      write(output_unit,'(a)') '               tapez Entrée pour continuer'
      read(input_unit,*)
      call help(output_unit) ; stop
   endif
!
!---Routines d'initialisation et vérification des données
   call IniMage(REPname,cmdline)
   call Analyse_Geo(check_fp)
#ifdef with_timers
   call cpu_time(end_t)
   init_t = end_t - start_t
#endif /* with_timers */

#ifdef mpi
!---Routine d'initialisation de Melissa
  ! The new MPI communicator is build by splitting MPI_COMM_WORLD by simulation inside the group.
  ! In the case of a single simulation group, this is equivalent to MPI_Comm_dup.
   call mpi_comm_rank(MPI_COMM_WORLD, me, statinfo)
   call MPI_Comm_get_attr(MPI_COMM_WORLD, MPI_APPNUM, appnum, statinfo);
   call MPI_Comm_split(MPI_COMM_WORLD, appnum, me, comm, statinfo);
   call mpi_comm_rank(comm, me, statinfo)
   call mpi_comm_size(comm, np, statinfo)
#ifdef melissa
   if (with_melissa) then
      if (long_BIN) then
         call melissa_init("Q"//c_null_char, la_topo%net%ns, comm)
         call melissa_init("Z"//c_null_char, la_topo%net%ns, comm)
         call melissa_init("S"//c_null_char, la_topo%net%ns, comm)
         call melissa_init("L"//c_null_char, la_topo%net%ns, comm)
         call melissa_init("M"//c_null_char, la_topo%net%ns, comm)
         call melissa_init("R"//c_null_char, la_topo%net%ns, comm)
         call melissa_init("U"//c_null_char, la_topo%net%ns, comm)
         call melissa_init("V"//c_null_char, la_topo%net%ns, comm)
         call melissa_init("W"//c_null_char, la_topo%net%ns, comm)
         call melissa_init("G"//c_null_char, la_topo%net%ns, comm)
         call melissa_init("D"//c_null_char, la_topo%net%ns, comm)
      else
         write(output_unit,'(a)') "melissa_init"
         call melissa_init("Q"//c_null_char, la_topo%net%ns, comm)
         call melissa_init("Z"//c_null_char, la_topo%net%ns, comm)
      endif
   endif
#endif /* melissa */
#endif /* mpi */
!
!---Routine d'execution des calculs (solveur)
   ! NOTE: extension Gfortran : interception du signal SIGINT (Ctrl+C)
#ifdef linux
   call signal(Signal_interruption,ask_stop)
#endif
#ifdef with_timers
   call cpu_time(start_t)
#endif /* with_timers */
   call ExeMage(err_code)
#ifdef with_timers
   call cpu_time(end_t)
   iter_t = end_t - init_t
#endif /* with_timers */

   if (linked) call save_Mage_Results()

   if (with_charriage > 0) then
      call export_ST_final('_final')
      call dump_contraintes_locales('contraintes_locales.txt')
   endif
#ifdef mpi
#ifdef melissa
   if (with_melissa) then
      call melissa_finalize()
   endif
#endif /* melissa */
#endif /* mpi */

! Fermeture des fichiers
   call close_All

! archivage des données et résultats texte de la simulation
! NOTE: il faut archiver après la simulation pour être sûr d'avoir des résultats correspondant aux données
   if (xbackup >= 0) call backup_data(xbackup)

   if (err_code /= 0) stop err_code
#ifdef mpi
   call mpi_finalize()
#endif /* mpi */

#ifdef with_timers
   print*, "init_t = ", init_t
   print*, "iter_t = ", iter_t
   call cpu_time(end_t2)
   total_t = end_t2-start_t2
   print*, "total cpu time = ", total_t
   print*, "regloc_t = ", regloc_t
   print*, "calcul_dt_t = ", calcul_dt_t
   print*, "cl_amont_t = ", cl_amont_t
   print*, "apport_t = ", apport_t
   print*, "update_positions_ouvrages_t = ", update_positions_ouvrages_t
   print*, "Debord_discretisation_t = ", Debord_discretisation_t
   print*, "init_CLaval_Debord_t = ", init_CLaval_Debord_t
   print*, "init_solution_Debord_t = ", init_solution_Debord_t
   print*, "nonLinear_iterations_t = ", nonLinear_iterations_t
   print*, "timestep_last_actions_t = ", timestep_last_actions_t

#endif /* with_timers */

end program Mage



subroutine Help(lu)
!==============================================================================
!     affichage des options de la ligne de commande
!==============================================================================
! -- Prototype --
   integer,intent(in) :: lu

   write(lu,*) ' Options de la ligne de commande de MAGE'
   write(lu,*) ' Syntaxe : mage [options] REPname pour lancer une simulation avec REPname comme fichier REP'
   write(lu,*) '           mage -v pour afficher le numéro de version et quitter'
   write(lu,*) '           mage -h pour afficher cette aide et quitter'
   write(lu,*) '           mage -t pour exécuter les tests unitaires et quitter'
   write(lu,*) ''
   write(lu,*) ' Options disponibles :'
   write(lu,*) '    -D=workDir avec workDir le chemin où se trouve REPname'
   write(lu,*) '         si workDir n''est pas spécifié c''est le répertoire courant'
   write(lu,*) ''
   write(lu,*) '    -l=type_resultat pour utiliser MAGE comme solveur externe à partir d''un autre code'
   write(lu,*) '         enregistre une sélection des résultats dans un fichier binaire nommé Mage_Results'
   write(lu,*) '         valeurs possibles pour type_resultat : ENV, FIN|END'
   write(lu,*) '         valeur par défaut : ENV'
   write(lu,*) ''
   write(lu,*) '    -r pour utiliser le mode fichier BIN réduit'
   write(lu,*) '         utile pour les longues simulations destinées à Adis-TS'
   write(lu,*) ''
   write(lu,*) '    -s pour utiliser le mode silencieux : pas d''affichage à l''écran'
   write(lu,*) ''
   write(lu,*) '    -cpu=n avec n le nombre de cpu à utiliser en mode parallèle ; valeur par défaut : 1'
   write(lu,*) ''
   write(lu,*) '    -i=[1|0|-1|-2] pour choisir entre ISM et Debord :'
   write(lu,*) '         1 = ISM'
   write(lu,*) '         0 = Debord (valeur par défaut)'
   write(lu,*) '        -1 = on corrige Debord par ISM si ISM réussit, sinon on garde Debord'
   write(lu,*) '        -2 = on bascule sur Debord si ISM échoue'
   write(lu,*) ''
   write(lu,*) '    -LC=n pour utiliser en interne une représentation en largeurs-cotes, comme dans Mage-7'
   write(lu,*) '          par défaut Mage-8 utilise la représentation XYZ des fichiers de géométrie'
   write(lu,*) '        n=0 : même calcul de représentation largeurs-cotes que dans Mage-7'
   write(lu,*) '        n=1 : calcul en utilisant largeur_XYZ(), perimetre_XYZ() et section_XYZ() sur le profil 3D'
   write(lu,*) '        n=2 : calcul en utilisant largeur_XYZ(), perimetre_XYZ() et section_XYZ() sur la projection'
   write(lu,*) '              abscisses-cotes du profil original (dans un monde parfait 2 == 0)'
   write(lu,*) ''
   write(lu,*) '     Mode de calcul de la vitesse à l''interface pour ISM :'
   write(lu,*) '    -w=x pour que ISM utilise V_interface_weighted avec la pondération x ; 0 ≤ x ≤ +1'
   write(lu,*) '         V_int = x*V_mc + (1-x)*V_fp '
   write(lu,*) '    -u=x pour que ISM utilise V_interface_upstream avec la correction x ; -1 ≤ x ≤ +1'
   write(lu,*) '         V_int = (1+x)*V_upstream '
   write(lu,*) '    -d=x pour que ISM utilise V_interface_downstream avec la correction x ; -1 ≤ x ≤ +1'
   write(lu,*) '         V_int = (1+x)*V_downstream '
   write(lu,*) '    -b=x pour que ISM utilise V_interface_biggest avec la correction x ; -1 ≤ x ≤ +1'
   write(lu,*) '         V_int = (1+x)*max(V_mc,V_fp) '
   write(lu,*) '    -y   pour que ISM utilise V_interface_yen'
   write(lu,*) '         V_int = (L_mc*V_fp + L_fp*V_mc) / (L_mc+L_fp) '
   write(lu,*) '     les options -w, -u, -d, -b et -y sont mutuellement exclusives'
   write(lu,*) '     si plusieurs de ces options sont utilisées, seule la dernière est appliquée'
   write(lu,*) ''
   write(lu,*) '    -z[=n] où n est un nombre de 0 à 9, valeur par défaut : 3. Indique à Mage de créer une '
   write(lu,*) '           archive de la simulation au format tar.xz sous Linux et 7z sous MS-Windows'
   write(lu,*) '           avec le taux de compression n. '
   write(lu,*) '           Dépendances : xz sous Linux et 7-zip sous MS-Windows.'
   write(lu,*) ''
   write(lu,*) '    -c=n où n a pour valeur 0, 1 ou 2 pour définir la prise en compte du charriage :'
   write(lu,*) '           n = 0 -> pas de charriage, les données sédimentaires sont ignorées'
   write(lu,*) '           n = 1 -> avec charriage, les données sédimentaires sont lues point par point'
   write(lu,*) '                    dans le fichier ST'
   write(lu,*) '           n = 2 -> avec charriage, les données sédimentaires sont lues dans un fichier SED'
   write(lu,*) '                    définies par tronçons. Chaque couche sédimentaire est définie alors par'
   write(lu,*) '                    son épaisseur et non par la cote de son plancher'
   write(lu,*) '           n = 3 -> avec charriage, les données sédimentaires sont lues section par section'
   write(lu,*) '                    dans le fichier ST'
   write(lu,*) ''
   write(lu,*) '    -fp=n où n a pour valeur 0 (défaut), 1, 2 ou 3 pour définir comment il faut traiter'
   write(lu,*) '          les profils dont un lit majeur a des points situés plus bas que la cote de débordement.'
   write(lu,*) '          Pour le lit majeur gauche [droit] ce sont des points situés à gauche [droite] de RG [RD]'
   write(lu,*) '          (dans le sens de l''écoulement) dont la cote est inférieure à celle du point RG [RD]'
   write(lu,*) '          Si des profils avec des points trop bas en lit majeur sont détectés :'
   write(lu,*) '          n = 0 -> la simulation est arrêtée avec un message d''erreur fatale (défaut)'
   write(lu,*) '          n = 1 -> on passe outre, la simulation continue'
   write(lu,*) '          n = 2 -> on corrige brutalement les points fautifs en ramenant leur cote à celle'
   write(lu,*) '                   du point RG ou RD correspondant'
   write(lu,*) '          n = 3 -> on corrige les points dont la cote est trop basse d''au plus 10 cm. S''il reste'
   write(lu,*) '                   des points trop bas, la simulation est arrêtée, sinon elle continue.'
   write(lu,*) '          Dans tous les cas, la liste des profils fautifs est donnée sur OUTPUT.'
   write(lu,*) ''
   write(lu,*) '    -eps=n où n définit la précision du test d''arrêt prise égale à 10^-n'
   write(lu,*) '           la valeur par défaut est 9'
   write(lu,*) ''
   write(lu,*) ' Interruption ou suspension de la simulation (Linux uniquement) : '
   write(lu,*) '    tapez ctrl+C et choisissez parmi les options proposées :'
   write(lu,*) '          Continuer (c)'
   write(lu,*) '          Arrêter tout de suite (s)'
   write(lu,*) '          Arrêter à la prochaine minute|heure|jour (m|h|j)'


end subroutine Help



subroutine Version(LU)
!==============================================================================
!          Version du programme : impression à l'écran et sur listing
!
!==============================================================================
   use Parametres, only: Long,NOSup,NNSup,Version_Number, &
                         NEsup,sp,DepotAPP,Authors
   use Licence, only: print_license
   use Data_Num_Fixes, only: long_BIN
   use, intrinsic :: iso_fortran_env, only: output_unit, compiler_version
   implicit none
! -- Prototype --
   integer,intent(in) :: LU

   include 'buildID.fi'
   integer :: i
   character (len=43) :: M(10)
              M(1) ='MM      MM     AA     GGGGGGGGGG EEEEEEEEEE'
              M(2) ='MMM    MMM    AAAA    GGGGGGGGGG EEEEEEEEEE'
              M(3) ='MMMM  MMMM   AA  AA   GG         EE        '
              M(4) ='MM MMMM MM  AA    AA  GG         EE        '
              M(5) ='MM  MM  MM AAAAAAAAAA GG  GGGGGG EEEEEE    '
              M(6) ='MM      MM AAAAAAAAAA GG  GGGGGG EEEEEE    '
              M(7) ='MM      MM AA      AA GG      GG EE        '
              M(8) ='MM      MM AA      AA GG      GG EE        '
              M(9) ='MM      MM AA      AA GGGGGGGGGG EEEEEEEEEE'
              M(10)='MM      MM AA      AA GGGGGGGGGG EEEEEEEEEE'

!------------------------------------------------------------------------------
   write(lu,998)
   write(lu,999)
   write(lu,'(//)')
   do i=1,10
      write(lu,'(20x,a)') m(i)
   enddo
   write(lu,*) ' '
   write(lu,1000) trim(Authors),trim(DepotAPP),Version_Number(1:len_trim(version_number)), &
                  trim(commitID),trim(dateCommit),trim(buildID),compiler_version()
   call print_license(lu)
   write(lu,*) ' '
!    write(lu,1001) ibsup,issup
   write(lu,1002) nnsup,nesup
   if (long_BIN) then
      write(lu,'(27x,a,2(i0,a))') 'Précision des réels : ',precision(1._long),' chiffres (',long,' octets) - Fichier BIN complet'
   else
      write(lu,'(27x,a,2(i0,a))') 'Précision des réels : ',precision(1._long),' chiffres - Fichier BIN court'
   endif
#ifdef openmp
   write(lu,'(27x,a)') 'Parallélisation OpenMP active'
#endif /* openmp */
#ifndef openmp
   write(lu,'(27x,a)') 'Parallélisation OpenMP inactive'
#endif /* openmp */
   if (lu/=output_unit) write(lu,'(///)')
!------------------------------------------------------------------------------
   998 format(2x,'INRAE')
   999 format(2x,'Centre Auvergne-Rhône-Alpes Est',19x,'5 rue de La Doua - CS 20244',/, &
             52x,'69625 Villeurbanne - FRANCE')
   1000 format(28x,'Exécution du programme MAGE', &
             /,20x,'Auteur : ',a, &
            //,16x,'Dépôt APP : ',a, &
#ifdef linux
             /,16x,'Version : ',a,' pour linux', &
#endif
#ifdef windows
             /,16x,'Version : ',a,' pour windows', &
#endif
             /,16x,'Dernier correctif : ',a, &
             /,16x,a, &
             /,16x,'Date de compilation : ',a, ' - Compilateur : ',a)
   1001 format(21x,i5,' Biefs',/,&
               21x,i5,' Sections')
   1002 format(20x,i6,' Points maximum pour décrire la géométrie d''un casier',/,&
               23x,i3,' Ouvrages élémentaires maximum par section singulière')
end subroutine Version



subroutine backup_data(taux)
! archivage du jeu de données et des fichiers de résultats en format texte
   use parametres, only: slash
   use Data_Num_Fixes, only: silent
   use, intrinsic :: iso_fortran_env, only: output_unit, error_unit
   implicit none
   ! -- prototype --
   integer,intent(in) :: taux
   ! -- variables locales --
   character(len=80) :: bakFile
   character(len=8)  :: subdir_archives = 'archives'
   character(len=24) :: timestamp
   character(len=180) :: command
   integer           :: time(8) ! valeurs renvoyees par la fonction date_and_time()
   logical           :: bexist

   if (.not.silent) then
      write(output_unit,*) ''
      write(output_unit,*) '>>>> Archivages des données et résultats texte de la simulation'
      write(output_unit,'(a,i1)')'      création archive en cours ; taux de compression ',taux
   endif
   call date_and_time(values=time)
   write(timestamp,'(i4.4,5(a1,i2.2),a1,i3.3)') time(1),'-',time(2),'-',time(3),'_',time(5),'-',time(6),'-',time(7),'-',time(8)
   ! si le sous-répertoire archives n'existe pas on le crée
   inquire(file=trim(subdir_archives)//slash//'.',exist=bexist)
   if (.not.bexist) then
      write(command,'(2a)') 'mkdir ',trim(subdir_archives)
      call execute_command_line(trim(command))
   endif

#ifdef linux
   !Nom du fichier de backup
   write(bakFile,'(5a)') trim(subdir_archives),slash,'backup_data_',trim(timestamp),'.tar'
   !Création archive (fichier tar)
   call execute_command_line('ls > liste')
   write(command,'(4a)') 'tar -cf ',trim(bakFile),' --exclude=liste --exclude=*.BIN --exclude=*.ods', &
                                                  ' --exclude=*.tar* --exclude=*.7z -T liste'
   call execute_command_line(trim(command))
   !Compression de l'archive
   write(command,'(a,i1,2a)') 'xz -',abs(taux),' ',trim(bakFile)
   call execute_command_line(trim(command))
   call execute_command_line('rm liste')
   if (.not.silent) write(output_unit,'(2a)') '      création archive terminée : ',trim(bakFile)//'.xz'
#endif
#ifdef windows
   !Nom du fichier de backup
   write(bakFile,'(5a)') trim(subdir_archives),slash,'backup_data_',trim(timestamp),'.7z'
   !Création archive (fichier 7z)
   call execute_command_line('dir /b > liste')
   write(command,'(a,i1,3a)') '7z.exe a -r -mx=',abs(taux),' ',trim(bakFile), &
                              ' @liste -x!liste -x!*.BIN -x!*.ods -x!*.tar* -x!*.7z -x!7z.log > 7z.log'
   call execute_command_line(trim(command))
   call execute_command_line('del liste')
   if (.not.silent) write(output_unit,'(2a)') '      création archive terminée : ',trim(bakFile)
#endif
#ifndef linux
#ifndef windows
   write(error_unit,*) 'L''option -z n''est disponible que pour Linux et MS-Windows'
#endif
#endif

end subroutine backup_data



subroutine close_All
!==============================================================================
!   Fermeture de tous les fichiers avec destruction éventuelle
!   des fichiers temporaires
!==============================================================================
   use booleens, only: deltmp
   use Parametres, only: l91, l9, lTra, lBin, lErr, long
   use IO_Files, only: latFile, varFile, cgnsFile
   use Ouvrages, only: nb_mobiles, all_OuvEle, write_buffer_mobiles, CSV_files_size
   use Mage_Utilitaires, only: simplification_lineaire, is_zero
#if WITH_CGNS==1
   use mage_cgns, only: mage_cg_close
#endif
   implicit none
   ! -- varaibles locales --
   integer :: nk, i, ios, npt, ier
   real(kind=long), allocatable :: x(:), y(:)
   real(kind=long) :: xi, yi

! DONE: vérifier que les unités logiques pour les fichiers temporaires ne seront pas prises par les fichiers de données
   if (deltmp) then
      if (latFile /= '') close(l91,status='delete')
    else
      if (latFile /= '') close(l91)
   endif
   !on force l'écriture des fichiers sur disque
   flush(unit=l9) ; flush(unit=lTra)

   !on vidange le buffer d'écriture des positions des ouvrages mobiles et on ferme les fichiers CSV
   if (nb_mobiles > 0) then  !il y a des ouvrages mobiles
      call write_buffer_mobiles()
      allocate (x(CSV_files_size+10), y(CSV_files_size+10))
      do nk = 1, size(all_OuvEle)
         if (all_OuvEle(nk)%mob > 0) then
            flush (unit=all_OuvEle(nk)%lu)
            rewind (all_OuvEle(nk)%lu)
            read(all_OuvEle(nk)%lu,*)
            read(all_OuvEle(nk)%lu,*)
            i = 1
            read(all_OuvEle(nk)%lu,'(f16.0,3x,f11.0)',iostat=ios) x(i), y(i)
            if (ios > 0) stop 1
            do
               read(all_OuvEle(nk)%lu,'(f16.0,3x,f11.0)',iostat=ios) xi, yi
               if (ios > 0) exit
               if (xi > x(i)) then  !on élimine les éventuels retours en arrière
                  i = i + 1
                  x(i) = xi  ;  y(i) = yi
               elseif (is_zero(xi-x(i))) then
                  x(i) = xi  ;  y(i) = yi
               else
                  cycle
               endif
            enddo
            npt = i
            rewind (all_OuvEle(nk)%lu)
            read(all_OuvEle(nk)%lu,*)
            read(all_OuvEle(nk)%lu,*)
            do i = 1, npt
               write(all_OuvEle(nk)%lu, '(f16.1,a3,f11.6)') x(i),' ; ',y(i)
            enddo
            flush (unit=all_OuvEle(nk)%lu)
            close (all_OuvEle(nk)%lu)
         endif
      enddo
   endif

   !fermeture des fichiers de résultat
   close(lTra) ; close(lBin) ; close(lErr)
   close(l9)

#if WITH_CGNS==1
   if (cgnsFile /= '') then
     call mage_cg_close()
   endif
#endif
end subroutine close_All


subroutine ask_stop
!==============================================================================
!   SIGNAL handler
!==============================================================================
   use Parametres, only: long, l9
   use Data_Num_Fixes, only: tmax
   use Data_Num_Mobiles, only: t
   use mage_utilitaires, only: heure
   use, intrinsic :: iso_fortran_env, only: output_unit, input_unit, int64
   ! -- Variables --
   character(len=1) :: cstop
   character(len=19) :: hms

   hms = heure(t)
   write(output_unit,*) ' '
   write(output_unit,'(3a)') '---> Interception du signal SIGINT (Ctrl+C) : voulez-vous interrompre la simulation à ', &
                             trim(hms),' ?'
   write(output_unit,*)      '     Non, je veux continuer     -> tapez c'
   write(output_unit,*)      '     Oui, tout de suite         -> tapez s[top] (défaut)'
   write(output_unit,*)      '     Oui, à la prochaine minute -> tapez m[inute]'
   write(output_unit,*)      '     Oui, à la prochaine heure  -> tapez h[eure]'
   write(output_unit,*)      '     Oui, au prochain jour      -> tapez j[our]'
   read(input_unit,'(a)') cstop
   select case (cstop(1:1))
      case ('c') ; continue
      case ('s') ; tmax = real(floor(t,kind=INT64),kind=long)+1._long
      case ('m') ; tmax = real(int(t/60._long)+1,kind=long)*60._long
      case ('h') ; tmax = real(int(t/3600._long)+1,kind=long)*3600._long
      case ('j') ; tmax = real(int(t/86400._long)+1,kind=long)*86400._long
      case default ; tmax = real(floor(t,kind=INT64),kind=long)+1._long
   end select
   if (cstop /= 'c') then
      hms = heure(tmax)
      write(output_unit,*) ' >>> interruption par l''utilisateur à ',trim(hms),' !!!'
      write(l9,*) ' >>> interruption par l''utilisateur !!! ',trim(hms),' !!!'
   endif
end subroutine ask_stop



subroutine echec_initialisation
! écritures pour mettre en évidence que l'initialisation a échoué
   use, intrinsic :: iso_fortran_env, only: output_unit
   use Parametres, only: lTra, l9
   use data_num_fixes, only : silent, plus
   implicit none
   ! -- variables --
   character(len=30) :: stars = repeat('*',30)
   character(len=80) :: blanc = repeat(' ',80)

   if (.not.silent) write(output_unit,'(a)') ' >>>> Fin inattendue de MAGE <<<<'
   write(lTra,'(1x,a)') stars(1:29)//' FIN INATTENDUE DE MAGE '//stars(1:27)
   write(l9,'(a)') plus//'---> FIN INATTENDUE DE MAGE'//blanc(26:79)  !bizarre mais on en a besoin pour PamHyr

end subroutine echec_initialisation
