!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - INRAE - 1989-2020                           #
!# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################


subroutine Analyse_Geo(check_fp)
!==============================================================================
!  Module d'analyse de la géométrie : appelle des routines d'analyse qui
!  détectent les bizarreries de la géométrie fournie par l'utilisateur
!
!Historique des modifications
!  24-07-2006 : création
!==============================================================================
   use, intrinsic :: iso_fortran_env, only: output_unit
   use Parametres, only: lTra, l9
   use data_num_fixes, only : silent
   use IO_Files, only: traFile
   implicit none
   ! -- prototype --
   ! vérification des lits majeurs (présence de cuvettes)
   integer, intent(in) :: check_fp  ! 0 -> abort si la vérification échoue (il y a au moins une cuvette)
                        ! 1 -> on passe outre
                        ! 2 -> on corrige brutalement en forçant les cotes trop basses à la cote de débordement
                        ! 3 -> on corrige seulement les points juste un peu trop bas (-10 cm par ex.)
                        !      et on continue si ça corrige tout, sinon on arrête
   ! -- variables --
   integer :: le

   le = output_unit ; if (silent) le = l9

   write(le,'(3a)') ' ----> Analyse de la géométrie (voir les détails dans ',trim(traFile),')'
   write(lTra,'(30x,a)') 'Analyse de la géométrie'
   write(lTra,'(a)') ' '
   write(le,'(7x,a)') '---> Recherche des marches d''escalier aux noeuds'
   call Detect_Marche_Noeud()
   write(le,'(7x,a)') '---> Recherche des ruptures de pente dans les biefs'
   call Analyse_Pentes()
   write(le,'(7x,a)') '---> Recherche des variations importantes de largeur du lit mineur'
   call Analyse_Largeurs()
   write(le,'(7x,a)') '---> Vérification du calcul des paramètres géométriques'
   call Verif_Geometrie()
   write(le,'(7x,a)') '---> Vérification de la cote du fond des lits majeurs'
   call Detect_Majeur_Creux
   ! on le fait 2 fois parce que c'est plus pratique pour faire une correction automatique
   ! et parce qu'on avait oublié que ça existait déjà !?!?!?
   write(le,'(7x,a)') '---> Vérification détaillée des lits majeurs...'
   call verif_LitMajeur(check_fp)

end subroutine Analyse_Geo



subroutine Detect_Marche_Noeud()
!==============================================================================
!  Module de détection des marches d'escalier au passage des noeuds
!
!Historique des modifications
!  24-07-2006 : création
!  30-01-2007 : dimension de Zfnd mise en paramètre et surveillance du
!               débordement du tableau (Err038)
!               seuil de détection d'une marche mis en paramètre
!==============================================================================
   use, intrinsic :: iso_fortran_env, only: output_unit
   use Parametres, only: Long, zero, lTra
   use TopoGeometrie, only: la_topo, zfd
   implicit none
   integer :: n, ib, k, kmax
   integer, parameter :: ksup=10
   real(kind=long), parameter :: seuilZ=0.001_long  !seuil de détection d'une marche
   real(kind=long) :: zfn, Zfnd(ksup), deltaZ

   write(lTra,'(23x,a)') ' Recherche des marches d''escalier aux noeuds'
   write(lTra,'(a)') ' '
   write(lTra,'(a)') ' Ecart maximum des cotes de fond à la moyenne'
   write(lTra,'(a,f6.3,a)') ' Filtre : écart absolu > ',seuilZ,' m'
   do n = 1, la_topo%net%nn
      zfn = zero
      k = 0  !nombre de biefs qui touche le nœud n
      do ib = 1, la_topo%net%nb
         if (la_topo%biefs(ib)%nam == n) then
            k = k+1 ; if (k>ksup) call err038
            Zfnd(k) = zfd(la_topo%biefs(ib)%is1)
            zfn = zfn + Zfnd(k)
         else if (la_topo%biefs(ib)%nav == n) then
            k = k+1 ; if (k>ksup) call err038
            Zfnd(k) = zfd(la_topo%biefs(ib)%is2)
            zfn = zfn + Zfnd(k)
         endif
      enddo
      kmax = k
      zfn = zfn / real(kmax,long)
      deltaZ = zero
      do k = 1, kmax
         deltaZ = max(deltaZ,abs(Zfnd(k)-Zfn))
      enddo
      if (deltaZ > seuilZ) then
         write(lTra,'(10x,3a,f8.3,a)') '   Nœud ',trim(la_topo%nodes(n)%name),' : ', deltaZ,' m'
      endif
   enddo
   write(lTra,'(a)') ' '
   write(lTra,'(a)') ' '
end subroutine Detect_Marche_Noeud



subroutine Analyse_Pentes()
!==============================================================================
!  Module de détection des ruptures de pente à l'intérieur des biefs
!  Filtre : pente_Max > 2 * pente_Min et pente_Amont ou pente_Aval > 0,5%
!
!Historique des modifications
!  24-07-2006 : création
!  02-02-2007 : documentation
!  08-03-2007 : correction d'un bug dans le calcul de dx (0,001 pour 0.001)
!               modification de la comparaison des pentes et de l'affichage
!               suppression du calcul de la variable rapport
!==============================================================================
   use, intrinsic :: iso_fortran_env, only: output_unit
   use Parametres, only: Long, zero, cent, lTra
   use TopoGeometrie, only: la_topo, xgeo, zfd
   implicit none
   integer :: is, ib
   real(kind=long) :: pente_amont, pente_aval, dxm, dxv
   real(kind=long) :: dzm, dzv, pente_max, pente_min
   character(len=20) :: chaine

   write(lTra,'(21x,a)') 'Recherche des ruptures de pente dans les biefs'
   write(lTra,'(a)') ' '
   write(lTra,'(2a)') ' Filtre : pente_max > 2 * pente_min et ', &
                         'pente_Amont ou pente_Aval > 0,5%'
   write(lTra,'(a)') '    Bief  Abscisse       Pente amont   Pente aval '

   do ib = 1, la_topo%net%nb  !boucle dans l'ordre des données
      do is = la_topo%biefs(ib)%is1+1, la_topo%biefs(ib)%is2-1
         dxm = abs(xgeo(is-1)-xgeo(is))
         dzm = abs(zfd(is-1)-zfd(is))
         dzv = abs(zfd(is)-zfd(is+1))
         dxv = abs(xgeo(is)-xgeo(is+1))
         if (dxm < 0.001_long .OR. dxv < 0.001_long) then
            if (chaine == ' marche ') then
               cycle
            else
               chaine = ' '
            endif
            if (dzm < 0.001_long) then
               pente_amont = zero
            elseif (dxm < 0.001_long) then
               pente_amont = sign(999.9999_long,zfd(is-1)-zfd(is))
               chaine = ' marche '
            else
               pente_amont = cent*(zfd(is-1)-zfd(is))/dxm
            endif
            if (dzv < 0.001_long) then
               pente_aval = zero
            elseif (dxv < 0.001_long) then
               pente_aval = sign(999.9999_long,zfd(is)-zfd(is+1))
               chaine = ' marche '
            else
               pente_aval = cent*(zfd(is)-zfd(is+1))/dxv
            endif
            if (chaine /= ' marche ') then
               if (pente_amont > pente_aval) then
                  chaine = ' ------ '
               else
                  chaine = ' ++++++ '
               endif
            endif
         else
            pente_amont = cent*(zfd(is-1)-zfd(is))/dxm
            pente_aval = cent*(zfd(is)-zfd(is+1))/dxv
            if (pente_amont > pente_aval) then
               chaine = ' ------ '
            else
               chaine = ' ++++++ '
            endif
         endif
         pente_max = max(abs(pente_amont),abs(pente_aval))
         pente_min = min(abs(pente_amont),abs(pente_aval))
         if ( pente_max < 2._long* pente_min .OR. pente_max < 0.5_long) then
            cycle
         else
            write(lTra,'(4x,i3.3,3x,f10.2,a,3x,f10.4,a,3x,f10.4,a,3x,a)')  &
                          ib,xgeo(is),'m ',pente_amont,'%',pente_aval,'%',chaine
         endif
      enddo
   enddo
   write(lTra,'(a)') ' '
   write(lTra,'(a)') ' '
end subroutine Analyse_Pentes



subroutine Analyse_Largeurs()
!==============================================================================
!  Module de détection des variations de largeurs importantes
!  Filtre : variation de largeur > 20%
!
!Historique des modifications
!  24-07-2006 : création
!  02-02-2007 : documentation
!==============================================================================
   use, intrinsic :: iso_fortran_env, only: output_unit
   use Parametres, only: Long, zero, cent, lTra
   use TopoGeometrie, only: la_topo, xgeo
   implicit none
   integer :: is, ib
   real(kind=long) :: la_surface, dx, largeur_moyenne, deltaL
   logical :: bool

   write(lTra,'(15x,a)') 'Recherche des variations importantes de largeur du lit mineur'
   write(lTra,'(a)') ' '
   write(lTra,'(a)') ' Filtre : variation de largeur > 20%'
   do ib = 1, la_topo%net%nb  !boucle dans l'ordre des données
      ! calcul de la largeur moyenne du bief à plein bord
      la_surface = zero
      do is = la_topo%biefs(ib)%is1, la_topo%biefs(ib)%is2-1
         dx = abs(xgeo(is+1) - xgeo(is))
         la_surface = la_surface + 0.5_long*dx*(la_topo%sections(is+1)%almy+la_topo%sections(is)%almy)
      enddo
      largeur_moyenne = la_surface / abs(xgeo(la_topo%biefs(ib)%is2)-xgeo(la_topo%biefs(ib)%is1))
      write(lTra,'(a,i3.3,a,f10.3,a)') ' Largeur moyenne à la cote de débordement mineur-moyen du bief ' &
                                         ,ib,' : ',largeur_moyenne,' m'
      bool = .true.
      do is = la_topo%biefs(ib)%is1, la_topo%biefs(ib)%is2
         deltaL = cent*abs(la_topo%sections(is)%almy-largeur_moyenne)/largeur_moyenne
         if (deltaL > 20._long) then
            if (bool) then
               write(lTra,'(12x,a)')'Abscisse   largeur   écart(%)'
               bool = .false.
            endif
            write(lTra,'(10x,3f10.2)') xgeo(is),la_topo%sections(is)%almy,deltaL
         endif
      enddo
   enddo
   write(lTra,'(a)') ' '
   write(lTra,'(a)') ' '
end subroutine Analyse_Largeurs



subroutine Verif_Geometrie()
!==============================================================================
!  Module de vérification du calcul des paramètres géométriques
!  Comparaison des calculs des largeur, périmètre et section mouillée
!  par les méthodes _LC et _XYZ
!==============================================================================
   use, intrinsic :: iso_fortran_env, only: output_unit
   use Parametres, only: Long, zero, lTra
   use objet_section, only: profil
   use TopoGeometrie, only: la_topo
   implicit none
   integer :: is, ib, k
   integer, parameter :: ndh=50
   real(kind=long) :: dh, z, tl, tp, ts, zl, zp, zs, tmp, zmax
   type(profil), pointer :: un_profil

   write(lTra,'(15x,a)') 'Vérification du calcul des paramètres géométriques'
   write(lTra,'(a)') ' Seuils de détection : 0,1 m pour la largeur et le périmètre, 1,0 m  pour la section'
   write(lTra,'(a,i3,a)') ' Échantillonage sur ',ndh,' niveaux'
   write(lTra,'(a)') ' '
   write(lTra,'(a)') ' Les profils ci-dessous sont probablement non standard car non situé dans un plan vertical.'
   write(lTra,'(a)') ' Autrement dit leur trace sur un plan horizontal n''est pas un segment de droite.'
   write(lTra,'(a)') ' Si c''est bien le cas, les calculs en XYZ sont a priori meilleurs.'
   write(lTra,'(a)') ' '
   write(lTra,'(a)') ' Bief, Pk, écarts max LC/XYZ et cote correspondante pour Largeur, Périmètre et Section mouillée'
   write(lTra,'(a)') ' Seconde ligne : valeurs LC / XYZ'
   do ib = 1, la_topo%net%nb  !boucle dans l'ordre des données
      do is = la_topo%biefs(ib)%is1, la_topo%biefs(ib)%is2
         un_profil => la_topo%sections(is)
         tl = zero ; tp =zero ; ts = zero; zmax = -999999.9
         do k = 1, un_profil%np
            zmax = max(zmax, un_profil%xyz(k)%z)
         enddo
         dh = (zmax - un_profil%zf) /real(ndh,kind=long)
         z = un_profil%zf
         do k = 1, ndh
            z = z + dh
            tmp = abs(un_profil%largeur_LC(z)-un_profil%largeur_XYZ(z,0))
            if (tl < tmp) then
               tl = tmp ; zl = z
            endif
            tmp = abs(un_profil%perimetre_LC(z)-un_profil%perimetre_XYZ(z,0))
            if (tp < tmp) then
               tp = tmp ; zp = z
            endif
            tmp = abs(un_profil%section_LC(z)-un_profil%section_XYZ(z,0))
            if (ts < tmp) then
               ts = tmp ; zs = z
            endif
            !tl = max(tl,abs(un_profil%largeur_LC(z)-un_profil%largeur_XYZ(z,0)))
            !tp = max(tp,abs(un_profil%perimetre_LC(z)-un_profil%perimetre_XYZ(z,0)))
            !ts = max(ts,abs(un_profil%section_LC(z)-un_profil%section_XYZ(z,0)))
         enddo
         if (tl > 0.1_long .or. tp > 0.1_long .or. ts > 1.0_long) then
            write(lTra,'(i3,3x,7f12.4)') ib,un_profil%pk,tl,zl,tp,zp,ts,zs
            write(lTra,'(16x,6f12.4)') un_profil%largeur_LC(zl),un_profil%largeur_XYZ(zl,0), &
                                   un_profil%perimetre_LC(zp),un_profil%perimetre_XYZ(zp,0), &
                                     un_profil%section_LC(zs),un_profil%section_XYZ(zs,0)
         endif
      enddo
   enddo
   write(lTra,'(a)') ' '
   write(lTra,'(a)') ' '
end subroutine Verif_Geometrie


subroutine Detect_Majeur_Creux()
!==============================================================================
!   routine de détection des lits majeurs dont la cote du fond est inférieure
!   à la cote de débordement
!==============================================================================
   use, intrinsic :: iso_fortran_env, only: output_unit
   use Parametres, only: Long, zero, lTra
   use objet_section, only: profil
   use TopoGeometrie, only: la_topo
   implicit none
   integer :: is, ib, irg, ird
   real(kind=long) :: zfd_l, zfd_r, zbg_l, zbg_r
   type(profil), pointer :: prof
   character(len=3) :: tag


   write(lTra,'(15x,a)') 'Vérification de la cote du fond des lits majeurs'
   write(lTra,'(a)')     'Pour chaque profil on vérifie si les cotes de fond des lits majeurs gauche et droit', &
                         'sont supérieures aux cotes de débordement correspondantes',&
                         'Si ce n''est pas le cas une étiquette !!! est ajoutée à la fin de la ligne',&
                         'Les colonnes Aire gche et Aire droite indiquent les surfaces mouillées des lits majeurs '//&
                         'à leur cote de berge respective',&
                         'Ces aires devraient être nulles'
   write(lTra,'(a)') ''
   write(lTra,'(a)') ' Bief       Pk       Zfd gauche  Zbg gauche  Aire gche   Zfd droit   Zbg droit  Aire droite'
   do ib = 1, la_topo%net%nb  !boucle dans l'ordre des données
      do is = la_topo%biefs(ib)%is1, la_topo%biefs(ib)%is2
         prof => la_topo%sections(is)
         zfd_l = zero  ;  zfd_r = zero
         irg = prof%li(1)  ;  ird = prof%li(2)
         zbg_l = prof%xyz(irg)%z
         zbg_r = prof%xyz(ird)%z
         ! recherche de la cote du fond du lit majeur gauche
         zfd_l = minval(prof%xyz(1:irg)%z)
         zfd_r = minval(prof%xyz(ird:prof%np)%z)
         if (zfd_l < zbg_l .or. zfd_r < zbg_r) then
            tag = '!!!'
         else if(zfd_l > zbg_l .or. zfd_r > zbg_r) then
            tag = '???'
         else
            tag = '   '
         endif
         write(lTra,'(i3,3x,7f12.4,3x,a)') ib, prof%pk, zfd_l, zbg_l, prof%smaj_left, &
                                                        zfd_r, zbg_r, prof%smaj_right, tag
      enddo
   enddo
   write(lTra,'(a)') ' '
   write(lTra,'(a)') ' '
end subroutine Detect_Majeur_Creux



subroutine verif_LitMajeur(check_fp)
! on vérifie que les points RG et RD sont les points les plus bas de leur lit majeur respectif
   use, intrinsic :: iso_fortran_env, only: output_unit
   use Parametres, only: long, lTra, l9
   use objet_section, only: profil
   use TopoGeometrie, only: la_topo, profil
   use data_num_fixes, only : silent
   implicit none
   ! -- prototype --
   integer,intent(in) :: check_fp !valeur de l'option CLI -fp
   ! -- variables --
   integer :: ib, is, k, nrg, nrd, nrgg, nrdd, le
   real(kind=long) :: z0
   type(profil), pointer :: prof
   integer :: nb_profils_KO, nb_profils_fixed

   le = output_unit ; if (silent) le = l9

   write(lTra,'(a)') ' ==> Vérification détaillée des lits majeurs...'
   nb_profils_KO = 0
   nb_profils_fixed = 0
   do ib = 1, la_topo%net%nb
      do is = la_topo%biefs(ib)%is1, la_topo%biefs(ib)%is2
         prof => la_topo%sections(is)
         nrg = 0  ;  nrgg = 0
         nrd = 0  ;  nrdd = 0
         z0 = prof%xyz(prof%li(1))%z
         do k = 1, prof%li(1)
            if (prof%xyz(k)%z < z0) nrg = nrg + 1
            if (check_fp == 2) then
               prof%xyz(k)%z = max(z0,prof%xyz(k)%z)
            elseif (check_fp == 3) then
               if (prof%xyz(k)%z < z0 - 0.1_long) then
                  nrgg = nrgg + 1
               elseif (prof%xyz(k)%z < z0) then
                  prof%xyz(k)%z = max(z0,prof%xyz(k)%z)
               endif
            endif
         enddo
         z0 = prof%xyz(prof%li(2))%z
         do k = prof%li(2), prof%li(3)
            if (prof%xyz(k)%z < z0) nrd = nrd + 1
            if (check_fp == 2) then
               prof%xyz(k)%z = max(z0,prof%xyz(k)%z)
            elseif (check_fp == 3) then
               if (prof%xyz(k)%z < z0 - 0.1_long) then
                  nrdd = nrdd + 1
               elseif (prof%xyz(k)%z < z0) then
                  prof%xyz(k)%z = max(z0,prof%xyz(k)%z)
               endif
            endif
         enddo
         if (nrg > 0) then
            write(lTra,'(a,f10.3,2(a,i0),a)') '>>>> ERREUR dans la géométrie : le profil situé au pk ',prof%pk, &
                     ' du bief ',ib,' a ',nrg,' points en lit majeur gauche plus bas que la cote du point RG'
         endif
         if (nrd > 0) then
            write(lTra,'(a,f10.3,2(a,i0),a)') '>>>> ERREUR dans la géométrie : le profil situé au pk ',prof%pk, &
                     ' du bief ',ib,' a ',nrd,' points en lit majeur droit plus bas que la cote du point RD'
         endif
         if (nrg+nrd > 0) then
            nb_profils_KO = nb_profils_KO + 1
            if (check_fp == 1) then
               nb_profils_KO = nb_profils_KO - 1
               nb_profils_fixed = nb_profils_fixed + 1
            elseif (check_fp == 2) then
               call prof%largeursCotes()  !profil réparé, mettre à jour prof%wh
               nb_profils_KO = nb_profils_KO - 1
               nb_profils_fixed = nb_profils_fixed + 1
            elseif (check_fp == 3 .and. nrgg+nrdd == 0) then
               call prof%largeursCotes()  !profil réparé, mettre à jour prof%wh
               nb_profils_KO = nb_profils_KO - 1
               nb_profils_fixed = nb_profils_fixed + 1
            endif
         endif
      enddo
   enddo
   if (nb_profils_KO > 0) then
      write(le,'(7x,a,i0,a)') '>>>> ERREUR dans la géométrie : ',nb_profils_KO, &
                              ' profils ont un lit majeur plus bas que la cote de débordement'
      write(le,'(12x,a)')'Voir le fichier TRA pour les détails'
      write(le,'(12x,a)') 'Vérification des lits majeurs : KO'
      write(lTra,'(3x,a)') 'Vérification des lits majeurs : KO'
      write(l9,'(a)') ' ==> Vérification des lits majeurs : KO (voir fichier TRA)'
      call echec_initialisation
      stop 1
   elseif (nb_profils_fixed > 0) then
      if (check_fp == 1) then
         write(le,'(12x,a,i0,a)') 'Vérification des lits majeurs : OK (',nb_profils_fixed,' profils laissés en l''état)'
         write(lTra,'(3x,a,i0,a)') 'Vérification des lits majeurs : OK (',nb_profils_fixed,' profils laissés en l''état)'
      elseif (check_fp > 1) then
         write(le,'(12x,a,i0,a)') 'Vérification des lits majeurs : OK (',nb_profils_fixed,' profils corrigés)'
         write(lTra,'(3x,a,i0,a)') 'Vérification des lits majeurs : OK (',nb_profils_fixed,' profils corrigés)'
      endif
      write(le,'(12x,a)')'Voir le fichier TRA pour les détails'
   else
      write(le,'(12x,a)') 'Vérification des lits majeurs : OK'
   endif
end subroutine verif_LitMajeur
