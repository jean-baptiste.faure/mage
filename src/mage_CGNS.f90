module mage_cgns
   use parametres, only: long

   ! CGNS

   implicit none

#if WITH_CGNS==1

   integer :: index_file,index_base(2),timestamp_cgns,index_coord
   integer,allocatable,private :: index_zone(:),index_field(:),index_section(:),index_izone(:),index_isect(:)
   integer,allocatable :: index_flow_vertex(:),index_flow_cell(:)
   integer :: nb_timesteps_cgns, max_nb_timesteps_cgns
   integer :: dim_cgns(2)
   character(len=32) :: basename,solname
   character(len=32),allocatable :: zonename(:),zonename2(:)

    type c32vector
        character(len=32),allocatable,dimension(:) :: values
        integer vectsize
        integer realsize
        integer base
    end type c32vector

    type(c32vector) :: gridmotionpointers,gridcoordpointers

    type ivector
        integer,allocatable,dimension(:) :: values
        integer vectsize
        integer realsize
    end type ivector

    type dvector
        real(kind=long),allocatable,dimension(:) :: values
        integer vectsize
        integer realsize
    end type dvector

    type(dvector) :: times_cgns

    contains

    subroutine c32_push_back(x, newvalue)
        class(c32vector),intent(inout) :: x
        character(len=32),intent(in) :: newvalue
        integer :: newsize
        character(len=32),allocatable :: tmp(:)

        ! ici on ajoute newvalue
        ! si x est trop petit, on double sa taille jusqu'à ce que ça soit assez grand
        do while (x%realsize < x%vectsize + 1)
            newsize = x%realsize*2
            allocate(tmp(newsize)) ! on double la taille allouée
            tmp(1:x%vectsize) = x%values(1:x%vectsize) !on copie les anciennes valeurs de x
            call move_alloc(tmp, x%values) ! x pointe sur l'espace mémoire de tmp, tmp est désalloué
            x%realsize = newsize
            ! x a maintenant la place d'accueilir newvalue
        end do
        ! on a plus qu'à ajouter newvalue
        x%values(x%vectsize+1) = newvalue
        ! on n'oublie pas d'augmenter la taille de x
        x%vectsize = x%vectsize + 1
    end subroutine c32_push_back

    subroutine d_push_back(x, newvalue)
        class(dvector),intent(inout) :: x
        real(kind=long),intent(in) :: newvalue
        integer :: newsize
        real(kind=long),dimension(:),allocatable :: tmp

        ! ici on ajoute newvalue
        ! si x est trop petit, on double sa taille jusqu'à ce que ça soit assez grand
        do while (x%realsize < x%vectsize + 1)
            newsize = x%realsize*2
            allocate(tmp(newsize)) ! on double la taille allouée
            tmp(1:x%vectsize) = x%values(1:x%vectsize) !on copie les anciennes valeurs de x
            call move_alloc(tmp, x%values) ! x pointe sur l'espace mémoire de tmp, tmp est désalloué
            x%realsize = newsize
            ! x a maintenant la place d'accueilir newvalue
        end do
        ! on a plus qu'à ajouter newvalue
        x%values(x%vectsize+1) = newvalue
        ! on n'oublie pas d'augmenter la taille de x
        x%vectsize = x%vectsize + 1
    end subroutine d_push_back

    subroutine i_push_back(x, newvalue)
        class(ivector),intent(inout) :: x
        integer,intent(in) :: newvalue
        integer :: newsize
        integer,dimension(:),allocatable :: tmp

        ! ici on ajoute newvalue
        ! si x est trop petit, on double sa taille jusqu'à ce que ça soit assez grand
        do while (x%realsize < x%vectsize + 1)
            newsize = x%realsize*2
            allocate(tmp(newsize)) ! on double la taille allouée
            tmp(1:x%vectsize) = x%values(1:x%vectsize) !on copie les anciennes valeurs de x
            call move_alloc(tmp, x%values) ! x pointe sur l'espace mémoire de tmp, tmp est désalloué
            x%realsize = newsize
            ! x a maintenant la place d'accueilir newvalue
        end do
        ! on a plus qu'à ajouter newvalue
        x%values(x%vectsize+1) = newvalue
        ! on n'oublie pas d'augmenter la taille de x
        x%vectsize = x%vectsize + 1
    end subroutine i_push_back

    subroutine c32_allocate(x, vect_size)
        type(c32vector),intent(inout) :: x
        integer,intent(in) :: vect_size
        allocate (x%values(vect_size))
        x%realsize = vect_size
        x%vectsize = 0
    end subroutine c32_allocate

    subroutine i_allocate(x, vect_size)
        type(ivector),intent(inout) :: x
        integer,intent(in) :: vect_size
        allocate (x%values(vect_size))
        x%realsize = vect_size
        x%vectsize = 0
    end subroutine i_allocate

    subroutine d_allocate(x, vect_size)
        type(dvector),intent(inout) :: x
        integer,intent(in) :: vect_size
        allocate (x%values(vect_size))
        x%realsize = vect_size
        x%vectsize = 0
    end subroutine d_allocate

    subroutine c32_deallocate(x)
        type(c32vector),intent(inout) :: x
        deallocate (x%values)
        x%realsize = 0
        x%vectsize = 0
    end subroutine c32_deallocate

    subroutine i_deallocate(x)
        type(ivector),intent(inout) :: x
        deallocate (x%values)
        x%realsize = 0
        x%vectsize = 0
    end subroutine i_deallocate

    subroutine d_deallocate(x)
        type(dvector),intent(inout) :: x
        deallocate (x%values)
        x%realsize = 0
        x%vectsize = 0
    end subroutine d_deallocate

subroutine init_bief_cgns
!   open CGNS file for read-only
      call cg_open_f('grid.cgns',CG_MODE_READ,index_file,ier)
      if (ier .ne. CG_OK) call cg_error_exit_f
!   we know there is only one base (real working code would check!)
      index_base=1
!   we know there is only one zone (real working code would check!)
      index_zone=1
!   get zone size (and name - although not needed here)
      call cg_zone_read_f(index_file,index_base,index_zone,zonename,
     + isize,ier)
!   lower range index
      irmin(1)=1
      irmin(2)=1
      irmin(3)=1
!   upper range index of vertices
      irmax(1)=isize(1,1)
      irmax(2)=isize(2,1)
      irmax(3)=isize(3,1)
!   read grid coordinates
      call cg_coord_read_f(index_file,index_base,index_zone,&
     & 'CoordinateX',RealSingle,irmin,irmax,x,ier)
      call cg_coord_read_f(index_file,index_base,index_zone,&
     & 'CoordinateY',RealSingle,irmin,irmax,y,ier)
      call cg_coord_read_f(index_file,index_base,index_zone,&
     & 'CoordinateZ',RealSingle,irmin,irmax,z,ier)
!   close CGNS file
      call cg_close_f(index_file,ier)
end init_geo_cgns

subroutine init_CGNS
!==============================================================================
!             Entête du fichier CGNS (fichier de sortie)
!==============================================================================
   use TopoGeometrie, only: la_topo
   use IO_Files, only: cgnsFile
   use cgns
   implicit none
   ! -- Variables locales temporaires --
   integer :: is, nsect, p, ier
   integer :: i, ib, j, index_field, nzone, npoints, k, l
   integer:: nb, ns
   integer :: nelem_end,ielem_no,nelem_start,ifirstnode,nbdyelem,icelldim,iphysdim
   real(kind=long),allocatable :: zone_id(:)
   integer,allocatable :: ielem(:,:)
   integer :: isize(1,3)
   real(kind=long),allocatable :: x(:),y(:),z(:)

   timestamp_cgns = 0
   max_nb_timesteps_cgns = 3
   nb = la_topo%net%nb
   ns = la_topo%net%ns
   allocate(index_izone(nb),index_isect(nb))
   allocate(index_zone(nb),index_section(nb),index_flow_vertex(nb),index_flow_cell(nb))
   allocate(zonename(nb))
   allocate(zonename2(nb))
   call d_allocate(times_cgns, 10)
   call c32_allocate(gridcoordpointers, 10)
   call c32_allocate(gridmotionpointers, 10)

   ! open CGNS file for write
   call cg_open_f(cgnsFile,CG_MODE_WRITE,index_file,ier)
   if (ier .ne. CG_OK) call cg_error_exit_f
   ! create base (user can give any name)
   basename='Base'
   icelldim=1
   iphysdim=3
   ib = 1
   call cg_base_write_f(index_file,basename,icelldim,iphysdim,index_base(1),ier)
!    do ib=1,nb
!       nsect = la_topo%biefs(ib)%is2-la_topo%biefs(ib)%is1+1
!       nzone = la_topo%sections(la_topo%biefs(ib)%is1)%nzone
      nsect = la_topo%net%ns
      nzone = la_topo%sections(1)%nzone
      npoints = 0
      do j=1,nsect
         npoints = npoints + la_topo%sections(j)%np
      enddo
      allocate(x(npoints),y(npoints),z(npoints))
      allocate(ielem(2,npoints-nsect))
      allocate(zone_id(npoints-nsect))
      k=0
      ielem_no=0
      do j=1,nsect
         do i=1,la_topo%sections(j)%np
            ! nodes
            k=k+1
            x(k) = la_topo%sections(j)%xyz(i)%x
            y(k) = la_topo%sections(j)%xyz(i)%y
            z(k) = la_topo%sections(j)%xyz(i)%z
            ! elements
            if (i .ne. la_topo%sections(j)%np) then
               ielem_no=ielem_no+1
               ielem(1,ielem_no)=k
               ielem(2,ielem_no)=k+1
               do l=1, la_topo%sections(j)%nzone
                  if (i >= la_topo%sections(j)%li(l-1) .and. &
                    & i < la_topo%sections(j)%li(l)) then
                     zone_id(ielem_no)=real(l)
                     exit
                  endif
               enddo
            endif
         enddo
      enddo
      write(zonename(ib),'(A4,I0)') 'Bief',ib
      ! vertex size
      isize(1,1)=npoints
      ! cell size
      isize(1,2)=npoints-nsect
      ! boundary vertex size (always zero for structured grids)
!       isize(1,3)=(nsect+np)*2
      isize(1,3)=0

      ! create zone
      call cg_zone_write_f(index_file,index_base(1),zonename(ib),isize,Unstructured,index_zone(ib),ier)
      ! write grid coordinates (user must use SIDS-standard names here)
      call cg_coord_write_f(index_file,index_base(1),index_zone(ib),RealDouble,'CoordinateX',x,index_coord,ier)
      call cg_coord_write_f(index_file,index_base(1),index_zone(ib),RealDouble,'CoordinateY',y,index_coord,ier)
      call cg_coord_write_f(index_file,index_base(1),index_zone(ib),RealDouble,'CoordinateZ',z,index_coord,ier)
      call cg_section_write_f(index_file,index_base(1),index_zone(ib),'Elem',&
     &BAR_2,1,ielem_no,0,ielem,index_section(ib),ier)
      call cg_sol_write_f(index_file,index_base(1),index_zone(ib),'FlowSolutionVertex',Vertex,index_flow_vertex(ib),ier)
      call cg_field_write_f(index_file,index_base(1),index_zone(ib),index_flow_vertex(ib),RealDouble,'Zfn',z,index_field,ier)
      call cg_sol_write_f(index_file,index_base(1),index_zone(ib),'FlowSolutionCell',CellCenter,index_flow_cell(ib),ier)
      call cg_field_write_f(index_file,index_base(1),index_zone(ib),index_flow_cell(ib),RealDouble,'Lit',zone_id,index_field,ier)

      deallocate(x,y,z)
      deallocate(zone_id)
      deallocate(ielem)
!    enddo
end subroutine init_CGNS

subroutine mage_ecrire_cgns

   use TopoGeometrie, only: la_topo
   use data_num_mobiles, only: t, dt
   use cgns

   implicit none

   integer :: ier, ib, index_motion
   integer:: nb, ns

   timestamp_cgns = timestamp_cgns+1
   call d_push_back(times_cgns, t-dt)
   write(sol_names_temp,'(a4,i0)')"Cell",nb_timesteps_cgns
   call c32_push_back(sol_names,sol_names_temp)
   write(sol_names_temp,'(a4,i0)')"Vertex",nb_timesteps_cgns
   call c32_push_back(sol_names2,sol_names_temp)

end subroutine mage_ecrire_cgns

subroutine mage_cg_close

   use TopoGeometrie, only: la_topo
   use IO_Files, only: cgnsFile
   use cgns

   implicit none

   integer :: ier, ib, k, index_motion
   integer:: nb, ns
   character(len=64) :: mychar, mychar2

   nb = la_topo%net%nb

   dim_cgns(1)=32
   dim_cgns(2)=timestamp_cgns

   ib = 1
   call cg_biter_write_f(index_file,index_base(1),'TimeIterValues',timestamp_cgns,ier)
   call cg_ziter_write_f(index_file,index_base(1),index_zone(ib),'ZoneIterativeData',ier)
   call cg_gopath_f(index_file,'/Base/TimeIterValues',ier)
   call cg_array_write_f('TimeValues',RealDouble,1,timestamp_cgns,times_cgns%values,ier)
   call cg_simulation_type_write_f(index_file,index_base(1),TimeAccurate,ier)

   call cg_close_f(index_file,ier)
   deallocate(index_zone,index_izone,index_section,index_flow_vertex,index_flow_cell)
   write(6,*)'Successfully wrote results to file ',trim(cgnsFile)
   call d_deallocate(times_cgns)

end subroutine mage_cg_close

#endif

end module mage_cgns
