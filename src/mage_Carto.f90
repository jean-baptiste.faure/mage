!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - INRAE - 1989-2020                           #
!# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          # #
!##############################################################################
!##############################################################################
!#                                                                            #
!#                    Programmme Mage6_Carto                                  #
!#                                                                            #
!#                           VERSION ???                                      #
!#                                                                            #
!##############################################################################
!
!
!role: cartographie au format dxf à partir de ENV

module parametres_carto

   implicit none
   integer, parameter :: long=kind(1.0d0)
   integer, parameter :: sp=kind(1.0)
   integer, parameter :: issup=3500 !nombre maxi de sections
   integer, parameter :: npmax=100   !nb de points maxi par section

end module parametres_carto

program Mage6_Carto
!
! programme principal de test de la routine spatialisation()
!
   use parametres_carto
   use DXFortranStd
   implicit none

   character(len=35) :: ficnet=' '   ! fichier NET = liste des fichiers ST avec la
                                     ! correspondance avec les numéro de bief de ENV et TAL
   character(len=35) :: ficenv=' '   ! fichier ENV resultat de MAGE

   character(len=80) :: ligne
   integer :: ldxf=64  !unite logique du fichier dxf
   character(len=10) :: layerDef(1,3)


   !lecture du fichier de configuration spatalisation.config
   open(unit=61,file='spatialisation.config',form='formatted',status='old')
   do
      read(61,'(a)',end=100) ligne
      if(ligne(1:14) == 'Fichier reseau') then
         call extraire(ligne,'=',ficnet)
         cycle
       elseif(ligne(1:17) == 'Fichier enveloppe') then
         call extraire(ligne,'=',ficenv)
         cycle
      endif
   enddo
   100 continue

   print*,' Fichier reseau = ',ficnet
   print*,' Fichier enveloppe = ',ficenv

   open(unit=ldxf,file=ficnet(1:len_trim(ficnet)-3)//'dxf',form='formatted',status='unknown')
   layerDef(1,1)='Water' ! nom de la couche
   layerDef(1,2)='0'     !line type
   layerDef(1,3)='5'     !couleur de la couche

   call dfBegin(ldxf,layerDef)

   call spatialisation(ficnet,ficenv,ldxf)

   call dfEnd(ldxf)

end program Mage6_Carto


subroutine extraire(ligne,car,mot)
! routine d'extraction de la chaine 'mot' qui suit le caractère de controle 'car'
! dans une chaine 'ligne'. 'mot' commence apres le dernier blanc qui suit 'car'

   character(len=80) :: ligne
   character(len=*) :: mot
   character(len=1) :: car
   integer :: nl, n, nc

   nl = len_trim(ligne) !longueur de la chaine ligne
   nc = index(ligne,car)  ! 1ere occurence de car
   do n=nc+1,80
      if (ligne(n:n) .ne. ' ') then
         mot(1:nl-n+1) = ligne(n:nl)
         return
      endif
   enddo
   mot = ' '
end subroutine extraire
!------------------------------------------------------------------------------------
subroutine spatialisation(ficnet,ficenv,ldxf)
!
! OBJET : maillage + MTO + projection pour chaque bief du réseau

! date de creation : 06/06/2002
! historique des modifications
   use parametres_carto
   use, intrinsic :: iso_fortran_env, only: output_unit

   implicit none

   character(len=35) :: ficnet   ! fichier NET = liste des fichiers ST avec la
                               ! correspondance avec les numéro de bief de ENV et TAL
   character(len=35) :: ficenv   ! fichier ENV resultat de MAGE
   character(len=35) :: ficout   ! fichier de sortie pour le GIS

   character(len=35) :: ficst    ! fichier ST courant
   character(len=35) :: ficm='tmp.m'
   character(len=35) :: ficmto='tmp.mto'
   integer :: nobief
   real(kind=sp) :: pas
   character(len=80) :: ligne

   integer :: ldxf  !unite logique du fichier dxf

   open(unit=61,file=ficnet,status='unknown',form='formatted')

   do
      read(61,'(a)',end=100) ligne
      if (ligne(1:1) == '*') cycle
      read(ligne,*,end=100) nobief,ficst, pas
      write(output_unit,'(i3,3x,a,f8.3)') nobief,ficst(1:len_trim(ficst)),pas
      print*,' appel mailleur'
      call secma5(ficst,ficm, pas)
      print*,' appel MTO'
      call mto(ficm,ficenv,nobief,ficmto)
      print*,' appel projection'
      call projection(ficm,ficmto,ldxf)
   enddo
   100 close(unit=61)
end subroutine spatialisation


!------------------------------------------------------------------------------------
subroutine projection(ficm,ficmto,ldxf)
!
! OBJET : projection d'un fichier MTO sur une grille uniforme

! date de creation : 06/06/2002
! historique des modifications
   use parametres_carto
   use DXFortranStd
   use, intrinsic :: iso_fortran_env, only: output_unit
   implicit none

   character(len=35) :: ficm
   character(len=35) :: ficmto
   real(kind=long) :: x1, x2, y1, y2

   integer :: ldxf  !unite logique du fichier dxf

   ! maillage
   real(kind=long) :: x(issup,npmax), y(issup,npmax),z(issup,npmax), h(issup,npmax)
   real(kind=long) :: xmin, xmax, ymin, ymax  !Bornes du maillage
   character(len=80) :: ligne
   integer :: np, i, j, is=0, ismax=0, k
   logical :: bool1, bool2

   !ouverture des fichiers
   open(unit=62,file=ficm,status='unknown',form='formatted')
   open(unit=63,file=ficmto,status='unknown',form='formatted')

   !lecture des sections
   is = 0
   do
      read(62,'(a)',end=100) ligne
      if (ligne(1:1) == '#') cycle
      read(ligne(19:24),'(i6)') np
      if (np > npmax) then
         write(output_unit,'(a,i3)') 'nombre de points par profil trop grand : ',np
         stop 1
      endif
      is = is+1
      do i=1,np
         read(62,'(a)') ligne
         read(ligne,'(3f13.0)') x(is,i),y(is,i),z(is,i)  !lecture des sommets de maille
      enddo
      read(62,'(a)') ligne   !ligne des 999.000
   enddo
   100 ismax = is
   xmin = minval(x) ; ymin = minval(y)
   xmax = maxval(x) ; ymax = maxval(y)
   do
      read(63,'(a)',end=200) ligne
      if (ligne(1:1) == '#') cycle
      exit
   enddo
   backspace(63)
   ! lecture des profondeurs par maille
   do is=1,ismax-1
      do i=1,np-1
         read(63,'(f9.0)',end=200) h(is,i)
      enddo
      read(63,'(a)') ligne   !ligne du 999.99
   enddo

   200 close(62)  !fermeture des fichiers
   close(63)

   ! remplissage du fichier dxf
   call gen_dxf(ldxf,x,y,z,h,ismax,np)
end subroutine projection


subroutine gen_dxf(unit,x,y,z,h,ismax,np)
! ecriture d'un fichier dxf avec des 3DFace colorees en fonction de la
! profondeur d'eau
   use parametres_carto
   use DXFortranStd
   implicit none

   !integer, parameter :: npmax=50   !nb de points maxi par section
   integer :: unit  ! unite logique du fichier dxf
   integer :: ismax ! nombre de profils
   integer :: np    ! nombre de points par profil

   character(len=3) :: color

   real(kind=long) :: x(issup,npmax), y(issup,npmax),z(issup,npmax), h(issup,npmax)
   real(kind=sp) :: x1,y1,z1,x2,y2,z2,x3,y3,z3,x4,y4,z4, zz, thickness
   integer :: is, k
   do is=1, ismax-1
      do k=1,np-1
         if(h(is,k) > -0.001) then
            if(h(is,k)>0.001 .and. h(is,k)<0.2) then
               color='120'
             elseif(h(is,k)>0.2_long .and. h(is,k)<0.4_long) then
               color='130'
             elseif(h(is,k)>0.4_long .and. h(is,k)<0.6_long) then
               color='140'
             elseif(h(is,k)>0.6_long .and. h(is,k)<0.8_long) then
               color='150'
             elseif(h(is,k)>0.8_long .and. h(is,k)<1.0_long) then
               color='160'
             elseif(h(is,k)>1.0_long .and. h(is,k)<2.0_long) then
               color='170'
             elseif(h(is,k)>2.0_long) then
               color='180'
             else
               color='0'
            endif
            x1=real(x(is,k),4)     ; y1=real(y(is,k),4)     ; z1=real(h(is,k),4)
            x2=real(x(is,k+1),4)   ; y2=real(y(is,k+1),4)   ; z2=real(h(is,k),4)
            x3=real(x(is+1,k+1),4) ; y3=real(y(is+1,k+1),4) ; z3=real(h(is,k),4)
            x4=real(x(is+1,k),4)   ; y4=real(y(is+1,k),4)   ; z4=real(h(is,k),4)
            !write(color,'(i3)') min(255,5*(int(zz*100.)/5))
            call df3DFace(unit,'Water','0',color,x1,y1,z1,x2,y2,z2,x3,y3,z3,x4,y4,z4)
            !zz = 0.25*(z1+z2+z3+z4)
            !thickness = 0.01
            !call dfSolid(unit,'Water','1',color,x1,y1,x2,y2,x3,y3,x4,y4,zz,thickness)
         endif
      enddo
   enddo
end subroutine gen_dxf
