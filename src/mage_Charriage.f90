!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - INRAE - 1989-2020                           #
!# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################

module charriage
use Parametres, only: long, zero, un, g, rho, nu, gnu2, untier, nzsup, i_debord, i_mixte, i_mixte2, deuxtiers, PI, lTra, l9
use, intrinsic :: iso_fortran_env, only: error_unit, output_unit

use objet_point, only: point3D, distanceH, p_scalaire_H, couche_sedimentaire
use objet_section, only: profil, is_LC, largeur_XYZ, couche_sed_section
use TopoGeometrie, only: la_topo, zfd, xgeo, numero_bief, export_ST_final, with_charriage
use Mage_Utilitaires, only: next_real, next_int, next_string, zegal, do_crash, f0p, heure, lire_date
use basic_Types, only: point2D
use Data_Num_Mobiles, only: ttra
use Data_Num_Fixes, only: dtbase, dt_char, tmax, dttra, date_format, tinf
use IO_Files, only: btmFile, qstFile, graFile
use Ouvrages, only: all_OuvCmp, all_OuvEle

implicit none

! precision sur les hauteurs
! utilise aussi pour les vitesses
   real(kind=long), parameter :: EPSY=0.000001
   real(kind=long), parameter :: SENSI=0.001

! valeurs par défaut
   real(kind=long), parameter :: rhos_def = 2650._long    !masse volumique du sédiment
   real(kind=long), parameter :: phis_def = 40._long      !angle de repos du sédiment
   real(kind=long), parameter :: porosity_def = 0.40_long !porosité = volume de vide / volume du sédiment
   real(kind=long), parameter :: a_Han_def = 100._long    !distance de Han
   real(kind=long), parameter :: L_d_def = 100._long      !distance de chargement relative à l’évolution du d50
   real(kind=long), parameter :: L_s_def = 100._long      !distance de chargement relative à l’évolution du sigma
   integer        , parameter :: method_geo_def = 1       !index de la méthode de mise à jour de la géométrie
   integer        , parameter :: shields_def = 1          !index de la méthode pour définir le nombre de Shields critique
   integer        , parameter :: shields_cor_def = 1      !index de la méthode de correction pour obtenir le nombre de Shields effectif
   integer        , parameter :: cap_sol_def = 1          !index de la méthode de calcul de la capacité solide
   real(kind=long), parameter :: f_mult_def = 1._long     !facteur multiplicateur pour la loi de capacité solide
   real(kind=long), parameter :: coefproche = 1._long     !coef proximite couches

! variables globales
   real(kind=long) :: rhos                 !masse volumique du sédiment
   real(kind=long) :: phis                 !angle de repos du sédiment
   real(kind=long) :: porosity             !porosité = volume de vide / volume du sédiment
   real(kind=long) :: a_Han                !distance de Han
   real(kind=long) :: L_d                  !distance de chargement relative à l’évolution du d50
   real(kind=long) :: L_s                  !distance de chargement relative à l’évolution du sigma
   integer         :: method_geo           !index de la méthode de mise à jour de la géométrie
   integer         :: shields              !index de la méthode pour définir le nombre de Shields critique
   integer         :: shields_correction   !index de la méthode de correction pour obtenir le nombre de Shields effectif
   integer         :: cap_sol              !index de la méthode de calcul de la capacité solide
   real(kind=long) :: f_mult               !facteur multiplicateur pour la loi de capacité solide
   character(len=3) :: LA1='', LA2=''      !lignes directrices de la largeur active
   integer         :: n_pas

   real(kind=long), allocatable :: Qs_total(:)

type compartiment_sedimentaire
   real(kind=long) :: m     !masse stockée dans le compartiment
   real(kind=long) :: d50   !diamètre médian de la courbe granulométrique représentative du compartiment
   real(kind=long) :: sigma !étendue granulométrique
end type


type hydraulic_state
   type(profil), pointer :: prof  !pointer sur le profil associé : la_topo%sections(is)
   real(kind=long) :: z           !cote de l'eau
   ! NOTE: les tableaux suivant sont dimensionnés à nzsup, car on sait d'avance que c'est la bonne taille (3 en fait)
   ! NOTE: pour l'indice 0 on a la valeur sur la section totale
   real(kind=long) :: q(0:nzsup)    !débits partiels par sous-section.
   ! NOTE: à voir si q(:) doit être remplacé par un debits_ISM
   real(kind=long) :: h(0:nzsup)    !profondeur d'eau par sous-section.
   real(kind=long) :: v(0:nzsup)    !vitesse moyenne par sous-section
   real(kind=long) :: rh(0:nzsup)   !rayon hydraulique par sous-section
   real(kind=long) :: jf(0:nzsup)   !pente de frottement par sous-section
end type


type CL_solide
   integer :: np                         !nombre de points du tableau %bc
   integer :: ip                         !indice courant dans le tableau %bc
   real(kind=long) :: d50                !diamètre médian du sédiment injecté
   real(kind=long) :: sigma              !étendue granulométrique du sédiment injecté
   type (point2D), allocatable :: bc(:)  !couples de points (temps, débit solide)
end type

interface assignment (=)
   module procedure clone_CS
end interface

interface operator(+)
   module procedure mixage
end interface


type(hydraulic_state), allocatable :: h_state(:)

type(CL_solide), allocatable, target :: allCLsolides(:)

type(compartiment_sedimentaire), allocatable :: CS_echange(:)


procedure(update_profil_uniforme), pointer :: update_profil => null()

procedure(shields_critique_0047), pointer :: shields_critique => null()

procedure(capacite_solide_MPM), pointer :: capacite_solide => null()

procedure(shields_brut), pointer :: shields_param => null()

contains

subroutine init_Param_Default_Charriage
   implicit none

   rhos = rhos_def
   phis = phis_def
   porosity = porosity_def
   a_Han = a_Han_def
   L_d = L_d_def
   L_s = L_s_def
   method_geo = method_geo_def
   shields = shields_def
   shields_correction = shields_cor_def
   cap_sol = cap_sol_def
   n_pas = 6
   f_mult = f_mult_def
   !update_profil => update_profil_uniforme
   !shields_critique => shields_critique_0047
   !capacite_solide => capacite_solide_MPM
   !shields_param => shields_effectif
!    dt_char = dtbase * 6._long

   call assign_pointer_functions()
end subroutine init_Param_Default_Charriage


subroutine clone_CS(c2,c1)
   !opération de clonage d'un compartiment sédimentaire -> affectation
   implicit none
   ! -- prototype --
   type(compartiment_sedimentaire), intent(in)  :: c1
   type(compartiment_sedimentaire), intent(out) :: c2
   ! -- variables locales --

   c2%m     = c1%m
   c2%d50   = c1%d50
   c2%sigma = c1%sigma

end subroutine clone_CS

function mixage(c1,c2) result(c3)
   !opération de mixage entre 2 compartiments sédimentaires
   implicit none
   ! -- prototype --
   type(compartiment_sedimentaire), intent(in) :: c1,c2
   type(compartiment_sedimentaire) :: c3
   real(kind=long) :: p1, p2
   ! -- variables locales --

   c3%m = c1%m + c2%m
   if (zegal(c2%m,c1%m,un)) then
      p1 = un
      p2 = zero
      !c3%d50 = c1%d50
      !c3%sigma = c1%sigma
   elseif (zegal(c1%m,c2%m,un)) then
      p1 = zero
      p2 = un
      !c3%d50 = c2%d50
      !c3%sigma = c2%sigma
   else
      p1 = c1%m/(c1%m+c2%m)
      p2 = c2%m/(c1%m+c2%m)
   endif
   c3%d50 = c1%d50**p1 * c2%d50**p2
   c3%sigma = c1%sigma**p1 * c2%sigma**p2

end function mixage


subroutine demixage(c1,M0,L_d,L_s,dx,c2,c3)
   !opération de démixage entre 2 compartiments sédimentaires
   implicit none
   ! -- prototype --
   type(compartiment_sedimentaire), intent(in) :: c1 !compartiment fournisseur
   real(kind=long), intent(in) :: M0   !masse  à extraire
   real(kind=long), intent(in) :: L_d  !distance de chargement pour le d50
   real(kind=long), intent(in) :: L_s  !distance de chargement pour sigma
   real(kind=long), intent(in) :: dx   !longueur du compartiment
   type(compartiment_sedimentaire), intent(out) :: c2 !compartiment expulsé (sédiments fins)
   type(compartiment_sedimentaire), intent(out) :: c3 !compartiment résiduel (sédiments grossiers)
   ! -- variables locales --
   real(kind=long) :: p, q

   if (l_d <= dx .or. l_s <= dx) then
      write(error_unit,'(a,/,a,/a,3(g0,3x))')  &
                             '>>>> ERREUR dans demixage() : les distances de chargement relatives au d50 et au sigma', &
                             '                              doivent être supérieures au pas d''espace.', &
                             '                              Valeurs actuelles : ', l_d, l_s, dx
      stop 1
   endif

   if (zegal(M0,zero,un)) then
      !la masse à extraire est négligeable
      c2 = c1  ;  c2%m = zero
      c3 = c1
   elseif (c1%m < M0 .or. zegal(c1%m,zero,un)) then
      !la masse disponible est inférieure à la masse à extraire ou négligeable
      c2 = c1
      c3 = c1 ; c3%m = zero
   else
      p = dx * (c1%sigma - un) / c1%sigma / c1%m
      q = -p / L_s
      p = p / L_d
      c2%m = M0
      c3%m = c1%m - M0

      c2%d50   = c1%d50 * exp(p*c3%m)
      c2%sigma = c1%sigma * exp(q*c3%m)

      c3%d50   = c1%d50 * exp(-p*c2%m)
      c3%sigma = c1%sigma * exp(-q*c2%m)
   endif
end subroutine demixage


subroutine extraction(c1,M0,c2,c3)
   !opération d'extraction entre 2 compartiments sédimentaires
   implicit none
   ! -- prototype --
   type(compartiment_sedimentaire), intent(in) :: c1 !compartiment fournisseur
   real(kind=long), intent(in) :: M0   !masse  à extraire
   type(compartiment_sedimentaire), intent(out) :: c2 !compartiment expulsé
   type(compartiment_sedimentaire), intent(out) :: c3 !compartiment résiduel

   c2 = c1
   c3 = c1
   if (zegal(M0,zero,un)) then
      !la masse à extraire est négligeable
      c2%m = zero
   elseif (c1%m < M0 .or. zegal(c1%m,zero,un)) then
      !la masse disponible est inférieure à la masse à extraire ou négligeable
      c3%m = zero
   else
      c2%m = M0
      c3%m = c1%m - M0
   endif
end subroutine extraction


function contrainte_locale(pts,Z,V_loc,K_skin,Rh_loc)
   implicit none
   ! -- Prototype --
   real(kind=long) :: contrainte_locale
   class(point3D), intent(in) :: pts
   real(kind=long), intent(in) :: Z, V_loc, K_skin, Rh_loc
   ! -- Variables locales --
   real(kind=long), parameter :: dtier = 2._long/3._long

   if (Z - pts%z > 0._long .and. Rh_loc > 0._long) then
      ! NOTE: on a Rh_loc ** 2/3 parce qu'il y a h ( = Z-pts%z) au numérateur
      contrainte_locale = rho * g * (Z - pts%z) * (V_loc / (K_skin * Rh_loc**dtier))**2
   else
      contrainte_locale = 0._long
   endif

end function contrainte_locale


function contrainte_moyenne(V, K, Rh)
   implicit none
   ! -- Prototype --
   real(kind=long) :: contrainte_moyenne
   real(kind=long), intent(in) :: V, K, Rh

   if (Rh > 0._long) then
      contrainte_moyenne = rho * g * (V / K)**2 / Rh**untier
   else
      contrainte_moyenne = 0._long
   endif

end function contrainte_moyenne


subroutine dump_contraintes_locales(filename)
!==============================================================================
!       Export de la contrainte locale sur le fichier filename
!
! Fait à l'instant courant en chaque point de chaque profil
!==============================================================================
   use hydraulique, only: qt,zt,vt,st,yt
   use StVenant_Debord, only: debord
   use data_num_mobiles, only: t
   use Mage_Utilitaires, only: heure
   implicit none
   ! -- Prototype --
   character(len=*) :: filename
   ! -- Variables locales --
   integer :: lw, ib, is, k
   type(point3D), pointer :: pts
   real(kind=long) :: k_skin, v_loc
   real(kind=long), parameter :: un6 = 1._long/6._long
   character(len=5) :: subsection
   real(kind=long) :: Rh_min, Rh_moy, P_correction, Rh_loc
   type (profil), pointer :: prof
   character(len=17) :: current_date
   real(kind=long) :: akmin, akmoy, Qmin, Qmoy, Vmin, Vmoy, Deb, beta, smin, smoy, pmin, pmoy

   current_date = heure(t)

   open(newunit=lw, file = trim(filename), form = 'formatted', status = 'unknown')
   write(output_unit,*) 'Export des contraintes locales au temps ',current_date,' dans le fichier ',trim(filename)
   write(lw,'(4a)') 'Export des contraintes locales au temps ',current_date,' dans le fichier ',trim(filename)
   write(lw,*)
   ! récupération de l'état hydraulique à t
   do ib = 1, la_topo%net%nb
      write(lw,'(a,i3.0)') '### Bief ',ib
      do is = la_topo%biefs(ib)%is1, la_topo%biefs(ib)%is2
         prof => la_topo%sections(is)
         write(lw,*) '* Profil ',prof%name,' au pk ',prof%pk
         write(lw,*) '* Ligne d''eau :  Q = ', qt(is), ' Z = ', zt(is), ' S = ', st(is), ' V = ',vt(is)
         write(lw,'(2a)') ' *       X                          Y                  contrainte locale            profondeur', &
                           '              Rayon hydraulique         Vitesse locale           Strickler de peau'
         akmin = prof%ks(prof%main)
         akmoy = prof%strickler_lit_moyen(zt(is))
         call debord(is,yt(is),qt(is),akmin,akmoy,Qmin, Qmoy, Vmin, Vmoy, Deb, beta)
         P_correction = 2._long*zt(is)-prof%xyz(prof%li(1))%z-prof%xyz(prof%li(2))%z
         smin = prof%section_mouillee(zt(is),2)
         smoy = prof%section_mouillee(zt(is),1) + prof%section_mouillee(zt(is),3)
         pmin = prof%perimetre(zt(is),2)
         pmoy = prof%perimetre(zt(is),1) + prof%perimetre(zt(is),3)
         Rh_min = smin/(pmin-P_correction)
         Rh_moy = smoy/(pmoy-P_correction)
         do k = 1, prof%np
            pts => prof%xyz(k)
            if (with_charriage == 1) then
               k_skin = strickler_peau(pts%cs(1)%d50)
            else
               k_skin = strickler_peau(prof%cs(1)%d50)
            endif
            if (k < prof%li(1)) then      ! le point k est dans le lit majeur gauche
               subsection = 'maj_G'
               V_loc = vmoy
               Rh_loc = Rh_moy
            else if (k <= prof%li(2)) then ! le point k est dans le lit mineur
               subsection = 'min__'
               V_loc = vmin
               Rh_loc = Rh_min
            else                               ! le point k est dans le lit majeur droit
               subsection = 'maj_D'
               V_loc = vmoy
               Rh_loc = Rh_moy
            endif
            write(lw,*) pts%x, pts%y, contrainte_locale(pts,zt(is),v_loc,k_skin,Rh_loc), max(zt(is)-pts%z,zero), &
                         Rh_loc, v_loc, k_skin, subsection
         enddo
      enddo
   enddo
   close(lw)
end subroutine dump_contraintes_locales


function strickler_peau(d50)
!==============================================================================
!      calcul du strickler à partir du d50
!
! Ce strickler représente le frottement pur, il est utilisé pour évaluer la
! contrainte au fond
!==============================================================================
   implicit none
   ! -- Prototype --
   real(kind=long) :: strickler_peau
   real(kind=long), intent(in) :: d50
   ! -- variables locales --
   real(kind=long), parameter :: un6 = 1._long/6._long

   strickler_peau = 21._long/d50**un6
end function strickler_peau


function settling_velocity(d)
!=====================================================================================
!   Vitesse de chute des particules selon la formule de Soulsby and Whitehouse (1997)
!=====================================================================================
   implicit none
   ! -- prototype --
   real(kind=long) :: settling_velocity
   real(kind=long), intent(in) :: d   !diamètre
   ! -- variables locales
   real(kind=long) :: alpha
   real(kind=long), parameter :: a = 10.36_long, b = 1.049_long

   alpha = ((rhos - rho)*gnu2)**untier

   settling_velocity = nu / d * (sqrt(a*a + b*(alpha*d)**3) - a)
end function settling_velocity


subroutine bilan_sedimentaire(is, lit, CS_in, CS_out, CS_active, CS_substrat, CS_subsubstrat, &
                              CS_nouvelle, L_d, L_s, dt, a_Han, dx, coeff_ouv)
!=====================================================================================
!  calcul du bilan sédimentaire selon la méthode des compartiments
!  (equivalent cacose Rubarbe)
!=====================================================================================
   implicit none
   ! -- prototype --
   integer, intent(in) :: is     !profil central de la maille sédimentaire
   integer, intent(in) :: lit    !n° du lit (sous-section) ; si 0 : section totale
   type(compartiment_sedimentaire), intent(in)    :: CS_in       !compartiment amont (entrée)
   type(compartiment_sedimentaire), intent(out)   :: CS_out      !compartiment aval (sortie)
   type(compartiment_sedimentaire), intent(inout) :: CS_active   !couche active
   type(compartiment_sedimentaire), intent(inout) :: CS_substrat !couche sous la couche active
   type(compartiment_sedimentaire), intent(inout) :: CS_subsubstrat
   type(compartiment_sedimentaire), intent(inout) :: CS_nouvelle
   real(kind=long), intent(in) :: L_d      !distance de chargement relative à l’évolution du d50
   real(kind=long), intent(in) :: L_s      !distance de chargement relative à l’évolution du sigma
   real(kind=long), intent(in) :: a_Han    !distance de Han
   real(kind=long), intent(in) :: dt       !pas de temps
   real(kind=long), intent(in) :: dx       !longueur de la maille sédimentaire
   real(kind=long), intent(in) :: coeff_ouv !coefficient d'abattement dû à la présence d'un ouvrage
                                            !sert à bloquer tout ou partie du charriage (transit et érosion)
                                            !quand il y a un obstacle (seuil ou vanne de fond)
   ! -- variables locales
   real(kind=long) :: M_ero    !masse solide érodée
   real(kind=long) :: La       !distance de chargement
   real(kind=long) :: amorti   !coefficient d'amortissement fonction de la distance de chargement
   type(compartiment_sedimentaire) :: CS_dep, CS_ero, CS_res, CS_tra
   type(profil), pointer :: prfl !profil courant
   real(kind=long) :: macind   !masse solide indicative de l'intensité d'interaction lit-écoulement (sert à régler la masse du compartiment CS_active) [kg]
   real(kind=long) :: mbadem !masse du compartiment CS_substrat demandée
   real(kind=long) :: mbaobt !masse du compartiment CS_substrat obtenue (=disponible)

   prfl => la_topo%sections(is)
   La = distance_chargement(is, lit, CS_substrat%d50, a_Han)
   if (La < 0.001_long) then
      amorti = 0._long
   else
      amorti = exp(-dx/La) * coeff_ouv
   endif
   ! dépôt et transit : séparation du compartiment d'entrée entre transit (fins) et dépôt (grossiers)
   call demixage(CS_in,CS_in%m*amorti,L_d,L_s,dx,CS_tra,CS_dep)
   CS_active = CS_active + CS_dep !ajout du compartiment dépôt à la couche active
   ! érosion
!======== test =======!
!    M_ero = capacite_solide(is, lit, CS_active%d50, CS_active%sigma) * largeur_charriage(is,lit) * dt * (1._long-amorti) !masse érodée
! !    M_ero = capacite_solide(is, lit, CS_active%d50, CS_active%sigma) * prfl%largeur(h_state(is)%z,lit) * dt * (1._long-amorti) !masse érodée
!    M_ero = f_mult * M_ero * coeff_ouv
!    call demixage(CS_active,M_ero,L_d,L_s,dx,CS_ero,CS_res) !extraction du compartiment érosion de la couche active
!
!    CS_active = CS_res ! la couche active devient ce qui reste après érosion
!    CS_out = CS_tra + CS_ero  !ce qui sort est la somme (mixage) des compartiments transit et érosion
!======== test =======!

  ! Fin: évaluation de l'interaction lit-écoulement
  !-----------------------------------------------------------------------
!    macind = capacite_solide(is, lit, CS_active%d50, CS_active%sigma) * prfl%largeur(h_state(is)%z,lit) * dt * (1._long-amorti) !masse érodée
   macind = capacite_solide(is, lit, CS_active%d50, CS_active%sigma) * largeur_charriage(is,lit) * dt * (1._long-amorti) !masse érodée

   if (CS_active%m.lt.(1.-SENSI)*macind) then ! erosion
      MBADEM=macind-CS_active%m
      MBADEM = f_mult * MBADEM * coeff_ouv
          if((MBADEM.le.0.).or.(MACIND.lt.0.).or.(CS_active%m.lt.0.))then
            WRITE(*,*)'PROBLEME bilan_sedimentaire erosion MBADEM=',MBADEM,'MACIND='&
              ,MACIND,' CS_active%m=',CS_active%m
            stop
          endif
!       call extraction(CS_substrat,MBADEM,CS_ero,CS_res)
      call demixage(CS_substrat,MBADEM,L_d,L_s,dx,CS_ero,CS_res)
      CS_active = CS_active + CS_ero
      CS_substrat = CS_res
      if (CS_active%m.lt.(1.-SENSI)*macind) then ! on attaque la couche suivante
         MBADEM=CS_active%m-macind
         call demixage(CS_subsubstrat,MBADEM,L_d,L_s,dx,CS_ero,CS_res)
         CS_active = CS_active + CS_ero
         CS_subsubstrat = CS_res
      endif ! on se limite a la couche suivante
   elseif (CS_active%m.gt.(1.+SENSI)*macind) then ! depot
     MBADEM=CS_active%m-macind
     call extraction(CS_active,MBADEM,CS_dep,CS_res)
     CS_active = CS_res
     if (proche(CS_substrat, CS_dep)) then
        CS_substrat = CS_substrat + CS_dep
     else
        CS_nouvelle = CS_dep
     endif
   endif

!======== test =======!
   CS_out = CS_tra + CS_active  !ce qui sort est la somme (mixage) des compartiments transit et échange
   CS_active%m = 0.0
!======== test =======!

end subroutine bilan_sedimentaire


function distance_chargement(is, lit, d, a_Han)
!=====================================================================================
!  Calcul de la distance de chargement
!=====================================================================================
   implicit none
   ! -- prototype --
   real(kind=long)             :: distance_chargement
   integer, intent(in)         :: is    !profil central de la maille sédimentaire
   integer, intent(in)         :: lit   !n° du lit (sous-section) ; si 0 : section totale
   real(kind=long), intent(in) :: d     !diamètre du sédiment
   real(kind=long), intent(in) :: a_Han !distance de Han
   ! -- variables locales --
   real(kind=long) :: V        !vitesse moyenne
   real(kind=long) :: K        !Strickler
   real(kind=long) :: Rh       !rayon hydraulique
   real(kind=long) :: V_frott  !vitesse de frottement

   if (a_Han < 0.001_long) then
      distance_chargement = 0._long
   else
      V  = h_state(is)%v(lit)
      Rh = h_state(is)%Rh(lit)
      K  = la_topo%sections(is)%Ks(lit)
      V_frott = sqrt(contrainte_moyenne(V,K,Rh)/rho)
      distance_chargement = a_Han * V_frott / settling_velocity(d)
   endif

end function distance_chargement


function diametre_representatif(d50, sigma) result(D)
!=====================================================================================
!  Estimation du diamètre représentatif pour les formules de capacité solide
!  source : thèse de Kamal ElKadi : I.6.i page 34 (formule de Wu et al., 2004)
!=====================================================================================
   implicit none
   ! -- prototype --
   real(kind=long), intent(in) :: d50    !diamètre caractéristique du sédiment
   real(kind=long), intent(in) :: sigma  !étendue granulométrique du sédiment
   real(kind=long) :: D

   D = d50 * exp(-0.5_long*log(sigma))
end function diametre_representatif


function capacite_solide_MPM(is, lit, d50, sigma) result(Qs)
!=====================================================================================
!  Calcul de la capacité solide en kg/s/m -- loi de Meyer-Peter & Muller
!=====================================================================================
   implicit none
   ! -- prototype --
   real(kind=long)             :: Qs     !débit solide par unité de largeur
   integer, intent(in)         :: is     !profil central de la maille sédimentaire
   integer, intent(in)         :: lit    !n° du lit (sous-section) ; si 0 : section totale
   real(kind=long), intent(in) :: d50    !diamètre caractéristique du sédiment
   real(kind=long), intent(in) :: sigma  !étendue granulométrique du sédiment
   ! -- variables locales --
   real(kind=long), parameter :: A = 8._long, B = 1.5_long
   real(kind=long) :: s_1, theta, theta_cr, D

   s_1 = rhos/rho - un
   D = diametre_representatif(d50,sigma)
   theta = shields_param(is,lit,D)
   theta_cr = shields_critique(is,lit,D)

   if (theta > theta_cr) then
      Qs = rhos * A * sqrt(s_1 * g * D**3) * (theta - theta_cr)**B
   else
      Qs = zero
   endif
end function capacite_solide_MPM


function capacite_solide_CL(is, lit, d50, sigma) result(Qs)
!=====================================================================================
!  Calcul de la capacité solide en kg/s/m -- loi de Camenen & Larson
!=====================================================================================
   implicit none
   ! -- prototype --
   real(kind=long)             :: Qs     !débit solide par unité de largeur
   integer, intent(in)         :: is     !profil central de la maille sédimentaire
   integer, intent(in)         :: lit    !n° du lit (sous-section) ; si 0 : section totale
   real(kind=long), intent(in) :: d50    !diamètre caractéristique du sédiment
   real(kind=long), intent(in) :: sigma  !étendue granulométrique du sédiment
   ! -- variables locales --
   real(kind=long), parameter :: A = 12._long, B = 1.5_long, C = 4.5_long
   real(kind=long) :: s_1, theta, theta_cr, D

   s_1 = rhos/rho - un
   D = diametre_representatif(d50,sigma)
   theta = shields_param(is,lit,D)
   theta_cr  = shields_critique(is,lit,D)

   Qs = rhos * A * sqrt(s_1 * g * D**3) * theta**B * exp(-C*theta_cr/theta)

end function capacite_solide_CL


function capacite_solide_EH(is, lit, d50, sigma) result(Qs)
!=====================================================================================
!  Calcul de la capacité solide en kg/s/m -- loi de Engelund & Hansen
!=====================================================================================
   implicit none
   ! -- prototype --
   real(kind=long)             :: Qs     !débit solide par unité de largeur
   integer, intent(in)         :: is     !profil central de la maille sédimentaire
   integer, intent(in)         :: lit    !n° du lit (sous-section) ; si 0 : section totale
   real(kind=long), intent(in) :: d50    !diamètre caractéristique du sédiment
   real(kind=long), intent(in) :: sigma  !étendue granulométrique du sédiment
   ! -- variables locales --
   real(kind=long), parameter :: A = 0.05_long, B = 1.5_long
   real(kind=long) :: s_1, V, D, theta

   s_1 = rhos/rho - un
   D = diametre_representatif(d50,sigma)
   theta = shields_param(is,lit,D)
   V = h_state(is)%v(lit)

   Qs = rhos * A * V * V * sqrt(D/(g*s_1)) * theta**B
end function capacite_solide_EH

function capacite_solide_Reck(is, lit, d50, sigma) result(Qs)
!=====================================================================================
!  Calcul de la capacité solide en kg/s/m -- loi de Recking
!=====================================================================================
   implicit none
   ! -- prototype --
   real(kind=long)             :: Qs     !débit solide par unité de largeur
   integer, intent(in)         :: is     !profil central de la maille sédimentaire
   integer, intent(in)         :: lit    !n° du lit (sous-section) ; si 0 : section totale
   real(kind=long), intent(in) :: d50    !diamètre caractéristique du sédiment
   real(kind=long), intent(in) :: sigma  !étendue granulométrique du sédiment
   ! -- variables locales --
   real(kind=long), parameter :: A = 14._long, B = 2.5_long, C = 4._long
   real(kind=long) :: s_1, theta_m, theta_84, d84
   real(kind=long) :: pente
   integer :: ib

   ib = numero_bief(is)

   ! calcul de la pente
   if (is == la_topo%biefs(ib)%is1) then
      pente = (zfd(is) - zfd(is+1)) / abs(xgeo(is+1) - xgeo(is))
   elseif (is == la_topo%biefs(ib)%is2) then
      pente = (zfd(is-1) - zfd(is)) / abs(xgeo(is) - xgeo(is-1))
   else
      pente = (zfd(is-1) - zfd(is+1)) / abs(xgeo(is+1) - xgeo(is-1))
   endif

   s_1 = rhos/rho - un
   d84 = 2.1_long*d50
   theta_84 = shields_param(is,lit,d84)
!   theta_84 = theta*(d50/d84)

   ! calcul de theta_m
   if (d50 >= 0.001_long) then
	  theta_m = (5*abs(pente) + 0.06_long)*(d84/d50)**(4.4_long*sqrt(Abs(pente))-1.5_long)
   else
	  theta_m = 0.045_long
   endif

   Qs = rhos * A * sqrt(s_1 * g * d84**3) * theta_84**B /(1+(theta_m/theta_84)**C)

end function capacite_solide_Reck


subroutine current_hydraulic_state(bFail)
!=====================================================================================
!  Collecte les infos sur l'état hydraulique à la date actuelle qui sont utiles
!  pour la simulation du charriage
!=====================================================================================
   use data_num_fixes, only: fp_model
   use data_num_mobiles, only: ISM_calcul_OK
   use hydraulique, only: qt, zt
   use StVenant_ISM, only: q_ism => q, z_ism => z, Q_total
   use StVenant_Debord, only: debord
   implicit none
   ! -- prototype --
   logical, intent(out) :: bFail !faux si on arrive au bout sans erreur
   ! -- variables locales
   integer :: is, k
   real(kind=long) :: qmin, qmoy, vmin, vmoy, deb, beta, akmin, akmoy, zz, s0
   type(profil), pointer :: prfl

   if (.not.allocated(h_state)) allocate(h_state(la_topo%net%ns))

   if (fp_model == i_debord .OR. ((fp_model == i_mixte .or. fp_model == i_mixte2) .and. .not.ISM_calcul_OK)) then
      !Debord OU mixte + échec ISM avec bascule sur Debord
      do is = 1, la_topo%net%ns  !les sections sont dans l'ordre des données
         prfl => la_topo%sections(is)
         h_state(is)%z = zt(is)
         akmin = prfl%ks(prfl%main)
         akmoy = prfl%strickler_lit_moyen(zt(is))
         s0 = prfl%section_mouillee(zt(is),0)
         call debord(is, zt(is)-zfd(is), qt(is), akmin, akmoy, Qmin, Qmoy, Vmin, Vmoy, Deb, beta)
         ! profondeurs locales
         h_state(is)%h(0) = zt(is) - prfl%zf
         h_state(is)%h(1) = max(zero,zt(is) - prfl%xyz(prfl%li(1))%z)
         h_state(is)%h(2) = zt(is) - prfl%zf
         h_state(is)%h(3) = max(zero,zt(is) - prfl%xyz(prfl%li(2))%z)
         ! section totale
         h_state(is)%q(0) = qt(is)
         h_state(is)%v(0) = qt(is) / prfl%section_mouillee(zt(is),0)
         ! lit mineur
         h_state(is)%q(2) = qt(is) - Qmoy
         h_state(is)%v(2) = Vmin
         ! lit majeur gauche
         if (h_state(is)%h(1) > 0.001_long) then
            h_state(is)%v(1) = Vmoy
            h_state(is)%q(1) = Qmoy * prfl%section_mouillee(zt(is),1) / s0
         else
            h_state(is)%v(1) = zero
            h_state(is)%q(1) = zero
         endif
         ! lit majeur droit
         if (h_state(is)%h(3) > 0.001_long) then
            h_state(is)%v(3) = Vmoy
            h_state(is)%q(3) = Qmoy * prfl%section_mouillee(zt(is),3) / s0
         else
            h_state(is)%v(3) = zero
            h_state(is)%q(3) = zero
         endif
      enddo
   else
      !ISM réussi
      do is = 1, la_topo%net%ns  !les sections sont dans l'ordre des données
         prfl => la_topo%sections(is)
         h_state(is)%z = z_ism(is)
         ! débits locaux
         h_state(is)%q(0) = Q_total(q_ism(is))
         h_state(is)%q(1) = q_ism(is)%ql
         h_state(is)%q(2) = q_ism(is)%qm
         h_state(is)%q(3) = q_ism(is)%qr
         ! vitesses locales
         h_state(is)%v(0) = h_state(is)%q(0) / prfl%section_mouillee(z_ism(is),0)
         if (prfl%section_mouillee(z_ism(is),1) > 0.01_long) then
            h_state(is)%v(1) = q_ism(is)%ql/prfl%section_mouillee(z_ism(is),1)
         else
            h_state(is)%v(1) = zero
         endif
         h_state(is)%v(2) = q_ism(is)%qm/prfl%section_mouillee(z_ism(is),2)
         if (prfl%section_mouillee(z_ism(is),3) > 0.01_long) then
            h_state(is)%v(3) = q_ism(is)%qr/prfl%section_mouillee(z_ism(is),3)
         else
            h_state(is)%v(3) = zero
         endif
         ! profondeurs locales
         h_state(is)%h(0) = z_ism(is) - prfl%zf
         h_state(is)%h(1) = max(zero,z_ism(is) - prfl%xyz(prfl%li(1))%z)
         h_state(is)%h(2) = z_ism(is) - prfl%zf
         h_state(is)%h(3) = max(zero,z_ism(is) - prfl%xyz(prfl%li(2))%z)
      enddo
   endif
   ! rayons hydrauliques et pentes de frottement
   do is = 1, la_topo%net%ns
      prfl => la_topo%sections(is)
      zz = h_state(is)%z
      h_state(is)%rh(0) = prfl%section_mouillee(zz,0) / prfl%perimetre(zz,0)
      ! NOTE: un profil n'a pas de strickler global ; faute de mieux, on prend celui du lit mineur.
      h_state(is)%jf(0) =  (h_state(is)%v(0) / prfl%Ks(prfl%main) / (h_state(is)%rh(0)**deuxtiers))**2
      do k = 1, prfl%nzone
         if (h_state(is)%h(k) > 0.001_long) then
            h_state(is)%rh(k) = prfl%section_mouillee(zz,k) / prfl%perimetre(zz,k)
            !h_state(is)%rh(k) = h_state(is)%h(k)
            h_state(is)%jf(k) =  (h_state(is)%v(k) / prfl%Ks(k) / (h_state(is)%rh(k)**deuxtiers))**2
         else
            h_state(is)%rh(k) = zero
            h_state(is)%jf(k) = zero
         endif
      enddo
   enddo
   bFail = .false.
end subroutine current_hydraulic_state


subroutine update_charriage(t, dt)
! Mise à jour du charriage
   implicit none
   ! -- prototype --
   real(kind=long), intent(in) :: t, dt
   ! -- variables locales --
   logical :: bFail
   integer :: ib, is, lit, kb, nb, ka, i
   type(profil), pointer :: prfl
   type(compartiment_sedimentaire) :: CS_in             !compartiment amont (entrée)
   type(compartiment_sedimentaire) :: CS_out            !compartiment aval (sortie)
   type(compartiment_sedimentaire) :: CS_substrat       !couche sous la couche active
   type(compartiment_sedimentaire) :: CS_echange_0      !couche active au début du pas de temps
   type(compartiment_sedimentaire) :: CS_substrat_0     !couche surface au début du pas de temps
   type(compartiment_sedimentaire) :: CS_subsubstrat    !couche sous la couche sous la couche active
   type(compartiment_sedimentaire) :: CS_subsubstrat_0  !couche sub-surface au début du pas de temps
   type(compartiment_sedimentaire) :: CS_nouvelle       !nouvelle couche éventuelle en fin de pas de temps
   type(compartiment_sedimentaire), allocatable :: CS_node(:) !assemblage des CS sortant au niveau des nœuds
   real(kind=long) :: dx          !pas d'espace
   character(len=15) :: mydate
   character(len=17) :: header
   integer :: ith, ns, k, nk
   real(kind=long) :: coeff_ouv     !coefficient d'abattement dû à la présence d'un ouvrage
                                    !sert à bloquer tout ou partie du charriage (transit et érosion)
                                    !quand il y a un obstacle (seuil ou vanne de fond)
   real(kind=long) :: z_vanne, s_vanne, section_totale

   call current_hydraulic_state(bFail)
   if (bFail) stop '>>>> ERREUR dans current_hydraulic_state() pour le charriage'

   ! parcours de l'ensemble du réseau selon le rang des biefs
   allocate (CS_node(1:la_topo%net%nn))
   do kb = 1, la_topo%net%nb
      ib = la_topo%net%numero(kb)
      nb = la_topo%biefs(ib)%nam
      if (la_topo%nodes(nb)%cl > 0) then
         !nœud amont -> il faut une CL solide ; sinon CS_node(nb) est défini par les apports des biefs amonts
         CS_node(nb) = get_CL_solide(nb,t,dt)
      endif

      is = la_topo%biefs(ib)%is1
      CS_out = CS_node(nb)

      do is = la_topo%biefs(ib)%is1, la_topo%biefs(ib)%is2
         if (is == la_topo%biefs(ib)%is1) then
            dx = abs(xgeo(is+1) - xgeo(is)) * 0.5_long
         elseif (is == la_topo%biefs(ib)%is2) then
            dx = abs(xgeo(is) - xgeo(is-1)) * 0.5_long
         else
            dx = abs(xgeo(is+1) - xgeo(is-1)) * 0.5_long
         endif

         if (is < la_topo%biefs(ib)%is2) then
            prfl => la_topo%sections(is+1)
            if (prfl%iss > 0) then
               !il y a un ouvrage entre prfl et le profil amont
               !si l'ouvrage est de type vanne de fond, le débit solide peut passer
               !si l'ouvrage est de type seuil, le débit solide ne passe pas.
               coeff_ouv = zero
               ns = prfl%iss
               do k = 1, all_OuvCmp(ns)%ne
                  nk = all_OuvCmp(ns)%OuEl(k,1)
                  if (all_OuvEle(nk)%iuv == 2 .or. all_OuvEle(nk)%iuv == 4) then
                     !vanne de fond
                     if (all_OuvEle(nk)%uv3 > 0.001_long) then   !la vanne est ouverte
                        !cote de mise en charge de la vanne
                        z_vanne = all_OuvEle(nk)%uv2 + all_OuvEle(nk)%uv3
                        !section d'écoulement de la vanne
                        s_vanne = all_OuvEle(nk)%uv1 * all_OuvEle(nk)%uv3
                        !on calcule la section d'écoulement à la cote de mise en charge de la vanne
                        section_totale = prfl%section_mouillee(z_vanne,0)
                        coeff_ouv = coeff_ouv + s_vanne / section_totale
                        !print*,xgeo(is), coeff_ouv, s_vanne, section_totale
                     else                                         ! la vanne est fermée
                        cycle
                     endif
                  elseif (all_OuvEle(nk)%iuv == 3 .or. all_OuvEle(nk)%iuv > 90) then
                     coeff_ouv = un
                  else
                     coeff_ouv = un
                  endif
               enddo
               coeff_ouv = min(un,coeff_ouv)
            else
               coeff_ouv = un
            endif
         endif

         if (zegal(h_state(is)%q(0), zero, un)) cycle !pas de débit dans la section -> on passe
         CS_echange_0 = CS_echange(is)
         CS_substrat = get_compartiment(is,0,1,dx)
         CS_substrat_0 = CS_substrat
         CS_nouvelle = CS_substrat
         CS_nouvelle%m = 0.0
         CS_subsubstrat = get_compartiment(is,0,2,dx)
         CS_subsubstrat_0 = CS_subsubstrat
         CS_in = CS_out
         ! mise à jour de la couche active et du substrat
         call bilan_sedimentaire(is, 0, CS_in, CS_out, CS_echange(is), CS_substrat, &
                                 CS_subsubstrat, CS_nouvelle, L_d, L_s, dt, a_Han, dx, coeff_ouv)
         Qs_total(is) = CS_out%m / dt
         ! mise à jour de la géométrie du profil
         call update_profil(is, 0, CS_substrat_0, CS_substrat, &
                            CS_subsubstrat_0, CS_subsubstrat, CS_nouvelle, dx)
         !if (prfl%iss > 0) print*,xgeo(is), lit, coeff_ouv, Qs_total(is)
      enddo
      if (la_topo%nodes(nb)%cl > 0) then
         !nœud amont -> il faut corriger le débit solide qui est imposé par la C.L.
         Qs_total(la_topo%biefs(ib)%is1) = CS_node(nb)%m / dt
      endif
      !mise à jour du compartiment de sortie au nœud aval -- on mélange toutes les sous-sections (mixage)
      nb = la_topo%biefs(ib)%nav
      CS_node(nb) = CS_out
   enddo
   if (t > ttra-dtbase) then
      mydate = heure(t)
      ka = scan(mydate,'+-0123456789') !on ne veut pas les blancs du début
      if (tmax > 864000._long) then
         if (dttra > 86399._long) then !affichage en jours entiers
            kb = scan(mydate,':') - 1  !on ne garde que le 1er champ, celui des jours
            header = mydate(ka:kb)//'_j'
         else                          !affichage en jours décimaux
            write(header,'(a,a2)') trim(f0p(t/86400._long,2)),'_j'
         endif
      else                             !affichage en heures
         ith = floor(t / 3600._long)
         write(header,'(i0,a2)') ith,'_h'
      endif
      if (btmFile /= '') call export_bottom_data(header)
      if (qstFile /= '') call export_Qsolid_data(header)
      do i = 1, len_trim(mydate)
         if (mydate(i:i) == ':') mydate(i:i) = '-'
      enddo
      call export_ST_final('_'//mydate(ka:))
   endif

end subroutine update_charriage


function shields_brut(is,lit,d50) result(theta)
! renvoie la valeur du paramètre de Shields
   implicit none
   ! -- prototype --
   integer, intent(in)         :: is  !profil où on veut le paramètre de Shields
   integer, intent(in)         :: lit !sous-section du profil is dans laquelle on veut le paramètre de Shields ; si 0 : section totale
   real(kind=long), intent(in) :: d50 !diamètre caractéristique du sédiment
   real(kind=long)             :: theta

   !paramètre de Shields
   theta = h_state(is)%rh(lit) * h_state(is)%jf(lit) / ((rhos/rho - un) * d50)

end function shields_brut


function shields_effectif(is,lit,d50) result(theta)
! renvoie la valeur du paramètre de Shields effectif
   implicit none
   ! -- prototype --
   integer, intent(in)         :: is  !profil où on veut le paramètre de Shields
   integer, intent(in)         :: lit !sous-section du profil is dans laquelle on veut le paramètre de Shields ; si 0 : section totale
   real(kind=long), intent(in) :: d50 !diamètre caractéristique du sédiment
   real(kind=long)             :: theta
   ! -- variables locales --
   type(profil), pointer :: prfl

   !paramètre de Shields
   theta = shields_brut(is,lit,d50)

   !paramètre de Shields effectif
   if (lit > 0) then
      theta = theta * (la_topo%sections(is)%Ks(lit) / strickler_peau(d50))**1.5_long
   else
      prfl => la_topo%sections(is)
      ! NOTE: un profil n'a pas de strickler global ; faute de mieux, on prend celui du lit mineur.
      theta = theta * (prfl%Ks(prfl%main) / strickler_peau(d50))**1.5_long
   endif
end function shields_effectif


function shields_critique_0047(is,lit,d50) result(theta_cr)
! renvoie la valeur du paramètre de Shields critique (seuil de mise en mouvement)
   implicit none
   ! -- prototype --
   integer, intent(in)         :: is  !profil où on veut le paramètre de Shields
   integer, intent(in)         :: lit !sous-section du profil is dans laquelle on veut le paramètre de Shields ; si 0 : section totale
   real(kind=long), intent(in) :: d50 !diamètre caractéristique du sédiment
   real(kind=long)             :: theta_cr
   ! -- variables locales --
   real(kind=long)             :: tau_cr
   integer                     :: k, k1, k2
   type(profil), pointer       :: prfl
   type(point3D), pointer      :: xyz

   prfl => la_topo%sections(is)
   ! tau critique moyen sur la sous-section
   if (with_charriage == 1) then
      tau_cr = zero
      if (lit == 0) then
        k1 = 1
        k2 = prfl%np
      else
        k1 = prfl%li(lit-1)
        k2 = prfl%li(lit)
      endif
      do k = k1, k2
         xyz => prfl%xyz(k)
         tau_cr = tau_cr + xyz%cs(1)%tau
      enddo
      tau_cr = tau_cr / real(k2-k1+1,kind=long)
   elseif (with_charriage >= 2) then
      tau_cr = prfl%cs(1)%tau
   endif
   if (tau_cr > zero) then
      theta_cr = tau_cr
   else
      theta_cr = 0.047_long
   endif
end function shields_critique_0047


function shields_critique_Soulsby(is,lit,d50) result(theta_cr)
! renvoie la valeur du paramètre de Shields critique (seuil de mise en mouvement)
   implicit none
   ! -- prototype --
   integer, intent(in)         :: is  !profil où on veut le paramètre de Shields
   integer, intent(in)         :: lit !sous-section du profil is dans laquelle on veut le paramètre de Shields
   real(kind=long), intent(in) :: d50 !diamètre caractéristique du sédiment
   real(kind=long)             :: theta_cr
   ! -- variables locales --
   real(kind=long) :: d_star

   d_star = (g*(rhos/rho - un)/nu**2)**untier * d50
   theta_cr = 0.3_long / (un + 1.2_long*d_star) + 0.055_long*(un - exp(-0.02_long*d_star))
end function shields_critique_Soulsby


function shields_critique_Camenen(is,lit,d50) result(theta_cr)
! renvoie la valeur du paramètre de Shields critique (seuil de mise en mouvement)
   implicit none
   ! -- prototype --
   integer, intent(in)         :: is  !profil où on veut le paramètre de Shields
   integer, intent(in)         :: lit !sous-section du profil is dans laquelle on veut le paramètre de Shields
   real(kind=long), intent(in) :: d50 !diamètre caractéristique du sédiment
   real(kind=long)             :: theta_cr
   ! -- variables locales --
   real(kind=long), parameter :: phi_s = 40._long / 360._long * 2._long * PI !angle de repos des sédiments
   real(kind=long) :: pente
   integer :: ib

   ib = numero_bief(is)
   if (is == la_topo%biefs(ib)%is1) then
      pente = (zfd(is) - zfd(is+1)) / abs(xgeo(is+1) - xgeo(is))
   elseif (is == la_topo%biefs(ib)%is2) then
      pente = (zfd(is-1) - zfd(is)) / abs(xgeo(is) - xgeo(is-1))
   else
      pente = (zfd(is-1) - zfd(is+1)) / abs(xgeo(is+1) - xgeo(is-1))
   endif
   theta_cr = shields_critique_Soulsby(is,lit,d50)
   if (pente > 0._long) then
      theta_cr = theta_cr * sin(phi_s - atan(pente)) / sin(phi_s) * (0.5_long + 6._long*pente**0.75_long)
   else
      theta_cr = 0.5_long * theta_cr
   endif
end function shields_critique_Camenen


subroutine update_profil_uniforme(is, lit, CS_substrat_0, CS_substrat_1, &
                                  CS_subsubstrat_0, CS_subsubstrat_1, CS_nouvelle, dx)
! mise à jour de la forme du profil sous l'effet du charriage
   implicit none
   ! -- prototype --
   integer, intent(in)                         :: is             !index du profil à mettre à jour
   integer, intent(in)                         :: lit            !sous-section à mettre à jour
   type(compartiment_sedimentaire), intent(in) :: CS_substrat_0  !couche substrat au début du pas de temps
   type(compartiment_sedimentaire), intent(in) :: CS_substrat_1  !couche substrat à la fin du pas de temps
   type(compartiment_sedimentaire), intent(in) :: CS_subsubstrat_0  !couche sub-substrat au début du pas de temps
   type(compartiment_sedimentaire), intent(in) :: CS_subsubstrat_1  !couche sub-substrat à la fin du pas de temps
   type(compartiment_sedimentaire), intent(in) :: CS_nouvelle    !nouvelle couche éventuelle à la fin du pas de temps
   real(kind=long), intent(in)                 :: dx             !longueur du compartiment sédimentaire
   ! -- variables locales --
   integer :: k, k1, k2, j, li1, li2, i
   type(profil), pointer :: prfl !profil à mettre à jour
   real(kind=long) :: delta_m_subsub, delta_m_sub    !variation de masse d'une couche sédimentaire ; > 0 si dépôt, < 0 si érosion
   real(kind=long) :: h_subsub, h_sub ,h_nouv               !épaisseur d'une couche sédimentaire
   real(kind=long) :: delta_h_subsub, delta_h_sub, delta_h_nouv    !variation d'épaisseur d'une couche sédimentaire (active et substrat)
   real(kind=long) :: z, zmin
   real(kind=long) :: L          !min entre largeur active et largeur au miroir
   type(couche_sed_section), dimension(:), allocatable :: compartiments_tmp
   type(couche_sedimentaire), dimension(:), allocatable :: couches_tmp
   type(Point3D), pointer :: xyz

   prfl => la_topo%sections(is)
   delta_m_sub = CS_substrat_1%m - CS_substrat_0%m
   delta_m_subsub = CS_subsubstrat_1%m - CS_subsubstrat_0%m
   !on convertit la variation de masse en variation d'épaisseur
   L = largeur_charriage(is, lit)
   delta_h_sub    = delta_m_sub/ (rhos * (1._long-porosity) * dx * L)
   delta_h_subsub = delta_m_subsub / (rhos * (1._long-porosity) * dx * L)
   h_nouv   = CS_nouvelle%m / (rhos * (1._long-porosity) * dx * L)
   h_sub    = CS_substrat_1%m / (rhos * (1._long-porosity) * dx * L)
   h_subsub = CS_subsubstrat_1%m / (rhos * (1._long-porosity) * dx * L)

   !correction du profil
   if (with_charriage == 1) then
      if (lit == 0)then
         k1 = prfl%kfg+1
         k2 = prfl%kfd-1
      else
         k1 = prfl%li(lit-1)
         k2 = prfl%li(lit)-1
      endif
      do k = k1, k2
         if (k <= prfl%kfg .or. k >= prfl%kfd) cycle  !les points kfg et kfd ne bougent pas
         if (prfl%xyz(k)%z > h_state(is)%z) cycle !on ne modifie que les points qui sont sous l'eau
         xyz => prfl%xyz(k)
         if (CS_nouvelle%m > 0.0) then ! nouvelle couche
            allocate(couches_tmp(xyz%nbcs+1))
            do i = 1, xyz%nbcs
               couches_tmp(i+1) = xyz%cs(i)
            enddo
            call move_alloc(couches_tmp, xyz%cs)
            xyz%cs(1)%zc = xyz%cs(2)%zc + h_sub
            xyz%cs(1)%d50 = CS_nouvelle%d50
            xyz%cs(1)%sigma = CS_nouvelle%sigma
            xyz%cs(1)%tau = xyz%cs(2)%tau
            xyz%z = xyz%z + h_nouv
            xyz%nbcs = xyz%nbcs + 1
         elseif (CS_substrat_1%m <= 0.0) then ! on enlève une couche
            if (xyz%nbcs > 1) then
               allocate(couches_tmp(xyz%nbcs-1))
               do i = 1, xyz%nbcs-1
                  couches_tmp(i) = xyz%cs(i+1)
               enddo
               call move_alloc(couches_tmp, xyz%cs)
               xyz%nbcs = xyz%nbcs - 1
               xyz%cs(1)%d50 = CS_subsubstrat_1%d50
               xyz%cs(1)%sigma = CS_subsubstrat_1%sigma
               xyz%z = xyz%cs(1)%zc + h_subsub
            elseif (xyz%nbcs == 1) then ! on garde une couche vide
               xyz%z = xyz%cs(1)%zc ! on a atteint le fond
            endif
         else
            xyz%cs(1)%zc = xyz%cs(1)%zc + delta_h_sub
            xyz%z = xyz%cs(1)%zc + h_sub
            xyz%cs(1)%d50 = CS_substrat_1%d50
            xyz%cs(1)%sigma = CS_substrat_1%sigma
         endif
      enddo
   elseif (with_charriage >= 2) then
      do k = prfl%kfg+1, prfl%kfd-1
         if (prfl%xyz(k)%z > h_state(is)%z) cycle !on ne modifie que les points qui sont sous l'eau
         prfl%xyz(k)%z = prfl%xyz(k)%z + delta_h_sub + delta_h_subsub + h_nouv
      enddo
      if (CS_nouvelle%m > 0.0) then ! nouvelle couche
         allocate(compartiments_tmp(prfl%nbcs+1))
         do i = 1, prfl%nbcs
            compartiments_tmp(i+1) = prfl%cs(i)
         enddo
         call move_alloc(compartiments_tmp, prfl%cs)
         prfl%nbcs = prfl%nbcs + 1
         prfl%cs(1)%d50 = CS_nouvelle%d50
         prfl%cs(1)%sigma = CS_nouvelle%sigma
         prfl%cs(1)%hc = h_nouv
         prfl%cs(1)%tau = prfl%cs(2)%tau
      elseif (CS_substrat_1%m <= 0.0) then ! on enlève une couche
         if (prfl%nbcs > 1) then
            allocate(compartiments_tmp(prfl%nbcs-1))
            do i = 1, prfl%nbcs-1
                compartiments_tmp(i) = prfl%cs(i+1)
            enddo
            call move_alloc(compartiments_tmp, prfl%cs)
            prfl%nbcs = prfl%nbcs - 1
         endif
         prfl%cs(1)%d50 = CS_subsubstrat_1%d50
         prfl%cs(1)%sigma = CS_subsubstrat_1%sigma
         prfl%cs(1)%hc = h_subsub
      else
         prfl%cs(1)%d50 = CS_substrat_1%d50
         prfl%cs(1)%sigma = CS_substrat_1%sigma
         prfl%cs(1)%hc = h_sub
      endif
   endif
   ! mise à jour de la cote du fond du profil et autres alias géométriques
   zmin=9999999.0
   do k = 1, prfl%np
      if (prfl%xyz(k)%z < zmin) zmin = prfl%xyz(k)%z
   enddo
   prfl%zf = zmin
   li1 = prfl%li(1) ; li2 = prfl%li(2)  !limites du lit mineur
   z = min(prfl%xyz(li1)%z,prfl%xyz(li2)%z)
   prfl%ymoy = z - prfl%zf
   prfl%almy = prfl%largeur(z,prfl%main)
   prfl%pmy  = prfl%perimetre(z,prfl%main)
   prfl%smy  = prfl%section_mouillee(z,prfl%main)
   prfl%ybgau = prfl%xyz(1)%z-prfl%zf
   prfl%ybdro = prfl%xyz(prfl%np)%z-prfl%zf
   prfl%ybmin = min(prfl%ybgau,prfl%ybdro)
   prfl%ybmax = max(prfl%ybgau,prfl%ybdro)

   prfl%smaj_left  = prfl%section_XYZ(prfl%xyz(li1)%z,1)
   prfl%smaj_right = prfl%section_XYZ(prfl%xyz(li2)%z,3)

   if (is_LC) call prfl%largeursCotes()  !mise à jour de la tabulation largeurs-cotes

end subroutine update_profil_uniforme


function get_compartiment(is,lit,kc,dx) result(CS)
!construit le compartiment sédimentaire correspondant à la couche sédimentaire "kc" pour la sous-section "lit"
!kc = 1 -> substrat
!kc = 2 -> sub-substrat (couche n°2)
   implicit none
   ! -- prototype --
   integer, intent(in) :: is
   integer, intent(in) :: lit
   integer, intent(in) :: kc
   real(kind=long), intent(in) :: dx
   type(compartiment_sedimentaire) :: CS
   ! -- variables locales --
   type(profil), pointer :: prfl
   type(compartiment_sedimentaire) :: CS_tmp
   integer :: j, kcc, jdebut, jfin
   real(kind=long) :: s !surface verticale
   real(kind=long) :: toit, dy
   type(Point3D), pointer :: xyz

   prfl => la_topo%sections(is)
   !initialisation
   CS%m = 0._long
   if (with_charriage == 1) then
      xyz => prfl%xyz(prfl%li(lit))
      if (xyz%nbcs < kc) then
         CS%d50 = xyz%cs(1)%d50
         CS%sigma = xyz%cs(1)%sigma
      else
         CS%d50 = xyz%cs(kc)%d50
         CS%sigma = xyz%cs(kc)%sigma
      endif
      if (lit == 0) then
         jdebut = 1; jfin = prfl%np
      else
         jdebut = prfl%li(lit-1); jfin = prfl%li(lit)
      endif
      if(jdebut <= prfl%kfg)  jdebut = prfl%kfg
      if(jfin >= prfl%kfd)  jfin = prfl%kfd
      if(jdebut <= prfl%irg)  jdebut = prfl%irg
      if(jfin >= prfl%ird)  jfin = prfl%ird
      do j = jdebut, jfin
          !calcul distance transversale
          if (j == jdebut) then !demie-colonne début
            dy = distanceH(prfl%ptX, prfl%xyz(j)) + distanceH(prfl%xyz(j),prfl%xyz(j+1))/2
          elseif (j == jfin) then !demie-colonne fin
            dy = distanceH(prfl%xyz(j-1),prfl%xyz(j))/2 + distanceH(prfl%xyz(j), prfl%ptY)
          else
            dy = distanceH(prfl%xyz(j-1),prfl%xyz(j))/2 + distanceH(prfl%xyz(j),prfl%xyz(j+1))/2
          endif
          xyz => prfl%xyz(j)
          if (xyz%nbcs == 0) then
              write(error_unit,*) 'Erreur : nombre de couches sédimentaires nul ! ',is,lit,xyz%x,xyz%y,xyz%z,xyz%tag
              stop 1
          elseif (xyz%nbcs >= kc) then
              if (kc == 1) then !couche surface
                toit = xyz%z
              elseif (kc == 2) then !sub-surface
                toit = xyz%cs(kc-1)%zc
              else
                write(error_unit,'(a,i0)') &
                      'Erreur : le n° de couche sédimentaire devrait être égal à 1 ou 2. Il est : ',kc
                stop 999
              endif
              s =  (toit - xyz%cs(kc)%zc) * dy
              CS_tmp%m     = s * dx * rhos * (1._long - porosity)
              CS_tmp%d50   = xyz%cs(kc)%d50
              CS_tmp%sigma = xyz%cs(kc)%sigma
          else
              !couche inexistante
              CS_tmp%m     = 0.0
              CS_tmp%d50   = xyz%cs(1)%d50
              CS_tmp%sigma = xyz%cs(1)%sigma
          endif
          CS = CS + CS_tmp
      enddo
   elseif (with_charriage >= 2) then
!       dy = prfl%largeur_active
!       dy = prfl%largeur(h_state(is)%z,lit)
      dy = largeur_charriage(is, lit)
      if (prfl%nbcs >= kc) then
         s =  prfl%cs(kc)%hc * dy
         CS%m     = s * dx * rhos * (1._long - porosity)
         CS%d50   = prfl%cs(kc)%d50
         CS%sigma = prfl%cs(kc)%sigma
      else ! on a atteint le fond, on renvoie une couche vide
         CS%m     = 0
         CS%d50   = prfl%cs(1)%d50
         CS%sigma = prfl%cs(1)%sigma
      endif
   endif
end function get_compartiment


subroutine lire_CL_solides(filename)
! Lecture des C. L. solides
   implicit none
   ! -- prototype --
   character(len=*), intent(in) :: filename  !nom du fichier de données
   ! -- variables locales --
   character(len=30), parameter :: bl = repeat(' ',30)
   logical :: is_first_definition_for_this_node, node_found
   integer :: luu, ios, nligne, n, nf, k, u
   integer, pointer :: np
   character(len=30) :: ligne
   character(len=3) :: node
   character(len=120) :: info
   real(kind=long) :: tt , qs
   character(len=20) :: sT
   type(CL_solide), pointer :: this_CL
   type(point2D), allocatable :: pt2D(:)

   node_found = .false.
   np => null()
   this_CL => null()

   ! initialisation des CL solides
   allocate (allCLsolides(la_topo%net%nn))
   do n = 1, la_topo%net%nn
      allCLsolides(n)%np = 0
      allCLsolides(n)%ip = -1
      allCLsolides(n)%d50 = -1._long
      allCLsolides(n)%sigma = -1._long
      ! NOTE: on ne peut pas initialiser %bc(:) parce qu'ici on ne connait pas encore le nombre de valeurs %np
   enddo

   ! ouverture du fichier de CL de type type_CL
   open(newunit=luu, file=trim(filename), status='old', form='formatted',iostat=ios)
   if (ios > 0) then
      write(error_unit,'(3a)') ' Ouverture du fichier des C.L. solides ', trim(filename),' impossible'
      stop 7
   endif

   ! lecture du fichier de CL
   nligne = 0
   is_first_definition_for_this_node = .true.
   n = 0  !si n reste nul c'est qu'il y a un bug
   do
      nligne = nligne+1
      read(luu,'(a)',iostat=ios) ligne
      if (ios > 0) then
         write(output_unit,*) ' Erreur de lecture à la ligne ',nligne
         exit
      elseif (ios < 0) then
         exit
      endif
      !---on saute les lignes blanches et les commentaires (lignes commençant par *)
      if (ligne == bl) then
         cycle
      elseif (ligne(1:1) == '*') then
         cycle
      elseif (ligne(1:1) == '$') then
         !recherche du numéro du nœud à partir du nom du nœud
         read(ligne,'(1x,a3)') node
         node_found = .false.
         do n = 1, la_topo%net%nn
            if (la_topo%nodes(n)%name == node) then
               !vérification que la loi est valide : il faut que le nœud soit un nœud non aval
               if (la_topo%nodes(n)%cl<0) then
                  write(info,'(3a)') ' >>>> Le nœud ',node,' est nœud aval : loi Q_solide(t) impossible <<<<'
                  write(error_unit,'(a)') trim(info)  ;  write(lTra,'(a)') trim(info)
                  stop 154
               endif
               !vérification que le nœud n'a pas déjà reçu une loi
               if (allCLsolides(n)%np .ne. 0) then
                  write(lTra,'(3a)') ' >>>> Le noeud ',node,' a déjà reçu une loi. On garde la première loi <<<<'
                  is_first_definition_for_this_node = .false.
                  cycle
               else
                  is_first_definition_for_this_node = .true.
                  this_CL => allCLsolides(n)
                  np => allCLsolides(n)%np
               endif

               ! d50 et étendue granulométrique
               nf = 5 ! on cherche sur ligne après le nom du nœud, donc à partir du caractère n°5
               this_CL%d50 = next_real(ligne,'',nf)
               this_CL%sigma = next_real(ligne,'',nf)
               if (next_string(ligne,'',nf) .ne. ' ')then
                  allCLsolides(n)%np = -1
                  is_first_definition_for_this_node = .false.
               else
                  allocate (this_CL%bc(10)) ! allocation de base
               endif
               node_found = .true.
            endif
         enddo
         if (node_found) then
            cycle ! on passe à la lecture de la ligne suivante
         else
            ! fin des vérifications et de l'initialisation pour le nœud
            write(info,'(5a)') '>>> Erreur dans ',trim(filename),' : le noeud ',node,' n''existe pas'
            write(error_unit,'(a)') trim(info)  ;  write(lTra,'(a)') trim(info)
            stop 157
         endif
      else
         !couple temps,débit solide
         if (is_first_definition_for_this_node) then
            !Lecture des temps
!             if (scan(ligne,':') > 0) then !les temps sont donnés en jjjj:hh:mn
!                !lecture des temps sous la forme jjj:hh:mm + conversion en secondes
!                nf = 1
!                jj = next_int(ligne,':',nf)
!                ih = next_int(ligne,':',nf)
!                mn = next_int(ligne,':',nf)
!                tt = ( real(abs(jj),kind=long)*1440._long + real(ih,kind=long)*60._long + real(mn,kind=long) ) * 60._long
!                if (ligne(1:1) == '-' .or. jj < 0) tt = -tt
!             else
!                !lecture des temps en minutes converties en secondes
!                nf = 1
!                tt = next_real(ligne,'',nf) * 60._long
!             endif
            nf = 1
            sT = next_string(ligne,'',nf)
            tt = real(lire_date(sT,tinf,date_format),kind=long)
            !Lecture des débits solides
            qs = next_real(ligne,'',nf)
            ! affectation
            this_CL%ip = 1 !on a au moins une valeur, on peut donc initialiser %ip à 1
            this_CL%np = this_CL%np + 1
            u = ubound(this_CL%bc,1)
            if (this_CL%np > u) then
               !il faut allouer plus de place à this_CL%bc
               allocate (pt2D(u+10))
               pt2D(1:u) = this_CL%bc(1:u)
               call move_alloc(from=pt2D,to=this_CL%bc)
            endif
            this_CL%bc(np)%x = tt
            this_CL%bc(np)%y = qs
            !vérification de la croissance des temps de la C.L.
            if (np == 1) then
               cycle
            elseif (this_CL%bc(np)%x < this_CL%bc(np-1)%x) then
               write(info,'(3a)') ' >>>> Loi non croissante dans ',trim(filename),' <<<<'
               write(error_unit,'(a)') trim(info)  ;  write(lTra,'(a)') trim(info)
               write(info,*) ' à la ligne numéro ',nligne
               write(error_unit,'(a)') trim(info)  ;  write(lTra,'(a)') trim(info)
               stop 158
            endif
         endif
      endif
   enddo
   close (luu)
   ! vérification de la lecture des CL solides
   do n = 1, la_topo%net%nn
      if (allCLsolides(n)%np <= 0) cycle
      write(l9,*) 'CL solide pour ',la_topo%nodes(n)%name
      write(l9,*) allCLsolides(n)%np, allCLsolides(n)%ip, allCLsolides(n)%d50, allCLsolides(n)%sigma
      do k = 1, allCLsolides(n)%np
         write(l9,*) '   ', allCLsolides(n)%bc(k)%x, allCLsolides(n)%bc(k)%y
      enddo
   enddo

end subroutine lire_CL_solides


function get_CL_solide(nb,t,dt) result(CS)
! CL amont solide - renvoie un objet de type compartiment_sedimentaire
   implicit none
   ! -- prototype --
   integer, intent(in)         :: nb      !index du nœud amont
   real(kind=long), intent(in) :: t, dt   !temps et pas de temps
   type(compartiment_sedimentaire) :: CS
   ! -- variables locales --
   integer, pointer :: ip
   integer :: np, k, is, lit
   real(kind=long) :: qs, a
   type(profil), pointer :: prfl

   ip => allCLsolides(nb)%ip
   np = allCLsolides(nb)%np
   qs = 0._long

   ! construction du compartiment sédimentaire vide
   CS%d50 = allCLsolides(nb)%d50
   CS%sigma = allCLsolides(nb)%sigma
   CS%m = 0

   if (np == -1) then ! capacite solide
      is = la_topo%biefs(la_topo%net%lbamv(nb))%is1
      prfl => la_topo%sections(is)
      do lit = 1, prfl%nzone ! remplissage du compartiment sédimentaire
          CS%m = CS%m + capacite_solide(is, lit, CS%d50, CS%sigma) * largeur_charriage(is,lit) * dt
      enddo
   else
      if (t <= allCLsolides(nb)%bc(1)%x) then
          ! prolongement constant avant le début
          qs = allCLsolides(nb)%bc(1)%y
          ip = 1
      elseif (t >= allCLsolides(nb)%bc(np)%x) then
          ! prolongement constant après la fin
          qs = allCLsolides(nb)%bc(np)%y
          ip = np
      else
          ! cas normal
          do k = ip+1, np
            if (t < allCLsolides(nb)%bc(k)%x) then
                !t est entre k-1 et k -> interpolation et mise à jour de l'index ip
                a = (allCLsolides(nb)%bc(k)%y - allCLsolides(nb)%bc(k-1)%y) / &
                    (allCLsolides(nb)%bc(k)%x - allCLsolides(nb)%bc(k-1)%x)
                qs = allCLsolides(nb)%bc(k-1)%y + a * (t - allCLsolides(nb)%bc(k-1)%x)
                ip = k-1
                exit
            endif
          enddo
      endif
      ! remplissage du compartiment sédimentaire
      CS%m = qs * dt
   endif

end function get_CL_solide


subroutine export_bottom_data(header)
!écrit les cotes de fond dans un fichier CSV ; une colonne par date
   implicit none
   ! -- prototype --
   character(len=*), intent(in) :: header
   ! -- variables locales --
   logical, save :: first_call = .true.
   integer, save :: lw
   character, parameter :: sep=';'
   integer :: ib, is
   character(len=:), allocatable :: lignes(:)
   integer, pointer :: ismax
   integer, save :: current_length
   character(len=120) :: line
   integer :: alonge

   ismax => la_topo%net%ns
   alonge = 0
   if (first_call) then
      !current_length = 0
      open(newunit=lw,file=trim(btmFile),form='formatted',status='unknown')
      write(lw,'(*(a))') 'IB',sep,'IS',sep,'Pk',sep,trim(header)
      current_length = 9+len_trim(header)
      do ib = 1, la_topo%net%nb
         do is = la_topo%biefs(ib)%is1, la_topo%biefs(ib)%is2
            write(line,'(2(i0,a1),a,a1,a)') ib, sep, is-la_topo%biefs(ib)%is1+1, sep, &
                                          trim(f0p(la_topo%sections(is)%pk,4)), sep, trim(f0p(la_topo%sections(is)%zf,4))
            current_length = max(current_length,len_trim(line))
            write(lw,'(a)') trim(line)
         enddo
      enddo
      first_call = .false.
      !current_length = int(alog10(real(la_topo%net%nb))) + int(alog10(real(ismax))) + 23
   else
      allocate(character(len=current_length) :: lignes(0:ismax))
      !allocate (lignes(0:ismax))
      rewind(lw)
      do is = 0, ismax
         read(lw,'(a)') lignes(is)
      enddo
      rewind(lw)
      alonge = len_trim(header)
      write(lw,'(a,a1,a)') trim(lignes(0)), sep, trim(header)
      do is = 1, ismax
         line = f0p(la_topo%sections(is)%zf,4)
         write(lw,'(a,a1,a)') trim(lignes(is)), sep, trim(line)
         alonge = max(alonge,len_trim(line))
      enddo
   endif
   flush(unit=lw)
   current_length = current_length + alonge + 1
end subroutine export_bottom_data


subroutine export_Qsolid_data(header)
!écrit les débits solides dans un fichier CSV ; une colonne par date
   implicit none
   ! -- prototype --
   character(len=*), intent(in) :: header
   ! -- variables locales --
   logical, save :: first_call = .true.
   integer, save :: lw
   character, parameter :: sep=';'
   integer :: ib, is
   character(len=:), allocatable :: lignes(:)
   integer, pointer :: ismax
   integer, save :: current_length
   character(len=120) :: line
   integer :: alonge

   ismax => la_topo%net%ns
   alonge = 0
   if (first_call) then
      current_length = 0
      open(newunit=lw,file=trim(qstFile),form='formatted',status='unknown')
      write(lw,'(*(a))') 'IB',sep,'IS',sep,'Pk',sep,trim(header)
      do ib = 1, la_topo%net%nb
         do is = la_topo%biefs(ib)%is1, la_topo%biefs(ib)%is2
            write(line,'(2(i0,a1),a,a1,a)') ib, sep, is-la_topo%biefs(ib)%is1+1, sep, &
                                          trim(f0p(la_topo%sections(is)%pk,4)), sep, trim(f0p(Qs_total(is),4))
            current_length = max(current_length,len_trim(line))
            write(lw,'(a)') trim(line)
         enddo
      enddo
      first_call = .false.
   else
      allocate(character(len=current_length) :: lignes(0:ismax))
      rewind(lw)
      do is = 0, ismax
         read(lw,'(a)') lignes(is)
      enddo
      rewind(lw)
      alonge = len_trim(header)
      write(lw,'(a,a1,a)') trim(lignes(0)), sep, trim(header)
      do is = 1, ismax
         line = f0p(Qs_total(is),4)
         write(lw,'(a,a1,a)') trim(lignes(is)), sep, trim(line)
         alonge = max(alonge,len_trim(line))
      enddo
   endif
   flush(unit=lw)
   current_length = current_length + alonge + 1
end subroutine export_Qsolid_data


! subroutine export_granulo_data(header, CS_active)
! !écrit les d50 dans un fichier CSV ; une colonne par date
!    implicit none
!    ! -- prototype --
!    character(len=*), intent(in) :: header
!    type(compartiment_sedimentaire), intent(in) :: CS_active
!    ! -- variables locales --
!    logical, save :: first_call = .true.
!    integer, save :: lw
!    character, parameter :: sep=';'
!    integer :: ib, is
!    character(len=:), allocatable :: lignes(:)
!    integer, pointer :: ismax
!    integer, save :: current_length
!    character(len=120) :: line
!    integer :: alonge
!
!    ismax => la_topo%net%ismax
!    alonge = 0
!    if (first_call) then
!       current_length = 0
!       open(newunit=lw,file=trim(graFile),form='formatted',status='unknown')
!       write(lw,'(*(a))') 'IB',sep,'IS',sep,'Pk',sep,trim(header)
!       do ib = 1, la_topo%net%ibmax
!          do is = la_topo%biefs(ib)%is1, la_topo%biefs(ib)%is2
!             write(line,'(2(i0,a1),a,a1,a)') ib, sep, is-la_topo%biefs(ib)%is1+1, sep, &
!                                           trim(f0p(la_topo%sections(is)%pk,4)), sep, trim(f0p(CS_active%d50,4))
!             current_length = max(current_length,len_trim(line))
!             write(lw,'(a)') trim(line)
!          enddo
!       enddo
!       first_call = .false.
!    else
!       allocate(character(len=current_length) :: lignes(0:ismax))
!       rewind(lw)
!       do is = 0, ismax
!          read(lw,'(a)') lignes(is)
!       enddo
!       rewind(lw)
!       alonge = len_trim(header)
!       write(lw,'(a,a1,a)') trim(lignes(0)), sep, trim(header)
!       do is = 1, ismax
!          line = f0p(CS_active%d50,4)
!          write(lw,'(a,a1,a)') trim(lignes(is)), sep, trim(line)
!          alonge = max(alonge,len_trim(line))
!       enddo
!    endif
!    flush(unit=lw)
!    current_length = current_length + alonge + 1
! end subroutine export_granulo_data


subroutine lire_Param_Charriage(filename)
!lecture du fichier de paramètres pour le charriage
!filename est un fichier de format clé=valeur avec .CHA comme extension par défaut
!Ce qui est lu dans le fichier CHA modifie ce qui a été défini par init_Param_Default_Charriage()

   implicit none
   ! -- prototype --
   character(len=*), intent(in) :: filename
   ! -- variables locales --
   integer :: lw, ios, k
   character(len=80) :: ligne
   character(len=30) :: cle

   if (trim(filename) == '') return !on garde les valeurs par défaut si le fichier CHA n'est pas fourni

   open(newunit=lw,file=trim(filename),status='unknown',form='formatted',iostat=ios)
   if (ios > 0) then
      write(error_unit,'(3a)') ' Ouverture du fichier des paramètres de charriage ', trim(filename),' impossible'
      stop 7
   endif
   do
      read(lw,'(a)',iostat=ios) ligne
      if (ios < 0) then
         exit
      elseif (ios > 0) then
         write(error_unit,'(2a)') '>>>> Erreur de lecture de ',trim(filename)
         stop 1
      else
         continue
      endif
      if (ligne(1:1) == '*') cycle
      if (len_trim(ligne) == 0) cycle
      k = scan(ligne,'=:') - 1 !dernier caractère avant le signe =
      cle = trim(ligne(1:k))
      k = 1 + max(scan(ligne,'=:'),scan(trim(ligne),' ',back=.true.))
      select case (cle)
         case ('distance_Han')
            read(ligne(k:),'(f10.0)') a_Han
         case ('distance_chargement_d50')
            read(ligne(k:),'(f10.0)') L_d
         case ('distance_chargement_sigma')
            read(ligne(k:),'(f10.0)') L_s
         case ('methode_modification_geometrie')
            read(ligne(k:),'(i2)') method_geo
         case ('shields_critique')
            read(ligne(k:),'(i2)') shields
         case ('shields_correction')
            read(ligne(k:),'(i2)') shields_correction
         case ('capacite_solide')
            read(ligne(k:),'(i2)') cap_sol
         case('pas_de_temps')
            read(ligne(k:),'(i3)') n_pas
            if (n_pas < 1) then
               write(error_unit,'()') '>>>> Erreur : le pas de temps pour le charriage est nul'
               stop 7
            endif
         case('facteur_multiplicateur')
            read(ligne(k:),'(f10.0)') f_mult
         case('sediment_masse_volumique')
             read(ligne(k:),'(f10.0)') rhos
         case('sediment_angle_repos')
            read(ligne(k:),'(f10.0)') phis
         case('sediment_porosity')
            read(ligne(k:),'(f10.0)') porosity
         case('largeur_active_extremite_1')
            read(ligne(k:),'(a3)') LA1
         case('largeur_active_extremite_2')
            read(ligne(k:),'(a3)') LA2
        case default
            write(error_unit,'(2a)') '>>>> Erreur lors de la lecture de ',trim(filename)
            write(error_unit,'(2a)') '     Clé inconnue : ',trim(cle)
      end select
   enddo

   dt_char = dtbase * real(n_pas,kind=long)

   call assign_pointer_functions()

   call create_largeur_active(LA1,LA2)

end subroutine lire_Param_Charriage


subroutine ecrire_Param_Charriage(lu)
! écrit les paramètres de charriage sur l'unité logique lu
   implicit none
   ! -- prototype --
   integer, intent(in) :: lu

   write(lu,'(/,1x,a)') 'Paramètres pour le charriage :'
   write(lu,'(5x,a,f8.3)') 'masse volumique du sédiment = ',rhos
   write(lu,'(5x,a,f8.3)') 'porosité du sédiment = ',porosity
   write(lu,'(5x,a,f8.3)') 'angle de repos du sédiment = ',phis
   write(lu,'(5x,a,f8.3)') 'distance de Han = ',a_Han
   write(lu,'(5x,a,f8.3)') 'distance de chargement pour le d50 = ',L_d
   write(lu,'(5x,a,f8.3)') 'distance de chargement pour le sigma = ',L_s
   write(lu,'(5x,a,i0)')   'méthode de mise à jour de la géométrie = ',method_geo
   write(lu,'(5x,a,i0)')   'méthode de calcul du nombre de Shields critique = ',shields
   write(lu,'(5x,a,i0)')   'méthode de correction du nombre de Shields = ',shields_correction
   write(lu,'(5x,a,i0)')   'méthode de calcul de la capacité solide = ',cap_sol
   write(lu,'(5x,a,f8.3)') 'pas de temps pour le charriage = ',dt_char
   if (trim(LA1) /= '' .and. trim(LA2) /= '') then
      write(lu,'(5x,4a)') 'lignes directrices de la largeur active : ',trim(LA1),' & ',trim(LA2)
   else
      write(lu,'(5x,a)') 'lignes directrices de la largeur active : limites du profil'
   endif
   write(lu,'(a)') ''
end subroutine ecrire_Param_Charriage


subroutine assign_pointer_functions()
   implicit none

   select case (method_geo)
      case (1) ; update_profil => update_profil_uniforme
      case default
         write(error_unit,'(a)') '>>>> Erreur de choix de méthode de mise à jour de la géométrie'
         write(error_unit,'(5x,a,i1,a)') 'la méthode n°',method_geo,' n''est pas disponible sur cette version'
         stop 7
   end select

   select case (shields)
      case (1) ; shields_critique => shields_critique_0047
      case (2) ; shields_critique => shields_critique_Soulsby
      case (3) ; shields_critique => shields_critique_Camenen
      case default
         write(error_unit,'(a)') '>>>> Erreur de choix de définition du nombre de Shields critique'
         write(error_unit,'(5x,a,i1,a)') 'la méthode n°',shields,' n''est pas disponible sur cette version'
         stop 7
   end select

   select case (shields_correction)
      case (0) ; shields_param => shields_brut
      case (1) ; shields_param => shields_effectif
      case default
         write(error_unit,'(a)') '>>>> Erreur de choix de méthode de correction du nombre de Shields effectif'
         write(error_unit,'(5x,a,i1,a)') 'la méthode n°',shields_correction,' n''est pas disponible sur cette version'
         stop 7
   end select

   select case (cap_sol)
      case (1) ; capacite_solide => capacite_solide_MPM
      case (2) ; capacite_solide => capacite_solide_CL
      case (3) ; capacite_solide => capacite_solide_EH
      case (4) ; capacite_solide => capacite_solide_Reck
      case default
         write(error_unit,'(a)') '>>>> Erreur de choix de la méthode de calcul de la capacité solide'
         write(error_unit,'(5x,a,i1,a)') 'la méthode n°',cap_sol,' n''est pas disponible sur cette version'
         stop 7
   end select

end subroutine assign_pointer_functions


subroutine create_largeur_active(LA1,LA2)
!calcule la largeur active pour chaque profil du modèle
!la largeur active est définie par les lignes directrices LA1 et LA2.
!on n'a pas besoin de savoir laquelle est à gauche et laquelle est à droite
   implicit none
   ! -- prototype --
   character(len=3), intent(in) :: LA1, LA2
   ! -- variables locales --
   integer :: is, n, kfg, kfd, LA_not_found
   type(profil), pointer :: prfl
   logical :: LA1_found, LA2_found
   class(point3D), pointer :: pTS(:)

   if (trim(LA1) /= '' .and. trim(LA2) /= '') then
      !LA1 et LA2 ont été définies
      LA_not_found = 0
      do is = 1, la_topo%net%ns
         prfl => la_topo%sections(is)
         prfl%largeur_active = 0.0
         LA1_found = .false.
         LA2_found = .false.
         !repérage des lignes directrices LA1 et LA2
         do n = 1, prfl%np
            if (trim(prfl%xyz(n)%tag) == trim(LA1)) then
               prfl%kfg = n
               LA1_found = .true.
            elseif (trim(prfl%xyz(n)%tag) == trim(LA2)) then
               prfl%kfd = n
               LA2_found = .true.
            elseif (LA1_found .and. LA2_found) then
               !inutile de parcourir les autres points, les 2 ont déjà été trouvés
               exit
            else
               continue
            endif
         enddo
         !on veut que %ptX soit le point de gauche c'est-à-dire celui de plus petit indice
         if (prfl%kfg > prfl%kfd) then
            n = prfl%kfd
            prfl%kfd = prfl%kfg
            prfl%kfg = n
         endif
         !calcul de la largeur active
         if (LA1_found .and. LA2_found) then
            pTS => prfl%xyz
            kfg = prfl%kfg  ;  kfd = prfl%kfd
            n = kfg
            prfl%largeur_active = prfl%largeur_active &
                                + 0.5_long * sign(distanceH(pTS(n),pTS(n+1)),p_scalaire_H(pTS(kfg),pTS(kfd),pTS(n),pTS(n+1)))
            do n = kfg+1, kfd-2
               prfl%largeur_active = prfl%largeur_active &
                                   + sign(distanceH(pTS(n),pTS(n+1)),p_scalaire_H(pTS(kfg),pTS(kfd),pTS(n),pTS(n+1)))
            enddo
            n = kfd-1
            prfl%largeur_active = prfl%largeur_active &
                                + 0.5_long * sign(distanceH(pTS(n),pTS(n+1)),p_scalaire_H(pTS(kfg),pTS(kfd),pTS(n),pTS(n+1)))
         else
            !si l'une des lignes directrices n'est pas trouvée, c'est comme si aucune n'avait été définie.
            prfl%largeur_active = 1.E+30_long
            prfl%kfg = 1
            prfl%kfd = prfl%np
            LA_not_found = LA_not_found + 1
         endif
      enddo
      if (LA_not_found > 0) then
         write(output_unit,'(2(a,i0),a)')  &
               ' >>>> NOTE : les lignes directrices spécifiées pour définir la largeur active sont introuvables sur ', &
               LA_not_found,' profils (sur un total de ',la_topo%net%ns,')'
         write(lTRA,'(2(a,i0),a)')  &
               ' >>>> NOTE : les lignes directrices spécifiées pour définir la largeur active sont introuvables sur ', &
               LA_not_found,' profils (sur un total de ',la_topo%net%ns,')'
      endif
   else
      !LA1 et LA2 n'ont pas été définies, on prend la section en entier
      do is = 1, la_topo%net%ns
         prfl => la_topo%sections(is)
         prfl%largeur_active = 0.0
         prfl%kfg = 1
         prfl%kfd = prfl%np
         pTS => prfl%xyz
         kfg = prfl%kfg  ;  kfd = prfl%kfd
         n = kfg
         prfl%largeur_active = prfl%largeur_active &
                             + 0.5_long * sign(distanceH(pTS(n),pTS(n+1)),p_scalaire_H(pTS(kfg),pTS(kfd),pTS(n),pTS(n+1)))
         do n = kfg+1, kfd-2
            prfl%largeur_active = prfl%largeur_active &
                                + sign(distanceH(pTS(n),pTS(n+1)),p_scalaire_H(pTS(kfg),pTS(kfd),pTS(n),pTS(n+1)))
         enddo
         n = kfd-1
         prfl%largeur_active = prfl%largeur_active &
                             + 0.5_long * sign(distanceH(pTS(n),pTS(n+1)),p_scalaire_H(pTS(kfg),pTS(kfd),pTS(n),pTS(n+1)))
      enddo
   endif
end subroutine create_largeur_active

!-----------------------------------------------------------------------
! function proche(d1,s1,t1,d2,s2)
function proche(c1, c2)
!-----------------------------------------------------------------------
! indique si les caractéristiques des sédiments 1 et 2 sont proches
!-----------------------------------------------------------------------

      use parametres,only:toutpetit

      implicit none
      logical :: proche
      type(compartiment_sedimentaire), intent(in) :: c1, c2
!       real(kind=long), intent(in) :: t1
      real(kind=long) :: dl1,dl2,dl3,dl4

! version provisoire: on pourrait tester les contraintes de mise en mouvement
! le 25/5/2020 ajout de t1 contrainte couche 1
!       if (t1.gt.900.)then
!          proche=.false.
!          return
!       endif

      proche=.true.
      dl1=c2%d50*(1.+coefproche*(c2%sigma-1.))*(1.+toutpetit)
      dl2=c1%d50/(1.+coefproche*(c1%sigma-1.))*(1.-toutpetit)
      dl3=c2%d50/(1.+coefproche*(c2%sigma-1.))*(1.-toutpetit)
      dl4=c1%d50*(1.+coefproche*(c1%sigma-1.))*(1.+toutpetit)
      if(c1%d50.gt.dl1.and.c2%d50.lt.dl2) then
        proche=.false.
      elseif(c1%d50.lt.dl3.and.c2%d50.gt.dl4)then
        proche=.false.
      endif

end function proche

!-----------------------------------------------------------------------
function largeur_charriage(is, lit)
   real(kind=long)       :: largeur_charriage
   integer, intent(in)   :: is, lit
   type(profil), pointer :: prfl

   prfl => la_topo%sections(is)
   if (with_charriage == 1) then
   select case (lit)
      case (0) ; largeur_charriage = min(distanceH(prfl%xyz(prfl%kfg), prfl%xyz(prfl%kfd)), prfl%largeur(h_state(is)%z,lit))
      case (1) ; largeur_charriage = min(distanceH(prfl%xyz(prfl%kfg), prfl%xyz(prfl%irg)), prfl%largeur(h_state(is)%z,lit))
      case (2) ; largeur_charriage = min(prfl%largeur_active, prfl%largeur(h_state(is)%z,lit))
      case (3) ; largeur_charriage = min(distanceH(prfl%xyz(prfl%ird), prfl%xyz(prfl%kfd)), prfl%largeur(h_state(is)%z,lit))
      case default
         write(error_unit,'(a)') '>>>> ERREUR : numéro de sous-section erroné dans largeur_charriage()'
         write(error_unit,'(a)') '              les valeurs possibles sont 1, 2 et 3'
         stop 1
   end select
   elseif (with_charriage >= 2) then
      largeur_charriage = prfl%largeur_active
   endif
end function largeur_charriage

subroutine init_cs_echange()
   integer :: i
   allocate(CS_echange(la_topo%net%ns))
   do i=1, la_topo%net%ns
     CS_echange(i)%m = 0.0
   enddo
end subroutine init_cs_echange

end module charriage
