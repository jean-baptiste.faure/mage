!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - INRAE - 1989-2020                           #
!# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################
!##############################################################################
!#                                                                            #
!#                    Programmme Mage_Compare_BIN                             #
!#                                                                            #
!##############################################################################



program Mage_Compare_BIN
!  programme de comparaison de 2 fichiers BIN produits par Mage-8

   use, intrinsic :: iso_fortran_env
   implicit none
   ! -- variables locales --
   integer, parameter :: sp = kind(1.), long = kind(1.d0)
   character(len=120) :: argmnt
   character(len=120) :: bin1, bin2
   integer :: iarg, lBin1, lBin2, ibmax1, ibmax2, ismax1, ismax2, ismax, is, ios
   logical :: long_T
   real(kind=sp), allocatable :: q1(:), q2(:), z1(:), z2(:)
   real(kind=long) :: t1, t2
   character(len=1) :: var1, var2
   real(kind=sp) :: qmin, qmax, qmoy, zmin, zmax, zmoy
   integer :: non_nul_Q, non_nul_Z, nb_dt
   character(len=10) :: date_version = '2021-03-18'

   iarg = 1 ; call get_command_argument(iarg,argmnt)
   if (len_trim(argmnt) > 0) then  !il y a des arguments sur la ligne de commande
      if (argmnt(1:2) == '-v') then
         write(output_unit,*)' Programme Mage_Compare_BIN pour Mage-8'
         write(output_unit,*)' Version du '//date_version
         stop
      elseif (argmnt(1:2) == '-h') then
         call help()
         stop
      else
         write(output_unit,'(a)') 'Programme Mage_Compare_BIN pour Mage-8 -- Version du '//date_version
         bin1 = trim(argmnt)
         write(output_unit,'(2a)') '--- Nom du premier fichier BIN : ',trim(bin1)
         iarg = iarg+1 ; call get_command_argument(iarg,argmnt)
         bin2 = trim(argmnt)
         write(output_unit,'(2a)') '+++ Nom du second fichier BIN : ',trim(bin2)
      endif
   else
      ! lecture des données par interrogation de l'utilisateur
      write(output_unit,'(a)') ' Programme Mage_Compare_BIN '
      write(output_unit,'(a)',advance='no') ' Nom du premier fichier BIN : '
      read(input_unit,'(a)') bin1
      write(output_unit,'(a)',advance='no') ' Nom du second fichier BIN : '
      read(input_unit,'(a)') bin2
   endif


! lecture des entêtes des fichiers BIN
   call lire_Entete(lBin1,bin1,ibmax1,ismax1,long_T)
   if (.not.long_T) then
      write(error_unit,*) '>>>> Le fichier ',trim(bin1),' n''est pas un fichier produit par Mage-8'
      stop 1
   endif
   call lire_Entete(lBin2,bin2,ibmax2,ismax2,long_T)
   if (.not.long_T) then
      write(error_unit,*) '>>>> Le fichier ',trim(bin2),' n''est pas un fichier produit par Mage-8'
      stop 1
   endif

!vérifications
   if (ibmax1 /= ibmax2 .or. ismax1 /= ismax2) then
      write(error_unit,'(5a)') '>>>> Les fichiers ',trim(bin1),' et ',trim(bin2),' correspondent à des modèles différents'
      stop 13
   endif

   allocate (q1(ismax1), q2(ismax2), z1(ismax1), z2(ismax2))

   qmin = 1.e+30 ; zmin = 1.e+30 ; qmax = 0. ; zmax = 0. ; qmoy = 0. ; zmoy = 0.
   nb_dt = 0  ;  non_nul_Q = 0  ;  non_nul_Z = 0
   do
      read(lBin1,iostat=ios) ismax,t1,var1,(q1(is),is=1,ismax)
      if (ios < 0) then
         read(lBin2,iostat=ios) ismax,t2,var2,(q2(is),is=1,ismax)
         if (ios == 0) then
            write(output_unit,'(2a)') '>>>> Les 2 fichiers BIN n''ont pas le même nombre de lignes d''eau, ', &
                                      'le fichier de référence est plus court'
            stop 1
         endif
         exit ! fin du fichier
      endif
      if (var1 == 'Q') then
         read(lBin1) ismax,t1,var1,(z1(is),is=1,ismax)
         read(lBin2,iostat=ios) ismax,t2,var2,(q2(is),is=1,ismax)
         if (ios < 0) then
            write(output_unit,'(2a)') '>>>> Les 2 fichiers BIN n''ont pas le même nombre de lignes d''eau, ', &
                                      'le fichier de référence est plus long'
            stop 2
         endif
         read(lBin2) ismax,t2,var2,(z2(is),is=1,ismax)
         !write(output_unit,*) t1,var1,t2,var2,' OK'
         if (abs(t2-t1) > 1.0e-09) then
          write(output_unit,'(a,f0.6,a,f0.6)') '>>>> Les 2 fichiers BIN sont incompatibles, les dates sont différentes : ', &
                                               t1,' et ',t2
          stop 3
         endif
      else
         read(lBin2) ismax,t2,var2,(q2(is),is=1,ismax)
         !write(output_unit,*) t1,var1,t2,var2,' passe'
         cycle
      endif
      nb_dt = nb_dt + 1
      do is = 1, ismax
         if (abs(q1(is)-q2(is)) > 1.E-09_long) then
            qmin = min(qmin,abs(q1(is)-q2(is)))
            non_nul_Q = non_nul_Q + 1
         endif
         qmax = max(qmax,abs(q1(is)-q2(is)))
         qmoy = qmoy + abs(q1(is)-q2(is))
         if (abs(z1(is)-z2(is)) > 1.E-09_long) then
            zmin = min(zmin,abs(z1(is)-z2(is)))
            non_nul_Z = non_nul_Z + 1
         endif
         zmax = max(zmax,abs(z1(is)-z2(is)))
         zmoy = zmoy + abs(z1(is)-z2(is))
      enddo
   enddo
   if (non_nul_Q > 0) then
      qmoy = qmoy / real(non_nul_Q)
   else
      qmin = 0._long
      qmoy = 0._long
   endif
   if (non_nul_Z > 0) then
      zmoy = zmoy / real(non_nul_Z)
   else
      zmin = 0._long
      zmoy = 0._long
   endif
   !write(output_unit,*) qmin, qmax, qmoy, non_nul_Q, zmin, zmax, zmoy, non_nul_Z, nb_dt*ismax
   write(output_unit,'(4x,a,e12.6,a)') 'Débits, maximum des écarts           : ',qmax,' m3/s'
   write(output_unit,'(4x,a,e12.6,a)') 'Débits, minimum des écarts non-nuls  : ',qmin,' m3/s'
   write(output_unit,'(4x,a,e12.6,a)') 'Débits, moyenne des écarts non-nuls  : ',qmoy,' m3/s'
   write(output_unit,'(4x,a,i0)')      'Débits, nombre d''écarts non-nuls     : ',non_nul_Q
   write(output_unit,'(4x,a,e12.6,a)') 'Cotes,  maximum des écarts           : ',zmax,' m'
   write(output_unit,'(4x,a,e12.6,a)') 'Cotes,  minimum des écarts non-nuls  : ',zmin,' m'
   write(output_unit,'(4x,a,e12.6,a)') 'Cotes,  moyenne des écarts non-nuls  : ',zmoy,' m'
   write(output_unit,'(4x,a,i0)')      'Cotes,  nombre d''écarts non-nuls     : ',non_nul_Z
   write(output_unit,'(4x,a,i0)')      'Nombre total de valeurs par champ    : ',nb_dt*ismax
   write(output_unit,'(4x,a,i0)')      'Nombre de pas de temps enregistrés   : ',nb_dt

   deallocate (q1, q2, z1, z2)
   close(lBin1) ; close (lBin2)
   stop

contains


subroutine lire_Entete(lu,binFile,ibmax,ismax,long_T)
!lecture de la partie déclarative de BIN
   implicit none
   ! -- prototype --
   character(len=*),intent(in) :: binFile  !fichier BIN
   integer, intent(out) :: lu, ibmax, ismax
   logical, intent(out) :: long_T !indique si les temps stockés dans BIN sont en simple ou double précision
   ! -- variables locales --
   integer :: ib, ks, is, ios, kbl, mage_version
   integer, allocatable :: ibu(:), is1(:), is2(:), ibs(:)
   real, allocatable :: xl(:), zfd(:), ygeo(:), ybas(:)
   real :: zmin

   inquire(file=trim(binFile),size=ks)
   if (ks == 0) then
      write(output_unit,'(a)') '>>>>'
      write(output_unit,'(3a)') '>>>> Erreur : le fichier ',trim(binFile),' est vide'
      write(output_unit,'(a)') '>>>>'
      stop 997
   endif
   open(newunit=lu,file=trim(binFile),status='old',form='unformatted',iostat=ios)
   if (ios > 0) then
      write(error_unit,*) '>>>> Erreur d''ouverture du fichier ',trim(binFile)
      stop 998
   endif

   read(lu) ibmax, ismax, mage_version
   if (mage_version >= 80) then
      ! nouvel entête
      allocate (is1(ibmax), is2(ibmax))
      allocate (xl(ismax), zfd(ismax), ygeo(ismax), ybas(ismax))
      long_T = .true.
      read(lu) (is1(ib),is2(ib),ib=1,ibmax)
      read(lu) (xl(is), is=1,ismax)
      read(lu) (zfd(is),ygeo(is),ybas(is),is=1,ismax)
      deallocate (is1, is2, xl, zfd, ygeo, ybas)
   else
      kbl = mage_version
      ! ancien entête
      allocate (ibu(ibmax), is1(ibmax), is2(ibmax), ibs(ismax))
      allocate (xl(ismax), zfd(ismax), ygeo(ismax), ybas(ismax))
      if (kbl < 0) then
         long_T = .true.
         kbl = -kbl
      else
         long_T = .false.
      endif
      read(lu) (ibu(ib),ib=1,ibmax)
      read(lu) (is1(ib),is2(ib),ib=1,ibmax)
      do ks = 1, ismax, kbl
         read(lu) (xl(is), is=ks,min(ks+kbl-1,ismax))
      enddo
      read(lu) zmin
      do ks = 1, ismax, kbl
         read(lu) (zfd(is),ygeo(is),ybas(is),is=ks,min(ks+kbl-1,ismax))
      enddo
      do ks = 1, ismax, kbl
         read(lu) (ibs(is), is=ks,min(ks+kbl-1,ismax))
      enddo
      deallocate (ibu, is1, is2, ibs, xl, zfd, ygeo, ybas)
   endif
   !write(output_unit,*) '--> fin de la lecture de l''entete de ',trim(binFile)
end subroutine lire_Entete


subroutine help()
!==============================================================================
!     affichage des options de la ligne de commande
!==============================================================================

   write(output_unit,*) 'Syntaxe de la ligne de commande de Mage_Compare_BIN'
   write(output_unit,*) '  Mage_Compare_BIN -v pour afficher le numéro de version et quitter'
   write(output_unit,*) '  Mage_Compare_BIN -h pour afficher cette aide et quitter'
   write(output_unit,*) '  Mage_Compare_BIN bin1 bin2'
   write(output_unit,*) '      bin1 et bin2 sont les noms des 2 fichiers BIN à comparer'
   write(output_unit,*) ''
   write(output_unit,*) 'Exemple : mage_Compare_BIN etude_REF.bin etude.bin'

end subroutine help

end program Mage_Compare_BIN
