!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - INRAE - 1989-2020                           #
!# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################
!##############################################################################
!
!        Module et sous-programmes relatifs aux Conditions aux Limites
!
!##############################################################################
! QUESTION: peut-on regrouper la gestion des C.L dans un seul fichier source ? actuellement il y a aussi des routines mage_ISM.f90


module Data_tmp_CL
!==============================================================================
!        Intermédiaire pour la lecture des Conditions aux limites
!==============================================================================
   use Parametres, only: Long
   implicit none
   real(kind=Long), allocatable :: T(:), Q(:) ! couples (date,debit) ou
                                              ! (cote,debit) ou (date,cote) discretisees
   real(kind=long), allocatable :: ql(:), qr(:) ! débits supplémentaires pour ISM : 3 débits par CL amont
                                                ! dans ce cas Q est le débit en lit mineur, Ql en lit majeur
                                                ! gauche et Qr en lit majeur droit
   integer, allocatable :: ITM(:)   ! ITM(N)  = rang de lecture pour le noeud N
   integer, allocatable :: JTM(:)   ! JTM(KN) = dernier point pour le noeud de rang de lecture KN
   logical, allocatable :: boolc(:) ! vrai si l'apport en débit au nœud N est un apport surfacique

   contains
      subroutine IniLireCL(nomax, iclmax)
         use Parametres, only: zero
         implicit none
         integer, intent(in) :: nomax,iclmax

         allocate (T(iclmax), Q(iclmax), ql(iclmax), qr(iclmax))
         allocate (ITM(0:nomax), JTM(0:nomax), boolc(nomax))
         T(1:iclmax) = zero ; Q(1:iclmax) = zero ; Ql(1:iclmax) = zero ; Qr(1:iclmax) = zero
         ITM(1:nomax) = 0    ; JTM(1:nomax) = 0
         boolc(1:nomax) = .false.
      end subroutine IniLireCL
end module Data_tmp_CL


module Conditions_Limites
!==============================================================================
!                  Données pour les conditions aux limites
!==============================================================================
use Parametres, only: long, zero, un, lTra, l9, g
use, intrinsic :: iso_fortran_env, only: error_unit

use Mage_Utilitaires, only: is_NaN, zegal, do_crash
use basic_Types, only: debits_ISM, point2D, point4D
use objet_section, only: profil
use TopoGeometrie, only: la_topo, numero_bief, xgeo, aire_noeud
! FIXME: il serait bon de se débarasser de la dépendance à StVenant_Debord (voir q_uniforme())
use StVenant_Debord, only: debitance_Debord
implicit none

! valeurs des CL
type Boundary_Condition
   integer :: np !nombre de points
   integer :: ip !indice courant
   logical :: boolc !vrai si le débit d'apport est un débit surfacique
   ! NOTE: on utilise CLASS pour pouvoir utiliser n'importe quel type qui étend point2D, par exemple point4D
   class (point2D), allocatable :: bc(:)
   contains
   procedure :: init => init_bc
   procedure :: get => get_pt
end type Boundary_Condition

type(Boundary_Condition), allocatable, target :: allCL(:)

real(kind=long), allocatable :: rmv(:),smv(:),tmv(:) ! c.l. linéarisées au temps courant

contains
subroutine init_bc(my_bc,np,dim,bool)
   integer, intent(in) :: np
   integer, intent(in) :: dim
   logical, intent(in) :: bool
   class (Boundary_Condition) :: my_bc
   integer :: i
   my_bc%np = np
   my_bc%ip = 1
   my_bc%boolc = bool
   select case (dim)
      case (2) ; allocate (point2D :: my_bc%bc(np))
      case (4) ; allocate (point4D :: my_bc%bc(np))
      case default ; stop 'ERROR: unexpected dimension'
   end select
   do i = 1, np
      call my_bc%bc(i)%init
   enddo
end subroutine init_bc

function get_pt(my_bc,n)
   class (Boundary_Condition) :: my_bc
   integer, intent(in) :: n
   type (point4D) :: get_pt

   associate (bc => my_bc%bc)
      select type (bc)
         class is(point4D)
            get_pt = bc(n)
         class is(point2D)
            get_pt%x = bc(n)%x
            get_pt%y = bc(n)%y
            get_pt%u = 0._long
            get_pt%v = 0._long
      end select
   end associate
end function get_pt

subroutine IniCL
! --- Initialisation des variables du module
   implicit none
   integer, pointer :: nomax => la_topo%net%nn
   allocate (allCL(nomax),rmv(nomax),smv(nomax),tmv(nomax))
   rmv = zero ; smv = zero ; tmv = zero
   allCL(1:nomax)%np = 0 ; allCL(1:nomax)%ip = 0 ; allCL(1:nomax)%boolc = .false.
end subroutine IniCL


function cl(n,tz,zz)
!==============================================================================
!                  définition des conditions aux limites
!
!--->si n est nœud amont de modèle, cl est un débit ou une cote et tz un temps
!--->si n est nœud aval de modèle :
!         -loi q(z) : cl est un débit et tz une cote
!         -loi z(t) : cl est une cote et tz un temps
!--->si n est un nœud quelconque, cl est un débit et tz un temps
!==============================================================================
   implicit none
   ! -- le prototype --
   real(kind=long) :: cl
   integer,intent(in) :: n
   real(kind=long),intent(in) :: tz
   real(kind=long), intent(in), optional :: zz !cote de l'eau si casier pour apport distribué sur la surface
   ! -- les variables --
   integer :: ib, isa, ip
   integer, pointer :: incl
   character(len=60) :: message
   real(kind=long), parameter :: fact = 0.001_long/3600._long !conversion mm/h en m/s
   ! NOTE: le facteur de conversion a changé par rapport à Mage-7 car les surfaces des casiers sont stockées en m²
   !---------------------------------------------------------------------------
   incl => la_topo%nodes(n)%cl
   if (incl < 0) then          !--->n est un nœud avec une c.l. aval
      if (incl == -1) then                       !--->loi q(z)
         cl = cl0(n,tz)
      else if (incl == -2) then                  !--->régime uniforme
         ib = la_topo%net%numero(la_topo%net%lbamv(n))
         isa = la_topo%biefs(ib)%is2
         cl = q_uniforme(isa,tz)
!         block
!            !vérification de la croissance de q_uniforme()
!            real(kind=long), parameter :: dh = 0.05_long
!            real(kind=long) :: cl_0, cl_1
!            cl_0 = q_uniforme(isa,tz-dh)
!            cl_1 = q_uniforme(isa,tz+dh)
!            if (.not. (cl_0 < cl .and. cl < cl_1)) then
!               print*,'>>>> ATTENTION : q_uniforme non croissante au voisinage de ',tz
!            endif
!         end block
      else if (incl == -3) then                    !--->loi z(t)
         cl = zaval(n,tz)
      else
         message = '>>>> CL : erreur de type de c.l. aval au nœud '
         write(lTra,'(1x,a,a3)') trim(message),la_topo%nodes(n)%name
         write(error_unit,'(1x,a,a3)') trim(message),la_topo%nodes(n)%name
         stop 182
      endif
   else
   !--->n est un nœud avec c.l. amont ou
   !--->n est un nœud interieur (il peut recevoir un débit d'apport ou de prise)
   !--->loi q(t)
      cl = cl0(n,tz)
      if (allCL(n)%boolc) then  !cas d'un apport surfacique
         if (present(zz)) then  ! on a la cote de l'eau dans le nœud => on l'utilise pour calculer la surface du casier
            cl = cl*(aire_noeud(la_topo%nodes(n),zz)*fact)
         else                   ! on n'a pas la cote de l'eau dans le nœud => on se contente de la surface pour l'index courant
            ip = la_topo%nodes(n)%ip
            cl = cl*la_topo%nodes(n)%sne(ip)*fact
         endif
      endif
   endif
end function cl



function cl0(n,tza)
!==============================================================================
!  interpolation dans le module conditions_limites sur le tableau allCL
!
! NB: pour les nœuds amont avec C.L. de type ISM, CL0() renvoie la somme des
!     3 débits partiels
!==============================================================================
   implicit none
   ! -- prototype --
   real(kind=long) :: cl0
   integer,intent(in) :: n
   real(kind=long),intent(in) :: tza
   ! -- variables --
   integer :: it, it0, it1, jt, incl
   real(kind=long) :: qzz_it, qzz_jt
   type (point4D) :: bc
   !---------------------------------------------------------------------------
   incl = la_topo%nodes(n)%cl
   it1 = allCL(n)%np
   cl0 = huge(zero)  !on ne peut pas avoir des valeurs de CL énormes
   if (tza < allCL(n)%bc(1)%x) then
      cl0 = allCL(n)%bc(1)%y
      if (incl == 3) then
         bc = allCL(n)%get(1)
         cl0 = cl0 + bc%u + bc%v
      endif
      allCL(n)%ip = 1
   else if (tza >= allCL(n)%bc(it1)%x) then
      if (la_topo%nodes(n)%cl == -1) then  !Q(z) : prolongement linéaire
         cl0 = allCL(n)%bc(it1)%y+(allCL(n)%bc(it1)%y-allCL(n)%bc(it1-1)%y)/ &
         (allCL(n)%bc(it1)%x-allCL(n)%bc(it1-1)%x)*(tza-allCL(n)%bc(it1)%x)
      else
         cl0 = allCL(n)%bc(it1)%y                    !Q(t) et Z(t) : prolongement constant
         if (incl == 3) then
            bc = allCL(n)%get(it1)
            cl0 = cl0 + bc%u + bc%v
         endif
      endif
      allCL(n)%ip = allCL(n)%np-1
   else
      it0 = 1
      if (la_topo%nodes(n)%cl > 0) then
         it0 = max(1,allCL(n)%ip) ! si Q(t) on ne repart pas du début
         ! NOTE: on ne peut pas faire pareil avec les Z(t) car on peut avoir une Z(t) périodique (marée)
         if (tza < allCL(n)%bc(it0)%x) it0 = 1 !possible retour en arrière
      endif
      do it = it0, it1-1
         jt = it+1
         if (tza >= allCL(n)%bc(it)%x .and. tza < allCL(n)%bc(jt)%x) then
            qzz_it = allCL(n)%bc(it)%y  ;  qzz_jt = allCL(n)%bc(jt)%y
            if (incl == 3) then
               bc = allCL(n)%get(it)  ;  qzz_it = qzz_it + bc%u + bc%v
               bc = allCL(n)%get(jt)  ;  qzz_jt = qzz_jt + bc%u + bc%v
            endif
            cl0 = qzz_it+(qzz_jt-qzz_it)/(allCL(n)%bc(jt)%x-allCL(n)%bc(it)%x)*(tza-allCL(n)%bc(it)%x)
            allCL(n)%ip = it
            exit
         endif
      enddo
   end if
   if (cl0 > huge(zero)/2) stop '>>>> BUG dans cl0()'
end function cl0



function cl0_ISM(n,tza)
!==============================================================================
!  interpolation dans le module conditions_limites sur le tableau allCL
!
!   ne traite que le cas des nœuds amont avec CL de type ISM c'est-à-dire
!   donnée sous la forme de 3 débits partiels
!==============================================================================
   implicit none
   ! -- prototype --
   type(debits_ISM) :: cl0_ISM
   integer,intent(in) :: n
   real(kind=long),intent(in) :: tza
   ! -- variables --
   integer :: it, it0, it1, jt
   real(kind=long) :: alpha
   !---------------------------------------------------------------------------
   if (la_topo%nodes(n)%cl /= 3) stop 'CL0_ISM() ne traite que les nœuds amont avec CL de type ISM'
   it1 = allCL(n)%np
   cl0_ISM%ql = huge(zero) ; cl0_ISM%qm = huge(zero) ; cl0_ISM%qr = huge(zero)  !on ne peut pas avoir des valeurs de CL énormes
   associate (bc => allCL(n)%bc(:))
      select type (bc)
         class is(point4D)
            if (tza >= bc(it1)%x) then       !prolongement à droite par la valeur finale
               cl0_ISM%ql = bc(it1)%y
               cl0_ISM%qm = bc(it1)%u
               cl0_ISM%qr = bc(it1)%v
               allCL(n)%ip = allCL(n)%np-1
            else if (tza < bc(1)%x) then     !prolongement à gauche par la valeur initiale
               cl0_ISM%ql = bc(1)%y
               cl0_ISM%qm = bc(1)%u
               cl0_ISM%qr = bc(1)%v
               allCL(n)%ip = 1
            else
               it0 = max(1,allCL(n)%ip)    ! pour une Q(t) on ne repart pas du début
               if (tza < bc(it0)%x) it0 = 1 ! possible retour en arrière
               do it = it0, it1-1
                  jt = it+1
                  if (tza >= bc(it)%x .and. tza < bc(jt)%x) then
                     alpha = (tza-bc(it)%x) / (bc(jt)%x-bc(it)%x)
                     cl0_ISM%ql = bc(it)%y+(bc(jt)%y-bc(it)%y) * alpha
                     cl0_ISM%qm = bc(it)%u+(bc(jt)%u-bc(it)%u) * alpha
                     cl0_ISM%qr = bc(it)%v+(bc(jt)%v-bc(it)%v) * alpha
                     allCL(n)%ip = it
                     exit
                  endif
               enddo
            end if
         class is(point2D)
            stop 'CL0_ISM() ne traite que les nœuds amont avec CL de type ISM'
      end select
   end associate
   if (cl0_ISM%ql+cl0_ISM%qm+cl0_ISM%qr > huge(zero)/2) stop '>>>> BUG dans cl0_ISM()'
end function cl0_ISM


function q_uniforme(is,z)
!==============================================================================
! calcul du débit en régime uniforme en fonction du niveau dans la section is
!
! z = cote de l'eau dans la section courante is
!==============================================================================
   implicit none
   ! -- prototype
   real(kind=long) :: q_uniforme
   integer, intent(in) :: is         ! numéro de la section
   real(kind=long), intent(in) :: z  ! niveau
   ! -- variables
   real(kind=long) :: pente, dx, dz, Kmin, Kmoy, Debt
   integer :: ib
   character(len=180) :: err_message
   !---------------------------------------------------------------------------
   dz = la_topo%sections(is-1)%zf-la_topo%sections(is)%zf
   dx = abs(la_topo%sections(is-1)%pk-la_topo%sections(is)%pk)
   if (dx < 0.01_long) then   !on néglige les pas d'espace trop petits ; 1er essai
      dx = abs(la_topo%sections(is-2)%pk-la_topo%sections(is)%pk)
      dz = la_topo%sections(is-2)%zf-la_topo%sections(is)%zf
   endif
   if (dx < 0.01_long) then   ! 2e essai
      dx = abs(la_topo%sections(is-3)%pk-la_topo%sections(is)%pk)
      dz = la_topo%sections(is-3)%zf-la_topo%sections(is)%zf
   endif
   pente = dz / dx
   if (pente < zero) then
      ib = numero_bief(is)
      write(err_message,'(a,f10.2,a,i3,a)') '>>> contrepente au pm ',xgeo(is), &
                           ' du bief ',ib,' : calcul q(z) uniforme impossible'
      write(error_unit,'(a)') trim(err_message) ; write(l9,'(a)') trim(err_message)
      write(lTra,'(a)') trim(err_message)
      write(err_message,'(a)') '>>> arrêt dans q_uniforme <<<'
      write(error_unit,'(a)') trim(err_message) ; write(l9,'(a)') trim(err_message)
      write(lTra,'(a)') trim(err_message)
      stop 115
   else
      ! FIXME: il faut adapter le calcul du régime uniforme au cas ISM
      Kmin = la_topo%sections(is)%ks(la_topo%sections(is)%main)
      Kmoy = 0.5_long*(la_topo%sections(is)%ks(1)+la_topo%sections(is)%ks(3))
      Debt = debitance_Debord(is,z,Kmin,Kmoy)
      if (is_NaN(Debt)) stop 'NaN dans Q_uniforme / Débitance'
      if (is_NaN(pente)) stop 'NaN dans Q_uniforme / pente'
      q_uniforme = sqrt(pente)*Debt
   endif
end function q_uniforme



function qcrit(n,z)
!==============================================================================
!                   calcul du débit en fonction de la cote
!                pour une c.l. aval en cote critique (froude = 1.)
!
! z = cote de l'eau au nœud n
!==============================================================================
   implicit none
   ! -- prototype --
   real(kind=long) :: qcrit
   integer,intent(in) :: n
   real(kind=long),intent(in) :: z
   ! -- variables --
   integer :: is, ib
   real(kind=long) :: y, al, s, ymax
   type(profil), pointer :: sec_is

   qcrit = huge(zero)
   ib = la_topo%net%numero(la_topo%net%lbamv(n))
   is = la_topo%biefs(ib)%is2 !section aval du bief qui arrive au nœud n
   sec_is => la_topo%sections(is)
   y = z-sec_is%zf
   if (y > zero) then
      ymax = sec_is%ybmax+100._long  !tirant d'eau maximal de la section is
      if (y > ymax) call war001(is,y,ymax,0) !interpolation impossible

      al = sec_is%largeur(z)           !largeur au miroir
      s  = sec_is%section_mouillee(z)  !section mouillée

      qcrit = sqrt(g*s*s*s/al)
   else
      !si y est NaN la comparaison avec zero est toujours fausse
      call err025(is,y,4)
   endif
   if (qcrit > huge(zero)/2) stop '>>>> BUG dans Qcrit()'
end function qcrit



function zaval(n,ti)
!==============================================================================
!              définition des limnigrammes aval lus sur .lim
!==============================================================================
   implicit none
   ! -- le prototype --
   real(kind=long) :: zaval
   integer,intent(in) :: n
   real(kind=long),intent(in) :: ti
   ! -- les variables --
   integer :: it0,it1
   real(kind=long) :: p, time
   !----------------------------------------------------------------------------
   it0 = 1
   it1 = allCL(n)%np
   if (zegal(allCL(n)%bc(it0)%y,allCL(n)%bc(it1)%y,un) .and. it1>it0) then  !limnigramme périodique non constant
      p = allCL(n)%bc(it1)%x-allCL(n)%bc(it0)%x
      time = ti-real(floor((ti-allCL(n)%bc(it0)%x)/p),kind=long)*p
    else                                              !cas général
      time = ti
   endif
   zaval = cl0(n,time)
end function zaval

end module Conditions_Limites


!NOTE: il faut garder les routines qui suivent en dehors des modules parce qu'elles utilises d'autres modules non liés aux C.L.
subroutine cl_Amont
!==============================================================================
!       actualisation des conditions aux limites amont
!==============================================================================
   use parametres, only: long, zero, un
   use casiers, only: qclat
   use conditions_limites, only: rm=>rmv,sm=>smv,tm=>tmv, cl
   use data_num_mobiles, only: t
   use hydraulique, only: qt
   use TopoGeometrie, only: la_topo
   implicit none
   ! -- les variables --
   integer :: n, ib
   !---------------------------------------------------------------------------
   call actu_Qlat_Casier  ! Actualisation des débits perdus par déversements
                          ! latéraux vers les casiers ; initialise qclat à 0
   do n = 1, la_topo%net%nn
      if (la_topo%nodes(n)%cl > 0) then
         ib = la_topo%net%numero(la_topo%net%lbamv(n))
         rm(n) = un
         sm(n) = zero
         tm(n) = cl(n,t)+qclat(n)-qt(la_topo%biefs(ib)%is1)
      endif
   enddo

end subroutine cl_Amont



function qlat(n,time)
!==============================================================================
!             définition des hydrogrammes latéraux lus sur .lat
!
! TODO: se débarasser du fichier à accès direct <etude>_qla (l91)
!==============================================================================
   use parametres, only: long, zero, lTra, l91
   use, intrinsic :: iso_fortran_env, only: error_unit

   use apports_lateraux, only: nlat,itm=>itl
   use PremierAppel, only : nap => nap_qlat
   use TopoGeometrie, only: la_topo
   implicit none
   ! -- le prototype --
   real(kind=long) :: qlat
   integer,intent(in) :: n
   real(kind=long),intent(in) :: time
   ! -- les variables --
   integer :: irec, nl
   integer,save,allocatable :: krec(:)
   real(kind=long),save,allocatable :: dql(:),ql(:),tl1(:),tl2(:)
   real(kind=long) :: q1n, q2n, t1n, t2n
   character(len=180) :: err_message
   integer, pointer :: ibmax
   !---------------------------------------------------------------------------
   qlat = huge(zero)
   if (n > nlat) then
      qlat = zero  ;  return
   endif
   ibmax => la_topo%net%nb
   if (nap < 1) then ! 1er appel -> initialisation
      allocate (dql(nlat),ql(nlat),tl1(nlat),tl2(nlat),krec(nlat))
      do nl = 1, nlat
         dql(nl) = zero
         tl1(nl) = -1.e+30_long
         krec(nl) = itm(nl-1)+1
         read(l91,rec=krec(nl)) tl2(nl),ql(nl)
         !---cas où le secteur nl est représenté par un seul point
         if (itm(nl)==itm(nl-1)+1) tl2(nl) = +1.e+30_long
      enddo
      nap = nap+1
   endif

   if (time <= tl2(n) .and. time >= tl1(n)) then
      qlat = ql(n)+dql(n)*(time-tl1(n))
      return
   else if (time > tl2(n)) then
      !--->il faut relire selon les temps croissants
      !------>krec(n) est le numéro du dernier enregistrement lu sur inimage.qla
      !       il correspond à tl2(n) : il est inutile de relire le début du fichier
      read(l91,rec=krec(n)) t1n,q1n
      if (krec(n) == itm(n)) then
         !------on est au bout de la liste pour le secteur n -> qlat est constant
         qlat = q1n
         ql(n)  = q1n  ;  dql(n) = zero
         tl1(n) = t1n  ;  tl2(n) = 1.e+30_long
         return
      endif
      do irec = krec(n)+1,itm(n)
         read(l91,rec=irec) t2n,q2n
         if (time >= t1n .and. time <= t2n) then
            ql(n) = q1n
            dql(n) = (q2n-q1n)/(t2n-t1n)
            tl1(n) = t1n  ;  tl2(n) = t2n
            qlat = q1n+dql(n)*(time-t1n)
            krec(n) = irec
            return
         else if (time > t2n) then
            t1n = t2n  ;  q1n = q2n
         else
            write(err_message,'(a)')' >>>> erreur 1 dans la lecture de inimage.qla'
            write(error_unit,'(a)') trim(err_message) ; write(lTra,'(a)') trim(err_message)
            write(err_message,'(a)') 'Merci d''envoyer un rapport de bug'
            write(error_unit,'(a)') trim(err_message) ; write(lTra,'(a)') trim(err_message)
            stop 116
         endif
      enddo
      krec(n) = itm(n)
      !------on est au bout de la liste pour le secteur n -> qlat est constant
      qlat = q2n
      ql(n) = q2n   ;  dql(n) = zero
      tl1(n) = t1n  ;  tl2(n) = 1.e+30_long
      return
   else if (time < tl1(n)) then
      !--->il faut relire selon les temps décroissants
      read(l91,rec=krec(n)) t2n,q2n
      if (krec(n) == itm(n-1)+1) then
         !------on est au début de la liste pour le secteur n -> qlat est constant
         qlat = q2n
         ql(n) = q2n  ;  dql(n) = zero
         tl1(n) = -1.e+30_long  ;  tl2(n) = t2n
         return
      endif
      do irec = krec(n)-1,itm(n-1)+1,-1
         read(l91,rec=irec) t1n,q1n
         if (time >= t1n .and. time <= t2n) then
            ql(n) = q1n
            dql(n) = (q2n-q1n)/(t2n-t1n)
            tl1(n) = t1n  ;  tl2(n) = t2n
            qlat = q1n+dql(n)*(time-t1n)
            krec(n) = irec+1
            return
         else if (time < t1n) then
            t2n = t1n  ;  q2n = q1n
         else
            write(err_message,'(a)') ' >>>> erreur 2 dans la lecture de inimage.qla'
            write(error_unit,'(a)') trim(err_message) ; write(lTra,'(a)') trim(err_message)
            write(err_message,'(a)') 'Merci d''envoyer un rapport de bug'
            write(error_unit,'(a)') trim(err_message) ; write(lTra,'(a)') trim(err_message)
            stop 117
         endif
      enddo
      krec(n) = itm(n-1)+1
      !------on est au début de la liste pour le secteur n -> qlat est constant
      qlat = q1n
      ql(n) = q1n  ;  dql(n) = zero
      tl1(n) = -1.e+30_long  ;  tl2(n) = t1n
      return
   endif
   if (qlat > huge(zero)/2) stop '>>>> BUG dans Qlat()'
end function qlat



subroutine apport(indic)
!==============================================================================
!         actualisation des apports/fuites latéraux
! qe(is)  = débit latéral à t-dt (fin du pas de temps précédent)
! dqe(si) = variation du débit latéral entre t-dt et t (pas de temps courant)
!==============================================================================
   use parametres, only: long, zero, un, lTra
   use, intrinsic :: iso_fortran_env, only: error_unit

   use mage_utilitaires, only: zegal, mean
   use apports_lateraux, only: nlat, nsect
   use booleens, only: booll
   use data_num_fixes, only: tinf
   use data_num_mobiles, only: t,dt
   use hydraulique, only: y=>yt, qe, dqe
   use TopoGeometrie, only: la_topo, xgeo, alfn
   use IO_Files, only: latFile
   implicit none
   ! -- prototype --
   integer, intent(in) :: indic !>0 convergence, <0 retour en arrière, =0 divergence
   ! -- les variables --
   integer :: is, isa, isz, n
   real(kind=long) :: dql, dx, seva, ql, ql0, qlat
   character(len=180) :: err_message
   !---------------------------------------------------------------------------
   if (latFile == '') return

   if (zegal(t,tinf,un)) then  !---initialisation de qe et dqe (fait par appel de
                               !   apport par le programme principal)
      qe(1:la_topo%net%ns) = zero
      dqe(1:la_topo%net%ns) = zero
      do n = 1, nlat
         isa = nsect(n,1)
         isz = nsect(n,2)
         ql = qlat(n,t)
         if (booll(n)) then
            !--->cas d'un bief soumis à ETP ou pluie
            seva = zero
            do is = isa, isz
               dx = abs(xgeo(is+1)-xgeo(is))
               !seva = seva+0.5_long*dx*( alfn(y(is),is)+alfn(y(is+1),is+1) )
               seva = seva+dx*mean( alfn(y(is),is),alfn(y(is+1),is+1) )
            enddo
            ql = ql*((seva*0.00001_long)/36._long)
         endif
         qe(isa:isz) = qe(isa:isz)+ql
         dqe(isa:isz) = zero
      enddo

   else if (t>tinf) then  !---calcul normal (t > tinf)
      do n = 1, nlat
         isa = nsect(n,1)
         isz = nsect(n,2)
         if (indic > 0) then        !--->il y a eu convergence : il faut incrémenter
            ql0 = qe(isa)+dqe(isa)
          else if (indic < 0) then  !--->il y a eu retour en arrière : il faut tout recalculer
            ql0 = qlat(n,t-dt)
          else                    !--->il y a eu divergence : il ne faut pas incrémenter
            ql0 = qe(isa)
         endif
         ql = qlat(n,t)
         if (booll(n)) then       !--->cas d'un bief soumis à ETP ou pluie
            seva = zero
            do is = isa, isz
               dx = abs(xgeo(is+1)-xgeo(is))
               !seva = seva+0.5_long*dx*( alfn(y(is),is)+alfn(y(is+1),is+1) )
               seva = seva+dx*mean( alfn(y(is),is),alfn(y(is+1),is+1) )
            enddo
            if (indic == 0) ql0 = ql0*((seva*0.00001_long)/36._long)
            ql = ql*((seva*0.00001_long)/36._long)
         endif
         dql = ql-ql0
         if (indic /= 0) qe(isa:isz) = ql0
         dqe(isa:isz) = dql
      enddo

   else
      write(err_message,'(a)') ' >>> erreur dans le sous-programme apport() : t < tinf'
      write(error_unit,'(a)') trim(err_message) ; write(lTra,'(a)') trim(err_message)
      write(err_message,'(a)') 'Merci d''envoyer un rapport de bug'
      write(error_unit,'(a)') trim(err_message) ; write(lTra,'(a)') trim(err_message)
      stop 183
   endif

end subroutine apport



! FIXME: doublon partiel entre actualize_QD() et actu_Qlat_Casier()
subroutine actu_Qlat_Casier
!==============================================================================
!   actualisation des débits échangés avec les casiers par déversement latéral
!
! qds0(is) = débit déversé à t-dt
! qds1(is) = débit déversé à t
! qdb0(ib) = volume déversé à t-dt
! qdb1(ib) = volume déversé à t
!==============================================================================
   use parametres, only: long, zero, rivg, rivd
   use casiers, only: nclat, qds1, qclat, devlat
   use TopoGeometrie, only: la_topo, xgeo
   implicit none
   ! -- variables locales temporaires --
   integer :: neu, is
   !------------------------------------------------------------------------------
   !actualisation des volumes déversés par casier
   !ne concerne pas les déversements vers l'extérieur (indice 0 de qclat)
   qclat(1:la_topo%net%nn) = zero

   if (devlat(rivg)) then   ! incrémentation du volume déversé par casier
      do is = 1, la_topo%net%ns
         neu = nclat(rivg,is)
         if (neu > 0) then !dans ce cas is deverse vers le nœud neu
            qclat(neu) = qclat(neu) + qds1(rivg,is)*abs(xgeo(is+1)-xgeo(is))
         endif
      enddo
   endif

   if (devlat(rivd)) then   ! incrémentation du volume déversé par casier
      do is = 1, la_topo%net%ns
         neu = nclat(rivd,is)
         if (neu > 0) then !dans ce cas is déverse vers le nœud neu
            qclat(neu) = qclat(neu) + qds1(rivd,is)*abs(xgeo(is+1)-xgeo(is))
         endif
      enddo
   endif

end subroutine Actu_Qlat_Casier



subroutine lateral_exchange(reduire_dt)
!==============================================================================
!                 mise à jour des débits d'échanges latéraux
!
!   Cette routine est appelée par time_iterations() pour la discrétisation et
!   update_NL_iteration() pour mise à jour lors des itérations de N-R.
!   Si reduire_dt prend la valeur vrai cela signifie que le pas de temps courant
!   est trop grand pour la dynamique du déversement latéral
!
!---modifications :
!   10/03/2007 : suppression de la sortie anticipée si dtlat < dt, il faut
!                continuer pour terminer la mise à jour de qds1 même si
!                reduire_dt vrai va provoquer un retour en arrière.
!==============================================================================
   use parametres, only: long, toutpetit, rivg, rivd, total, zero
   use data_num_fixes, only: dtbase
   use casiers, only: qds1, cdlat, nclat, rvclat, dtlat, devlat
   use data_num_mobiles, only: dt
   use TopoGeometrie, only: la_topo, xgeo
   implicit none
   ! -- le prototype --
   logical,intent(out) :: reduire_dt
   ! -- interface
   interface
      function qd_actu (is,rive,dtl)
         use parametres, only: long
         real(kind=long) :: qd_actu
         integer,intent(in) :: is, rive
         real(kind=long),intent(out) :: dtl
      end function qd_actu
   end interface
   ! -- variables locales
   integer :: rive, ib, is, cible
   real(kind=long) :: dtl
   integer, pointer :: ismax
   !------------------------------------------------------------------------------
   reduire_dt = .false.  ;  dtlat = dtbase
   ismax => la_topo%net%ns
   qds1(total:rivd,1:ismax) = zero   ! remise à zéro

   do rive = rivg, rivd
      do ib = 1, la_topo%net%nb
         do is = la_topo%biefs(ib)%is1, la_topo%biefs(ib)%is2-1
            if (la_topo%sections(is)%iss /= 0) then
               cycle
            elseif (cdlat(rive,is) > toutpetit) then
               qds1(rive,is) = qd_actu(is,rive,dtl)
               dtlat = min(dtlat,dtl)
               reduire_dt = reduire_dt .or. dtlat < dt
            else
               cycle
            endif
         enddo
      enddo
   enddo

   do is = 1, ismax   !symétrisation dans le cas des échanges entre biefs
      do rive = rivg, rivd
         if (nclat(rive,is) < 0) then
            cible = -nclat(rive,is)
            if (la_topo%sections(cible+1)%iss == 0 .and. abs(qds1(rive,is)) > toutpetit) then
               qds1(rvclat(rive,is),cible) = -qds1(rive,is) * abs(xgeo(is+1)-xgeo(is))  &
                                                      / abs(xgeo(cible+1)-xgeo(cible))
            endif
         endif
      enddo
   enddo

   qds1(total,1:ismax) = qds1(rivg,1:ismax) + qds1(rivd,1:ismax)
   do rive = rivg, rivd
      devlat(rive) = maxval(qds1(rive,1:ismax)) > toutpetit
   enddo

end subroutine lateral_exchange



function qd_actu(is,rive,dtl)
!==============================================================================
!              évaluation du débit d'échange latéral
!
! entrée : numéro de section is et rive (gche ou dte) rive
! sortie : qd_actu  (= qds1(rive,is) et dtl
! le débit est positif si l'écoulement va de la section origine vers la cible
!
!==============================================================================
   use parametres, only: long, toutpetit, zero, un, lTra
   use, intrinsic :: iso_fortran_env, only: error_unit

   use casiers, only: cdlat, ydev, htraff, pceaff, nclat
   use hydraulique, only: zt, yt
   use solution, only: dz
   use data_num_fixes, only: dtbase
   use TopoGeometrie, only: la_topo, xgeo, numero_bief, zfd, volume_noeud, aire_noeud, node
   implicit none
   ! -- le prototype --
   real(kind=long) :: qd_actu
   integer,intent(in) :: is, rive
   real(kind=long),intent(out) :: dtl
   ! -- interface
   interface
      function debq(h1,h2,cd)
         use parametres, only: long
         real(kind=long), intent(in) :: h1, h2, cd
         real(kind=long) :: debq
      end function debq
   end interface
   ! -- variables locales
   integer :: cible, ib, js, kb
   real(kind=long) :: h_origine, h_cible, qd1, qd2, h_haut, h_bas
   real(kind=long) :: echange, vol_a_vider, vol_a_remplir, volmax
   real(kind=long), parameter :: geant=1.e+30_long
   real(kind=long), parameter :: toosmall=1.0e-5_long
   real(kind=long) :: z_origine, z_cible
   character(len=180) :: err_message
   type(node), pointer :: noeud
   !------------------------------------------------------------------------------
   volmax = geant  ;  h_cible = geant  ;  dtl = geant
   ! localisation de la cible (bief ou casier) pour avoir la cote aval du déversement
   if (nclat(rive,is) < 0) then           !échange avec un autre bief
      cible = -nclat(rive,is)             ! section cible
    elseif ( nclat(rive,is) > 0) then     !échange avec un casier
      kb = la_topo%net%lbz(nclat(rive,is),1)  !premier bief qui part du casier
      ib = la_topo%net%numero(kb)
      cible = la_topo%biefs(ib)%is1           !section amont du 1er bief qui part du casier
    else                                  !déversement vers l'extérieur du réseau
      cible = 0 ; h_cible = -9999._long
   endif

   ! débit sans affaissements
   h_origine = yt(is) + dz(is) - ydev(rive,is)   !hauteur dans la section origine
   if (nclat(rive,is) /= 0) then
      z_cible = zt(cible) + dz(cible)
      h_cible = z_cible - zfd(is) - ydev(rive,is)
   else
      h_cible = -geant  ;  z_cible = -geant
   endif
   h_haut = max(h_origine, h_cible) ; h_bas = min(h_origine, h_cible)
   qd1 = debq(h_haut,h_bas,cdlat(rive,is))

   ! débit avec affaissements
   h_origine = yt(is) + dz(is) - htraff(rive,is)
   if (nclat(rive,is) /= 0) then
      z_cible = zt(cible) + dz(cible)
      h_cible = z_cible - zfd(is) - htraff(rive,is)
   else
      h_cible = -geant  ;  z_cible = -geant
   endif
   h_haut = max(h_origine, h_cible) ; h_bas = min(h_origine, h_cible)
   qd2 = debq(h_haut,h_bas,cdlat(rive,is))

   ! débit total en valeur absolue
   qd_actu = qd1 + pceaff(rive,is)*(qd2 - qd1)
   if (qd_actu < toosmall) then
      qd_actu = zero
      return
   endif

   z_origine = zt(is)+dz(is)
   echange = sign(un, z_origine-z_cible)  !sens de l'échange latéral

   ! limitation du pas de temps en fonction du volume disponible du coté à vider
   !                                     et du volume libre du coté à remplir
   !     les hauteurs origine et cible imposent un débit
   !     les volumes disponible et libre limitent la durée pendant laquelle ce débit sera tenu

   if (echange > zero) then            !===>échange origine --> cible
   ! le volume disponible est à prendre coté origine : vol_a_vider
   ! le volume libre est à prendre coté cible        : vol_a_remplir
      vol_a_vider = la_topo%sections(is)%surface_mouillee_cote(z_origine) &
                  - la_topo%sections(is)%surface_mouillee_cote(zfd(is)+htraff(rive,is))
      vol_a_vider = vol_a_vider * abs(xgeo(is+1)-xgeo(is))
      if (nclat(rive,is) > 0) then            !!!la cible est un casier!!!
         noeud => la_topo%nodes(nclat(rive,is))
         vol_a_remplir = -volume_noeud(noeud,z_cible) + volume_noeud(noeud,geant)  !volume max du casier
         ! division par le pas d'espace pour une estimation du volume local disponible
         vol_a_remplir = vol_a_remplir / max(1._long,abs(xgeo(is+1)-xgeo(is)))
         if (vol_a_remplir < toutpetit) then  !le casier est plein : on sort
            qd_actu = zero  ;  return
         endif
      elseif (nclat(rive,is) < 0) then        !!!la cible est un bief!!!
         vol_a_remplir = la_topo%sections(cible)%surface_mouillee_cote(z_origine)&
                       - la_topo%sections(cible)%surface_mouillee_cote(z_cible)
         vol_a_remplir = vol_a_remplir * abs(xgeo(is+1)-xgeo(is))
      else                                    !!!la cible est l'exterieur du réseau!!!
         vol_a_remplir = geant
      endif
   else                       !===>échange cible --> origine (retour)
      ! le volume disponible est à prendre coté cible
      ! le volume libre est à prendre coté origine
      vol_a_remplir = la_topo%sections(is)%surface_mouillee_cote(z_cible)&
                    - la_topo%sections(is)%surface_mouillee_cote(z_origine)
      vol_a_remplir = vol_a_remplir * abs(xgeo(is+1)-xgeo(is))
      if (nclat(rive,is) > 0) then        !!!la cible est un casier!!!
         !volume dans le casier compris entre la cote du casier et la cote du mineur
         noeud => la_topo%nodes(nclat(rive,is))
         vol_a_vider = volume_noeud(noeud,z_cible) - volume_noeud(noeud,z_origine)
         vol_a_vider = vol_a_vider * abs(xgeo(is+1)-xgeo(is)) / sqrt(aire_noeud(noeud,z_cible))
         if (vol_a_vider < toutpetit) then  !le casier est vide
            qd_actu = zero  ;  return
         endif
      elseif (nclat(rive,is) < 0) then    !!!la cible est un bief!!!
         vol_a_vider = la_topo%sections(cible)%surface_mouillee_cote(z_cible)&
                     - la_topo%sections(cible)%surface_mouillee_cote(zfd(is)+htraff(rive,is))
         vol_a_vider = vol_a_vider * abs(xgeo(is+1)-xgeo(is))
      else                                !!!la cible est l'extérieur du réseau!!!
         write(err_message,'(a)') ' >>>> erreur dans qd_actu <<<<' !pas de retour possible depuis l'extérieur
         write(error_unit,'(a)') trim(err_message) ; write(lTra,'(a)') trim(err_message)
         write(err_message,'(a)') 'Merci d''envoyer un rapport de bug'
         write(error_unit,'(a)') trim(err_message) ; write(lTra,'(a)') trim(err_message)
         ib = numero_bief(is)  ;  js = is-la_topo%biefs(ib)%is1+1  !numéro de section local dans le bief
         write(lTra,*)  ib, js, xgeo(is), z_origine, z_cible
         stop 118
      endif
   endif

   if (vol_a_vider < toutpetit .or. vol_a_remplir < toutpetit) then
      dtl = dtbase
      qd_actu = zero
   else
      dtl = 0.8_long * min(vol_a_vider/qd_actu,vol_a_remplir/qd_actu)
      qd_actu = echange * qd_actu
   endif

end function qd_actu



function debq(h1,h2,coeff)
!==============================================================================
!           calcul du débit (linéïque en m2/s) pour un déversement latéral
!
! h1 = hauteur d'eau amont au dessus du déversoir
! h2 = hauteur d'eau aval au desus du déversoir
! coeff = coefficient de débit
!==============================================================================
   use parametres, only: long, g2, zero, deuxtiers
   use Mage_Utilitaires, only: sqrt2
   implicit none
   ! -- le prototype --
   real(kind=long) :: debq
   real(kind=long),intent(in) :: h1, h2, coeff
   ! -- les constantes --
   real(kind=long), parameter :: c1=3._long*sqrt(3._long)/2._long
   !---------------------------------------------------------------------------
   if (h1 < zero .or. h2 >= h1) then
      debq = zero
   else if (h2 < deuxtiers*h1) then  !régime surface libre dénoyé
      debq = coeff*h1*sqrt2(g2*h1)
   else                              !régime surface libre noyé
      debq = coeff*c1*h2*sqrt2(g2*(h1-h2))
   endif

end function debq



subroutine devers(dtr,bool)
!==============================================================================
!   estimation du pas de temps compatible avec le volume
!         disponible pour un déversement latéral
!
! bool est mis à vrai s'il faut réduire le pas de temps
! dtr est le pas de temps maximum possible estimé par devers()
!==============================================================================
   use parametres, only: long
   use casiers, only: dtlat
   use data_num_fixes, only: dtbase
   implicit none
   ! -- prototype --
   real(kind=long), intent(out) :: dtr
   logical, intent(out) :: bool
   !---------------------------------------------------------------------------
   ! NOTE: bool est toujours faux ; reliquat ancienne version
   ! NOTE: à garder au cas où on aurait besoin pour forcer un retour en arrière
   bool = .false.
   if (dtlat < dtbase-10._long) then
      dtr = 0.9_long*dtlat
   else
      dtr = dtbase
   endif

end subroutine devers



subroutine Lire_CL(type_CL)
!===============================================================================
!       programme de lecture des fichiers .HYD , .LIM , .AVA
!===============================================================================
   use parametres, only: long, nosup, zero, lTra
   use, intrinsic :: iso_fortran_env, only: error_unit, output_unit

   use data_num_fixes, only: tinf,tmax,date_format
   use data_tmp_cl, only: t,q,itm,jtm,ql,qr, boolc
   use Data_Num_Mobiles, only: tmax_cl
   use IO_Files, only: hydFile, limFile, avaFile
   use premierappel, only : firstCall_LireCL
   use TopoGeometrie, only: la_topo
   use Mage_Utilitaires, only: ecr0, next_int, next_real, next_string, do_crash, lire_date
   implicit none
   ! -- prototype --
   character(len=3), intent(in) :: type_CL
   ! -- variables locales permanentes --
   character(len=31), parameter :: bl = repeat(' ',31)
   logical, save, allocatable :: booln(:)
   integer, save :: kn = 0
   ! -- variables locales temporaires --
   real(kind=long), save, allocatable :: vol(:)
   logical :: is_first_definition_for_this_node,node_found
   integer :: n, ik, jtmn, k, kn0, ligne, luu, ios, nf
   character :: etpp,u3*3,chain*14,u2*99,info*120
   real(kind=long) :: q0, q1, qz, t0, t1, ts, tt, ttmax, vtot, scale_factor
   real(kind=long) :: q_l, q_m, q_r
   character(len=20) :: sT

   node_found = .false.
   t0 = huge(zero) ; q0 = huge(zero)

   if (firstCall_LireCL) then   ! on ne repassera pas une 2e fois ici
      allocate (vol(la_topo%net%nn),booln(la_topo%net%nn))
      !--->ITM(N) = rang de lecture pour le noeud N
      !--->JTM(KN) = dernier point pour le noeud de rang de lecture KN
      itm(0) = 0  ;  itm(1:) = -1  ;  jtm(0) = 0  ;  kn = 0
      booln(1:) = .false.

      firstCall_LireCL = .false.
   endif

   ! ouverture du fichier de CL de type type_CL
   select case (type_CL)
      case ('HYD')
         open(newunit=luu,file=trim(hydFile),status='old',form='formatted')
         vol = 0._long  !initialisation des volumes injectes
      case ('LIM')
         open(newunit=luu,file=trim(limFile),status='old',form='formatted')
      case ('AVA')
         open(newunit=luu,file=trim(avaFile),status='old',form='formatted')
      case default
         write(error_unit,'(2a)') '>>>> ERREUR : type de CL inconnu : ', type_CL
         info = ' >>>> Erreur dans la routine Lire_CL() ; Merci d''envoyer un rapport de bug'
         write(lTra,'(a)') trim(info)  ;  write(error_unit,'(a)') trim(info)
         call do_crash('LireCL()')
         luu = -1
   end select

   ! lecture du fichier de CL
   ligne = 0
   is_first_definition_for_this_node = .true.
   n = 0  !si n reste nul c'est qu'il y a un bug
   do
      ligne = ligne+1
      read(luu,'(a)',iostat=ios) u2
      if (ios > 0) then
         write(output_unit,*) ' erreur de lecture à la ligne ',ligne
         exit
      elseif (ios < 0) then
         exit
      endif
      !---on saute les lignes blanches
      if (u2 == bl) then
         cycle
      elseif (u2(1:1) == '*') then
         cycle
      elseif (u2(1:1) == '$') then
         !nouveau noeud : KN = rang de lecture des lois des nœuds
         !KN est sauvegardé d'un appel de Lire_CL à l'autre
         kn = kn+1
         !recherche du numéro du nœud à partir du nom du nœud (table TC)
         read(u2,'(1x,a)') u3
         node_found = .false.
         do n = 1, la_topo%net%nn
            if (la_topo%nodes(n)%name == u3) then
               !vérification que la loi est valide
               ! Q(t) pour nœud non-aval et Z(t) ou Q(Z) pour nœud aval
               if (type_CL == 'HYD' .and. la_topo%nodes(n)%cl<0) then
                  write(info,'(3a)') ' >>>> Le nœud ',u3,' est nœud aval : loi Q(t) impossible <<<<'
                  write(error_unit,'(a)') trim(info)  ;  write(lTra,'(a)') trim(info)
                  stop 154
               else if (type_CL == 'AVA' .and. la_topo%nodes(n)%cl >= 0) then
                  write(info,'(3a)') ' >>>> le nœud ',u3,' est nœud non aval : loi q(z) impossible <<<<'
                  write(error_unit,'(a)') trim(info)  ;  write(lTra,'(a)') trim(info)
                  stop 155
               else if (type_CL == 'LIM' .and. la_topo%nodes(n)%cl >= 0) then
                  write(info,'(3a)') ' >>>> le nœud ',u3,' est nœud non aval : loi z(t) impossible <<<<'
                  write(error_unit,'(a)') trim(info)  ;  write(lTra,'(a)') trim(info)
                  stop 156
               endif
               !vérification que le nœud n'a pas déjà reçu une loi
               if (booln(n)) then
                  write(lTra,'(3a)') ' >>>> Le noeud ',u3,' a déjà reçu une loi. On garde la première loi <<<<'
                  is_first_definition_for_this_node = .false.
                  cycle
               else
                  booln(n) = .true.
                  is_first_definition_for_this_node = .true.
               endif

               ! TS temps de synchronisation, indic d'ETP, facteur multiplicatif
               read(u2,'(10x,f10.0,a1,f10.0)') ts,etpp,scale_factor
               !conversion de ts en secondes (supposé donné en minutes) si Q(t) ou Z(t)
               if (type_CL == 'LIM' .or. type_CL == 'HYD') ts = ts * 60._long

               !TODO revoir la lecture avec le bloc suivant:
!                if (type_CL == 'LIM' .or. type_CL == 'HYD') then
!                   nf = 2
!                   sT = next_string(u2,'',nf)
!                   ts = real(lire_date(sT,tinf,date_format),kind=long)
!                else ! type_CL == 'AVA'
!                   ts = next_real(u2,'',nf)
!                endif
!                etpp = next_string(u2,'',nf)
!                scale_factor = next_real(u2,'',nf)

               !débit surfacique en mm/heure seulement si le nœud est un nœud intérieur
               boolc(n) = ((la_topo%nodes(n)%cl == 0) .and. (type_CL == 'HYD') .and.            &
                           (etpp == 'e' .or. etpp == 'p' .or. etpp == 'E' .or. etpp == 'P'))

               if (u2(22:31) == bl(1:10)) scale_factor = 1._long
               !initialisation de jtm
               if (kn>1) then
                  jtm(kn) = jtm(kn-1)
               else
                  jtm(kn) = 0
               endif
               t0 = tinf
               q0 = -1.e+30_long
               node_found = .true.
               exit
            endif
         enddo
         if (node_found) cycle !on passe à la lecture de la ligne suivante
         !---fin des vérifications et de l'initialisation pour le nœud
         write(info,'(5a)') '>>> Erreur dans ',type_CL,' : le noeud ',U3,' n''existe pas'
         write(error_unit,'(a)') trim(info)  ;  write(lTra,'(a)') trim(info)
         stop 157
      else
         !couple temps,débit / temps,cote / cote,débit
         if (is_first_definition_for_this_node) then
            if (n == 0) stop '>>>> BUG 3 dans Lire_CL()'
            itm(n) = kn  ;  jtm(kn) = jtm(kn)+1  ;  jtmn = jtm(kn)
            ! il faut pouvoir lire des dates au-dela de 1000 jours
            !Lecture des temps
             if (type_CL == 'LIM' .or. type_CL == 'HYD') then
               nf = 1
               sT = next_string(u2,'',nf)
               tt = real(lire_date(sT,tinf,date_format),kind=long)
             else ! type_CL == 'AVA'
                nf = 1
                tt = next_real(u2,'',nf)
             endif

            !Lecture des valeurs (débits ou cotes)
            if (type_CL == 'HYD') then
               q_l = next_real(u2,'',nf)
               !print*,'A > ',ligne,trim(u2),q_l,nf
               q_m = next_real(u2,'',nf)
               !print*,'B > ',ligne,trim(u2),q_m,nf
               if (nf == 0) then
                  !1 débit total
                  qz = q_l
                  q_m = q_l ; q_l = zero ; q_r = zero ! on en a besoin si l'utilisateur a mélangé des lignes avec 1 débit total
                                                      ! et des lignes avec 3 débits partiels
               else
                  !3 débits partiels :  on continue la lecture
                  q_r = next_real(u2,'',nf)
                  !print*,'C > ',ligne,trim(u2),q_r,nf
                  if (nf == 0) then
                     print*,'>>>> ERREUR dans '//trim(hydFile)//' il manque un des 3 débits partiels'
                     print*,'ligne ',ligne,' : ',trim(u2)
                     stop 3
                  endif
                  if (la_topo%nodes(n)%cl > 0) then
                     la_topo%nodes(n)%cl = 3 !CL amont de type ISM (3 débits partiels)
                     qz = -1.e+99_long !on ne devrait plus utiliser cette variable ensuite si on est dans ce cas
                  else if (la_topo%nodes(n)%cl == 0) then
                     !nœud intérieur -> on ne garde que le débit total
                     qz = q_l + q_m + q_r
                  else
                     stop '>>>> BUG 4 dans Lire_CL()'
                  endif
               endif
            else
               !CL aval de type LIM ou AVA
               qz = next_real(u2,' ',nf)
               !initialisation de q_l, q_m et q_r pour satisfaire le compilateur
               q_l = -1.E+30_long ; q_m = -1.E+30_long ; q_r = -1.E+30_long
            endif
            if (type_CL /= 'HYD') then
               !t(jtmn) est une cote si type_CL = 'AVA'
               t(jtmn) = ts+tt
               !q(jtmn) est une cote si type_CL = 'LIM'
               q(jtmn) = qz*scale_factor
               if (type_CL /= 'LIM') then
                  if (t(jtmn) > tmax_cl) tmax_cl = t(jtmn)
               endif
            else
               if ((etpp == 'e'.or. etpp == 'E') .and. qz>zero) then
                  write(output_unit,'(2a,/,a)') ' >>>> ATTENTION : le débit d''apport ponctuel au noeud ',u3, &
                                 ' est annoncé comme ETP et il est positif : MAGE change son signe'
                  qz = -qz
                  if (la_topo%nodes(n)%cl /= 0) stop '>>>> BUG 5 dans Lire_CL()'
               endif
               t(jtmn) = ts+tt  ;   t1 = t(jtmn)
               if (la_topo%nodes(n)%cl == 3 ) then
                  ql(jtmn) = q_l*scale_factor  ;  q(jtmn) = q_m*scale_factor  ;  qr(jtmn) = q_r*scale_factor
                  q1 = (ql(jtmn) + q(jtmn) + qr(jtmn))
               else
                  q(jtmn) = qz*scale_factor
                  q1 = q(jtmn)
               endif
               !mise à jour du volume de l'hydrogramme
               if (q0 > huge(zero)/2) stop '>>>> BUG 1 dans Lire_CL()'
               if (q0 < -1.e+29_long) q0 = q1 !1er débit de l'hydrogramme
               if (t0 > huge(zero)/2) stop '>>>> BUG 2 dans Lire_CL()'
               if (t1>tinf .and. t0 <= tmax) then
                  vol(n) = vol(n)+0.5_long*(min(t1,tmax)-t0)*(q1+q0)
               endif
               t0 = t1  ;  q0 = q1
               if (t0 > tmax_cl) tmax_cl = t0
            endif
            !vérification de la croissance des lois introduites
            if (jtmn>jtm(kn-1)+1) then
               if(t(jtmn)<t(jtmn-1)) then
                  write(info,'(3a)') ' >>>> Loi non croissante dans ',type_CL,' <<<<'
                  write(error_unit,'(a)') trim(info)  ;  write(lTra,'(a)') trim(info)
                  write(info,*) ' à la ligne numéro ',ligne
                  write(error_unit,'(a)') trim(info)  ;  write(lTra,'(a)') trim(info)
                  write(info,*) ' ---> supprimer les lignes blanches'
                  write(error_unit,'(a)') trim(info)  ;  write(lTra,'(a)') trim(info)
                  stop 158
               endif
               !fin de la vérification de la croissance des lois introduites
            endif
         endif
      endif
   enddo
   close (luu)

   !---affichage des volumes des hydrogrammes pour les nœuds décrits dans .HYD
   if (type_CL == 'HYD') then
!      info = repeat('-',80) ; info(1:1) = ' ' ; info(24:59) = ' VOLUMES DES HYDROGRAMMES PONCTUELS '
!      write(lTra,'(a)') info(1:80)
      write(lTra,'(/,1x,a)') 'Volumes des hydrogrammes ponctuels :'
      vtot = 0._long
      if (tmax>1.e+28_long) then
         ttmax = tinf+86400._long
      else
         ttmax = tmax
      endif
      do n = 1, la_topo%net%nn
         if (booln(n) .and. la_topo%nodes(n)%cl >= 0) then
            kn0 = itm(n)
            !volume déjà pris en compte lors de la lecture du 1er point en initialisant t0 à tinf
            ! ajout du volume entre le dernier point et tmax
            ik = jtm(kn0)
            if (t(ik)<ttmax) vol(n) = vol(n)+(ttmax-t(ik))*q(ik)
            call ecr0(chain,vol(n),k)
            if (chain(1:1)  /=  '!') then
               write(lTra,'(5x,5a)') ' Volume de l''hydrogramme injecté au noeud ',trim(la_topo%nodes(n)%name), &
                                  ' : ',chain(k:),' m3'
            else
               write(lTra,'(5x,5a)') ' Volume de l''hydrogramme injecté au noeud ',trim(la_topo%nodes(n)%name), &
                                  ' : ',chain(k:),' E+6 m3'
            endif
         else
            vol(n) = 0._long
         endif
         vtot = vtot+vol(n)
      enddo
      call ecr0(chain,vtot,k)
      if (chain(1:1)  /=  '!') then
         write(lTra,'(5x,3a)') ' Volume total des apports ponctuels : ',chain(k:),' m3'
      else
         write(lTra,'(5x,3a)') ' Volume total des apports ponctuels : ', chain(k:),' E+6 m3'
      endif
      write(lTra,'(a)') ''
      deallocate (vol)
   endif
end subroutine Lire_CL



subroutine Lire_LAT
!==============================================================================
!             Lecture des hydrogrammes latéraux sur .lat (I = 8)
!==============================================================================
   use parametres, only: long, lTra, l91, zero, l9
   use, intrinsic :: iso_fortran_env, only: error_unit, output_unit

   use Data_Num_Mobiles, only: tmax_cl
   use apports_lateraux, only: nlat,nsect,itm =>itl, iniAppLat
   use booleens, only: booll
   use data_num_fixes, only: tinf,tmax,date_format
   use IO_Files, only: repFile, latFile
   use mage_utilitaires, only: next_int,next_real, ecr0, next_string, lire_date
   use TopoGeometrie, only: la_topo, xgeo, numero_bief, section_id_from_pk
   implicit none
   ! -- Constante --
   real(kind=long),parameter :: qevmin = 1.e-2_long

   ! -- Variables locales temporaires --
   integer :: ib,ii,irec,isa,isz,iu3,k,luu,n,nligne,io_status, nf
   real(kind=long) :: dx,q0,qt,t0,t1,ts,tt,ttmax,vtot,scale_factor,xisa,xisz
   character :: etpp, chain*14,u2*80, bl*80
   character(len=180) :: err_message=''
   character(len=7) :: str_unit=''
   character(len=20) :: sT
   real(kind=long), parameter :: seuil=0.001_long
   logical :: new_range_open
   real(kind=long),allocatable :: vol(:)
   integer :: ilat
   !------------------------------------------------------------------------------
   if (latFile == '') return    ! sortie sur .lat n'existe pas
   bl = repeat(' ',80)
   ! ouverture du fichier à accés direct <etude>_qla
   open(unit=l91,file=repFile(1:index(repFile,'.')-1)//'_qla',status='unknown',access='direct',recl=2*long)

   ! lecture du fichier .LAT
   open(newunit=luu,file=trim(latFile),status='old',form='formatted')
   nligne = 0
   t0 = huge(zero) ; q0 = huge(zero) ; dx = huge(zero)
   new_range_open = .false.

   nlat = 0
   do  !boucle de lecture du fichier
      nligne = nligne+1
      read(luu,'(a)',iostat=io_status) u2
      if (io_status < 0) exit     !fin du fichier
      if (io_status > 0) then  !erreur de lecture
         write(err_message,*) ' >>>> ERREUR de lecture du fichier LAT à la ligne ',nligne
         write(error_unit,'(a)') trim(err_message)  ;  write(lTra,'(a)') trim(err_message)
         stop 160
      endif
      if (u2(1:1) == '$') then
         nlat = nlat+1
      else
        cycle
      end if
   enddo
   rewind(luu)

   !module Apports_Lateraux
   call iniAppLat(nlat)
   allocate (vol(nlat), booll(nlat))
   booll(:) = .false.
   itm(0) = 0
   ilat = 0
   nligne = 0
   do  !boucle de lecture du fichier
      nligne = nligne+1
      read(luu,'(a)',iostat=io_status) u2
      if (io_status < 0) exit     !fin du fichier
      if (io_status > 0) then  !erreur de lecture
         write(err_message,*) ' >>>> ERREUR de lecture du fichier LAT à la ligne ',nligne
         write(error_unit,'(a)') trim(err_message)  ;  write(lTra,'(a)') trim(err_message)
         stop 160
      endif
      !on saute les lignes blanches et les commentaires (* en colonne 1)
      if (u2 == bl .or. u2(1:1) == '*') cycle

      !---décodage de la ligne u2
      if (u2(1:1) == '$') then
         ! pour traiter le cas où on ne donne pas de données pour un secteur précédemment défini
         if (new_range_open) then
            ! un secteur est déjà ouvert, il n'a donc pas eu de données
            write(err_message,'(a,i3,3x,2f10.2,a)') 'Le secteur ',iu3, xisa,xisz,' ne contient aucune donnée'
            write(error_unit,'(a)') trim(err_message)  ;  write(lTra,'(a)') trim(err_message)
            stop 226
         endif
         ! Nouveau secteur : bief, section amont, section aval
         read(u2,'(1x,i3,6x,2f10.0)',iostat = io_status) iu3,xisa,xisz
         if (io_status > 0) call Erreur_Syntaxe_LAT(nligne)
         isa = section_id_from_pk (la_topo, iu3, xisa, seuil)
         isz = section_id_from_pk (la_topo, iu3, xisz, seuil)

         ilat = ilat+1
         if (ilat > nlat) then
            write(err_message,'(a,i2,a)') ' >>>> Le fichier .LAT contient plus de ',nlat,' secteurs'
            write(error_unit,'(a)') trim(err_message)  ;  write(lTra,'(a)') trim(err_message)
            stop 159
         endif

         ! Définition du nouveau secteur
         nsect(ilat,1) = isa
         nsect(ilat,2) = isz
         vol(ilat) = 0._long
         t0 = tinf
         q0 = zero
         new_range_open = .true.
         ! Longueur effective du nouveau secteur
         dx = abs(xgeo(nsect(ilat,2))-xgeo(nsect(ilat,1)))
         if (dx < 0.001_long) then
            write(output_unit,*) ' >>>> ERREUR dans .LAT <<<<'
            write(output_unit,*) ' Longueur de secteur nulle à la ligne : '
            write(output_unit,*) u2
         endif
         nsect(ilat,2) = nsect(ilat,2)-1

         ! TS temps de synchronisation, indic d'ETP, facteur multiplicatif
         read(u2,'(30x,f10.0,a1,f10.0)',iostat = io_status) ts,etpp,scale_factor
         if (io_status > 0) call Erreur_Syntaxe_LAT(nligne)
         if (u2(42:51) == bl(1:10)) scale_factor = 1._long
         if (etpp == 'e' .or. etpp == 'p' .or. etpp == 'E' .or. etpp == 'P') then
            ! débit surfacique en mm/heure
            booll(ilat) = .true.
         else
            booll(ilat) = .false.
         endif
         if (ilat>1) then
            itm(ilat) = itm(ilat-1)
         else
            itm(ilat) = 0
         endif

      elseif (u2(1:1)  /=  '*') then
         !---Lecture des couples (temps,débit)
         itm(ilat) = itm(ilat)+1
         ! il faut pouvoir lire des dates au-dela de 1000 jours
! !          if (scan(u2,':') > 0) then !la 1ère colonne est un temps et il est donné en jj:hh:mn
! !             nf = 1
! !             jj = next_int(u2,':',nf)
! !             ih = next_int(u2,':',nf)
! !             mn = next_int(u2,':',nf)
! !             qt = next_real(u2,'',nf)
! !             if (io_status > 0) call Erreur_Syntaxe_LAT(nligne)
! !             tt = real(abs(jj),kind=long)*1440._long + real(ih,kind=long)*60._long+real(mn,kind=long)
! !             if (u2(1:1) == '-' .or. jj<0) tt = -tt
! !          else
! !             read(u2,*,iostat=io_status) tt,qt
! !             if (io_status > 0) call Erreur_Syntaxe_LAT(nligne)
! !          endif
         nf = 1
         sT = next_string(u2,' ',nf)
         tt = real(lire_date(sT,tinf,date_format),kind=long)
         qt = next_real(u2,'',nf)
         if ((etpp == 'e'.or. etpp == 'E') .and. qt>0._long) then
            write(err_message,'(2(a,f8.2),i3,a)') ' >>>> ATTENTION : le débit d''apport réparti entre les Pm ', &
                  xisa,' et ',xisz,' du bief ',iu3,' est annoncé comme ETP et il est positif : MAGE change son signe'
            write(l9,'(a)') trim(err_message)  ;  write(output_unit,'(a)') trim(err_message)
            qt = -qt
         endif
         t1 = ts*60._long + tt
!          t1 = (ts+tt)*60._long
         ! NOTE: si bug 1, 2 ou 3 c'est qu'on n'est pas passé dans le cas (u2(1:1) == '$'), donc pas de définition du secteur
         if (new_range_open) then
            q0 = qt
            new_range_open = .false.
         endif
         if (t0 > huge(zero)/2) stop '>>>> BUG 3 dans Lire_LAT()'
         if (t1>tinf .and. t0 <= tmax) vol(ilat) = vol(ilat)+0.5_long*(min(t1,tmax)-t0)*(q0+qt)
         q0 = qt  ;  t0 = t1
         if (t0 > tmax_cl) tmax_cl = t0
         if (dx > huge(zero)/2) stop '>>>> BUG 4 dans Lire_LAT()'
         write(l91,rec=itm(ilat)) t1, qt/dx
      else
         cycle
      endif
   enddo
   !---On arrive ici quand on trouve la marque de fin de fichier
   close (luu)
!
!---affichage des volumes des hydrogrammes lateraux sur .TRA
   err_message = '(/,1x,22(''-''),'' VOLUMES DES HYDROGRAMMES LATERAUX '',22(''-''))'
   write(lTra,trim(err_message))
   vtot = 0._long
   if (tmax > 1.e+28_long) then
      ttmax = tinf+86400._long
   else
      ttmax = tmax
   endif
   do n = 1, nlat
      dx = abs(xgeo(nsect(n,2)+1)-xgeo(nsect(n,1)))
      irec = itm(n-1)+1
      read(l91,rec=irec) tt,qt
      if (tt>tinf) vol(n) = vol(n)+(tt-tinf)*qt*dx
      irec = itm(n)
      read(l91,rec=irec) tt,qt
      if (tt<ttmax) vol(n) = vol(n)+(ttmax-tt)*qt*dx
      call ecr0(chain,vol(n),k)
      ii = nsect(n,1)
      xisa = xgeo(ii)
      xisz = xgeo(nsect(n,2)+1)
      ib = numero_bief(ii)
      vtot = vtot+vol(n)
      if (chain(1:1) /= '!') then
         str_unit = ' m3'
      else
         str_unit = ' E+6 m3'
      endif
      write(lTra,'(a,i3,2(a,f8.2),3a)') ' Hydrogramme injecté au bief ',ib, &
                   ' du Pm ',xisa,' au Pm ',xisz,' : ',chain(k:),trim(str_unit)
   enddo
   call ecr0(chain,vtot,k)
   if (chain(1:1) /= '!') then
      str_unit = ' m3'
   else
      str_unit = ' E+6 m3'
   endif
   write(lTra,'(3a)') ' Volume total des apports latéraux : ',chain(k:),trim(str_unit)

end subroutine Lire_LAT



subroutine Erreur_Syntaxe_LAT(nligne)
!==============================================================================
!     erreur de syntaxe dans le fichier LAT
!==============================================================================
   use Parametres, only: lTra
   use, intrinsic :: iso_fortran_env, only: error_unit
   implicit none
   integer, intent(in) :: nligne

   character(len=180) :: err_message
   character(len=53) :: err_syntaxe = ' >>>> ERREUR de syntaxe dans fichier LAT à la ligne '

   write(err_message,*) trim(err_syntaxe),nligne
   write(error_unit,'(a)') trim(err_message)
   write(lTra,'(a)') trim(err_message)
   stop 160

end subroutine  Erreur_Syntaxe_LAT



subroutine Lire_TypeCL
!==============================================================================
!                Lecture du type des c.l. amont et aval
!==============================================================================
   use parametres, only: long
   use IO_Files, only: avaFile, limFile, hydfile
   use TopoGeometrie, only: la_topo
   use Data_tmp_CL, only: IniLireCL
   implicit none
   ! -- variables locales temporaires --
   integer :: n, lu1, lu2, ios, nlignes
   character :: u1
   character(len=3) :: u2=''
   !------------------------------------------------------------------------------
   do n = 1, la_topo%net%nn
      if (la_topo%nodes(n)%cl > 0) then
         la_topo%nodes(n)%cl = 1  ! hydrogramme amont global
         ! NOTE: la_topo%nodes(n)%cl est mis à 3 dans Lire_CL() si on a une CL de type ISM
      else if (la_topo%nodes(n)%cl < 0) then
         la_topo%nodes(n)%cl = -2  !initialisation des c.l. aval au régime uniforme
      endif
   enddo
   !-----affectation des vraies C.L.
   if (avaFile /= '') open(newunit=lu1,file=trim(avaFile),status='old',form='formatted')
   if (limFile /= '') open(newunit=lu2,file=trim(limFile),status='old',form='formatted')
   do n = 1, la_topo%net%nn
      if (la_topo%nodes(n)%cl < 0) then  !n est nœud aval de modèle
         if (avaFile /= '') then  !recherche des lois q(z) dans le fichier [.AVA]
            rewind lu1
            do
               nlignes = nlignes + 1
               read(lu1,'(2a)',iostat=ios) u1, u2
               if (ios /= 0) exit
               if (u1=='$' .and. u2==la_topo%nodes(n)%name) then
                  la_topo%nodes(n)%cl = -1
                  exit
               endif
            enddo
         endif
         if (limFile /= '') then  !recherche des limnigrammes dans le fichier [.LIM]
            rewind lu2
            do
               nlignes = nlignes + 1
               read(lu2,'(2a)',iostat=ios) u1,u2
               if (ios /= 0) exit
               if (u1=='$' .and. u2==la_topo%nodes(n)%name) then
                  la_topo%nodes(n)%cl = -3
                  exit
               endif
            enddo
         endif
      endif
   enddo
   if (avaFile /= '') close (lu1)
   if (limFile /= '') close (lu2)

   ! NOTE: on compte aussi les lignes des fichiers HYD, LIM et AVA pour estimer le nombre de points nécessaires pour définir toutes les C.L.
   nlignes = 0
   ! comptage des lignes de HYD
   if (hydFile /= '') then
      open(newunit=lu1,file=trim(hydFile),status='old',form='formatted')
      do
         nlignes = nlignes + 1
         read(lu1,'(2a)',iostat=ios) u1,u2
         if (ios /= 0) exit
      enddo
      close (lu1)
   endif
   ! comptage des lignes de LIM
   if (limFile /= '') then
      open(newunit=lu1,file=trim(limFile),status='old',form='formatted')
      do
         nlignes = nlignes + 1
         read(lu1,'(2a)',iostat=ios) u1,u2
         if (ios /= 0) exit
      enddo
      close (lu1)
   endif
   ! comptage des lignes de AVA
   if (avaFile /= '') then
      open(newunit=lu1,file=trim(avaFile),status='old',form='formatted')
      do
         nlignes = nlignes + 1
         read(lu1,'(2a)',iostat=ios) u1,u2
         if (ios /= 0) exit
      enddo
      close (lu1)
   endif
   call IniLireCL(la_topo%net%nn,nlignes)
end subroutine Lire_TypeCL



subroutine VerifAllCL
!==============================================================================
!         vérification de l'introduction de toutes les C.L. nécessaires
!==============================================================================
   use, intrinsic :: iso_fortran_env, only: output_unit

   use parametres, only: l9, lTra
   use Data_Num_Fixes, only: silent
   use TopoGeometrie, only: la_topo
   implicit none
   ! -- variables locales temporaires --
   integer :: n
   character(len=120) :: ligne
   !------------------------------------------------------------------------------
   do n = 1, la_topo%net%nn
      if (la_topo%nodes(n)%cl == -2) then
         write(ligne,'(4a)') ' >>>> Le nœud aval ',trim(la_topo%nodes(n)%name),' n''a pas', &
                         ' reçu de C.L. : on prend le régime uniforme'
         if (.not.silent) then
            write(output_unit,'(a)') trim(ligne)
         else
             write(l9,'(a)') trim(ligne)
        endif
         write(lTra,'(a)') trim(ligne)
      endif
   enddo
end subroutine VerifAllCL



subroutine IniDataCL
!==============================================================================
!          Transfert de Data_tmp_CL vers allCL(:)
!==============================================================================
   use parametres, only: long, zero
   use basic_Types, only: point4D, point2D
   use conditions_limites, only: allCL, iniCL
   use Data_tmp_CL, only: tz=>t, qz=>q, ltm=>itm, jtm, ql, qr, boolc
   use TopoGeometrie, only: la_topo
   implicit none
   ! -- variables --
   integer :: j, kn, n, i, np
   !------------------------------------------------------------------------------

   call iniCL()

   do n = 1, la_topo%net%nn
      !KN=rang de lecture dans fichiers .HYD .LIM .AVA pour le nœud N
      kn = ltm(n)
      if (kn > 0) then !nœud pour lequel on a fourni des données
         np = jtm(kn)-jtm(kn-1)
         if (la_topo%nodes(n)%cl == 3) then
            call allCL(n)%init(np,dim=4,bool=boolc(n))
            associate (bc => allCL(n)%bc(:))
               select type (bc)
                  class is(point4D)
                     i = 0
                     do j = jtm(kn-1)+1,jtm(kn)
                        i = i+1
                        bc(i)%x = tz(j) ; bc(i)%y = ql(j) ; bc(i)%u = qz(j) ; bc(i)%v = qr(j)
                     enddo
                  class is(point2D)
                     stop 666
               end select
            end associate
         else
            call allCL(n)%init(np,dim=2,bool=boolc(n))
            i = 0
            do j = jtm(kn-1)+1,jtm(kn)
               i = i+1
               allCL(n)%bc(i)%x = tz(j)
               allCL(n)%bc(i)%y = qz(j)
            enddo
         endif
      else !nœud pour lequel on n'a pas fourni des données
         call allCL(n)%init(1,dim=2,bool=.false.)
      endif
   enddo
   !on désalloue tous les tableaux de Data_tmp_CL
   deallocate (tz,qz,ltm,jtm,ql,qr,boolc)
end subroutine IniDataCL
