!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - INRAE - 1989-2020                           #
!# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
! Sauf mention contraire les tableaux indexés par bief le sont par le rang de
! calcul des biefs et non par leur numéro d'ordre dans .TAL .
!
! Les tableaux indexés par section le sont par le numéro de section de calcul
! donc dans l'ordre de calcul de l'amont vers l'aval.
!-------------------------------------------------------------------------------
!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
module IO_Files
!==============================================================================
!      Définition des noms des fichiers de données et de résultats
!==============================================================================
   use Parametres, only: lname
   ! parametres des fichiers de donnees et de sortie
   implicit none

   ! noms des fichiers de données
   character(len=lname) :: repFile, avaFile, casFile, devFile, hydFile, &
                              iniFile, latFile, limFile, netFile, numFile, &
                              rugFile, sinFile, varFile, drgFile, psiFile, &
                              sedFile, qsoFile, chaFile, btmFile, qstFile, &
                              cgnsFile, parfile

   ! noms des fichiers de résultats
   ! doivent obligatoirement être initialisés lors de la lecture de REP
   character(len=lname) :: binFile, traFile, errFile, envFile, &
                              graFile

   contains
   subroutine init_Filenames
      repFile = ''
      avaFile = ''
      casFile = ''
      devFile = ''
      hydFile = ''
      iniFile = ''
      latFile = ''
      limFile = ''
      netFile = ''
      numFile = ''
      parFile = ''
      rugFile = ''
      sinFile = ''
      varFile = ''
      binFile = ''
      traFile = ''
      errFile = ''
      envFile = ''
      drgFile = ''
      psiFile = ''
      sedFile = ''
      qsoFile = ''
      chaFile = ''
      btmFile = ''
      qstFile = ''
      graFile = ''
      cgnsFile = ''
   end subroutine init_Filenames
end module IO_Files
!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
module Apports_Lateraux
!==============================================================================
! ce module donne les pointeurs permettant de lire le fichier temporaire des
! apports latéraux INIMAGE.QLA ainsi que la définition des secteurs.
!
!==============================================================================
   use Parametres, only: long
   implicit none
   integer :: nlat ! nombre de secteurs pour décrire les apports/fuites latéraux
   integer, allocatable :: nsect(:,:) ! nsect(n,1) = section amont du secteur n
                              ! nsect(n,2) = section aval du secteur n
   integer, allocatable :: itl(:) ! itl(n) = numéro du dernier enregistrement pour le
                            !          secteur n

   contains
      subroutine iniAppLat(nlat)
      !Routine d'initialisation des variables du module
         implicit none
         integer, intent(in) :: nlat
         allocate (nsect(1:nlat,1:2),itl(0:nlat))
         nsect(1:nlat,1:2) = 0
         itl(0:nlat) = 0
      end subroutine iniAppLat
end module Apports_Lateraux
!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
module Booleens
!==============================================================================
!             Ce module regroupe diverses variables logiques
!==============================================================================
   implicit none
   logical :: deltmp ! vrai si on ne conserve pas les fichiers temporaires en
                     ! fin de de simulation
   logical :: uselat ! vrai s'il y a des échanges latéraux autres que ceux dus
                     ! au lit moyen et aux apports latéraux :
                     !      - lit majeur de stockage (maj existe)
                     !      - déversements latéraux (dev existe)
   logical :: donotComputeVolumeBalance  ! vrai si on ne vérifie pas la conservation des volumes
   logical :: ber014 ! vrai si smoy ou pmoy négatif
   logical,allocatable :: booll(:) ! vrai si le débit injecté dans le secteur est surfacique

   contains
      subroutine iniBool()
      ! --- initialisation des variables du module booleens
         implicit none
!          integer,intent(in) :: nlat
         deltmp = .false.
         uselat = .false.
         donotComputeVolumeBalance = .true.
         ber014 = .false.
!          allocate(booll(1:nlat))
!          booll = .false.
      end subroutine iniBool
end module Booleens
!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
module Casiers
!==============================================================================
!       Données pour la modélisation du déversement par dessus les berges
!                    vers un casiers ou un autre bief
!==============================================================================
   use Parametres, only: Long, zero, RivG, RivD, total
   implicit none
   ! NOTE: les dimensions effectives sont fixées par Init_Allocatable_Arrays() dans mage_Init.f90
   real(kind=Long), allocatable :: YDEV(:,:)   ! tirant d'eau de déversement
   real(kind=Long), allocatable :: CDLAT(:,:)  ! coefficient de débit pour la loi de seuil qui
                                               ! modélise le déversement par dessus les berges
   real(kind=Long), allocatable :: HTRAFF(:,:) ! profondeur d'affaissement de la berge
   real(kind=Long), allocatable :: PCEAFF(:,:) ! proportion de longueur du secteur
                                               ! déversant soumis à affaissement
   real(kind=Long), allocatable :: QDS0(:,:)   ! débit déversé par section à t
                                               ! défini par copie de QDS1 dans M13()
   real(kind=Long), allocatable :: QDS1(:,:)   ! débit déversé par section à t+dt
   real(kind=Long), allocatable :: QDB0(:,:)   ! débit déversé par bief à t
   real(kind=Long), allocatable :: QDB1(:,:)   ! débit déversé par bief à t+dt
   real(kind=Long), allocatable :: QCLAT(:)    ! débit déversé vers l'exterieur (index=0)
                                               !               ou un noeud (index>0)
   real(kind=Long), allocatable :: SU(:)       ! surface inondée par bief
   real(kind=Long), allocatable :: VO(:)       ! volume de l'inondation par bief
   real(kind=Long), allocatable :: VD(:,:)     ! volume déversé par bief
   real(kind=Long), allocatable :: VB(:)       ! volume du bief
   real(kind=Long) :: DTLAT

   integer, allocatable :: NCLAT(:,:)   ! cible du déversement
                                     ! >0 si N° de noeud et <0 si section de bief
   integer, allocatable :: RVCLAT(:,:)  ! rive de réception de la cible du déversement

   logical :: DEVLAT(RivG:RivD)      ! vrai si déversement latéral effectif
   !
   contains
   subroutine IniCase(ibmax,ismax,nomax)
! --- Initialisation des variables du module CASIERS
      implicit none
      integer, intent(in) :: ibmax, ismax, nomax

      DevLat = .FALSE.
      DTLat = zero

      allocate(ydev(rivg:rivd,ismax),cdlat(rivg:rivd,ismax),htraff(rivg:rivd,ismax))
      allocate(pceaff(rivg:rivd,ismax),qds0(total:rivd,ismax),qds1(total:rivd,ismax))
      allocate(qdb0(total:rivd,ibmax),qdb1(total:rivd,ibmax),qclat(0:nomax))
      allocate(su(ibmax),vo(ibmax),vd(total:rivd,ibmax),vb(ibmax))
      allocate(nclat(rivg:rivd,ismax),rvclat(rivg:rivd,ismax))
      ydev = zero  ;  cdlat = -1._long  ;  htraff = zero
      pceaff = zero  ;  qds0 = zero  ;  qds1 = zero
      qdb0 = zero  ;  qdb1 = zero  ;  qclat = zero
      su = zero  ;  vo = zero  ;  vd = zero  ;  vb = zero
      nclat  = 0  ;  rvclat = zero

   end subroutine IniCase
end module Casiers
!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
module Data_Num_Fixes
!==============================================================================
!      Paramètres numériques divers : pas de temps, bornes supérieures, etc.
!==============================================================================
   use Parametres, only: Long
   implicit none
   logical :: long_BIN
   logical :: silent
   logical :: steady  ! si vrai on recherche la stabilisation sur un etat permanent
   logical :: ini_internal  ! utilisation du module d'initialisation en permanent
   integer :: newt    ! type d'iterations
   character(len=1) :: plus ! mode d'ecriture a l'ecran (sur-impression)
   real(kind=Long) :: sch1 ! type de discretisation de l'equation de
                           ! continuite ; defini par .NUM
   real(kind=Long) :: sch2 ! type de discretisation du terme de frottement ;
                           ! defini par .NUM
   real(kind=Long) :: sch3 ! type de test d'arret ; defini par .NUM
   real(kind=Long) :: flag ! FLAG est mis a 1. s'il y a des pompes

   ! Dates et pas de temps
   real(kind=Long) :: tinf   ! date de debut de la simulation
   real(kind=Long) :: tmax   ! date de fin de la simulation
   real(kind=Long) :: theta  ! parametre d'implicitation
   real(kind=Long) :: dtbase ! pas de temps maximum fourni par l'utilisateur
   real(kind=Long) :: dtmin  ! pas de temps minimal
   real(kind=Long) :: dttra  ! pas de temps d'ecriture sur .TRA
   real(kind=Long) :: dtbin  ! pas de temps d'ecriture sur .BIN
   real(kind=long) :: dtcsv  ! pas de temps d'ecriture sur les CSV
   real(kind=long) :: dtmelissa ! pas de temps d'envoi des messages Melissa
   real(kind=long) :: dt_char !pas de temps pour le charriage (multiple de dtbase)

   real(kind=Long) :: c_lissage ! coefficient de lissage pour les chocs faibles (M161)

   real(kind=Long) :: yinf   ! tirant d'eau minimal
   real(kind=Long) :: crmax  ! nombre de Courant maximal, contrôle le dt effectif
   ! détection de divergence :
   real(kind=Long) :: frmax  ! nb de Froude maximal
   real(kind=Long) :: fma    ! borne d'erreur en cote sur la maille
   real(kind=Long) :: err_volume_maxAllowed ! erreur en volume maximale admissible (bilan par bief)
   real(kind=Long) :: vbmin  ! volume de bief minimal pour calcul bilan

   ! contrôle des itérations :
   integer iter  ! nombre maximal d'iterations par pas de temps
   integer ndt   ! facteur de reduction du pas de temps en cas de divergence
   integer inr   ! nombre d'iterations de Newton avant basculement en Point Fixe
   integer iter1 ! nombre maximum d'iterations sans reduction de la precision
   real(kind=Long) dfrp1 ! facteur de reduction de la precision sur les cotes
   real(kind=Long) dfrp2 ! facteur de reduction de la precision sur les debits
   real(kind=Long) dfrp3 ! facteur de reduction de la precision sur les residus

   ! Paramètres de la routine RELAX() pour le torrentiel
   real(kind=long) :: relax_a, relax_b, reste, CRtor, CRnormal

   ! Encodage des fichiers de sortie texte : TRA, ERR, OUTPUT
   character(len=12) :: encodage

   integer :: fp_model ! valeurs possibles définies dans le module Parametres

   integer :: nb_cpu ! nombre de cpu à utiliser avec OpenMP

!   logical :: date_format ! .true. if the date is in the DD/MM/YY HH:MM:SS format
   logical :: date_format ! .true. if the date is in the YYYY-MM-DD HH:MM:SS ISO format
   logical :: with_melissa = .false.

   contains
      subroutine IniDataFix
         ! --- Initialisation des variables du module DATA_NUM_FIXES
         use Parametres, only: Long, zero
         implicit none
         DTTra=zero ; DTBin=zero ; DTcsv=zero
         Newt=0
         Iter=0 ; NDT=0 ; INR=0 ; ITER1=0
         dfrp1=0.0; dfrp2=0.0; dfrp3=0.0;
         TInf=zero ; TMax=zero ; Theta=zero
         DTBase=zero ; Flag=zero
         c_lissage=zero ; CRMax=100._long ; YInf=zero
         FrMax=zero
         Sch1=zero ; Sch2=zero ; Sch3=zero
         FMA=zero
         DTMin=zero
         err_volume_maxAllowed=1.e30_long ; VBMin=zero
         ini_internal = .false.
         Plus=' '
         relax_a = 0.95_long
         relax_b = 1.05_long
      end subroutine IniDataFix
end module Data_Num_Fixes
!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
module Data_Num_Mobiles
!==============================================================================
!      Variables numériques diverses : pas de temps actuel, etc.
!==============================================================================
   use Parametres, only: long, zero
   implicit none
   real(kind=long) :: ttra    ! date de prochaine ecriture sur .TRA
   real(kind=long) :: tbin    ! date de prochaine ecriture sur .BIN
   real(kind=long) :: tcsv    ! date de prochaine écriture sur les CSV
   real(kind=long) :: tmelissa! date de prochain envoi au serveur Melissa
   real(kind=long) :: t       ! date courante
   real(kind=long) :: dt      ! pas de temps courant
   real(kind=Long) :: area_factor    ! facteur de conversion des aires des noeuds a
                              ! surfaces non-nulles
   real(kind=long) :: dx      ! pas d'espace courant
   real(kind=long) :: dtold   ! pas de temps precedent
   real(kind=long) :: dtmin0  ! variable pour l'adaptation du pas de temps
   real(kind=long) :: cr      ! nombre de Courant actuel
   real(kind=long) :: frsup   ! sup des Nb de Froude

   integer :: ifrsup ! position du sup des Nb de Froude

   integer :: iscr   ! indice de la section ou on a le CR le plus grand
   integer :: isi    ! indice de debordement de la section courante dans SECT(B) et
                     ! BIEF(B) repère d'origine d'appel de BAL (M143 ou M15)
                     ! est aussi utilise comme indice de section.
   integer :: isj    ! indice de la section courante
   integer :: ib     ! indice du bief courant
   integer :: niter  ! nombre total d'iterations sur toute la simulation
   integer :: npas   ! nombre total de pas de temps sur toute la simulation

   logical :: ISM_calcul_OK

   integer :: consecutive_ISM_timesteps

   real(kind=long) :: t_char

   real(kind=long) :: tmax_cl ! temps max parmis toutes les c.l.

   contains
      subroutine IniDataMob
      ! --- Initialisation des variables du module Data_Num_Mobiles --- (BF)
         implicit none
         ttra = zero ; tbin = zero ; tcsv = zero; tmelissa = zero
         isi = 0 ; isj = 0 ; ib = 0
         niter = 0 ; npas = 0
         t = zero ; dt = zero ; dx = zero
         dtold = zero
         cr = zero ; iscr = 0
         dtmin0 = zero
         ifrsup = 1
         area_factor = zero  !valeur actuelle définie dans init_ouvrages_and_claval() : area_factor= 10000./DT

         tmax_cl = 0._long
      end subroutine IniDataMob


      pure logical function ISM_available()
      ! renvoie vrai si on a un résultat ISM
         use parametres, only: i_ism, i_mixte, i_mixte2
         use data_num_fixes, only: fp_model

         ISM_available = (fp_model == i_ism) .or. ((fp_model == i_mixte .or. fp_model == i_mixte2) .and. ISM_calcul_OK)
      end function ISM_available
end module Data_Num_Mobiles
!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
module Erreurs_Sing
!==============================================================================
!         Variables locales issues de la discrétisation
!==============================================================================
   use Parametres, only: Long, zero
   implicit none
   ! FIXME: rendre ces tableaux allouable et ajouter l'allocation de mémoire (où ?)
   ! NOTE: il faudrait peut-être déplacer ces tableaux dans le module ouvrages
   real(kind=Long), allocatable :: DQSup(:)  ! DQSup(NS) = erreur en débit à la section singuliere NS
   real(kind=Long), allocatable :: TSup(:)   ! TSup(NS) = date à laquelle se produit l'erreur maximale en débit à la section singulière NS
   real(kind=Long), allocatable :: VolSNG(:) ! VolSNG(NS) = volume total passant sur la singularité NS

   contains
      subroutine IniErrSing(nsmax)
         ! -- prototype --
         integer, intent(in) :: nsmax

         allocate (DQSup(nsmax), TSup(nsmax), VolSNG(nsmax))
         DQSup(1:nsmax) = zero
         TSup(1:nsmax) = zero
         VolSNG(1:nsmax) = zero
      end subroutine IniErrSing
end module Erreurs_Sing
!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
module Erreurs_STV
!==============================================================================
!         Variables locales relatives à l'évaluation du résidu
!==============================================================================
   use Parametres, only: Long, zero
   implicit none
   real(kind=long), allocatable :: errvol(:) !erreurs en volume par bief au dt courant
   real(kind=long) :: errmax        !erreur en volume maximale sur la simulation
   real(kind=long) :: t_errmax      !date à laquelle on a l'erreur maximale sur la simulation
   real(kind=long) :: errsup        !erreur en volume maximale à l'instant final
   real(kind=long) :: ermax0        !copie de errmax pour retour en arrière
   real(kind=long) :: reste    ! résidu
   real(kind=long) :: echelle  ! facteur de cadrage (routine bief6)
   real(kind=long) :: reste_sup, t_reste_sup
   integer :: kbmax  !bief où se produit l'erreur en volume maximale sur la simulation
   integer :: kbsup  !bief où se produit l'erreur en volume maximale à l'instant final
   integer :: ireste !numéro de section où le résidu est maximal

   contains
      subroutine IniErrSTV(ibmax)
         implicit none
         integer, intent(in) :: ibmax
         allocate (ErrVol(1:ibmax))
         kbmax = 0
         kbsup = 0
         ErrVol = zero
         ErrMax = zero
         t_errmax = -1.e+30_long
         ErrSup = zero
         ErMax0 = zero
         Reste = -1.e+30_long
         Reste_sup = -1.e+30_long
         IReste = 0
         t_reste_sup = 0._long
         echelle = 1.e-30_long
      end subroutine IniErrSTV
end module Erreurs_STV
!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
module Hydraulique
!==============================================================================
!        Grandeurs hydrauliques pour les sections (profils en travers)
!==============================================================================
   use Parametres, only: Long, zero
   use topoGeometrie, only: la_topo

   implicit none
   !
   real(kind=Long),allocatable :: akmin(:) ! strickler mineur
   real(kind=Long),allocatable :: akmoy(:) ! strickler moyen

   real(kind=Long),allocatable :: qt(:)  ! dernière ligne d'eau calculée (DT précédent) : débit
   real(kind=Long),allocatable,target :: zt(:)  !                                       : cote
   real(kind=Long),allocatable :: yt(:)  !                                              : tirant d'eau
   real(kind=Long),allocatable :: vt(:)  !                                              : vitesse
   real(kind=Long),allocatable :: qe(:)  ! débit latéral pour la dernière ligne d'eau
   real(kind=Long),allocatable :: dqe(:) ! increment du debit lateral pour le DT courant
   !variables à l'instant T (discrétisation)
   real(kind=Long),allocatable :: st(:)   ! section mouillée "hydraulique" (mineur+moyen)
   real(kind=Long),allocatable :: ajt(:)  ! perte de charge linéaire
   real(kind=Long),allocatable :: ajst(:) ! perte de charge linéaire singulière (Borda)
   real(kind=Long),allocatable :: stt(:)  ! section mouillée totale (mineur+moyen+majeur)
   real(kind=Long),allocatable :: psit(:) ! Q*Q/S
   !équations discrétisées brutes
   real(kind=Long),allocatable :: aa11(:)
   real(kind=Long),allocatable :: aa12(:)
   real(kind=Long),allocatable :: aa21(:)
   real(kind=Long),allocatable :: aa22(:)
   real(kind=Long),allocatable :: b01(:)
   real(kind=Long),allocatable :: b02(:)

   contains

   integer function ibu(ib)
      ! remplace le tableau IBU des versions 7 et précédentes
      ! IBU(IB) = rang de calcul du bief de numero IB dans .NET
      integer, intent(in) :: ib
      ibu = la_topo%net%rang(ib)
   end function ibu

   integer function iub(ib)
      ! remplace le tableau IUB des versions 7 et précédentes
      ! IUB(IB) = numéro dans .NET du bief de rang IB dans
                      ! l'ordre de calcul de la ligne d'eau. En particulier :
                      ! IUB(1) = numéro du premier bief amont du modèle
                      ! IUB(IBMAX) = numéro du dernier bief aval du modèle
                      ! IUB(IB) est négatif si le bief IB est dans la maille
      integer, intent(in) :: ib
      iub = la_topo%net%numero(ib)
   end function iub

   subroutine IniHyd
      ! --- Initialisation des variables du module HYDRAULIQUE --- (BF)
      implicit none
      integer :: ismax

      ismax = la_topo%net%ns
      allocate (AKMin(1:ismax), AKMoy(1:ismax), QT(1:ismax), ZT(1:ismax),YT(1:ismax),VT(1:ismax))
      allocate (QE(1:ismax),DQE(1:ismax),ST(1:ismax),AJT(1:ismax),AJST(1:ismax),STT(1:ismax),PSIT(1:ismax))
      allocate (AA11(1:ismax),AA12(1:ismax),AA21(1:ismax),AA22(1:ismax),B01(1:ismax) ,B02(1:ismax))
      ! -- /RHyd/ --
      AKMin=zero ; AKMoy=zero ; QT=zero ; ZT=zero ; YT=zero ; VT=zero ; QE=zero ; DQE=zero
      ! -- /SHyd/ --
      ST =zero ; AJT =zero ; AJST=zero ; STT=zero ; PSIT=zero
      ! -- /Sauve/ --
      AA11=zero ; AA12=zero ; AA21=zero ; AA22=zero ; B01 =zero ; B02 =zero
   end subroutine IniHyd
end module Hydraulique
!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
module Matrice_STV
!==============================================================================
!           Coefficients des équations discrètes après cadrage
!    utilisé par mage_e4.for (discrétisation) et mage_e3.for (double balayage)
!
!==============================================================================
   use Parametres, only: Long, zero
   implicit none
   real(kind=Long), allocatable :: A(:),B(:),C(:),D(:),E(:),F(:)
   real(kind=Long), allocatable :: AB(:),BB(:),CB(:),DB(:),EB(:),FB(:)

   contains

      subroutine IniMatSTV(ismax)
      ! --- Initialisation des variables du module MATRICE_STV
         implicit none
         integer, intent(in) :: ismax

         allocate (A(ismax),B(ismax),C(ismax),D(ismax),E(ismax),F(ismax))
         allocate (AB(ismax),BB(ismax),CB(ismax),DB(ismax),EB(ismax),FB(ismax))

         A=zero ; B=zero ; C=zero ; D=zero ; E=zero ; F=zero
         AB=zero ; BB=zero ; CB=zero ; DB=zero ; EB=zero ; FB=zero
      end subroutine IniMatSTV
end module Matrice_STV
!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
module Solution
!==============================================================================
!                Stockage de la solution
!==============================================================================
   use Parametres, only: Long, Zero
   implicit none
   ! /SOL/
   real(kind=Long), allocatable :: DQ(:)  ! variation de Q (débit) sur le pas de temps courant
   real(kind=Long), allocatable, target :: DZ(:)  ! variation de Z (cote) sur le pas de temps courant
   real(kind=Long), allocatable :: DZN(:) ! variation des cotes aux nœuds sur le pas de temps courant
   real(kind=Long), allocatable :: SR(:)  ! coeff liés à l'aire du noeud
   real(kind=Long), allocatable :: TR(:)  ! voir RST1A() et RST1Z()
   real(kind=Long), allocatable :: QN(:)  !bilan des débits au nœud n
   real(kind=Long), allocatable :: ZN(:)  !cote moyenne au nœud n
   ! /SOLA/
   real(kind=Long), allocatable :: DQA(:)  ! variation de Q (débit) sur le pas de temps précédent
   real(kind=Long), allocatable :: DZA(:)  ! variation de Z (cote) sur le pas de temps précédent
   real(kind=Long), allocatable :: DZNA(:) ! variation des cotes aux nœuds sur le pas de temps précédent
   real(kind=Long), allocatable :: SRA(:)  ! coeff liés à l'aire du noeud
   real(kind=Long), allocatable :: TRA(:)  ! voir RST1A() et RST1Z()
   real(kind=Long), allocatable :: QNA(:)  !bilan des débits au nœud n au pas de temps précédent
   real(kind=Long), allocatable :: ZNA(:)  !cote moyenne au nœud n au pas de temps précédent

   contains
      subroutine IniSol(ismax,nomax)
      ! --- Initialisation des variables du module SOLUTION
         implicit none
         integer, intent(in) :: ismax, nomax

         allocate (dq(1:ismax),dz(1:ismax),dzn(1:nomax),sr(1:nomax),tr(1:nomax),qn(1:nomax),zn(1:nomax))
         allocate (dqa(1:ismax),dza(1:ismax),dzna(1:nomax),sra(1:nomax),tra(1:nomax),qna(1:nomax),zna(1:nomax))

         ! -- /SOL/ --
         dq(1:ismax)  = zero ; dz(1:ismax) = zero
         dzn(1:nomax) = zero ; sr(1:nomax) = zero ; tr(1:nomax) = zero
         qn(1:nomax)  = zero ; zn(1:nomax) = zero
         ! -- /sola/ --
         dqa(1:ismax)  = zero ; dza(1:ismax) = zero
         dzna(1:nomax) = zero ; sra(1:nomax) = zero ; tra(1:nomax) = zero
         qna(1:nomax)  = zero ; zna(1:nomax) = zero
      end subroutine IniSol
end module Solution
!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
module Data_Iterations_loc
!==============================================================================
!          Donnees propres a la routine M143 (itérations de N-R)
!       utilisé pour transmettre des variables à ses sous-routines
!==============================================================================
   use Parametres, only: Long, Zero
   implicit none
   ! DZ0, DQ0, DH et DZN0 sont des variations au cours du pas de temps
   ! DZ0 : niveaux
   ! DQ0 : débits
   ! DH : charges (ouvrages)
   ! DZN0 : cotes aux noeuds
   ! EZ et EQ sont des écarts entre 2 itérations (cotes et débits)
   ! FRPZ et FRQZ sont les facteurs de réduction de la précision actuels
   real(kind=Long), allocatable :: DZ0(:), DQ0(:), DZN0(:)
   real(kind=long) :: EZ, EQ, FRPZ, FRPQ, FRPR
   integer :: IZMAX, IQMAX
   ! IZMAX et IQMAX sont les n° de section où les écarts EZ?? et EQ?? sont maximaux.

   contains
      subroutine IniM143(ismax,nomax)
         implicit none! -- /M143/ --
         integer, intent(in) :: ismax, nomax

         allocate (DZ0(ismax), DQ0(ismax), DZN0(nomax))
         DZ0 = zero ; DQ0 = zero ; DZN0 = zero

         EZ = zero ; EQ = zero ; FRPZ = zero ; FRPQ = zero ; FRPR = zero
         IZMax = 0 ; IQMax = 0
      end subroutine IniM143
end module Data_Iterations_loc
!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
module Mage_Results
!==============================================================================
!            Résultats regroupés pour le module de propagation
!                     des incertitudes par optimisation
!==============================================================================
   use parametres, only : long, zero
   use TopoGeometrie, only: la_topo
   implicit none

   real(kind=long),allocatable :: pm(:), z_fd(:), largeur_totale(:)
   real(kind=long),allocatable :: largeur_mineur(:),Z_max(:)
   real(kind=long),allocatable :: qtot(:), qfp(:)  !fp : flood plaine
   real(kind=long) :: cpu_total
   integer :: date_fin, flag
   logical :: calcul_OK
   character(len=3) :: type_resultat

   contains
      subroutine save_Mage_Results()
         integer :: is, ib, lu

         open(newunit=lu,file='Mage_Results',form='unformatted',status='unknown')
         if (calcul_OK) then
            flag = 1 ; write(lu) flag
         else
            flag = 0 ; write(lu) flag
         endif
         write(lu) date_fin, la_topo%net%nb, la_topo%net%ns, type_resultat, cpu_total
         do ib = 1, la_topo%net%nb
            write(lu) la_topo%biefs(ib)%is1,la_topo%biefs(ib)%is2, la_topo%net%rang(ib)
         enddo
         do is = 1, la_topo%net%ns
            write(lu) pm(is), z_fd(is), largeur_totale(is),largeur_mineur(is), &
                      Z_max(is),qtot(is),qfp(is)
         enddo
         close(lu)
      end subroutine save_Mage_Results

      subroutine init_Mage_Results()
         integer :: ismax

         ismax = la_topo%net%ns
         allocate (pm(ismax), z_fd(ismax), largeur_totale(ismax), largeur_mineur(ismax))
         allocate (Z_max(ismax), qtot(ismax), qfp(ismax))

         pm = zero
         z_fd = zero
         largeur_totale = zero
         largeur_mineur = zero
         Z_max = zero
         date_fin = -99999
         qtot = zero
         qfp = zero
         flag = 0
         if (type_resultat /= 'FIN' .and. type_resultat /= 'END' ) type_resultat = 'ENV'
         calcul_OK = .false.
         cpu_total = zero
      end subroutine init_Mage_Results

end module Mage_Results
!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
module PremierAppel
!==============================================================================
! Ce module PremierAppel regroupe les indicateurs NAP qui permettent
! de détecter le premier appel d'une routine.
! Utile pour réinitialiser ces indicateurs dans le cas d'une utilisation
! en boucle du solveur (optimisation)
!==============================================================================
   implicit none
   integer :: nap_runuptotime
   integer :: nap_m11
   integer :: nap_m142
   integer :: niter_m15
   integer :: it0_m172
   integer :: nap_qlat
   integer :: nap_envlop
   integer :: nap_e010
   integer :: nap_w001, nap_w001b
   integer :: nap_w005
   integer :: nap_observ
   integer :: nap_pflot
   integer :: nap_AskForFile
   integer :: nap_lire


   logical :: nap_m1436
   logical :: nap_inifin
   logical :: nap_write_TRA_Ouvrages
   logical :: firstCall_LireCL

   contains
      subroutine init_NAP
         nap_m1436 = .true.
         nap_inifin = .true.
         nap_write_TRA_Ouvrages = .true.

         nap_runuptotime = 0
         nap_m11 = 0
         nap_m142 = 0
         niter_m15 = 0
         it0_m172 = 1
         nap_qlat = 0
         nap_envlop = 0
         nap_e010 = 0
         nap_w001 = 0
         nap_w001b = 0
         nap_w005 = 0
         nap_observ = 0
         nap_pflot = 0
         nap_AskForFile = 0
         nap_lire = 0
         firstCall_LireCL = .true.
      end subroutine init_NAP

end module PremierAppel


module data_csv
   use Parametres, only: long

! -- type dérivé
   type csv_object
      !données pour extraire une courbe à la volée dans un fichier csv
      !le nom du fichier sera : prefix_var_ib_pk.csv
      real(kind=long)   :: pk      !pk de la section
      real(kind=long)   :: w       !valeur courante
      integer           :: ib      !n° du bief
      integer           :: is      !n° de section
      integer           :: mode    !0 = valeur instantanée ; 1 = moyenne sur le pas d'enregistrement
      character(len=10) :: prefix  !nom de base du fichier csv
      character(len=3)  :: var     !variable à écrire : Q, Z, V, Qm, Ql, Qr, qml, qmr, etc.
      character(len=60) :: filename!nom du fichier csv
   end type csv_object

   type(csv_object), allocatable, target :: filtre(:)
   integer :: ncsv ! nombre de fichiers csv à écrire == nb de courbes à extraire à la volée
                   ! dimension effective du tableau filtre
end module data_csv


#ifdef with_timers
module mage_timers
   use Parametres, only: long

   real(kind=long) :: init_t = 0.0, iter_t = 0.0, total_t = 0.0, regloc_t = 0.0, &
                      calcul_dt_t = 0.0, cl_amont_t = 0.0, apport_t = 0.0, &
                      update_positions_ouvrages_t = 0.0, Debord_discretisation_t = 0.0, &
                      init_CLaval_Debord_t = 0.0, init_solution_Debord_t = 0.0, &
                      nonLinear_iterations_t = 0.0, timestep_last_actions_t = 0.0
end module mage_timers
#endif /* with_timers */
