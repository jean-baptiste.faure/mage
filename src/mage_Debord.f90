!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - INRAE - 1989-2020                           #
!# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################
module StVenant_Debord
!########################## module bief #######################################
!     ce module contient les sous-programmes effectuant la discrétisation
!     semi-implicite de Saint-Venant (bief et sect) et les sous-programmes
!     qui modifient cette discrétisation lors des itérations de type
!     Newton-Raphson sur Saint-Venant (biefb)
!     Ce module utilise la méthode Debord (UAN 1979)
!
!##############################################################################
!

use Parametres, only: long, g, zero, un, l9, lTra, deuxtiers, pi, rivg, rivd, gs2, total
use, intrinsic :: iso_fortran_env, only: error_unit

use mage_utilitaires, only: is_NaN, zegal, heure
use objet_section, only: profil, is_lc
use TopoGeometrie, only: la_topo, xgeo, zfd, zfond
use Ouvrages, only: all_OuvCmp, all_OuvEle, debit_pompe_total, debit, debit_total

implicit none
type section_values
   !divers paramètres d'une section de calcul calculés à partir des données
   !géométriques et hydrauliques. Ces paramètres sont ceux nécessaires à
   !la mise en œuvre du schéma de Preissmann pour les équations de St-Venant
   !toutes les valeurs sont prises au temps courant
   real(kind=long) :: q     !débit
   real(kind=long) :: dq    !q(t+dt)-q(t)
   real(kind=long) :: z     !cote réelle (on a supprimé la normalisation par Zmin)
   real(kind=long) :: dz    !z(t+dt)-z(t)
   real(kind=long) :: s     !section mouillée dynamique (mineur+moyen)
   real(kind=long) :: ds    !s(t+dt)-s(t)
   real(kind=long) :: al    !largeur au miroir dynamique (mineur + moyen)
   real(kind=long) :: aj    !perte de charge linéaire (Manning-Strickler et Debord)
   real(kind=long) :: daj   !daj =aj(t+dt)-aj(t)
   real(kind=long) :: psi   !beta/S (Boussinesq / section mouillée)
   real(kind=long) :: phi   !ph = sec2%d(psi)/dz
   real(kind=long) :: de    !débitance
   real(kind=long) :: dde   !dérivée de la débitance : dde=d(de)/dz
   real(kind=long) :: alt   !largeur totale (mineur+moyen+majeur)
   real(kind=long) :: st    !section mouillée totale (mineur+moyen+majeur)
   real(kind=long) :: dstot !st(t+dt)-st(t)
   real(kind=long) :: dsmaj !smaj(t+dt)-smaj(t)
   real(kind=long) :: y     !tirant d'eau
   real(kind=long) :: q2ph  !q2ph = q*q*phi
   real(kind=long) :: qps   !qps = q*psi
   real(kind=long) :: qqps  !qqps = qps*q = q*q*psi
   real(kind=long) :: qde   !qde = 0.5*dJ/dQ = abs(Q)/De**2
   real(kind=long) :: xl    !largeur équivalente = section/tirant d'eau
   real(kind=long) :: vs    !vitesse / section
   real(kind=long) :: yl    !yl = (al-xl)/y
   real(kind=long) :: v     !vitesse moyenne = Q/S
   real(kind=long) :: almaj !largeur lit majeur = alt-al
   real(kind=long) :: vl    !largeur * vitesse = al*v
   real(kind=long) :: fr    !nombre de Froude
   real(kind=long) :: akmi  !Strickler lit mineur
   real(kind=long) :: akmo  !Strickler lit moyen
   real(kind=long) :: almin !largeur lit mineur
   real(kind=long) :: pmin  !périmètre mouillé lit mineur
   real(kind=long) :: smin  !section mouillée lit mineur
   real(kind=long) :: almoy !largeur lit moyen
   real(kind=long) :: pmoy  !périmètre mouillé lit moyen
   real(kind=long) :: smoy  !section mouillée lit moyen
   real(kind=long) :: h     !valeur pour Debord
   !real(kind=long) :: yd    !tirant d'eau de débordement mineur-moyen
   real(kind=long) :: p     !périmètre mouillé
   real(kind=long) :: dp    !dp = dp/dz
end type section_values

type(section_values), target :: sec_val_1, sec_val_2
! Le pointeur sec_val sert à simplifier les appels aux sous-routines de section_data()
! C'est section_data() qui doit affecter le pointeur
type(section_values), pointer :: sec_val => null()

real(kind=long), target :: a11,a12,b11,b12,b13,a21,a22,b21,b22,b23,c5,c6

! TODO deallocate alpha somewere
real(kind=long),allocatable :: alpha(:)  !paramètre pour contrôler les passages en torrentiel

contains
subroutine ini_sect_values(sec_val,val)
   type(section_values), intent(inout) :: sec_val
   real(kind=long),intent(in) :: val
   sec_val%Q=val ; sec_val%Z=val ; sec_val%S=val ; sec_val%Al=val ; sec_val%AJ=val ; sec_val%Psi = val
   sec_val%Phi = val ; sec_val%DE=val ; sec_val%DDE=val ; sec_val%Alt=val ; sec_val%ST=val ; sec_val%Y=val
   sec_val%Q2PH=val ; sec_val%QPS=val ; sec_val%QQPS=val ; sec_val%QDE=val ; sec_val%XL=val
   sec_val%VS=val ; sec_val%YL=val ; sec_val%V=val ; sec_val%ALMaj=val ; sec_val%VL=val ; sec_val%fr = val
   sec_val%p=val ; sec_val%dp=val ; sec_val%akmi = val ; sec_val%akmo=val ; sec_val%almin=val
   sec_val%pmin=val ; sec_val%smin=val ; sec_val%almoy=val ; sec_val%pmoy=val ; sec_val%smoy=val ; sec_val%h=val
end subroutine ini_sect_values


subroutine initialize_Debord(ismax)
! --- Initialisation des variables globales du module
   integer,intent(in)::ismax
   call ini_sect_values(sec_val_1,zero)
   call ini_sect_values(sec_val_2,zero)

   a11=zero ; a12=zero ; b11=zero ; b12=zero ; b13=zero
   a21=zero ; a22=zero ; b21=zero ; b22=zero ; b23=zero
   c5=zero ; c6=zero

   allocate(alpha(ismax))
   alpha = un
end subroutine initialize_Debord


subroutine Debord_discretisation(Bool)
!==============================================================================
!          Sous-programme faisant la linéarisation de la discrétisation
!                         de Saint-Venant avec Debord
!
! Renvoie Bool = VRAI si un problème est détecté sur l'état au début du pas de
!                     temps (calculé au pas de temps précédent)
! La routine Bief5() a 2 variantes selon la façon dont les apports latéraux sont
!                    pris en compte
!
! Pour chaque couple de sections on obtient une discrétisation de la forme :
!
!  ( DQi )     ( B  A )   ( DQi+1 )   ( C )
!  (     )  =  (      ) * (       ) + (   )
!  ( DZi )     ( E  D )   ( DZi+1 )   ( F )
!
!==============================================================================
   use booleens, only: ber014
   use data_num_mobiles, only: ib, cr, FRsup
   use casiers, only: devlat
   implicit none
   ! -- Prototype --
   logical,intent(out) :: Bool !vrai s'il y a un probleme lors de la discrétisation
   ! -- Variables locales temporaires --
   integer :: isens, iba1, ibz1, ibb
   character(len=180) :: err_message
   integer, pointer :: ibmax, iba, ibz

   !---initialisation : remise à 0 de Cr et FRsup (phase torrentielle)
   Cr = zero ; devlat = .false. ; ber014 = .false. ; FRsup = zero
   err_message = ' >>>> Erreur 014 (Debord_discretisation) : voir le fichier .TRA'

   !--->dans la suite on sort directement dés que BOOL est VRAI

   !------------------------------------------------------------------------------
   !             TRAITEMENT DES BIEFS AMONT DE LA MAILLE
   !---> IBA = rang du dernier bief a l'amont de la maille
   !------------------------------------------------------------------------------
   iba => la_topo%net%iba
   if (iba > 0) then
      !il y a des biefs à l'amont de la maille : discrétisation de Saint-Venant
      isens = 1
      do ib = 1,iba !parcours dans l'ordre de calcul amont -> aval
         ibb = la_topo%net%numero(ib)
         call discretise_bief(ibb,isens,bool)
         if (ber014) then  !il y a eu un appel à ERR014 quelque part
            write(lTra,'(a)') trim(err_message)  ;  write(error_unit,'(a)') trim(err_message)
            stop 107
         endif
         if (bool) return
      enddo
   endif
   !-----------------------------------------------------------------------
   !               TRAITEMENT DES BIEFS AVAL DE LA MAILLE
   !---> IBZ = rang du premier bief à l'aval de la maille
   !-----------------------------------------------------------------------
   ibmax => la_topo%net%nb
   ibz => la_topo%net%ibz
   if (ibz <= ibmax) then
      !il y a des biefs à l'aval de la maille : discrétisation de saint-venant
      isens = -1
      do ib = ibz,ibmax !parcours dans l'ordre de calcul amont -> aval
         ibb = la_topo%net%numero(ib)
         call discretise_bief(ibb,isens,bool)
         if (ber014) then  !il y a eu un appel à ERR014 quelque part
            write(lTra,'(a)') trim(err_message)  ;  write(error_unit,'(a)') trim(err_message)
            stop 107
         endif
         if (bool) return
      enddo
   end if
   !-----------------------------------------------------------------------
   !        DISCRETISATION DE SAINT-VENANT SUR LES BIEFS DE LA MAILLE
   !-----------------------------------------------------------------------
   if (iba /= ibmax) then
      !il y a une maille : discrétisation de saint-venant
      iba1 = max(iba+1,1)
      !s'il n'y a pas de bief à l'amont de la maille alors iba=-1
      ibz1 = ibz-1
      isens = 1
      do ib = iba1,ibz1 !parcours dans l'ordre de calcul amont -> aval
         ibb = la_topo%net%numero(ib)
         call discretise_bief(ibb,isens,bool)
         if (ber014) then
            write(lTra,'(a)') trim(err_message)  ;  write(error_unit,'(a)') trim(err_message)
            stop 107
         endif
         if (bool) return
      enddo
   endif
end subroutine Debord_discretisation


subroutine Debord_update_NR(Bool)
!==============================================================================
!   actualisation pour Newton-Raphson dans le cas des sections non singulières
!==============================================================================
   use booleens, only: ber014
   use data_num_mobiles, only: ib
   use erreurs_stv, only: reste,ireste
   implicit none
   ! -- Prototype --
   logical,intent(inout) :: bool
   ! -- Variables locales temporaires --
   integer :: isens, ibb
   character(len=80) :: err_message

   err_message = ' >>>> Erreur 014 (Debord_update_NR) : voir le fichier .TRA'
   reste = -1._long  ; ireste = 0

   !------------------------------------------------------------------------------
   !             Traitement des biefs amont de la maille
   !------------------------------------------------------------------------------
   ber014 = .false.
   if (la_topo%net%iba > 0) then
   !il y a des biefs amont de la maille : discrétisation de Saint-Venant
      do ib = 1,la_topo%net%iba !parcours dans l'ordre de calcul (rang) amont -> aval
         ibb = la_topo%net%numero(ib)
         isens = 1
         call discretise_biefb(ibb,isens,bool)
         if (ber014) then
            write(l9,'(a)') trim(err_message) ; write(error_unit,'(a)') trim(err_message)
            stop 108
         endif
         if (bool) return  ! pb de discrétisation : on sort
      enddo
   end if
   !------------------------------------------------------------------------------
   !               Traitement des biefs aval de la maille
   !------------------------------------------------------------------------------
   if (la_topo%net%ibz <= la_topo%net%nb) then
   !il y a des biefs aval de la maille : discrétisation de Saint-Venant
      do ib = la_topo%net%ibz,la_topo%net%nb  !parcours dans l'ordre de calcul (rang) amont -> aval
         ibb = la_topo%net%numero(ib)
         isens = -1
         call discretise_biefb(ibb,isens,bool)
         if (ber014) then
            write(l9,'(a)') trim(err_message) ; write(error_unit,'(a)') trim(err_message)
            stop 108
         endif
         if (bool) return  ! pb de discrétisation : on sort
      enddo
   end if
   !------------------------------------------------------------------------------
   !        Discrétisation de Saint-Venant sur les biefs de la maille
   !------------------------------------------------------------------------------
   if (la_topo%net%iba > 0 .and. la_topo%net%iba < la_topo%net%nb) then  ! il y a une maille
      do ib = la_topo%net%iba+1,la_topo%net%ibz-1  !parcours dans l'ordre de calcul (rang) amont -> aval
         ibb = la_topo%net%numero(ib)
         isens = 1
         call discretise_biefb(ibb,isens,bool)
         if (ber014) then
            write(l9,'(a)') trim(err_message) ; write(error_unit,'(a)') trim(err_message)
            stop 108
         endif
         if (bool) return  ! pb de discrétisation : on sort
      enddo
   endif

end subroutine Debord_update_NR


subroutine discretise_bief(ib,isens,bool)
!==============================================================================
!
!    sous-programme bief calculant la discrétisation de
!    Saint-Venant (avant le premier balayage)
!
!    les coefficients calculés se trouvent dans les vecteurs
!    a,b,c,d,e,f du module matrice_stv
!
!    ib est le numéro du bief dans l'ordre des données du fichier .NET
!
!!==============================================================================
   use casiers, only: su, vo, vb
   use data_num_mobiles, only: dt, dx, t
   use data_num_fixes, only: sch1, theta
   use hydraulique, only: qa=>qe, dqa=>dqe, zt
   implicit none
   ! -- prototype --
   integer,intent(in) :: ib, isens
   logical,intent(out) :: bool
   ! -- les variables locales --
   integer :: iflag, isa, nk, isi, isj, ns, nkk, k
   real(kind=long) :: alm,altm, sm, qd0, dqq, y2
   logical :: borda, pompe, dev_lat
   type(section_values), pointer :: secval_i, secval_j


   !write(l9,*) ' Discrétisation du bief ',la_topo%biefs(ib)%name,ib,' à T = ',t,' et DT = ',dt
   if (ib > la_topo%net%nb) then
      write(error_unit,*) 'Erreur pour Discretise_Bief() : numéro de bief trop grand'
      stop 5
   endif
   su(ib) = zero ; vo(ib) = zero ; vb(ib) = zero ; nk = -1
   !initialisation (sens amont--->aval)
   isa = la_topo%biefs(ib)%is1
   dx = abs(la_topo%sections(isa+1)%pk-la_topo%sections(isa)%pk)
   !initialisation des pointeurs sur sec_val_1 et sec_val_2
   secval_i => sec_val_1  ;  secval_j => sec_val_2
   call section_data(0,isa,bool,secval_i)
   if (bool) return

   !---boucle sur les sections du bief ib
   do isj = isa+1,la_topo%biefs(ib)%is2
      alm = secval_i%alt-secval_i%almin ; sm = secval_i%st-secval_i%smin
      isi = isj-1
      dx = abs(la_topo%sections(isj)%pk-la_topo%sections(isi)%pk)
      call section_data(0,isj,bool,secval_j)
      if (bool) return
      vb(ib) = vb(ib)+dx*(secval_i%st+secval_j%st)*0.5_long  !volume du bief
      !---calcul des débordements : mise à jour des surfaces inondées et des
      !   volumes déversés (on prend tout ce qui est au dessus du mineur : moyen + majeur)
      alm = (alm+secval_j%alt-secval_j%almin)*0.5_long ; sm = (sm+secval_j%st-secval_j%smin)*0.5_long
      if (alm > 1.e-05_long .and. sm > 1.e-05_long) then
         su(ib) = su(ib)+alm*dx  ;  vo(ib) = vo(ib)+sm*dx
      endif

      !---discrétisation des ouvrages (sections singulières)
      borda  =  .false.  ;  pompe = .false.  ;  dev_lat = .false.  ;  dqq = zero
      ns = la_topo%sections(isj)%iss
      if (la_topo%sections(isj)%iss /= 0) then  ! cas d'une section singulière
         nk = all_OuvCmp(la_topo%sections(isj)%iss)%OuEl(1,1)
         if (all_OuvEle(nk)%iuv == 91) then                                    !ouvrage perte de charge à la Borda -> STV
            dx = all_OuvEle(nk)%uv1
            borda = .true.
         else if (all_OuvEle(nk)%iuv == 3 .and. .not. zegal(dx,zero,un)) then  !pompe dans une "fausse" section singulière -> STV
            dqq = debit_pompe_total(la_topo%sections(isj)%iss,t)               !la pompe est modélisée comme un apport/fuite latérale
            pompe = .true.
         else if (all_OuvEle(nk)%iuv == 5 .and. .not. zegal(dx,zero,un)) then  !déversoir latéral dans une "fausse" section singulière -> STV
            do k = 1, all_OuvCmp(ns)%ne
               nkk = all_OuvCmp(ns)%OuEl(k,1)
               if (all_OuvEle(nk)%irf > 0) then
                  y2 = zt(all_OuvEle(nk)%irf)
               else
                  y2 = -99999._long
               endif
               dqq = dqq + debit(nkk,secval_j%z,y2)
            enddo
            dev_lat = .true.
         else
            call discretise_bief0(isj,iflag,secval_i,secval_j)
            if (iflag/=1) then
               !mise des équations de Saint-Venant linéarisées sous forme
               !matricielle (inversion des matrices élémentaires a ou b)
               call discretise_bief6(isj,isens,bool) ; if (bool) return
            endif
         endif
      else
         nk = -1 !on en a besoin dans le bloc test suivant pour l'appel à discretise_bief4()
      end if

      !---discrétisation Saint-Venant
      if (la_topo%sections(isj)%iss == 0 .or. borda .or. pompe .or. dev_lat) then  ! section non singulière ou Borda ou Pompe ou Déversoir Latéral

         !linéarisation de l'équation dynamique
         c6 = 2._long*dt/dx ; c5 = theta*c6
         call discretise_bief1(isj,secval_i,secval_j)      !contribution de [beta*q**2/s] (calcul de a1 à a5)
         call discretise_bief2(secval_i,secval_j)          !contribution de [g*s*(dz/dx)] (calcul de a6 à a10)
         call discretise_bief3(secval_i,secval_j)          !contribution de [g*s*j] (calcul de a11 à a15)
         call discretise_bief4(nk,isj,secval_i,secval_j)   !contribution de [g*s*js] (calcul de a16 à a20 : élargissement)
         call discretise_bief5(isj,qd0,secval_i,secval_j)  !contribution des apports/fuites et fin du calcul

         !linéarisation de l'équation de continuité
         a21 = un ; b21 = un
         if (sch1 < un) then       !discrétisation de dst/dt + dq/dx = q (type s)
            a22 = -secval_i%alt/c5 ; b22 =  secval_j%alt/c5
         else                      !discrétisation de lt*dz/dt + dq/dx = q (type l)
            altm = (secval_i%alt+secval_j%alt)*0.5_long
            a22 = -altm/c5 ; b22 = +altm/c5
         endif
         b23 = ((secval_j%q-secval_i%q)-dx*(qa(isi)+theta*dqa(isi)-qd0) + dqq)/theta
         call discretise_bief6(isj,isens,bool) !mise des équations de Saint-Venant linéarisées sous forme
                                               !matricielle (inversion des matrices élémentaires A ou B)
         if (bool) return
      endif
      !permutation des pointeurs
      if (associated(secval_j,sec_val_2)) then
         secval_j => sec_val_1 ; secval_i => sec_val_2
      else
         secval_i => sec_val_1 ; secval_j => sec_val_2
      endif
   enddo
end subroutine discretise_bief


subroutine discretise_biefb(ib,isens,bool)
!=============================================================================
!    sous-programme bief calculant la discrétisation de Saint-Venant
!                   (avant le premier balayage)
!
!          modification de la discrétisation lors des itérations
!
!       les coefficients calculés se trouvent dans les vecteurs
!                            a,b,c,d,e,f
!    ib est le numéro du bief dans l'ordre des données du fichier .NET
!
!==============================================================================
   use data_num_fixes, only: sch1, sch2, newt, theta, dtmin
   use data_num_mobiles, only: dt, dx
   use hydraulique, only: zt, st, ajt, ajst
   implicit none
   ! -- prototype --
   integer, intent(in) :: ib
   integer,intent(inout) :: isens
   logical,intent(inout) :: bool
   ! -- variables --
   integer :: iflag, isa, nk, isi, isj
   real(kind=long) :: ajs0, ajs1, ak1, ak2, akqv, altm, c60, deltaz, dqd, dsji, dsjj
   real(kind=long) :: sij, somds, dqq
   type(section_values),pointer :: secval_i,secval_j


   isa = la_topo%biefs(ib)%is1
   !initialisation des pointeurs
   secval_i => sec_val_1  ;  secval_j => sec_val_2
   call section_data(1,isa,bool,secval_i)
   if (bool) return
   !---boucle sur les sections du bief ib
   c60 = theta*dt
   do isj = isa+1, la_topo%biefs(ib)%is2
      isi = isj-1
      dx = abs(la_topo%sections(isj)%pk-la_topo%sections(isi)%pk)
      call section_data(1,isj,bool,secval_j)
      if (bool) return
      dqq = zero

      !---Ouvrages (sections singulières)
      if (la_topo%sections(isj)%iss /= 0) then  !cas d'une section singulière
         nk = all_OuvCmp(la_topo%sections(isj)%iss)%OuEl(1,1)
         if (all_OuvEle(nk)%iuv == 91) then   !ouvrage perte de charge à la Borda
            dx = all_OuvEle(nk)%uv1
         elseif (all_OuvEle(nk)%iuv == 3 .and. .not. zegal(dx,zero,un)) then
            continue  !on continue en Saint-Venant
         elseif (all_OuvEle(nk)%iuv == 5 .and. .not. zegal(dx,zero,un)) then
            continue  !on continue en Saint-Venant
         else
            call discretise_bief0b(isj,iflag,secval_i,secval_j)
            if (iflag /= 1) call discretise_bief6b(isj,isens)
            if (associated(secval_j,sec_val_2)) then
               secval_j => sec_val_1 ; secval_i => sec_val_2
            else
               secval_i => sec_val_1 ; secval_j => sec_val_2
            endif
            cycle
         endif
      else
         nk = -1 ! on en a besoin pour l'appel à discretise_bief4b() plus loin
      endif

      !---équation dynamique
      c6 = c60  ;  c5 = 2._long*c6/dx
      !---méthode 0 : pas de coefficient d'amortissement de [beta*q**2/s]
      !ak1 = -dqi-dqj+c5*(phii-phij)  !contribution de [beta*q**2/s]
      !---méthode 1 : le coefficient d'amortissement de [beta*q**2/s] est
      !               mis à jour à chaque itération
      !ak1 = -secval_i%dq-secval_j%dq+c5*(secval_i%phi-secval_j%phi) * relax(max(secval_i%fr,secval_j%fr)) !contribution de [beta*q**2/s]
      !---méthode 2 : le coefficient d'amortissement de [beta*q**2/s] n'est pas mis à jour
      ak1 = -secval_i%dq-secval_j%dq+c5*(secval_i%phi-secval_j%phi) * alpha(isi) !contribution de [beta*q**2/s]

      !contribution de [g*s*(dz/dx)]
      somds = secval_i%ds+secval_j%ds  ;  sij = st(isi)+st(isj)+theta*somds
      !deltaz = yt(isi)-yt(isj)-pente(isi)
      deltaz = zt(isi)-zt(isj)
      ak1 = ak1+c5*gs2*(sij*(secval_i%dz-secval_j%dz)+somds*deltaz)
      !contribution des pertes de charge linéaires [g*s*j]
      if (sch2 > zero) then
         !discrétisation b
         dsji = secval_i%s*secval_i%aj-st(isi)*ajt(isi)
         dsjj = secval_j%s*secval_j%aj-st(isj)*ajt(isj)
         ak1 = ak1-2._long*c6*gs2*(dsji+dsjj)
      else
         !discrétisation a (génère des pb de convergence)
         ak1 = ak1-c6*gs2*(sij*(secval_i%daj+secval_j%daj)+(ajt(isi)+ajt(isj))*somds)
      endif
      !contribution des pertes de charge singulieres [g*s*js]
      call discretise_bief4b(ajs1,nk,secval_i,secval_j)
      ajs0 = ajst(isj)
      ak1 = ak1-c5*(sij*(ajs1-ajs0)+ajs0*somds)

      !contribution des apports/fuites
      call discretise_bief5b(isj,akqv,dqd,secval_i,secval_j)

      bool = bool .and. (dt>5._long*dtmin)
      if (bool) then
         call war002  ;  return
      endif
      ak1 = ak1+c6*akqv

      !équation de continuité
      if (sch1 < un) then
         !discrétisation de dst/dt + dq/dx = q (type s)
         ak2 = (secval_i%dq-secval_j%dq)-(secval_i%dstot+secval_j%dstot)/c5
         ! NOTE: définir NEWT à -3 pour empêcher la prise en compte des déversements latéraux dans les itérations
         if (newt /= -3) ak2 = ak2 - dqd*dx
      else
         !discrétisation de lt*dz/dt + dq/dx = q (type l)
         altm = (secval_i%alt+secval_j%alt)*0.5_long
         ak2 = (secval_i%dq-secval_j%dq)-altm*(secval_i%dz+secval_j%dz)/c5-dqd*dx
      endif
      !------------------------------------------------------------------------------
      !                           -1                   -1
      !                  [a11 a12]            [b11 b12]
      !     produit par  [       ]    ou par  [       ]
      !                  [a21 a22]            [b21 b22]
      !------------------------------------------------------------------------------
      c5 = ak1  ;  c6 = ak2
      call discretise_bief6b(isj,isens)

      if (associated(secval_j,sec_val_2)) then
         secval_j => sec_val_1 ; secval_i => sec_val_2
      else
         secval_i => sec_val_1 ; secval_j => sec_val_2
      endif
   enddo
end subroutine discretise_biefb
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine discretise_bief0(isj,iflag,scti,sctj)
!==============================================================================
!                 linéarisation des lois d'ouvrage
!
!--->iflag = 1 : discrétisation de type point fixe sur cet ouvrage
!--->iflag = 0 : discrétisation de type Newton-Raphson sur cet ouvrage
!==============================================================================
   use casiers, only: qds1
   use data_num_fixes, only: newt
   use hydraulique, only: zt
   use matrice_stv, only: a, b, d, e
   use data_num_mobiles, only: t
   implicit none
   ! -- prototype --
   integer,intent(in) :: isj
   integer,intent(inout) :: iflag
   type(section_values),intent(in) :: scti, sctj
   ! -- constantes --
   real(kind=long),parameter :: alp=1.5_long , eps=1.e-05_long
   real(kind=long),parameter :: fact=0.5_long/eps , pmax=1.e+05_long
   ! -- variables locales --
   integer :: k, nk, ns, ink, iuvnk, isi
   real(kind=long) :: deb, qdlat, dqdlat, y1, y2
   real(kind=long) :: dqzam, dqzam1, dqzam2, dqzav, dqzav1, dqzav2


   ns = la_topo%sections(isj)%iss
   isi = isj-1
   if ( newt > -1 ) then
      do k = 1, all_OuvCmp(ns)%ne
         if ( all_OuvEle(all_OuvCmp(ns)%OuEl(k,1))%iuv < 8 ) then
            !sur les buses, les déversoirs trapézoïdaux et les ouvrages définis par
            !l'utilisateur on fait toujours du Newton-Raphson (ouvrages de types 8 et 9)
            a(isj) = zero  ;  b(isj) = -un
            d(isi) = -un   ;  e(isi) = zero
            iflag = 1  ;  return
         endif
      enddo
   endif

   iflag = 0
   if (all_OuvEle(all_OuvCmp(ns)%OuEl(1,1))%iuv == 99) then  !ouvrage non défini : on transfère
                                   !z et q sans changement
      a21 = un   ; a22 = zero ; b21 = un   ; b22 = zero ; b23 = sctj%q-scti%q !équation de continuité
      a11 = zero ; a12 = un   ; b11 = zero ; b12 = un   ; b13 = sctj%z-scti%z !équation dynamique
      return
   elseif (all_OuvEle(all_OuvCmp(ns)%OuEl(1,1))%iuv == 3) then  !pompes
      !équation de continuité
      a21 = un ; a22 = zero ; b21 = un ; b22 = zero ; b23 = sctj%q-scti%q
      b23 = b23 + debit_pompe_total(ns,t)
      !équation dynamique
      a11 = zero ; a12 = un ; b11 = zero ; b12 = un ; b13 = sctj%z-scti%z
      return
   elseif (all_OuvEle(all_OuvCmp(ns)%OuEl(1,1))%iuv == 5) then  !déversoir latéral (prise en
                                                                !compte des échanges entre biefs)
      !équation de continuité
      a21 = un ; a22 = zero ; b21 = un ; b22 = zero ; b23 = sctj%q-scti%q
      do k = 1, all_OuvCmp(ns)%ne
         nk = all_OuvCmp(ns)%OuEl(k,1) ; ink = all_OuvEle(nk)%irf
         y1 = sctj%z - all_OuvCmp(ns)%zfs
         if (ink > 0) then
            y2 = zt(ink)
         else
            y2 = -99999._long
         endif
         qdlat = debit(nk,sctj%z,y2) ; dqdlat = debit(nk,sctj%z+eps,y2)
         b22 = b22+(dqdlat-qdlat)/eps  ;  b23 = b23+qdlat
         if (ink > 0) then
            qds1(total,ink) = -qdlat/abs(xgeo(ink)-xgeo(ink+1))
         endif
      enddo
      !équation dynamique
      a11 = zero ; a12 = un ; b11 = zero ; b12 = un ; b13 = sctj%z-scti%z
      return
   endif
   !---autres ouvrages
   a21 = un ; a22 = zero ; b21 = un ; b22 = zero ; b23 = sctj%q-scti%q !équation de continuité
   deb = zero
   dqzam1 = zero ; dqzav1 = zero ; dqzam2 = zero ; dqzav2 = zero   !équation dynamique
   do k = 1, all_OuvCmp(ns)%ne
      nk = all_OuvCmp(ns)%OuEl(k,1) ; iuvnk = all_OuvEle(nk)%iuv
      if (iuvnk == 1 .or. iuvnk == 3 .or. iuvnk == 7) then
         !cas des pompes et clapets (normaux ou inversés)
         a(isj) = zero ; b(isj) = -un ; d(isi) = -un
         e(isi) = zero ; iflag = 1
         return
      endif
   enddo
   deb = debit_total(ns,scti%z,sctj%z)
   dqzam=(debit_total(ns,scti%z+eps,sctj%z)-debit_total(ns,scti%z-eps,sctj%z))*fact
   dqzav=(debit_total(ns,scti%z,sctj%z+eps)-debit_total(ns,scti%z,sctj%z-eps))*fact
   !--->limitation des pentes à pmax
   if (abs(dqzam) > pmax) dqzam = sign(pmax,dqzam)
   if (abs(dqzav) > pmax) dqzav = sign(pmax,dqzav)
   if (newt /= -2) then  !discrétisation standard (newt=-1)
      a11 = zero
      b13 = sctj%q-deb
      if (abs(dqzam) > eps) then
         a12 = dqzam
      else   !déversoir dénoyé coulant à l'envers
         a12 = un
      endif
      b11 = un
      if (abs(dqzav) > eps) then
         b12 = -dqzav
      else
         b12 = un
      endif
   else  !discrétisation modifiée pour les pertes de charge trés faibles (newt=-2)
      a11 = -un
      if (abs(dqzam) > eps) then
         a12 = 2._long*dqzam
      else  !déversoir dénoyé coulant à l'envers
         a12 = un
      endif
      b11 = un
      if (abs(dqzav) > eps) then
         b12 = -2._long*dqzav
      else
         b12 = un
      endif
      b13 = scti%q+sctj%q-2._long*deb
   endif
end subroutine discretise_bief0
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine discretise_bief0b(isj,iflag,scti,sctj)
!==============================================================================
!                   actualisation des lois d'ouvrage
!
!--->iflag = 1 : discrétisation de type point fixe sur cet ouvrage
!--->iflag = 0 : discrétisation de type Newton-Raphson sur cet ouvrage
!
!---modification du 30-10-1996 : les ouvrages non définis sont pris en compte
!                                comme des singularités qui transferent q et z
!   sans changements. (modif de bief, biefb bief0 et bief0b)
!==============================================================================
   use casiers, only: qds1
   use data_num_fixes, only: newt
   use hydraulique, only: zt
   use solution, only: dzt=>dz
   implicit none
   ! -- prototype --
   integer,intent(in) :: isj
   integer,intent(inout) :: iflag
   type(section_values),intent(in) :: scti, sctj
   ! -- variables locales --
   integer :: k, ink, iuvnk, nk,ns, isi
   real(kind=long) :: deb0,deb1, dxx, yam,yav, ylat0,ylat1
   real(kind=long), parameter :: fuite = 0.0005_long
   real(kind=long), pointer :: ak1=>c5, ak2=>c6


   isi = isj-1
   ns = la_topo%sections(isj)%iss
   do k = 1, all_OuvCmp(ns)%ne
      if ((newt > -1) .and. (all_OuvEle(all_OuvCmp(ns)%OuEl(k,1))%iuv < 8)) then
         !sur les buses, les déversoirs trapézoïdaux et les ouvrages définis par
         !l'utilisateur on fait toujours du Newton-Raphson (ouvrage de type 8 et 9)
         iflag = 1 ; return
      else
         iflag = 0
      endif
   enddo
   if (all_OuvEle(all_OuvCmp(ns)%OuEl(1,1))%iuv == 99) then
      !ouvrage non défini : transfert de z et q sans changements
      ak2 = scti%dq-sctj%dq
      ak1 = scti%dz-sctj%dz
   elseif (all_OuvEle(all_OuvCmp(ns)%OuEl(1,1))%iuv == 3) then  !pompe
      !équation dynamique
      ak1 = scti%z-sctj%z
      !équation de continuité
      deb0 = zero
      deb1 = zero
      ak2 = (deb0-deb1)+(scti%dq-sctj%dq)
   elseif (all_OuvEle(all_OuvCmp(ns)%OuEl(1,1))%iuv == 5) then  !déversoir latéral
      !équation dynamique
      ! TODO: vérifier si ce n'est pas mieux avec ak1 = dzi-dzj
      ak1 = scti%z-sctj%z
      !équation de continuité
      deb0 = zero
      deb1 = zero
      do k = 1, all_OuvCmp(ns)%ne
         nk = all_OuvCmp(ns)%OuEl(k,1)
         ink = all_OuvEle(nk)%irf
         if (ink > 0) then
            ylat0 = zt(ink)
            ylat1 = ylat0+dzt(ink)
         else
            ylat0 = zero
            ylat1 = zero
         endif
         deb1 = deb1+debit(nk,scti%z,ylat1)
         deb0 = deb0+debit(nk,scti%z-scti%dz,ylat0)
         if (ink > 0) then
            dxx = abs(xgeo(ink)-xgeo(ink+1))
            qds1(total,ink) = qds1(total,ink)-(deb1-deb0)/dxx
         endif
      enddo
      ak2 = (deb0-deb1)+(scti%dq-sctj%dq)
   else  !ouvrages autres que les déversoirs latéraux
      !équation de continuité (il faut laisser cette expression même si ak2 est toujours nul en principe)
      ak2 = scti%dq-sctj%dq
      !équation dynamique
      yam = scti%y-scti%dz
      yav = sctj%y-sctj%dz
      deb0 = zero
      deb1 = zero
      do k = 1, all_OuvCmp(ns)%ne
         nk = all_OuvCmp(ns)%OuEl(k,1)
         iuvnk = all_OuvEle(nk)%iuv
         if (iuvnk == 1 .or. iuvnk == 3 .or. iuvnk == 7) then  !cas des pompes et clapets
            iflag = 1 ; return
         endif
      enddo
      deb0 = debit_total(ns,scti%z-scti%dz,sctj%z-sctj%dz)
      deb1 = debit_total(ns,scti%z,sctj%z)
      if (newt /= -2) then
         ak1 = deb1-deb0-sctj%dq
      else
         ak1 = (deb1-deb0)-sctj%dq-scti%dq
      endif
   endif
end subroutine discretise_bief0b
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine discretise_bief1(isj,scti,sctj)
!==============================================================================
!
!             contribution de [beta*q**2/s] (calcul de a1 à a5)
!
!---20/10/2005 : coefficient d'amortissement de [beta*q**2/s] pour le torrentiel
!---03/12/2005 : sauvegarde du coefficient d'amortissement de [beta*q**2/s] pour
!                pour gérer sa mise à jour lors des itérations.
!==============================================================================
   implicit none
   ! -- prototype --
   integer,intent(in) :: isj
   type(section_values),intent(in) :: scti, sctj
   ! -- variables --
   real(kind=long) :: alpha_isi

   !--->qps=2*q*psi ; q2ph=phi*q*q ; qqps=q*q*psi
   alpha_isi = relax((scti%fr+sctj%fr)*0.5_long)
   a11 = -scti%qps*c5  * alpha_isi
   a12 = -scti%q2ph*c5 * alpha_isi
   b11 =  sctj%qps*c5  * alpha_isi
   b12 =  sctj%q2ph*c5 * alpha_isi
   b13 = (sctj%qqps-scti%qqps)*c6 * alpha_isi
   alpha(isj-1) = alpha_isi

end subroutine discretise_bief1
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine discretise_bief2(scti,sctj)
!==============================================================================
!        contribution de [g*s*(dz/dx)] (calcul de a6 à a10)
!==============================================================================
   implicit none
   ! -- prototype --
   type(section_values),intent(in) :: scti, sctj
   ! -- variables --
   real(kind=long) :: dydx, soms

   dydx = sctj%z-scti%z
   soms = scti%s+sctj%s

   a12 = a12+gs2*(scti%al*dydx-soms)*c5
   b12 = b12+gs2*(sctj%al*dydx+soms)*c5
   b13 = b13+gs2*soms*dydx*c6
end subroutine discretise_bief2
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine discretise_bief3(scti,sctj)
!==============================================================================
!           contribution de [g*s*j]     (calcul de a11 à a15)
!==============================================================================
   use data_num_fixes, only: sch2, theta
   use data_num_mobiles, only: dt
   implicit none
   ! -- prototype --
   type(section_values),intent(in) :: scti, sctj
   ! -- variables --
   real(kind=long) :: c4,somj,soms
   !------------------------------------------------------------------------------
   !--->qde=abs(q)/de/de ; dj=dde/de*aj
   c4 = g*theta*dt
   if (sch2>zero) then  !discrétisation B
      a11 = a11+c4*2._long*scti%s*scti%qde
      a12 = a12+c4*(scti%aj*scti%al-2._long*scti%s*scti%dde/scti%de*scti%aj)
      b11 = b11+c4*2._long*sctj%s*sctj%qde
      b12 = b12+c4*(sctj%aj*sctj%al-2._long*sctj%s*sctj%dde/sctj%de*sctj%aj)
      b13 = b13+g*dt*(scti%s*scti%aj+sctj%s*sctj%aj)
   else                 !discrétisation A
      soms = scti%s+sctj%s
      somj = (scti%aj+sctj%aj)*0.5_long
      a11 = a11+c4*soms*scti%qde
      a12 = a12+c4*((somj    )*scti%al-soms*scti%dde/scti%de*scti%aj)
      b11 = b11+c4*soms*sctj%qde
      b12 = b12+c4*((somj    )*sctj%al-soms*sctj%dde/sctj%de*sctj%aj)
      b13 = b13+g*dt*soms*(somj    )
   endif
end subroutine discretise_bief3
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
function fb4(x,se,pl)
!==============================================================================
!       fonction utilisée pour la modélisation de la perte de charge
!       due à un élargissement ou retrécissement brusque
!==============================================================================
   implicit none
   ! -- prototype --
   real(kind=long) :: fb4
   real(kind=long),intent(in) :: x,se,pl
   ! -- variables --
   real(kind=long) :: x0

   x0 = pi*((x-se)/(pl-se)-0.5_long)
   fb4 = 0.5_long*(un+sin(x0))
end function fb4
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
function dfb4(x,se,pl)
!==============================================================================
!                dérivée de fb4 par rapport à x (?)
!==============================================================================
   implicit none
   ! -- le prototype --
   real(kind=long) :: dfb4
   real(kind=long),intent(in) :: x,se,pl
   ! -- les variables --
   real(kind=long) :: x0,a

   a = un/(pl-se)
   x0 = pi*(a*(x-se)-0.5_long)
   dfb4 = pi*0.5_long*a*cos(x0)
end function dfb4
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine discretise_bief4(nk,isj,scti,sctj)
!==============================================================================
!                  contribution de [g*s*js]
!    (calcul de a16 à a20 : perte de charge singulière à la Borda)
!==============================================================================
   use data_num_mobiles, only: dx
   use hydraulique, only: ajst
   use Ouvrages, only: all_OuvEle
   implicit none
   ! -- prototype --
   integer,intent(in) :: nk, isj
   type(section_values),intent(in) :: scti, sctj
   ! -- constantes --
   real(kind=long),parameter :: g2=2._long/g
   real(kind=long), parameter :: seuil0 = 0.1_long
   ! -- les variables --
   real(kind=long) :: ajs, ak, akij, bkij, c2, c3, c4
   real(kind=long) :: dtgei, dtgej, dv2, seuil, soms, tgemax, tge
   character(len=180) :: err_message
   logical :: goto99, bool
!------------------------------------------------------------------------------
   goto99 = .false.
   !--->xl=s/y ; v=q/s ; yl=(al-xl)/y ; vs=v/s
   if (nk < 1) then
      bool = .true.
   else if (all_OuvEle(nk)%iuv == 3 .and. dx>zero) then
      bool = .true.
   else if (all_OuvEle(nk)%iuv == 5 .and. dx>zero) then
      bool = .true.
   else if (all_OuvEle(nk)%iuv>98) then
      bool = .true.
   else
      bool = .false.
   endif
   if (bool) then  !section non singulière
      tge    = (sctj%xl-scti%xl)*0.5_long/dx
      dtgei  = scti%yl
      dtgej  = sctj%yl
      seuil  = seuil0
      tgemax = un
      ak     = un
   else if (all_OuvEle(nk)%iuv>90) then  !section singulière avec perte de charge à la Borda
      if (sctj%s > scti%s) then
         tge = sqrt(sctj%s-scti%s)*0.5_long/dx
      else
         tge = -sqrt(scti%s-sctj%s)*0.5_long/dx
      endif
      seuil  = all_OuvEle(nk)%uv2
      tgemax = 2._long*seuil
      dtgei  = -scti%al
      dtgej  = sctj%al
      ak     = all_OuvEle(nk)%uv4
   else
      write(err_message,'(a)') ' >>>> erreur dans discretise_bief4 <<<<'
      write(error_unit,'(a)') trim(err_message) ; write(lTra,'(a)') trim(err_message)
      write(err_message,'(a,i3.3,a,i3)') ' iuv(',nk,')  =  ',all_OuvEle(nk)%iuv
      write(error_unit,'(a)') trim(err_message) ; write(lTra,'(a)') trim(err_message)
      write(err_message,'(a)') 'Merci d''envoyer un rapport de bug'
      write(error_unit,'(a)') trim(err_message) ; write(lTra,'(a)') trim(err_message)
      stop 109
   endif
   if (scti%v>zero .and. sctj%v>zero) then
      if (tge<seuil) then
         ajs = zero
         goto99 = .true.
      elseif (tge>tgemax) then
         akij = ak
         bkij = zero  !bkij désigne la dérivée de akij par rapport à tge
      else
         akij = ak*fb4(tge,seuil,tgemax)
         bkij = ak*dfb4(tge,seuil,tgemax)
      end if
   else if (scti%v<zero .and. sctj%v<zero) then
      if (-tge<seuil) then
         ajs = zero
         goto99 = .true.
      else if (-tge>tgemax) then
         akij = -ak
         bkij = zero  !bkij désigne la dérivée de akij par rapport à tge
      else
         akij = -ak*fb4(-tge,seuil,tgemax)
         bkij = -ak*dfb4(-tge,seuil,tgemax)
      end if
   else
      ajs = zero
      goto99 = .true.
   endif
   !---discrétisation pour une perte de charge singulière
   if (.not.goto99) then
      dv2 = (scti%v-sctj%v)*0.5_long
      c3 = akij*dv2
      ajs = c3*dv2
      soms = scti%s+sctj%s
      c2 = c3*soms*c5
      c4 = soms*dv2*dv2*bkij/(2._long*dx)
      a11 = a11+c2/scti%s
      b11 = b11-c2/sctj%s
      a12 = a12+(c3*scti%al*(dv2-soms*scti%vs)-c4*dtgei)*c5
      b12 = b12+(c3*sctj%al*(dv2+soms*sctj%vs)+c4*dtgej)*c5
      b13 = b13+ajs*soms*c6
   endif
   ajst(isj) = ajs

   if (nk>0) then
      if (all_OuvEle(nk)%iuv>90) all_OuvEle(nk)%uv3 = ajs*g2
   endif
end subroutine discretise_bief4
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine discretise_bief4b(ajs,nk,scti,sctj)
!==============================================================================
!       contribution de g*s*js (perte de charge singulière à la Borda)
!==============================================================================
   use data_num_mobiles, only: dx
   implicit none
   ! -- le prototype --
   real(kind=long),intent(inout) :: ajs
   integer,intent(in) :: nk
   type(section_values),intent(in) :: scti, sctj
   ! -- les constantes --
   real(kind=long), parameter :: g2 = 2._long / g
   real(kind=long), parameter :: seuil0 = 0.10_long
   ! -- les variables --
   real(kind=long) :: ak,akij
   real(kind=long) :: dv2
   real(kind=long) :: seuil
   real(kind=long) :: tge, tgemax
   logical :: goto99, bool
   !------------------------------------------------------------------------------
   goto99 = .false.
   if (nk < 1) then
      bool = .true.
   else if (all_OuvEle(nk)%iuv == 3 .and. dx>zero) then
      bool = .true.
   else if (all_OuvEle(nk)%iuv == 5 .and. dx>zero) then
      bool = .true.
   else if (all_OuvEle(nk)%iuv>98) then
      bool = .true.
   else
      bool = .false.
   endif
   if (bool) then
      tge = (sctj%xl-scti%xl)*0.5_long/dx
      seuil = seuil0
      tgemax = un
      ak = un
   else if (all_OuvEle(nk)%iuv>90) then
      if (sctj%s > scti%s) then
         tge =  sqrt(sctj%s-scti%s)*0.5_long/dx
      else
         tge = -sqrt(scti%s-sctj%s)*0.5_long/dx
      endif
      seuil = all_OuvEle(nk)%uv2
      tgemax = 2._long*seuil
      ak = all_OuvEle(nk)%uv4
   else
      write(lTra,'(a)') ' >>>> erreur dans bief4b <<<<'
      write(lTra,'(a)') 'Merci d''envoyer un rapport de bug'
      write(error_unit,'(a)') ' >>>> erreur dans bief4b <<<<'
      write(error_unit,'(a)') 'Merci d''envoyer un rapport de bug'
      stop 110
   endif
   akij = huge(zero)
   if ((scti%v > zero) .and. (sctj%v > zero)) then
      if (tge < seuil) then
         ajs = zero
         goto99 = .true.
      else if (tge > tgemax) then
         akij = ak
      else if (tge > zero) then
         akij = ak*fb4(tge,seuil,tgemax)
      end if
   else if ((scti%v < zero) .and. (sctj%v < zero)) then
      if (-tge < seuil) then
         ajs = zero
         goto99 = .true.
      else if (-tge > tgemax) then
         akij = -ak
      else if (-tge > zero) then
         akij = -ak*fb4(-tge,seuil,tgemax)
      end if
   else
      ajs = zero
      goto99 = .true.
   endif
   !---discrétisation pour une perte de charge singuliere
   if (.not.goto99) then
      if (akij > huge(zero)/2) stop '>>>> BUG dans discretise_bief4b()'
      dv2 = (scti%v-sctj%v)*0.5_long
      ajs = (akij*dv2)*dv2
   endif
   if (nk>0) then
      if (all_OuvEle(nk)%iuv>90) all_OuvEle(nk)%uv3 = ajs*g2
   endif
end subroutine discretise_bief4b
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine discretise_bief5(isj,qd,scti,sctj)
!==============================================================================
!                  contribution des apports et fin du calcul
!==============================================================================
   use apports_lateraux, only: nlat
   use casiers, only:  qds0
   use data_num_fixes, only: theta
   use data_num_mobiles, only: dt
   use hydraulique, only: qa=>qe,dqa=>dqe
   implicit none
   ! -- prototype --
   integer,intent(in) :: isj
   real(kind=long),intent(out) :: qd
   type(section_values),intent(in) :: scti, sctj
   ! -- variables locales --
   integer :: isi
   real(kind=long) :: qei

   isi = isj-1
   if (nlat > 0) then !il y a des apports latéraux
      qei = qa(isi)+theta*dqa(isi)
    else
      qei = zero
   endif
   qd = qds0(total,isi)
   qei = qei-qd  !échanges latéraux

   !  contribution des apports à l'équation dynamique
   !  dans le cas d'une perte de quantité de mvt
   if (qei < zero) then
      a11 = a11 - qei/scti%s*theta*dt
      b11 = b11 - qei/sctj%s*theta*dt
      a12 = a12 + qei*scti%vl/scti%s*theta*dt
      b12 = b12 + qei*sctj%vl/sctj%s*theta*dt
      b13 = b13 - (scti%v+sctj%v)*qei*dt
   endif

   a11 = -un-a11   ;  a12 = -a12  ;   b11 = un+b11
end subroutine discretise_bief5
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine discretise_bief5b(isj,akqv,dqd,scti,sctj)
!==============================================================================
!          contribution des apports et des déversements latéraux
!==============================================================================
   use apports_lateraux, only: nlat
   use casiers, only: qds1, qds0
   use data_num_fixes, only: theta, newt
   use hydraulique, only: vt, qa=>qe, dqa=>dqe
   implicit none
   ! -- prototype --
   integer,intent(in) :: isj
   real(kind=long),intent(out) :: akqv, dqd
   type(section_values),intent(in) :: scti, sctj
   ! -- variables locales --
   integer :: isi
   real(kind=long) :: qaij

   isi = isj-1
   ! contribution des apports/fuites latéraux (donnes par .lat)
   qaij = zero  ;  if (nlat > 0) qaij = qa(isi)+theta*dqa(isi)

   ! contribution des déversements latéraux
   dqd = zero
   ! NOTE: définir NEWT à -3 pour empêcher la prise en compte des déversements latéraux dans les itérations
   if (newt /= -3) then
      dqd = qds1(total,isi) - qds0(total,isi)
      qaij = qaij - (qds0(total,isi) + theta*dqd)
   endif

   ! contribution pour l'équation de quantité de mouvement
   akqv = zero
   if (qaij < zero) akqv = qaij*((scti%v+sctj%v)-(vt(isj-1)+vt(isj)))
end subroutine discretise_bief5b
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine discretise_bief6(isj,isens,bool)
!==============================================================================
!
!                                       [ a11  a12 ]
!   inversion de la matrice élémentaire [          ]   si isens > 0.
!                                       [ a21  a22 ]
!   pour les biefs de maille ou à l'amont de la maille
!    ====> le premier balayage sera amont ---> aval
!
!                                       [ b11  b12 ]
!   inversion de la matrice élémentaire [          ]   si isens ≤ 0.
!                                       [ b21  b22 ]
!   pour les biefs à l'aval de la maille
!    ====> le premier balayage sera aval ---> amont
!==============================================================================
   use data_num_fixes, only: newt
   use hydraulique, only: aa11,aa12,aa21,aa22,b01,b02
   use matrice_stv, only: a,b,c,d,e,f
   use erreurs_stv, only: echelle
   implicit none
   ! -- prototype --
   integer, intent(in) :: isj,isens
   logical, intent(out) :: bool
   ! -- variables --
   integer :: isi
   real(kind=long) :: a11b,a12b,a21b,a22b,den
   !------------------------------------------------------------------------------
   bool = .false.
   isi = isj-1
   if (isens > 0) then
      den = a11*a22-a21*a12
      if (abs(den) < 1.e-06_long) then  !dénominateur numériquement nul : on sort
         call err009(isi,a11,a12,a21,a22)
         bool = .true.
         return
      end if
      den = -1._long/den
      a11b = a22*den
      a12b = a12*den
      a21b = a21*den
      a22b = a11*den
      !---inversion
      a(isj) = b12*a11b-b22*a12b
      b(isj) = b11*a11b-b21*a12b
      c(isj) = b23*a12b-b13*a11b
      d(isi) = b22*a22b-b12*a21b
      e(isi) = b21*a22b-b11*a21b
      f(isi) = b13*a21b-b23*a22b
   else
      den = b11*b22-b21*b12
      if (abs(den) < 1.e-06_long) then  !dénominateur numériquement nul : on sort
         call err009(isi,a11,a12,a21,a22)
         bool = .true.
         return
      end if
      den = -1._long/den
      a11b = b22*den
      a12b = b12*den
      a21b = b21*den
      a22b = b11*den
      !---inversion
      a(isj) = a12*a11b-a22*a12b
      b(isj) = a11*a11b-a21*a12b
      c(isj) = b13*a11b-b23*a12b
      d(isi) = a22*a22b-a12*a21b
      e(isi) = a21*a22b-a11*a21b
      f(isi) = b23*a22b-b13*a21b
   end if
   if (newt <= 0) then
      !---sauvegarde de a11b,a12b,a21b,a22b
      aa11(isj) = -a11b
      aa12(isj) = a12b
      aa21(isi) = a21b
      aa22(isi) = -a22b
      !---sauvegarde de c et f
      b02(isi) = f(isi)
      b01(isj) = c(isj)
      echelle  =  max(echelle,abs(f(isi)),abs(c(isj)))
   endif
end subroutine discretise_bief6
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine discretise_bief6b(isj,isens)
!==============================================================================
!
!                                       [ a11  a12 ]
!   inversion de la matrice élémentaire [          ]   si isens > 0
!                                       [ a21  a22 ]
!   pour les biefs de maille ou à l'amont de la maille
!    ====> le premier balayage sera amont ---> aval
!
!                                       [ b11  b12 ]
!   inversion de la matrice élémentaire [          ]   si isens ≤ 0
!                                       [ b21  b22 ]
!   pour les biefs à l'aval de la maille
!    ====> le premier balayage sera aval ---> amont
!==============================================================================
   use erreurs_stv, only: reste,ireste
   use hydraulique, only: aa11,aa12,aa21,aa22,b01,b02
   use matrice_stv, only: c,f
   implicit none
   ! -- le prototype --
   integer,intent(in) :: isj,isens
   ! -- les variables --
   integer :: isi
   real(kind=long) :: ak11,ak22
   real(kind=long) :: tmp
   real(kind=long), pointer :: ak1=>c5, ak2=>c6

   isi = isj-1
   !---inversion
   ak11 = ak1*aa11(isj)+ak2*aa12(isj)
   ak22 = ak1*aa21(isi)+ak2*aa22(isi)
   if (isens > 0) then
      ak1 = ak11
      ak2 = ak22
   else
      ak1 = -ak11
      ak2 = -ak22
   end if
   ! DONE: vérifier l'évaluation de F(0) sinon ce n'est pas b01 et b02 qu'il faut garder de la 1ère itération
   c(isj) = c(isj)+(b01(isj)-ak1)
   f(isi) = f(isi)+(b02(isi)-ak2)
   !---calcul des résidus
   tmp = max(abs(b01(isj)-ak1),abs(b02(isi)-ak2))
   if (reste < tmp) then
      reste = tmp
      ireste = isi
   endif
end subroutine discretise_bief6b
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine section_data(ityp,is,bool,secval)
!==============================================================================
!    calcul des grandeurs nécessaires à la discrétisation des
!                  équations de Saint-Venant
!
!--->ityp = 0 : appel par bief (1ere discrétisation)
!--->ityp = 1 : appel par biefb (actualisation)
!--->ityp = -1 : appel par EULER() (permanent)
!
!  Le résultat est dans la variable secval
!==============================================================================
   use solution, only: dqt=>dq
   use data_num_fixes, only: frmax,newt,tinf,relax_a,relax_b
   use data_num_mobiles, only: t,dt,dx,cr,iscr,frsup,ifrsup
   use hydraulique, only: qt,vt,st,ajt,stt,psit

   implicit none
   ! -- prototype --
   integer,intent(in) :: ityp, is
   logical,intent(inout) :: bool
   type(section_values), target, intent(inout) :: secval
   ! -- variables --
   real(kind=long) :: cris, ttmp, beta, vmoy
   type(profil), pointer :: prof

   prof => la_topo%sections(is)
   sec_val => secval
   bool = .false.
   if (ityp > 0) then
      sec_val%dq = dqt(is)
      sec_val%q  = qt(is) + dqt(is)
   else
      sec_val%q = qt(is)
   end if
   !------------------------------------------------------------------------------
   !   début du remplissage de sec : éléments géométriques
   !------------------------------------------------------------------------------
   call secj0(ityp,is,bool)
   if (bool) return  !la ligne d'eau de la section courante est fausse
   !------------------------------------------------------------------------------
   !   fin du remplissage de sec : éléments dynamiques
   !------------------------------------------------------------------------------
   sec_val%akmi = prof%ks(prof%main)
   if (sec_val%y>prof%ymoy) then  !il y un débordement
      call secj1(ityp,is)
   else                    !il n'y a pas de débordement
      call secj2(ityp)
      sec_val%almin = sec_val%al ; sec_val%smin = sec_val%s ; sec_val%smoy = zero ; vmoy = zero
   end if

   call secj3(ityp,is)
   !---stockage des éléments de /shyd/ (ityp=0)
   if (ityp == 0) then
      vt(is) = sec_val%v
      if (newt <= 0) then
         st(is)   = sec_val%s
         psit(is) = sec_val%qqps
         ajt(is)  = sec_val%aj
         stt(is)  = sec_val%st
      endif
   endif

   !---calcul du nombre de froude
   ! on ignore les tout petits débordements
   if (sec_val%y > prof%ymoy .and. sec_val%smoy > 0.01_long*sec_val%s) then    !cas avec débordement en lit moyen
      beta = sec_val%s*(un/sec_val%smin+sec_val%h**2/sec_val%smoy)/(un+sec_val%h)**2
      sec_val%fr = sec_val%v*sqrt(beta/(g*sec_val%s/sec_val%al+sec_val%v*sec_val%v*beta*(beta-un)))
   else                                                         !cas sans débordement
      sec_val%fr = sec_val%v*sqrt(sec_val%al/(sec_val%s*g))
   endif

   if ((sec_val%fr > frmax) .AND. (frmax < 99._long)) then    !Divergence : erreur
      ttmp = t ; if (ityp == 0 .AND. t > tinf) ttmp = t-dt
      call err010(is,sec_val%fr,ttmp,sec_val%v,sec_val%al,sec_val%s,sec_val%y,bool)
   else if (sec_val%fr > 0.999_long .AND. ityp == 0) then !On laisse fr dépasser 1, cela permet parfois
                                                !de traiter un passage localisé en torrentiel
      if (relax_a > relax_b) then
         ttmp = max(tinf,t-dt)
         call war005(is,sec_val%fr,ttmp)
      endif
   end if
   if (sec_val%fr < frmax .AND. ityp == 0) then
      if (sec_val%fr > frsup) then
         frsup = sec_val%fr
         ifrsup = is
      endif
   endif
   !---calcul du nombre de Courant (à un facteur dt près)
   if ((ityp <= 0) .AND. (dx > 0.001_long) .AND. (la_topo%sections(is)%iss == 0)) then
      cris = (abs(sec_val%v)+sqrt(g*sec_val%s/sec_val%al))/dx
      if (cris > cr) then
         cr = cris ; iscr = is
      endif
   endif
end subroutine section_data
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine secj0(ityp,is,bool)
!==============================================================================
!   calcul de z,y,yd (tirant d'eau de débordement),s,st,al,alt,p
!
!--->ityp = 0 : appel par discretise_bief (1ere discrétisation)
!--->ityp = 1 : appel par discretise_biefb (actualisation)
!==============================================================================
   use casiers, only: cdlat
   use hydraulique, only: zt
   use solution, only: dzt=>dz
   implicit none
   ! -- prototype --
   integer,intent(in) :: ityp, is
   logical,intent(inout) :: bool
   !type(section_values), pointer, intent(inout) :: sec
   ! -- variables --
   real(kind=long) :: ymax
   integer :: ip
   type(profil), pointer :: prof
   real(kind=long),parameter :: eps = 0.005_long
   !------------------------------------------------------------------------------
   !--->y =
   !---recherche des cotes donnees encadrant y ----> indice k
   !------------------------------------------------------------------------------
   prof => la_topo%sections(is)
   !---tirant d'eau
   if (ityp > 0) then
      sec_val%dz = dzt(is)
      sec_val%z = zt(is)+sec_val%dz
   else
      sec_val%z = zt(is)
   end if
   sec_val%y = sec_val%z - prof%zf  !tirant d'eau dans la section courante
   !---vérifications
   if (sec_val%y > zero) then
      if (sec_val%y > prof%ybmin) then
         ymax = prof%ybmax+100._long
         !on vérifie y >= prof%ybmin = tirant d'eau maximal de la section is (berge la + basse)
         if (cdlat(rivg,is) < zero .and. cdlat(rivd,is) < zero) then !dév. lat. interdit
            if (sec_val%y > prof%ybmin+0.5_long) then  !le débordement est trop important
               bool = .true.
               call war001(is,sec_val%y,prof%ybmin,-1)
               return
            else                             !on tolère un petit débordement
               call war001(is,sec_val%y,prof%ybmin,-1)
            endif
         elseif (sec_val%y > ymax) then !dév. lat. autorisé : warning si le déversement devient gigantesque
            bool = .true.
            call war001(is,sec_val%y,ymax,-1)
            return
         endif
      endif
   else
      !si sec_val%y est NaN la comparaison avec zero est toujours fausse
      call err025(is,sec_val%y,ityp)
   endif
   !  Géométrie
   sec_val%al  = prof%largeur(sec_val%z)   !largeur dynamique (mineur+moyen)
   sec_val%alt = sec_val%al                                !largeur totale (mineur+moyen+majeur)

   sec_val%s  = prof%section_mouillee(sec_val%z)   !section mouillée dynamique (mineur+moyen)
   sec_val%st = sec_val%s                                 !section mouillée totale (mineur+moyen+majeur)

   sec_val%p   = prof%perimetre(sec_val%z) !périmètre mouillé
   !il faut garder dp/dz
   if (is_LC) then
      ip  = prof%ip  !prof%ip a été mis à jour avec l'appel de largeur()
      sec_val%dp  = (prof%wh(ip+1)%p - prof%wh(ip)%p) / (prof%wh(ip+1)%h - prof%wh(ip)%h)
   else
      sec_val%dp = 0.5_long * (prof%perimetre(sec_val%z+eps) - prof%perimetre(sec_val%z-eps)) / eps
   endif

end subroutine secj0
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine secj1(ityp,is)
!==============================================================================
!
!   Géométrie : cas du débordement mineur/moyen
!-nb-----------------on utilise la formulation debord
!
!--->ityp = 0 : appel par bief (1ère discrétisation)
!--->ityp = 1 : appel par biefb (actualisation)
!==============================================================================
   implicit none
   ! -- prototype --
   integer,intent(in) :: ityp, is
   ! -- variable --
   type(profil), pointer :: prof

   prof => la_topo%sections(is)

   !------------------------------------------------------------------------------
   ! calcul des paramètres géométriques (AL,S,P,R) dans les lits mineurs et moyens
   ! suffixe 'min' ----> lit mineur
   ! suffixe 'moy' ----> lit moyen
   !------------------------------------------------------------------------------
   !---dimensions du lit mineur
   sec_val%almin = prof%almy
   ! IDEA: variante possible : sec_val%pmin = prof%pmy + 2._long*(sec_val%y-prof%ymoy)
   ! QUESTION: comment tester cette variante ? Peut-être sur les mêmes données que ISM
   sec_val%pmin = prof%pmy
   sec_val%smin = min(sec_val%s,prof%smy+sec_val%almin*(sec_val%y-prof%ymoy))
   !---dimensions du lit lit moyen (majeur actif)
   sec_val%almoy = sec_val%al-sec_val%almin
   sec_val%pmoy = sec_val%p-sec_val%pmin
   ! TODO: à tester : pmoy = p-pmy(is)+ 2._long*(y-yd1)
   sec_val%smoy = sec_val%s-sec_val%smin
   !---cas des petits débordements
   if (sec_val%smoy < zero) then
      call err014(is,1,sec_val)  !vérification sur la section mouillée
   elseif (sec_val%smoy<0.01_long*sec_val%s) then
      !le lit moyen est traité comme un lit majeur : pris en compte seulement dans l'équation de continuité
      sec_val%al = sec_val%almin  !largeur mouillée dynamique = largeur mineur
      sec_val%p = sec_val%pmin+2._long*(sec_val%y-prof%ymoy)
      sec_val%dp = 2._long
      sec_val%s = sec_val%smin  !section mouillée dynamique = section mineur
      sec_val%h = zero
      sec_val%smoy = zero
      sec_val%pmoy = zero
      sec_val%almoy = zero
      call secj2(ityp)
   else
      if (sec_val%pmoy < zero) call err014(is,2,sec_val)  !vérification sur le périmètre mouillé
      sec_val%akmo = prof%strickler_lit_moyen(sec_val%z)
      call secj11(ityp)
   end if
end subroutine secj1
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine secj11(ityp)
!==============================================================================
!      calcul des éléments dynamiques pour la fomulation Debord
!
!--->ityp = 0 : appel par bief (1ère discrétisation)
!--->ityp = 1 : appel par biefb (actualisation)
!==============================================================================
   implicit none
   ! -- le prototype --
   integer,intent(in) :: ityp
   ! -- constantes --
   real(kind=long),parameter :: dixtiers = 10._long/3._long
   real(kind=long),parameter :: untier = 1._long/3._long
   real(kind=long),parameter :: unsix = 1._long/6._long
   ! -- les variables
   real(kind=long) :: a,a0,a21b,ak,aksm,als,asmin
   real(kind=long) :: da,df,dh,dr,f,hsm,hh,hh2
   real(kind=long) :: r,r23,rmin,rmoy, sm1, w
   !------------------------------------------------------------------------------
   rmoy = sec_val%smoy/sec_val%pmoy
   rmin = sec_val%smin/sec_val%pmin
   !---calcul de r et dr=d(r)/dz
   r = rmoy/rmin
   als = sec_val%almin/sec_val%smin
   !---calcul de a et da=d(a)/dz
   ak = sec_val%akmo/sec_val%akmi
   a0 = 0.9_long*ak**unsix
   if (r > 0.3_long) then
      a = a0
   else
      w = pi*r*dixtiers
      a = ((a0+un)-(a0-un)*cos(w))*0.5_long
   end if
   !---calcul de h=qmoy/qmin
   asmin = a*sec_val%smin
   aksm = ak/asmin
   r23 = r**deuxtiers
   a21b = un-a*a
   f = max(1.e-4_long,sec_val%smoy*(sec_val%smoy+sec_val%smin*a21b))  !JBF 25/01/2005 : pour éviter un plantage quand
   f = sqrt(f)                               !                 Kmoy >> Kmin (détecté sur Hogneau)
   sec_val%h = aksm*f*r23
   hh = un/(un+sec_val%h)
   hh2 = hh*hh
   hsm = sec_val%h/sec_val%smoy
   sm1 = un/sec_val%smin
   !calcul de psi = beta/S (Boussinesq / section mouillée)
   sec_val%psi = (sec_val%h*hsm+sm1)*hh2
   !calcul de la débitance et de sa dérivée dde=d(de)/dz
   sec_val%de = sec_val%akmi*asmin*(un+sec_val%h)*rmin**deuxtiers
   !calcul de la perte de charge aj
   sec_val%qde = sec_val%q/(sec_val%de*sec_val%de)  ! = 0.5*dJ/dQ = abs(Q)/De**2
   sec_val%aj = sec_val%q * sec_val%qde
   if (sec_val%q < zero) then
      sec_val%aj = -sec_val%aj
      sec_val%qde = -sec_val%qde
   end if
   !---calcul des dérivées
   if (ityp > 0) then
      return
   else
      !calcul de dr=d(r)/dz
      dr = r*(sec_val%almoy/sec_val%smoy-sec_val%dp/sec_val%pmoy-als)
      !calcul de da=d(a)/dz
      if (r > 0.3_long) then
         da = zero
      else
         da = pi*(a0-un)*sin(w)*dr*(un+deuxtiers)
      end if
      !calcul de df=d(f)/dz
      df = 2._long*sec_val%smoy*(sec_val%almoy-asmin*da)
      df = (df+a21b*(sec_val%almin*sec_val%smoy+sec_val%almoy*sec_val%smin))/f*0.5_long
      !calcul de dh=d(h)/dz
      dh = (df-f*(da/a+als))*r23
      dh = (dh+deuxtiers*dr*r**untier*f)*aksm
      !calcul de phi=d(psi)/dz
      sec_val%phi = hsm*(2._long*dh-hsm*sec_val%almoy)-als*sm1
      sec_val%phi = hh2*sec_val%phi-2._long*dh*hh*sec_val%psi
      !calcul de dde=d(de)/dz
      sec_val%dde = sec_val%de*(da/a+(un+deuxtiers)*als+dh*hh)
   endif
end subroutine secj11
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine secj2(ityp)
!==============================================================================
!         calcul des éléments dynamiques : cas sans débordement
!
!--->ityp = 0 : appel par bief (1ère discrétisation)
!--->ityp = 1 : appel par biefb (actualisation)
!==============================================================================
   implicit none
   ! -- le prototype --
   integer,intent(in) :: ityp
   ! -- les variables --
   real(kind=long), parameter :: cinqtiers = 5._long/3._long
   !------------------------------------------------------------------------------
   sec_val%psi = un/sec_val%s
   sec_val%de = sec_val%akmi*sec_val%s*(sec_val%s/sec_val%p)**deuxtiers
   !calcul de la perte de charge aj
   sec_val%qde = sec_val%q/(sec_val%de*sec_val%de)
   sec_val%aj = sec_val%q * sec_val%qde
   if (sec_val%q < zero) then
      sec_val%aj = -sec_val%aj
      sec_val%qde = -sec_val%qde
   end if
   if (ityp > 0) then
      return
   else
      sec_val%phi = -sec_val%al/(sec_val%s*sec_val%s)
      sec_val%dde = sec_val%de*(cinqtiers*sec_val%al/sec_val%s - deuxtiers*sec_val%dp/sec_val%p)
   endif
end subroutine secj2
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine secj3(ityp,is)
!==============================================================================
!     calcul des elements de /sectj/ complémentaires (dynamiques)
!
!--->ityp = 0 : appel par bief (1ère discrétisation)
!--->ityp = 1 : appel par biefb (actualisation)
!==============================================================================
   use hydraulique, only: st,ajt,stt,psit
   implicit none
   ! -- le prototype --
   integer,intent(in) :: ityp, is
   !------------------------------------------------------------------------------
   sec_val%xl = sec_val%s/sec_val%y
   sec_val%v = sec_val%q/sec_val%s
   if (ityp > 0) then                 ! discrétisations suivantes : appel venant de biefb
      sec_val%psi   = sec_val%psi*sec_val%q*sec_val%q
      sec_val%phi   = sec_val%psi-psit(is)
      sec_val%daj   = sec_val%aj-ajt(is)
      sec_val%ds    = sec_val%s-st(is)
      sec_val%dstot = sec_val%st-stt(is)
      sec_val%dsmaj = sec_val%yl-sec_val%ds
   else                               ! 1ère discrétisation : appel venant de bief
      sec_val%q2ph  = sec_val%q*sec_val%q*sec_val%phi
      sec_val%qps   = sec_val%q*sec_val%psi
      sec_val%qqps  = sec_val%qps*sec_val%q
      sec_val%qps   = sec_val%qps*2._long
      sec_val%vs    = sec_val%v/sec_val%s
      sec_val%yl    = (sec_val%al-sec_val%xl)/sec_val%y
      sec_val%almaj = sec_val%alt-sec_val%al
      sec_val%vl    = sec_val%al*sec_val%v
   endif
end subroutine secj3
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
function relax(x)
!==============================================================================
!fonction valant 1 si x < a et 0 si x > 1 avec raccord régulier par un cosinus
!
! 18-01-2006 : suppression du raccord par cosinus pour un raccord linéaire
! 18-01-2006 : création d'un paramètre pour pouvoir modifier la valeur minimale
!              de la fonction.
!==============================================================================
   use data_num_fixes, only : relax_a, relax_b, reste
   implicit none
   real(kind=long), intent(in) :: x
   real(kind=long) :: relax
   !------------------------------------------------------------------------------
   if (relax_a > relax_b) then
      relax = un
   else
      if (x < relax_a) then
         relax = un
      else if (x > relax_b) then
         relax = reste
      else
         relax = (reste-un)/(relax_b-relax_a)*(x-relax_a)+un
      endif
   endif
end function relax


function debitance_Debord(is,z,Kmin,Kmoy)
!==============================================================================
!
!             Calcul de la débitance en appliquant la formule Debord
!
! Variables d'entrée :
!     - is : numéro de section
!     - z : niveau d'eau
!     - Kmin et Kmoy : Strikler en lit mineur et moyen
!==============================================================================
   implicit none
   ! prototype
   real(kind=long) :: debitance_debord
   integer, intent(in) :: is
   real(kind=long), intent(in) :: z, Kmin, Kmoy
   ! variables locales
   real(kind=long) :: lmin, pmin, smin, lmoy, pmoy, smoy, Deb
   real(kind=long) :: ldyn, pdyn, sdyn
   real(kind=long) :: p1, rmin, rmoy, r, a, a0, aksm, f, r23, h, y
   logical :: debord_ok
   real(kind=long), parameter :: dixtiers=10._long/3._long
   type(profil), pointer :: sec_is

   sec_is => la_topo%sections(is)  !section de calcul courante (profil)
   y = z - sec_is%zf
   ! calcul
   include 'mage_include_debord.f90' !même bloc de code que celui utilisé dans la routine Debord()
   if (debord_ok) then
      rmoy = smoy/pmoy  ! rayons hydrauliques
      rmin = smin/pmin
      r = rmoy/rmin
      !---calcul de a
      a0 = 0.9_long*(Kmoy/Kmin)**deuxtiers
      if (r > 0.3_long) then
         a = a0
      else
         a = (un+a0-(a0-un)*cos(pi*r*dixtiers))*0.5_long
      end if
      !---calcul de h=qmoy/qmin
      aksm = Kmoy/(Kmin*a*smin)
      r23 = r**deuxtiers
      f = sqrt(max(1.e-4_long,smoy*(smoy+smin*(un-a*a))))  ! pour éviter un plantage quand
                                                           ! Kmoy >> Kmin (détecté sur Hogneau)
      h = aksm*f*r23
      Deb = Kmin*smin*(un+h)*rmin**deuxtiers
   else
      if (sdyn < 0.01_long) then
         write(l9,*) 'Erreur dans Debord() : sdyn = ',sdyn,y,sec_is%ymoy,is
         !stop
      endif
      p1 = un / pdyn
      Deb = Kmin*sdyn*(sdyn*p1)**deuxtiers
   endif
   debitance_debord = Deb
end function debitance_Debord



subroutine debord(is,y,Q,Kmin,Kmoy,Qmin,Qmoy,Vmin,Vmoy,Deb,Beta)
!==============================================================================
!
!                   Application de la formule Debord
!
! Variables d'entrée :
!     - is : numéro de section
!     - y : tirant d'eau
!     - Q : débit
!     - Kmin et Kmoy : Strikler en lit mineur et moyen
! Variables de sortie :
!     - Qmin et Qmoy : débits partiels en lits mineur et moyen
!     - Vmin et Vmoy : vitesses moyennes en lits mineur et moyen
!     - Deb : débitance
!     - Beta : coefficient de Boussinesq
!==============================================================================
   implicit none
   ! prototype
   integer, intent(in) :: is
   real(kind=long), intent(in) :: y, Q, Kmin, Kmoy
   real(kind=long), intent(out) :: Qmin, Qmoy, Vmin, Vmoy, Deb, Beta
   ! variables locales
   real(kind=long) :: lmin, pmin, smin, lmoy, pmoy, smoy
   real(kind=long) :: ldyn, pdyn, sdyn
   real(kind=long) :: p1, rmin, rmoy, r, a, a0, aksm, f, r23, h, z
   logical :: debord_ok
   real(kind=long), parameter :: dixtiers=10._long/3._long
   type(profil), pointer :: sec_is

   sec_is => la_topo%sections(is)  !section de calcul courante (profil)
   z = sec_is%zf + y
   ! calcul
   include 'mage_include_debord.f90' !même bloc de code que celui utilisé dans la routine debitance_Debord()
   if (debord_ok) then
      rmoy = smoy/pmoy  ! rayons hydrauliques
      rmin = smin/pmin
      r=rmoy/rmin
      !---calcul de a
      a0 = 0.9_long*(Kmoy/Kmin)**deuxtiers
      if (r > 0.3_long) then
         a = a0
      else
         a = (un+a0-(a0-un)*cos(pi*r*dixtiers))*0.5_long
      end if
      !---calcul de h=qmoy/qmin
      aksm = Kmoy/(Kmin*a*smin)
      r23 = r**deuxtiers
      f = sqrt(max(1.e-4_long,smoy*(smoy+smin*(un-a*a))))  ! pour éviter un plantage quand
                                                           ! Kmoy >> Kmin (détecté sur Hogneau)
      h = aksm*f*r23
      Qmin = Q / (un+h)  ;  Vmin = Qmin / smin
      Qmoy = Q - Qmin    ;  Vmoy = Qmoy / smoy
      Beta = sdyn * (h*h/smoy+un/smin)/(un+h)/(un+h)
      Deb = Kmin*smin*(un+h)*rmin**deuxtiers
      if (is_NaN(Qmin)) write(l9,*) ' Debord -> NaN : ', r, r23, f, h, smin, smoy, y, sec_is%ymoy
   else
      if (sdyn < 0.01_long) then
         write(l9,*) 'Erreur dans Debord() : sdyn = ',sdyn,y,sec_is%ymoy,is
         !stop
      endif
      Qmin = Q ; Qmoy = zero  ;  Vmin = Qmin / sdyn  ;  Vmoy = zero
      p1 = un / pdyn
      Deb = Kmin*sdyn*(sdyn*p1)**deuxtiers
      Beta = un
   endif
end subroutine debord


subroutine Debord_IniFin(nomfin,q,z)
!==============================================================================
!        Ecriture de la derniere ligne d'eau au format .INI
!
!==============================================================================
   use parametres, only: gravite=>g
   use data_num_fixes, only: encodage
   use data_num_mobiles, only: t,dt
   use hydraulique, only: qe
   use casiers, only: qds1, cdlat, ydev
   use mage_results, only: date_fin, type_resultat, pm, z_fd, largeur_totale, largeur_mineur, z_max, qtot, qfp
   implicit none
   ! -- le prototype --
   character(len=12),intent(in) :: nomfin
   real(kind=long),intent(in) :: q(:),z(:)
   ! -- les constantes --
   integer, parameter :: nl=236
   ! -- les variables --
   integer :: ib,is,rive,ltmp,is_local
   real(kind=long) :: hauteur,largeur_miroir,section_mouillee,perimetre_mouille,froude,dx,qdev
   real(kind=long) :: zdev,akmin,akmoy
   character :: debt*1,devt*1,ib_text*3
   character(nl),save :: ligne,ligne0 = repeat('*',nl)
   real(kind=long) :: qmin,qmoy,vmin,vmoy,debitance,beta
   character(len=19) :: hms
   integer :: values(8)
   character(len=8) :: ddate
   character(len=10) :: ttime
   character(len=5) :: zone

   call date_and_time(ddate,ttime,zone,values)
   date_fin = int((t-dt)/60._long)
   hms = heure(t-dt)

   open(newunit=ltmp,file=nomfin,status='unknown',form='formatted',encoding=encodage)
   write(Ltmp,'(a)') Ligne0
   write(ligne,'(3a,i4.4,5(a1,i2.2))') '* État final par Mage avec la méthode Debord à T = ',trim(hms), &
               ' Date simulation : ',values(1),'-',values(2),'-',values(3),' ',values(5),':',values(6),':',values(7)
   write(Ltmp,'(a)') Ligne
   Ligne=' ' ; Ligne(1:1)='*'
   write(Ltmp,'(a)') Ligne
   Ligne='* ATTENTION : en lecture, seuls les débits et cotes sont utilisés'
   write(Ltmp,'(a)') Ligne
   Ligne='* un # dans la colonne H (profondeur) indique un débordement en lit moyen'
   write(Ltmp,'(a)') Ligne
   Ligne='* un ! dans la colonne H (profondeur) indique un dépassement de la cote de berge la plus basse'
   write(Ltmp,'(a)') Ligne
   write(Ltmp,'(a)') Ligne0
#ifndef fake_mage7
   write(Ltmp,'(3a,i4.4,5(a1,i2.2))') '$ date finale = ',trim(hms), &
                           ' Date simulation : ',values(1),'-',values(2),'-',values(3),' ',values(5),':',values(6),':',values(7)
#endif /* fake_mage7 */
#ifdef fake_mage7
   write(Ltmp,'(a,f10.2)') '$ date en minutes : ',(t-dt)/60._long
#endif /* fake_mage7 */
   write(ltmp,1001)
   do ib = 1, la_topo%net%nb
      do is = la_topo%biefs(ib)%is1, la_topo%biefs(ib)%is2
         Hauteur = Z(is)-Zfd(is)
         if (Hauteur > la_topo%sections(is)%ymoy) then
            Debt = '#'  ! débordement en lit moyen
          else
            Debt = ' '
         endif
         if (Hauteur>la_topo%sections(is)%ybmin) then
            Devt = '!'  ! dépassement de la cote de berge la plus basse
          else
            Devt = ' '
         endif
         largeur_miroir    = la_topo%sections(is)%largeur(z(is))      ! largeur au miroir
         section_mouillee  = la_topo%sections(is)%section_mouillee(z(is))      ! surface mouillée
         perimetre_mouille = la_topo%sections(is)%perimetre(z(is))    ! périmètre mouillé
         Froude = abs(Q(is))*sqrt(Largeur_miroir/(gravite*section_mouillee**3))
         if (is < la_topo%biefs(ib)%is2) then
            Dx = abs(xgeo(is+1)-xgeo(is)) ! pas d'espace
            Qdev = Qds1(Total,is)*Dx      ! débit déversé
            Zdev = 100000._long
            do rive = rivg,rivd
               if (CDlat(rive,is)>0.0001_Long) then
                  Zdev = min(Zdev,Zfond(is)+Ydev(rive,is))  ! cote de déversement
               else
                  Zdev = min(Zdev,Zfond(is)+la_topo%sections(is)%ybmin)      ! cote de berge
               endif
            enddo
         else
            dx = zero
         endif
         if (ib < 100) then
            write(ib_text,'(i2,a)') ib,' '
         else
            write(ib_text,'(i3)') ib
         endif
         akmoy = la_topo%sections(is)%strickler_lit_moyen(z(is))
         akmin = la_topo%sections(is)%Ks(la_topo%sections(is)%main)
         call debord(is,hauteur,q(is),akmin,akmoy,qmin,qmoy,vmin,vmoy,debitance,beta)
         is_local = is-la_topo%biefs(ib)%is1+1
         if (q(is) >= 10000._long) then
            write(ltmp,1004) ib_text,is_local,q(is),z(is),xgeo(is),zfond(is),zfond(is)+la_topo%sections(is)%ymoy,  &
                             zdev, hauteur,debt,devt,largeur_miroir,froude,q(is)/section_mouillee,qe(is)*dx,       &
                             qdev,akmin,akmoy,section_mouillee,perimetre_mouille,qmin,qmoy,vmin,vmoy,debitance,beta
         elseif (q(is) > -1000._long) then
            write(ltmp,1002) ib_text,is_local,q(is),z(is),xgeo(is),zfond(is),zfond(is)+la_topo%sections(is)%ymoy,  &
                             zdev, hauteur,debt,devt,largeur_miroir,froude,q(is)/section_mouillee,qe(is)*dx,       &
                             qdev,akmin,akmoy,section_mouillee,perimetre_mouille,qmin,qmoy,vmin,vmoy,debitance,beta
         else
            write(ltmp,1003) ib_text,is_local,q(is),z(is),xgeo(is),zfond(is),zfond(is)+la_topo%sections(is)%ymoy,  &
                             zdev, hauteur,debt,devt,largeur_miroir,froude,q(is)/section_mouillee,qe(is)*dx,       &
                             qdev,akmin,akmoy,section_mouillee,perimetre_mouille,qmin,qmoy,vmin,vmoy,debitance,beta
         endif
         ! sauvegarde des résultats dans Mage_Results
         if (type_resultat == 'FIN' .or. type_resultat == 'END') then
            pm(is) = xgeo(is)
            z_fd(is) = zfd(is)
            largeur_Totale(is) = largeur_miroir
            largeur_mineur(is) = la_topo%sections(is)%almy
            Z_max(is) = Z(is)
            qtot(is) = q(is)
            qfp(is) = qmoy
         endif
      enddo
   enddo
   close(ltmp)
   1001 format('*',' IB',' IS',3X,'   Débit  ','     Cote   ','     Pm    ','  Z_Fond  ','  Z_moyen  ','  Z_Berge  ', &
             '   H   ','  Largeur  ','  Fr   ','   V   ','  Q_Lat  ',                                             &
             '   Qdev  ','  K_min  ','  K_maj  ','  Surface  ','  Périmètre  ','  Q_mineur  ','  Q_moyen  ',      &
             '  V_mineur  ','  V_moyen  ','  Débitance  ','  Boussinesq  ')
#ifndef fake_mage7
   1002 format(1x,a3,i4,2x,f10.5,1x,f11.6,1x,f9.2,f11.6,2x,f8.3,2x,f8.3,2x,    &
             f6.3,2a1,f9.2,1x,f6.3,1x,f6.3,f8.4,f10.4,2f9.3,f10.3,2x,  &
             f10.3,2x,f10.3,2x,2f10.3,2x,f10.3,2x,f12.3,5x,f6.3)
   1003 format(1x,a3,i4,2x,f10.3,1x,f11.6,1x,f9.2,f11.6,2x,f8.3,2x,f8.3,2x,    &
             f6.3,2a1,f9.2,1x,f6.3,1x,f6.3,f8.4,f10.4,2f9.3,f10.3,2x,  &
             f10.3,2x,f10.3,2x,2f10.3,2x,f10.3,2x,f12.3,5x,f6.3)
   1004 format(1x,a3,i4,2x,f10.4,1x,f11.6,1x,f9.2,f11.6,2x,f8.3,2x,f8.3,2x,    &
             f6.3,2a1,f9.2,1x,f6.3,1x,f6.3,f8.4,f10.4,2f9.3,f10.3,2x,  &
             f10.3,2x,f10.3,2x,2f10.3,2x,f10.3,2x,f12.3,5x,f6.3)
#endif /* fake_mage7 */
#ifdef fake_mage7
   1002 format(1x,a3,i3,2x,f10.5,1x,f11.6,1x,f9.2,f11.6,2x,f8.3,2x,f8.3,2x,    &
             f6.3,2a1,f9.2,1x,f6.3,1x,f6.3,f8.4,f10.4,2f9.3,f10.3,2x,  &
             f10.3,2x,f10.3,2x,2f10.3,2x,f10.3,2x,f12.3,5x,f6.3)
   1003 format(1x,a3,i3,2x,f10.3,1x,f11.6,1x,f9.2,f11.6,2x,f8.3,2x,f8.3,2x,    &
             f6.3,2a1,f9.2,1x,f6.3,1x,f6.3,f8.4,f10.4,2f9.3,f10.3,2x,  &
             f10.3,2x,f10.3,2x,2f10.3,2x,f10.3,2x,f12.3,5x,f6.3)
   1004 format(1x,a3,i3,2x,f10.4,1x,f11.6,1x,f9.2,f11.6,2x,f8.3,2x,f8.3,2x,    &
             f6.3,2a1,f9.2,1x,f6.3,1x,f6.3,f8.4,f10.4,2f9.3,f10.3,2x,  &
             f10.3,2x,f10.3,2x,2f10.3,2x,f10.3,2x,f12.3,5x,f6.3)
#endif /* fake_mage7 */
end subroutine Debord_IniFin


end module StVenant_Debord
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
