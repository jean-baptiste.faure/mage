!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - INRAE - 1989-2020                           #
!# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################
!######################### Double Balayage ####################################
!
!  Réalise la résolution du système linéaire pour les cas ramifiés et maillés
!  dans le cadre de la formulation Debord
!
!##############################################################################
!
subroutine BAL
!==============================================================================
!           sous-programme faisant le double balayage sur le modele
!                      complet ou le modele condense
!==============================================================================
   use Parametres, only: Long, zero
   use matrice_stv, only: a, b, c, d, e, f, ab, bb, cb, db, eb, fb
   use solution, only: qn, zn, dzn, sr, tr
   use TopoGeometrie, only: la_topo
   implicit none
   integer, pointer :: ismax, nomax

   ismax => la_topo%net%ns
   nomax => la_topo%net%nn
   !------------------------------------------------------------------------------
   !   Copie de A,B,C,D,E,et F et initialisation de SR, TR, DZN, QN, ZN
   ! --> BAL et ses sous-programmes ne travaillent que sur la copie des tableaux
   !     a,b,c,d,e,f
   !------------------------------------------------------------------------------
   ! NOTE: parallélisation possible ici, mais mauvaise idée car il faudrait ouvrir une section parallèle à chaque itération
   ab(1:ismax) = a(1:ismax)
   bb(1:ismax) = b(1:ismax)
   cb(1:ismax) = c(1:ismax)
   db(1:ismax) = d(1:ismax)
   eb(1:ismax) = e(1:ismax)
   fb(1:ismax) = f(1:ismax)

   dzn(1:nomax) = zero  ! dzn(n)  =  dernière cote calculée au nœud n
   sr(1:nomax)  = zero  ! coeff liés à l'aire du noeud
   tr(1:nomax)  = zero
   qn(1:nomax)  = zero  ! qn(n)  =  bilan des débits au nœud n
   zn(1:nomax)  = zero  ! zn(n)  =  cote moyenne au nœud n

   !Premier balayage sur les biefs amont de la maille sens amont--->aval
   call bal1

   !Prise en compte des c.l. aval - premier balayage sur les biefs aval de la maille
   if (la_topo%net%ibz > la_topo%net%nb) then  !il n'y a pas de bief aval de la maille
      call bal2a
   else                               !premier balayage aval--->amont (aprés la maille)
      call bal2b
   end if

   !Résolution de la maille
   if (la_topo%net%iba /= la_topo%net%nb) then  !le réseau est maillé
      call bal3
   end if

   !Premier balayage dans la maille - deuxième balayage sur tout le réseau
   call bal4
end subroutine bal
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine bal1
!==============================================================================
! calcule les r.s.t. (relations d'impédance) résultantes aux confluents lors du
!                   premier balayage amont ---> aval
!==============================================================================
   use parametres, only: long, zero, un
   use conditions_limites,only: rm=>rmv, tm=>tmv, cl
   use data_num_mobiles, only: t, area_factor
   use matrice_stv, only: sa=>ab, ra=>bb, ta=>cb
   use solution, only: dz, sr, tr, qna, zna
   use casiers, only: qclat
   use TopoGeometrie,only: la_topo, aire_noeud
   implicit none
   ! -- Variables --
   integer :: isa, nb, ib, ibb
   real(kind=long) :: area

   do ib = 1, la_topo%net%iba
      ibb = la_topo%net%numero(ib)
      isa = la_topo%biefs(ibb)%is1
      nb = la_topo%biefs(ibb)%nam
      area = aire_noeud(la_topo%nodes(nb),zna(nb) + dz(isa))
      if (area > zero) area = area*area_factor
      if (la_topo%nodes(nb)%cl > 0) then
         !--->ib est bief amont du modèle
         ra(isa) = rm(nb)
         sa(isa) = area
         ta(isa) = tm(nb)
      else
         ra(isa) = un
         sa(isa) = sr(nb) + area
         ta(isa) = tr(nb) + qna(nb) + cl(nb,t,zna(nb)+dz(isa)) + qclat(nb)
      end if
      call rst1a(ibb)
   end do
end subroutine bal1
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine bal2a
!==============================================================================
!                 prise en compte des conditions aval
!---> fait par bal2b si réseau maillé fermé (bief à l'aval de la maille)
!==============================================================================
   use conditions_limites, only: rmv, smv, tmv
   use matrice_stv, only: s=>db, r=>eb, t=>fb
   use TopoGeometrie,only: la_topo
   implicit none
   ! -- les variables --
   integer :: n, isz, ib

   do n = 1, la_topo%net%nn
      if (la_topo%nodes(n)%cl < 0) then
         ib = la_topo%net%numero(la_topo%net%lbamv(n))
         isz = la_topo%biefs(ib)%is2
         s(isz) = smv(n)
         r(isz) = rmv(n)
         t(isz) = tmv(n)
      endif
   enddo
end subroutine bal2a
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine bal2b
!==============================================================================
! calcule les r.s.t. (relations d'impédance) résultantes aux confluents lors du
!                   premier balayage aval--->amont
!                      (réseau maille fermé)
!==============================================================================
   use parametres, only: long, zero, un
   use conditions_limites, only: rv=>rmv, sv=>smv, tv=>tmv, cl
   use data_num_mobiles, only: t, area_factor
   use solution, only: dz, sr, tr, qna, zna
   use matrice_stv, only: db, eb, fb
   use casiers, only: qclat
   use TopoGeometrie, only: la_topo, aire_noeud
   implicit none
   ! -- variables --
   integer :: nb,isz,ib,ibb

   do ibb = la_topo%net%nb, la_topo%net%ibz, -1  !rang de calcul
      ib = la_topo%net%numero(ibb)
      nb = la_topo%biefs(ib)%nav
      isz = la_topo%biefs(ib)%is2
      if (la_topo%nodes(nb)%cl < 0) then
         !--->kb est bief aval du modele
         eb(isz) = rv(nb)
         db(isz) = sv(nb)
         fb(isz) = tv(nb)
      else
         eb(isz) = -un
         if (la_topo%nodes(nb)%np > 0) then  !nœud à surface non-nulle
            db(isz) = sr(nb)+area_factor*aire_noeud(la_topo%nodes(nb),zna(nb)+dz(isz))
         else
            db(isz) = sr(nb)
         endif
         fb(isz) = tr(nb)+qna(nb)+cl(nb,t)+qclat(nb)
      endif
      call rst1z(ib)
   enddo
end subroutine bal2b
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine bal3
!==============================================================================
!    résolution de la maille c'est à dire calcul de la répartition
!                    des débits aux diffluences
!==============================================================================
   use maille, only: arv, asv, atv
   use matrice_stv, only: as=>db, ar=>eb, at=>fb
   use TopoGeometrie, only: la_topo, is_bief_aval_maille
   implicit none
   ! -- les variables --
   integer :: i, k, n, num_i
   integer, pointer :: iba, ibz

   iba => la_topo%net%iba  !rang du dernier bief à l'amont de la maille ; = ibmax si pas de maille
                           !                                              si maille iba ≥ 1 (iba=0 impossible)
   ibz => la_topo%net%ibz  !rang du premier bief à l'aval de la maille ; = ibmax+1 si maille ouverte ou pas de maille

   !---arv asv atv (r',s',t' aux noeuds aval de maille)
   if (la_topo%net%ibz == la_topo%net%nb+1) then
   !---maille ouverte (noeuds aval de maille = noeuds aval de modèle)
      do i = la_topo%net%iby, la_topo%net%ibz-1
      !on ne teste que les biefs de rang compris entre le 1er et le dernier bief aval de maille
      !le dernier bief de maille (ibz-1) est toujours un bief aval de maille, mais il y en a au moins un autre
      !dont le rang est très voisin
         if (is_bief_aval_maille(i)) then  !le bief de rang i est bief aval de maille
            num_i = la_topo%net%numero(i)
            n = la_topo%biefs(num_i)%nav
            k = la_topo%biefs(num_i)%is2
            arv(n) = ar(k) ; asv(n) = as(k) ; atv(n) = at(k)
         endif
      enddo
   else
   !---maille fermée (il y a au moins 1 bief à l'aval de la maille) : r',s',t' remontés depuis l'aval
      num_i = la_topo%net%numero(ibz) ; n = la_topo%biefs(num_i)%nam ; k = la_topo%biefs(num_i)%is1
      arv(n) = ar(k) ; asv(n) = as(k) ; atv(n) = at(k)
   endif

   !   remplissage des blocs    k,l,c4,q,f,c2,n,c3
   call bal31
   !   calcul des variations de débit à l'amont des biefs de maille
   call bal32
end subroutine bal3
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine bal31
!==============================================================================
!     éléments pour remplir les blocs :     b  c1
!                                       n      c3
!     remplissage des blocs :
!              k   l   c4
!              q   f   c2
!==============================================================================
   use parametres, only: zero, un, nosup, nosup1, long
   use mage_utilitaires, only: zegal
   use maille, only: nr, ir, nx, xk, xl, &
                     xc4, xq, xf, xc2, sno, qno, arv, asv, atv
   use data_num_mobiles, only: t, area_factor
   use solution, only: qna, zna, dz, sr, tr
   use casiers, only: qclat
   use TopoGeometrie, only: la_topo, is_bief_aval_maille, aire_noeud
   use Conditions_Limites, only: cl
   implicit none
   ! -- variables --
   logical, allocatable, dimension(:), save :: boolk
   integer :: i, j, k, n
   real(kind=long) :: arvn
   integer, pointer :: nomax, iba, ibz, num(:)
   logical, save :: first_call = .true.

   nomax => la_topo%net%nn
   iba => la_topo%net%iba
   ibz => la_topo%net%ibz
   if (first_call) then
      allocate (boolk(nomax))
      first_call = .false.
   endif
   boolk(1:nomax) = .false.
   qno(1:nomax) = zero ; sno(1:nomax) = zero
   num => la_topo%net%numero

   !Initialisation de nr
   !On fait ce calcul ici au lieu de la faire à l'initialisation, ce qui sera compatible avec
   !une éventuelle possibilité de changer la topologie du réseau en cours de simulation
   nr = ibz-1-iba

   !---casier et apport aux noeuds de maille
   do i = iba+1, ibz-1
      n = la_topo%biefs(num(i))%nam ! n est noeud de maille
      if (.not.boolk(n) .and. la_topo%nodes(n)%cl>=0) then
         !     apport au noeud aval du bief (si ce n'est pas un noeud avec c.l. aval)
         !     et bilan de débit
         qno(n) = cl(n,t,zna(n)+dz(la_topo%biefs(num(i))%is1)) + qna(n) + qclat(n)
         !     (sno) <--- surface_casier/dt
         !     (sno) sert à remplir (b) si n noeud non aval de maille, sinon (n)
         sno(n) = area_factor*aire_noeud(la_topo%nodes(n),zna(n)+dz(la_topo%biefs(num(i))%is1))
         boolk(n) = .true. !le noeud n est initialisé
      endif
      n = la_topo%biefs(num(i))%nav ! n est noeud de maille
      if (.not.boolk(n) .and. la_topo%nodes(n)%cl>=0) then
         !     apport au noeud aval du bief (si ce n'est pas un noeud avec c.l. aval)
         !     et bilan de débit
         qno(n) = cl(n,t,zna(n)+dz(la_topo%biefs(num(i))%is2)) + qna(n) + qclat(n)
         !     (sno) <--- surface_casier/dt
         !     (sno) sert à remplir (b) si n noeud non aval de maille, sinon (n)
         sno(n) = area_factor*aire_noeud(la_topo%nodes(n),zna(n)+dz(la_topo%biefs(num(i))%is2))
         boolk(n) = .true. !le noeud n est initialisé
      endif
   enddo

   !---biefs amont de maille arrivant sur la maille
   do i = 1, iba
      n = la_topo%biefs(num(i))%nav
      if (boolk(n)) then !n est noeud de maille
         !     (qno) <--- q_bief_amont + t_bief_amont/r_bief_amont
         !     (qno) sert à remplir (c1) si n noeud non aval de maille,sinon (c3)
         qno(n) = qno(n)+tr(n)
         !     (sno) <--- s_bief_amont/r_bief_amont
         sno(n) = sno(n)+sr(n)
         boolk(n) = .false.
      endif
   enddo

   !---condensation des biefs de maille
   do i = iba+1, ibz-1
      boolk(la_topo%biefs(num(i))%nam) = .true.  ;  boolk(la_topo%biefs(num(i))%nav) = .true.
      !     condensation du bief avec cadrage
      call bal310(i,xq(i),xf(i),xc2(i),xk(i),xl(i),xc4(i))
   enddo

   !---biefs aval de maille
   nx(1:nomax,1:nomax+1) = 0
   do i = iba+1, ibz-1
      if (is_bief_aval_maille(i)) then
         n = la_topo%biefs(num(i))%nav
         if (.not.zegal(arv(n),zero,un)) then !loi q(z) ou q(t) au noeud aval, ou bief a l'aval de la maille
            do j = 1, nr
               if (i==ir(j)) then !j colonne correspondant au bief i
                  !     (nx) <--- éléments pour remplir (n)
                  !     (nx) doit être défini à ce niveau car la loi aval peut changer au cours
                  !          du temps (exemple des portes a la mer dq=f(dz) <---> dq=cte)
                  nx(n,1) = nx(n,1)+1 !nx(n,1) nombre de biefs aval de maille reliés au noeud n
                  !     (nx(n,k),k=2,nx(n,1)+1) liste des colonnes correspondant aux biefs
                  k = nx(n,1)+1  ;  nx(n,k) = j
                  exit
               endif
            enddo
            qno(n) = qno(n)+xc4(i) ! (qno) <--- xc4
            sno(n) = sno(n)+xl(i)  ! (sno) <--- xl
         endif
      endif
   enddo

   do i = iba+1, ibz-1
      if (is_bief_aval_maille(i)) then
         n = la_topo%biefs(num(i))%nav
         if (boolk(n)) then
            if (nx(n,1) == 0) then !loi z(t) au noeud aval (casier et debit de fuite inoperants)
               qno(n) = -atv(n)
            else                   !loi q(z) ou q(t) au noeud aval , ou bief a l'aval de la maille
               arvn = 1._long/arv(n)
               sno(n) = sno(n)-asv(n)*arvn
               qno(n) = (qno(n)-atv(n)*arvn)/sno(n)
            endif
            boolk(n) = .false.
         endif
      endif
   enddo

end subroutine bal31
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
   subroutine bal310(i,xqi,xfi,xc2i,xki,xli,xc4i)
!==============================================================================
!                         condensation du bief de rang i
!==============================================================================
   use parametres, only: long, zero, un
   use matrice_stv, only: a=>ab, b=>bb, c=>cb, d=>db, e=>eb, f=>fb
   use TopoGeometrie, only: la_topo
   implicit none
   ! -- prototype --
   integer,intent(in) :: i !bief de rang i
   real(kind=long),intent(out) :: xqi, xfi, xc2i, xki, xli, xc4i
   ! -- les variables --
   integer :: j,j1
   real(kind=long) :: a1j, a2j, b1j, x, a1j1, a2j1, b1j1, x1
   real(kind=long) :: det1, ap, bp, cp, dp, ep, fp
   real(kind=long),parameter :: eps=(10._long**long)*epsilon(1._long)
   integer, pointer :: num(:)

   num => la_topo%net%numero

   !   condensation amont--->aval
   a1j = un  ;  a2j = zero  ;  b1j = zero  ;  x = un
   do j1 = la_topo%biefs(num(i))%is1+1, la_topo%biefs(num(i))%is2
      j = j1-1
      a1j1 = -(a1j*b(j1)+a2j*e(j))
      a2j1 = -(a1j*a(j1)+a2j*d(j))
      b1j1 = b1j-a1j*c(j1)-a2j*f(j)

      x1 = un/max(abs(a1j1),abs(a2j1)) !x1 facteur de cadrage partiel
      a1j = a1j1*x1  ;  a2j = a2j1*x1  ;  b1j = b1j1*x1
      x  = x*x1                        !x facteur de cadrage total
      if (x < eps) x = zero    ! mise à zéro pour éviter un underflow
   enddo
   !      équation de condensation  2
   x1  = un/a1j
   xki = -x*x1  ;  xli = a2j*x1  ;  xc4i = b1j*x1

   !   condensation aval--->amont
   a1j1 = zero  ;  a2j1 = un  ;  b1j1 = zero  ;  x = un
   do j = la_topo%biefs(num(i))%is2-1, la_topo%biefs(num(i))%is1, -1
      j1 = j+1
      det1 = un/(b(j1)*d(j)-a(j1)*e(j))
      ap = -a(j1)*det1       ;  bp = d(j)*det1
      cp = ap*f(j)+bp*c(j1)  ;  dp = b(j1)*det1
      ep = -e(j)*det1        ;  fp = dp*f(j)+ep*c(j1)
      a1j = -(a1j1*bp+a2j1*ep)
      a2j = -(a1j1*ap+a2j1*dp)
      b1j = b1j1-a1j1*cp-a2j1*fp

      x1 = un/max(abs(a1j),abs(a2j)) !x1 facteur de cadrage partiel
      a1j1 = a1j*x1  ;  a2j1 = a2j*x1  ;  b1j1 = b1j*x1
      x  = x*x1                      !x facteur de cadrage total
      if ( x < eps ) x = zero   ! mise à zéro pour éviter un underflow
   enddo
   x1  = un/a2j1
   xqi = a1j1*x1  ;  xfi = -x/x1  ;  xc2i = b1j1*x1

end subroutine bal310
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine bal32
!------------------------------------------------------------------------------
!     calcul des variations de débit à l'amont des biefs de maille
!------------------------------------------------------------------------------
   use parametres, only: long, zero, un
   use maille, only: nr, na, nb, nh, ir, ia, ja, ib, jb, ih, jh, &
                     bool, nx, xk, xl, xc4, xq, xf, xc2, sno,qno
   use hydraulique, only: zt
   use solution, only: dq
   use mage_utilitaires, only: LU_solution, LU_factorisation
   use TopoGeometrie, only: la_topo
   implicit none
   ! -- variables --
   integer :: i, j, k, l, n, nzhjj
! DONE: mettre les tableaux en allocation dynamique ; faire l'allocation une seule fois au 1er appel
   integer, allocatable, dimension(:), save :: indx, nzhj
   logical, allocatable, dimension(:), save  :: boolk
   real(kind=long),allocatable, dimension(:), save :: c1, c3
   integer, allocatable, dimension(:,:), save :: nzh
   real(kind=long), allocatable, dimension(:,:), save :: a, a1, e, h, u

   real(kind=long) :: eik, c1i, snon1, uik, v
   integer :: id0
   integer, pointer :: num(:)
   integer, pointer :: ibmax

   logical, save :: first_call = .true.

   ibmax => la_topo%net%nb
   num => la_topo%net%numero

   if (first_call) then
      allocate (indx(ibmax),nzhj(ibmax),boolk(ibmax+1),c1(ibmax),c3(ibmax))
      allocate (nzh(ibmax,ibmax),a(ibmax,ibmax),a1(ibmax,ibmax),e(ibmax,ibmax),h(ibmax,ibmax),u(ibmax,ibmax))
      first_call = .false.
   endif

   !==============================================================================
   !       1)   construction du système réduit en dq1
   !==============================================================================
   !-----    découpage en blocs du système original d'équations :
   !-----
   !-----    :  m a b    :   : dq1    : c1
   !-----    :  k i   l  : x : dqn  = : c4
   !-----    :  q   i f  :   : dz1    : c2
   !-----    :  n   h i  :   : dzn    : c3
   !-----
   !-----       k,l,q,f  diagonales  , i  identité
   !-----       h  triangulaire superieure avec diagonale nulle
   !-----
   !-----       m a     c1    continuité des débits aux nœuds non aval de maille
   !-----           b   c1    égalité des cotes amont-amont aux diffluences
   !-----       k i   l c4    condensation 2
   !-----       q   i f c2    condensation 1
   !-----       n   h i c3    lois aval aux biefs aval de maille ou égalité des
   !-----                     cotes amont-aval aux nœuds non aval de maille
   !-----
   !------------------------------------------------------------------------------
   !----- système réduit en dq1 :
   !    (m-ak-bq+(al+bf)(hf-i)^-1(hq-n))dq1=(c1-ac4-bc2+(al+bf)(hf-i)^-1(hc2-c3)
   !------------------------------------------------------------------------------


   !---tableau <---      matrice
   !      (a)  <--- ( (h)*(f) - (i) )
   !      (h)  <--- ( (h)*(q) - (n) )
   !      (c3) <--- ( (h)*(c2) - (c3) )
   do j = 1, nr
      c3(j) = zero   !remise à 0 de (c3)
      do i = 1, nr      !remise à 0 de (a) et (h)
         a(i,j) = zero
         h(i,j) = zero
         nzh(i,j) = 0
      enddo
   enddo

   do l = 1, nh
      i = ih(l) ; j = jh(l) ; n = ir(j)
      a(i,j) = -xf(n)  ! (a) <---  (h)*(f)
      h(i,j) = -xq(n)  ! (h) <---  (h)*(q)
      nzh(i,j) = 1
      ! (c3) <--- (h)*(c2)
      ! (c3) <--- (-c3)     (cote amont du bief de rang ir(j))
      c3(i) = c3(i)-xc2(n)-zt(la_topo%biefs(num(n))%is1)
   enddo

   do j = 1, nr
      a(j,j) = -un  !(a) <--- (-I)
      i = ir(j)  ;  n = la_topo%biefs(num(i))%nav
      if (bool(i)) then    !--->le bief de rang i est bief aval de maille

      ! la ligne j de (n) est nulle si loi z(t) au nœud n aval du bief de rang i
         if (nx(n,1) /= 0) then
         ! loi q(z) ou q(t) au noeud n aval du bief de rang i
         ! somme(k.dq1)/sno(n)+dzn=qno(n)/sno(n)
            snon1 = -xk(i)/sno(n)
            do k = 2, nx(n,1)+1
               l = nx(n,k)
               h(j,l) = snon1 ! (h) <--- (-n)
               nzh(j,l) = 1
            enddo
         endif
         c3(j) = -qno(n) ! (c3) <--- (-c3)     ( (qno) =(qno)/(sno) )

      else                 !--->le bief de rang i n'est pas aval de maille

         c3(j) = c3(j)+zt(la_topo%biefs(num(i))%is2) ! (c3) <--- (-c3)    (cote aval du bief de rang i)

      endif
   enddo


   do j = 1, nr  ! repérage des colonnes non nulles de (h)
      nzhjj = 0
      do i = 1, nr
         nzhjj = nzhjj+nzh(i,j)
      enddo
      nzhj(j) = nzhjj
   enddo

   !     inversion de ( (h)*(f)-(i) ) : (a1) <--- ( (h)*(f)-(i) )**-1
   a1(1:nr,1:nr) = zero
   do i = 1, nr
      a1(i,i) = un
      indx(i) = i
   enddo
   !nr0 = -nr
   do j = 1, nr
      call LU_solution(a,nr,ibmax,indx,a1(1,j),1)
   enddo

   !     (a)  <--- ( (a)*(l) + (b)*(f) )
   !     (c1) <--- ( (c1) - (a)*(c4) - (b)*(c2) )
   !     (u)  <--- ( (m)  - (a)*(k)  - (b)*(q)  )

   !     remise à 0 de (c1), de (a) et (u), à vrai de (boolk)
   c1(1:nr) = zero ; a(1:nr,1:nr) = zero ; u(1:nr,1:nr) = zero
   boolk(1:la_topo%net%nn) = .true.

   do l = 1, na  !continuite des débits aux noeuds  (a est transposée)
      i=ia(l) ; j=ja(l)
      if (j < 0) then
         j = -j
         n = ir(j)
         a(j,i) = -xl(n)      ! (a) <--- (a)*(l)
         u(i,j) = xk(n)       ! (u) <--- (-a)*(k)
         c1(i) = c1(i)+xc4(n) ! (c1) <--- (-a)*(c4)
      else
         !(u) <--- (m)
         u(i,j) = un
         n = ir(j)
         k = la_topo%biefs(num(n))%nam
         if (boolk(k)) then
            !(b)=casier et apport=(sno)
            v = sno(k)
            a(j,i) = v*xf(n)        ! (a) <--- (b)*(f)
            u(i,j) = u(i,j)-v*xq(n) ! (u) <--- (-b)*(q)
            !(c1) <--- (-b)*(c2)
            !(c1) <--- (c1)      (biefs amont de maille et apport au noeud)
            c1(i) = c1(i)-v*xc2(n)+qno(k)

            boolk(k) = .false.  !le noeud k a été traité
         endif
      endif
   enddo

   k=0 ; do l=1,nb  !égalité des cotes amont (a est transposée)
      i = ib(l) ; j = jb(l)
      v = un ; if (k==i) v = -v
      n = ir(j)
      a(j,i) = -v*xf(n) ! (a) <--- (b)*(f)
      u(i,j) = v*xq(n)  ! (u) <--- (-b)*(q)
      !(c1) <--- (-b)*(c2)
      !(c1) <--- cote amont du bief de rang ir(j)
      c1(i) = c1(i)+v*(xc2(n)+zt(la_topo%biefs(num(n))%is1))
      k = i
   enddo

   do i = 1, nr
      ! (e) <--- ( (a)*(l) + (b)*(f) ) * ( (h)*(f)-(i) )**-1 par lignes
      do k = 1, nr
         eik = zero
         do j = 1, k
            eik = eik+a(j,i)*a1(j,k)
         enddo
         e(k,i) = eik
      enddo

      !      (u)  <--- (u) + (e)*(h)
      !      (u)  = ( (m)  - (a)*(k)  - (b)*(q)  )
      !      (e)  = ( (a)*(l) + (b)*(f) ) * ( (h)*(f)-(i) )**-1
      !      (h)  = ( (h)*(q) - (n) )
      do k = 1, nr
         if (nzhj(k) /= 0) then !il y a des termes non nuls sur la colonne k de (h)
            uik = u(i,k)
            do j = 1, k-1
               if (nzh(j,k) /= 0) uik = uik+e(j,i)*h(j,k)
            enddo
            u(i,k) = uik
         endif
      enddo

      !      (c1)  <--- (c1) + (e)*(c3)
      !      (c1) = ( (c1) - (a)*(c4) - (b)*(c2) )
      !      (c3) = ( (h)*(c2) - (c3) )
      c1i = c1(i)
      do j = 1, nr
         c1i = c1i+e(j,i)*c3(j)
      enddo
      c1(i)=c1i
   enddo

   !=======================================================================
   !       2)    résolution du système linéaire (u)*(dq1)=(c1)
   !=======================================================================
   ! 1-) décomposition LU
   call LU_factorisation(u,nr,ibmax,indx,id0)
   if (id0 /= 0) call err024

   ! 2-) résolution du système, la solution est dans c1
   call LU_solution(u,nr,ibmax,indx,c1)

   do l = 1, nr
      !-----(c1) débits rangés dans l'ordre des colonnes des matrices
      !-----(dq)  débits aux sections amont des biefs de maille
      dq(la_topo%biefs(num(ir(l)))%is1) = c1(l)
   enddo

end subroutine bal32
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine bal4
!==============================================================================
!    sous-programme faisant les calculs (à chaque pas de temps)
!    postérieurs au calcul de la maille :
!       1/ premier balayage dans la maille
!       2/ deuxième balayage dans tout le modèle
!==============================================================================
   use TopoGeometrie, only: la_topo
   implicit none

   !   si réseau maillé premier balayage dans la maille
   if (la_topo%net%iba < la_topo%net%nb) call bal41

   !   réseau maillé-fermé : deuxième balayage amont-->aval
   !   (une seule condition aval dans ce cas)
   if (la_topo%net%iba<la_topo%net%nb .and. la_topo%net%ibz<=la_topo%net%nb) call bal42

   !   deuxième balayage aval-->amont à partir du noeud aval de maille
   !   ou du dernier noeud aval du modèle (ibz)
   call bal43

   !---calcul des cotes moyennes aux noeuds
   call bal44

   !---forçage des lois d'ouvrage
   call force_ouvrages

end subroutine bal4
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine bal41
!==============================================================================
!
!                    premier balayage dans la maille
!
!==============================================================================
   use parametres, only: long, zero, un

   use mage_utilitaires, only: zegal
   use hydraulique, only: qt
   use data_num_mobiles, only: tt=>t, area_factor
   use solution, only: dq, dz, sr, tr, qna, zna
   use matrice_stv, only: s=>ab, r=>bb, t=>cb
   use casiers, only: qclat
   use TopoGeometrie, only: la_topo, aire_noeud
   use Conditions_Limites, only: cl
   implicit none
   ! -- les variables --
   integer :: isa, islb, nb, lb, lbb, kb, kbb
   real(kind=long) :: sqn, sqnp1, repq

   do kbb = la_topo%net%iba+1, la_topo%net%ibz-1 !boucle sur les biefs de maille
      kb = la_topo%net%numero(kbb)
      isa = la_topo%biefs(kb)%is1
      nb = la_topo%biefs(kb)%nam
      if (la_topo%net%lbz(nb,2) /= la_topo%net%lbz(nb,1)) then
         !on est à l'aval d'une diffluence : on utilise le résultat du calcul de maille
         sqnp1 = zero
         sqn = qna(nb)
         do lbb = la_topo%net%lbz(nb,1),la_topo%net%lbz(nb,2)
            lb = la_topo%net%numero(lbb)
            islb = la_topo%biefs(lb)%is1
            sqnp1 = sqnp1+qt(islb)+dq(islb)
            sqn = sqn+qt(islb)
         enddo
         if (zegal(sqnp1,zero,un)) then
            s(isa) = zero
            t(isa) = dq(isa)
         else
            repq = (qt(isa)+dq(isa))/sqnp1
            if (la_topo%nodes(nb)%np > 0) then
               s(isa) = repq*(sr(nb)+area_factor*aire_noeud(la_topo%nodes(nb),zna(nb)+dz(isa)))
            else
               s(isa) = repq*sr(nb)
            endif
            t(isa) = repq*(tr(nb)+sqn+cl(nb,tt)+qclat(nb))-qt(isa)
         endif
      else
         !on est à l'aval d'un confluent : composition des relations d'impédance
         if (la_topo%nodes(nb)%np > 0) then
            s(isa) = sr(nb) + area_factor*aire_noeud(la_topo%nodes(nb),zna(nb)+dz(isa))
         else
            s(isa) = sr(nb)
         endif
         t(isa) = tr(nb)+qna(nb)+cl(nb,tt)+qclat(nb)
      endif
      r(isa) = un

      call rst1a(kb)   !premier balayage dans la maille
   enddo
end subroutine bal41
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine bal42
!==============================================================================
!                  deuxième balayage amont-->aval
!                       (réseau maillé fermé)
!==============================================================================
   use parametres, only: long, zero, un
   use data_num_mobiles, only: tt=>t, area_factor
   use solution, only: dz, dzn, sr, tr, qna, zna
   use matrice_stv, only: s=>ab, r=>bb, t=>cb
   use TopoGeometrie, only: la_topo, aire_noeud
   use Conditions_Limites, only: cl
   implicit none
   ! -- variables --
   integer :: isa, nb, kb, kbb

   do kbb = la_topo%net%ibz, la_topo%net%nb
      kb = la_topo%net%numero(kbb)
      nb = la_topo%biefs(kb)%nam
      isa = la_topo%biefs(kb)%is1
      if (kb > la_topo%net%ibz) then
         !cas des biefs suivants : on connait la cote au noeud amont du bief kb
         r(isa) = zero
         s(isa) = un
         t(isa) = dzn(nb)
      else
         !cas du premier bief aval de la maille : combinaison de la r.s.t.
         !résultante à la sortie de la maille et de celle qui vient de l'aval
         r(isa) = un
         if (la_topo%nodes(nb)%np > 0) then
            s(isa) = sr(nb) + area_factor*aire_noeud(la_topo%nodes(nb),zna(nb)+dz(isa))
         else
            s(isa) = sr(nb)
         endif
         t(isa) = tr(nb)+qna(nb)+cl(nb,tt)
      endif
      call rst2z(kb)
   enddo
end subroutine bal42
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine bal43
!==============================================================================
!                deuxième balayage aval-->amont
!==============================================================================
   use parametres, only: long, zero, un
   use hydraulique, only: zt
   use solution, only: dz
   use matrice_stv, only: s=>db, r=>eb, t=>fb
   use TopoGeometrie, only: la_topo
   implicit none
   ! -- les variables --
   integer :: isz, lb, lbz1, lbz2, nb, ib, ibb, lbb
   real(kind=long) :: somz

   !   ibz-1 est le rang du dernier bief de maille si le réseau est maillé-fermé
   !   ibz-1=ibmax si le réseau est ramifié
   do ibb = la_topo%net%ibz-1, 1, -1
      ib = la_topo%net%numero(ibb)
      nb = la_topo%biefs(ib)%nav
      if (la_topo%nodes(nb)%cl >= 0) then
         !le bief ib se termine par un confluent ou une diffluence--->condition aval du bief
         somz = zero
         lbz1 = la_topo%net%lbz(nb,1)
         lbz2 = la_topo%net%lbz(nb,2)
         do lbb = lbz1,lbz2
            lb = la_topo%net%numero(lbb)
            isz = la_topo%biefs(lb)%is1
            somz = somz+zt(isz)+dz(isz)
         enddo
         isz = la_topo%biefs(ib)%is2
         r(isz) = zero
         s(isz) = un
         somz = somz/real(lbz2-lbz1+1,kind=long)
         t(isz) = somz-zt(isz)
      endif
      call rst2b(ib) !résolution
   enddo
end subroutine bal43
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine bal44
!==============================================================================
!            calcul des cotes moyennes aux noeuds
!==============================================================================
   use parametres, only: long, zero
   use hydraulique, only: zt
   use solution, only: dz, dzn, zn
   use data_num_fixes, only: fma
   use data_num_mobiles, only: dt
   use casiers, only: qclat
   use TopoGeometrie, only: la_topo, aire_noeud
   implicit none
   ! -- variables --
   integer :: n, ib, is
   real(kind=long) :: ddz
   integer, pointer :: nomax, ismax

   nomax => la_topo%net%nn
   ismax => la_topo%net%ns

   !   calcul de la somme de dz
   dzn(1:nomax) = zero
   do ib = 1, la_topo%net%nb
      n = la_topo%biefs(ib)%nam ; dzn(n) = dzn(n)+dz(la_topo%biefs(ib)%is1)
      n = la_topo%biefs(ib)%nav ; dzn(n) = dzn(n)+dz(la_topo%biefs(ib)%is2)
   enddo

   !   calcul des moyennes
   !   la somme zn est calculée dans rst2z et rst2b
   zn(1:nomax) = zn(1:nomax)/real(la_topo%net%nzno(1:nomax),long)
   dzn(1:nomax) = dzn(1:nomax)/real(la_topo%net%nzno(1:nomax),long)

   !   correction pour les casiers avec prélèvement : on abaisse le niveau
   !   pour tenir compte du débit prélevé par déversement latéral inversé
   !   (retour d'un déversement de bief vers casier)
   do n = 1, nomax
      if (qclat(n) < zero) then
         ddz = -qclat(n) * dt/aire_noeud(la_topo%nodes(n),zn(n))
         zn(n) = zn(n) - ddz
         dzn(n) = dzn(n) - ddz
      endif
   enddo

   if (fma < zero) then !on impose l'égalité des cotes aux noeuds
      do ib = 1, la_topo%net%nb
         is = la_topo%biefs(ib)%is1 ; dz(is) = zn(la_topo%biefs(ib)%nam)-zt(is)
         is = la_topo%biefs(ib)%is2 ; dz(is) = zn(la_topo%biefs(ib)%nav)-zt(is)
      enddo
   endif

end subroutine bal44
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine force_ouvrages
!==============================================================================
!        on force la vérification des lois d'ouvrage
!==============================================================================
   use parametres, only: long
   use Ouvrages, only: all_OuvCmp, all_OuvEle, debit_total
   use hydraulique, only: zt, qt
   use solution, only: dz, dq
   use TopoGeometrie, only: la_topo
   implicit none
   ! -- variables --
   integer :: ns, js, is, type_first_element
   real(kind=long) :: qtt, z1, z2

   do ns = 1, la_topo%net%nss
      type_first_element = all_OuvEle(all_OuvCmp(ns)%OuEl(1,1))%iuv
      if (type_first_element == 99) cycle !ouvrage non défini
      if (type_first_element == 3) cycle  !pompe
      if (type_first_element == 5) cycle  !déversoir latéral
      js = all_OuvCmp(ns)%js ; is = js-1
      z1 = zt(is)+dz(is)
      z2 = zt(js)+dz(js)   ! niveaux amont et aval (routine débit() )
      qtt = debit_total(ns,z1,z2)
      if (abs(qtt) < 1.e-05_long) then
         !si le débit théorique est petit alors le débit simulé est forcé à la
         !valeur du débit théorique
         dq(is) = qtt-qt(is)
         dq(js) = dq(is)
      endif
   enddo
end subroutine force_ouvrages
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine rst1a(ib)
!==============================================================================
!    premier balayage sens amont ---> aval dans le bief numéro ib
!    (pour les biefs de maille et ceux situés à l'amont de la maille)
!
!     ( les r.s.t. obtenues se trouvent dans b,a et c)
!==============================================================================
   use parametres, only: long, un
   use matrice_stv, only: s=>ab, r=>bb, t=>cb, d=>db, e=>eb, f=>fb
   use solution, only: sr, tr
   use TopoGeometrie, only: la_topo
   implicit none
   ! -- paramètres --
   integer, intent(in) :: ib
   ! -- variables --
   integer :: isi, isj, isz, nb
   real(kind=long) :: c0, sj, rj, tj, risz

   !nb : la première r.s.t. du bief est fournie soit par les c.l.
   !amont soit par la combinaison des r.s.t. aval au confluent amont
   do isj = la_topo%biefs(ib)%is1+1, la_topo%biefs(ib)%is2
      isi = isj-1
      sj = -(r(isi)*s(isj)+s(isi)*d(isi))
      rj = -(r(isi)*r(isj)+s(isi)*e(isi))
      tj = t(isi)-r(isi)*t(isj)-s(isi)*f(isi)
      ! normalisation
      c0 = un / max(abs(sj),abs(rj))
      s(isj) = sj*c0
      r(isj) = rj*c0
      t(isj) = tj*c0
   enddo
   nb = la_topo%biefs(ib)%nav
   isz = la_topo%biefs(ib)%is2
   risz = un/r(isz)
   sr(nb) = sr(nb)+s(isz)*risz
   tr(nb) = tr(nb)+t(isz)*risz
end subroutine rst1a
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine rst1z(ib)
!==============================================================================
!    premier balayage sens aval ---> amont dans le bief numéro ib
!    (pour les biefs situés à l'aval de la maille)
!
!--->revoir l'utilisation des c.l. aval dans ce cas
!
!      (les r.s.t. obtenues se trouvent dans e, d et f)
!==============================================================================
   use parametres, only: long, un
   use matrice_stv, only: a=>ab, b=>bb, c=>cb, s=>db, r=>eb, t=>fb
   use solution, only: sr, tr
   use TopoGeometrie, only: la_topo
   implicit none
   ! -- paramètres --
   integer, intent(in) :: ib
   ! -- variables --
   integer :: isa, isi, isj, nb
   real(kind=long) :: c0, ti, si, ri, risa

   do isi = la_topo%biefs(ib)%is2-1, la_topo%biefs(ib)%is1, -1
      !la derniere r.s.t. est fournie par les c.l. aval
      isj = isi+1
      si = -(r(isj)*a(isj)+s(isj)*s(isi))
      ri = -(r(isj)*b(isj)+s(isj)*r(isi))
      ti = t(isj)-r(isj)*c(isj)-s(isj)*t(isi)
      !normalisation
      c0 = un / max(abs(ri),abs(si))
      s(isi) = si*c0
      r(isi) = ri*c0
      t(isi) = ti*c0
   enddo
   if (la_topo%net%rang(ib) == la_topo%net%ibz) return  !ib est le 1er bief à l'aval de la maille
   nb = la_topo%biefs(ib)%nam
   isa = la_topo%biefs(ib)%is1
   risa = un/r(isa)
   sr(nb) = sr(nb)-s(isa)*risa
   tr(nb) = tr(nb)-t(isa)*risa
end subroutine rst1z
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine rst2b(ib)
!==============================================================================
!    deuxième balayage ; sens aval ---> amont dans le bief ib
!    cas des biefs dont la c.l. aval est linéaire
!    (pour les biefs de maille et ceux situés à l'amont de la maille)
!
!       on récupère la solution dans dz, dq et dzn
!==============================================================================
   use parametres, only: long, zero, un
   use mage_utilitaires, only : zegal
   use hydraulique, only: qt, zt
   use solution, only: dq, dz, dzn, qn, zn
   use matrice_stv, only: s=>ab, r=>bb, t=>cb, d=>db, e=>eb, f=>fb
   use TopoGeometrie, only: la_topo
   implicit none
   ! -- paramètres --
   integer, intent(in) :: ib
   ! -- les variables --
   integer :: isa, isi, isj, n, isz
   real(kind=long) :: dqj, dzj
   real(kind=long) :: sz, rz, tz, sv, rv, tv

   isa = la_topo%biefs(ib)%is1  ;  isz = la_topo%biefs(ib)%is2

   !résolution des 2 dernières équations
   !      r.s.t. transmise à l'aval du bief ib par le premier balayage
   !           ---> avant-dernière équation du bief
   sz = s(isz)  ;  rz = r(isz)  ;  tz = t(isz)
   !      r.s.t. transmise par l'aval du bief ib (c.l. et diffluence)
   !           ---> dernière équation du bief lue dans e, d et f
   sv = d(isz)  ;  rv = e(isz)  ;  tv = f(isz)

   if (zegal(rv,zero,un) .and. zegal(sv,un,un)) then
      !le bief a une c.l. aval en cote
      dzj = tv
      dqj = (tz-sz*dzj)/rz
   else
      !le bief est bief aval avec c.l. aval en q(z)
      dzj = (rz*tv-rv*tz)/(rz*sv-rv*sz)
      dqj = (tv-sv*dzj)/rv
   endif
   dz(isz) = dzj  ;  dq(isz) = dqj

   !enregistrement des résultats au noeud aval
   n = la_topo%biefs(ib)%nav
   dzn(n) = dzj
   zn(n) = zn(n)+zt(isz)+dzj
   qn(n) = qn(n)+qt(isz)+dqj

   !remontée
   do isi = isz-1, isa, -1
      isj = isi+1
      dzj = f(isi)-(d(isi)*dzj+e(isi)*dqj)
      dqj = (t(isi)-s(isi)*dzj)/r(isi)
      dq(isi) = dqj  ;  dz(isi) = dzj
   enddo

   !enregistrement des résultats au noeud amont
   n = la_topo%biefs(ib)%nam
   dzn(n) = dzj
   zn(n) = zn(n)+(zt(isa)+dzj)
   qn(n) = qn(n)-(qt(isa)+dqj)

end subroutine rst2b
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine rst2z(ib)
!==============================================================================
!    deuxieme balayage ; sens amont ---> aval dans le bief ib
!    pour les biefs situés à l'aval de la maille
!
!       on récupère la solution directement dans dq, dz et dzn
!==============================================================================
   use parametres, only: long, zero, un
   use mage_utilitaires, only: zegal, LU_factorisation, LU_solution
   use hydraulique, only: qt,zt
   use solution, only: dq,dz,dzn,qn,zn
   use matrice_stv, only: a=>ab,b=>bb,c=>cb,s=>db,r=>eb,t=>fb
   use TopoGeometrie, only: la_topo
   implicit none
   ! -- paramètres --
   integer, intent(in) :: ib
   ! -- constantes --
   integer :: ide=2
   ! -- variables --
   integer :: id, indx(2), isi, isj, isz, n, isa
   real(kind=long) :: dzi, dqi, dzj, dqj
   real(kind=long), target :: am(2,2), bm(2)
   real(kind=long), pointer :: rm, sm, tm, ra, sa, ta

   rm => am(1,1) ; sm => am(1,2) ; tm => bm(1)
   ra => am(2,1) ; sa => am(2,2) ; ta => bm(2)

   dzj = huge(zero) ; dqj = huge(zero)

   isa = la_topo%biefs(ib)%is1  ;  isz = la_topo%biefs(ib)%is2

   !                résolution des 2 équations amont du bief

   !r.s.t. transmise par l'amont du bief ib (confluent de sortie de la maille)
   !---> première équation du bief (lue dans b,a,c)
   rm = b(isa)  ;  sm = a(isa)  ;  tm = c(isa)
   !r.s.t. transmise à l'amont du bief ib par le premier balayage
   !---> deuxième équation du bief
   ra = r(isa)  ;  sa = s(isa)  ;  ta = t(isa)
   if (zegal(rm,zero,un) .and. zegal(sm,un,un)) then
   !le bief a une c.l. amont en cote
      dzi = tm
      dqi = (ta-sa*dzi)/ra
   else
   !le bief a une c.l. amont en q(z)
      call LU_factorisation(am,ide,2,indx,id)
      if (id /= 0) call err023
      call LU_solution(am,ide,2,indx,bm)
      dqi = bm(1)
      dzi = bm(2)
   endif
   dz(isa) = dzi
   dq(isa) = dqi
   !   enregistrement des résultats au noeud amont
   n = la_topo%biefs(ib)%nam
   dzn(n) = dzi
   zn(n) = zn(n)+zt(isa)+dzi
   qn(n) = qn(n)-qt(isa)-dqi

   !                         descente

   do isj = isa+1, isz
      isi = isj-1
      dqj = c(isj)-(b(isj)*dqi+a(isj)*dzi)
      dzj = (t(isj)-r(isj)*dqj)/s(isj)
      dz(isj) = dzj
      dq(isj) = dqj
      dzi = dzj  ;  dqi = dqj
   enddo
   !---enregistrement des résultats au noeud aval
   if (dzj > huge(zero)/2) stop '>>>> BUG 1 dans rst2z()'
   if (dqj > huge(zero)/2) stop '>>>> BUG 2 dans rst2z()'
   n = la_topo%biefs(ib)%nav
   dzn(n) = dzj
   zn(n) = zn(n)+zt(isz)+dzj
   qn(n) = qn(n)+qt(isz)+dqj

end subroutine rst2z
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
