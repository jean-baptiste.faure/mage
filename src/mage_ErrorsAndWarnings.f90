!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - INRAE - 1989-2020                           #
!# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################


!############################ GESTION DES INCIDENTS ###########################

subroutine Erreur_Lecture(fichier,ligne)
!==============================================================================
!     Erreur de lecture d'un fichier de données déclenchée
!     par next_int() ou next_real()
!==============================================================================
   use, intrinsic :: iso_fortran_env, only: error_unit
   implicit none
   ! -- prototype --
   character(len=*), intent(in) :: fichier, ligne

   write(error_unit,*)
   write(error_unit,*) ' >>>>'
   write(error_unit,*) ' >>>> Erreur de lecture du fichier ',trim(fichier),' à la ligne '
   write(error_unit,*) ' >>>> ',ligne
   write(error_unit,*) ' >>>>'
   write(error_unit,*)
   stop 042
end subroutine Erreur_Lecture


subroutine Err001(t,i,ecarz,ecarz0,ecarq,ecarq0,izmax,iqmax,modele)
!==============================================================================
!              Erreur : divergence des itérations
!==============================================================================
   use parametres, only: long, un, lErr
   use data_num_mobiles, only: dt
   use IO_Files, only: errFile
   use mage_utilitaires, only: heure
   use TopoGeometrie, only: xgeo, numero_bief
   implicit none
   ! -- prototype --
   integer,intent(in) :: i
   integer,intent(inout) :: izmax,iqmax
   real(kind=long),intent(in) :: t,ecarz,ecarz0,ecarq,ecarq0
   character(len=*), optional :: modele
   ! -- variables --
   character(len=19) :: hms
   integer :: ibz,ibq
   real(kind=long) :: xq,xz
   !---------------------------------------------------------------------------
   if (errFile == '') return
   xz = xgeo(izmax)          ;  xq = xgeo(iqmax)
   ibz = numero_bief(izmax)  ;  ibq = numero_bief(iqmax)
   hms = heure(t-dt)
   if (dt >= 100._long) then
      write(lErr,999) trim(hms),int(dt),trim(modele)
   elseif (dt >= un) then
      write(lErr,1001) trim(hms),dt,trim(modele)
   else
      write(lErr,1002) trim(hms),dt,trim(modele)
   endif
   write(lErr,1000)
   write(lErr,2000) i-1,ecarz0,ecarq0
   write(lErr,2000) i,ecarz,ecarq
   write(lErr,3001) xz,ibz,xq,ibq

   999  format(1x,'Err001 à ',A,' + ',I3.3,' : DIVERGENCE des itérations (',A,')')
   1000 format(7x,'écart relatif (à Ymax et Qmax) croissant entre 2 itérées successives')
   1001 format(1x,'Err001 à ',A,' + ',F0.3,' : DIVERGENCE des itérations (',A,')')
   1002 format(1x,'Err001 à ',A,' + 0',F0.3,' : DIVERGENCE des itérations (',A,')')
   2000 format(7x,'Itération ',I2.2,' : écart en DZ = ',E12.6,' ; écart en DQ = ',E12.6)
   3001 format(7x,'Z : Pm ',F10.2,' du bief ',I3,1X,'; Q : Pm ',F10.2,' du bief ',I3)
end subroutine Err001
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine Err002(T,I,EcarZ0,EcarZ,EcarQ0,EcarQ,IZMax,IQMax)
!==============================================================================
!              Erreur : non convergence des itérations
!==============================================================================
   use parametres, only: long, un, lErr
   use data_num_fixes, only: iter
   use data_num_mobiles, only: dt
   use IO_Files, only: errFile
   use erreurs_stv, only: reste,ireste
   use mage_utilitaires, only: heure
   use TopoGeometrie, only: xgeo, numero_bief
   implicit none
   ! -- prototype --
   integer,intent(in) :: i
   integer,intent(inout) :: izmax,iqmax
   real(kind=long),intent(in) :: t,ecarz,ecarz0,ecarq,ecarq0
   ! -- variables --
   character(len=19) :: hms
   integer :: ibq,ibz,ibrest
   real(kind=long) :: xq,xz,xrest
   !---------------------------------------------------------------------------
   if (errFile == '') return
   xz = xgeo(izmax)   ;  ibz = numero_bief(izmax)
   xq = xgeo(iqmax)   ;  ibq = numero_bief(iqmax)
   hms = heure(t-dt)
   if (dt >= 100._long) then
      write(lErr,999) trim(hms),int(dt)
   elseif (dt >= un) then
      write(lErr,1001) trim(hms),dt
   else
      write(lErr,1002) trim(hms),dt
   endif
   write(lErr,1000) iter
   write(lErr,2000) i-1,ecarz0,ecarq0
   write(lErr,2000) i,ecarz,ecarq
   write(lErr,3001) xz,ibz,xq,ibq

   ibrest = numero_bief(ireste)
   xrest = xgeo(ireste)
   write(lErr,'(7x,a,e12.6,a,i3.3,a,f9.2,a)') 'Residu : ',reste,'(',ibrest,':',xrest,')'

   999  format(1X,'Err002 à ',a,' + ',i3.3,' : NON CONVERGENCE des itérations ')
   1000 format(7X,'écart relatif (à Ymax et Qmax) trop grand',' après ',i2.2,' itérations')
   1001 format(1X,'Err002 à ',a,' + ',f0.3,' : NON CONVERGENCE des itérations ')
   1002 format(1X,'Err002 à ',a,' + 0',f0.3,' : NON CONVERGENCE des itérations ')
   2000 format(7X,'Itération ',i2.2,' : écart en DZ = ',e12.6,' ; écart en DQ = ',e12.6)
   3001 format(7X,'Z : Pm ',f10.2,' du bief ',i3,1x,'; Q : Pm ',f10.2,' du bief ',i3)
end subroutine Err002
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine Err003(T,I,EcarZ,EcarZ0,EcarQ,EcarQ0,IZMax,IQMax)
!==============================================================================
!                       Bouclage des itérations
!==============================================================================
   use parametres, only: long, un, lErr
   use data_num_mobiles, only: dt
   use IO_Files, only: errFile
   use mage_utilitaires, only: heure
   use TopoGeometrie, only: xgeo, numero_bief
   use erreurs_stv, only: reste,ireste
   implicit none
   ! -- prototype --
   integer,intent(in) :: i
   integer,intent(inout) :: izmax,iqmax
   real(kind=long),intent(in) :: t,ecarz,ecarz0,ecarq,ecarq0
   ! -- variables --
   character(len=19) :: hms
   integer :: ibq,ibz,ibrest
   real(kind=long) :: xq,xz,xrest
   !---------------------------------------------------------------------------
   if (errFile == '') return
   xz = xgeo(izmax)   ;  ibz = numero_bief(izmax)
   xq = xgeo(iqmax)   ;  ibq = numero_bief(iqmax)
   hms = heure(t-dt)
   if (dt >= 100._long) then
      write(lErr,1000) hms,int(dt)
   elseif (dt >= un) then
      write(lErr,1001) hms,dt
   else
      write(lErr,1002) hms,dt
   endif
   write(lErr,1100)
   write(lErr,1200) i-1,ecarz0,ecarq0
   write(lErr,1200) i,ecarz,ecarq
   write(lErr,1300) xz,ibz,xq,ibq

   ibrest = numero_bief(ireste)
   xrest = xgeo(ireste)
   write(lErr,'(7x,a,e12.6,a,i3.3,a,f9.2,a)') 'Residu : ',reste,'(',ibrest,':',xrest,')'

   1000 format(1x,'Err003 à ',a,' + ',i3.3,' : BOUCLAGE des itérations ')
   1001 format(1x,'Err003 à ',a,' + ',f0.3,' : BOUCLAGE des itérations ')
   1002 format(1x,'Err003 à ',a,' + 0',f0.3,' : BOUCLAGE des itérations ')
   1100 format(7x,'écart relatif (à Ymax et Qmax) constant : réduire le pas d''espace (?)')
   1200 format(7x,'Itération ',i2.2,' : écart en DZ = ',e12.6,' ; écart en DQ = ',e12.6)
   1300 format(7x,'Z : Pm ',f10.2,' du bief ',i3,1x,'; Q : Pm ',f10.2,' du bief ',i3)
end subroutine Err003
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine Err009(isi,a11,a12,a21,a22)
!==============================================================================
!              Dénominateur nul à une section ou un pm du bief
!
!==============================================================================
   use parametres, only: long, lErr
   use data_num_mobiles, only: t,dt
   use IO_Files, only: errFile
   use mage_utilitaires, only: heure
   use TopoGeometrie, only: xgeo, numero_bief
   implicit none
   ! -- prototype --
   integer,intent(in) :: isi
   real(kind=long),intent(in) :: a11,a12,a21,a22
   ! -- constantes --
   integer,parameter :: lecran = 6
   ! -- variables --
   character(len=19) :: hms
   integer :: lu3
   !------------------------------------------------------------------------------
   if (errFile /= '') then
      lu3 = lErr
   else
      lu3 = lecran
   endif
   hms = heure(t-dt)
   write(lu3,1000) hms,int(dt)
   write(lu3,1001) xgeo(isi),numero_bief(isi)
   write(lu3,1010) a11,a12
   write(lu3,1010) a21,a22

   1000 format(1x,'Err009 a ',a,' + ',i3.3,' ')
   1001 format(1x,'Dénominateur nul au Pm ',f10.2,' du bief ',i3)
   1010 format(25x,2(e12.6,5x))
end subroutine Err009
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine Err010(is,Fr,t,v,al,s,y,bool)
!============================================================================
!          ligne d'eau absurde (froude trés grand)
!============================================================================
   use parametres, only: long, lErr
   use IO_Files, only: errFile
   use mage_utilitaires, only: heure
   use Ouvrages, only: all_OuvCmp, all_OuvEle
   use PremierAppel, only : nap_e010
   use TopoGeometrie, only: la_topo, numero_bief, xgeo
   implicit none
   ! -- prototype --
   integer,intent(in) :: is
   real(kind=long),intent(in) :: fr, t, V, AL, S, Y
   logical :: bool
   ! -- constantes --
   integer,parameter :: max010 = 10000
   ! -- variables --
   character(len=19) :: hms
   integer :: ib, nk
   logical :: warning
   !------------------------------------------------------------------------------
   if (errFile /= '') then
      if (nap_e010 > max010) then
         rewind(lErr)
         write(lErr,'(a,i6,2a)') ' ATTENTION : plus de ',max010,  &
         ' messages Err010 -> suppression des messages précédents'
         nap_e010 = 0
      endif

      ib = numero_bief(is)
      warning = .true.
      if (la_topo%sections(is)%iss /= 0) warning = .false.  !rien a faire :  singularite
      if ((is == la_topo%biefs(ib)%is2) .and. (la_topo%nodes(la_topo%biefs(ib)%nav)%cl < 0)) warning = .false. !rien à faire : CL aval
      if(la_topo%sections(is+1)%iss /= 0) then
         nk = all_OuvCmp(la_topo%sections(is+1)%iss)%OuEl(1,1)
         if (all_OuvEle(nk)%iuv /= 91) warning = .false. !singularite non Borda
      endif
      if (warning) then
         nap_e010 = nap_e010+1
         hms = heure(t)
         write(lErr,1001) hms
         write(lErr,1002) fr,xgeo(is),ib
         write(lErr,1003) V, AL, S, Y
      endif
      bool = warning
   else
      bool = .true.
   endif

   1001 format(1x,'Err010 à ',a,6x,' : INSTABILITÉ NUMÉRIQUE',' (Froude grand)')
   1002 format(7x,'Froude = ',e8.2,' au Pm ',f10.2,' du Bief ',i3.3)
   1003 format(7x,'Vitesse = ',f6.3,' Largeur = ',f8.3,' Section = ',f8.2,' Hauteur = ',f6.3)
end subroutine Err010
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine Err011(T,Nom)
!============================================================================
!       inégalité des cotes aux noeud et probablement bief sec
!============================================================================
   use parametres, only: long, lErr
   use IO_Files, only: errFile
   use data_num_mobiles, only: dt
   use mage_utilitaires, only: heure
   implicit none
   ! -- prototype --
   real(kind=long),intent(in) :: t
   character(len=3),intent(in) :: nom
   ! -- variables --
   character(len=19) :: hms

   if (errFile == '') return
   hms = heure(t-dt)
   write(lErr,1000) hms,int(dt),nom
   write(lErr,2000) nom

   1000 format(1X,'Err011 à ',a,' + ',i3.3,' : inégalité des cotes au noeud ',a)
   2000 format(1X,'===> Il est possible que l''un des biefs liés au noeud ',a,' soit sec')
end subroutine Err011
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine Err012(is)
!============================================================================
!       C.L. aval en cote inférieure à la cote du fond.
!============================================================================
   use parametres, only: long, lTra
   use, intrinsic :: iso_fortran_env, only: error_unit

   use data_num_mobiles, only: t,dt
   use mage_utilitaires, only: heure
   use TopoGeometrie, only: numero_bief
   implicit none
   ! -- prototype --
   integer,intent(inout) :: is
   ! -- variables --
   integer :: ib
   character(len=19) :: hms

   ib = numero_bief(is)
   hms = heure(t-dt)
   write(lTra,1000) ib,hms,int(dt)
   write(lTra,1010)
   write(error_unit,1000) ib,hms,int(dt)
   write(error_unit,1010)
   stop 012

   1000 format(1x,'Err012 à la section aval du bief ',i3.3,' au temps ',a,' + ', &
               i3.3,' : ',/,'      Cote imposée inférieure à la cote du fond')
   1010 format(1x,'Il y a une erreur dans la géométrie ou la C.L. aval')
end subroutine Err012
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine Err013(n)
!============================================================================
!       C.L. aval en Q(z) qui dépasse la cote de berge
!============================================================================
   use parametres, only: long, lTra
   use, intrinsic :: iso_fortran_env, only: error_unit

   use data_num_mobiles, only: t,dt
   use mage_utilitaires, only: heure
   use TopoGeometrie, only: la_topo
   implicit none
   ! -- prototype --
   integer,intent(inout) :: n
   ! -- variables --
   character(len=19) :: hms

   hms = heure(t-dt)
   write(lTra,1000) la_topo%nodes(n)%name,hms,int(dt)
   write(lTra,1010)
   write(error_unit,1000) la_topo%nodes(n)%name,hms,int(dt)
   write(error_unit,1010)
   stop 013

   1000 format(1x,'Err013 au noeud aval ',a3,' au temps ',a,' + ',i3.3,' : ',/, &
   '       Le débit maximum de la loi de tarage aval est inférieur au débit réel')
   1010 format(1x,'       Il y a une erreur dans la géométrie ou la C.L. aval')
end subroutine Err013
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine Err014(is,n,sect)
!==============================================================================
!         Message d'erreur pour smoy (n=1) ou pmoy (n=2) négatif
!==============================================================================
   use parametres, only: long, zero, un, cent, lTra
   use, intrinsic :: iso_fortran_env, only: error_unit

   use mage_utilitaires, only: zegal
   use booleens, only: ber014
   use StVenant_Debord, only: section_values
   use TopoGeometrie, only: la_topo, numero_bief, xgeo, zfond
   implicit none
   ! -- prototype --
   integer,intent(in) :: is,n
   type(section_values),intent(inout),target :: sect
   ! -- constantes --
   real(kind=long),parameter :: eps = 1.e-1_long
   ! -- variables --
   integer :: ib
   real(kind=long),pointer :: r,r0,r1

   if (n == 1) then
      r  => sect%smoy  ;  r0 => la_topo%sections(is)%smy  ;  r1 => la_topo%sections(is)%almy
   else if (n == 2) then
      r  => sect%pmoy  ;  r0 => sect%pmin  ;  r1 => la_topo%sections(is)%ymoy
   else
      write(lTra,'(a)') ' erreur dans err014 '
      write(lTra,'(a)') ' Merci d''envoyer un rapport de bug'
      write(error_unit,'(a)') ' erreur dans err014 '
      write(error_unit,'(a)') ' Merci d''envoyer un rapport de bug'
      stop 185
   endif
   if (zegal(r,zero,r0) .or. abs(r)<eps) then
      r = zero
      return
   endif
   ib = numero_bief(is)
   if (n == 1) then
      write(lTra,1000) xgeo(is),ib
      write(lTra,1001) r,sect%z
      write(lTra,1006) r0,r1
      write(lTra,1004)
      write(error_unit,1000) xgeo(is),ib
      write(error_unit,1001) r,sect%z
      write(error_unit,1004)
   else if (n==2) then
      write(lTra,1002) xgeo(is),ib
      write(lTra,1003) r,sect%z,r1+zfond(is)
      write(lTra,1005)
      write(error_unit,1002) xgeo(is),ib
      write(error_unit,1003) r,sect%z,r1+zfond(is)
      write(error_unit,1005)
   end if
   ber014 = .true.
   r = zero

   1000 format(1x,'Erreur 014 : ',/,1x,'Surface mouillée du lit moyen négative ', &
                  'à l''abscisse ',f10.2,' du bief ',i3)
   1001 format(1x,'Smoy = ',e14.8,' ; cote de la surface libre = ',f8.3)
   1006 format(1x,'Section mineur plein bord = ',e14.8,' ; largeur plein bord = ',f8.3)
   1002 format(1x,'Erreur 014 : ',/,1x,'Périmètre mouillé du lit moyen négatif ', &
                  'à l''abscisse ',f10.2,' du bief ',i3)
   1003 format(1x,'Pmoy = ',e14.8,' ; cote de la surface libre = ',f8.3,' ; cote de débordement = ',f8.3)
   1004 format(1x,'Vérifier que le profil en question ne se rétrécit pas au dessus de la cote',/, &
               1x,'de débordement mineur-moyen.')
   1005 format(1x,'Ce type de problème peut se produire si la pente latérale des profils',/, &
               1x,'de données qui encadrent la section fautive est trop petite.')
end subroutine Err014
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine Err015(nk,ns)
!==============================================================================
!        Cas d'une vanne à surface libre
!==============================================================================
   use Parametres, only: lTra
   use, intrinsic :: iso_fortran_env, only: error_unit

   use Ouvrages, only: all_OuvCmp
   use TopoGeometrie, only: numero_bief
   implicit none
   ! -- prototype --
   integer,intent(in) :: nk,ns
   ! -- variables --
   integer :: ib,js,k,ne
   logical :: bool
   !---------------------------------------------------------------------------
   ne = all_OuvCmp(ns)%ne
   bool = .true.
   do k = 1, ne
      if (all_OuvCmp(ns)%OuEl(k,1) == nk) then
         bool = .false.
         exit
      endif
   enddo
   if (bool) then
      ! il n'y a pas de vanne à la section NS : bug
      write(lTra,'(a)') ' Erreur dans Err015 : Merci d''envoyer un rapport de bug'
      write(error_unit,'(a)') ' Erreur dans Err015 : Merci d''envoyer un rapport de bug'
      stop 186
   else
      js = all_OuvCmp(ns)%js-1
      ib = numero_bief(js)
      write(lTra,1000) k,js,ib
      write(error_unit,1000) k,js,ib
      stop 015
   endif

   1000 format(1x,'Err015 ',/,1x,'la vanne numéro ',i1,' située à la section ',i3,' du bief ',i3, &
        /,' fonctionne à surface libre :',/,'>>>> utiliser une formulation déversoir-orifice ')
end subroutine Err015
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine Err018(NS)
!==============================================================================
!     Err018 dans le calcul de la répartition du débit entre les ouvrages
!     élémentaires composant une singularité
!==============================================================================
   use Parametres, only: lTra
   use, intrinsic :: iso_fortran_env, only: error_unit

   use Ouvrages, only: all_OuvCmp
   use TopoGeometrie, only: numero_bief
   implicit none
   ! -- prototype --
   integer,intent(in) :: ns
   ! -- variables --
   integer :: ib,js
   !---------------------------------------------------------------------------
   js = all_OuvCmp(ns)%js
   ib = numero_bief(js)
   write(lTra,1000) ns,js-1,ib
   write(error_unit,1000) ns,js-1,ib
   stop 018

   1000 format(1x,'Err018 dans le calcul de la répartition du débit',/,       &
               1x,'     entre les ouvrages élémentaires composant la ',/,     &
               1x,'     singularité numéro ',i3,' située à la section ',i3,/, &
               1x,'     du bief ',i3)
end subroutine Err018
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine Err020
!==============================================================================
!                 Pas de temps inférieur à dtmin
!==============================================================================
   use parametres, only: lTra, l9

   use mage_utilitaires, only: zegal, f0p
   use data_num_fixes, only: dtmin
   implicit none
   ! -- variables --
   character(len=80) :: err_message1, err_message2, err_message3

   write(err_message1,'(1x,3a)') 'ERR020 : pas de temps inférieur à ',trim(f0p(dtmin,3)),' seconde(s) '
   err_message3 = ' vérifier les DONNÉES et consulter la NOTICE (chapitre ERREURS FATALES)'
   err_message2 = ' ---> divergence des itérations : consulter les fichiers .tra et .err'

   write(lTra,'(a)') ''
   write(lTra,'(a)') trim(err_message1)
   write(lTra,'(a)') trim(err_message2)
   write(lTra,'(a)') trim(err_message3)
   write(lTra,'(a)') ''
   write(l9,'(a)') trim(err_message1)
   write(l9,'(a)') trim(err_message2)
   write(l9,'(a)') trim(err_message3)
end subroutine Err020
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine Err021(ns,h1k,h2k,q0)
   use parametres, only: long, lTra
   use, intrinsic :: iso_fortran_env, only: error_unit

   use Ouvrages, only: all_OuvCmp
   use TopoGeometrie, only: la_topo, numero_bief
   implicit none
   ! -- prototype --
   integer,intent(in) :: ns
   real(kind=long),intent(in) :: h1k,h2k,q0
   ! -- variables --
   integer :: ib, js, i, u
   character(len=50) :: titre_erreur = ' >>>> ERREUR 021 <<<<'
   real(kind=long) :: pk

   write(error_unit,'(1x,a)') trim(titre_erreur)
   write(lTra,'(1x,a)') trim(titre_erreur)
   js = all_OuvCmp(ns)%js-1
   ib = numero_bief(js)
   u = lTra
   pk = la_topo%sections(js)%pk
   do i = 1, 2
      if (i == 2) u = error_unit
      write(u,'(1x,a)') trim(titre_erreur)
      write(u,'(a)') ' Calcul de perte de charge sur ouvrage (Type d''itération 00)'
      write(u,'(a,f10.4,(a,i3),a)') '      Pk de la section singulière : ',pk,' (bief : ',ib,')'
      write(u,'(a,e12.6)')   '      Charge amont = ',h1k
      write(u,'(a,e12.6)')   '      Charge aval  = ',h2k
      write(u,'(a,e12.6)')   '      Débit sur l''ouvrage = ',q0
      write(u,'(a,/,a)') ' ----> Vérifier qu''il n''y a pas d''ouvrage DÉNOYÉ ou de', &
                         ' section singulière qui ne coule pas dans un bief de la MAILLE.'
      write(u,'(a,/,a)') ' Si c''est le cas utiliser le type d''itération -1', &
                         ' sans basculement sur le type 0.'
   enddo
   stop 021
end subroutine err021
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine Err023
!==============================================================================
!              ERREUR dans RST2Z : matrice singuliere
!==============================================================================
   use Parametres, only: lTra
   use, intrinsic :: iso_fortran_env, only: error_unit

   implicit none
   ! -- les variables --
   integer ::u, i

   u = lTra
   do i = 1, 2
      if (i == 2) u = error_unit
      write(u,*) ' ERREUR 023 dans RST2Z : matrice singulière'
      write(u,*) ' Vérifier que chaque noeud aval correspond à un seul bief'
   enddo
   stop 023
end subroutine Err023
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine Err024
!==============================================================================
!                    la matrice de la maille est singulière
!==============================================================================
   use Parametres, only: lTra
   use, intrinsic :: iso_fortran_env, only: error_unit

   implicit none
   ! -- les variables --
   integer :: u, i

   u = lTra
   do i = 1, 2
      if (i == 2) u = error_unit
      write(u,*) ' ERREUR 024 : la matrice de la maille est singulière'
      write(u,*) ' Arrêt dans BAL32'
   enddo
   stop 024
end subroutine Err024
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine Err025(iss,y,ityp)
!==============================================================================
!                       Tirant d''eau négatif
!==============================================================================
   use parametres, only: long, lErr, lTra
   use, intrinsic :: iso_fortran_env, only: error_unit

   use data_num_mobiles, only: t,dt
   use IO_Files, only: errFile
   use mage_utilitaires, only: heure
   use TopoGeometrie, only: numero_bief, xgeo, zfd
   implicit none
   ! -- prototype --
   integer,intent(in) :: iss,ityp
   real(kind=long),intent(in) :: y
   ! -- variables --
   character(len=50) :: car
   character(len=19) :: hms
   integer :: ib,idt
   !------------------------------------------------------------------------------
   if (errFile /= '') then
      ib = numero_bief(iss)
      hms = heure(t-dt)
      idt = int(dt)
      if (ityp == 1) then
         car = 'Discretise_BiefB '
      else if (ityp == 0) then
         car = 'Discretise_Bief  '
         hms = heure(t)
         idt=0
      else if (ityp == 2) then
         car = 'Divergence des itérations'
      else if (ityp == 3) then
         car = 'SURF  '
      else if (ityp == 4) then
         car = 'QCRIT '
      else if (ityp == 5) then
         car = 'ALFN  '
      else if (ityp == 6) then
         car = 'Perimetre'
      else if (ityp == 7) then
         car = 'Q_critique'
      else if (ityp == -1) then
         car = 'Euler'
      endif
      write(lErr,1000) hms,idt,xgeo(iss),ib,y,zfd(iss)
      write(lErr,1001) trim(car)
   endif
   if (ityp/=2) then
      write(lTra,1020)
      write(error_unit,1020)
      stop 025
   endif

   1000 format(1x,'Err025 à ',a,' + ',i3.3,/,'>>>> Tirant d''eau négatif au Pm ', &
             f10.2,' du bief ',i3,' : ',f8.3,' (cote du fond : ',f8.3,')')
   1001 format(1x,'Appel par ',a)
   1020 format(1x,' >> REMARQUE : ',/,' Il y a un problème numérique.'  &
               ,/,' SOLUTIONS POSSIBLES : '                           &
               ,/,' Réduire le pas de temps'                          &
               ,/,' Réduire le pas d''espace'                         &
               ,/,' Augmenter la valeur de THETA'                     &
               ,/,' Utiliser une viscosité artificielle')
end subroutine Err025
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine Err026(ns,k,ham,h2,q)
!==============================================================================
!       Erreur : HAM < 0  ou  H2 > HAM
!==============================================================================
   use parametres, only: long, lTra
   use, intrinsic :: iso_fortran_env, only: error_unit
   implicit none
   ! -- prototype --
   integer,intent(in) :: ns,k
   real(kind=long),intent(in) :: ham,h2,q
   ! -- variables --
   character(len=120) :: err_message1, err_message2, err_message3, err_message4

   err_message1 = ' Hamont < 0  ou  Haval > Hamont'
   err_message2 = ' Si c''est le cas utiliser le type d''itération -1 sans basculement sur le type 0.'
   err_message3 = ' ----> Vérifier qu''il n''y a pas d''ouvrage DÉNOYÉ ou de section singulière'
   err_message4 = ' qui ne coule pas dans un bief de la MAILLE.'
   write(lTra,1000) k,ns
   write(lTra,'(a)') trim(err_message1)
   write(lTra,1001) ham,h2,q
   write(lTra,'(a,/,a)') trim(err_message3),trim(err_message4)
   write(lTra,'(a)') trim(err_message2)
   write(error_unit,1000) k,ns
   write(error_unit,'(a)') trim(err_message1)
   write(error_unit,1001) ham,h2,q
   write(error_unit,'(a,/,a)') trim(err_message3),trim(err_message4)
   write(error_unit,'(a)') trim(err_message2)

   stop 026

   1000 format(1X,'Err026 à l''ouvrage élémentaire ',i3,' de la singularité ',i3)
   1001 format(1X,' Hamont = ',e12.6,5x,' Haval = ',e12.6,5x,' Débit = ',e12.6)
end subroutine Err026
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine Err027(iss,ityp)
!==============================================================================
!                       Tirant d''eau NaN
!==============================================================================
   use parametres, only: long, lErr, lTra
   use, intrinsic :: iso_fortran_env, only: error_unit

   use data_num_mobiles, only: t,dt
   use IO_Files, only: errFile
   use mage_utilitaires, only: heure
   use TopoGeometrie, only: numero_bief, xgeo
   implicit none
   ! -- prototype --
   integer,intent(in) :: iss,ityp
   ! -- variables --
   character(len=50) :: car
   character(len=19) :: hms
   integer :: ib,idt
   !------------------------------------------------------------------------------
   if (errFile /= '') then
      ib = numero_bief(iss)
      hms = heure(t-dt)
      idt = int(dt)
      if (ityp == 1) then
         car = 'Discretise_BiefB '
      else if (ityp == 0) then
         car = 'Discretise_Bief  '
         hms = heure(t)
         idt = 0
      else if (ityp == 2) then
         car = 'verif_HauteurEau : Divergence des itérations'
      else if (ityp == 3) then
         car = 'SURF  '
      else if (ityp == 4) then
         car = 'QCRIT '
      else if (ityp == 5) then
         car = 'ALFN  '
      else if (ityp == 6) then
         car = 'Perimetre'
      else if (ityp == 7) then
         car = 'Q_critique'
      else if (ityp == -1) then
         car = 'Euler'
      endif
      if (dt > 1._long .or. ityp == 0) then
         write(lErr,1000) hms,idt,xgeo(iss),ib
      else
         write(lErr,1001) hms,dt,xgeo(iss),ib
      endif
      write(lErr,1010) trim(car)
   endif
   if (ityp/=2) then
      write(lTra,1020)
      write(error_unit,1020)
      stop 025
   endif

   1000 format(1x,'Err027 à ',a,' + ',i3.3,/,'>>>> Tirant d''eau NaN au Pm ', &
                  f10.2,' du bief ',i3,' ')
   1001 format(1x,'Err027 à ',a,' + ',f4.2,/,'>>>> Tirant d''eau NaN au Pm ', &
                  f10.2,' du bief ',i3,' ')
   1010 format(1x,'Appel par ',a)
   1020 format(1x,' >> REMARQUE : ',/,' Il y a un problème numérique.'  &
               ,/,' SOLUTIONS POSSIBLES : '                           &
               ,/,' Réduire le pas de temps'                          &
               ,/,' Réduire le pas d''espace'                         &
               ,/,' Augmenter la valeur de THETA'                     &
               ,/,' Utiliser une viscosité artificielle')
end subroutine Err027
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine Err030(js)
   use Parametres, only: ltra
   use, intrinsic :: iso_fortran_env, only: error_unit

   use TopoGeometrie, only: la_topo, numero_bief
   implicit none
   ! -- prototype --
   integer,intent(in) :: js
   ! -- variables locales temporaires --
   integer :: ib,is
   character(len=120) :: err_message
   character(len=30) :: titre_erreur = ' >>>> ERREUR 030 <<<<'

   ib = numero_bief(js)
   is = js - la_topo%biefs(ib)%is1+1
   write(error_unit,'(1x,a)') trim(titre_erreur)
   write(lTra,'(1x,a)') trim(titre_erreur)
   write(err_message,'(1x,a,3(i3,a))') 'Les sections ',is,' et ',is+1,' du bief ',ib, &
                                       ' sont à la même abscisse sans être singulières'
   write(error_unit,'(a)') trim(err_message)
   write(lTra,'(a)') trim(err_message)
end subroutine Err030
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine Err031(ib)
   use Parametres, only: lTra
   use, intrinsic :: iso_fortran_env, only: error_unit

   use TopoGeometrie, only: la_topo
   implicit none
   ! -- prototype --
   integer,intent(in) :: ib
   character(len=120) :: err_message
   character(len=30) :: titre_erreur = ' >>>> ERREUR 031 <<<<'

   write(error_unit,'(1x,a)') trim(titre_erreur)
   write(lTra,'(1x,a)') trim(titre_erreur)
   write(err_message,'(1x,a,i3,a)') 'La section aval du bief ',la_topo%net%numero(ib), &
                                    ' est singulière sans être doublée'
   write(error_unit,'(a)') trim(err_message)
   write(lTra,'(a)') trim(err_message)
end subroutine Err031
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine Err032(pk,ib)
   use parametres, only: long, lTra
   use, intrinsic :: iso_fortran_env, only: error_unit
   implicit none
   ! -- prototype --
   integer,intent(in) :: ib
   real(kind=long)    :: pk
   character(len=120) :: err_message
   character(len=30) :: titre_erreur = ' >>>> ERREUR 032 <<<<'

   write(error_unit,'(1x,a)') trim(titre_erreur)
   write(lTra,'(1x,a)') trim(titre_erreur)
   write(err_message,'(1x,a,g0,a,i3)') 'Il n''y a pas de section singulière au pk ',pk,' du bief ',ib
   write(error_unit,'(a)') trim(err_message)
   write(lTra,'(a)') trim(err_message)
end subroutine Err032
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine Err033(is)
   use Parametres, only: lTra
   use, intrinsic :: iso_fortran_env, only: error_unit

   use TopoGeometrie, only: la_topo, numero_bief
   implicit none
   ! -- prototype --
   integer,intent(in) :: is
   ! -- variables --
   integer :: ib,js
   character(len=120) :: err_message
   character(len=30) :: titre_erreur = ' >>>> ERREUR 033 <<<<'

   ib = numero_bief(js)
   js = is - la_topo%biefs(ib)%is1+1
   write(error_unit,'(1x,a)') trim(titre_erreur)
   write(lTra,'(1x,a)') trim(titre_erreur)
   write(err_message,'(1x,2(a,i3),a)') 'La section ',js,' du bief ',ib, &
         ' rétrécit au dessus de la cote de débordement mineur-moyen'
   write(error_unit,'(a)') trim(err_message)
   write(lTra,'(a)') trim(err_message)
end subroutine Err033
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine Err034(n)
   use parametres, only: long, lTra
   use, intrinsic :: iso_fortran_env, only: error_unit

   use TopoGeometrie, only: la_topo
   implicit none
   ! -- prototype --
   integer,intent(in) :: n
   ! -- variables --
   character(len=120) :: err_message1, err_message2
   character(len=30) :: titre_erreur = ' >>>> ERREUR 034 <<<<'

   write(error_unit,'(1x,a)') trim(titre_erreur)
   write(lTra,'(1x,a)') trim(titre_erreur)
   write(err_message1,'(1x,3a)') ' Le noeud ',la_topo%nodes(n)%name,' est nœud amont et défluent :'
   write(err_message2,'(1x,4a)') ' Ajouter un bief fictif à l''amont de ce nœud',    &
                                 ' et transférer la C.L. de ',la_topo%nodes(n)%name, &
                                 ' vers le nouveau nœud amont'
   write(error_unit,'(a)') trim(err_message1),trim(err_message2)
   write(lTra,'(a)') trim(err_message1),trim(err_message2)
end subroutine Err034
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine Err035
!==============================================================================
!            ERREUR : Absence de biefs amont de maille
!==============================================================================
   use Parametres, only: lTra
   use, intrinsic :: iso_fortran_env, only: error_unit
   implicit none
   ! -- variables locales temporaires --
   character :: err_message1*55, err_message2*24
   character(len=30) :: titre_erreur = ' >>>> ERREUR 035 <<<<'

   write(error_unit,'(1x,a)') trim(titre_erreur)
   write(lTra,'(1x,a)') trim(titre_erreur)
   err_message1 = ' >>>> il faut un bief à l''amont de la maille <<<<'
   err_message2 = ' calcul impossible sinon'
   write(error_unit,'(a)') err_message1  ;  write(error_unit,'(a)') err_message2
   write(lTra,'(a)') err_message1  ;  write(lTra,'(a)') err_message2
   stop 035
end subroutine Err035
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine Err037(is)
   use Parametres, only: lTra
   use, intrinsic :: iso_fortran_env, only: error_unit

   use TopoGeometrie, only: numero_bief, xgeo
   implicit none
   ! -- prototype --
   integer,intent(in) :: is
   ! -- variables locales temporaires --
   integer :: ib
   character(len=30) :: titre_erreur = ' >>>> ERREUR 037 <<<<'
   character(len=120) :: err_message

   write(error_unit,'(1x,a)') trim(titre_erreur)
   write(lTra,'(1x,a)') trim(titre_erreur)
   ib = numero_bief(is)
   write(err_message,'(1x,a,f10.2,i3.3)') ' La section Pm = ',xgeo(is),' du bief ', &
                                         ib,' a un lit mineur de profondeur nulle'
   write(error_unit,'(a)') trim(err_message)
   write(lTra,'(a)') trim(err_message)
end subroutine Err037
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine Err038
!==============================================================================
! surveillance du débordement du tableau Zfond dans Detect_Marche_Noeud()
!==============================================================================
   use Parametres, only : lTra
   use, intrinsic :: iso_fortran_env, only: error_unit
   implicit none
   ! -- variables locales --
   character(len=30) :: titre_erreur = ' >>>> ERREUR 038 <<<<'
   character(len=120) :: err_message

   write(error_unit,'(1x,a)') trim(titre_erreur)
   write(lTra,'(1x,a)') trim(titre_erreur)

   write (err_message,'(a)') 'Débordement du tableau Zfond dans la routine Detect_Marche_Noeud()'
   write(error_unit,'(a)') trim(err_message)
   write(lTra,'(a)') trim(err_message)

   write (err_message,'(a)') 'Veuillez envoyer un rapport de bug'
   write(error_unit,'(a)') trim(err_message)
   write(lTra,'(a)') trim(err_message)
   stop 038
end subroutine Err038
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine Err039(kb,neu)
!==============================================================================
!  Message d'erreur pour le déversement inversé casier -> bief
!==============================================================================
   use Parametres, only: lTra, l9
   use, intrinsic :: iso_fortran_env, only: error_unit

   use TopoGeometrie, only: la_topo
   implicit none
   ! -- prototype --
   integer, intent(in) :: kb, neu
   ! -- variables locales --
   integer :: u, i

   u = lTra
   do i = 1, 3
      if (i==2) u = error_unit
      if (i==3) u = l9
      write(u,'(a,i3,3a)') ' Erreur 039 : le déversement du bief ', &
              kb,' vers le casier ',la_topo%nodes(neu)%name,' coule à l''envers'
      write(u,'(a)') ' Ce type de fonctionnement est mal simulé par MAGE.'
      write(u,'(a)') ' Modéliser plutôt le casier par un bief.'
   enddo
   stop 039
end subroutine Err039
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine Err040(lu,nom,regle)
!==============================================================================
!     erreur de type d'ouvrage pour une règle de régulation (VAR)
!==============================================================================
   use, intrinsic :: iso_fortran_env, only: error_unit
   implicit none
   ! -- prototype --
   integer, intent(in) :: lu
   character(len=*), intent(in) :: nom, regle

   write(lu,'(2a)') '>>> Erreur dans la règle',regle
   write(lu,'(4a)')  'L''ouvrage ',trim(nom),' devrait être de type O, V ou W'
   write(error_unit,'(2a)') '>>> Erreur dans la règle',regle
   write(error_unit,'(4a)')  'L''ouvrage ',trim(nom),' devrait être de type O, V ou W'
   stop 040
end subroutine Err040
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine Err041(lu,nligne,u,yz)
!==============================================================================
!     Erreur de lecture du fichier DEV
!==============================================================================
   use, intrinsic :: iso_fortran_env, only: error_unit
   implicit none
   integer, intent(in) :: lu,nligne
   character(len=*),intent(in) :: u
   character, intent(in) :: yz
   character(len=10) :: detype

   if (yz ==' ') then
      detype = ''
   else
      detype = ' de type '//yz
   endif
   write(lu,'(a,i3,2a)') ' >>>> ERREUR de lecture dans .DEV, ligne numéro ',nligne,detype
   write(lu,'(1x,a78)') u

   write(error_unit,'(a,i3,2a)')' >>>> ERREUR de lecture dans .DEV, ligne numéro ',nligne,detype
   write(error_unit,'(1x,a78)') u

end subroutine Err041
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine Err042(lu,message)
!==============================================================================
!     Instabilité dans ISM
!==============================================================================
   use parametres, only: long, un
   use data_num_mobiles, only: t, dt, dtmin0
   use mage_utilitaires, only: heure
   implicit none
   integer, intent(in) :: lu
   character(len=*),intent(in) :: message
   character(len=19) :: hms

   hms = heure(t-dt)
   if (dtmin0 >= un) then
      write(lu,'(3a,i3.3,a)') ' Err042 à ',trim(hms),' + ',int(dt),' dans la discrétisation ISM : instabilité'
   else
      if (dt >= un) then
         write(lu,'(3a,f0.2,a)') ' Err042 à ',trim(hms),' + ',dt,' dans la discrétisation ISM : instabilité'
      else
         write(lu,'(3a,f0.2,a)') ' Err042 à ',trim(hms),' + 0',dt,' dans la discrétisation ISM : instabilité'
      endif
   endif
   write(lu,'(7x,a)') trim(message)

end subroutine Err042
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine War001(is,y,ymax,l)
!==============================================================================
!                 Tirant d'eau > Yberge + 10 m
!==============================================================================
   use parametres, only: long, issup, un, lErr, nomfin_ini
   use, intrinsic :: iso_fortran_env, only: error_unit

   use mage_utilitaires, only: zegal, heure
   use data_num_fixes, only: tinf,silent
   use data_num_mobiles, only: t,dt
   use IO_Files, only: errFile
   use PremierAppel, only : nap => nap_w001, nap001 => nap_w001b
   use TopoGeometrie, only: la_topo, numero_bief, xgeo
   use StVenant_Debord, only: Debord_IniFin
   implicit none
   ! -- prototype --
   integer,intent(in) :: is,l
   real(kind=long),intent(in) :: y,ymax
   ! -- variables --
   character(len=3) :: cdt
   character(len=19) :: hms
   integer :: ib,lu15
   real(kind=long),save :: t0=-1000000._long
   real(kind=long) :: tyet
   real(kind=long),dimension(:),allocatable :: q,v,yy,z
   integer, parameter :: max001=5000
   character(len=120) :: err_message
   character(len=25), parameter :: stop_message = '001 - Arrêt du programme'

   allocate(q(la_topo%net%ns),v(la_topo%net%ns), &
          & yy(la_topo%net%ns), z(la_topo%net%ns))
   if (errFile /= '') then
      lu15 = lErr
   else
      lu15 = error_unit
   endif
   !---comptage du nombre total de messages war001
   !   valable aussi lors de l'initialisation en permanent
   if (nap001 > max001 .and. lu15 > 0) then
      close(lu15)
      open(unit=lu15,file=errFile,status='old',form='formatted')
      write(lu15,'(a,i6,2a)') ' ATTENTION : plus de ',max001, &
                ' messages War001 -> suppression des messages précédents'
      nap001 = 0
   endif
   nap001 = nap001+1

   !---comptage de messages consécutifs
   if (.not.zegal(t,t0,un)) then
      nap = 0
      t0 = t
   endif
   if (t > tinf+dt) nap = nap+1

   !---message d'alerte principal
   hms = heure(t-dt)
   if (dt >= un) then
      write(cdt,'(i3.3)') int(dt)
   else
      write(cdt,'(f3.2)') dt
   endif
   if (y >= la_topo%sections(is)%ybmax+10._long) then
      write(lu15,'(5a)') 'War001 à ',hms,' + ',cdt,' : Tirant d''eau > Yberge + 10m'
   else
      write(lu15,'(5a)') 'War001 à ',hms,' + ',cdt,' : Tirant d''eau > Yberge'
   endif
   ib = numero_bief(is)
   write(lu15,'(a,f10.2,a,i3,2(a,f10.3),/,25x,2(a,f7.3))') ' Pm ',xgeo(is),' du bief ',ib, &
        ' : Y     = ',y,' Ymax  = ',ymax,' Yberge.min. = ',la_topo%sections(is)%ybmin,     &
        ' Yberge.max. = ',la_topo%sections(is)%ybmax

   !---Cas où il faut parfois arrêter le programme
   err_message = '' !réinitialisation pour éliminer les caractères indésirables à la fin
   if (l == +1) then
      err_message = ' Appel par SURF ou SURFY : interpolation impossible'
      write(lu15,'(a)') trim(err_message)
      write(lu15,'(a)') stop_message
      if (.not.silent) write(error_unit,'(a)') trim(err_message)
      if (.not.silent) write(error_unit,'(a)') stop_message
      stop 001
   else if (l == -1) then
      err_message(1:1) = ' '  ;  err_message(2:39) = repeat('-',38)
      err_message(39:) = ' Appel par SECJ0 '
      write(lu15,'(a)') trim(err_message)
   else if (l==0) then
      err_message = ' Appel par QCRIT : interpolation impossible'
      write(lu15,'(a)') trim(err_message)
      write(lu15,'(a)') stop_message
      if (.not.silent) write(error_unit,'(a)') trim(err_message)
      if (.not.silent) write(error_unit,'(a)') stop_message
      stop 001
   else if (l==2) then
      err_message(1:1) = ' '  ;  err_message(2:39) = repeat('-',38)
      err_message(39:) = ' Appel par nonLinear_iterations : INSTABILITE'
      write(lu15,'(a)') trim(err_message)
   else if (l==3) then
      err_message = ' Appel par ALFN : interpolation impossible'
      write(lu15,'(a)') trim(err_message)
      write(lu15,'(a)') stop_message
      if (.not.silent) write(error_unit,'(a)') trim(err_message)
      if (.not.silent) write(error_unit,'(a)') stop_message
      stop 001
   else if (l==4) then
      err_message = ' Appel par Q_critique : interpolation impossible'
      write(lu15,'(a)') trim(err_message)
      write(lu15,'(a)') stop_message
      if (.not.silent) write(error_unit,'(a)') trim(err_message)
      if (.not.silent) write(error_unit,'(a)') stop_message
      stop 001
   else if (l==5) then
      err_message = ' Appel par Perimetre() : interpolation impossible'
      write(lu15,'(a)') trim(err_message)
      write(lu15,'(a)') stop_message
      if (.not.silent) write(error_unit,'(a)') trim(err_message)
      if (.not.silent) write(error_unit,'(a)') stop_message
      stop 001
   else
      write(err_message,'(a,i3)') ' Cas non prévu dans War001 : ',L
      write(lu15,'(a)') trim(err_message)
      write(lu15,'(a)') stop_message
      if (.not.silent) write(error_unit,'(a)') trim(err_message)
      if (.not.silent) write(error_unit,'(a)') stop_message
      stop 001
   endif

   if (nap>10 .and. zegal(t,t0,un)) then
      call copier(tyet,q,z,yy,v)
      call Debord_IniFin(nomfin_ini,q,z)
      err_message = ' >>>> Plus de 10 messages War001 consécutifs'
      write(lu15,'(a)') trim(err_message)
      if (.not.silent) write(error_unit,'(a)') trim(err_message)
      err_message = ' 001 - dernière ligne d''eau dans Mage_fin.ini'
      write(lu15,'(a)') trim(err_message)
      if (.not.silent) write(error_unit,'(a)') trim(err_message)
      stop 001
   endif
   deallocate(q,v,yy,z)
end subroutine War001
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine War002
!==============================================================================
!     INSTABILITE : Les déversements latéraux imposent la réduction de DT
!==============================================================================
   use parametres, only: un, lErr
   use IO_Files, only: errFile
   use data_num_mobiles, only: t,dt
   use mage_utilitaires, only: heure
   implicit none
   ! -- variables --
   character(len=19) :: hms
   character(len=3) :: cdt

   if (errFile == '') return
   hms = heure(t)
   if (dt >= un) then
      write(cdt,'(i3.3)') int(dt)
   else
      write(cdt,'(f3.2)') dt
   endif
   write(lErr,'(5a)') ' War002 à ',hms,' + ',cdt,' >>> INSTABILITÉ <<<'
   write(lErr,'(a)') ' Les déversements latéraux imposent la réduction de DT'
end subroutine War002
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine War005(is,Fr,T)
!==============================================================================
!     Alerte FROUDE > 1
!==============================================================================
   use parametres, only: long, lErr
   use IO_Files, only: errFile
   use PremierAppel, only : nap => nap_w005
   use mage_utilitaires, only: heure
   use TopoGeometrie, only: la_topo, numero_bief, xgeo
   use Ouvrages, only: all_OuvCmp, all_OuvEle
   implicit none
   ! -- prototype --
   integer,intent(in) :: is
   real(kind=long),intent(in) :: fr,t
   ! -- constantes --
   integer,parameter :: max005 = 10000
   ! -- variables --
   character(len=19) :: hms
   integer :: ib, nk
   logical :: warning
   character(len=24) :: fmt

   if (errFile == '') return
   if (nap > max005) then
      rewind(lErr)
      write(lErr,'(a,i6,2a)') ' ATTENTION : plus de ',max005,  &
                   ' messages War005 -> suppression des messages précédents'
      nap=0
   endif

   ib = numero_bief(is)
   warning = .true.
   if (la_topo%sections(is)%iss /= 0) warning = .false.         !singularité : on laisse passer
   if ((is == la_topo%biefs(ib)%is2) .AND. (la_topo%nodes(la_topo%biefs(ib)%nav)%cl < 0)) warning = .false. !CL aval : on laisse passer
   if(la_topo%sections(is+1)%iss /= 0) then
      nk = all_OuvCmp(la_topo%sections(is+1)%iss)%OuEl(1,1)
      if (all_OuvEle(nk)%iuv /= 91) warning = .false.           !singularite non Borda : on laisse passer
   endif

   if (warning) then
      nap = nap+1
      hms = heure(t)
      if (fr < 10._long) then
         fmt = '(3a,f8.3,a,f10.2,a,i3.3)'
      else
         fmt = '(3a,e8.3,a,f10.2,a,i3.3)'  !e8.3 permet de passer le cas FR grand avec FRMAX > 99
      endif
      write(lErr,fmt) 'War005 à ',hms,'       : Froude = ',fr,'  Pm ',xgeo(is),' Bief ',ib
   endif
end subroutine War005
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine War008(lu3,js)
   use parametres, only: long
   use TopoGeometrie, only: la_topo, numero_bief
   implicit none
   ! -- prototype --
   integer,intent(in) :: lu3,js
   ! -- variables locales temporaires --
   integer :: ib
   real(kind=long) :: pk
   character(len=30) :: titre_erreur = ' >>>> WARNING 008'
   !---------------------------------------------------------------------------
   write(lu3,'(1x,a)') trim(titre_erreur)

   ib = numero_bief(js)
   pk = la_topo%sections(js)%pk
   write (lu3,'(2a,g0,a,i3,/,a)') ' >>>> le débit nominal des pompes dans la section ',  &
                                  'située au Pm ',pk,' du bief ',ib,                    &
                                  ' >>>> est différent du saut en débit de la ligne d''eau initiale'
end subroutine War008
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine War009(lu3,js)
   use TopoGeometrie, only: la_topo, numero_bief
   implicit none
   ! -- prototype --
   integer,intent(in) :: lu3,js
   ! -- variables locales temporaires --
   integer :: ib,is
   character(len=30) :: titre_erreur = ' >>>> WARNING 009'
   !------------------------------------------------------------------------------
   write(lu3,'(1x,a)') trim(titre_erreur)
   ib = numero_bief(js)
   is = js - la_topo%biefs(ib)%is1+1

   write(lu3,'(1x,a)')       '>>>> les débits nominaux des pompes situées à'
   write(lu3,'(1x,2(a,i3))') '>>>> la section ',is,' du bief ',ib
   write(lu3,'(1x,a)')       '>>>> sont de signes différents '
end subroutine War009
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine War010(pk)
!==============================================================================
!     profil donné de RD vers RG
!==============================================================================
   use parametres, only: long, lErr
   use IO_Files, only: errFile
   implicit none
   real(kind=long), intent(in) :: pk

   if (errFile /= '') then
      write(lErr,*) '>>>> le profil du pk ', pk, ' est donné de RD vers RG, on le retourne'
   endif

end subroutine War010
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
