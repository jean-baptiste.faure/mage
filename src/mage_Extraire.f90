!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - INRAE - 1989-2020                           #
!# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################
!##############################################################################
!#                                                                            #
!#                    Programmme Mage_Extraire                                #
!#                                                                            #
!##############################################################################



program Mage_Extraire
!  programme d'extraction des résultats de Mage-8 des fichiers BIN

   use data_Extraire
   use, intrinsic :: iso_fortran_env, only: input_unit, output_unit, real32, real64, int64
   use Mage_Utilitaires, only: lire_date
   implicit none
   ! -- variables locales --
   character :: nombase*18    !nom de base des fichiers
   character :: date*20       !date
   character :: type_courbe*3 !type de courbe à extraire
   character :: ficres*32     !fichier de résultats à lire : BIN
   character :: ficout*32     !fichier à écrire
   character :: line*80
   integer :: ios, iarg
   integer :: ib, isa, isb, ns, lu, is, js, lout, jj, hh, mm, ss, lBin
   real(kind=real32) :: pk, tmin, tmax, dxmin
   real(kind=real64) :: temps
   integer(kind=int64) :: tinf
   character :: tmp*3
   character(len=120) :: argmnt

   iarg = 1 ; call get_command_argument(iarg,argmnt)
   if (len_trim(argmnt) > 0) then  !il y a des arguments sur la ligne de commande
      if (argmnt(1:2) == '-v') then
         write(output_unit,*)' Programme Mage_Extraire adapté pour Mage-8'
         write(output_unit,*)' Version du 2021-03-18'
         stop
      elseif (argmnt(1:2) == '-h') then
         call help()
         stop
      else
         nombase = trim(argmnt)
         write(output_unit,'(2a)') ' Nom de base des fichiers de résultats : ',trim(nombase)
         ficout=nombase(1:len_trim(nombase))//'.res'
         iarg = iarg+1 ; call get_command_argument(iarg,argmnt)
         read(argmnt,'(a3)') tmp
         call capitalize(tmp,type_courbe)
         write(output_unit,'(2a)') ' Type de la courbe à extraire [Q|Z|Y|S|L|M|R|U|V|W|G|D]d[T|X] : ',type_courbe
         iarg = iarg+1 ; call get_command_argument(iarg,argmnt)
         read(argmnt,'(i3)') ib
         write(output_unit,'(a,i3)') ' Numéro du bief : ',ib
         iarg = iarg+1 ; call get_command_argument(iarg,argmnt)
         if (type_courbe(3:3).eq.'T') then
            read(argmnt,*) pk
            write(output_unit,'(a,g0)') ' Pk de la section à extraire : ',pk
            tmin = -1.E30
            tmax = +1.E30
         elseif (type_courbe(3:3).eq.'X') then
            if (argmnt(1:1) == 'j') then
               read(argmnt(2:),'(f10.0)') temps
               temps = aint(temps*86400._real64)
            elseif (argmnt(1:1) == 'h') then
               read(argmnt(2:),'(f10.0)') temps
               temps = aint(temps*3600._real64)
            elseif (argmnt(1:1) == 'm') then
               read(argmnt(2:),'(f10.0)') temps
               temps = aint(temps*60._real64)
            elseif (argmnt(1:1) == 's') then
               read(argmnt(2:),'(f10.0)') temps
            endif
            write(output_unit,'(a,g0)') ' Date de la ligne d''eau à extraire en secondes : ',temps
         endif
      endif
   else
      ! lecture des données par interrogation de l'utilisateur
#ifdef fake_mage7
! NOTE: on fait ça pour PamHyr qui attend cette chaîne de caractères
      write(output_unit,'(a)') ' Programme Mage6_Extraire '
#endif /* fake_mage7 */
#ifndef fake_mage7
      write(output_unit,'(a)') ' Programme Mage_Extraire '
#endif /* fake_mage7 */
      write(output_unit,'(a)',advance='no') ' Nom de base des fichiers de résultats : '
      read(input_unit,'(a)') nombase
      ficout=nombase(1:len_trim(nombase))//'.res'
      write(output_unit,'(a)',advance='no') ' Type de la courbe à extraire [Q|Z|Y|S|L|M|R|U|V|W|G|D]d[T|X] : '
      read(input_unit,'(a3)') tmp
      call capitalize(tmp,type_courbe)
      if (type_courbe(3:3).eq.'T') then
         write(output_unit,'(a)',advance='no') ' Numéro du bief : '
         read(input_unit,'(i3)') ib
         write(output_unit,'(a)',advance='no') ' Pk de la section à extraire : '
         read(input_unit,*) pk
         tmin = -1.E30
         tmax = +1.E30
      elseif (type_courbe(3:3).eq.'X') then
         write(output_unit,'(a)',advance='no') ' Numéro du bief : '
         read(input_unit,'(i3)') ib
         write(output_unit,'(a)',advance='no') ' Date de T0 au format AAAA:MM:JJTHH:MM:SS ou en secondes : '
         read(input_unit,'(a20)') date
         tinf = lire_date(date)
         write(output_unit,'(a)',advance='no') ' Date de la ligne d''eau à extraire en secondes : '
         read(input_unit,'(f10.0)') temps
         temps = temps + tinf
      endif
   endif

!---lecture fichier .REP pour trouver le nom du fichier BIN
   call find_bin(nombase, ficres)

! lecture des données générales de description
   call lire_Entete_BIN(lBin,ficres)  !lecture de l'entête de BIN
                                      !se charge de l'ouverture du fichier BIN et renvoie son unité logique
!vérifications
   if (ib > ibmax) then
      write(output_unit,'(a,i3,a)') ' Erreur : le bief ',ib,' n''existe pas'
      stop 13
   endif
! ouverture du fichier RES
   open(newunit=lout,file=ficout,status='unknown',form='formatted')


   if (type_courbe(3:3) == 'T') then
      !  recherche du numero de la section de pk Pk ; on prend la section la plus proche
      is = 0
      dxmin = 1.e+30
      do ns = is1(ib), is2(ib)
         if (abs(xl(ns)-Pk) < dxmin) then
            dxmin = abs(xl(ns)-Pk)
            is = ns
         endif
      enddo
      if (xl(is+1) == xl(is) .and. pk > xl(is) .and. is < ismax) is = is+1 !cas d'une double section
      if (is == 0 .or. dxmin > 1.) then
         write(output_unit,*) '>>>> Erreur : Pk non trouvé !!!'
         write(output_unit,*) '     Section la plus proche : ',xl(is)
         stop 14
      else
         ! entête du fichier RES
         write(lout,'(a,i3,a,f9.2,a,i0)')'* Numéro du bief: ',ib,' Pk : ',pk,' N° de section : ',is
         write(output_unit,'(a,i3,a,f9.2,a,i0)')'* Numéro du bief: ',ib,' Pk : ',pk,' N° de section : ',is
         if(type_courbe(1:1) == 'Q') then
            write(lout,'(2a)') '*  Date   ',' Débit Total   '
         elseif (type_courbe(1:1) == 'Z') then
            write(lout,'(2a)') '*  Date   ','   Cotes   '
         elseif (type_courbe(1:1) == 'Y') then
            write(lout,'(2a)') '*  Date   ',' Profondeurs  '
         elseif(type_courbe(1:1) == 'S') then
            write(lout,'(2a)') '*  Date   ',' V Moyenne   '
         elseif(type_courbe(1:1) == 'L') then
            write(lout,'(2a)') '*  Date   ',' Q Maj. Gauche '
         elseif(type_courbe(1:1) == 'M') then
            write(lout,'(2a)') '*  Date   ',' Q Mineur '
         elseif(type_courbe(1:1) == 'R') then
            write(lout,'(2a)') '*  Date   ',' Q Maj. Droit '
         elseif(type_courbe(1:1) == 'U') then
            write(lout,'(2a)') '*  Date   ',' V Maj Gauche '
         elseif(type_courbe(1:1) == 'V') then
            write(lout,'(2a)') '*  Date   ',' V Mineur   '
         elseif(type_courbe(1:1) == 'W') then
            write(lout,'(2a)') '*  Date   ',' V Maj Droit '
         elseif(type_courbe(1:1) == 'G') then
            write(lout,'(2a)') '*  Date   ',' Q échange Gauche '
         elseif(type_courbe(1:1) == 'D') then
            write(lout,'(2a)') '*  Date   ',' Q échange Droit '
         endif
         write(output_unit,*) 'lecture de ',ficres(1:len_trim(ficres)),' pour une ',type_courbe
         call lecfdt(lBin,lout,type_courbe,is,tmin,tmax)
      endif

   elseif (type_courbe(3:3) == 'X') then
      !------on trace toutes les sections du bief
      isa = is1(ib)
      isb = is2(ib)
      ! entête du fichier RES
      write(lout,'(a,i3,a,f11.4)') '* Numéro du bief : ',ib,'   date (heures): ',temps/3600.
      if(type_courbe(1:1) == 'Q') then
         write(lout,'(2a)') '*   Pk    ',' Débit Total   '
      elseif (type_courbe(1:1) == 'Z') then
         write(lout,'(2a)') '*   Pk    ','   Cotes   '
      elseif (type_courbe(1:1) == 'Y') then
         write(lout,'(2a)') '*   Pk    ',' Profondeurs  '
      elseif(type_courbe(1:1) == 'S') then
         write(lout,'(2a)') '*   Pk    ',' V Moyenne   '
      elseif(type_courbe(1:1) == 'L') then
         write(lout,'(2a)') '*   Pk    ',' Q Maj. Gauche '
      elseif(type_courbe(1:1) == 'M') then
         write(lout,'(2a)') '*   Pk    ',' Q Mineur '
      elseif(type_courbe(1:1) == 'R') then
         write(lout,'(2a)') '*   Pk    ',' Q Maj. Droit '
      elseif(type_courbe(1:1) == 'U') then
         write(lout,'(2a)') '*   Pk    ',' V Maj Gauche '
      elseif(type_courbe(1:1) == 'V') then
         write(lout,'(2a)') '*   Pk    ',' V Mineur   '
      elseif(type_courbe(1:1) == 'W') then
         write(lout,'(2a)') '*   Pk    ',' V Maj Droit '
      elseif(type_courbe(1:1) == 'G') then
         write(lout,'(2a)') '*   Pk    ',' Q échange Gauche '
      elseif(type_courbe(1:1) == 'D') then
         write(lout,'(2a)') '*   Pk    ',' Q échange Droit '
      endif
      write(output_unit,*) 'lecture de ',ficres(1:len_trim(ficres)),' pour une ',type_courbe
      call lecfdx(lBin,lout,type_courbe,isa,isb,temps)
   endif
   close(lBin)
   close(lout)
   write(output_unit,'(2a)')' Fin écriture de ',trim(ficout)
   stop

contains

subroutine capitalize(in, out)
   implicit none
   ! prototype
   character(len=*), intent(in) :: in
   character(len=*), intent(out) :: out
   ! variables locales
   integer :: offset, ic, i

   out = in
   offset = ichar('A') - ichar('a')
   !write(output_unit,*)offset
   !write(output_unit,*)ichar('A'),ichar('Z'),ichar('a'),ichar('z')
   do i = 1, len_trim(in)
      ic = ichar(in(i:i))
      !write(output_unit,*)in(i:i),ic
      if (ic >= ichar('a') .AND. ic <= ichar('z')) then
         out(i:i) = char(ic+offset)
         !write(output_unit,*)out(i:i)
      endif
   enddo
end subroutine capitalize


subroutine help()
!==============================================================================
!     affichage des options de la ligne de commande
!==============================================================================

   write(output_unit,*) 'Syntaxe de la ligne de commande de Mage_Extraire'
   write(output_unit,*) '  mage_extraire -v pour afficher le numéro de version et quitter'
   write(output_unit,*) '  mage_extraire -h pour afficher cette aide et quitter'
   write(output_unit,*) '  mage_extraire nombase type_courbe num_bief [Pk|temps]'
   write(output_unit,*) '      nombase est le nom sans extension du fichier BIN à lire'
   write(output_unit,*) '      la courbe extraite est enregistrée dans un fichier nommé <nombase>.res'
   write(output_unit,*) ''
   write(output_unit,*) '      type_courbe est le code du type de courbe à extraire :'
   write(output_unit,*) '      Il est de la forme [Q|Z|Y|S|L|M|R|U|V|W|G|D]d[T|X]'
   write(output_unit,*) '        Q -> débit total'
   write(output_unit,*) '        Z -> cote de l''eau'
   write(output_unit,*) '        Y -> profondeur d''eau'
   write(output_unit,*) '        S -> vitesse moyenne'
   write(output_unit,*) '        L -> débit dans le lit majeur gauche'
   write(output_unit,*) '        M -> débit dans le lit mineur'
   write(output_unit,*) '        R -> débit dans le lit majeur droit'
   write(output_unit,*) '        U -> vitesse moyenne du lit majeur gauche'
   write(output_unit,*) '        V -> vitesse moyenne du lit mineur'
   write(output_unit,*) '        W -> vitesse moyenne du lit majeur droit'
   write(output_unit,*) '        G -> débit d''échange mineur <-> lit majeur gauche (nul si DEBORD)'
   write(output_unit,*) '        D -> débit d''échange mineur <-> lit majeur droit (nul si DEBORD)'
   write(output_unit,*) ''
   write(output_unit,*) '      num_bief est le numéro du bief pour lequel on veut des résultats'
   write(output_unit,*) ''
   write(output_unit,*) '      Si le 3e caractère de type_courbe est T on extrait une fonction du temps '
   write(output_unit,*) '                                              et l''argument suivant doit être un Pk'
   write(output_unit,*) '      Si le 3e caractère de type_courbe est X on extrait une fonction du Pk '
   write(output_unit,*) '                                              et l''argument suivant doit être un temps'
   write(output_unit,*) '                                              l''unité de temps est donnée par l''un des'
   write(output_unit,*) '                                              préfixes j, h, m ou s accolé à la valeur fournie'
   write(output_unit,*) 'Exemple : mage_extraire Test zdx 1 h24'
   write(output_unit,*) '          pour extraire du fichier BIN indiqué dans Test.REP la ligne d''eau du bief n°1 '
   write(output_unit,*) '          à t = 24 heures'
   write(output_unit,*) ''
   write(output_unit,*) 'N.B. type_courbe est converti en capitales lors de sa lecture'
   write(output_unit,*) ''
   write(output_unit,*) 'Utiliser la commande less (or more) pour afficher l''aide page par page : mage_extraire -h | less'

end subroutine help

end program Mage_Extraire
