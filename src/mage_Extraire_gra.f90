!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - INRAE - 1989-2020                           #
!# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
!# Licence : propriétaire jusqu'à nouvel ordre                                #
!#                                                                            #
!#                                                                            #
!##############################################################################
!##############################################################################
!#                                                                            #
!#                    Programmme Mage_Extraire                                #
!#                                                                            #
!##############################################################################



program Mage_Extraire_gra
!  programme d'extraction des résultats de Mage-8 des fichiers BIN

   use data_Extraire, only: lire_Entete_GRA, carotte, ibmax, xl, is1, is2
   use, intrinsic :: iso_fortran_env, only: input_unit, error_unit, output_unit, real32, real64, int64
   use Mage_Utilitaires, only: lire_date
   implicit none
   ! -- variables locales --
   character :: fichier*32    !nom de fichier générique
   character :: nombase*18    !nom de base des fichiers
   character :: date*20       !date
   character :: ficres*32     !fichier de résultats à lire : BIN
   character :: ficout*32     !fichier à écrire
   character :: line*80
   integer :: ios, iarg
   integer :: ib, ns, lu, is, lout, lGra
   real(kind=real32) :: pk, tmin, tmax, dxmin
   real(kind=real64) :: temps
   integer(kind=int64) :: tinf
   character :: tmp*3
   character(len=120) :: argmnt

   iarg = 1 ; call get_command_argument(iarg,argmnt)
   if (len_trim(argmnt) > 0) then  !il y a des arguments sur la ligne de commande
      if (argmnt(1:2) == '-v') then
         write(output_unit,*)' Programme Mage_Extraire_gra adapté pour Mage-8'
         write(output_unit,*)' Version du 2023-09-29'
         stop
      elseif (argmnt(1:2) == '-h') then
         call help()
         stop
      else
         nombase = trim(argmnt)
         write(output_unit,'(2a)') ' Nom de base des fichiers de résultats : ',trim(nombase)
         ficout=nombase(1:len_trim(nombase))//'.res'
         iarg = iarg+1 ; call get_command_argument(iarg,argmnt)
         read(argmnt,'(i3)') ib
         write(output_unit,'(a,i3)') ' Numéro du bief : ',ib
         iarg = iarg+1 ; call get_command_argument(iarg,argmnt)
         read(argmnt,*) pk
         write(output_unit,'(a,g0)') ' Pk de la section à extraire : ',pk
         tmin = -1.E30
         tmax = +1.E30
         iarg = iarg+1 ; call get_command_argument(iarg,argmnt)
         if (argmnt(1:1) == 'j') then
            read(argmnt(2:),'(f10.0)') temps
            temps = aint(temps*86400._real64)
         elseif (argmnt(1:1) == 'h') then
            read(argmnt(2:),'(f10.0)') temps
            temps = aint(temps*3600._real64)
         elseif (argmnt(1:1) == 'm') then
            read(argmnt(2:),'(f10.0)') temps
            temps = aint(temps*60._real64)
         elseif (argmnt(1:1) == 's') then
            read(argmnt(2:),'(f10.0)') temps
         else
            read(argmnt(:),'(f10.0)') temps
         endif
         write(output_unit,'(a,g0)') ' Date de la ligne d''eau à extraire en secondes : ',temps
      endif
   else
      ! lecture des données par interrogation de l'utilisateur
      write(output_unit,'(a)') ' Programme Mage_Extraire_gra '
      write(output_unit,'(a)',advance='no') ' Nom de base des fichiers de résultats : '
      read(input_unit,'(a)') nombase
      ficout=nombase(1:len_trim(nombase))//'.res'
      write(output_unit,'(a)',advance='no') ' Numéro du bief : '
      read(input_unit,'(i3)') ib
      write(output_unit,'(a)',advance='no') ' Pk de la section à extraire : '
      read(input_unit,*) pk
      write(output_unit,'(a)',advance='no') ' Date de T0 au format AAAA:MM:JJTHH:MM:SS ou en secondes : '
      read(input_unit,'(a20)') date
      tinf = lire_date(date)
      write(output_unit,'(a)',advance='no') ' Date de la ligne d''eau à extraire en secondes : '
      read(input_unit,'(f10.0)') temps
      temps = temps + tinf
   endif

!---lecture fichier .REP pour trouver le nom du fichier GRA
   fichier=nombase(1:len_trim(nombase))//'.REP'
   open (newunit=lu,file=fichier,status='old',form='formatted',iostat=ios)
   if (ios /= 0) then
      write(error_unit,*) 'Erreur d''ouverture du fichier ',trim(fichier)
      stop 999
   else
      do
         read(lu,'(a)',iostat=ios) line
         if (ios < 0) then
            exit
         elseif (ios > 0) then
            write(output_unit,*) '>>>> ERREUR lors de la lecture du fichier '//trim(fichier)
            stop 15
         elseif (line(1:3).eq.'GRA' .or. line(1:3).eq.'gra') then
            ficres=line(5:len_trim(line))
         endif
      enddo
      close (lu)
   endif

! lecture des données générales de description
   call lire_Entete_GRA(lGra,ficres)  !lecture de l'entête de BIN
                                      !se charge de l'ouverture du fichier BIN et renvoie son unité logique
!vérifications
   if (ib > ibmax .or. ib < 1) then
      write(output_unit,'(a,i3,a)') ' Erreur : le bief ',ib,' n''existe pas'
      stop 13
   endif
! ouverture du fichier RES
   open(newunit=lout,file=ficout,status='unknown',form='formatted')

   !------on trace une carotte au PK
   is = 0
   dxmin = 1.e+30
   do ns = is1(ib), is2(ib)
      if (abs(xl(ns)-Pk) < dxmin) then
         dxmin = abs(xl(ns)-Pk)
         is = ns
      endif
   enddo
   if (is < is2(ib)) then
      if (xl(is+1) == xl(is) .and. pk > xl(is)) is = is+1 !cas d'une double section
   endif
   if (is == 0 .or. dxmin > 1.) then
      write(output_unit,*) '>>>> Erreur : Pk non trouvé !!!'
      write(output_unit,*) '     Section la plus proche : ',xl(is)
      stop 14
   else
      ! entête du fichier RES
      write(output_unit,'(a,i0,a,f9.2,a,i0,a,i0)')'* Numéro du bief: ',ib,' Pk : ',pk,' N° de section : '&
      &,is,'   date (heures): ',int(temps)/3600
      write(output_unit,*) 'lecture de ',ficres(1:len_trim(ficres))
      write(lout,'(a,i0,a,f9.2,a,i0,a,i0)')'* Numéro du bief: ',ib,' Pk : ',pk,' N° de section : '&
      &,is,'   date (heures): ',int(temps)/3600
      write(lout,'(6a)') '* ',' Point ',' Couche ',' Epaisseur ',' Diamètre ',' Sigma '
      call carotte(lGra,lout,is,temps)
   endif
   close(lGra)
   close(lout)
   write(output_unit,'(2a)')' Fin écriture de ',trim(ficout)
   stop

contains

subroutine capitalize(in, out)
   implicit none
   ! prototype
   character(len=*), intent(in) :: in
   character(len=*), intent(out) :: out
   ! variables locales
   integer :: offset, ic, i

   out = in
   offset = ichar('A') - ichar('a')
   !write(output_unit,*)offset
   !write(output_unit,*)ichar('A'),ichar('Z'),ichar('a'),ichar('z')
   do i = 1, len_trim(in)
      ic = ichar(in(i:i))
      !write(output_unit,*)in(i:i),ic
      if (ic >= ichar('a') .AND. ic <= ichar('z')) then
         out(i:i) = char(ic+offset)
         !write(output_unit,*)out(i:i)
      endif
   enddo
end subroutine capitalize


subroutine help()
!==============================================================================
!     affichage des options de la ligne de commande
!==============================================================================

   write(output_unit,*) 'Syntaxe de la ligne de commande de Mage_Extraire_gra'
   write(output_unit,*) '  mage_extraire -v pour afficher le numéro de version et quitter'
   write(output_unit,*) '  mage_extraire -h pour afficher cette aide et quitter'
   write(output_unit,*) '  mage_extraire nombase num_bief Pk temps'
   write(output_unit,*) '      nombase est le nom sans extension du fichier GRA à lire'
   write(output_unit,*) '      la courbe extraite est enregistrée dans un fichier nommé <nombase>.res'
   write(output_unit,*) ''
   write(output_unit,*) '      num_bief est le numéro du bief pour lequel on veut des résultats'
   write(output_unit,*) ''
   write(output_unit,*) '      Pk est le Pk pour lequel on veut des résultats'
   write(output_unit,*) '      temps est le temps auquel on veut des résultats'
   write(output_unit,*) '                           l''unité de temps est donnée par l''un des'
   write(output_unit,*) '                           préfixes j, h, m ou s accolé à la valeur fournie'
   write(output_unit,*) 'Exemple : mage_extraire Test 1 0 h24'
   write(output_unit,*) '          pour extraire du fichier GRA indiqué dans Test.REP les couches du bief n°1 '
   write(output_unit,*) '          au Pk 0 à t = 24 heures'
   write(output_unit,*) ''
   write(output_unit,*) 'Utiliser la commande less (or more) pour afficher l''aide page par page : mage_extraire -h | less'

end subroutine help


end program Mage_Extraire_gra
