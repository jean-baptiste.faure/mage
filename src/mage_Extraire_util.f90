!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - INRAE - 1989-2020                           #
!# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################
!##############################################################################
!#                                                                            #
!#                    Programmme Mage_Extraire                                #
!#                                                                            #
!##############################################################################

module data_Extraire
   use, intrinsic :: iso_fortran_env, only: error_unit, output_unit, real32, real64, real128
   implicit none

   !données lues dans l'entête de BIN
   integer :: ibmax, ismax, mage_version
   integer, allocatable :: is1(:), is2(:), np(:)
   real(kind=real32), allocatable :: xl(:), zfd(:), ygeo(:), ybas(:)
   procedure (leccarottes), pointer :: carotte => null ()

contains

subroutine find_bin(nombase, bin)
!lecture fichier .REP pour trouver le nom du fichier BIN
   character, intent(in) :: nombase*18    !nom de base des fichiers
   character, intent(out) :: bin*32     !fichier de résultats à lire : BIN
   character  :: rep*32    !nom de fichier générique
   character :: line*80
   integer :: ios, lu
   rep=nombase(1:len_trim(nombase))//'.REP'
   open (newunit=lu,file=rep,status='old',form='formatted',iostat=ios)
   if (ios /= 0) then
      write(error_unit,*) 'Erreur d''ouverture du fichier ',trim(rep)
      stop 999
   else
      do
         read(lu,'(a)',iostat=ios) line
         if (ios < 0) then
            exit
         elseif (ios > 0) then
            write(output_unit,*) '>>>> ERREUR lors de la lecture du fichier '//trim(rep)
            stop 15
         elseif (line(1:3).eq.'BIN' .or. line(1:3).eq.'bin') then
            bin=line(5:len_trim(line))
         endif
      enddo
      close (lu)
   endif
end subroutine find_bin

subroutine lire_Entete_BIN(lu,BIN_file)
!lecture de la partie déclarative de BIN
   implicit none
   ! -- prototype --
   character(len=32),intent(in) :: BIN_file  !fichier BIN
   integer, intent(out) :: lu
   ! -- variables locales --
   integer :: ib, is, ios
!
   open(newunit=lu,file=trim(BIN_file),status='old',form='unformatted',iostat=ios)
   if (ios > 0) then
      write(error_unit,*) '>>>> Erreur d''ouverture du fichier ',trim(BIN_file)
      stop 998
   endif
!
   read(lu) ibmax, ismax, mage_version
   if (mage_version <= 80) then
      ! on a un fichier BIN avec l'ancien entête
      write(error_unit,*) '>>>> ERREUR : le fichier ',trim(BIN_file),' a été créé par une version plus ancienne de Mage'
      write(error_unit,*) '     Lecture impossible'
      stop 1
   endif

   ! allocation des tableaux
   allocate (is1(ibmax), is2(ibmax))
   allocate (xl(ismax+1), zfd(ismax), ygeo(ismax),ybas(ismax))
   ! suite de la lecture de l'entête
   read(lu) (is1(ib),is2(ib),ib=1,ibmax)
   read(lu) (xl(is), is=1,ismax)
   xl(ismax+1) = xl(ismax)
   read(lu) (zfd(is),ygeo(is),ybas(is),is=1,ismax)

   write(output_unit,*) '--> fin de la lecture de l''entete de ',trim(BIN_file)
end subroutine lire_Entete_BIN

subroutine lire_Entete_GRA(lu,GRA_file)
!lecture de la partie déclarative de BIN
   implicit none
   ! -- prototype --
   character(len=32),intent(in) :: GRA_file  !fichier GRA
   integer, intent(out) :: lu
   ! -- variables locales --
   integer :: ib, is, ios
!
   open(newunit=lu,file=trim(GRA_file),status='old',form='unformatted',iostat=ios)
   if (ios > 0) then
      write(error_unit,*) '>>>> Erreur d''ouverture du fichier ',trim(GRA_file)
      stop 998
   endif
!
   read(lu) ibmax, ismax, mage_version
   if (mage_version <= 80) then
      ! on a un fichier GRA avec l'ancien entête
      write(error_unit,*) '>>>> ERREUR : le fichier ',trim(GRA_file),' a été créé par une version plus ancienne de Mage'
      write(error_unit,*) '     Lecture impossible'
      stop 1
   else if (mage_version <= 82) then
      carotte => leccarottes
   else
      carotte => leccarottes2
   endif

   ! allocation des tableaux
   allocate (is1(ibmax), is2(ibmax))
   allocate (xl(ismax))
!    allocate (zfd(ismax), ygeo(ismax),ybas(ismax))
   allocate (np(ismax))
   ! suite de la lecture de l'entête
   read(lu) (is1(ib),is2(ib),ib=1,ibmax)
   read(lu) (xl(is), is=1,ismax)
!    read(lu) (zfd(is),ygeo(is),ybas(is),is=1,ismax)
   read(lu) (np(is), is=1,ismax)

   write(output_unit,*) '--> fin de la lecture de l''entete de ',trim(GRA_file)
end subroutine lire_Entete_GRA


subroutine lecfdx(lu,lout,type_courbe,isa,isb,temps)
!  lecture des données d'un fichier BIN pour une courbe F(x)
   implicit none
   ! -- prototype --
   integer, intent(in) :: lu  !unité logique du fichier BIN
   integer, intent(in) :: lout  !unité logique du fichier RES
   character, intent(in) :: type_courbe*3
   integer, intent(in) :: isa, isb
   real(kind=real64), intent(in) :: temps
   ! -- variables locales --
   integer :: is, npts, ios, ndes, ntmp
   character :: a*1, variable*1
   real(kind=real32) :: tinf, tsup
   real(kind=real32), allocatable :: xdes(:), ydes(:), y(:), yinf(:), ysup(:)
   real(kind=real64) :: t
   logical :: is_Y

   a = '&'
   t = -1.e+30 ; tinf = t ; tsup = t
   ntmp = isb-isa+1
   variable = type_courbe(1:1)
   if (variable == 'Y') then
      is_Y = .true.
      variable = 'Z'
   else
      is_Y = .false.
   endif
   allocate (y(ismax), yinf(ismax), ysup(ismax))
   allocate (xdes(ntmp), ydes(ntmp),stat=ios)
   if (ios > 0) then
      write(error_unit,*) '>>> Erreur dans l''allocation pour xdes ou ydes : ',ios, ntmp
      stop 19
   endif
! encadrement
   do
      read(lu,iostat=ios) npts, t, a, (yinf(is),is=1,npts)
      if (npts /= ismax) stop 16
      if (ios < 0) then
         write(output_unit,'(3a,g0,a)') 'Pas de données suffisantes pour ',type_courbe,' à T = ',temps,' secondes'
         write(output_unit,'(a,g0,a)') 'Dernière date trouvée : ',t,' secondes'
         stop 17
      endif
      if (a == variable) exit
   enddo
   if (temps < t-0.5) then
      write(output_unit,'(3a,g0,a)') 'Pas de données suffisantes pour ',type_courbe,' à T = ',temps,' secondes'
      write(output_unit,'(a,g0,a)') 'Première date trouvée : ',t,' secondes'
      stop 18
   elseif (abs(t-temps) < 0.5) then  !première ligne d'eau
      if (xl(isa) > xl(isb)) then
         do is = isa, isb
            ndes = isb-is+1
            xdes(ndes) = xl(is)
            ydes(ndes) = yinf(is)
         enddo
      else
         do is = isa, isb
            ndes = is-isa+1
            xdes(ndes) = xl(is)
            ydes(ndes) = yinf(is)
         enddo
      endif
   else
      do                         !lignes d'eau suivantes
         read(lu,iostat=ios) npts, t, a, (y(is),is=1,npts)
         if (ios < 0) then
            write(output_unit,'(3a,g0,a)') 'Pas de données suffisantes pour ',type_courbe,' à T = ',temps,' secondes'
            write(output_unit,'(a,g0,a)') 'Dernière date trouvée : ',t,' secondes'
            stop 17
         endif
         if (a /= variable) cycle
         if (t < temps) then
            tinf = t
            do is = isa, isb
               yinf(is) = y(is)
            enddo
         else
            tsup = t
            do is = isa, isb
               ysup(is) = y(is)
            enddo
            exit
         endif
      enddo
   ! interpolation linéaire avec réorganisation dans le sens des pk croissants
      if (xl(isa) > xl(isb)) then
         do is = isa, isb
            ndes = isb-is+1
            xdes(ndes) = xl(is)
            ydes(ndes) = yinf(is) + (temps-tinf)/(tsup-tinf)*(ysup(is)-yinf(is))
         enddo
      else
         do is = isa, isb
            ndes = is-isa+1
            xdes(ndes) = xl(is)
            ydes(ndes) = yinf(is) + (temps-tinf)/(tsup-tinf)*(ysup(is)-yinf(is))
         enddo
      endif
   endif
   ndes = isb-isa+1
   !ecriture sur RES
   if (tsup-temps > 1.) then
      write(output_unit,'(2(a,g0),a)') ' >>>> Données interpolées entre ',tinf,' secondes et ',tsup,' secondes'
      write(lout,'(2(a,g0),a)') '*Données interpolées entre ',tinf,' secondes et ',tsup,' secondes'
   endif
   if (is_Y) then
      do is = 1, ndes
         ydes(is) = ydes(is) - zfd(isa+is-1)
      enddo
   endif
   do is = 1, ndes
      write(lout,'(2(es14.6,1x))') xdes(is),ydes(is)
   enddo
   deallocate (xdes,ydes,y,yinf,ysup)

end subroutine lecfdx


subroutine lecfdt(lin,lout,type_courbe,is,tmin,tmax)
!  lecture des donnees d'un fichier BINpour une courbe F(t)
   implicit none
   ! -- prototype --
   integer, intent(in) :: lin   !unité logique du fichier BIN
   integer, intent(in) :: lout  !unité logique du fichier RES
   character, intent(in) :: type_courbe*3
   integer, intent(in) :: is
   real(kind=real32), intent(in) :: tmin, tmax
!variables locales
   integer :: ndes
   integer :: i, npts, nmax, dnmax, ios
   real(kind=real32), allocatable :: y(:)
   real(kind=real64) :: t, vol
   real(kind=real64), allocatable :: xbak(:), ybak(:), xdes(:), ydes(:)
   character :: a*1, variable*1
   logical :: is_Y

   ndes = 0
   t = -1.e+30
   nmax = 1000 ; dnmax = 1000
   variable = type_courbe(1:1)
   if (variable == 'Y') then
      is_Y = .true.
      variable = 'Z'
   else
      is_Y = .false.
   endif
   allocate (xdes(nmax),stat=ios) ; if (ios>0) stop 001
   allocate (ydes(nmax),stat=ios) ; if (ios>0) stop 002
   allocate (y(ismax),stat=ios) ; if (ios>0) stop 003

! traitement
   do while (t <= tmax)
      read(lin,iostat=ios) npts,t,a,(y(i),i=1,npts)
      if (ios < 0) exit ! fin du fichier
      if (a /= variable) cycle
      if (t < tmin) cycle
      ndes = ndes+1
      if (ndes > nmax) then  ! allocation mémoire
         allocate (xbak(nmax),stat=ios) ; if (ios>0) stop 003
         xbak = xdes
         allocate (ybak(nmax),stat=ios) ; if (ios>0) stop 004
         ybak = ydes
         deallocate(xdes,stat=ios) ; deallocate(ydes,stat=ios)
         nmax = nmax+dnmax
         allocate (xdes(nmax),stat=ios) ; if (ios>0) stop 005
         xdes(1:nmax-dnmax) = xbak(1:nmax-dnmax)
         allocate (ydes(nmax),stat=ios) ; if (ios>0) stop 006
         ydes(1:nmax-dnmax) = ybak(1:nmax-dnmax)
         deallocate(xbak,stat=ios) ; deallocate(ybak,stat=ios)
      endif
      xdes(ndes) = t
      ydes(ndes) = y(is)
   enddo
   write(output_unit,*)'Fin de lecture pour ',type_courbe
   write(lout,'(a,f10.3)') '* Pk = ',xl(is)
   if(variable == 'Q') then
      vol = volhyd(xdes,ydes,ndes)
      write(output_unit,'(a,g0)') ' >>>> Volume de l'' hydrogramme : ',vol
      write(lout,'(a,g0)') '* Volume hydrogramme : ',vol
      write(lout,'(2a)') '*  Date   ','   Débits   '
   endif

   xdes = xdes/3600._real64
#ifdef simplin
   alpha = 0.00001_real64
   if(variable == 'Z') alpha = alpha*sqrt(alpha)
   print*,'simplification linéaire avec alpha = ',alpha
   call simplification_lineaire(alpha,xdes,ydes,ndes)
#endif /* simplin */

   if (is_Y) then
      do i = 1, ndes
         ydes(i) = ydes(i) - zfd(is)
      enddo
   endif
   do i = 1, ndes
      write(lout,'(2(es14.6,1x))') xdes(i),ydes(i)
    enddo
    deallocate (xdes,ydes)
end subroutine lecfdt


subroutine leccarottes(lin,lout,section,temps)
!  lecture des donnees d'un fichier GRA
   use, intrinsic :: iso_fortran_env, only: real64, real32
   implicit none
   ! -- prototype --
   integer, intent(in) :: lin   !unité logique du fichier GRA
   integer, intent(in) :: lout  !unité logique du fichier RES
   integer, intent(in) :: section
   real(kind=real64), intent(inout) :: temps
!variables locales
   integer :: ns, ncs, ncsmax, j, k, is, ios, nzone
   real(kind=real64) :: tr, previoustr
   real(kind=real32) :: tinf, tsup
   real(kind=real64),allocatable :: h(:,:), d50(:,:), sigma(:,:)

   tr = -1.e+30 ; tinf = tr ; tsup = tr
   ! premier temps
   read(lin,iostat=ios) ns,tr
   previoustr = tr
   if (ios < 0) then
      write(output_unit,'(a,g0,a)') 'Pas de données suffisantes pour T = ',temps,' secondes'
      write(output_unit,'(a,g0,a)') 'Dernière date trouvée : ',tr,' secondes'
      stop 17
   endif
   if (temps < tr-0.5) then
      write(output_unit,'(a,g0,a)') 'Pas de données suffisantes pour T = ',temps,' secondes'
      write(output_unit,'(a,g0,a)') 'Première date trouvée : ',tr,' secondes'
      stop 18
   endif
   ncsmax=10
   do is=1, ns
      read(lin) nzone
      if (section == is) then
         allocate(h(nzone,ncsmax), d50(nzone,ncsmax), sigma(nzone,ncsmax))
         do j = 1, nzone
            read(lin) ncs
            do k = 1, ncs
              read(lin) h(j,k), d50(j,k), sigma(j,k)
            enddo
         enddo
      else
         do j = 1, nzone
            read(lin) ncs
            do k = 1, ncs
              read(lin)
            enddo
         enddo
      endif
   enddo
   if (abs(tr-temps) > 0.5) then  ! pas la première ligne d'eau
      ! temps suivants
      do
         read(lin,iostat=ios) ns,tr
         if (ios < 0) then
            if (previoustr < temps + 0.5) exit
            write(output_unit,'(a,g0,a)') 'Pas de données suffisantes pour T = ',temps,' secondes'
            write(output_unit,'(a,g0,a)') 'Dernière date trouvée : ',tr,' secondes'
            stop 17
         endif
         if (tr > temps + 0.5) exit
         previoustr = tr
         deallocate(h, d50, sigma)
         do is=1, ns
            read(lin) nzone
            if (section == is) then
               allocate(h(nzone,ncsmax), d50(nzone,ncsmax), sigma(nzone,ncsmax))
               do j = 1, nzone
                   read(lin) ncs
                   do k = 1, ncs
                     read(lin) h(j,k), d50(j,k), sigma(j,k)
                   enddo
               enddo
            else
               do j = 1, nzone
                   read(lin) ncs
                   do k = 1, ncs
                     read(lin)
                   enddo
               enddo
            endif
         enddo
      enddo
   endif
   print*, nzone, ncs
   do j = 1, nzone
      do k = 1, ncs
         write(*,*) j, k, h(j,k), d50(j,k), sigma(j,k)
         write(lout,*) j, k, h(j,k), d50(j,k), sigma(j,k)
      enddo
   enddo

   print*, "lecture OK"
end subroutine leccarottes


subroutine leccarottes2(lin,lout,section,temps)
!  lecture des donnees d'un fichier GRA
   use, intrinsic :: iso_fortran_env, only: real64, real32
   implicit none
   ! -- prototype --
   integer, intent(in) :: lin   !unité logique du fichier GRA
   integer, intent(in) :: lout  !unité logique du fichier RES
   integer, intent(in) :: section
   real(kind=real64), intent(inout) :: temps
!variables locales
   integer :: ns, ncsmax, npmax, i, j, k, is, ios, with_charriage
   real(kind=real64) :: tr, previoustr
   real(kind=real32) :: tinf, tsup
   integer,allocatable :: np(:), nbcs(:,:)
   real(kind=real64),allocatable :: h(:,:,:), d50(:,:,:), sigma(:,:,:)

   tr = -1.e+30 ; tinf = tr ; tsup = tr
   ! premier temps
   read(lin,iostat=ios) ns,tr,with_charriage
   previoustr = tr
   if (ios < 0) then
      write(output_unit,'(a,g0,a)') 'Pas de données suffisantes pour T = ',temps,' secondes'
      write(output_unit,'(a,g0,a)') 'Dernière date trouvée : ',tr,' secondes'
      stop 17
   endif
   if (temps < tr-0.5) then
      write(output_unit,'(a,g0,a)') 'Pas de données suffisantes pour T = ',temps,' secondes'
      write(output_unit,'(a,g0,a)') 'Première date trouvée : ',tr,' secondes'
      stop 18
   endif
   if (with_charriage == 1) then ! aux noeuds
         allocate(np(ns))
         read(lin) (np(i), i=1, ns)
         npmax = maxval(np)
         allocate(nbcs(npmax,ns))
         read(lin) ((nbcs(i,j), i=1, np(j)), j=1, ns)
         ncsmax = maxval(nbcs)
         allocate(h(ncsmax,npmax,ns), d50(ncsmax,npmax,ns), sigma(ncsmax,npmax,ns))
         read(lin) (((h(i,j,k), d50(i,j,k), sigma(i,j,k), i=1, nbcs(j,k)), &
                                                          j=1, np(k)), &
                                                          k=1, ns)
   else ! aux sections
         allocate(np(ns))
         np(:) = 1
         allocate(nbcs(1,ns))
         read(lin) (nbcs(1,i), i=1, ns)
         ncsmax = maxval(nbcs)
         allocate(h(ncsmax,1,ns), d50(ncsmax,1,ns), sigma(ncsmax,1,ns))
         read(lin) ((h(i,1,j), d50(i,1,j), sigma(i,1,j), i=1, nbcs(1,j)), &
                                                         j=1, ns)
   endif

   if (abs(tr-temps) > 0.5) then  ! pas la première ligne d'eau
      ! temps suivants
      do
         read(lin,iostat=ios) ns,tr,with_charriage
         if (ios < 0) then
            if (previoustr < temps + 0.5) exit
            write(output_unit,'(a,g0,a)') 'Pas de données suffisantes pour T = ',temps,' secondes'
            write(output_unit,'(a,g0,a)') 'Dernière date trouvée : ',tr,' secondes'
            stop 17
         endif
         if (tr > temps + 0.5) exit
         previoustr = tr
         deallocate(h, d50, sigma, np, nbcs)
         if (with_charriage == 1) then ! aux noeuds
               allocate(np(ns))
               read(lin) (np(i), i=1, ns)
               npmax = maxval(np)
               allocate(nbcs(npmax,ns))
               read(lin) ((nbcs(i,j), i=1, np(j)), j=1, ns)
               ncsmax = maxval(nbcs)
               allocate(h(ncsmax,npmax,ns), d50(ncsmax,npmax,ns), sigma(ncsmax,npmax,ns))
               read(lin) (((h(i,j,k), d50(i,j,k), sigma(i,j,k), i=1, nbcs(j,k)), &
                                                                j=1, np(k)), &
                                                                k=1, ns)
         else ! aux sections
               allocate(np(ns))
               np(:) = 1
               allocate(nbcs(1,ns))
               read(lin) (nbcs(1,i), i=1, ns)
               ncsmax = maxval(nbcs)
               allocate(h(ncsmax,1,ns), d50(ncsmax,1,ns), sigma(ncsmax,1,ns))
               read(lin) ((h(i,1,j), d50(i,1,j), sigma(i,1,j), i=1, nbcs(1,j)), &
                                                               j=1, ns)
         endif
      enddo
   endif
   do i = 1, np(section)
      do j = 1, nbcs(i,section)
         write(*,*) i, j, h(j,i,section), d50(j,i,section), sigma(j,i,section)
         write(lout,*) i, j, h(j,i,section), d50(j,i,section), sigma(j,i,section)
      enddo
   enddo

   print*, "lecture OK"
end subroutine leccarottes2


#ifdef simplin
subroutine simplification_lineaire(alpha, x, y, npt)
   implicit none
   ! -- prototype --
   integer,intent(inout) :: npt
   real(kind=real64),intent(in) :: alpha
   real(kind=real64),intent(inout) :: x(:), y(:)
   ! -- variables locales --
   integer :: i, j, k, n, n_sup
   real(kind=real64) :: htmin, ht

   htmin = 1.e+30
   do
      i = 1  ;  j = 2  ;  k = 3  ;  n = 1  ;  n_sup = 0
      do while (k <= npt)
         ht = hauteur_triangle(x(i),y(i),x(j),y(j),x(k),y(k))
         htmin = min(htmin,ht)
         if (ht < alpha) then
            ! on oublie le point j
            j = k  ;  k = k+1  ;  n_sup = n_sup+1
         else
            ! on garde le point j
            n = n+1
            if (n > j) stop ">>>> Erreur dans simplification_lineaire()"
            x(n) = x(j)  ;  y(n) = y(j)
            i = j  ;  j = j+1  ;  k = k+1
         endif
      enddo
      n = n+1
      x(n) = x(npt)  ;  y(n) = y(npt)
      write(output_unit,'(a,i0,a,i0,2x,i0,a)') ' Nombre de points à lisser : ',npt,' -- Gain : ',n_sup, n_sup*100/npt,'%'
      if (n_sup < 0.01*npt) then
         npt = n  ;  exit
      else
         npt = n
      endif
   enddo
   print*,' Hauteur triangle minimale : ',htmin
   print*,' Nombre de points conservés : ',npt
end subroutine simplification_lineaire


function hauteur_triangle(x1,y1,x2,y2,x3,y3)
   implicit none
   ! -- prototype --
   real(kind=real64) :: hauteur_triangle
   real(kind=real64),intent(in) :: x1,y1,x2,y2,x3,y3
   ! -- variables locales --
   real(kind=real128) :: a, b, c, x14 ,y14 ,x24 ,y24 ,x34 ,y34

   x14 = real(x1,kind=q4)  ;  y14 = real(y1,kind=q4)
   x24 = real(x2,kind=q4)  ;  y24 = real(y2,kind=q4)
   x34 = real(x3,kind=q4)  ;  y34 = real(y3,kind=q4)

   a = sqrt((x24-x14)**2+(y14-y24)**2)
   b = sqrt((x34-x24)**2+(y24-y34)**2)
   c = sqrt((x34-x14)**2+(y14-y34)**2)
   hauteur_triangle = real((a+b-c)/c,kind=real64)
end function hauteur_triangle
#endif /* simplin */


function volhyd(xdes,ydes,ndes)
!  procédure de calcul du volume d'un hydrogramme
!     (temps en heures volume calculé en m3)
   implicit none
   ! -- prototype --
   integer,intent(in) :: ndes
   real(kind=real64),intent(in) :: xdes(*), ydes(*)
   real(kind=real64) ::  volhyd
   ! -- variable locale --
   integer :: n

   volhyd = 0._real64
   do n = 1, ndes-1
      volhyd = volhyd + 0.5_real64*(ydes(n)+ydes(n+1))*(xdes(n+1)-xdes(n))
   enddo
end function volhyd

end module data_Extraire
