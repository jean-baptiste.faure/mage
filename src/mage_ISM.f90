!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - INRAE - 1989-2020                           #
!# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################
!
module StVenant_ISM
!===============================================================================
!  Implémentation de ISM (Independent Subsections Method) alternative à la
!  méthode Debord traditionnelle.
!===============================================================================

use parametres, only: long, zero, un, g, l9, lTra, nomini_ini, i_ism
use, intrinsic :: iso_fortran_env, only: error_unit, output_unit
#ifdef openmp
use omp_lib
#endif /* openmp */

use basic_Types, only: debits_ISM
use objet_section, only: profil
use TopoGeometrie, only: la_topo, numero_bief, &
                         xgeo, zfd, zfond, point3D
use Data_Num_Fixes, only: theta, plus, nb_cpu
use Data_Num_Mobiles, only: t,dt
use Conditions_Limites, only: cl, cl0_ISM
use Mage_Utilitaires, only: is_NaN, is_zero, zegal, LU_factorisation, LU_solution, &
                            solve_systlin, sgefa_c_omp, sgesl, block_tridiag, produit_matrice_inverse, permut_4123, heure

! dans les matrices suivantes le 1er indice est celui des lignes, le 2e celui des colonnes
! Attention : en Fortran c'est le 1er indice qui tourne le + vite
! Pour les produits de matrice utiliser matmul()
real(kind=long), allocatable, dimension(:,:,:) :: CLM  !C.L. amont, 3 lignes et 4 colonnes, indexé par les n° de bief
real(kind=long), allocatable, dimension(:,:) :: TM     !C.L amont, second membre, vecteur colonne 3 lignes
real(kind=long), allocatable, dimension(:,:,:) :: A, B, E
! A et B sont les matrices 4x4 discrétisées brutes, E est la matrices B après transformation de A
real(kind=long), allocatable, dimension(:,:) :: C, F, C0
! C est le second membre discrétisé brut, F le second membre après transformation de A
! C1 est la sauvegarde de C à la 1ère itération de N-R, nécessaire pour calculer les corrections à ajouter à C.
real(kind=long), allocatable, dimension(:,:) :: CLV !C.L. aval (vecteur ligne 4 colonnes)
real(kind=long), allocatable, dimension(:) :: TV    !C.L. aval, second membre

real(kind=long) :: DeuxThetaDt
logical, parameter :: with_mass=.true. , with_turbulence=.true. , with_qlat=.false.
logical :: with_trace_full=.false. , with_trace_small = .false.
logical, parameter :: double_balayage=.false.
logical :: first_iteration
real(kind=long) :: weight_Vint
integer, parameter :: LEFT = 1
integer, parameter :: RIGHT = 2

real(kind=long), allocatable, dimension(:),target  :: Z       !cote au temps t-dt
real(kind=long), allocatable, dimension(:),target  :: dZ, dZ0, dz_old      !variation de Z entre t-dt et t
! dz et dz0 sont utilisés pour les itérations au cours d'un pas de temps
! dz_old est la solution trouvée au pas de temps précédent, c'est cette valeur qu'il faut retrancher quand on revient en
! arrière en cas de problème (mauvaise discrétisation) ; à ne pas confondre avec l'annulation du pas de temps
real(kind=long), allocatable, dimension(:) :: qml, qmr   !débits d'échange lits moyens -> lit mineur

!type debits_ISM
!   !triplet de débits mineur, moyen gauche et moyen droit
!   real(kind=long) :: Qm, Ql, Qr
!end type debits_ISM
type(debits_ISM), allocatable, dimension(:) :: Q       !débit au temps t-dt
type(debits_ISM), allocatable, dimension(:) :: dQ, dQ0, dq_old      !variation de Q entre t-dt et t
! dq et dq0 sont utilisés pour les itérations au cours d'un pas de temps
! dq_old est la solution trouvée au pas de temps précédent, c'est cette valeur qu'il faut retrancher quand on revient en
! arrière en cas de problème (mauvaise discrétisation) ; à ne pas confondre avec l'annulation du pas de temps
type(debits_ISM), allocatable, dimension(:) :: Qa      !débits d'apports distribués
type(debits_ISM), allocatable, dimension(:) :: dQa     !variations des débits d'apports distribués entre t-dt et t
type(debits_ISM) :: q_zero

type hydraulic_data
   !grandeurs hydrauliques pour un profil donné (x=m,l,r)
   ! lx  -> largeurs au miroir
   ! sx  -> sections mouillées
   ! px  -> périmètres mouillés
   ! vx  -> vitesses moyennes
   ! dex -> débitances
   ! jx  -> pertes de charge linéaires
   ! jsx -> pertes de charge singulières
   ! dpx -> dp/dz
   ! lt -> largeur totale
   ! st -> section totale
   ! hpb -> profondeur plein bord
   real(kind=long) :: lm, ll, lr, sm, sl, sr, pm, pl, pr, vm, vl, vr, jm, jl, jr, jsm, jsl, jsr
   real(kind=long) :: dpm, dpl, dpr, hl, hr, lt, st, hpb, jdm, jdl, jdr
   integer :: irg, ird
   logical :: bFail
end type hydraulic_data
type(hydraulic_data), allocatable, dimension(:) :: hd0  !données hydrauliques à l'instant t
type(hydraulic_data), allocatable, dimension(:), target :: hd1  !données hydrauliques à l'instant t+dt

interface operator(+)
   module procedure add
   module procedure add1
end interface

interface operator(-)
   module procedure substract
   module procedure substract1
end interface

procedure(V_interface_upstream), pointer :: V_interface => null()

contains

pure function add(q1,q2)
!fait la somme de 2 debits_ISM
   implicit none
   ! -- prototype --
   type(debits_ISM), intent(in) :: q1, q2
   type(debits_ISM)             :: add

   add%qm = q1%qm+q2%qm
   add%ql = q1%ql+q2%ql
   add%qr = q1%qr+q2%qr
end function add



pure function add1(q1,q2)
!fait la somme de 2 debits_ISM ; il vaut mieux que les 2 tableaux soient de même taille
!                                sinon on se base sur celle du plus petit
   implicit none
   ! -- prototype --
   type(debits_ISM),dimension(:),intent(in) :: q1, q2
   type(debits_ISM),dimension(min(size(q1),size(q2))) :: add1
   ! -- variables --
   integer :: is

   do is = 1, min(size(q1),size(q2))
      add1(is) = q1(is) + q2(is)
   enddo
end function add1



pure function substract(q1,q2)
!fait la différence de 2 debits_ISM
   implicit none
   ! -- prototype --
   type(debits_ISM), intent(in) :: q1, q2
   type(debits_ISM)             :: substract

   substract%qm = q1%qm-q2%qm
   substract%ql = q1%ql-q2%ql
   substract%qr = q1%qr-q2%qr
end function substract



pure function substract1(q1,q2)
!fait la différence de 2 tableaux de debits_ISM
   implicit none
   ! -- prototype --
   type(debits_ISM),dimension(:),intent(in) :: q1, q2
   type(debits_ISM),dimension(min(size(q1),size(q2))) :: substract1
   ! -- variables --
   integer :: is

   do is = 1, min(size(q1),size(q2))
      substract1(is) = q1(is) - q2(is)
   enddo
end function substract1



subroutine initialize_ISM
!allocation des matrices
   implicit none
   ! -- variables --
   integer :: ismax, ibmax

   ibmax = la_topo%net%nb
   allocate (CLM(3,4,ibmax),TM(3,ibmax))
   allocate (CLV(4,ibmax),TV(ibmax))

   ismax = la_topo%net%ns
   allocate (A(4,4,ismax),B(4,4,ismax),E(4,4,ismax))
   allocate (C(4,ismax),C0(4,ismax),F(4,ismax))
   allocate (q(ismax),dq(ismax),dq0(ismax),dq_old(ismax),z(ismax),dz(ismax),dz0(ismax),dz_old(ismax))
   allocate (qml(ismax),qmr(ismax))
   allocate (qa(ismax),dqa(ismax))
   allocate (hd0(ismax),hd1(ismax))

   q_zero%ql = zero ; q_zero%qm = zero ; q_zero%qr = zero
   !initialisation des tableaux à 0
   CLM = zero ; tm = zero
   CLV = zero ; tv = zero
   a = zero ; b = zero ; e = zero
   c = zero ; c0 = zero ; f = zero
   z = zero ; dz = zero ; dz0 = zero ; dz_old = zero
   !autres initialisations
   q = q_zero
   dq = q_zero
   dq0 = q_zero
   dq_old = q_zero
   qa = q_zero
   dqa = q_zero
   qml = zero
   qmr = zero
end subroutine initialize_ISM



pure function Q_total(q)
! débit total dans la section
   implicit none
   ! -- prototype
   type(debits_ISM), intent(in) :: q
   real(kind=long) :: Q_total

   Q_total = q%qm+q%ql+q%qr
end function Q_total



pure function S_total(hd)
! section mouillée totale dans la section
   implicit none
   ! -- prototype
   type(hydraulic_data), intent(in) :: hd
   real(kind=long) :: S_total

   S_total = hd%sm+hd%sl+hd%sr
end function S_total



subroutine compute_hydraulic_data(hd,is,z,q)
!remplit hd avec les données hydrauliques du profil is pour un couple (z,q)
   use parametres, only: gravite => g, lErr
   use data_num_mobiles, only: cr, iscr, ifrsup, frsup, dx
   implicit none
   ! -- prototype
   type(hydraulic_data),intent(out) :: hd
   integer,intent(in) :: is
   real(kind=long),intent(in) :: z
   type(debits_ISM),intent(in) :: q
   ! -- variables
   real(kind=long) :: dbtc
   real(kind=long) :: froude, qtotal, cris
   real(kind=long),parameter :: eps = 0.005_long, e23 = 2._long/3._long
   type(profil), pointer :: prof
   character(len=180) :: err_message
   class(point3D), pointer :: p3D(:)
   real(kind=long), parameter :: v_sup = 10._long

   if (is >0 .and. is <= la_topo%net%ns) then
      prof => la_topo%sections(is)
      p3D(1:) => prof%xyz(:)
   else
      write(error_unit,*) ' >>>> ERREUR : indice hors limites : ',is, la_topo%net%ns
      stop 4
   endif

   if (prof%main /= 2) then
      write(error_unit,*) ' >>>> ERREUR : le profil ',prof%name,' est incompatible avec ISM'
      stop 4
   endif

   !Lit mineur
   hd%lm = prof%largeur(z,2)
   hd%bFail = hd%lm < zero
   hd%sm = prof%section_mouillee(z,2)
   hd%bFail = hd%bFail .or. hd%sm < zero
   hd%pm = prof%perimetre(z,2)
   hd%bFail = hd%bFail .or. hd%pm < zero

   if (hd%bFail) then
      if (z < prof%zf) then
         write(l9,*) '!!!! compute_hydraulic_data ERREUR : tirant d''eau négatif au pk ',prof%pk, &
                    ' du bief ',numero_bief(is)
      else
         write(l9,*) '!!!! compute_hydraulic_data ERREUR : ',is, prof%pk, prof%zf, z, hd%lm, hd%sm, hd%pm
      endif
      return
   endif
   hd%dpm = 0.5_long * (prof%perimetre(z+eps,2) - prof%perimetre(z-eps,2)) / eps
   hd%vm = q%qm / hd%sm
   hd%bFail = hd%bFail .or. (abs(hd%vm) > v_sup)
   dbtc = prof%Ks(2) * hd%sm * (hd%sm/hd%pm)**e23
   hd%jm = q%qm * abs(q%qm) / (dbtc**2)
   !drag force
   hd%jdm = prof%drag_force(2) * hd%vm * abs(hd%vm)
   ! profondeur de plein bord
   hd%hpb = prof%ymoy
   !Lit moyen gauche
   hd%irg = prof%li(1) !limite moyen gauche - mineur
   hd%hl = max(zero,z-p3D(hd%irg)%z)
   if (hd%hl > zero) then
      hd%ll = prof%largeur(z,1)
      hd%sl = prof%section_mouillee(z,1) - prof%smaj_left
      hd%pl = prof%perimetre(z,1)
      if (is_zero(hd%ll) .and. is_zero(hd%sl)) then
         print*,prof%name,prof%pk,prof%zf,z, hd%ll, hd%pl
         write(output_unit,*) '>>>> ERREUR : la limite du lit majeur gauche est placé au pied d''une', &
                              ' paroi verticale, placez la en haut'
         write(output_unit,*) prof%name,prof%pk,p3D(hd%irg)%z
         stop '>>>> Erreur de définition des limites mineur-majeur'
      endif
      if (is_zero(hd%pl)) stop '>>>> Erreur 1b dans compute_hydraulic_data()'
      hd%dpl = 0.5_long * (prof%perimetre(z+eps,1) - prof%perimetre(z-eps,1)) / eps
      hd%vl = q%ql / hd%sl
      dbtc = prof%Ks(1) * hd%sl * (hd%sl/hd%pl)**e23
      hd%jl = q%ql * abs(q%ql) / (dbtc**2)
      !drag force
      hd%jdl = prof%drag_force(1) * hd%vl * abs(hd%vl)
   else
      hd%ll = zero   ;  hd%sl = zero  ;  hd%pl = zero
      hd%dpl = zero  ;  hd%vl = zero  ;  hd%jl = zero
      hd%jdl = zero
   endif
   !Lit moyen droit
   hd%ird = prof%li(2) !limite mineur - moyen droit
   hd%hr = max(zero,z-p3D(hd%ird)%z)
   if (hd%hr > zero) then
      hd%lr = prof%largeur(z,3)
      hd%sr = prof%section_mouillee(z,3) - prof%smaj_right
      hd%pr = prof%perimetre(z,3)
      if (is_zero(hd%lr) .and. is_zero(hd%sr)) then
         print*,prof%name,prof%pk,prof%zf,z, hd%lr, hd%pr
         write(output_unit,*) '>>>> ERREUR : la limite du lit majeur droit est placé au pied d''une', &
                              ' paroi verticale, placez la en haut'
         write(output_unit,*) prof%name,prof%pk,p3D(hd%ird)%z
         stop '>>>> Erreur de définition des limites mineur-majeur'
      endif
      if (is_zero(hd%pr)) stop '>>>> Erreur 2b dans compute_hydraulic_data()'
      hd%dpr = 0.5_long * (prof%perimetre(z+eps,3) - prof%perimetre(z-eps,3)) / eps
      hd%vr = q%qr / hd%sr
      dbtc = prof%Ks(3) * hd%sr * (hd%sr/hd%pr)**e23
      hd%jr = q%qr * abs(q%qr) / (dbtc**2)
      !drag force
      hd%jdr = prof%drag_force(3) * hd%vr * abs(hd%vr)
   else
      hd%lr = zero   ;  hd%sr = zero  ;  hd%pr = zero
      hd%dpr = zero  ;  hd%vr = zero  ;  hd%jr = zero
      hd%jdr = zero
   endif
   hd%lt = hd%lm + hd%ll + hd%lr
   hd%st = hd%sm + hd%sl + hd%sr
   !cas des très petits débordements -> stockage seul
   if (is_small_deb(hd,LEFT)) then
      hd%sm = hd%sm + hd%sl
      hd%lm = hd%lm + hd%ll
      hd%pm = hd%pm + hd%pl
      hd%vl = zero  ;  hd%jl = zero  ;  hd%jdl = zero  ;  hd%pl = zero  ;  hd%sl = zero ;  hd%ll = zero   ;  hd%dpl = zero
   endif
   if (is_small_deb(hd,RIGHT)) then
      hd%sm = hd%sm + hd%sr
      hd%lm = hd%lm + hd%lr
      hd%pm = hd%pm + hd%pr
      hd%vr = zero  ;  hd%jr = zero  ;  hd%jdr = zero  ;  hd%pr = zero  ;  hd%sr = zero  ;  hd%lr = zero   ;  hd%dpr = zero
   endif

   !vérification si les vitesses sont réalistes
   hd%bFail = hd%bFail .or. (abs(hd%vl) > v_sup) .or. (abs(hd%vr) > v_sup)

   !calcul du nombre de Froude
   qtotal = q%qm + q%ql + q%qr
   froude = abs(qtotal)*sqrt(hd%lt/(gravite*hd%st**3))
   if (froude > frsup) then
      frsup = froude
      ifrsup = is
   endif
   !calcul du nombre de Courant (à un facteur dt près)
   if ((dx > 0.001_long) .AND. (prof%iss == 0)) then
      cris = (abs(qtotal/hd%st)+sqrt(gravite*hd%st/hd%lt))/dx
      if (cris > cr) then
         cr = cris ; iscr = is
      endif
   endif
   if (hd%bFail) then
      write(err_message,'(a,f4.1,a,f10.3,a,*(g14.6))') 'Vitesses hors limites acceptables (±',v_sup,' m/s) au Pk ', &
                                                      prof%pk,' : ',hd%vl,hd%vm,hd%vr
      call err042(lErr,err_message)
      !hd%bFail = .false.  !on se contente du message d'information
   endif

end subroutine compute_hydraulic_data



subroutine ISM_discretise_bief(ib,bool)
!sous-programme bief calculant la discrétisation de Saint-Venant + ISM
!
!  ib est le numéro du bief dans l'ordre des données du fichier .NET
!  bool est renvoyé à faux si un problème est détecté
   use casiers, only: vb
   use data_num_mobiles, only: dx
   implicit none
   ! -- prototype
   integer,intent(in) :: ib
   logical,intent(out) :: bool  !variable à utiliser si on détecte un problème numérique
   ! -- variables locales
   integer :: isi, isj, k
   real(kind=long) :: alpha, beta, sij, dzdx, jij, U_int, UL
   real(kind=long) :: qex

   bool = .true.
   vb(ib) = zero
   isi = la_topo%biefs(ib)%is1  ;  isj = isi+1
   call compute_hydraulic_data(hd0(isi),isi,z(isi),q(isi))
   if (hd0(isi)%bFail) then
      bool = .false.  ;  return
   endif
   do isi = la_topo%biefs(ib)%is1, la_topo%biefs(ib)%is2-1
      a(:,:,isi) = zero ; b(:,:,isi) = zero ; c(:,isi) = zero
      isj = isi+1
      dx = abs(la_topo%sections(isj)%pk-la_topo%sections(isi)%pk) !pas d'espace
      call compute_hydraulic_data(hd0(isj),isj,z(isj),q(isj))
      if (hd0(isj)%bFail) then
         bool = .false.  ;  return
      endif
      !mise à jour du volume du bief
      vb(ib) = vb(ib)+dx*(hd0(isi)%st+hd0(isj)%st)*0.5_long

      alpha = dx/DeuxThetaDt

   !équation de continuité -> ligne 1
      ! 2e indice : 1 -> dQm ; 2 -> dQl ; 3 -> dQr ; 4 -> dZ
      a(1,1,isi) = un ; b(1,1,isi) = -un
      a(1,2,isi) = un ; b(1,2,isi) = -un
      a(1,3,isi) = un ; b(1,3,isi) = -un
      a(1,4,isi) = -alpha*(hd0(isi)%lm+hd0(isi)%ll+hd0(isi)%lr)
      b(1,4,isi) = -alpha*(hd0(isj)%lm+hd0(isj)%ll+hd0(isj)%lr)
      c(1,isi) = -(Q_total(q(isi))-Q_total(q(isj)) + dx*(Q_total(qa(isi))+theta*Q_total(dqa(isi))))/theta


   !équations quantité de mvt -> lignes 2, 3 et 4
   !NB : les matrices A et B sont toutes les 2 à gauche du signe égal -> les contributions sont de même
   !     signes pour les dérivées en t et de signes contraires pour les dérivées en x
      !contribution de dQ/dt
      ! 2e indice : 1 -> dQm ; 2 -> dQl ; 3 -> dQr ; 4 -> dZ
      !équation de quantité de mvt - lit mineur -> ligne 2
      a(2,1,isi) = un  ;  b(2,1,isi) = un
      !équation de quantité de mvt - lit moyen gauche -> ligne 3
      a(3,2,isi) = un  ;  b(3,2,isi) = un
      !équation de quantité de mvt - lit moyen droit -> ligne 4
      a(4,3,isi) = un  ;  b(4,3,isi) = un

      !contribution de d(Q²/S)/dx = d(V²S)/dx
      !w = QV => Dw = 2V.DQ - V²L.DZ
      ! 2e indice : 1 -> dQm ; 2 -> dQl ; 3 -> dQr ; 4 -> dZ
      alpha = 2._long*dt/dx ; beta = theta*alpha
      !équation de quantité de mvt - lit mineur -> ligne 2
      a(2,1,isi) = a(2,1,isi) - beta*2._long*hd0(isi)%vm
      b(2,1,isi) = b(2,1,isi) + beta*2._long*hd0(isj)%vm
      a(2,4,isi) = a(2,4,isi) + beta*hd0(isi)%vm*hd0(isi)%vm*hd0(isi)%lm
      b(2,4,isi) = b(2,4,isi) - beta*hd0(isj)%vm*hd0(isj)%vm*hd0(isj)%lm
      c(2,isi)   = c(2,isi)   - alpha*(hd0(isj)%vm*q(isj)%qm-hd0(isi)%vm*q(isi)%qm)
      !équation de quantité de mvt - lit moyen gauche -> ligne 3
      a(3,2,isi) = a(3,2,isi) - beta*2._long*hd0(isi)%vl
      b(3,2,isi) = b(3,2,isi) + beta*2._long*hd0(isj)%vl
      a(3,4,isi) = a(3,4,isi) + beta*hd0(isi)%vl*hd0(isi)%vl*hd0(isi)%ll
      b(3,4,isi) = b(3,4,isi) - beta*hd0(isj)%vl*hd0(isj)%vl*hd0(isj)%ll
      c(3,isi)   = c(3,isi)   - alpha*(hd0(isj)%vl*q(isj)%ql-hd0(isi)%vl*q(isi)%ql)
      !équation de quantité de mvt - lit moyen droit -> ligne 4
      a(4,3,isi) = a(4,3,isi) - beta*2._long*hd0(isi)%vr
      b(4,3,isi) = b(4,3,isi) + beta*2._long*hd0(isj)%vr
      a(4,4,isi) = a(4,4,isi) + beta*hd0(isi)%vr*hd0(isi)%vr*hd0(isi)%lr
      b(4,4,isi) = b(4,4,isi) - beta*hd0(isj)%vr*hd0(isj)%vr*hd0(isj)%lr
      c(4,isi)   = c(4,isi)   - alpha*(hd0(isj)%vr*q(isj)%qr-hd0(isi)%vr*q(isi)%qr)

      if(with_trace_full) write(l9,*) 'd(Q²/S)/dx : ',(a(k,2,isi),k=2,4),(b(k,2,isi),k=2,4),(c(k,isi),k=2,4)

      !contribution de gS.dZ/dx
      ! 2e indice : 1 -> dQm ; 2 -> dQl ; 3 -> dQr ; 4 -> dZ
      alpha = g*dt/dx ; beta = theta*alpha
      dzdx = z(isj)-z(isi)
      !équation de quantité de mvt - lit mineur -> ligne 2
      sij = hd0(isi)%sm + hd0(isj)%sm
      a(2,4,isi) = a(2,4,isi) + beta*(dzdx*hd0(isi)%lm - sij)
      b(2,4,isi) = b(2,4,isi) + beta*(dzdx*hd0(isj)%lm + sij)
      c(2,isi)   = c(2,isi)   - alpha*sij*dzdx
      !équation de quantité de mvt - lit moyen gauche -> ligne 3
      if (is_overbank(hd0(isi),LEFT) .and. is_overbank(hd0(isj),LEFT)) then
         sij = hd0(isi)%sl + hd0(isj)%sl
         a(3,4,isi) = a(3,4,isi) + beta*(dzdx*hd0(isi)%ll - sij)
         b(3,4,isi) = b(3,4,isi) + beta*(dzdx*hd0(isj)%ll + sij)
         c(3,isi)   = c(3,isi)   - alpha*sij*dzdx
      endif
      !équation de quantité de mvt - lit moyen droit -> ligne 4
      if (is_overbank(hd0(isi),RIGHT) .and. is_overbank(hd0(isj),RIGHT)) then
         sij = hd0(isi)%sr + hd0(isj)%sr
         a(4,4,isi) = a(4,4,isi) + beta*(dzdx*hd0(isi)%lr - sij)
         b(4,4,isi) = b(4,4,isi) + beta*(dzdx*hd0(isj)%lr + sij)
         c(4,isi)   = c(4,isi)   - alpha*sij*dzdx
      endif

      if(with_trace_full) write(l9,*) 'gS.dZ/dx   : ',(a(k,2,isi),k=2,4),(b(k,2,isi),k=2,4),(c(k,isi),k=2,4)

      !contribution de gS.J
      ! 2e indice : 1 -> dQm ; 2 -> dQl ; 3 -> dQr ; 4 -> dZ
      alpha = g*dt*0.5_long ; beta = theta*alpha
      !équation de quantité de mvt - lit mineur -> ligne 2
      sij = hd0(isi)%sm + hd0(isj)%sm
      jij = hd0(isi)%jm + hd0(isj)%jm
      if (abs(q(isi)%qm) > zero) a(2,1,isi) = a(2,1,isi) + beta * sij * 2._long*hd0(isi)%jm/q(isi)%qm
      if (abs(q(isj)%qm) > zero) b(2,1,isi) = b(2,1,isi) + beta * sij * 2._long*hd0(isj)%jm/q(isj)%qm
      a(2,4,isi) = a(2,4,isi) + beta*(jij*hd0(isi)%lm - sij*hd0(isi)%jm*(5._long*hd0(isi)%lm/hd0(isi)%sm &
                                                              -2._long*hd0(isi)%dpm/hd0(isi)%pm)/3._long)
      b(2,4,isi) = b(2,4,isi) + beta*(jij*hd0(isj)%lm - sij*hd0(isj)%jm*(5._long*hd0(isj)%lm/hd0(isj)%sm &
                                                              -2._long*hd0(isj)%dpm/hd0(isj)%pm)/3._long)
      c(2,isi)   = c(2,isi)   - alpha*sij*jij
      !équation de quantité de mvt - lit moyen gauche -> ligne 3
      if (is_overbank(hd0(isi),LEFT) .and. is_overbank(hd0(isj),LEFT)) then
         sij = hd0(isi)%sl + hd0(isj)%sl
         jij = hd0(isi)%jl + hd0(isj)%jl
         if (abs(q(isi)%ql) > zero) a(3,2,isi) = a(3,2,isi) + beta * sij * 2._long*hd0(isi)%jl/q(isi)%ql
         if (abs(q(isj)%ql) > zero) b(3,2,isi) = b(3,2,isi) + beta * sij * 2._long*hd0(isj)%jl/q(isj)%ql
         a(3,4,isi) = a(3,4,isi) + beta*(jij*hd0(isi)%ll - sij*hd0(isi)%jl*(5._long*hd0(isi)%ll/hd0(isi)%sl &
                                                                 -2._long*hd0(isi)%dpl/hd0(isi)%pl)/3._long)
         b(3,4,isi) = b(3,4,isi) + beta*(jij*hd0(isj)%ll - sij*hd0(isj)%jl*(5._long*hd0(isj)%ll/hd0(isj)%sl &
                                                                 -2._long*hd0(isj)%dpl/hd0(isj)%pl)/3._long)
         c(3,isi)   = c(3,isi)   - alpha*sij*jij
      endif
      !équation de quantité de mvt - lit moyen droit -> ligne 4
      if (is_overbank(hd0(isi),RIGHT) .and. is_overbank(hd0(isj),RIGHT)) then
         sij = hd0(isi)%sr + hd0(isj)%sr
         jij = hd0(isi)%jr + hd0(isj)%jr
         if (abs(q(isi)%qr) > zero) a(4,3,isi) = a(4,3,isi) + beta * sij * 2._long*hd0(isi)%jr/q(isi)%qr
         if (abs(q(isj)%qr) > zero) b(4,3,isi) = b(4,3,isi) + beta * sij * 2._long*hd0(isj)%jr/q(isj)%qr
         a(4,4,isi) = a(4,4,isi) + beta*(jij*hd0(isi)%lr - sij*hd0(isi)%jr*(5._long*hd0(isi)%lr/hd0(isi)%sr &
                                                                 -2._long*hd0(isi)%dpr/hd0(isi)%pr)/3._long)
         b(4,4,isi) = b(4,4,isi) + beta*(jij*hd0(isj)%lr - sij*hd0(isj)%jr*(5._long*hd0(isj)%lr/hd0(isj)%sr &
                                                                 -2._long*hd0(isj)%dpr/hd0(isj)%pr)/3._long)
         c(4,isi)   = c(4,isi)   - alpha*sij*jij
      endif

      if(with_trace_full) write(l9,*) 'gS.J       : ',(a(k,2,isi),k=2,4),(b(k,2,isi),k=2,4),(c(k,isi),k=2,4)


      ! ROLE: prise en compte de la drag force (discrétisation)
      !contribution de gS.J_D (drag force)
      ! 2e indice : 1 -> dQm ; 2 -> dQl ; 3 -> dQr ; 4 -> dZ
      alpha = g*dt*0.5_long ; beta = theta*alpha
      !équation de quantité de mvt - lit mineur -> ligne 2
      sij = hd0(isi)%sm + hd0(isj)%sm
      jij = hd0(isi)%jdm + hd0(isj)%jdm
      if (abs(q(isi)%qm) > zero) a(2,1,isi) = a(2,1,isi) + beta * sij * 2._long*hd0(isi)%jdm/q(isi)%qm
      if (abs(q(isj)%qm) > zero) b(2,1,isi) = b(2,1,isi) + beta * sij * 2._long*hd0(isj)%jdm/q(isj)%qm
      a(2,4,isi) = a(2,4,isi) + beta*(jij*hd0(isi)%lm - sij*hd0(isi)%jdm*(hd0(isi)%lm/hd0(isi)%sm))
      b(2,4,isi) = b(2,4,isi) + beta*(jij*hd0(isj)%lm - sij*hd0(isj)%jdm*(hd0(isj)%lm/hd0(isj)%sm))
      c(2,isi)   = c(2,isi)   - alpha*sij*jij
      !équation de quantité de mvt - lit moyen gauche -> ligne 3
      if (is_overbank(hd0(isi),LEFT) .and. is_overbank(hd0(isj),LEFT)) then
         sij = hd0(isi)%sl + hd0(isj)%sl
         jij = hd0(isi)%jdl + hd0(isj)%jdl
         if (abs(q(isi)%ql) > zero) a(3,2,isi) = a(3,2,isi) + beta * sij * 2._long*hd0(isi)%jdl/q(isi)%ql
         if (abs(q(isj)%ql) > zero) b(3,2,isi) = b(3,2,isi) + beta * sij * 2._long*hd0(isj)%jdl/q(isj)%ql
         a(3,4,isi) = a(3,4,isi) + beta*(jij*hd0(isi)%ll - sij*hd0(isi)%jdl*(hd0(isi)%ll/hd0(isi)%sl))
         b(3,4,isi) = b(3,4,isi) + beta*(jij*hd0(isj)%ll - sij*hd0(isj)%jdl*(hd0(isj)%ll/hd0(isj)%sl))
         c(3,isi)   = c(3,isi)   - alpha*sij*jij
      endif
      !équation de quantité de mvt - lit moyen droit -> ligne 4
      if (is_overbank(hd0(isi),RIGHT) .and. is_overbank(hd0(isj),RIGHT)) then
         sij = hd0(isi)%sr + hd0(isj)%sr
         jij = hd0(isi)%jdr + hd0(isj)%jdr
         if (abs(q(isi)%qr) > zero) a(4,3,isi) = a(4,3,isi) + beta * sij * 2._long*hd0(isi)%jdr/q(isi)%qr
         if (abs(q(isj)%qr) > zero) b(4,3,isi) = b(4,3,isi) + beta * sij * 2._long*hd0(isj)%jdr/q(isj)%qr
         a(4,4,isi) = a(4,4,isi) + beta*(jij*hd0(isi)%lr - sij*hd0(isi)%jdr*(hd0(isi)%lr/hd0(isi)%sr))
         b(4,4,isi) = b(4,4,isi) + beta*(jij*hd0(isj)%lr - sij*hd0(isj)%jdr*(hd0(isj)%lr/hd0(isj)%sr))
         c(4,isi)   = c(4,isi)   - alpha*sij*jij
      endif

      if(with_trace_full) write(l9,*) 'gS.J_D       : ',(a(k,2,isi),k=2,4),(b(k,2,isi),k=2,4),(c(k,isi),k=2,4)
      ! TODO: contribution de gS.Js dans une seconde étape

      !contribution des échanges de masse MC <--> FP
      ! 2e indice : 1 -> dQm ; 2 -> dQl ; 3 -> dQr ; 4 -> dZ
      alpha = 2._long*dt/dx  ;  beta = theta*alpha
      if (with_mass .and. is_overbank(hd0(isi),LEFT) .and. is_overbank(hd0(isj),LEFT)) then
      !débordement en lit moyen gauche
         qex = qa(isi)%ql - (q(isj)%ql-q(isi)%ql)/dx
         qex = qex - 0.25_long*(hd0(isi)%ll+hd0(isj)%ll)*(dz_old(isi)+dz_old(isj))/dt
         !vitesses à l'interface mineur - moyen
         U_int = V_interface(hd0,qex,isi,.true.,weight_Vint)
         UL = 0.5_long * U_int * (hd0(isi)%ll + hd0(isj)%ll)
         !équation de quantité de mvt - lit mineur -> ligne 2
         a(2,2,isi) = a(2,2,isi) - beta*U_int   ;   b(2,2,isi) = b(2,2,isi) + beta*U_int
         a(2,4,isi) = a(2,4,isi) + UL           ;   b(2,4,isi) = b(2,4,isi) + UL
         c(2,isi)   = c(2,isi)   + 2._long*dt*U_int*qex
         !équation de quantité de mvt - lit moyen gauche -> ligne 3
         a(3,2,isi) = a(3,2,isi) + beta*U_int   ;   b(3,2,isi) = b(3,2,isi) - beta*U_int
         a(3,4,isi) = a(3,4,isi) - UL           ;   b(3,4,isi) = b(3,4,isi) - UL
         c(3,isi)   = c(3,isi)   - 2._long*dt*U_int*qex
         !équation de quantité de mvt - lit moyen droit -> ligne 4
               !>>> pas de contribution du lit moyen droit
      endif
      if (with_mass .and. is_overbank(hd0(isi),RIGHT) .and. is_overbank(hd0(isj),RIGHT)) then
      !débordement en lit moyen droit
         qex = qa(isi)%qr - (q(isj)%qr -q(isi)%qr)/dx
         qex = qex - 0.25_long*(hd0(isi)%lr+hd0(isj)%lr)*(dz_old(isi)+dz_old(isj))/dt
         !vitesses à l'interface mineur - moyen
         U_int = V_interface(hd0,qex,isi,.false.,weight_Vint)
         UL = 0.5_long * U_int * (hd0(isi)%lr + hd0(isj)%lr)
         !équation de quantité de mvt - lit mineur -> ligne 2
         a(2,3,isi) = a(2,3,isi) - beta*U_int   ;   b(2,3,isi) = b(2,3,isi) + beta*U_int
         a(2,4,isi) = a(2,4,isi) + UL           ;   b(2,4,isi) = b(2,4,isi) + UL
         c(2,isi)   = c(2,isi)   + 2._long*dt*U_int*qex
         !équation de quantité de mvt - lit moyen gauche -> ligne 3
               !>>> pas de contribution du lit moyen gauche
         !équation de quantité de mvt - lit moyen droit -> ligne 4
         a(4,3,isi) = a(4,3,isi) + beta*U_int   ;   b(4,3,isi) = b(4,3,isi) - beta*U_int
         a(4,4,isi) = a(4,4,isi) - UL           ;   b(4,4,isi) = b(4,4,isi) - UL
         c(4,isi)   = c(4,isi)   - 2._long*dt*U_int*qex
      endif

      if(with_trace_full) write(l9,*) 'MC <--> FP : ',(a(k,2,isi),k=2,4),(b(k,2,isi),k=2,4),(c(k,isi),k=2,4)

      !contribution des échanges turbulents
      ! NOTE: les échanges turbulents sont positifs dans la sous-section accélérée (majeur) et négatifs dans la sous-section ralentie (mineur)
      alpha = -dt*0.5_long*(la_topo%sections(isi)%psi_t+la_topo%sections(isj)%psi_t) ; beta = theta*alpha
      if (with_turbulence .and. is_overbank(hd0(isi),LEFT) .and. is_overbank(hd0(isj),LEFT)) then
      !débordement en lit moyen gauche
         !équation de quantité de mvt - lit mineur -> ligne 2
         a(2,1,isi) = a(2,1,isi) + 2._long*beta*(hd0(isi)%Vm-hd0(isi)%Vl)*hd0(isi)%hl/hd0(isi)%sm
         b(2,1,isi) = b(2,1,isi) + 2._long*beta*(hd0(isj)%Vm-hd0(isj)%Vl)*hd0(isj)%hl/hd0(isj)%sm
         a(2,2,isi) = a(2,2,isi) - 2._long*beta*(hd0(isi)%Vm-hd0(isi)%Vl)*hd0(isi)%hl/hd0(isi)%sl
         b(2,2,isi) = b(2,2,isi) - 2._long*beta*(hd0(isj)%Vm-hd0(isj)%Vl)*hd0(isj)%hl/hd0(isj)%sl
         a(2,4,isi) = a(2,4,isi) + beta*((hd0(isi)%Vm - hd0(isi)%Vl)**2  &
                                          + 2._long*(hd0(isi)%Vm-hd0(isi)%Vl)*hd0(isi)%hl*&
                                            (hd0(isi)%Vl*hd0(isi)%ll/hd0(isi)%sl-hd0(isi)%Vm*hd0(isi)%lm/hd0(isi)%sm))
         b(2,4,isi) = b(2,4,isi) + beta*((hd0(isj)%Vm - hd0(isj)%Vl)**2  &
                                          + 2._long*(hd0(isj)%Vm-hd0(isj)%Vl)*hd0(isj)%hl*&
                                            (hd0(isj)%Vl*hd0(isj)%ll/hd0(isj)%sl-hd0(isj)%Vm*hd0(isj)%lm/hd0(isj)%sm))
         c(2,isi) = c(2,isi) + alpha*(hd0(isi)%hl*(hd0(isi)%Vm-hd0(isi)%Vl)**2 + hd0(isj)%hl*(hd0(isj)%Vm-hd0(isj)%Vl)**2)
         !équation de quantité de mvt - lit moyen gauche -> ligne 3
         a(3,1,isi) = a(3,1,isi) - 2._long*beta*(hd0(isi)%Vm-hd0(isi)%Vl)*hd0(isi)%hl/hd0(isi)%sm
         b(3,1,isi) = b(3,1,isi) - 2._long*beta*(hd0(isj)%Vm-hd0(isj)%Vl)*hd0(isj)%hl/hd0(isj)%sm
         a(3,2,isi) = a(3,2,isi) + 2._long*beta*(hd0(isi)%Vm-hd0(isi)%Vl)*hd0(isi)%hl/hd0(isi)%sl
         b(3,2,isi) = b(3,2,isi) + 2._long*beta*(hd0(isj)%Vm-hd0(isj)%Vl)*hd0(isj)%hl/hd0(isj)%sl
         a(3,4,isi) = a(3,4,isi) - beta*((hd0(isi)%Vm - hd0(isi)%Vl)**2  &
                                          + 2._long*(hd0(isi)%Vm-hd0(isi)%Vl)*hd0(isi)%hl*&
                                            (hd0(isi)%Vl*hd0(isi)%ll/hd0(isi)%sl-hd0(isi)%Vm*hd0(isi)%lm/hd0(isi)%sm))
         b(3,4,isi) = b(3,4,isi) - beta*((hd0(isj)%Vm - hd0(isj)%Vl)**2  &
                                          + 2._long*(hd0(isj)%Vm-hd0(isj)%Vl)*hd0(isj)%hl*&
                                            (hd0(isj)%Vl*hd0(isj)%ll/hd0(isj)%sl-hd0(isj)%Vm*hd0(isj)%lm/hd0(isj)%sm))
         c(3,isi) = c(3,isi) - alpha*(hd0(isi)%hl*(hd0(isi)%Vm-hd0(isi)%Vl)**2 + hd0(isj)%hl*(hd0(isj)%Vm-hd0(isj)%Vl)**2)
         !équation de quantité de mvt - lit moyen droit -> ligne 4
               !>>> pas de contribution du lit moyen droit
      endif
      if (with_turbulence .and. is_overbank(hd0(isi),RIGHT) .and. is_overbank(hd0(isj),RIGHT)) then
      !débordement en lit moyen droit
         !équation de quantité de mvt - lit mineur -> ligne 2
         a(2,1,isi) = a(2,1,isi) + 2._long*beta*(hd0(isi)%Vm-hd0(isi)%Vr)*hd0(isi)%hr/hd0(isi)%sm
         b(2,1,isi) = b(2,1,isi) + 2._long*beta*(hd0(isj)%Vm-hd0(isj)%Vr)*hd0(isj)%hr/hd0(isj)%sm
         a(2,3,isi) = a(2,3,isi) - 2._long*beta*(hd0(isi)%Vm-hd0(isi)%Vr)*hd0(isi)%hr/hd0(isi)%sr
         b(2,3,isi) = b(2,3,isi) - 2._long*beta*(hd0(isj)%Vm-hd0(isj)%Vr)*hd0(isj)%hr/hd0(isj)%sr
         a(2,4,isi) = a(2,4,isi) + beta*((hd0(isi)%Vm - hd0(isi)%Vr)**2  &
                                          + 2._long*(hd0(isi)%Vm-hd0(isi)%Vr)*hd0(isi)%hr*&
                                            (hd0(isi)%Vr*hd0(isi)%lr/hd0(isi)%sr-hd0(isi)%Vm*hd0(isi)%lm/hd0(isi)%sm))
         b(2,4,isi) = b(2,4,isi) + beta*((hd0(isj)%Vm - hd0(isj)%Vr)**2  &
                                          + 2._long*(hd0(isj)%Vm-hd0(isj)%Vr)*hd0(isj)%hr*&
                                            (hd0(isj)%Vr*hd0(isj)%lr/hd0(isj)%sr-hd0(isj)%Vm*hd0(isj)%lm/hd0(isj)%sm))
         c(2,isi) = c(2,isi) + alpha*(hd0(isi)%hr*(hd0(isi)%Vm-hd0(isi)%Vr)**2 + hd0(isj)%hr*(hd0(isj)%Vm-hd0(isj)%Vr)**2)
         !équation de quantité de mvt - lit moyen gauche -> ligne 3
               !>>> pas de contribution du lit moyen gauche
         !équation de quantité de mvt - lit moyen droit -> ligne 4
         a(4,1,isi) = a(4,1,isi) - 2._long*beta*(hd0(isi)%Vm-hd0(isi)%Vr)*hd0(isi)%hr/hd0(isi)%sm
         b(4,1,isi) = b(4,1,isi) - 2._long*beta*(hd0(isj)%Vm-hd0(isj)%Vr)*hd0(isj)%hr/hd0(isj)%sm
         a(4,3,isi) = a(4,3,isi) + 2._long*beta*(hd0(isi)%Vm-hd0(isi)%Vr)*hd0(isi)%hr/hd0(isi)%sr
         b(4,3,isi) = b(4,3,isi) + 2._long*beta*(hd0(isj)%Vm-hd0(isj)%Vr)*hd0(isj)%hr/hd0(isj)%sr
         a(4,4,isi) = a(4,4,isi) - beta*((hd0(isi)%Vm - hd0(isi)%Vr)**2  &
                                          + 2._long*(hd0(isi)%Vm-hd0(isi)%Vr)*hd0(isi)%hr*&
                                            (hd0(isi)%Vr*hd0(isi)%lr/hd0(isi)%sr-hd0(isi)%Vm*hd0(isi)%lm/hd0(isi)%sm))
         b(4,4,isi) = b(4,4,isi) - beta*((hd0(isj)%Vm - hd0(isj)%Vr)**2  &
                                          + 2._long*(hd0(isj)%Vm-hd0(isj)%Vr)*hd0(isj)%hr*&
                                            (hd0(isj)%Vr*hd0(isj)%lr/hd0(isj)%sr-hd0(isj)%Vm*hd0(isj)%lm/hd0(isj)%sm))
         c(4,isi) = c(4,isi) - alpha*(hd0(isi)%hr*(hd0(isi)%Vm-hd0(isi)%Vr)**2 + hd0(isj)%hr*(hd0(isj)%Vm-hd0(isj)%Vr)**2)
      endif

      if(with_trace_full) write(l9,*) 'Tau_ij     : ',(a(k,2,isi),k=2,4),(b(k,2,isi),k=2,4),(c(k,isi),k=2,4)

      !contribution des apports latéraux distribués dans chaque sous-section
      ! TODO: à revoir, ça parait faire doublon avec les échanges de masse MC <-> FP
      !équation de quantité de mvt - lit mineur -> ligne 2
      if (with_qlat .and. qa(isi)%qm < zero) then  !perte de quantité de mvt
         alpha = dt*qa(isi)%qm ; beta = theta*alpha
         a(2,1,isi) = a(2,1,isi) - beta/hd0(isi)%sm
         b(2,1,isi) = b(2,1,isi) - beta/hd0(isj)%sm
         a(2,4,isi) = a(2,4,isi) + beta*hd0(isi)%vm*hd0(isi)%lm/hd0(isi)%sm
         b(2,4,isi) = b(2,4,isi) + beta*hd0(isj)%vm*hd0(isj)%lm/hd0(isj)%sm
         c(2,isi)   = c(2,isi)   + alpha*(hd0(isi)%vm+hd0(isj)%vm)
      endif
      !équation de quantité de mvt - lit moyen gauche -> ligne 3
      if (with_qlat .and. qa(isi)%ql < zero) then  !perte de quantité de mvt
         alpha = dt*qa(isi)%ql ; beta = theta*alpha
         a(3,2,isi) = a(3,2,isi) - beta/hd0(isi)%sl
         b(3,2,isi) = b(3,2,isi) - beta/hd0(isj)%sl
         a(3,4,isi) = a(3,4,isi) + beta*hd0(isi)%vl*hd0(isi)%ll/hd0(isi)%sl
         b(3,4,isi) = b(3,4,isi) + beta*hd0(isj)%vl*hd0(isj)%ll/hd0(isj)%sl
         c(3,isi)   = c(3,isi)   + alpha*(hd0(isi)%vl+hd0(isj)%vl)
      endif
      !équation de quantité de mvt - lit moyen droit -> ligne 4
      if (with_qlat .and. qa(isi)%qm < zero) then  !perte de quantité de mvt
         alpha = dt*qa(isi)%qr ; beta = theta*alpha
         a(4,3,isi) = a(4,3,isi) - beta/hd0(isi)%sr
         b(4,3,isi) = b(4,3,isi) - beta/hd0(isj)%sr
         a(4,4,isi) = a(4,4,isi) + beta*hd0(isi)%vr*hd0(isi)%lr/hd0(isi)%sr
         b(4,4,isi) = b(4,4,isi) + beta*hd0(isj)%vr*hd0(isj)%lr/hd0(isj)%sr
         c(4,isi)   = c(4,isi)   + alpha*(hd0(isi)%vr+hd0(isj)%vr)
      endif

      !trace
      if (with_trace_full) write(l9,*) 'qlat       : ',(a(k,2,isi),k=2,4),(b(k,2,isi),k=2,4),(c(k,isi),k=2,4)
      if (with_trace_full) call ecrit_A_B_C(isi)
   enddo
end subroutine ISM_discretise_bief



subroutine ISM_correction_bief(ib,bool)
!sous-programme bief calculant la correction pour la discrétisation de Saint-Venant + ISM
!
!  ib est le numéro du bief dans l'ordre des données du fichier .NET
!  bool est renvoyé à faux si un problème est détecté
   use data_num_mobiles, only: dx
   use erreurs_stv, only: reste,ireste
   implicit none
   ! -- prototype
   integer,intent(in) :: ib
   logical,intent(out) :: bool  !variable à utiliser si on détecte un problème numérique
   ! -- variables locales
   integer :: isi, isj
   real(kind=long) :: alpha, sij, dzdx, jij, U_int, UL
   real(kind=long) :: dsi, dsj, dwi, dwj, dfli, dflj, dfri, dfrj, dji, djj
   real(kind=long) :: c1, c2, c3, c4
   type(debits_ISM) :: qt
   real(kind=long)  :: zt
   real(kind=long) :: tmp

   bool = .true.
   isi = la_topo%biefs(ib)%is1
   zt = z(isi)+dz(isi)  ;  qt = q(isi)+dq(isi)
   call compute_hydraulic_data(hd1(isi),isi,zt,qt)
   if (hd1(isi)%bFail) then
      bool = .false.  ;  return
   endif
   do isi = la_topo%biefs(ib)%is1, la_topo%biefs(ib)%is2-1
      isj = isi+1
      dx = abs(la_topo%sections(isj)%pk-la_topo%sections(isi)%pk) !pas d'espace
      zt = z(isj)+dz(isj)  ;  qt = q(isj)+dq(isj)
      call compute_hydraulic_data(hd1(isj),isj,zt,qt)
      if (hd1(isj)%bFail) then
         bool = .false.  ;  return
      endif
      if(with_trace_full) write(l9,'(a,i0,a,i0)') '>>>> Corrections pour la maille ',isi,' - ',isj
      alpha = dx/DeuxThetaDt

                                 !équation de continuité -> ligne 1

      dsi = S_total(hd1(isi)) - S_total(hd0(isi))
      dsj = S_total(hd1(isj)) - S_total(hd0(isj))
      c1 = Q_total(dq(isi))-Q_total(dq(isj)) - alpha*(dsi+dsj)

                                 !équations quantité de mvt -> lignes 2, 3 et 4

   !contribution de dQ/dt
      c2 = dq(isi)%qm + dq(isj)%qm
      c3 = dq(isi)%ql + dq(isj)%ql
      c4 = dq(isi)%qr + dq(isj)%qr
      if(with_trace_full) write(l9,'(9x,a,4g14.6)') 'dQ/dt : ',c1,c2,c3,c4

   !contribution de d(Q²/S)/dx = d(V²S)/dx
      !w = V²S => Dw = 2V.DQ - V²L.DZ
      alpha = DeuxThetaDt/dx
      !équation de quantité de mvt - lit mineur -> ligne 2
      dwi = hd1(isi)%sm*hd1(isi)%vm**2 - hd0(isi)%sm*hd0(isi)%vm**2
      dwj = hd1(isj)%sm*hd1(isj)%vm**2 - hd0(isj)%sm*hd0(isj)%vm**2
      c2 = c2 + alpha*(dwj-dwi)
      !équation de quantité de mvt - lit moyen gauche -> ligne 3
      if (is_overbank(hd1(isi),LEFT) .and. is_overbank(hd1(isj),LEFT)) then
         dwi = hd1(isi)%sl*hd1(isi)%vl**2 - hd0(isi)%sl*hd0(isi)%vl**2
         dwj = hd1(isj)%sl*hd1(isj)%vl**2 - hd0(isj)%sl*hd0(isj)%vl**2
         c3 = c3 + alpha*(dwj-dwi)
      endif
      !équation de quantité de mvt - lit moyen droit -> ligne 4
      if (is_overbank(hd1(isi),RIGHT) .and. is_overbank(hd1(isj),RIGHT)) then
         dwi = hd1(isi)%sr*hd1(isi)%vr**2 - hd0(isi)%sr*hd0(isi)%vr**2
         dwj = hd1(isj)%sr*hd1(isj)%vr**2 - hd0(isj)%sr*hd0(isj)%vr**2
         c4 = c4 + alpha*(dwj-dwi)
      endif
      if(with_trace_full) write(l9,'(4x,a,4g14.6)') 'd(Q²/S)/dx : ',c1,c2,c3,c4

   !contribution de gS.dZ/dx
      alpha = g*theta*dt/dx
      dzdx = z(isj)-z(isi)
      !équation de quantité de mvt - lit mineur -> ligne 2
      sij = hd0(isi)%sm + hd0(isj)%sm
      dsi = hd1(isi)%sm - hd0(isi)%sm  ;  dsj = hd1(isj)%sm - hd0(isj)%sm
      c2 = c2 + alpha*(sij*(dz(isj)-dz(isi)) + dzdx*(dsi+dsj))
      !équation de quantité de mvt - lit moyen gauche -> ligne 3
      if (is_overbank(hd1(isi),LEFT) .and. is_overbank(hd1(isj),LEFT)) then
         sij = hd0(isi)%sl + hd0(isj)%sl
         dsi = hd1(isi)%sl - hd0(isi)%sl  ;  dsj = hd1(isj)%sl - hd0(isj)%sl
         c3 = c3 + alpha*(sij*(dz(isj)-dz(isi)) + dzdx*(dsi+dsj))
      endif
      !équation de quantité de mvt - lit moyen droit -> ligne 4
      if (is_overbank(hd1(isi),RIGHT) .and. is_overbank(hd1(isj),RIGHT)) then
         sij = hd0(isi)%sr + hd0(isj)%sr
         dsi = hd1(isi)%sr - hd0(isi)%sr  ;  dsj = hd1(isj)%sr - hd0(isj)%sr
         c4 = c4 + alpha*(sij*(dz(isj)-dz(isi)) + dzdx*(dsi+dsj))
      endif
      if(with_trace_full) write(l9,'(6x,a,4g14.6)') 'gS.dZ/dx : ',c1,c2,c3,c4

   !contribution de gS.J
      alpha = g*theta*dt*0.5_long
      !équation de quantité de mvt - lit mineur -> ligne 2
      sij = hd0(isi)%sm + hd0(isj)%sm  ;  jij = hd0(isi)%jm + hd0(isj)%jm
      dsi = hd1(isi)%sm - hd0(isi)%sm  ;  dsj = hd1(isj)%sm - hd0(isj)%sm
      dji = hd1(isi)%jm - hd0(isi)%jm  ;  djj = hd1(isj)%jm - hd0(isj)%jm
      c2 = c2 + alpha*(sij*(dji+djj)+jij*(dsi+dsj))
      !équation de quantité de mvt - lit moyen gauche -> ligne 3
      if (is_overbank(hd1(isi),LEFT) .and. is_overbank(hd1(isj),LEFT)) then
         sij = hd0(isi)%sl + hd0(isj)%sl  ;  jij = hd0(isi)%jl + hd0(isj)%jl
         dsi = hd1(isi)%sl - hd0(isi)%sl  ;  dsj = hd1(isj)%sl - hd0(isj)%sl
         dji = hd1(isi)%jl - hd0(isi)%jl  ;  djj = hd1(isj)%jl - hd0(isj)%jl
         c3 = c3 + alpha*(sij*(dji+djj)+jij*(dsi+dsj))
      endif
      !équation de quantité de mvt - lit moyen droit -> ligne 4
      if (is_overbank(hd1(isi),RIGHT) .and. is_overbank(hd1(isj),RIGHT)) then
         sij = hd0(isi)%sr + hd0(isj)%sr  ;  jij = hd0(isi)%jr + hd0(isj)%jr
         dsi = hd1(isi)%sr - hd0(isi)%sr  ;  dsj = hd1(isj)%sr - hd0(isj)%sr
         dji = hd1(isi)%jr - hd0(isi)%jr  ;  djj = hd1(isj)%jr - hd0(isj)%jr
         c4 = c4 + alpha*(sij*(dji+djj)+jij*(dsi+dsj))
      endif
      if(with_trace_full) write(l9,'(10x,a,4g14.6)') 'gS.J : ',c1,c2,c3,c4

   !contribution de gS.J_D (drag force)
      ! ROLE: prise en compte de la drag force (correction)
      alpha = g*theta*dt*0.5_long
      !équation de quantité de mvt - lit mineur -> ligne 2
      sij = hd0(isi)%sm + hd0(isj)%sm    ;  jij = hd0(isi)%jdm + hd0(isj)%jdm
      dsi = hd1(isi)%sm - hd0(isi)%sm    ;  dsj = hd1(isj)%sm  - hd0(isj)%sm
      dji = hd1(isi)%jdm - hd0(isi)%jdm  ;  djj = hd1(isj)%jdm - hd0(isj)%jdm
      c2 = c2 + alpha*(sij*(dji+djj)+jij*(dsi+dsj))
      !équation de quantité de mvt - lit moyen gauche -> ligne 3
      if (is_overbank(hd1(isi),LEFT) .and. is_overbank(hd1(isj),LEFT)) then
         sij = hd0(isi)%sl + hd0(isj)%sl    ;  jij = hd0(isi)%jdl + hd0(isj)%jdl
         dsi = hd1(isi)%sl - hd0(isi)%sl    ;  dsj = hd1(isj)%sl  - hd0(isj)%sl
         dji = hd1(isi)%jdl - hd0(isi)%jdl  ;  djj = hd1(isj)%jdl - hd0(isj)%jdl
         c3 = c3 + alpha*(sij*(dji+djj)+jij*(dsi+dsj))
      endif
      !équation de quantité de mvt - lit moyen droit -> ligne 4
      if (is_overbank(hd1(isi),RIGHT) .and. is_overbank(hd1(isj),RIGHT)) then
         sij = hd0(isi)%sr + hd0(isj)%sr    ;  jij = hd0(isi)%jdr + hd0(isj)%jdr
         dsi = hd1(isi)%sr - hd0(isi)%sr    ;  dsj = hd1(isj)%sr  - hd0(isj)%sr
         dji = hd1(isi)%jdr - hd0(isi)%jdr  ;  djj = hd1(isj)%jdr - hd0(isj)%jdr
         c4 = c4 + alpha*(sij*(dji+djj)+jij*(dsi+dsj))
      endif
      if(with_trace_full) write(l9,'(8x,a,4g14.6)') 'gS.J_D : ',c1,c2,c3,c4

   ! TODO: contribution de gS.Js dans une seconde étape

   !contribution des échanges de masse MC <--> FP
      alpha = DeuxThetaDt/dx
      if (with_mass .and. is_overbank(hd1(isi),LEFT) .and. is_overbank(hd1(isj),LEFT)) then
      !débordement en lit moyen gauche
         dsi = hd1(isi)%sl - hd0(isi)%sl  ;  dsj = hd1(isj)%sl - hd0(isj)%sl
         qml(isi) = qa(isi)%ql - 0.5_long*(dsi+dsj)/dt - (q(isj)%ql -q(isi)%ql)/dx
         U_int = V_interface(hd1,qml(isi),isi,.true.,weight_Vint)
         UL = 0.5_long * U_int * (hd1(isi)%ll + hd1(isj)%ll)
         !équation de quantité de mvt - lit mineur -> ligne 2
         c2 = c2 + UL*(dz(isi)+dz(isj)) + U_int*alpha*(dq(isj)%ql-dq(isi)%ql)
         !équation de quantité de mvt - lit moyen gauche -> ligne 3
         c3 = c3 - UL*(dz(isi)+dz(isj)) - U_int*alpha*(dq(isj)%ql-dq(isi)%ql)
         !équation de quantité de mvt - lit moyen droit -> ligne 4
               !>>> pas de contribution du lit moyen droit
      else
         qml(isi) = zero
      endif
      if (with_mass .and. is_overbank(hd1(isi),RIGHT) .and. is_overbank(hd1(isj),RIGHT)) then
      !débordement en lit moyen droit
         dsi = hd1(isi)%sr - hd0(isi)%sr  ;  dsj = hd1(isj)%sr - hd0(isj)%sr
         qmr(isi) = qa(isi)%qr - 0.5_long*(dsi+dsj)/dt - (q(isj)%qr -q(isi)%qr)/dx
         U_int = V_interface(hd1,qmr(isi),isi,.false.,weight_Vint)
         UL = 0.5_long * U_int * (hd1(isi)%lr + hd1(isj)%lr)
         !équation de quantité de mvt - lit mineur -> ligne 2
         c2 = c2 + UL*(dz(isi)+dz(isj)) + U_int*alpha*(dq(isj)%qr-dq(isi)%qr)
         !équation de quantité de mvt - lit moyen gauche -> ligne 3
               !>>> pas de contribution du lit moyen gauche
         !équation de quantité de mvt - lit moyen droit -> ligne 4
         c4 = c4 - UL*(dz(isi)+dz(isj)) - U_int*alpha*(dq(isj)%qr-dq(isi)%qr)
      else
         qmr(isi) = zero
      endif
      if(with_trace_full) write(l9,'(4x,a,4g14.6)') 'MC <--> FP : ',c1,c2,c3,c4

   !contribution des échanges turbulents
   ! NOTE: les échanges turbulents sont positifs dans la sous-section accélérée (majeur) et négatifs dans la sous-section ralentie (mineur)
      alpha = -dt*theta*0.5_long*(la_topo%sections(isi)%psi_t+la_topo%sections(isj)%psi_t)
      if (with_turbulence .and. is_overbank(hd1(isi),LEFT) .and. is_overbank(hd1(isj),LEFT)) then
      !débordement en lit moyen gauche
         dfli = hd1(isi)%hl*(hd1(isi)%vm-hd1(isi)%vl)**2 - hd0(isi)%hl*(hd0(isi)%vm-hd0(isi)%vl)**2
         dflj = hd1(isj)%hl*(hd1(isj)%vm-hd1(isj)%vl)**2 - hd0(isj)%hl*(hd0(isj)%vm-hd0(isj)%vl)**2
         !équation de quantité de mvt - lit mineur -> ligne 2
         c2 = c2 + alpha*(dfli+dflj)
         !équation de quantité de mvt - lit moyen gauche -> ligne 3
         c3 = c3 - alpha*(dfli+dflj)
         !équation de quantité de mvt - lit moyen droit -> ligne 4
               !>>> pas de contribution du lit moyen droit
      endif
      if (with_turbulence .and. is_overbank(hd1(isi),RIGHT) .and. is_overbank(hd1(isj),RIGHT)) then
      !débordement en lit moyen droit
         dfri = hd1(isi)%hr*(hd1(isi)%vm-hd1(isi)%vr)**2 - hd0(isi)%hr*(hd0(isi)%vm-hd0(isi)%vr)**2
         dfrj = hd1(isj)%hr*(hd1(isj)%vm-hd1(isj)%vr)**2 - hd0(isj)%hr*(hd0(isj)%vm-hd0(isj)%vr)**2
         !équation de quantité de mvt - lit mineur -> ligne 2
         c2 = c2 + alpha*(dfri+dfrj)
         !équation de quantité de mvt - lit moyen gauche -> ligne 3
               !>>> pas de contribution du lit moyen gauche
         !équation de quantité de mvt - lit moyen droit -> ligne 4
         c4 = c4 - alpha*(dfri+dfrj)
      endif
      if(with_trace_full) write(l9,'(8x,a,4g14.6)') 'Tau_ij : ',c1,c2,c3,c4
      if (with_trace_full) call ecrit_A_B_C(isi)

      !mise à jour du second membre
      c(1,isi) = c(1,isi) + C0(1,isi) - c1
      c(2,isi) = c(2,isi) + C0(2,isi) - c2
      c(3,isi) = c(3,isi) + C0(3,isi) - c3
      c(4,isi) = c(4,isi) + C0(4,isi) - c4

      if (with_trace_full) call ecrit_A_B_C(isi)
      !---calcul des résidus
      tmp = max(abs(C0(1,isi) - c1),abs(C0(2,isi) - c2),abs(C0(3,isi) - c3),abs(C0(4,isi) - c4))
      if (reste < tmp) then
         reste = tmp
         ireste = isi
      endif
   enddo
end subroutine ISM_correction_bief



subroutine ISM_update_NR(yapb)
!réalise la correction des itérations de N-R sur tout le réseau
!renvoie yapb = vrai si un problème a été détecté
   use data_num_mobiles, only: frsup, ifrsup, cr, iscr
   use erreurs_stv, only: reste,ireste
   implicit none
   ! -- prototype --
   logical,intent(out) :: yapb
   ! -- variables --
   integer :: ib
   logical :: b_bief

   frsup = zero  ;  ifrsup = 0
   cr = zero  ;  iscr = 0
   reste = -1._long  ; ireste = 0

   !boucle sur tous les biefs
   ! NOTE: on suppose pour l'instant que le réseau est ramifié
   yapb = .false.
   do ib = 1, la_topo%net%iba
      !discrétisation des 4 équations de Saint-Venant
      call ISM_correction_bief(ib,b_bief)
      if (.not.b_bief) then
         if (with_trace_small) write(l9,*) '>>>> Problème avec la correction ISM du bief ', ib
         yapb = .true.
      endif
   enddo
   first_iteration = .false.
end subroutine ISM_update_NR



subroutine ISM_discretisation(yapb)
!discrétisation sur l'ensemble du réseau
!renvoie yapb = vrai si un problème a été détecté
   use data_num_mobiles, only: frsup, ifrsup, cr, iscr
   use erreurs_stv, only: echelle
   implicit none
   ! -- prototype --
   logical,intent(out) :: yapb
   ! -- variables --
   integer :: ib
   logical :: b_bief

   !call initialize_solution
   DeuxThetaDt = theta*dt*2._long
   frsup = zero  ;  ifrsup = 0
   cr = zero  ;  iscr = 0

   !boucle sur tous les biefs
   ! NOTE: on suppose pour l'instant que le réseau est ramifié
   yapb = .false.
   do ib = 1, la_topo%net%iba
      !discrétisation des 4 équations de Saint-Venant
      call ISM_discretise_bief(ib,b_bief)
      if (.not.b_bief) then
         if (with_trace_small) write(l9,*) '>>>> Problème avec la discrétisation ISM du bief ', ib
         yapb = .true.
      endif
   enddo
   !backup de C dans C0
   C0 = C
   echelle = maxval(abs(C))
   ! NOTE: test pour vérifier que MAXVAL() fonctionne correctement
   if (.NOT.(echelle > zero)) then
      write(error_unit,*) '>>>> BUG dans ISM_discretisation : MAXVAL renvoie une valeur erronée : ',echelle
      write(error_unit,*) '     méthode de calcul alternative pour echelle'
      echelle = alt_maxval(c,size(c))
   endif
   first_iteration = .true.
end subroutine ISM_discretisation



pure function alt_maxval(c,n)
!méthode alternative de calcul de la valeur maximale d'un tableau
   implicit none
   ! -- prototype --
   real(kind=long) :: alt_maxval
   integer, intent(in) :: n  !taille du tableau
   real(kind=long),intent(in) :: c(n)
   ! -- variables --
   integer :: i

   alt_maxval = zero
   do i = 1, n
      alt_maxval = max(alt_maxval,abs(c(i)))
   enddo
end function alt_maxval



subroutine ISM_balayage_1_amont_aval
!1er balayage dans le sens amont --> aval
!             complète aussi les matrices pour les CL aval
!version préliminaire :
!      # réseau ramifié uniquement
!      # ne prend pas en compte les casiers : tous les nœuds sont supposés à surface nulle
   implicit none
   ! -- variables --
   integer :: ib, kb, is
   integer, parameter :: n=4

   !initialisation des matrices pour les CL amont et aval
   !initialisation des matrices pour les CL amont et aval
   CLM = zero ; tm = zero
   CLV = zero ; tv = zero
!   n = 4
   !boucle sur le réseau dans l'ordre amont --> aval à l'amont de la maille
   do ib = 1, la_topo%net%iba
      kb = la_topo%net%numero(ib)
      call set_CL_amont(ib)

      !cas 1ere section, donc avec une matrice 3*4 au-dessus
      is = la_topo%biefs(kb)%is1
      !#1 : produit par A^-1 --> matrices E et 2nd mb F ; le bloc à gauche de E est l'identité
      call produit_matrice_inverse(A(:,:,is),B(:,:,is),E(:,:,is),C(:,is),F(:,is),n)
      !#2 : changement de l'ordre des équations : 4, 1, 2, 3 pour remplacer I 4x4 par K = I 3x3 en bas à gauche
      !     et 1 en haut à droite
      call permut_4123(E(:,:,is),F(:,is))
      if (with_trace_full) call ecrit_E_F(is)
      !#3 : élimination de Gauss adaptée : on élimine le bloc I 3x3 de K (bloc à gauche de E)
      !     on procède exactement comme pour le double-balayage à 2 équations mais avec des matrices
      E(2:4,1:4,is) = matmul(CLM(1:3,1:3,kb),E(2:4,1:4,is)) + matmul(CLM(1:3,4:4,kb),E(1:1,1:4,is))
      F(2:4,is) = matmul(CLM(1:3,1:3,kb),F(2:4,is)) + matmul(CLM(1:3,4:4,kb),F(1:1,is)) - TM(:,kb)
      call normalize_E_F(is)
      if (with_trace_full) call ecrit_E_F(is)

      !cas des sections suivantes
      do is = la_topo%biefs(kb)%is1+1, la_topo%biefs(kb)%is2-1
         !#1 : produit par A^-1 --> matrices E et 2nd mb F ; le bloc à gauche de E est l'identité
         call produit_matrice_inverse(A(:,:,is),B(:,:,is),E(:,:,is),C(:,is),F(:,is),n)
         if (with_trace_full) call ecrit_E_F(is)
         !#2 : changement de l'ordre des équations : 4, 1, 2, 3 pour remplacer I 4x4 par K = I 3x3 en bas à gauche
         !     et 1 en haut à droite
         call permut_4123(E(:,:,is),F(:,is))
         !#3 : élimination de Gauss adaptée : on élimine le bloc I 3x3 de K (bloc à gauche de E)
         !     on procède exactement comme pour le double-balayage à 2 équations mais avec des matrices
         E(2:4,1:4,is) = matmul(E(2:4,1:3,is-1),E(2:4,1:4,is)) + matmul(E(2:4,4:4,is-1),E(1:1,1:4,is))
         F(2:4,is) = matmul(E(2:4,1:3,is-1),F(2:4,is)) + matmul(E(2:4,4:4,is-1),F(1:1,is)) - F(2:4,is-1)
         call normalize_E_F(is)
         if (with_trace_full) call ecrit_E_F(is)
      enddo
   enddo
end subroutine ISM_balayage_1_amont_aval



subroutine ISM_balayage_2_aval_amont
!2e balayage dans le sens aval --> amont
!version préliminaire :
!      # réseau ramifié uniquement
   implicit none
  ! -- variables --
   integer :: ib, kb, is, isn
   !integer, parameter :: n=4
   real(kind=long),dimension(4,4) :: EEn
   real(kind=long),dimension(3,3) :: En
   real(kind=long),dimension(4) :: Fn

   !initialisation des matrices pour les CL aval
   !boucle sur le réseau dans l'ordre amont --> aval à l'amont de la maille
   do ib = la_topo%net%iba, 1, -1
      kb = la_topo%net%numero(ib)
      isn = la_topo%biefs(kb)%is2
      call set_CL_aval(ib)

      !section aval du bief -> résolution des 4 équations aval
      EEn(1:3,1:4) = E(2:4,1:4,isn-1)  ;  Fn(1:3) = F(2:4,isn-1)
      EEn(4,1:4)   = CLV(1:4,kb)       ;  Fn(4)   = TV(kb)
      if (with_trace_full) call ecrit_systlin(EEn,Fn,4,isn)
      call solve_systlin(EEn,Fn,4,.true.)
      call apply_solution(Fn,isn)
      !remontée
      do is = isn-1, la_topo%biefs(kb)%is1+1, -1
         !calcul de DZ
         Fn(4) = f(4,is) - dot_product(E(1,1:4,is),Fn)
         En(1:3,1:3) = E(2:4,1:3,is-1)  ;  Fn(1:3) = F(2:4,is-1) - Fn(4)*E(2:4,4,is-1)
         if (with_trace_full) call ecrit_systlin(En,Fn,3,is)
         call solve_systlin(En,Fn(1:3),3,.true.)
         call apply_solution(Fn,is)
      enddo
      !section amont du bief
      is = la_topo%biefs(kb)%is1
      !calcul de DZ
      Fn(4) = f(4,is) - dot_product(E(1,1:4,is),Fn)
      En(1:3,1:3) = CLM(1:3,1:3,kb)  ;  Fn(1:3) = TM(1:3,kb)
      if (with_trace_full) call ecrit_systlin(En,Fn,3,is)
      call solve_systlin(En,Fn(1:3),3,.true.)
      call apply_solution(Fn,is)
   enddo
end subroutine ISM_balayage_2_aval_amont



subroutine ISM_resolution(bFail)
!double balayage
   implicit none
   ! -- prototype --
   logical,intent(out) :: bFail
   ! -- variables locales --
   integer, pointer :: ismax
   integer :: isi
   type(profil),pointer :: prof

   ismax => la_topo%net%ns
   if (double_balayage) then
      call ISM_balayage_1_amont_aval()
      call ISM_balayage_2_aval_amont()
      bFail = .false.
   else
      !call ISM_resolution_globale(bFail)
      call ISM_resolution_tridiag(bFail)
   endif
   ! correction dans le cas débit en lit majeur sans débordement
   do isi = 1, ismax
      prof => la_topo%sections(isi)
      if (is_not_overflowing(prof,z(isi)+dz(isi),LEFT) .AND. abs(q(isi)%ql+dq(isi)%ql) > zero) then
         dq(isi)%qm = dq(isi)%qm+q(isi)%ql+dq(isi)%ql
         dq(isi)%ql = -q(isi)%ql
      endif
      if (is_not_overflowing(prof,z(isi)+dz(isi),RIGHT) .AND. abs(q(isi)%qr+dq(isi)%qr) > zero) then
         dq(isi)%qm = dq(isi)%qm+q(isi)%qr+dq(isi)%qr
         dq(isi)%qr = -q(isi)%qr
      endif
   enddo
end subroutine ISM_resolution



subroutine ISM_resolution_tridiag(bFail)
!Résolution sans double balayage mais avec un système bloc-tridiagonal par bief
!             complète aussi les matrices pour les CL aval
!version préliminaire :
!      # réseau ramifié uniquement
! FIXME: en fait le parcours du réseau ne fonctionne pas, on ne peut pas résoudre bief par bief.
!        il faut faire un 1er balayage sur le réseau complet de l'amont à l'aval puis remonter
!        de l'aval vers l'amont.
!      # ne prend pas en compte les casiers : tous les nœuds sont supposés à surface nulle
!      # il n'y a pas d'apport ponctuel aux nœuds intérieurs
   implicit none
   ! -- prototype --
   logical,intent(out) :: bFail
   ! -- variables --
   integer :: ib, kb, isi, nb_nan, bloc, i, j, nbloc, is1, is2
   ! bloc sous-diagonal : AT
   ! bloc diagonal      : BT
   ! bloc sur-diagonal  : CT
   ! second membre      : DT
   ! solution           : XT
   ! taille des blocs : 4
   real(kind=long), allocatable, save :: AT(:,:,:), BT(:,:,:), CT(:,:,:), DT(:,:), XT(:,:)
   logical,save :: first_call = .true.

   bFail = .false.
   if (with_trace_small) then
      write(l9,*) '---> Appel de ISM_resolution_tridiag()'
   endif
   nb_nan = 0

   if (first_call) then
      nbloc = 0
      do ib = 1, la_topo%net%iba
         kb = la_topo%net%numero(ib)
         nbloc = max(nbloc, (la_topo%biefs(kb)%is2 - la_topo%biefs(kb)%is1 + 1))
      enddo
      allocate (AT(4,4,nbloc),BT(4,4,nbloc),CT(4,4,nbloc),DT(4,nbloc),XT(4,nbloc))
      first_call = .false.
   endif

   !initialisation des matrices pour les CL amont et aval
   CLM = zero ; tm = zero
   CLV = zero ; tv = zero
   !boucle sur le réseau dans l'ordre amont --> aval à l'amont de la maille
   do ib = 1, la_topo%net%iba
      ! C.L. Amont
      call set_CL_amont(ib)
      ! C.L. Aval
      call set_CL_aval(ib)
      !Assemblage de la matrice
      kb = la_topo%net%numero(ib)
      is1 = la_topo%biefs(kb)%is1
      is2 = la_topo%biefs(kb)%is2
      nbloc = is2 - is1 +1

      bloc = 1  !CL amont
      AT(1:4,1:4,bloc) = zero
      BT(1:3,1:4,bloc) = CLM(1:3,1:4,kb)
      BT(4,1:4,bloc) = A(1,1:4,is1)
      CT(1:3,1:4,bloc) = zero
      CT(4,1:4,bloc) = B(1,1:4,is1)
      DT(1:3,bloc) = TM(1:3,kb)
      DT(4,bloc) = C(1,is1)

      is1 = la_topo%biefs(kb)%is1 - 2
      do bloc = 2, nbloc-1
         isi = is1 + bloc
         AT(1:3,1:4,bloc) = A(2:4,1:4,isi)
         AT(4,1:4,bloc) = zero
         BT(1:3,1:4,bloc) = B(2:4,1:4,isi)
         BT(4,1:4,bloc) = A(1,1:4,isi+1)
         CT(1:3,1:4,bloc) = zero
         CT(4,1:4,bloc) = B(1,1:4,isi+1)
         DT(1:3,bloc) = C(2:4,isi)
         DT(4,bloc) = C(1,isi+1)
      enddo
      bloc = nbloc  !CL aval
      AT(1:3,1:4,bloc) = A(2:4,1:4,is2-1)
      AT(4,1:4,bloc) = zero
      BT(1:3,1:4,bloc) = B(2:4,1:4,is2-1)
      BT(4,1:4,bloc) = CLV(1:4,kb)
      CT(1:4,1:4,bloc) = zero
      DT(1:3,bloc) = C(2:4,is2-1)
      DT(4,bloc) = TV(kb)

      !Recherche de NaN dans le système linéaire
#ifdef debug
      do bloc = 1, nbloc
         do j = 1, 4
            do i = 1, 4
               if (.not.(AT(i,j,bloc)<=zero) .and. .not.(AT(i,j,bloc)>=zero)) nb_nan = nb_nan + 1
               if (.not.(BT(i,j,bloc)<=zero) .and. .not.(BT(i,j,bloc)>=zero)) nb_nan = nb_nan + 1
               if (.not.(CT(i,j,bloc)<=zero) .and. .not.(CT(i,j,bloc)>=zero)) nb_nan = nb_nan + 1
            enddo
         enddo
         do i = 1, 4
            if (.not.(DT(i,bloc)<=zero) .and. .not.(DT(i,bloc)>=zero)) nb_nan = nb_nan + 1
         enddo
      enddo
      if (nb_nan > 0) then
         write(output_unit,'(a)') '>>>> Ouh la la ! Il y a plein de NaN dans le système linéaire pour le bief ',kb,' !!!'
         bFail = .true.
         return
      endif
#endif /* debug */
      !Résolution
      call block_tridiag(AT,BT,CT,DT,XT,nbloc,4)

      !Récupération de la solution
      is1 = la_topo%biefs(kb)%is1 - 1
      do bloc = 1, nbloc
         isi = is1 + bloc
         dq(isi)%qm = XT(1,bloc)  ;  dq(isi)%ql = XT(2,bloc)  ;  dq(isi)%qr = XT(3,bloc)
         dz(isi) = XT(4,bloc)
      enddo
   enddo
end subroutine ISM_resolution_tridiag



subroutine ISM_resolution_sgefa(bFail)
! Résolution sans double balayage mais avec un système global par bief
!             complète aussi les matrices pour les CL aval
! La méthode de résolution utilisée est une décomposition LU avec pivot partiel
! La routine sgefa_c_omp() est adaptée de la routine sgefa de LINPACK
!
!version préliminaire :
!      # réseau ramifié uniquement
!      # ne prend pas en compte les casiers : tous les nœuds sont supposés à surface nulle
!      # il n'y a pas d'apport ponctuel aux nœuds intérieurs
   implicit none
   ! -- prototype --
   logical,intent(out) :: bFail
   ! -- variables --
   integer :: ib, kb, isi, i, j, isn, nb_nan, info
   real(kind=long),allocatable,save :: AT(:,:,:), BT(:)
   integer,allocatable,save :: indx(:,:)
   logical,save :: first_call = .true.

   bFail = .false.
   if (with_trace_small) then
      write(l9,*) '---> Appel de ISM_resolution_sgefa()'
   endif
   nb_nan = 0

   if (first_call) then
      isn = 0
      do ib = 1, la_topo%net%iba
         kb = la_topo%net%numero(ib)
         isn = max(isn, 4 * (la_topo%biefs(kb)%is2 - la_topo%biefs(kb)%is1 + 1))
      enddo
      ! NOTE: réduire le volume de stockage de la matrice AT : inutile ici, voir ISM_resolution_tridiag()
      ! NOTE: inutile de créer un vecteur BT pour chaque bief car il est reconstruit à chaque itération; il suffit que la taille de départ soit suffisante
      allocate (AT(isn,isn,la_topo%net%iba),BT(isn),indx(isn,la_topo%net%iba))
      first_call = .false.
   endif

   !initialisation des matrices pour les CL amont et aval
   CLM = zero ; tm = zero
   CLV = zero ; tv = zero
   !boucle sur le réseau dans l'ordre amont --> aval à l'amont de la maille
   do ib = 1, la_topo%net%iba
      ! C.L. Amont
      call set_CL_amont(ib)
      ! C.L. Aval
      call set_CL_aval(ib)
      !Assemblage de la matrice
      kb = la_topo%net%numero(ib)
      isn = 4 * (la_topo%biefs(kb)%is2 - la_topo%biefs(kb)%is1 + 1)
      if (first_iteration) then
         AT(1:isn,1:isn,ib) = zero

         !CL amont : lignes 1 à 3
         AT(1:3,1:4,ib) = CLM(1:3,1:4,kb)
         i = 4  ;  j = 1
         do isi = la_topo%biefs(kb)%is1, la_topo%biefs(kb)%is2-1
            AT(i:i+3,j:j+3,ib) = A(:,:,isi)
            AT(i:i+3,j+4:j+7,ib) = B(:,:,isi)
            i = i+4  ;  j = j+4
         enddo

         !CL aval : dernière ligne
         AT(isn,isn-3:isn,ib) = CLV(:,kb)

#ifdef debug
         !Recherche de NaN
         do j = 1, isn
            do i = 1, isn
               if (.not.(AT(i,j,ib)<=zero) .and. .not.(AT(i,j,ib)>=zero)) then
                  nb_nan = nb_nan + 1
               endif
            enddo
         enddo

         if (nb_nan > 0) then
            write(output_unit,'(a)') '>>>> Ouh la la ! Il y a plein de NaN ' &
                             ,'dans la matrice du système linéaire pour le bief ',kb,' !!!'
            bFail = .true.
            return
         endif
#endif /* debug */

         !décomposition L.U de la matrice
         call sgefa_c_omp(AT(:isn,:isn,ib),isn,indx(:isn,ib),info,nb_cpu)
         if (info /= 0) then
            write(error_unit,'(1x,a)') '>>>> Matrice singulière dans ISM_resolution_sgefa()'
            stop 4
         endif
      endif

      !Assemblage second membre
      BT = zero
      !CL amont : lignes 1 à 3
      BT(1:3) = TM(1:3,kb)
      i = 4  ;  j = 1
      do isi = la_topo%biefs(kb)%is1, la_topo%biefs(kb)%is2-1
         BT(i:i+3) = C(:,isi)
         i = i+4  ;  j = j+4
      enddo
      !CL aval : dernière ligne
      BT(isn) = TV(kb)

#ifdef debug
      !Recherche de NaN
      do j = 1, isn
         if (.not.(BT(j)<=zero) .and. .not.(BT(j)>=zero)) then
            nb_nan = nb_nan + 1
         endif
      enddo
      if (nb_nan > 0) then
         write(output_unit,'(a)') '>>>> Ouh la la ! Il y a plein de NaN ' &
                          ,'dans le second membre du système linéaire pour le bief ',kb,' !!!'
         bFail = .true.
         return
      endif
#endif /* debug */

      !Résolution
      call sgesl(AT(:isn,:isn,ib),isn,indx(:isn,ib),BT(:isn))

      !Récupération de la solution
      do isi = la_topo%biefs(kb)%is1, la_topo%biefs(kb)%is2
         i = 4*(isi-la_topo%biefs(kb)%is1)+1
         dq(isi)%qm = BT(i)  ;  dq(isi)%ql = BT(i+1)  ;  dq(isi)%qr = BT(i+2)
         dz(isi) = BT(i+3)
      enddo
   enddo
end subroutine ISM_resolution_sgefa



subroutine apply_solution(F,is)
!écrit la solution dq(is),dz(is) à partir des composantes du vecteur F
   implicit none
   ! -- prototype --
   real(kind=long),intent(in),dimension(4) :: F
   integer,intent(in) :: is

   dq(is)%qm = f(1) ; dq(is)%ql = f(2) ; dq(is)%qr = f(3) ; dz(is) = f(4)
   if (with_trace_small) write(l9,'(a,i3,3x,f10.3,4g14.6)') ' solution : ',is,la_topo%sections(is)%pk,dz(is), &
                                                                           dq(is)%ql,dq(is)%qm,dq(is)%qr
end subroutine apply_solution



subroutine add_rst_aval(ib,kb)
!ajoute la contribution du bief ib à la RST amont du bief kb
   implicit none
   ! -- prototype --
   integer,intent(in) :: ib, kb
   ! -- variables --
   integer :: is, indx(3), id, n
   real(kind=long) :: RR(3,3), SS(3), TT(3)

   is = la_topo%biefs(ib)%is2-1
   RR = E(2:4,1:3,is) ;  SS = E(2:4,4,is)  ;  TT = F(2:4,is)
   call LU_factorisation(RR,3,3,indx,id)
   if (id /= 0) then
      write(error_unit,*) ' >>>> Matrice 3x3 singulière dans add_rst_aval()'
      stop 4
   endif
   n = 3
   call LU_solution(RR,n,n,indx,SS)
   call LU_solution(RR,n,n,indx,TT)
   if (la_topo%biefs(ib)%gcd == -1) then !bief gauche
      SS(1) = SS(1) + SS(3)  ;  TT(1) = TT(1) + TT(3)  !le lit majeur droit va dans le lit mineur
      SS(3) = 0._long  ;  TT(3) = 0._long
   elseif (la_topo%biefs(ib)%gcd == +1) then !bief droit
      SS(1) = SS(1) + SS(2)  ;  TT(1) = TT(1) + TT(2)  !le lit majeur gauche va dans le lit mineur
      SS(2) = 0._long  ;  TT(2) = 0._long
   elseif (la_topo%biefs(ib)%gcd == 0) then !bief centré
      SS(1) = SS(1) + SS(2) + SS(3)  ;  TT(1) = TT(1) + TT(2) + TT(3) !les 2 lits majeurs vont dans le lit mineur
      SS(2) = 0._long  ;  SS(3) = 0._long  ;  TT(2) = 0._long  ;  TT(3) = 0._long
   endif
   CLM(1:3,4,kb) = CLM(1:3,4,kb) + SS
   tm(:,kb) = tm(:,kb) + TT
end subroutine add_rst_aval



subroutine initial_state_ISM(qt,zt)
!construction d'un état initial adapté pour ISM à partir d'une ligne d'eau standard
!le débit total est partagé entre les sous-sections au prorata de leur surface
!cela revient à supposer que les 3 lits ont la même vitesse moyenne.
!l'état initial résultant est dans Q et Z
   use StVenant_Debord, only: debord
   implicit none
   ! -- prototype --
   real(kind=long),dimension(:) :: qt, zt
   ! -- variables --
   integer :: is
   type(profil), pointer :: prfl
   real(kind=long) :: sm, sl, sr, st
   real(kind=long) :: Kmin, Kmoy, Qmin, Qmoy, Vmin, Vmoy, Deb, Beta, Q0, y, tmp
   character(len=80) :: message

   message = ' Construction de l''état initial pour ISM à partir d''un débit simple'
   write(l9,'(a)') trim(message)  ;  write(output_unit,'(a)') trim(message)

   do is = 1, la_topo%net%ns
      prfl => la_topo%sections(is)
      sl = prfl%section_mouillee(zt(is),1)
      sm = prfl%section_mouillee(zt(is),2)
      sr = prfl%section_mouillee(zt(is),3)
      st = sl+sm+sr
      z(is) = zt(is)
      Kmin = la_topo%sections(is)%ks(la_topo%sections(is)%main)
      Kmoy = prfl%strickler_lit_moyen(zt(is))
      Q0 = qt(is)
      y = z(is) - la_topo%sections(is)%zf
      call debord(is,y,Q0,Kmin,Kmoy,Qmin,Qmoy,Vmin,Vmoy,Deb,Beta)
      if (abs(Qmoy) > zero .and. sl+sr > zero) then
         tmp = Qmoy/(Q0*(sl+sr))
      else
         tmp = zero
      endif
      q(is)%ql = sl*tmp * qt(is) ;  q(is)%qr = sr*tmp * qt(is)
      q(is)%qm = qt(is) - q(is)%ql - q(is)%qr
      !write(l9,'(i3,3x,10f10.3)') is,prfl%pk,z(is),z(is)-zfd(is),q(is)%ql,q(is)%qm,q(is)%qr,Qmin,sl,sm,sr
   enddo

   !copie de l'état initial dans Mage_ini.ini
   call ISM_IniFin(nomini_ini)
end subroutine initial_state_ISM



subroutine convert_state_Debord_to_ISM(qt,zt)
!construction d'un état "initial" adapté pour ISM à partir d'une ligne d'eau standard
!le débit total est partagé entre les sous-sections au prorata de leur surface
!cela revient à supposer que les 3 lits ont la même vitesse moyenne.
!l'état initial résultant est dans Q et Z
   use StVenant_Debord, only: debord
   implicit none
   ! -- prototype --
   real(kind=long),dimension(:) :: qt, zt
   ! -- variables --
   integer :: is
   type(profil), pointer :: prfl
   real(kind=long) :: sm, sl, sr, st
   real(kind=long) :: Kmin, Kmoy, Qmin, Qmoy, Vmin, Vmoy, Deb, Beta, Q0, y, tmp

   do is = 1, la_topo%net%ns
      prfl => la_topo%sections(is)
      sl = prfl%section_mouillee(zt(is),1)
      sm = prfl%section_mouillee(zt(is),2)
      sr = prfl%section_mouillee(zt(is),3)
      st = sl+sm+sr
      z(is) = zt(is)
      Kmin = la_topo%sections(is)%ks(la_topo%sections(is)%main)
      Kmoy = prfl%strickler_lit_moyen(zt(is))
      Q0 = qt(is)
      y = z(is) - la_topo%sections(is)%zf
      call debord(is,y,Q0,Kmin,Kmoy,Qmin,Qmoy,Vmin,Vmoy,Deb,Beta)
      if (abs(Qmoy) > zero .and. st > zero .and. sl+sr > 0.01_long*st) then
         tmp = Qmoy/(sl+sr)
      else
         tmp = zero
      endif
      q(is)%ql = sl*tmp ;  q(is)%qr = sr*tmp
      q(is)%qm = qt(is) - q(is)%ql - q(is)%qr
   enddo
end subroutine convert_state_Debord_to_ISM



subroutine retour_arriere_ISM(bool1,indic)
!restauration de l'état à la fin du pas de temps précédent
   use data_num_fixes, only: plus, tinf, dtmin
   use data_num_mobiles, only: t, dt, dtold
   use erreurs_stv, only: errmax,errsup,ermax0
   implicit none
   ! -- prototype --
   logical,intent(out) :: bool1
   integer,intent(out) :: indic
   ! -- variables --
   integer, pointer :: ismax
   character(len=180) :: err_message
   character(len=19) :: hms

   hms = heure(t,dtmin)
   write(l9,'(3a,f6.2,a)') plus//'>>>> RETOUR EN ARRIERE (ISM) : mauvaise discrétisation à ',hms,' (dt = ',dt,') <<<<'
   indic = -1
   bool1 = .false.

   ismax => la_topo%net%ns
   q(1:ismax) = q(1:ismax) - dq_old(1:ismax)
   z(1:ismax) = z(1:ismax) - dz_old(1:ismax)

   if (zegal(errmax,errsup,un)) errmax=ermax0
   t = max(tinf,t-dt-dtold)
   !dt = dtold  !je ne comprends pas pourquoi on aurait besoin de ça.
   if (t <= tinf .and. dt <= dtmin) then
      write(err_message,'(a)') ' >>>> La ligne d''eau initiale ne permet pas de démarrer le calcul'
      write(l9,'(a)') trim(err_message)
      write(lTra,'(a)') trim(err_message)
      write(error_unit,'(a)') trim(err_message)
      stop 103
   endif
end subroutine retour_arriere_ISM



subroutine save_previous_iteration_ISM(ez0,eq0)
!copie la solution courante pour pouvoir comparer 2 itérations successives
   use Data_Iterations_loc, only: ez, eq
   implicit none
   ! -- prototype --
   real(kind=long),intent(out) :: ez0, eq0

   ez0 = ez  ;  eq0 = eq
   dq0 = dq  ;  dz0 = dz
end subroutine save_previous_iteration_ISM



subroutine init_solution_ISM
!initialise la solution courante à zéro
   implicit none

   dz = zero    ;  dz0 = zero
   dq = q_zero  ;  dq0 = q_zero
end subroutine init_solution_ISM



subroutine ecrit_E_F(is)
! écriture de résultats intermédiaires (mode debug)
   implicit none
   ! -- prototype --
   integer,intent(in) :: is
   ! -- variables --
   integer :: i, j

   !if (2 > 1) return  !pour désactiver l'écriture
   write(l9,*) ' >>>> Matrices E et second membre F pour section ',is
   do i = 1, 4
      write(l9,'(10e25.16)') (E(i,j,is),j=1,4),F(i,is)
   enddo
   write(l9,*)
   if (is_zero(maxval(E(:,:,is))) .and. is_zero(minval(E(:,:,is)))) then
      write(error_unit,*) '>>>> ERREUR 3 dans ISM_balayage_1_amont_aval : matrice E nulle à ',is
   endif
end subroutine ecrit_E_F



subroutine ecrit_systlin(En,Fn,n,is)
! écriture de résultats intermédiaires (mode debug)
   implicit none
   ! -- prototype --
   integer,intent(in) :: n, is
   real(kind=long),intent(in) :: En(n,n), Fn(n)
   integer :: i, j

   write(l9,'(a,i0)') ' >>>> Système linéaire En.X = Fn à la section ',is
   do i = 1, n
      write(l9,'(10e25.16)') (En(i,j),j=1,n),Fn(i)
   enddo
   write(l9,*)
end subroutine ecrit_systlin



subroutine ecrit_A_B_C(is)
! écriture de résultats intermédiaires (mode debug)
   implicit none
   ! -- prototype --
   integer,intent(in) :: is
   ! -- variables --
   integer :: k, j, isi, isj

   !if (2 > 1) return  !pour désactiver l'écriture
   isi = is  ;  isj = isi+1
   write(l9,*)
   write(l9,'(a,i0,a,i0)') '>>>> Matrices A et B, vecteur C pour la maille ',isi,' - ',isj
   do k = 1, 4
      write(l9,'(5x,10g14.6)') (a(k,j,isi),j=1,4),(b(k,j,isi),j=1,4),c(k,isi)
   enddo
   write(l9,'(4x,a,10g14.6)') ' S  : ',hd0(isi)%sm,hd0(isj)%sm,hd0(isi)%sl,hd0(isj)%sl,hd0(isi)%sr,hd0(isj)%sr
   write(l9,'(4x,a,10g14.6)') ' J  : ',hd0(isi)%jm,hd0(isj)%jm,hd0(isi)%jl,hd0(isj)%jl,hd0(isi)%jr,hd0(isj)%jr
   write(l9,'(4x,a,10g14.6)') ' Q  : ',q(isi)%qm,q(isj)%qm,q(isi)%ql,q(isj)%ql,q(isi)%qr,q(isj)%qr
   write(l9,'(4x,a,10g14.6)') ' JD : ',hd0(isi)%jdm,hd0(isj)%jdm,hd0(isi)%jdl,hd0(isj)%jdl,hd0(isi)%jdr,hd0(isj)%jdr
   write(l9,*)
end subroutine ecrit_A_B_C



subroutine normalize_E_F(is)
!normalisation de la matrice E et du vecteur F
   implicit none
   ! -- prototype --
   integer,intent(in) :: is
   ! -- variables --
   real(kind=long) :: w

   ! il ne faut pas toucher à la 1ère ligne car il y a un terme unité à gauche du bloc E
!   w = sqrt(dot_product(E(2,:,is),E(2,:,is))) ; E(2,:,is) = E(2,:,is)/w ; F(2,is) = F(2,is)/w
!   w = sqrt(dot_product(E(3,:,is),E(3,:,is))) ; E(3,:,is) = E(3,:,is)/w ; F(3,is) = F(3,is)/w
!   w = sqrt(dot_product(E(4,:,is),E(4,:,is))) ; E(4,:,is) = E(4,:,is)/w ; F(4,is) = F(4,is)/w
   w = maxval(abs(E(2,:,is))) ; E(2,:,is) = E(2,:,is)/w ; F(2,is) = F(2,is)/w
   w = maxval(abs(E(3,:,is))) ; E(3,:,is) = E(3,:,is)/w ; F(3,is) = F(3,is)/w
   w = maxval(abs(E(4,:,is))) ; E(4,:,is) = E(4,:,is)/w ; F(4,is) = F(4,is)/w
!   w = maxval(abs(E(2:4,:,is)))
!   E(2,:,is) = E(2,:,is)/w ; F(2,is) = F(2,is)/w
!   E(3,:,is) = E(3,:,is)/w ; F(3,is) = F(3,is)/w
!   E(4,:,is) = E(4,:,is)/w ; F(4,is) = F(4,is)/w
end subroutine normalize_E_F



subroutine set_CL_amont(ib)
!Remplit les matrices CLM et TM pour le bief ib
   use StVenant_Debord, only: debord
   use Data_Num_Fixes, only: fp_model
   use Data_Num_Mobiles, only: consecutive_ISM_timesteps
   use Conditions_Limites, only: cl0_ISM
   implicit none
   ! -- prototype --
   integer,intent(in) :: ib
   ! -- variables --
   integer :: nb, kb, is1, is_ref
   real(kind=long) :: qam, sl, sr
   real(kind=long) :: Kmin, Kmoy, Qmin, Qmoy, Vmin, Vmoy, Deb, Beta, Q0, y, tmp
   type(profil),pointer :: upstream_prof
   integer, pointer :: ibg, ibd, ibc
   type(debits_ISM) :: qam_ISM

   kb = la_topo%net%numero(ib)
   nb = la_topo%biefs(kb)%nam
   if (la_topo%nodes(nb)%cl > 0) then
      !ib est bief amont du modèle
      CLM(1,1,kb) = 1._long  ;  CLM(2,2,kb) = 1._long  ;  CLM(3,3,kb) = 1._long
      is1 = la_topo%biefs(kb)%is1
      upstream_prof => la_topo%sections(is1)
      if (la_topo%nodes(nb)%cl == 3) then
         ! C.L. amont de type ISM : 3 débits partiels
         qam_ISM = cl0_ISM(nb,t)
      else
         ! C.L. amont avec débit global
         if (fp_model == i_ism .or. consecutive_ISM_timesteps > 15) then
            ! estimation de la répartition des débits en reprenant celle obtenue à la section suivante
            qam = cl(nb,t)
            is_ref = is1+1
            Q0 = Q_total(q(is_ref))
            qam_ISM%qm = q(is_ref)%qm/Q0*qam
            qam_ISM%ql = q(is_ref)%ql/Q0*qam
            qam_ISM%qr = q(is_ref)%qr/Q0*qam
         else
            ! estimation de la répartition des débits entre sous-sections par Debord
            ! dans les cas mixtes où on remplace ISM par Debord si ISM échoue
            qam = cl(nb,t)
            Q0 = Q_total(q(is1))
            Kmin = upstream_prof%ks(upstream_prof%main)
            Kmoy = upstream_prof%strickler_lit_moyen(z(is1))
            y = z(is1) - upstream_prof%zf
            call debord(is1,y,Q0,Kmin,Kmoy,Qmin,Qmoy,Vmin,Vmoy,Deb,Beta)
            sl = upstream_prof%section_mouillee(z(is1),1)
            sr = upstream_prof%section_mouillee(z(is1),3)
            if (abs(Qmoy) > zero .and. sl+sr > zero) then
               tmp = Qmoy/(Q0*(sl+sr))
            else
               tmp = zero
            endif
            qam_ISM%qm = Qmin/Q0*qam
            qam_ISM%ql = sl*tmp*qam
            qam_ISM%qr = sr*tmp*qam
         endif
      endif
      tm(1,kb) = qam_ISM%qm-q(is1)%qm
      tm(2,kb) = qam_ISM%ql-q(is1)%ql
      tm(3,kb) = qam_ISM%qr-q(is1)%qr

   else
      !ib / kb est à l'aval d'un autre bief ou d'un confluent
      ibg => la_topo%biefs(kb)%ibg ; ibd => la_topo%biefs(kb)%ibd ; ibc => la_topo%biefs(kb)%ibc
      CLM(1,1,kb) = 1._long  ;  CLM(2,2,kb) = 1._long  ;  CLM(3,3,kb) = 1._long
      if (ibc > 0 .and. ibg == 0 .and. ibd == 0) then
         !un seul bief amont -> correspondance sous-section à sous-section
         !la RST aval se transmet telle quelle.
         ! TODO: il faudra ajouter les apports ponctuels
         CLM(:,:,kb) = CLM(:,:,ibc)
         tm(:,kb)    = tm(:,ibc)
      elseif (ibg > 0 .and. ibd > 0 .and. ibc == 0) then
         !2 biefs amont (gauche et droit)
         call add_rst_aval(ibg,kb)
         call add_rst_aval(ibd,kb)
      else
         write(error_unit,*) ' >>>> ERREUR : les autres cas de confluence ne sont pas implémentés !!!'
      endif
   end if
   if (with_trace_full) write(l9,*) ' C.L. amont : ',kb,tm(1,kb),tm(2,kb),tm(3,kb),Qmin,Q0,qam,q(is1)%qm
end subroutine set_CL_amont



subroutine set_CL_aval(ib)
!Remplit les matrices CLV et TV pour le bief ib
   implicit none
   ! -- prototype --
   integer,intent(in) :: ib
   ! -- variables --
   integer :: nb, kb, isn, lb
   real(kind=long) :: temp1, temp2, w
   real(kind=long), parameter :: dh = 0.0005_long

   kb = la_topo%net%numero(ib)
   nb = la_topo%biefs(kb)%nav
   isn = la_topo%biefs(kb)%is2
   if (la_topo%nodes(nb)%cl < 0) then
      !ib est bief aval du modèle
      if (la_topo%nodes(nb)%cl /= -3) then
      !loi Q(Z) ou cote critique
         CLV(1,kb) = 1._long ; CLV(2,kb) = 1._long ; CLV(3,kb) = 1._long
         temp1 = z(isn)+dh ; temp2 = z(isn)-dh
         CLV(4,kb) = -0.5_long * (cl(nb,temp1)-cl(nb,temp2)) / dh
         TV(kb) = cl(nb,z(isn))-Q_total(q(isn))
      else
      !cas d'une Z(t) aval
         CLV(1,kb) = 0._long ; CLV(2,kb) = 0._long ; CLV(3,kb) = 0._long
         CLV(4,kb) = 1._long
         TV(kb) = cl(nb,t) - z(isn)
      endif
   else
      !on crée une CL aval en Z(t) à partir de la solution à l'amont du bief aval
      lb = la_topo%net%lbz(nb,1)  !rang du 1er bief qui part du nœud nb
      lb = la_topo%net%numero(lb)
      CLV(1,kb) = 0._long ; CLV(2,kb) = 0._long ; CLV(3,kb) = 0._long
      CLV(4,kb) = 1._long
      TV(kb) = dz(la_topo%biefs(lb)%is1)
   endif
   w = maxval(abs(CLV(:,kb))) ; CLV(:,kb) = CLV(:,kb)/w ; TV(kb) = TV(kb)/w
   if (with_trace_full) write(l9,*) ' C.L. aval : ', kb, la_topo%nodes(nb)%cl, CLV(4,kb), TV(kb), w
end subroutine set_CL_aval



subroutine ISM_actualize_Q_Z
! actualisation des débits et cotes pour ISM et Debord
   use data_num_fixes, only: c_lissage
   implicit none
   ! -- variables --
   integer, pointer :: ismax
   integer :: isi, is, ib, ism, isp, isa, isa1, isz, isz1
   real(kind=long) :: dxm, dxp, alpha
   type(profil),pointer :: prof

   ismax => la_topo%net%ns
   ! copie de la solution actuelle
   dq_old(1:ismax) = dq(1:ismax)
   dz_old(1:ismax) = dz(1:ismax)
   ! actualisation pour ISM
   q(1:ismax) = q(1:ismax)+dq(1:ismax)
   z(1:ismax) = z(1:ismax)+dz(1:ismax)

   ! correction dans le cas débit en lit majeur sans débordement
   do isi = 1, ismax
      prof => la_topo%sections(isi)
      if (is_not_overflowing(prof,z(isi),LEFT) .AND. abs(q(isi)%ql) > zero) then
         q(isi)%qm = q(isi)%qm+q(isi)%ql
         q(isi)%ql = zero
      endif
      if (is_not_overflowing(prof,z(isi),RIGHT) .AND. abs(q(isi)%qr) > zero) then
         q(isi)%qm = q(isi)%qm+q(isi)%qr
         q(isi)%qr = zero
      endif
   enddo

   if (c_lissage > 1.e-05_long) then
      !---Lissage bief par bief pour simuler les chocs faibles (pseudo-viscosité)
      do ib = 1,la_topo%net%nb
         isa = la_topo%biefs(ib)%is1 ; isz = la_topo%biefs(ib)%is2
         isa1 = isa+1 ; do while(la_topo%sections(isa1)%iss /= 0)   ; isa1 = isa1+1 ; enddo
         isz1 = isz-1 ; do while(la_topo%sections(isz1+1)%iss /= 0) ; isz1 = isz1-1 ; enddo
         do is = isa1,isz1
            ism = is-1 ; do while(la_topo%sections(ism+1)%iss /= 0) ; ism = ism-1 ; enddo
            isp = is+1 ; do while(la_topo%sections(isp)%iss /= 0)   ; isp = isp+1 ; enddo
            dxp = abs(xgeo(isp)-xgeo(is))
            dxm = abs(xgeo(is)-xgeo(ism))
            alpha = c_lissage/(dxm+dxp)
            q(is)%qm = q(is)%qm + alpha*(dxm*q(isp)%qm+dxp*q(ism)%qm-(dxm+dxp)*q(is)%qm)
            q(is)%ql = q(is)%ql + alpha*(dxm*q(isp)%ql+dxp*q(ism)%ql-(dxm+dxp)*q(is)%ql)
            q(is)%qr = q(is)%qr + alpha*(dxm*q(isp)%qr+dxp*q(ism)%qr-(dxm+dxp)*q(is)%qr)
            z(is)    = z(is)    + alpha*(dxm*z(isp)+dxp*z(ism)-(dxm+dxp)*z(is))
         enddo
      enddo
   endif

   if (with_trace_full) then
      do isi = 1, ismax
         write(l9,'(a,i3,3x,f10.3,4g14.6)') ' solution : ',isi,la_topo%sections(isi)%pk,z(isi),q(isi)%ql,q(isi)%qm,q(isi)%qr
      enddo
   endif
end subroutine ISM_actualize_Q_Z



subroutine ISM_IniFin(nomfin)
!==============================================================================
!        Écriture de la dernière ligne d'eau au format .INI
!
!==============================================================================
   use parametres, only: gravite=>g, rivg, rivd, total
   use data_num_fixes, only: encodage
   use data_num_mobiles, only: t,dt
   use casiers, only: qds1, cdlat, ydev
   use mage_results, only: date_fin, type_resultat, pm, z_fd, largeur_totale, largeur_mineur, z_max, qtot, qfp
   implicit none
   ! -- le prototype --
   character(len=12),intent(in) :: nomfin
   ! -- les constantes --
   integer, parameter :: nl=284
   ! -- les variables --
   integer :: ib,is,rive,ltmp,is_local, li1, li2
   real(kind=long) :: hauteur,largeur_miroir,section_mouillee,perimetre_mouille,froude,dx,qdev
   real(kind=long) :: zdev,akmin
   character :: debt_left*2,debt_right*2,devt_left*1,devt_right*1,ib_text*3
   character(nl),save :: ligne,ligne0 = repeat('*',nl)
   !logical,save :: appel1=.true.
   real(kind=long) :: akg, akd, qtotal, tau_lm, tau_rm, hfp_left, hfp_right, V_int_left, V_int_right
   type(hydraulic_data), pointer :: hd
   character(len=19) :: hms
   integer :: values(8)
   character(len=8) :: ddate
   character(len=10) :: ttime
   character(len=5) :: zone
   type(profil), pointer :: prf
   class(point3D), pointer :: p3D(:)

   call date_and_time(ddate,ttime,zone,values)
   date_fin = int((t-dt)/60._long) !on en besoin pour Mage_Results
   hms = heure(t-dt)

   open(newunit=ltmp,file=nomfin,status='unknown',form='formatted',encoding=encodage)
   write(Ltmp,'(a)') Ligne0
   write(ligne,'(3a,i4.4,5(a1,i2.2))') '* État final par Mage avec la méthode ISM à T = ',trim(hms), &
               ' Date simulation : ',values(1),'-',values(2),'-',values(3),' ',values(5),':',values(6),':',values(7)
   write(Ltmp,'(a)') trim(Ligne)
   write(Ltmp,'(a)') '*'
   Ligne='* ATTENTION : en lecture, seuls les débits et cotes sont utilisés'
   write(Ltmp,'(a)') Ligne
   Ligne='* un # dans la colonne H_FP_rel (profondeur) indique un débordement en lit moyen'
   write(Ltmp,'(a)') trim(Ligne)
   Ligne='* un ! dans la colonne H_FP_rel (profondeur) indique un dépassement de la cote de berge la plus basse'
   write(Ltmp,'(a)') trim(Ligne)
   write(Ltmp,'(a)') Ligne0
   write(Ltmp,'(3a,i4.4,5(a1,i2.2))') '@ date finale = ',trim(hms), &
                           ' Date simulation : ',values(1),'-',values(2),'-',values(3),' ',values(5),':',values(6),':',values(7)
   write(ltmp,1001)
   do ib = 1, la_topo%net%nb
      do is = la_topo%biefs(ib)%is1, la_topo%biefs(ib)%is2
         prf => la_topo%sections(is)
         p3D(1:) => prf%xyz(:)
         Hauteur = Z(is)-Zfd(is)
         li1 = prf%li(1) ; li2 = prf%li(2)  !limites du lit mineur
         Hfp_left  = 100._long*max(zero,Z(is) - p3D(li1)%z) / Hauteur
         Hfp_right = 100._long*max(zero,Z(is) - p3D(li2)%z) / Hauteur
         !marqueurs
         Debt_left = '  ' ; Debt_right = '  ' ; Devt_left = ' ' ; Devt_right = ' '
         if (Hfp_left > zero) Debt_left = ' #'                       ! débordement en lit moyen gauche
         if (Hfp_right > zero) Debt_right = ' #'                     ! débordement en lit moyen gauche
         if (Hauteur > prf%ybgau) Devt_left = '!'   ! dépassement de la cote de berge gauche
         if (Hauteur > prf%ybdro) Devt_right = '!'  ! dépassement de la cote de berge droite

         largeur_miroir    = prf%largeur(z(is))      ! largeur au miroir
         section_mouillee  = prf%section_mouillee(z(is))      ! surface mouillée
         perimetre_mouille = prf%perimetre(z(is))    ! périmètre mouillé
         qtotal = Q_total(q(is))
         Froude = abs(Qtotal)*sqrt(Largeur_miroir/(gravite*section_mouillee**3))
         if (is < la_topo%biefs(ib)%is2) then
            Dx = abs(xgeo(is+1)-xgeo(is)) ! pas d'espace
            Qdev = Qds1(Total,is)*Dx      ! débit déversé
            Zdev = 100000._long
            do rive = rivg,rivd
               if (CDlat(rive,is)>0.0001_Long) then
                  Zdev = min(Zdev,Zfond(is)+Ydev(rive,is))  ! cote de déversement
               else
                  Zdev = min(Zdev,Zfond(is)+prf%ybmin)      ! cote de berge
               endif
            enddo
         endif
         if (ib < 100) then
            write(ib_text,'(i2,a)') ib,' '
         else
            write(ib_text,'(i3)') ib
         endif
         hd => hd1(is)  ;  call compute_hydraulic_data(hd,is,z(is),q(is))
         is_local = is-la_topo%biefs(ib)%is1+1
         akg = prf%Ks(1)
         akmin = prf%Ks(prf%main)
         akd = prf%Ks(3)
         tau_lm = 1000._long*prf%psi_t*(hd%vm-hd%vl)**2
         tau_rm = 1000._long*prf%psi_t*(hd%vm-hd%vr)**2
         if (is < la_topo%biefs(ib)%is2) then
            call compute_hydraulic_data(hd1(is+1),is+1,z(is+1),q(is+1))
            V_int_left  = V_interface(hd1,qml(is),is,.true. ,weight_Vint)
            V_int_right = V_interface(hd1,qmr(is),is,.false.,weight_Vint)
         else
            V_int_left = zero
            V_int_right = zero
         endif

         !Enfin on peut écrire
         if (qtotal > 10000._long) then
            write(ltmp,1004) ib_text,is_local,qtotal,z(is),xgeo(is),zfond(is),zfond(is)+prf%ymoy,zdev,Hfp_left,  &
                             debt_left,devt_left,hfp_right,debt_right,devt_right,largeur_miroir,froude,akg,akmin,akd,             &
                             q(is)%ql,q(is)%qm,q(is)%qr,hd%vl,hd%vm,hd%vr,qtotal/section_mouillee,qml(is),qmr(is),tau_lm,tau_rm,  &
                             V_int_left,V_int_right
         elseif (qtotal > -1000._long) then
            write(ltmp,1002) ib_text,is_local,qtotal,z(is),xgeo(is),zfond(is),zfond(is)+prf%ymoy,zdev,Hfp_left,  &
                             debt_left,devt_left,hfp_right,debt_right,devt_right,largeur_miroir,froude,akg,akmin,akd,             &
                             q(is)%ql,q(is)%qm,q(is)%qr,hd%vl,hd%vm,hd%vr,qtotal/section_mouillee,qml(is),qmr(is),tau_lm,tau_rm,  &
                             V_int_left,V_int_right
         else
            write(ltmp,1003) ib_text,is_local,qtotal,z(is),xgeo(is),zfond(is),zfond(is)+prf%ymoy,zdev,Hfp_left,  &
                             debt_left,devt_left,hfp_right,debt_right,devt_right,largeur_miroir,froude,akg,akmin,akd,             &
                             q(is)%ql,q(is)%qm,q(is)%qr,hd%vl,hd%vm,hd%vr,qtotal/section_mouillee,qml(is),qmr(is),tau_lm,tau_rm,  &
                             V_int_left,V_int_right
         endif
         ! sauvegarde des résultats dans Mage_Results
         if (type_resultat == 'FIN' .or. type_resultat == 'END') then
            pm(is)             = xgeo(is)
            z_fd(is)           = zfd(is)
            largeur_Totale(is) = largeur_miroir
            largeur_mineur(is) = prf%almy
            Z_max(is)          = Z(is)
            qtot(is)           = qtotal
            qfp(is)            = q(is)%ql+q(is)%qr
         endif
      enddo
   enddo
   close(ltmp)

! NOTE: ne pas mettre de caractère accentué dans les en-têtes car ils comptes pour 2 caractères ascii et cela complique l'analyse
   1001 format('* IB IS      Debit       Cote         Pm      Z_Fond    Z_moyen    Z_Berge  H_FP_g%   H_FP_d%   ', &
               'Largeur   Fr   K_gauche  K_mineur  K_droite   Q_gauche    Q_mineur    Q_droite    V_gauche   V_mineur   ',&
               'V_droite   V_moyenne   Q_ex_G     Q_ex_D     tau_lm    tau_rm  V_int_left  V_int_right')
   1002 format(1x,a3,i4,2x,f10.5,1x,f11.6,1x,f10.3,f11.6,1x,f9.3,1x,f9.3,2(1x,f6.2,a2,a1),f9.2,1x,f6.3,1x,2(f7.3,3x),f7.3,2x,&
               3(en12.3,1x),2x,f7.4,3(4x,f7.4),2(2x,e10.3),2x,4(f8.4,3x))
   1003 format(1x,a3,i4,2x,f10.3,1x,f11.6,1x,f10.3,f11.6,1x,f9.3,1x,f9.3,2(1x,f6.2,a2,a1),f9.2,1x,f6.3,1x,2(f7.3,3x),f7.3,2x,&
               3(en12.3,1x),2x,f7.4,3(4x,f7.4),2(2x,e10.3),2x,4(f8.4,3x))
   1004 format(1x,a3,i4,2x,f10.4,1x,f11.6,1x,f10.3,f11.6,1x,f9.3,1x,f9.3,2(1x,f6.2,a2,a1),f9.2,1x,f6.3,1x,2(f7.3,3x),f7.3,2x,&
               3(en12.3,1x),2x,f7.4,3(4x,f7.4),2(2x,e10.3),2x,4(f8.4,3x))
end subroutine ISM_IniFin



function V_interface_downstream(hd,q_exchange,is,left,alpha)
! vitesse à l'interface mineur - majeur
   implicit none
   ! -- prototype --
   real(kind=long) :: V_interface_downstream
   type (hydraulic_data), intent(in) :: hd(*)
   real(kind=long), intent(in) :: q_exchange  !débit d'échange, positif si le débit va du majeur vers le mineur
   integer, intent(in) :: is
   logical, intent(in) :: left
   real(kind=long), intent(in) :: alpha
   ! -- variables locales --
   integer :: js
   character(len=8) :: dir
   character(len=19) :: hms

   js = is+1
   if (q_exchange > zero) then
      V_interface_downstream = 0.5_long*(hd(is)%vm+hd(js)%vm)
   else
      if (left) then
         V_interface_downstream = 0.5_long*(hd(is)%vl+hd(js)%vl)
      else
         V_interface_downstream = 0.5_long*(hd(is)%vr+hd(js)%vr)
      endif
   endif
   v_interface_downstream = (un+alpha)*v_interface_downstream

   if (with_trace_full .and. V_interface_downstream < 0._long) then
      dir = 'MC -> FP' ; if (q_exchange > zero) dir = 'FP -> MC'
      hms = heure(t)
      write(l9,*) 'V_interface_downstream < 0 à ',hms,' : ',trim(dir),xgeo(is),V_interface_downstream,q_exchange
   endif
end function V_interface_downstream



function V_interface_upstream(hd,q_exchange,is,left,alpha)
! vitesse à l'interface mineur - majeur
   use data_num_mobiles, only: t
   implicit none
   ! -- prototype --
   real(kind=long) :: V_interface_upstream
   type (hydraulic_data), intent(in) :: hd(*)
   real(kind=long), intent(in) :: q_exchange  !débit d'échange, positif si le débit va du majeur vers le mineur
   integer, intent(in) :: is
   logical, intent(in) :: left
   real(kind=long), intent(in) :: alpha
   ! -- variables locales --
   integer :: js
   character(len=8) :: dir
   character(len=19) :: hms
   real(kind=long) :: correction !on enlève alpha si Uint = U_MC et on ajoute alpha si Uint = U_FP

   js = is+1
   if (abs(q_exchange) < 1.0E-06_long) then
      dir = 'MC == FP'
      V_interface_upstream = V_interface_weighted(hd,q_exchange,is,left,0.5_long)
      correction = 1._long
   elseif (q_exchange > zero) then
      dir = 'FP -> MC'
      if (left) then
         V_interface_upstream = 0.5_long*(hd(is)%vl+hd(js)%vl)
      else
         V_interface_upstream = 0.5_long*(hd(is)%vr+hd(js)%vr)
      endif
      correction = un + abs(alpha)
   else
      dir = 'MC -> FP'
      V_interface_upstream = 0.5_long*(hd(is)%vm+hd(js)%vm)
      correction = un - abs(alpha)
   endif
   v_interface_upstream = correction*v_interface_upstream

   if (with_trace_full .and. V_interface_upstream < 0._long) then
      if (abs(q_exchange) < 1.0E-06_long) then
         dir = 'MC == FP'
      elseif (q_exchange > zero) then
         dir = 'FP -> MC'
      else
         dir = 'MC -> FP'
      endif
      hms = heure(t)
      write(l9,*) 'V_interface_upstream < 0 à ',hms,' : ',trim(dir),xgeo(is),V_interface_upstream,q_exchange
   endif
end function V_interface_upstream



function V_interface_weighted(hd,q_exchange,is,left,alpha)
! vitesse à l'interface mineur - majeur
   implicit none
   ! -- prototype --
   real(kind=long) :: V_interface_weighted
   type (hydraulic_data), intent(in) :: hd(*)
   real(kind=long), intent(in) :: q_exchange  !débit d'échange, positif si le débit va du majeur vers le mineur
   integer, intent(in) :: is
   logical, intent(in) :: left
   real(kind=long), intent(in) :: alpha
   ! -- variables locales --
   integer :: js
   real(kind=long) :: V_mc, V_fp
   character(len=8) :: dir
   character(len=19) :: hms

   js = is+1
   V_mc = 0.5_long * (hd(is)%vm+hd(js)%vm)
   if (left) then
      V_fp = 0.5_long * (hd(is)%vl+hd(js)%vl)
   else
      V_fp = 0.5_long * (hd(is)%vr+hd(js)%vr)
   endif
   V_interface_weighted = alpha*V_mc + (1._long-alpha)*V_fp

   if (with_trace_full .and. V_interface_weighted < 0._long) then
      if (abs(q_exchange) < 1.0E-06_long) then
         dir = 'MC == FP'
      elseif (q_exchange > zero) then
         dir = 'FP -> MC'
      else
         dir = 'MC -> FP'
      endif
      hms = heure(t)
      write(l9,*) 'V_interface_weighted < 0 à ',hms,' : ',trim(dir),xgeo(is),V_interface_weighted,q_exchange
   endif
end function V_interface_weighted



function V_interface_yen(hd,q_exchange,is,left,alpha)
! vitesse à l'interface mineur - majeur
   use data_num_mobiles, only: t
   implicit none
   ! -- prototype --
   real(kind=long) :: V_interface_yen
   type (hydraulic_data), intent(in) :: hd(*)
   real(kind=long), intent(in) :: q_exchange  !débit d'échange, positif si le débit va du majeur vers le mineur
   integer, intent(in) :: is
   logical, intent(in) :: left
   real(kind=long), intent(in) :: alpha
   ! -- variables locales --
   integer :: js
   real(kind=long) :: V_mc, V_fp, B_fp, B_mc
   character(len=8) :: dir
   character(len=19) :: hms

   js = is+1
   V_mc = 0.5_long * (hd(is)%vm+hd(js)%vm)
   B_mc = 0.5_long * (hd(is)%lm+hd(js)%lm)
   if (left) then
      V_fp = 0.5_long * (hd(is)%vl+hd(js)%vl)
      B_fp = 0.5_long * (hd(is)%ll+hd(js)%ll)
   else
      V_fp = 0.5_long * (hd(is)%vr+hd(js)%vr)
      B_fp = 0.5_long * (hd(is)%lr+hd(js)%lr)
   endif
   V_interface_yen = (B_mc*V_fp + B_fp*V_mc) / (B_mc+B_fp)

   if (with_trace_full .and. V_interface_yen < 0._long) then
      if (q_exchange > zero) then  !sens échange : majeur --> mineur
         dir = 'FP -> MC'
      else                         !sens échange : mineur --> majeur
         dir = 'MC -> FP'
      endif
      hms = heure(t)
      write(l9,*) 'V_interface_yen < 0 à ',hms,' : ',trim(dir),xgeo(is),V_interface_yen,q_exchange
   endif
end function V_interface_yen



function V_interface_biggest(hd,q_exchange,is,left,alpha)
! vitesse à l'interface mineur - majeur
   use data_num_mobiles, only: t
   implicit none
   ! -- prototype --
   real(kind=long) :: V_interface_biggest
   type (hydraulic_data), intent(in) :: hd(*)
   real(kind=long), intent(in) :: q_exchange  !débit d'échange, positif si le débit va du majeur vers le mineur
   integer, intent(in) :: is
   logical, intent(in) :: left
   real(kind=long), intent(in) :: alpha
   ! -- variables locales --
   integer :: js
   real(kind=long) :: V_amont, V_aval
   character(len=8) :: dir
   character(len=19) :: hms

   js = is+1
   if (q_exchange > zero) then  !sens échange : majeur --> mineur
      V_aval = 0.5_long * (hd(is)%vm+hd(js)%vm)
      if (left) then
         V_amont = 0.5_long * (hd(is)%vl+hd(js)%vl)
      else
         V_amont = 0.5_long * (hd(is)%vr+hd(js)%vr)
      endif
   else                         !sens échange : mineur --> majeur
      V_amont = 0.5_long * (hd(is)%vm+hd(js)%vm)
      if (left) then
         V_aval = 0.5_long * (hd(is)%vl+hd(js)%vl)
      else
         V_aval = 0.5_long * (hd(is)%vr+hd(js)%vr)
      endif
   endif
   if (abs(V_amont) > abs(V_aval)) then
      V_interface_biggest = (1._long+alpha) * V_amont
   else
      V_interface_biggest = (1._long+alpha) * V_aval
   endif
   if (with_trace_full .and. V_interface_biggest < 0._long) then
      if (q_exchange > zero) then  !sens échange : majeur --> mineur
         dir = 'FP -> MC'
      else                         !sens échange : mineur --> majeur
         dir = 'MC -> FP'
      endif
      hms = heure(t)
      write(l9,*) 'V_interface_biggest < 0 à ',hms,' : ',trim(dir),xgeo(is),V_interface_biggest,q_exchange
   endif
end function V_interface_biggest



pure function is_overbank(hd,side)
! indicateur qui détermine si le débordement est assez grand pour être pris en compte
   implicit none
   ! -- prototype --
   logical :: is_overbank
   type(hydraulic_data),intent(in) :: hd
   integer, intent(in) :: side
   ! -- variables --
   real(kind=long) :: hs
   integer :: n

   select case (side)
      case (LEFT)   ;  hs = hd%sl
      case (RIGHT)  ;  hs = hd%sr
      !on veut faire planter si on ne fournit pas une valeur correcte de side
      case default  ;  n = side ; hs = real(n /(n-side),kind=long)
   end select
   is_overbank = hs > 0.01_long * hd%st
end function is_overbank



pure function is_small_deb(hd,side)
! indicateur qui détermine si le débordement est petit et peut donc être négligé
   implicit none
   ! -- prototype --
   logical :: is_small_deb
   type(hydraulic_data),intent(in) :: hd
   integer, intent(in) :: side
   ! -- variables --
   real(kind=long) :: hs
   integer :: n

   select case (side)
      case (LEFT)   ;  hs = hd%hl
      case (RIGHT)  ;  hs = hd%hr
      !on veut faire planter si on ne fournit pas une valeur correcte de side
      case default  ;  n = side ; hs = real(n /(n-side),kind=long)
   end select
   is_small_deb = .not.is_overbank(hd,side) .and. hs > zero
end function is_small_deb



pure function froude_local(q,hd,lit)
! renvoie une estimation locale du nombre de froude
   use parametres, only: gravite => g
   implicit none
   ! -- prototype --
   real(kind=long) :: froude_local
   type(debits_ISM), intent(in) :: q       !débit
   type(hydraulic_data), intent(in) :: hd  !variables hydrauliques calculées par compute_hydraulic_data()
   integer, intent(in) :: lit              !numéro de la sous-section : 1 = majeur gauche, 2 = lit mineur, 3 = majeur droit

   select case (lit)
      case (1)  ;  froude_local = abs(q%ql)*sqrt(hd%ll/(gravite*hd%sl**3))
      case (2)  ;  froude_local = abs(q%qm)*sqrt(hd%lm/(gravite*hd%sm**3))
      case (3)  ;  froude_local = abs(q%qr)*sqrt(hd%lr/(gravite*hd%sr**3))
      case default  ;  froude_local = -un
   end select
end function froude_local



pure function is_not_overflowing(un_profil,zt,side)
! indicateur qui détermine si le débordement est assez grand pour être pris en compte
   implicit none
   ! -- prototype --
   logical :: is_not_overflowing
   type(profil),intent(in) :: un_profil
   real(kind=long),intent(in) :: zt
   integer,intent(in) :: side
   ! -- variables --
   integer :: li

   select case (side)
      case (LEFT)   ;  li = un_profil%li(1)  !lit majeur gauche (extrémité droite)
      case (RIGHT)  ;  li = un_profil%li(2)  !lit majeur droit (extrémité gauche) == extrémité droite du mineur
      !on veut faire planter si on ne fournit pas une valeur correcte de side
      case default  ;  li = side ; li = li /(li-side)
   end select
   is_not_overflowing = zt < un_profil%xyz(li)%z
end function is_not_overflowing



end module StVenant_ISM
