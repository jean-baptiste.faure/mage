!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - INRAE - 1989-2020                           #
!# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################


subroutine IniMage(REPname,cmdline)
!==============================================================================
!      pour l'explicitation des noms de variable consulter les fichiers
!                mage_DataModules.f90 et mage_Parametres.for
!==============================================================================
   use Parametres, only: Long, lname, lTra, l9
   use, intrinsic :: iso_fortran_env, only: output_unit

   use Data_Num_Fixes, only: Plus,silent,encodage
   implicit none
   ! -- Prototype
   character(len=lname),intent(in) :: REPname
   character(len=180),intent(in) :: cmdline
   ! -- Variables locales temporaires --
   logical :: bexist
   character(len=120) :: message=''
   integer :: ltmp
   !------------------------------------------------------------------------------
   plus = ' '
   !---Copie des sorties trop longues à afficher à l'écran (unite logique 9)
   inquire(file='output',exist=bexist)
   if (bexist) then
      open (unit=l9,file='output',form='formatted',status='old')
      close(unit=l9,status='delete')
   endif
   open (unit=l9,file='output',form='formatted',status='new',encoding=encodage)
   write(l9,'(2a)') ' Ligne de commande de la simulation : ',trim(cmdline)

   !---Initialisation des Variables des Modules
   call Init_All_DefaultDataModule

   !---Impression de la version du programme sur écran
   if (silent) then
      call version(l9)
   else
      call version(output_unit)
   endif
   !---Recherche des fichiers INIMAGE.*, destruction s'ils existent
   inquire(file='inimage.qla',exist=bexist)
   if (bexist) then
      open(newunit=ltmp,file='inimage.qla',form='unformatted',status='unknown',access='direct',recl=2*long)
      close(unit=ltmp,status='delete')
   endif
   inquire(file='inimage.ouv',exist=bexist)
   if (bexist) then
      open(newunit=ltmp,file='inimage.ouv',form='unformatted',status='unknown',access='direct',recl=2*long)
      close(unit=ltmp,status='delete')
   endif

   !---Suppression d'un éventuel fichier NUM
   inquire(file='num',exist=bexist)
   if (bexist) then
      open(newunit=ltmp,file='num',form='formatted',status='unknown')
      close(unit=ltmp,status='delete')
   endif


   !---catalogue et ouverture des fichiers
   call Lire_REP(REPname)
   !---Impression de la version du programme sur listing
   call version(lTra)
   write(lTra,'(2a)') ' Ligne de commande de la simulation : ',trim(cmdline)
   write(lTra,*)
   write(lTra,'(1x,78("-"))')
   !---Copie du répertoire sur le listing
   call Ecrire_REP
   write(lTra,'(1x,78("-"))')

   !---Initialisation
   call Lire_Data

   !---Fin du sous-programme IniMage
   write(lTra,'(/,1x,78("-"),/,1x)')
   message = ' ----> Fin normale de l''initialisation'
   if (silent) then
      write(l9,'(a)') trim(message)
   else
      write(output_unit,'(a)') trim(message)
   endif

end subroutine IniMage



subroutine Lire_REP(the_repFile)
!------------------------------------------------------------------------------
!                 catalogue et ouverture des fichiers
!------------------------------------------------------------------------------
   use parametres, only: long, lname, zero, l9, lTra, lBin, lErr, lBinGra
   use, intrinsic :: iso_fortran_env, only: output_unit, error_unit

   use booleens, only: deltmp
   use IO_Files, only: repFile, binFile, traFile, envFile, errFile, avaFile, casFile, &
                       devFile, hydFile, latFile, limFile, netFile, numFile, &
                       rugFile, sinFile, varFile, drgFile, psiFile, sedFile, &
                       qsoFile, parfile,&
                       chaFile, btmFile, qstFile, iniFile, graFile, cgnsFile
   use data_num_fixes, only: silent, dtcsv
   use Mage_Utilitaires, only: capitalize, next_string, next_real, next_int
   use data_csv, only: filtre, ncsv, csv_object
   use topoGeometrie, only: with_charriage
   implicit none
   ! -- Prototype
   character(len=lname),intent(in) :: the_repFile
   ! -- Variables --
   logical :: file_missing, bexist
   integer :: ios, lRep, lu, length, nf
   character(len=lname+4) :: Ligne='', Line_Cap=''
   character(len=120) :: message=''
   real(kind=long) :: dt0
   type(csv_object), allocatable, target :: filtre_tmp(:)


   deltmp = .true.
   repFile = the_repFile
   ncsv = 0
   dtcsv = 1.e30_long

   ! ouverture du fichier [.rep]
   length = len_trim(repFile)
   if (length > 0) then
      if (scan(repFile,'.') == 0) then
         repFile(length+1:length+4) = '.REP'
      endif
      inquire(file=trim(repFile),exist=bExist)
      if (.not.bExist) repFile = repFile(1:len_trim(repFile)-4)//'.rep'
      inquire(file=trim(repFile),exist=bExist)
      if (.not.bExist) then
         write(error_unit,*) '>>>> ERREUR : le fichier répertoire ',repFile(1:len_trim(repFile)-4)//'[.rep|.REP] n''existe pas'
         call echec_initialisation
         stop 1
      else
         open(newunit=lRep,file=repFile,status='old',form='formatted')
      endif
   else
      message = ' ERREUR : le nom du fichier REP est vide'
      write(error_unit,*) trim(message) ; write(output_unit,*) trim(message)
      call echec_initialisation
      stop 1
   endif
   lu = output_unit ; if (silent) lu = 9
   write(lu,'(/,2a)') ' Lecture du fichier répertoire : ',trim(repFile)

   !-----lecture du répertoire
   do
      read(lRep,'(a)',iostat=ios) ligne !sortie normale de la boucle 20-30
      if (ios < 0) exit ! fin de fichier : on sort
      if (ios > 0) then
         message = '>>>> Erreur dans le répertoire à la ligne : '
         write(output_unit,'(a)') trim(message)  ;  write(output_unit,'(a)') ligne
         write(error_unit,'(a)') trim(message)  ;  write(error_unit,'(a)') ligne
         close(lRep)
         call echec_initialisation
         stop 127
      endif
      ! on saute les commentaires
      if (ligne(1:1)=='*') cycle
      !conversion en majuscules
      call capitalize(ligne,line_cap)

! DONE: vérifier si cette confirmation a encore une utilité : aucune mais il faut pouvoir lire un fichier REP avec cette ligne
      if (line_cap(1:12)=='CONFIRMATION') cycle
! DONE: vérifier si la non-destruction des fichiers temporaires a encore une utilité : aucune mais il faut pouvoir lire un fichier REP avec cette ligne
! NOTE: il n'y a plus de cas d'utilisation de MAGE lancé avec relecture des fichiers temporaires
      if (line_cap(1:36)=='DESTRUCTION DES FICHIERS TEMPORAIRES') cycle

      select case (line_cap(1:3))
         ! fichiers résultats
         case ('BIN') ; binFile = ligne(5:lname+4)
         case ('ENV') ; envFile = ligne(5:lname+4)
         case ('ERR') ; errFile = ligne(5:lname+4)
         case ('TRA') ; traFile = ligne(5:lname+4)
         case ('CSV')
            ncsv = ncsv + 1
            nf = 5
            allocate(filtre_tmp(ncsv))
            filtre_tmp(ncsv)%prefix = next_string(ligne,' ',nf)
            filtre_tmp(ncsv)%var = next_string(ligne,' ',nf)
            filtre_tmp(ncsv)%ib = next_int(ligne,' ',nf)
            filtre_tmp(ncsv)%pk = next_real(ligne,' ',nf)
            filtre_tmp(ncsv)%mode = next_int(ligne,' ',nf)
            dt0 = next_real(ligne,' ',nf)
            if (dt0 > zero) dtcsv = min(dtcsv,dt0)
            call move_alloc(filtre_tmp, filtre)
         ! fichiers de données
         case ('AVA') ; avaFile = ligne(5:lname+4)
         case ('BTM') ; btmFile = ligne(5:lname+4)
         case ('CAS') ; casFile = ligne(5:lname+4)
         case ('CHA') ; chaFile = ligne(5:lname+4)
         case ('DEV') ; devFile = ligne(5:lname+4)
         case ('DRG') ; drgFile = ligne(5:lname+4)
         case ('HYD') ; hydFile = ligne(5:lname+4)
         case ('INI') ; iniFile = ligne(5:lname+4)
         case ('LAT') ; latFile = ligne(5:lname+4)
         case ('LIM') ; limFile = ligne(5:lname+4)
         case ('NET') ; netFile = ligne(5:lname+4)
         case ('NUM') ; numFile = ligne(5:lname+4)
         case ('PAR') ; parFile = ligne(5:lname+4)
         case ('PSI') ; psiFile = ligne(5:lname+4)
         case ('QSO') ; qsoFile = ligne(5:lname+4)
         case ('QST') ; qstFile = ligne(5:lname+4)
         case ('RUG') ; rugFile = ligne(5:lname+4)
         case ('SIN') ; sinFile = ligne(5:lname+4)
         case ('SED') ; sedFile = ligne(5:lname+4)
         case ('VAR') ; varFile = ligne(5:lname+4)
         case ('GRA') ; graFile = ligne(5:lname+4)
         case ('CGN') ; cgnsFile = ligne(5:lname+4)
         case default
            write(error_unit,'(3a)') '>>>> ERREUR : le type de fichier ',line_cap(1:3),' est inconnu'
            write(error_unit,'(3a)') '              la ligne correspondante de ',trim(repFile),' est ignorée'
      end select
   enddo
   close (lRep)

   !---demande des noms de fichiers indispensables manquant dans le répertoire
   file_missing = .false.
   if (netFile == '') then
      write(error_unit,*) ' >>>> ERREUR : il manque le nom du fichier NET pour la topologie du réseau'
      file_missing = .true.
   endif
   if (numFile == '' .and. parFile == '') then
      write(error_unit,*) ' >>>> ERREUR : il manque le nom du fichier NUM ou du fichier PAR des paramètres numériques'
      file_missing = .true.
   endif
   ! on donne priorité au fichier PAR (plus recent)
   if (parFile .ne. '') numFile = ''
   if (rugFile == '') then
      write(error_unit,*) ' >>>> ERREUR : il manque le nom du fichier RUG des coefficients de Strickler'
      file_missing = .true.
   endif
   if (hydFile == '') then
      write(error_unit,*) ' >>>> ERREUR : il manque le nom du fichier HYD des apports amont'
      file_missing = .true.
   endif
   if (binFile == '') then
      write(error_unit,*) ' >>>> ERREUR : il manque le nom du fichier de sortie BIN'
      file_missing = .true.
   endif
   if (traFile == '') then
      write(error_unit,*) ' >>>> ERREUR : il manque le nom du fichier de sortie TRA'
      file_missing = .true.
   endif
   if (varFile /= '' .AND. sinFile == '') then
      write(error_unit,*) ' >>>> ERREUR : il y a un fichier VAR mais pas de fichier SIN pour les ouvrages'
      file_missing = .true.
   endif
   if (sedFile == '' .and. with_charriage == 2) then
      write(error_unit,*) ' >>>> ERREUR : il manque le nom du fichier des couches sédimentaires SED'
      file_missing = .true.
   endif
   if (qsoFile == '' .and. with_charriage > 0) then
      write(error_unit,*) ' >>>> ERREUR : il manque le nom du fichier de C.L. solides QSO'
      file_missing = .true.
   endif
   if (graFile == '' .and. with_charriage > 0) then
      write(error_unit,*) ' >>>> ERREUR : il manque le nom du fichier de sortie GRA'
      file_missing = .true.
   endif

   if (file_missing) then
      call echec_initialisation
      stop 214
   endif

   !---Ouverture des fichiers de résultats indispensables
   ! NOTE: il faut que ces fichiers soient ouverts avant les fichiers de données
   ! NOTE: cela permet d'éviter des conflits sur des unités logiques déjà utilisées
   open(unit=lTra,file=trim(traFile),status='unknown',form='formatted',iostat=ios)
   if (ios > 0) then
      message = ' >>>> Erreur lors de l''ouverture du fichier : '//trim(traFile)
      write(error_unit,'(a)') trim(message) ; write(l9,'(a)') trim(message)
      stop 128
      call echec_initialisation
   endif
   if (with_charriage > 0) then
      inquire(file=trim(graFile),exist=bexist)
      if (bexist) then
          open(unit=lBinGra,file=trim(graFile),status='old',form='unformatted',iostat=ios)
          close (lBinGra,status='delete')
      endif
      open(unit=lBinGra,file=trim(graFile),status='new',form='unformatted',iostat=ios)
      if (ios > 0) then
          message = ' >>>> Erreur lors de l''ouverture du fichier : '//trim(graFile)
          write(error_unit,'(a)') trim(message) ; write(l9,'(a)') trim(message)
          call echec_initialisation
          stop 128
      endif
   endif
   inquire(file=trim(binFile),exist=bexist)
   if (bexist) then
      open(unit=lBin,file=trim(binFile),status='old',form='unformatted',iostat=ios)
      close (lBin,status='delete')
   endif
   open(unit=lBin,file=trim(binFile),status='new',form='unformatted',iostat=ios)
   if (ios > 0) then
      message = ' >>>> Erreur lors de l''ouverture du fichier : '//trim(binFile)
      write(error_unit,'(a)') trim(message) ; write(l9,'(a)') trim(message)
      call echec_initialisation
      stop 128
   endif
   !si errFile existe déjà on le détruit sinon on pourrait avoir un fichier incohérent
   !avec les autres fichiers de résultats (messages d'erreurs d'une précédente exécution)
   if (errFile /= '') then
      inquire(file=trim(errFile),exist=bexist)
      if (bexist) then
         open(unit=lErr,file=trim(errFile),status='old',form='formatted',iostat=ios)
         close (lErr,status='delete')
      endif
      open(unit=lErr,file=trim(errFile),status='new',form='formatted',iostat=ios)
      if (ios > 0) then
         message = ' >>>> Erreur lors de l''ouverture du fichier : '//trim(errFile)
         write(error_unit,'(a)') trim(message) ; write(l9,'(a)') trim(message)
         call echec_initialisation
         stop 128
      endif
   endif
end subroutine Lire_REP



subroutine Ecrire_REP
!==============================================================================
!   écriture d'une copie du répertoire sur le listing
!==============================================================================
   use Parametres, only: lTra
   use IO_Files, only: repFile, binFile, traFile, envFile, errFile, avaFile, casFile, &
                       devFile, hydFile, latFile, limFile, netFile, numFile, &
                       rugFile, sinFile, varFile, drgFile, psiFile, sedFile, qsoFile, &
                       chaFile, btmFile, qstFile, iniFile, graFile, cgnsFile, parFile
   use data_csv, only: ncsv, filtre
   ! -- variables locales --
   integer :: n, lmax

   !lmax = borne supérieure des longueurs des noms de fichiers utilisés, pour avoir un alignement élégant
   !       en évitant les trop grands espaces vides

   lmax = max(len_trim(numFile),len_trim(netFile),len_trim(iniFile),len_trim(hydFile),len_trim(rugFile), &
              len_trim(avaFile),len_trim(casFile),len_trim(devFile),len_trim(latFile),len_trim(limFile), &
              len_trim(sinFile),len_trim(varFile),len_trim(drgFile),len_trim(traFile),len_trim(binFile), &
              len_trim(errFile),len_trim(envFile),len_trim(graFile),len_trim(parFile))
   if (ncsv > 0) then
      ! NOTE: il est légitime de définir les noms des fichiers CSV ici parce qu'on en a besoin un peu plus loin pour en écrire la liste
      do n = 1, ncsv
         write(filtre(n)%filename,'(4a,i0,a1,i0,a)') trim(filtre(n)%prefix),'_',trim(filtre(n)%var),'_', &
                                                     filtre(n)%ib,'_',int(filtre(n)%pk),'.csv'
         lmax = max(lmax,len_trim(filtre(n)%filename))
      enddo
   endif

   write(lTra,'(4a)') ' Copie du répertoire utilisé pour cette simulation', &
                       ' (fichier : ',trim(repFile),')'
   write(lTra,'(a)') ' '
   write(lTra,'(2(1x,a),5x,a)') 'NUM',numFile(1:lmax),'Nom du fichier des paramètres NUMériques'
   write(lTra,'(2(1x,a),5x,a)') 'PAR',numFile(1:lmax),'Nom du fichier des PARamètres numériques'
   write(lTra,'(2(1x,a),5x,a)') 'NET',netFile(1:lmax),'Nom du fichier définissant la topologie du réseau'
   if (iniFile /= '') write(lTra,'(2(1x,a),5x,a)') 'INI',iniFile(1:lmax),'Nom du fichier des conditions INItiales'
   write(lTra,'(2(1x,a),5x,a)') 'HYD',hydFile(1:lmax),'Nom du fichier des HYDrogrammes amont'
   write(lTra,'(2(1x,a),5x,a)') 'RUG',rugFile(1:lmax),'Nom du fichier des rugosités'
   if (avaFile /= '')  write(lTra,'(2(1x,a),5x,a)') 'AVA',avaFile(1:lmax),'Nom du fichier des lois Q(Z) AVAl'
   if (casFile /= '')  write(lTra,'(2(1x,a),5x,a)') 'CAS',casFile(1:lmax),'Nom du fichier définissant les CASiers'
   if (devFile /= '')  write(lTra,'(2(1x,a),5x,a)') 'DEV',devFile(1:lmax),'Nom du fichier des déversements latéraux'
   if (latFile /= '')  write(lTra,'(2(1x,a),5x,a)') 'LAT',latFile(1:lmax),'Nom du fichier des hydrogrammes LATéraux'
   if (limFile /= '')  write(lTra,'(2(1x,a),5x,a)') 'LIM',limFile(1:lmax),'Nom du fichier des LIMnigrammes aval'
   if (sinFile /= '')  write(lTra,'(2(1x,a),5x,a)') 'SIN',sinFile(1:lmax),'Nom du fichier des SINgularités'
   if (varFile /= '')  write(lTra,'(2(1x,a),5x,a)') 'VAR',varFile(1:lmax),'Nom du fichier VARiation de singularité'
   if (psiFile /= '')  write(lTra,'(2(1x,a),5x,a)') 'PSI',drgFile(1:lmax),'Nom du fichier des coefficients Psi_t'
   if (drgFile /= '')  write(lTra,'(2(1x,a),5x,a)') 'DRG',drgFile(1:lmax),'Nom du fichier des coefficients de trainée'
   if (sedFile /= '')  write(lTra,'(2(1x,a),5x,a)') 'SED',sedFile(1:lmax),'Nom du fichier des couches sédimentaires'
   if (qsoFile /= '')  write(lTra,'(2(1x,a),5x,a)') 'QSO',qsoFile(1:lmax),'Nom du fichier des C.L. solides'
   if (chaFile /= '')  write(lTra,'(2(1x,a),5x,a)') 'CHA',chaFile(1:lmax),'Nom du fichier des paramètres de charriage'
   write(lTra,'(2(1x,a),5x,a)') 'TRA',traFile(1:lmax),'Nom du fichier de sortie formaté'
   write(lTra,'(2(1x,a),5x,a)') 'BIN',binFile(1:lmax),'Nom du fichier de stockage BINaire'
   write(lTra,'(2(1x,a),5x,a)') 'GRA',graFile(1:lmax),'Nom du fichier de stockage GRAnulo'
   write(lTra,'(2(1x,a),5x,a)') 'ERR',errFile(1:lmax),'Nom du fichier des messages d''ERReur'
   if (envFile /= '')  write(lTra,'(2(1x,a),5x,a)') 'ENV',envFile(1:lmax),'Nom du fichier des enveloppes'
   if (ncsv > 0) then
      do n = 1, ncsv
         write(lTra,'(2(1x,a),5x,a)') 'CSV',filtre(n)%filename(1:lmax),'Nom d''un fichier de sortie CSV'
      enddo
   endif
   if (btmFile /= '')  write(lTra,'(2(1x,a),5x,a)') 'BTM',btmFile(1:lmax),'Nom du fichier CSV de l''évolution des cotes de fond'
   if (qstFile /= '')  write(lTra,'(2(1x,a),5x,a)') 'QST',qstFile(1:lmax),'Nom du fichier CSV de l''évolution des débits solides'

   if (iniFile == '') write(lTra,'(a)') ' >>>> État Initial permanent calculé par MAGE'
   if (avaFile == '' .and. limFile == '') write(lTra,'(a)') ' >>>> Conditions aux Limites Aval : régime uniforme partout'
   if (cgnsFile /= '')  write(lTra,'(2(1x,a),5x,a)') 'CGN',qsoFile(1:lmax),'Nom du fichier de resultats au format CGNS'


end subroutine Ecrire_REP



subroutine Lire_Data
!==============================================================================
!                          LECTURE DES DONNÉES
!
!       ce ss-pg lance la lecture des fichiers de données
!
!==============================================================================
   use parametres, only: long, i_ism, i_mixte, i_debord, i_mixte2, lTra, &
                         lBin, zero, l9
   use, intrinsic :: iso_fortran_env, only: output_unit

   use IO_Files, only: sedFile, netFile, sinFile, numFile, rugFile, iniFile, casFile, &
                       devFile, hydFile, limFile, avaFile, latFile, varFile, binFile, drgFile, &
                       psiFile, qsoFile, chaFile, graFile, cgnsFile, parFile
   use data_num_fixes, only : silent, ini_internal, fp_model
   use TopoGeometrie, only: la_topo, xgeo, init_from_file_CAS, initTopoGeometrie, with_charriage, lire_Sediments
   use Maille, only: init_MatriceMaille
   use StVenant_ISM, only: initialize_ISM
   use StVenant_Debord, only: initialize_Debord
   use charriage, only: init_Param_Default_Charriage, lire_Param_Charriage, ecrire_Param_Charriage, lire_CL_solides, &
                        Qs_total, init_cs_echange
#if WITH_CGNS==1
   use mage_cgns, only:init_cgns
#endif
   implicit none
   ! -- Variables --
   logical :: bool
   integer :: nbele, le, ib
   real (kind=long) :: longueur
   !------------------------------------------------------------------------------
   le = output_unit ; if (silent) le = l9


   if (with_charriage > 0) then
      !initialisation par défaut
      call init_param_default_charriage
   endif
   !---Lecture des données sédimentaires si le fichier SED existe
   !NOTE: on en a besoin avant la lecture des données géométriques qui y font référence s'il y a des couches sédimentaires
   select case (with_charriage)
      case (0)
         write(le,'(a)') ' Initialisation de la topologie et de la géométrie sans couches sédimentaires'
      case (1)
         write(le,'(a)') ' Initialisation de la topologie et de la géométrie avec couches sédimentaires aux points'
      case (2)
         write(le,'(3a)') ' Initialisation de la topologie et de la géométrie AVEC ', &
                                                    'couches sédimentaires fournies par le fichier ',trim(sedFile)
         if (sedFile == '') then
            call echec_initialisation
            stop '>>> Erreur : fichier SED manquant !'
         endif
         write(le,'(3a)') ' Lecture de ',trim(sedFile),' pour le transport solide par charriage'
         call Lire_Sediments(sedFile)
      case (3)
         write(le,'(a)') ' Initialisation de la topologie et de la géométrie avec couches sédimentaires aux sections'
      case default
         call echec_initialisation
         stop '>>> Erreur : valeur incorrecte pour with_charriage'
   end select

   !---Lecture du fichier .NET et des fichiers de géométrie
   write(le,'(2a)') ' Lecture de ',trim(netFile)
   call initTopoGeometrie(netFile)

   !allocation des tableaux dont la taille dépend de ibmax, ismax ou nomax
   ! NOTE: removing dependency on IBsup, ISsup and NOsup: work in progress
   call Init_Allocatable_Arrays()

   !!données pour remplir les blocs de la matrice de la maille
   call init_MatriceMaille

   call init_drag_force
   call init_psi_t

   if (fp_model == i_ism) then
      call initialize_ISM
   else if (fp_model == i_debord) then
      call initialize_Debord(la_topo%net%ns)
   else
      call initialize_Debord(la_topo%net%ns)
      call initialize_ISM
   endif

   !---Lecture du fichier .SIN (ouvrages élémentaires) même si le fichier SIN n'existe pas
   !   car il faut toujours initialiser les possibles sections singulières sans ouvrage
   if (trim(sinFile) /= '') write(le,'(2a)') ' Lecture de ',sinFile
   call Lire_SIN(sinFile,nbele)

   !---écritures sur TRA
   write(lTra,'(a,i3)') ' Nombre total de biefs : ',la_topo%net%nb
   write(lTra,'(a,i3)') ' Nombre total de noeuds : ',la_topo%net%nn
   write(lTra,'(a,i5)') ' Nombre total de sections de calcul : ',la_topo%net%ns
   write(lTra,'(a,i3)') ' Nombre total de sections singulières : ',la_topo%net%nss
   if (la_topo%net%iba == la_topo%net%nb) then
      write(lTra,'(a)') ' Réseau ramifié'
   else
      write(lTra,'(a)') ' Réseau maillé'
   endif
   longueur = zero
   do ib = 1, la_topo%net%nb
      longueur = longueur + abs(xgeo(la_topo%biefs(ib)%is1)-xgeo(la_topo%biefs(ib)%is2))
   enddo
   write(lTra,'(a,f10.2,a)') ' Longueur totale du réseau = ',longueur,' m'

   !---Lecture du fichier .NUM
   !  Initialisation du temps de calcul, de theta, des pas d'espace et de temps
   if (numFile .ne. '')then
   !---Lecture du fichier .NUM
      write(le,'(2a)') ' Lecture de ',trim(numFile)
      call Lire_NUM(numFile,.false.)
   elseif (parFile .ne. '')then
   !---Lecture du fichier .PAR
      write(le,'(2a)') ' Lecture de ',trim(parFile)
      call Lire_PAR(parFile,.false.)
   endif

   !---Lecture des strickler sur le fichier [.RUG]
   write(le,'(2a)') ' Lecture de ',trim(rugFile)
   call Lire_RUG(rugFile)

   !---Lecture des conditions initiales sur le fichier [.INI]
   if (ini_internal) write(le,'(4a)') ' Le fichier d''état initial ',trim(iniFile),' sera ignoré sur ordre de ',trim(numFile)
   if (iniFile == '') ini_internal = .true.
   if (ini_internal) then
      write(le,'(a)')  &
            ' État initial calculé par MAGE à partir des C.L. à l''instant initial ; débit simple avec Debord si débordement'
      !ici l'état initial sera défini au début de time_iterations() dans mage_Iterations.f90
   else
      write(le,'(2a)') ' État initial lu dans ',trim(iniFile)
      select case (fp_model)
         case (i_ism)    ; call Lire_INI_ISM(iniFile)
         case (i_mixte,i_mixte2)  ; call Lire_INI_ISM(iniFile)
         case (i_debord) ; call Lire_INI(iniFile)
      end select
      ! L'état initial ne sera pas calculé par IniPer() et on en a maintenant une copie dans Mage_ini.ini
   endif

   !---Définition des casiers (nœuds à surface non nulle) (lecture de .CAS)
   if (casFile /= '') then
      write(le,'(2a)') ' Lecture de ',casFile
      call init_from_file_CAS(casFile,la_topo%nodes,la_topo%net%nn)
      call Ecrire_CAS
   endif

   !---Définition et Initialisation des Conditions aux Limites
   call Lire_TypeCL
   if (hydFile /= '') then   !lecture de .HYD
      write(le,'(2a)') ' Lecture de ',trim(hydFile)
      call Lire_CL('HYD')
   endif
   if (avaFile /= '') then   !lecture de .AVA
      write(le,'(2a)') ' Lecture de ',trim(avaFile)
      call Lire_CL('AVA')
   endif
   if (limFile /= '') then   !lecture de .LIM
      write(le,'(2a)') ' Lecture de ',trim(limFile)
      call Lire_CL('LIM')
   endif
   call VerifAllCL
   call IniDataCL

   !---Initialisation des Débits Latéraux (.LAT)
   if (latFile /= '') then
      write(le,'(2a)') ' Lecture de ',trim(latFile)
      call Lire_LAT
   endif

   !---Initialisation des Variations des Ouvrages (.VAR)
   if (varFile /= '') then
      write(le,'(2a)') ' Lecture de ',trim(varFile)
      call Lire_VAR(varFile)
   endif

   !---Impression sur le listing [.TRA] des paramètres des ouvrages
   call ecrire_SIN(nbele)

   !---Écriture de l'entête du fichier BIN
   inquire(file=binFile,opened=bool)
   if (.not.bool) open(unit=lBin,file=binFile,status='unknown',form='unformatted')
   call ecrire_entete_BIN
#if WITH_CGNS==1
   if (cgnsFile /= '') then
      call init_CGNS
   endif
#endif

   if (with_charriage > 0) then
      !---Écriture de l'entête du fichier GRA
      inquire(file=graFile,opened=bool)
      if (.not.bool) open(unit=lBin,file=binFile,status='unknown',form='unformatted')
      call ecrire_entete_GRA
   endif

   !---Initialisation des données pour les déversements latéraux
   if (devFile /= '') then
      write(le,'(2a)') ' Lecture de ',trim(devFile)
      call Lire_DEV(devFile)
   endif

   !---Initialisation des données pour la drag-force (trainée)
   if (drgFile /= '') then
      write(le,'(2a)') ' Lecture de ',trim(drgFile)
      call Lire_DRG(drgFile)
   endif

   !---Initialisation des données pour Psi_t (cisaillement à l'interface / ISM)
   if (psiFile /= '') then
      write(le,'(2a)') ' Lecture de ',trim(psiFile)
      call Lire_PSI(psiFile)
   endif

   !---Initialisation des C.L. solides
   if (qsoFile /= '' .and. with_charriage > 0) then
      write(le,'(2a)') ' Lecture de ',trim(qsoFile)
      call lire_CL_solides(qsoFile)
   endif

   !---initialisation des paramètres du charriage
   if (with_charriage > 0) then
      if (chaFile /= '') then
         write(le,'(2a)') ' Lecture de ',trim(chaFile)
         call lire_Param_Charriage(chaFile)
      endif
      call init_cs_echange()
      call ecrire_Param_Charriage(l9)
      call ecrire_Param_Charriage(lTra)
      allocate (Qs_total(la_topo%net%ns))
      Qs_total = 0._long
   endif

end subroutine Lire_Data


subroutine ecrire_entete_BIN_old
!==============================================================================
!             Entête du fichier BIN (anciennement fichier ECR)!
!
! TODO: changer le format de l'entête de BIN pour éliminer les infos inutiles
! TODO: voir quelles infos doivent être ajoutées (lits majeurs gauche et droit)
!==============================================================================
   use parametres, only: long, sp, lBin
   use TopoGeometrie, only: la_topo, xgeo, zfd
   implicit none
   ! -- Variables locales temporaires --
   integer :: ib, is, ks, ibs
   integer, parameter :: kbl=1000  !taille des blocs d'écriture des grands tableaux
   integer, pointer :: ibmax, ismax
   real(kind=sp) :: zmin_OLD = 0. !on a supprimé la normalisation des cotes par zmin

   ibmax => la_topo%net%nb
   ismax => la_topo%net%ns

   ! NOTE: kbl est écrit -kbl en tant que flag pour signaler que les temps sont écrits en double précision dans BIN
   ! NOTE: c'est nécessaire pour stocker correctement les grands temps en secondes
   ! NOTE: Mage_Extraire assure la rétro-compatibilité avec les fichiers BIN anciens.
   write(lBin) ibmax,ismax,-kbl
   ! NOTE: plus besoin de stocker l'ordre de calcul hydraulique amont -> aval
   ! NOTE: les données qui suivent sont stockées dans l'ordre des données de .NET
   write(lBin) (-la_topo%net%rang(ib),ib=1,ibmax)
   write(lBin) (la_topo%biefs(ib)%is1,la_topo%biefs(ib)%is2,ib=1,ibmax)
   do ks = 1, ismax, kbl
      write(lBin) (real(xgeo(is),kind=sp), is=ks,min(ks+kbl-1,ismax))
   enddo
   write(lBin) zmin_OLD
   !TODO : revoir les infos à stocker pour prendre en compte les lits majeurs gauche et droit
   do ks = 1, ismax, kbl
      write(lBin) (real(zfd(is),kind=sp), real(la_topo%sections(is)%ymoy,kind=sp), &
                   real(zfd(is)+la_topo%sections(is)%ybmin,kind=sp), is=ks,min(ks+kbl-1,ismax))
   enddo
   ! NOTE: on conserve ce qui suit pour raison de compatibilité : pour que l'entête fasse la même taille qu'avant
   ibs = -1
   do ks = 1, ismax, kbl
      write(lBin) (ibs, is=ks,min(ks+kbl-1,ismax))
   enddo
end subroutine ecrire_entete_BIN_old


subroutine ecrire_entete_BIN
!==============================================================================
!             Entête du fichier BIN (anciennement fichier ECR)!
!
! DONE: changer le format de l'entête de BIN pour éliminer les infos inutiles
! TODO: voir quelles infos doivent être ajoutées (lits majeurs gauche et droit)
!==============================================================================
   use parametres, only: sp, lBin, mage_version
   use TopoGeometrie, only: la_topo, xgeo, zfd
   implicit none
   ! -- Variables locales temporaires --
   integer :: ib, is
   integer, pointer :: ibmax, ismax

   ibmax => la_topo%net%nb
   ismax => la_topo%net%ns

   write(lBin) ibmax,ismax,mage_version
   write(lBin) (la_topo%biefs(ib)%is1,la_topo%biefs(ib)%is2,ib=1,ibmax)
   write(lBin) (real(xgeo(is),kind=sp), is=1,ismax)
   !TODO : revoir les infos à stocker pour prendre en compte les lits majeurs gauche et droit
   write(lBin) (real(zfd(is),kind=sp), real(la_topo%sections(is)%ymoy,kind=sp), &
                real(zfd(is)+la_topo%sections(is)%ybmin,kind=sp), is=1,ismax)
end subroutine ecrire_entete_BIN


subroutine ecrire_entete_GRA
!==============================================================================
!             Entête du fichier BIN (anciennement fichier ECR)!
!
! DONE: changer le format de l'entête de BIN pour éliminer les infos inutiles
! TODO: voir quelles infos doivent être ajoutées (lits majeurs gauche et droit)
!==============================================================================
   use parametres, only: sp, lBinGra, mage_version
   use TopoGeometrie, only: la_topo, xgeo, zfd
   implicit none
   ! -- Variables locales temporaires --
   integer :: ib, is
   integer, pointer :: ibmax, ismax

   ibmax => la_topo%net%nb
   ismax => la_topo%net%ns

   write(lBinGra) ibmax,ismax,mage_version
   write(lBinGra) (la_topo%biefs(ib)%is1,la_topo%biefs(ib)%is2,ib=1,ibmax)
   write(lBinGra) (real(xgeo(is),kind=sp), is=1,ismax)
   !TODO : revoir les infos à stocker pour prendre en compte les lits majeurs gauche et droit
!    write(lBin) (real(zfd(is),kind=sp), real(la_topo%sections(is)%ymoy,kind=sp), &
!                 real(zfd(is)+la_topo%sections(is)%ybmin,kind=sp), is=1,ismax)
   write(lBinGra) (real(la_topo%sections(is)%np,kind=sp), is=1,ismax)
end subroutine ecrire_entete_GRA


subroutine Ecrire_CAS
!==============================================================================
!      Écriture de la géométrie des nœuds à surface non-nulle sur TRA
!==============================================================================
   use parametres, only: long, lTra
   use TopoGeometrie, only: la_topo
   use Mage_Utilitaires, only: ecr0
   implicit none
   ! -- Variables locales temporaires --
   integer :: n, k, n0, k0
   character(len=14) :: chain='', chain1=''
   character(len=80) :: ligne=''
!------------------------------------------------------------------------------
   ligne = repeat('-',80) ; ligne(1:1) = ' '
   write(lTra,'(a)') ligne
   write(lTra,'(16x,a)') 'Lois hauteur-surface des casiers'
   do n = 1, la_topo%net%nn
      if (la_topo%nodes(n)%np > 0) then !nœud à surface non-nulle
         write(lTra,'(1x,2a)') 'Nœud : ',la_topo%nodes(n)%name
         do n0 = 1, la_topo%nodes(n)%np
            call ecr0(chain,la_topo%nodes(n)%vne(n0),k)
            k0 = 15-k
            chain1(1:k0) = chain(k:14)
            chain1(k0+1:k0+4) = ' m3 '
            if (k0+4<14) then
               do k = k0+5, 14
                  chain1(k:k) = ' '
               enddo
            endif
            write(lTra,'(1x,f8.3,a,f10.1,2a)')  la_topo%nodes(n)%zne(n0),' mètres ***** ',&
                                       la_topo%nodes(n)%sne(n0)*0.0001_long,' hectares ***** ', chain1
         enddo
      endif
   enddo
   write(lTra,'(a)') ligne
end subroutine Ecrire_CAS



subroutine Init_All_DefaultDataModule
!==============================================================================
!                 Routine d'initialisation générale
!==============================================================================
   !use Apports_Lateraux, only: IniAppLat
   !use Booleens,only: IniBool
   !use Casiers,only: IniCase
   use Data_Num_Fixes, only: IniDataFix
   use Data_Num_Mobiles, only: IniDataMob
   !use Erreurs_STV, only: IniErrSTV
   !use Hydraulique, only: IniHyd
   !use Maille, only: IniMaille
   !use Matrice_STV, only: IniMatSTV
   !use Solution, only: IniSol
   !use Data_Iterations_loc, only: IniM143
   !use Mage_Results, only: init_Mage_Results
   use PremierAppel, only: init_NAP
   use IO_Files, only: init_Filenames
   implicit none

   call init_Filenames
   !CALL IniAppLat
   !CALL IniBool
   !CALL IniCase
   CALL IniDataFix
   CALL IniDataMob
   !CALL IniErrSTV
   !CALL IniHyd
   !CALL IniMaille
   !CALL IniMatSTV
   !CALL IniSol
   !CALL IniM143
   !call init_Mage_Results
   call init_NAP
end subroutine Init_All_DefaultDataModule


subroutine Init_Allocatable_Arrays()
! allocation des tableaux dimensionnés à IBMAX, ISMAX ou NOMAX
! dimensions que l'on ne connait qu'une fois que la topo a été initialisée
   use TopoGeometrie, only: la_topo
   use casiers, only: iniCase
   use hydraulique, only: iniHyd
   use Erreurs_STV, only: iniErrSTV
   use Data_Iterations_loc, only: iniM143
   use Mage_Results, only: init_Mage_Results
   use Solution, only: IniSol
   use Matrice_STV, only: iniMatSTV
   use Booleens,only: iniBool
   use Maille, only: iniMaille
   implicit none
   ! -- variables locales --
   integer :: ibmax, ismax, nomax

   ibmax = la_topo%net%nb
   ismax = la_topo%net%ns
   nomax = la_topo%net%nn

   !module casiers
   call iniCase(ibmax,ismax,nomax)
   !module Hydraulique
   call iniHyd()
   !module erreurs_stv
   call iniErrSTV(ibmax)
   !module solution
   call iniSol(ismax,nomax)
   !module Mage_Results
   call init_Mage_Results
   !module Data_Iterations_loc
   call IniM143(ismax,nomax)
   !module matrice_STV
   call iniMatSTV(ismax)
   !module Booleens
   call iniBool()
   !module Maille
   call iniMaille

end subroutine Init_Allocatable_Arrays
