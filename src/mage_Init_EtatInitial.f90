!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - INRAE - 1989-2020                           #
!# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################



subroutine Lire_INI(INI_File)  !Lecture INI
!==============================================================================
!                          LECTURE DU FICHIER .INI
!==============================================================================
   use parametres, only: long, zero, un, cent, lTra, nomini_ini, l9
   use, intrinsic :: iso_fortran_env, only: error_unit

   use data_num_fixes, only: tinf, flag, ini_internal
   use hydraulique, only: qt, zt, qe, dqe, yt
   use Ouvrages, only: all_OuvCmp, all_OuvEle
   use mage_utilitaires, only: zegal, next_int, next_real, is_zero
   use TopoGeometrie, only: la_topo, xgeo, zfd
   use data_num_mobiles, only: t, dt
   use StVenant_Debord, only: Debord_IniFin
   use IO_Files, only: repFile
   implicit none
   ! -- Prototype
   character(len=*),intent(in) :: INI_file
   ! -- Variables locales temporaires --
   integer :: ib, is, js, k, kpomp, luu, n, nk, nmax, ns, l
   character :: car1
   character(len=80) :: ligne=''
   real(kind=long) :: dqt, q, qnom, z, x
   logical :: bopen
   logical, allocatable :: eureka(:)
   integer :: ios, nf, is1, is2
   character(len=120) :: message1='', message2='', message3=''
   integer,pointer :: ismax,ibmax,nsmax

   ismax => la_topo%net%ns
   ibmax => la_topo%net%nb
   nsmax => la_topo%net%nss

   ns = 0
   allocate(eureka(ismax))
   eureka = .false.

   if (.not.ini_internal) then
      open(newunit=luu,file=trim(INI_file),status='old',form='formatted')
      do  !boucle de lecture ligne par ligne
         read(luu,'(a)',iostat=ios) ligne
         if (ios /= 0) exit
         l = l+1
         car1 = ligne(1:1)
         if (car1 == '*' .or. car1 == '$') then
            cycle
         else   !Ligne d'eau seule (apport lateral et strickler lus à part)
            if ((scan(ligne(1:7),' ',back=.true.) < 4) .and. (ligne(1:1) == ' ')) then
               !on a un vieux fichier INI fait par PamHyr sans séparateur entre IB et IS
               !il faut utiliser l'ancienne méthode de lecture
               read(ligne,'(1x,2i3,3x,f10.5,f11.6,f9.2)') ib, is, q, z, x
            else
               nf = 1  ;  ib = next_int(ligne,'',nf)  ;  is = next_int(ligne,'',nf)
               is1 = la_topo%biefs(ib)%is1
               is2 = la_topo%biefs(ib)%is2
               if (scan(ligne(1:40),' ',back=.true.) < scan(ligne(1:31),'.',back=.true.)) then
                  !pas de séparateur entre z et x
                  !il faut utiliser l'ancienne méthode de lecture
                  read(ligne,'(1x,2i3,3x,f10.5,f11.6,f9.2)') ib, is, q, z, x
               elseif (scan(ligne(1:31),' ',back=.true.) < scan(ligne(1:20),'.',back=.true.)) then
                  !pas de séparateur entre q et z
                  !il faut utiliser l'ancienne méthode de lecture
                  read(ligne,'(1x,2i3,3x,f10.5,f11.6,f9.2)') ib, is, q, z, x
               else
                  q = next_real(ligne,'',nf)  ;  z = next_real(ligne,'',nf)  ;  x = next_real(ligne,'',nf)
               endif
            endif
            if (ib > ibmax) cycle
            is = is1-1+is
            if (is > is2) then
               cycle
            elseif (abs(xgeo(is)-x) > 0.01_long) then
               cycle
            elseif (eureka(is)) then
               write(message1,'(a,i3,a,i3,a,f10.2)') '>>>> Dans INI la section',is-is1+1,&
                            ' du bief ',ib,' (Pm = ',xgeo(is),') est en double'
               write(lTra,'(a)') trim(message1) ; write(l9,'(a)') trim(message1)
               cycle
            endif

            ns = ns+1
            eureka(is) = .true.
            qt(is) = q         ! initialisation du débit
            zt(is) = z         ! initialisation de la cote de l'eau (normalisée)
            qe(is) = zero      ! initialisation débit latéral (maj à chaque dt)
            dqe(is) = zero     ! initialisation variation débit latéral
            yt(is) = zt(is)-zfd(is)  ! tirant d'eau
         endif
      enddo  !Lecture finie

      if (ns == 0) then
         write(error_unit,'(3a)') ' >>>> Erreur : le fichier d''état initial ',trim(ini_File),' est vide'
         write(error_unit,'(3a)') '      Enlevez le de ',trim(repFile),' pour forcer la construction de l''état initial par Mage'
         stop 7
      endif

      do ib = 1, ibmax
         do is = is1, is2
            if (.not.eureka(is)) then
               write(l9,'(a,i3,a,i3,a,f10.2,a)') '>>>> Il manque des données dans INI section ',&
                     is-is1+1,' du bief ', ib,' (Pm = ',xgeo(is),')'
            endif
         enddo
      enddo
      if (ns /= ismax) then   ! traitement erreur
         message1 = ' >>>> ALERTE : il manque des données dans .INI'
         message2 = ' ==> Tentative d''interpolation de la ligne d''eau initiale'
         message3 = ' ATTENTION : risque d''erreur s''il y a 2 sections à la même abscisse (ouvrages)'
         write(error_unit,'(1x,/,a,/,14x,a,/,a,/,1x)') trim(message1),trim(message2),trim(message3)
         write(lTra,'(1x,/,a,/,14x,a,/,a,/,1x)') trim(message1),trim(message2),trim(message3)
         rewind (luu)
         call lire_ini_alt(luu)
      endif

      !copie de l'état initial dans Mage_ini.ini
      inquire(file=nomini_ini,opened=bopen) !si le fichier est ouvert c'est que c'est celui
                                                !lu par Lire_INI() donc initialisation par IniPer()
      if(.not.bopen) then
         t = tinf + dt
         call Debord_IniFin(nomini_ini,qt,zt)
      endif
      close (luu)
   endif

   !---mise à jour de la position des pompes : s'il y en a il faut determiner celles qui sont en marche
   !   on décide qu'une pompe est en train de fonctionner si le saut de débit entre les sections is et is+1
   !   (is+1 étant singulière) est supérieur à son débit nominal + la somme des débits nominaux des pompes précédentes
   if (zegal(flag,zero,un)) then  !il y a des pompes
      n = 0
      do ns = 1, nsmax
         nmax = all_OuvCmp(ns)%ne
         if (all_OuvEle(all_OuvCmp(ns)%OuEl(1,1))%iuv == 3) then
            js = all_OuvCmp(ns)%js
            qnom = zero
            kpomp = 0
            dqt = qt(js-1)-qt(js)
            do k = 1, nmax
               nk = all_OuvCmp(ns)%OuEl(k,1)
               qnom = qnom+all_OuvEle(nk)%uv1
               if (abs(dqt)+0.0001_long >= abs(qnom)) then  !la pompe est en train de fonctionner
                  all_OuvEle(nk)%uv2 = tinf-600._long
                  kpomp = kpomp+1
               endif
            enddo
            if (.not.zegal(dqt,qnom,un)) call war008(lTra,js-1)
         endif
      enddo
   endif
   deallocate(eureka)
end subroutine Lire_INI



subroutine Lire_INI_ISM(INI_File)  !Lecture INI
!==============================================================================
!   Lecture du fichier .INI dans le cas où c'est un fichier Mage_fin.ini
!   produit par MAGE avec l'option ISM
!==============================================================================
   use parametres, only: long, zero, un, cent, lTra, nomini_ini, l9
   use, intrinsic :: iso_fortran_env, only: output_unit, error_unit

   use hydraulique, only: qe, dqe, qt0=>qt, zt0=>zt, yt0 => yt
   use mage_utilitaires, only: zegal, next_int, next_real
   use TopoGeometrie, only: la_topo, xgeo, zfd
   use StVenant_ISM, only: qt=>q, zt=>z, debits_ISM, Q_total, ISM_IniFin, initial_state_ISM
   use IO_Files, only: repFile
   implicit none
   ! -- Prototype
   character(len=*),intent(in) :: INI_file
   ! -- Variables locales temporaires --
   integer :: ib, is, luu, ns, l, k, nf
   character :: car1
   character(len=260) :: ligne=''
   real(kind=long) :: zz, xx, fake
   type(debits_ISM) :: qq
   logical :: bopen
   logical, allocatable :: eureka(:)
   integer :: ios, is1, is2
   character(len=120) :: message1=''
   integer,pointer :: ismax,ibmax,nsmax

   ismax => la_topo%net%ns
   ibmax => la_topo%net%nb
   nsmax => la_topo%net%nss

   ns = 0
   allocate(eureka(ismax))
   eureka = .false.

   open(newunit=luu,file=trim(INI_file),status='old',form='formatted')
   do  !boucle de lecture ligne par ligne
      read(luu,'(a)',iostat=ios) ligne
      if (ios /= 0) exit
      l = l+1
      car1 = ligne(1:1)
      if (car1 == '*') then  !commentaire -> on passe
         if (ligne(1:4) =='* IB') then
            ! ligne d'entête : vérification que le format est correct. Q_gauche doit se trouver en colonne 141
            ! pour que les champs qq%ql,qq%qm,qq%qr soient bien trouvés en colonne 137
            if (ligne(143:150) /= 'Q_gauche') then
               write(output_unit,'(3a)') ' >>>> ERREUR : le fichier ',trim(INI_File),' a un format obsolète'
               write(output_unit,'( a)') '               vérifiez que sa ligne d''entêtes est bien identique à celle'
               write(output_unit,'( a)') '               d''un fichier Mage_fin.ini produit par cette version de MAGE'
               write(output_unit,'(3a,i0)') 'ligne(143:150) = ',ligne(143:150),'index(ligne,''Q_gauche'') = ', &
                                          index(ligne,'Q_gauche')
               call echec_initialisation
               stop 2
            else
               continue
            endif
         endif
      else if (car1 == '$') then  !temps origine de la ligne d'eau DEBORD
         !Ce n'est pas un fichier INI produit par ISM -> lecture par Lire_INI() standard
         write(output_unit,'(1x,a)') &
              trim(INI_File)//' n''est pas un fichier INI produit par ISM -> lecture par Lire_INI() standard'
         close (luu)
         call Lire_INI(INI_File)
         call initial_state_ISM(qt0,zt0)
         return
      else if (car1 == '@') then  !temps origine de la ligne d'eau ISM
         write(output_unit,'(14x,a)') &
              trim(INI_File)//' est un fichier INI produit par ISM -> lecture par Lire_INI() pour ISM'
         ! on continue la lecture du fichier ligne par ligne
      else                        !Ligne d'eau seule (apport lateral et strickler lus à part)
         nf = 1  ;  ib = next_int(ligne,'',nf)  ;  is = next_int(ligne,'',nf)
                    fake = next_real(ligne,'',nf)  ;  zz = next_real(ligne,'',nf)  ;  xx = next_real(ligne,'',nf)
         is1 = la_topo%biefs(ib)%is1
         is2 = la_topo%biefs(ib)%is2
         !recherche du dernier # ou ! de la ligne : on partira de là pour continuer la lecture
         nf = max(nf,1+scan(ligne,'!#',back=.true.))
         do k = 1, 5  !il faut ignorer les 5 colonnes suivantes
            fake = next_real(ligne,'',nf)
         enddo
         qq%ql = next_real(ligne,'',nf)  ;  qq%qm = next_real(ligne,'',nf)  ;  qq%qr = next_real(ligne,'',nf)

         if (ib > ibmax) cycle
         is = is1-1+is
         if (is > is2) then
            cycle
         elseif (abs(xgeo(is)-xx) > 0.01_long) then
            cycle
         elseif (eureka(is)) then
            write(message1,'(a,i3,a,i3,a,f10.2)') '>>>> Dans INI la section',is-is1+1,&
                         ' du bief ',ib,' (Pm = ',xgeo(is),') est en double'
            write(lTra,'(a)') trim(message1) ; write(l9,'(a)') trim(message1)
            cycle
         endif

         ns = ns+1
         eureka(is) = .true.
         qt(is) = qq        ! initialisation du débit
         zt(is) = zz        ! initialisation de la cote de l'eau (normalisée)
         qe(is) = zero      ! initialisation débit latéral (maj à chaque dt)
         dqe(is) = zero     ! initialisation variation débit latéral
         qt0(is) = Q_total(qt(is))
         zt0(is) = zt(is)
         yt0(is) = zt0(is)-zfd(is)  ! tirant d'eau
      endif
   enddo  !Lecture finie

   if (ns == 0) then
      write(error_unit,'(3a)') ' >>>> Erreur : le fichier d''état initial ',trim(ini_File),' est vide'
      write(error_unit,'(3a)') '      Enlevez le de ',trim(repFile),' pour forcer la construction de l''état initial par Mage'
      stop 7
   endif

   do ib = 1, ibmax
      do is = is1, is2
         if (.not.eureka(is)) then
            write(l9,'(a,i3,a,i3,a,f10.2,a)') '>>>> Il manque des données dans INI section',&
                  is-is1+1,' du bief ', ib,' (Pm = ',xgeo(is),')'
         endif
      enddo
   enddo

   !copie de l'état initial dans Mage_ini.ini
   inquire(file=nomini_ini,opened=bopen)  !si le fichier est ouvert c'est que c'est celui
                                          !lu par Lire_INI_ISM()
   if(.not.bopen) call ISM_IniFin(nomini_ini)
   close (luu)
   deallocate(eureka)
end subroutine Lire_INI_ISM



subroutine Lire_INI_alt(luu)  !Lecture INI
!==============================================================================
!                LECTURE alternative DU FICHIER .INI
!              interpolation de la ligne d'eau initiale
!
!---> Appelé par Lire_INI() en cas d'erreur
!
!==============================================================================
   use parametres, only: long, zero, un, cent, lTra, l9
   use basic_Types, only: courbe2D, init_courbe2D, interpole
   use data_num_fixes, only: ini_internal
   use hydraulique, only: qt, zt, qe, dqe, yt
   use TopoGeometrie, only: la_topo, xgeo, zfd
   use mage_utilitaires, only: next_int, next_real, is_zero
   implicit none
   ! -- Prototype
   integer, intent(in) :: luu
   ! -- Variables locales temporaires --
   character(len=80) :: Ligne=''
   integer :: ib, is, i, j, L, ios, nf
   integer,allocatable :: imax(:)
   real(kind=Long) :: q, z, x, xx
   real(kind=long), allocatable :: xqz(:,:,:)
   type (courbe2D) :: Q2x, Z2x
   integer,pointer :: ismax,ibmax,nsmax

   ismax => la_topo%net%ns
   ibmax => la_topo%net%nb
   nsmax => la_topo%net%nss

   allocate(imax(ibmax))
   imax = 0
   allocate(xqz(ismax,3,ibmax))
   xqz = zero

   if (.not.ini_internal) then
      do  !boucle de lecture ligne par ligne donc dans l'ordre des biefs de .NET
         read(luu,'(a)',iostat=ios) ligne
         if (ios /= 0) exit
         L = L+1
         if (ligne(1:1) == '*' .or. ligne(1:1) == '$') then  !temps origine de la ligne d'eau
            cycle
         else  !Ligne d'eau seule (apport latéral et strickler lus à part)
            if (scan(ligne(1:7),' ',back=.true.) < 4) then
               !on a un vieux fichier INI fait par PamHyr sans séparateur entre IB et IS
               !il faut utiliser l'ancienne méthode de lecture
               read(ligne,'(1x,2i3,3x,f10.5,f11.6,f9.2)') ib, is, q, z, x
            else
               nf = 1  ;  ib = next_int(ligne,'',nf)  ;  is = next_int(ligne,'',nf)
               if (scan(ligne(1:40),' ',back=.true.) < scan(ligne(1:31),'.',back=.true.)) then
                  !pas de séparateur entre z et x
                  !il faut utiliser l'ancienne méthode de lecture
                  read(ligne,'(1x,2i3,3x,f10.5,f11.6,f9.2)') ib, is, q, z, x
               elseif (scan(ligne(1:31),' ',back=.true.) < scan(ligne(1:20),'.',back=.true.)) then
                  !pas de séparateur entre q et z
                  !il faut utiliser l'ancienne méthode de lecture
                  read(ligne,'(1x,2i3,3x,f10.5,f11.6,f9.2)') ib, is, q, z, x
               else
                  q = next_real(ligne,'',nf)  ;  z = next_real(ligne,'',nf)  ;  x = next_real(ligne,'',nf)
               endif
            endif
!            nf = 1  ;  ib = next_int(ligne,'',nf)  ;  is = next_int(ligne,'',nf)
!                       q = next_real(ligne,'',nf)  ;  z = next_real(ligne,'',nf)  ;  x = next_real(ligne,'',nf)
            if (ib > ibmax) then
               write(l9,'(a,3(i3,a))') ' Erreur dans INI à la ligne ', L,' bief ',ib,&
                                      ' non trouvé : la ligne ',L,' est ignorée'
               cycle
            endif
            imax(ib) = imax(ib)+1
            xqz(imax(ib),1,ib) = x  ;  xqz(imax(ib),2,ib) = q  ; xqz(imax(ib),3,ib) = z
         endif
      enddo  !Lecture finie

      do ib = 1, ibmax  !parcours l'ordre des biefs de .NET
         if (xqz(1,1,ib) < xqz(imax(ib),1,ib)) then
            Q2x = init_courbe2D(xqz(:,1,ib),xqz(:,2,ib),imax(ib))
            Z2x = init_courbe2D(xqz(:,1,ib),xqz(:,3,ib),imax(ib))
         else
            allocate (Q2x%yx(imax(ib)),Z2x%yx(imax(ib)))
            do i = 1, imax(ib)
               j = imax(ib)-i+1
               Q2x%yx(i)%x = xqz(j,1,ib)
               Q2x%yx(i)%y = xqz(j,2,ib)
               Z2x%yx(i)%x = xqz(j,1,ib)
               Z2x%yx(i)%y = xqz(j,3,ib)
            enddo
         endif
         Q2x%ip = 1  ;  Z2x%ip = 1
         Q2x%np = imax(ib)  ;  Z2x%np = imax(ib)
         do is = la_topo%biefs(ib)%is1, la_topo%biefs(ib)%is2
            xx = xgeo(is)  !interpolation pour xx
            qt(is) = interpole(Q2x,xx)
            zt(is) = interpole(Z2x,xx)
            qe(is)  = 0._long
            dqe(is) = 0._long
            yt(is) = zt(is)-zfd(is)
         enddo
         deallocate (Q2x%yx,Z2x%yx)
      enddo
   endif
   deallocate(imax)
end subroutine Lire_INI_alt
