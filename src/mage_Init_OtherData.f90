!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - INRAE - 1989-2020                           #
!# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################


subroutine Lire_NUM(NUM_file, relire)
!==============================================================================
!              Lecture du fichier de parametres .NUM
!==============================================================================
   use parametres, only: long, lname, zero, un, lTra, param_torrentiel
   use, intrinsic :: iso_fortran_env, only: error_unit

   use mage_utilitaires, only: lire_date, heure, is_zero, heure1, heure2, heure_func
   use booleens, only: donotComputeVolumeBalance
   use data_num_fixes, only: ini_internal, newt, c_lissage, crmax, yinf, tinf, tmax,theta, &
                             dttra, dtbin, dtbase, itemax=>iter, ndtf=>ndt, inr, iter1, &
                             dfrp1, dfrp2, dfrp3, fma, frmax, sch1, sch2, sch3, &
                             dtmin, err_volume_maxAllowed, vbmin, crnormal, steady, relax_a, relax_b, &
                             reste, crtor, date_format, dtmelissa
   use data_num_mobiles, only: dt, ttra,tbin, dtold, dtmin0, t
   use charriage, only: dt_char, n_pas
   implicit none
   ! -- Prototype
   character(len=*), intent(in) :: NUM_file
   logical, intent(in) :: relire
   ! -- Variables locales temporaires --
   integer :: i, lu14, ltmp, tri1
   integer :: ifrp1, ifrp2, ifrp3, ifrp4, ifrp5, ifrp6
   character :: ch1, ch2, ch3, rep, statiq
   character(len=79) :: ligne='',blanc=''
   character(len=*), parameter :: separateur=',;:'
   character :: message1*60, hms*19
   logical :: bool
   real(kind=Long) :: sch11, sch22, sch33
   integer, parameter :: ilong = selected_int_kind(18)
   integer(kind=ilong), parameter :: T_infini = 8639999999_ilong

   blanc = repeat(' ',79) ; blanc(1:1) = '*'
   open(newunit=lu14,file=trim(NUM_file),status='old',form='formatted')
   if (relire) then
      hms = heure(t)
      write(lTra,'(/,2a)') ' >>>> Modification des paramètres numériques au temps ',hms
   endif

   !---lecture de theta
   ligne = blanc
   sch11 = huge(zero) ; sch22 = huge(zero) ; sch33 = huge(zero)
   do while (ligne(1:1)=='*') ; read(lu14,'(a)') ligne ; enddo
   read(ligne,'(41x,f4.2,4(1x,a))') theta, ch1, ch2, ch3, statiq
   if (is_zero(theta)) theta = 0.65_long
   if (theta > un) theta = un
   if (ch1=='L' .or. ch1=='l')  then
      ch1 = 'L'
      sch11 = un+un
   else
      ch1 = 'S'
      sch11 = zero
   endif
   if (ch2==' ')  then
      ch2 = 'B'
      sch22 = un
    else if (ch2=='A' .or. ch2=='a') then
      ch2 = 'A'
      sch22 = zero
    else if (ch2=='B' .OR. ch2=='b') then
      ch2 = 'B'
      sch22 = un
   endif
   if (ch3==' ' .or. ch3=='R' .or. ch3=='r') then
      ch3 = 'R'
      sch33 = -un
    else if (ch3=='G' .or. ch3=='g') then
      ch3 = 'G'
      sch33 = zero
    else if (ch3=='A' .or. ch3=='a') then
      ch3 = 'A'
      sch33 = un
   endif

   ! NOTE: on garde cette option pour la compatibilités avec PamHyr
   ! NOTE: ini_internal est aussi mis à VRAI par Lire_Data() si le fichier INI est absent
   ini_internal = (statiq=='p' .or. statiq=='P')

   write(lTra,3011) theta,ch1,ch2,ch3,statiq
   if (sch11 > huge(zero)/2) stop '>>>> BUG 1 dans Lire_NUM()'
   if (sch22 > huge(zero)/2) stop '>>>> BUG 2 dans Lire_NUM()'
   if (sch33 > huge(zero)/2) stop '>>>> BUG 3 dans Lire_NUM()'
   sch1 = sch11 ; sch2 = sch22 ; sch3 = sch33

   !---lecture de la date de début (TINF)
   ligne = blanc
   do while (ligne(1:1)=='*') ; read(lu14,'(a)') ligne ; enddo
   if (.not.relire) then
      tri1 = scan(ligne(42:),'-') + 1
   !   if (scan(message,'/').ne.0) then
      if (tri1 > 1 .and. scan(ligne(42+tri1:),'-').ne.0) then
         !s'il y a 2 tirets on suppose qu'on a un format de date ISO
      !if (scan(ligne(42:),'/').ne.0) then
        date_format = .true. ! format DD-MM-YYTHH:MM:SS
        heure => heure2
      else
        date_format = .false. ! format DDD:HH:MM:SS
        heure => heure1
      endif
      tinf = real(lire_date(ligne(42:)),kind=long)
      ttra = tinf
      tbin = tinf
   endif

   !---lecture de la date de fin (TMAX)
   ligne = blanc
   do while (ligne(1:1)=='*') ; read(lu14,'(a)') ligne ; enddo
   if (ligne(42:47)=='999:99' .or. ligne(42:48)=='9999:99' .or. ligne(42:49)=='99999:99' ) then
      steady = .true.
      tmax = real(t_infini,kind=long)
   else
      tmax = real(lire_date(ligne(42:),tinf,date_format),kind=long)
      steady = .false.
   endif

   if (tinf > tmax .and. .not. steady) then
      write(lTra,*) ' >>>> Temps de début posterieur au temps de fin'
      write(error_unit,*) ' >>>> Temps de début posterieur au temps de fin'
      call echec_initialisation
      stop 153
   endif

   !---lecture du pas de temps (dt)
   ligne = blanc
   do while (ligne(1:1) == '*') ; read(lu14,'(a)') ligne ; enddo
   read(ligne,'(41x,2f10.0)',err=999) dt,dtmin
   if (is_zero(dt)) dt = 600._long
   if (dt < zero) then
      write(lTra,*) ' >>>> Pas de temps négatif'
      write(error_unit,*) ' >>>> Pas de temps négatif'
      call echec_initialisation
      stop 151
   endif
   if (dtmin <= zero) dtmin = un
   if (dt < dtmin) dt = 16._long*dtmin
   dtmin0 = dtmin
   dt = dt+0.001_long
   dt = anint(100._long*dt)*0.01_long
   !ajustement du tmax pour un arrêt immédiat propre
   if (relire .and. tmax < t) tmax = t+dt

   !---lecture du pas de temps d'impression sur listing (DTTRA)
   ligne = blanc
   do while (ligne(1:1)=='*') ; read(lu14,'(a)') ligne ; enddo
   read(ligne,'(41x,f10.0)',err=999) dttra
   if (dttra <= zero) then
      dttra = dt
   else if (dttra < dt) then
      dt = dttra
   endif
   if ((tinf+dttra)>tmax .and. dttra>zero) then
      dttra = tmax-tinf
   else if ((tinf+dttra)<tmax .and. dttra<zero) then
      dttra = tmax-tinf
   endif

   !---lecture du pas de temps d'ecriture sur fichier resultat .bin,
   ligne = blanc
   do while (ligne(1:1)=='*') ; read(lu14,'(a)') ligne ; enddo
   read(ligne,'(41x,3f10.0)',err=999) dtbin !,seuil,tcar
   if (dtbin < 0.1_long) dtbin = dttra
   if (dtmelissa < 0.1_long) dtmelissa = dtbin
   if (dtbin>zero .and. dtbin<dt) then
      dt = dtbin
   endif
   if (dtmelissa>zero .and. dtmelissa<dt) then
      dt = dtmelissa
   endif
   if ((tinf+dtbin)>tmax .and. dtbin>zero) then
      dtbin = tmax-tinf
   else if ((tinf+dtbin)<tmax .and. dtbin<zero) then
      dtbin = tmax-tinf
   endif

   !---affectation du pas de temps (DTBASE)
   dt = anint(dt/dtmin)*dtmin
   dtbase = dt
   dtold = dt
   dt_char = dtbase * real(n_pas,kind=long)

   !---lecture du coefficient NEWT (type des itérations)
   ! NOTE: pas d'autres choix que Newton-Raphson et ses variantes.
   ligne = blanc
   do while (ligne(1:1)=='*') ; read(lu14,'(a)') ligne ; enddo
   read(ligne,'(41x,i2)',err=999) newt
   if (newt >= 0) newt = -1
   write(lTra,3012) newt

   !---lecture du coefficient de lissage
   ligne = blanc
   do while (ligne(1:1)=='*') ; read(lu14,'(a)') ligne ; enddo
   read(ligne,'(41x,f10.0)',err=999) c_lissage
   write(lTra,3020) c_lissage

   !---lecture du coefficient CRMAX pour le reglage du DT automatique
   ligne = blanc
   do while (ligne(1:1)=='*') ; read(lu14,'(a)') ligne ; enddo
   read(ligne,'(41x,f10.0)',err=999) crmax
   if (abs(crmax) < 0.0001_long) crmax = -1._long
   if (crmax < zero) then
      write(lTra,3030) dtbase
   else
      write(lTra,3031) crmax
   endif

   !---lecture du coefficient YINF pour le réglage du DT automatique
   ligne = blanc
   do while (ligne(1:1)=='*') ; read(lu14,'(a)') ligne ; enddo
   read(ligne,'(41x,f10.0)',err=999) yinf
   write(lTra,3040) yinf

   !---lecture du nombre maximum d'itérations dans nonLinear_iterations()
   ligne = blanc
   do while (ligne(1:1)=='*') ; read(lu14,'(a)') ligne ; enddo
   read(ligne,'(41x,i2)',err=999) itemax
   itemax = min(99,itemax)
   if (abs(itemax) < 1) itemax = 10
   write(lTra,3050) itemax

   !---lecture du facteur de réduction du pas de temps en cas de non-convergence
   ligne = blanc
   do while (ligne(1:1)=='*') ; read(lu14,'(a)') ligne ; enddo
   read(ligne,'(41x,i2)',err=999) ndtf
   ndtf = min(99,ndtf)
   if (abs(ndtf) < 1) ndtf = 2
   write(lTra,3060) ndtf

   !---lecture du nombre maximum d'itérations sans réduction de la précision
   ligne = blanc
   do while (ligne(1:1)=='*') ; read(lu14,'(a)') ligne ; enddo
   read(ligne,'(41x,i2)',err=999) iter1
   if (iter1==0) iter1 = 99
   if (newt>0) iter1 = -1
   write(lTra,3070) iter1

   !---lecture du facteur de réduction de la précision si convergence difficile
   ligne = blanc
   do while (ligne(1:1)=='*') ; read(lu14,'(a)') ligne ; enddo
   read(ligne,'(40x,3(1x,i1,i2))',err=999) ifrp1,ifrp2,ifrp3,ifrp4,ifrp5,ifrp6
   if (ligne(46:48) == '   ') then
      ifrp3 = ifrp1
      ifrp4 = ifrp2
   endif
   if (ligne(50:52) == '   ') then
      ifrp5 = 1
      ifrp6 = 0
   endif
   if (ifrp1 < 1) ifrp1 = 1
   if (ifrp3 < 1) ifrp3 = 1
   if (ifrp5 < 1) ifrp5 = 1
   dfrp1 = real(ifrp1,kind=long)*10._long**ifrp2
   dfrp2 = real(ifrp3,kind=long)*10._long**ifrp4
   dfrp3 = real(ifrp5,kind=long)*10._long**ifrp6

   write(lTra,3080) ifrp1,ifrp2,ifrp3,ifrp4,ifrp5,ifrp6

   !---lecture du nombre d'itérations de Newton avant basculement en Point Fixe
   ! NOTE: obsolète, conservé pour des raisons de compatibilité du format de fichier NUM
   ligne = blanc
   do while (ligne(1:1)=='*') ; read(lu14,'(a)') ligne ; enddo
   read(ligne,'(41x,i2)',err=999) inr
   inr = max(99,inr)

   !---lecture de la borne d'erreur en cote pour la vérification des bilans aux nœuds de maille
   ligne = blanc
   do while (ligne(1:1)=='*') ; read(lu14,'(a)') ligne ; enddo
   read(ligne,'(41x,f10.0)',err=999) fma
   if (abs(fma) < 0.0000001_long) fma = -1._long

   !---lecture de la valeurs maxi du froude
   ligne = blanc
   do while (ligne(1:1)=='*') ; read(lu14,'(a)') ligne ; enddo
   read(ligne,'(41x,f10.0)',err=999) frmax
   if (is_zero(frmax)) frmax = 1.5_long

   !---Calcul du bilan en volume par bief
   ligne = blanc
   do while (ligne(1:1)=='*') ; read(lu14,'(a)') ligne ; enddo
   read(ligne,'(41x,a,1x,2f10.0)',err=999) rep, err_volume_maxAllowed, vbmin
   donotComputeVolumeBalance = .true.
   if (rep == 'O' .or. rep == 'o') donotComputeVolumeBalance = .false.

   !---lecture des paramètres de la routine RELAX() - JBF/04-04-2006
   CRnormal = CRmax
   inquire(file=param_torrentiel,exist=bool)
   if (bool) then
      open(newunit=ltmp,file=param_torrentiel,status='old')
         read(ltmp,'(f10.0)') relax_a
         read(ltmp,'(f10.0)') relax_b
         read(ltmp,'(f10.0)') reste
         reste = max(zero,min(1._long,reste))
         read(ltmp,'(f10.0)') CRtor
      close(ltmp)
      if (CRnormal > zero) CRtor = min(CRnormal,CRtor)
   else
      relax_a = 0.95_long
      relax_b = 1.05_long
      reste = zero
      CRtor = 5._long
      if (CRnormal > zero) CRtor = min(CRnormal,CRtor)
      open(newunit=ltmp,file=param_torrentiel,status='new')
         write(ltmp,'(f10.4,a)') relax_a,' !seuil inferieur sur Fr'
         write(ltmp,'(f10.4,a)') relax_b,' !seuil superieur sur Fr'
         write(ltmp,'(f10.4,a)') reste,' !valeur min de coupure'
         write(ltmp,'(f10.4,a)') CRtor,' !Cr pour torrentiel'
      close (ltmp)
   endif

   close(lu14)
   return
   999 message1 = ' >>>> ERREUR dans la lecture de .NUM à la ligne :'
   ltmp = lTra
   do i = 1, 2
      if (i==2) ltmp = 0
      write(ltmp,*) message1
      write(ltmp,*) ligne
   enddo
   call echec_initialisation
   stop 152
!------------------------------------------------------------------------------
   3011 format(/,1x,'Coefficient du schéma implicite : ',f5.3,' Schéma :',4(1x,a))
   3012 format(/,1x,'Type des itérations : ',i2)
   3020 format(1x,'Coefficient de sous-relaxation : ',f5.2)
   3030 format(1x,'Pas de temps fixe : ',f8.2)
   3031 format(1x,'Nombre de Courant maximum : ',g10.4)
   3040 format(1x,'Tirant d''eau minimum : ',g10.4)
   3050 format(1x,'Nombre maximum d''itérations sur les non-linéarites : ',i2)
   3060 format(1x,'Facteur de réduction du pas de temps en cas de non-convergence : ',i2)
   3070 format(1x,'Nombre maximum d''itérations sans réduction de la précision : ',i2)
   3080 format(1x,'Facteur de réduction de la précision : ',i1,'.E',i2.2,&
                  ' en cote ',i1,'.E',i2.2,' en débit et ',i1,'.E',i2.2,' pour le résidu')
   3090 format(1x,'Nombre d''itérations de Newton avant basculement en Point Fixe : ',i2)
end subroutine Lire_NUM



subroutine Lire_PAR(parFile, relire)
!==============================================================================
!              Lecture du fichier de parametres .PAR
!==============================================================================
   use parametres, only: long, lname, zero, un, lTra, param_torrentiel
   use, intrinsic :: iso_fortran_env, only: error_unit

   use mage_utilitaires, only: lire_date, heure, is_zero, heure1, heure2, heure_func, &
                               next_real, next_string, next_int, capitalize
   use booleens, only: donotComputeVolumeBalance
   use data_num_fixes, only: ini_internal, newt, c_lissage, crmax, yinf, tinf, tmax,theta, &
                             dttra, dtbin, dtbase, itemax=>iter, ndtf=>ndt, inr, iter1, &
                             dfrp1, dfrp2, dfrp3, fma, frmax, sch1, sch2, sch3, &
                             dtmin, err_volume_maxAllowed, vbmin, crnormal, steady, relax_a, relax_b, &
                             reste, crtor, date_format, dtmelissa, with_melissa
   use data_num_mobiles, only: dt, ttra,tbin, dtold, dtmin0, t, tmelissa
   use charriage, only: a_Han, L_d, L_s, method_geo, shields, shields_correction, cap_sol,&
   dt_char, f_mult, rhos, phis, porosity, LA1, LA2, assign_pointer_functions, create_largeur_active, n_pas
   use TopoGeometrie, only: with_charriage

   implicit none
   ! -- Prototype
   character(len=*), intent(in) :: parFile
   logical, intent(in) :: relire
   ! -- Variables locales temporaires --
   integer :: i, lu14, ltmp, tri1, nf, ios
   character :: ch1, ch2, ch3, rep, statiq
   character(len=79) :: ligne='',blanc='',ligne_cap='',ligne2=''
   character(len=*), parameter :: separateur=',;:'
   character :: message1*60, hms*19
   logical :: bool
   real(kind=Long) :: sch11, sch22, sch33
   integer, parameter :: ilong = selected_int_kind(18)
   integer(kind=ilong), parameter :: T_infini = 8639999999_ilong

   blanc = repeat(' ',79) ; blanc(1:1) = '*'
   open(newunit=lu14,file=trim(parFile),status='old',form='formatted')
   if (relire) then
      hms = heure(t)
      write(lTra,'(/,2a)') ' >>>> Modification des paramètres numériques au temps ',hms
   endif

   ! init default values
   theta = 0.65_long
   ch1 = 'S'
   ch2 = 'B'
   ch3 = 'R'
   statiq = 'p'
   sch11 = zero
   sch22 = un
   sch33 = -un
   dt = 300.0
   tinf = 0.0
   date_format = .false.
   tmax = real(t_infini,kind=long)
   steady = .true.
   dttra = 3600.0
   dtbin = 300.0
   dtmelissa = zero
   dtmin = 1.0
   dtmin0 = dtmin
   with_melissa = .false.
   newt = -1
   c_lissage = 0.0
   crmax = -1.0
   yinf = 0.1
   itemax = 10
   ndtf = 2
   iter1 = 99
   dfrp1 = 1.0
   dfrp2 = 1.0
   dfrp3 = 1.0
   inr = 99
   fma = -1.0
   frmax = 1.5
   rep = 'O'
   err_volume_maxAllowed = 0.001
   vbmin = 1000.0

   ! lecture fichier PAR
   do
      read(lu14,'(a)',iostat=ios) ligne
      nf = 1
      if (ios < 0) then
         exit                             !on est arrivé à la fin du fichier
      elseif (ligne(1:1) == '*') then     !ligne de commentaire : ne compte pas
         cycle
      elseif (len_trim(ligne) < 1) then   !ligne vide : ne compte pas
         cycle
      else
         call capitalize(ligne,ligne_cap)
         ligne = next_string(ligne_cap, ' =:', nf)
         select case (ligne)
         case ('INIT_INTERNAL')
            statiq = next_string(ligne_cap, ' =:', nf)
         case ('IMPLICITATION')
            theta = next_real(ligne_cap, ' ', nf)
            if (is_zero(theta)) theta = 0.65_long ! default
            if (theta < zero) theta = 0.65_long ! default
            if (theta > un) theta = un
         case ('CONTINUITY_DISCRETIZATION')
            ch1 = next_string(ligne_cap, ' =:', nf)
            if (ch1=='L') then
               sch11 = un+un
            else ! default
               sch11 = zero
            endif
         case ('QSJ_DISCRETIZATION')
            ch2 = next_string(ligne_cap, ' =:', nf)
            if (ch2=='A') then
               sch22 = zero
            else ! default
               sch22 = un
            endif
         case ('STOP_CRITERION_ITERATIONS')
            ch3 = next_string(ligne_cap, ' =:', nf)
            if (ch3=='G') then
               sch33 = zero
            else if (ch3=='A') then
               sch33 = un
            else ! default
               sch33 = -un
            endif
         case ('INIT_TIME')
            if (.not.relire) then
               ligne2 = next_string(ligne_cap, ' =', nf)
               tri1 = scan(ligne2(:),'-') + 1
               if (tri1 > 1 .and. scan(ligne2(:),'-').ne.0) then
                  !s'il y a 2 tirets on suppose qu'on a un format de date ISO
                  date_format = .true. ! format DD-MM-YYTHH:MM:SS
                  heure => heure2
               else
                  date_format = .false. ! format DDD:HH:MM:SS
                  heure => heure1
               endif
               tinf = real(lire_date(ligne2(:)),kind=long)
            endif
         case ('FINAL_TIME')
            ligne2 = next_string(ligne_cap, ' =', nf)
            if (ligne2(1:6)=='999:99' .or. ligne2(1:7)=='9999:99' &
                                    .or. ligne2(1:8)=='99999:99') then
                steady = .true.
                tmax = real(t_infini,kind=long)
            else
                tmax = real(lire_date(ligne2(:),tinf,date_format),kind=long)
                steady = .false.
            endif
         case ('TIMESTEP')
            dt = next_real(ligne_cap, ' =:', nf)
            if (is_zero(dt)) dt = 600._long
            if (dt < zero) then
                write(lTra,*) ' >>>> Pas de temps négatif'
                write(error_unit,*) ' >>>> Pas de temps négatif'
                call echec_initialisation
                stop 151
            endif
         case ('MIN_TIMESTEP')
            dtmin = next_real(ligne_cap, ' =:', nf)
            if (dtmin <= zero) dtmin = un
            dtmin0 = dtmin
         case ('TIMESTEP_BIN')
            dtbin = next_real(ligne_cap, ' =:', nf)
         case ('TIMESTEP_TRA')
            dttra = next_real(ligne_cap, ' =:', nf)
         case ('TIMESTEP_MELISSA')
            dtmelissa = next_real(ligne_cap, ' =:', nf)
            with_melissa = .true.
         case ('ITERATION_TYPE')
            newt = next_int(ligne_cap, ' =:', nf)
            if (newt >= 0) newt = -1
         case ('SMOOTH_COEF')
            c_lissage = next_real(ligne_cap, ' =:', nf)
         case ('CFL_MAX')
            crmax = next_real(ligne_cap, ' =:', nf)
            if (abs(crmax) < 0.0001_long) crmax = -1._long
         case ('MIN_HEIGHT')
            yinf = next_real(ligne_cap, ' =:', nf)
         case ('MAX_NITER')
            itemax = next_int(ligne_cap, ' =:', nf)
            itemax = min(99,itemax)
            if (abs(itemax) < 1) itemax = 10
         case ('TIMESTEP_REDUCTION_FACTOR')
            ndtf = next_int(ligne_cap, ' =:', nf)
            ndtf = min(99,ndtf)
            if (abs(ndtf) < 1) ndtf = 2
         case ('NITER_MAX_PRECISION')
            iter1 = next_int(ligne_cap, ' =:', nf)
            if (iter1==0) iter1 = 99
         case ('PRECISION_REDUCTION_FACTOR_Q')
            dfrp1 = next_real(ligne_cap, ' =:', nf)
            if (dfrp1 < 1.0) dfrp1 = 1.0
         case ('PRECISION_REDUCTION_FACTOR_Z')
            dfrp2 = next_real(ligne_cap, ' =:', nf)
            if (dfrp2 < 1.0) dfrp2 = 1.0
         case ('PRECISION_REDUCTION_FACTOR_R')
            dfrp3 = next_real(ligne_cap, ' =:', nf)
            if (dfrp3 < 1.0) dfrp3 = 1.0
         case ('NITER_BEFORE_SWITCH')
            inr = next_int(ligne_cap, ' =:', nf)
            inr = max(99,inr)
         case ('DIFFLUENCE_NODE_HEIGHT_BALANCE')
            fma = next_real(ligne_cap, ' =:', nf)
            if (abs(fma) < 0.0000001_long) fma = -1._long
         case ('MAX_FROUDE')
            frmax = next_real(ligne_cap, ' =:', nf)
            if (is_zero(frmax)) frmax = 1.5_long
         case ('COMPUTE_REACH_VOLUME_BALANCE')
            rep = next_string(ligne_cap, ' =:', nf)
         case ('MAX_REACH_VOLUME_BALANCE')
            err_volume_maxAllowed = next_real(ligne_cap, ' =:', nf)
         case ('MIN_REACH_VOLUME_TO_CHECK')
            vbmin = next_real(ligne_cap, ' =:', nf)
         case ('DISTANCE_HAN')
            a_Han = next_real(ligne_cap, ' =:', nf)
         case ('DISTANCE_CHARGEMENT_D50')
            L_d = next_real(ligne_cap, ' =:', nf)
         case ('DISTANCE_CHARGEMENT_SIGMA')
            L_s = next_real(ligne_cap, ' =:', nf)
         case ('METHODE_MODIFICATION_GEOMETRIE')
            method_geo = next_int(ligne_cap, ' =:', nf)
         case ('SHIELDS_CRITIQUE')
            shields = next_int(ligne_cap, ' =:', nf)
         case ('SHIELDS_CORRECTION')
            shields_correction = next_int(ligne_cap, ' =:', nf)
         case ('CAPACITE_SOLIDE')
            cap_sol = next_int(ligne_cap, ' =:', nf)
         case('PAS_DE_TEMPS_CHARRIAGE')
            n_pas = next_int(ligne_cap, ' =:', nf)
            if (n_pas < 1) then
               write(error_unit,'()') '>>>> Erreur : le pas de temps pour le charriage est nul'
               stop 7
            endif
         case('FACTEUR_MULTIPLICATEUR')
            f_mult = next_real(ligne_cap, ' =:', nf)
         case('SEDIMENT_MASSE_VOLUMIQUE')
            rhos = next_real(ligne_cap, ' =:', nf)
         case('SEDIMENT_ANGLE_REPOS')
            phis = next_real(ligne_cap, ' =:', nf)
         case('SEDIMENT_POROSITY')
            porosity = next_real(ligne_cap, ' =:', nf)
         case('LARGEUR_ACTIVE_EXTREMITE_1')
            LA1 = next_string(ligne_cap, ' =:', nf)
         case('LARGEUR_ACTIVE_EXTREMITE_2')
            LA2 = next_string(ligne_cap, ' =:', nf)
         case default
            write(error_unit,'(2a)') '>>>> Erreur lors de la lecture de ',trim(parFile)
            write(error_unit,'(2a)') '     Clé inconnue : ',trim(ligne)
         end select
      endif
   enddo

   ! NOTE: on garde cette option pour la compatibilités avec PamHyr
   ! NOTE: ini_internal est aussi mis à VRAI par Lire_Data() si le fichier INI est absent
   ini_internal = (statiq=='p' .or. statiq=='P')

   ! verifications
   if (sch11 > huge(zero)/2) stop '>>>> BUG 1 dans lire_PAR()'
   if (sch22 > huge(zero)/2) stop '>>>> BUG 2 dans lire_PAR()'
   if (sch33 > huge(zero)/2) stop '>>>> BUG 3 dans lire_PAR()'
   sch1 = sch11 ; sch2 = sch22 ; sch3 = sch33
   ttra = tinf; tbin = tinf; tmelissa = tinf
   if (tinf > tmax .and. .not. steady) then
      write(lTra,*) ' >>>> Temps de début posterieur au temps de fin'
      write(error_unit,*) ' >>>> Temps de début posterieur au temps de fin'
      call echec_initialisation
      stop 153
   endif
   if (dt < dtmin) dt = 16._long*dtmin
   dt = dt+0.001_long
   dt = anint(100._long*dt)*0.01_long
   !ajustement du tmax pour un arrêt immédiat propre
   if (relire .and. tmax < t) tmax = t+dt
   if (dttra <= zero) then
      dttra = dt
   else if (dttra < dt) then
      dt = dttra
   endif
   if ((tinf+dttra)>tmax .and. dttra>zero) then
      dttra = tmax-tinf
   else if ((tinf+dttra)<tmax .and. dttra<zero) then
      dttra = tmax-tinf
   endif
   if (dtbin < 0.1_long) dtbin = dttra
   if (dtbin>zero .and. dtbin<dt) then
      dt = dtbin
   endif
   if ((tinf+dtbin)>tmax .and. dtbin>zero) then
      dtbin = tmax-tinf
   else if ((tinf+dtbin)<tmax .and. dtbin<zero) then
      dtbin = tmax-tinf
   endif
   if (with_melissa) then
      if (dtmelissa < 0.1_long) dtmelissa = dtbin
      if (dtmelissa>zero .and. dtmelissa<dt) then
         dt = dtmelissa
      endif
      if ((tinf+dtmelissa)>tmax .and. dtmelissa>zero) then
         dtmelissa = tmax-tinf
      else if ((tinf+dtmelissa)<tmax .and. dtmelissa<zero) then
         dtmelissa = tmax-tinf
      endif
   endif
   !---affectation du pas de temps (DTBASE)
   dt = anint(dt/dtmin)*dtmin
   dtbase = dt
   dtold = dt
   if (newt>0) iter1 = -1
   if (rep == 'O' .or. rep == 'o') donotComputeVolumeBalance = .false.

   !---lecture des paramètres de la routine RELAX() - JBF/04-04-2006
   CRnormal = CRmax
   inquire(file=param_torrentiel,exist=bool)
   if (bool) then
      open(newunit=ltmp,file=param_torrentiel,status='old')
         read(ltmp,'(f10.0)') relax_a
         read(ltmp,'(f10.0)') relax_b
         read(ltmp,'(f10.0)') reste
         reste = max(zero,min(1._long,reste))
         read(ltmp,'(f10.0)') CRtor
      close(ltmp)
      if (CRnormal > zero) CRtor = min(CRnormal,CRtor)
   else
      relax_a = 0.95_long
      relax_b = 1.05_long
      reste = zero
      CRtor = 5._long
      if (CRnormal > zero) CRtor = min(CRnormal,CRtor)
      open(newunit=ltmp,file=param_torrentiel,status='new')
         write(ltmp,'(f10.4,a)') relax_a,' !seuil inferieur sur Fr'
         write(ltmp,'(f10.4,a)') relax_b,' !seuil superieur sur Fr'
         write(ltmp,'(f10.4,a)') reste,' !valeur min de coupure'
         write(ltmp,'(f10.4,a)') CRtor,' !Cr pour torrentiel'
      close (ltmp)
   endif
   close(lu14)
   ! tra
   write(lTra,3011) theta,ch1,ch2,ch3
   write(lTra,3012) newt
   write(lTra,3020) c_lissage
   if (crmax < zero) then
      write(lTra,3030) dtbase
   else
      write(lTra,3031) crmax
   endif
   write(lTra,3040) yinf
   write(lTra,3050) itemax
   write(lTra,3060) ndtf
   write(lTra,3070) iter1
   write(lTra,3080) dfrp1,dfrp2,dfrp3 ! TODO

   dt_char = dtbase * real(n_pas,kind=long)
   if (with_charriage > 0) then
      call assign_pointer_functions()

      call create_largeur_active(LA1,LA2)
   endif

   return
   999 message1 = ' >>>> ERREUR dans la lecture de .PAR à la ligne :'
   ltmp = lTra
   do i = 1, 2
      if (i==2) ltmp = 0
      write(ltmp,*) message1
      write(ltmp,*) ligne
   enddo
   call echec_initialisation
   stop 152
!------------------------------------------------------------------------------
   3011 format(1x,'Coefficient du schéma implicite : ',f5.3,' Schéma :',4(1x,a))
   3012 format(1x,'Type des itérations : ',i2)
   3020 format(1x,'Coefficient de sous-relaxation : ',f5.2)
   3030 format(1x,'Pas de temps fixe : ',f8.2)
   3031 format(1x,'Nombre de Courant maximum : ',1p,g10.4)
   3040 format(1x,'Tirant d''eau minimum : ',1p,g10.4)
   3050 format(1x,'Nombre maximum d''itérations sur les non-linéarites : ',i2)
   3060 format(1x,'Facteur de réduction du pas de temps en cas de non-convergence : ',i2)
   3070 format(1x,'Nombre maximum d''itérations sans réduction de la précision : ',i2)
   3080 format(1x,'Facteur de réduction de la précision : ',1p,g10.4,&
                  ' en cote ',1p,g10.4,' en débit et ',1p,g10.4,' pour le résidu') ! TODO
   3090 format(1x,'Nombre d''itérations de Newton avant basculement en Point Fixe : ',i2)

end subroutine Lire_PAR



subroutine Lire_DEV(devFile)
!==============================================================================
!         Lecture du fichier de description des déversements latéraux
!==============================================================================
   use parametres, only: long, total, rivg, rivd, lTra, l9
   use, intrinsic :: iso_fortran_env, only: error_unit

   use booleens, only: uselat
   use casiers, only: ydlat =>ydev, cdlat, htraff, pceaff, nclat, rvclat
   use TopoGeometrie, only: la_topo, xgeo, section_id_from_pk, zfd
   use mage_utilitaires, only: is_zero
   implicit none
   ! -- Prototype
   character(len=*), intent(in) :: devFile
   ! -- Variables --
   logical :: bool,bool1
   integer :: i,ib,is,js1,js2,kb,ks1,ks2,luu,mclat,n,nligne,j,k,ios
   character :: ligne , rive
   character(len=3)  :: nom=''
   character(len=80) :: u=''
   real(kind=long) :: cd, dx, dydeb, htaff, pcaff
   real(kind=long) :: x1, xs1, xs2, xt1, xt2, y1, y2, z1, z2, tmp
   character (len=8) :: rgrd=''
   character(len=120) :: message=''
   real(kind=long), parameter :: seuil = 0.001_long
   integer,pointer :: nomax

   nomax => la_topo%net%nn
   bool = .false. ; bool1 = .false.
   !---lecture de .DEV s'il existe
   i = 19
   cdlat = -1._long
   ydlat = 9999._long
   htraff = 9999._long
   pceaff = 0._long
   if (devFile == '') then
      return  ! le fichier '.DEV' n'existe pas : pas de déversements latéraux
   else
      open(newunit=luu,file=trim(devFile),status='old',form='formatted')
      write(lTra,'(/a)') ' Déversements latéraux :'
   endif
   !---le fichier '.DEV' existe
   nligne = 0
   !-----------------------------------------lecture du FICHIER .DEV
   !
   !---Lecture d'une nouvelle ligne du fichier
   do
      read(luu,'(a)',iostat=ios) u
      if (ios < 0) exit
      if (ios > 0) then
         write(message,'(a,i3)') ' >>>> ERREUR de lecture dans .DEV, ligne numéro ',nligne
         write(lTra,'(a)') trim(message) ; write(error_unit,'(a)') trim(message)
         write(lTra,'(1x,a)') u ; write(error_unit,'(1x,a)') u
         call echec_initialisation
         stop 171
      endif
      nligne = nligne+1
      write(l9,*) 'DEV : lecture de la ligne ',nligne,' : ',u
      !---Décodage de la ligne
      if (u(1:1) == '*') then
         cycle
      elseif (u(1:1) == 'Y' .or. u(1:1) == 'y') then  !ligne Y : échange avec casier
         read(u,'(2a1,i3,2f8.0,5f10.0,a3)',iostat=ios) ligne,rive,ib,xs1,xs2,z1,z2,cd,htaff,pcaff,nom
         if (ios > 0) then
            call Erreur_Lecture_DEV(lTra,nligne,u,'Y')
            call echec_initialisation
            stop 172
         endif
         ligne = 'Y'
      elseif (u(1:1) == 'Z' .or. u(1:1) == 'z') then  !ligne Z : échange avec bief
         read(u,'(2a1,i3,2f8.0,5f10.0,i3,f7.0)',iostat=ios) ligne,rive,ib,xs1,xs2,z1,z2,cd,htaff,pcaff,kb,xt1
                            !on ne donne que le point de début du secteur récepteur
         if (ios > 0) then
            call Erreur_Lecture_DEV(lTra,nligne,u,'Z')
            call echec_initialisation
            stop 170
         endif
         xt2 = 0._long
         ligne = 'Z'
      else  ! --erreur : il faut une ligne y ou z
         write(message,'(a,i3,a)') ' >>>> la ligne numéro ',nligne, &
                                   'du fichier dev devrait être une ligne de type y ou z'
         write(lTra,'(a)') trim(message) ; write(error_unit,'(a)') trim(message)
         write(lTra,'(1x,a)') u ; write(error_unit,'(1x,a)') u
         call echec_initialisation
         stop 168
      endif
      if (rive == 'D' .or. rive == 'd') then
         rive = 'D'
      else    ! rive gauche par défaut
         rive = 'G'
      endif
      !---numéros absolus des sections amont et aval du secteur déversant
      !------cas des entrées nulles : on prend le bief entier
      if (is_zero(xs1)) xs1 = xgeo(la_topo%biefs(ib)%is1)
      if (is_zero(xs2)) xs2 = xgeo(la_topo%biefs(ib)%is2)

      js1 = section_id_from_pk (la_topo, ib, xs1, seuil)
      js2 = section_id_from_pk (la_topo, ib, xs2, seuil)
      !---cas d'un secteur déversant de longueur nulle : on n'en tient pas compte
      if (abs(xs1-xs2) < 0.01_long) cycle
      !---numéros locaux des sections amont et aval du secteur récepteur (ligne z)
      if (ligne == 'Z' .and. kb == 0) then
         ks1 = 0 ; ks2 = 0
      elseif (ligne == 'Z') then
         ks1 = section_id_from_pk (la_topo, kb, xt1, seuil)
         ks2 = ks1+(js2-js1) ! correction de ks2 pour avoir le même nombre
                           ! de sections que le secteur déversant
         if (ks2 > la_topo%biefs(kb)%is2) then
            write(message,'(a)') ' Erreur dans les données de déversement latéral'
            write(lTra,'(a)') trim(message) ; write(error_unit,'(a)') trim(message)
            message = ' Il y a une erreur de définition du secteur récepteur à la ligne : '
            write(lTra,'(a)') trim(message) ; write(error_unit,'(a)') trim(message)
            write(lTra,'(a)') trim(u) ; write(error_unit,'(a)') trim(u)
            message = ' Le secteur récepteur doit avoir un pas d''espace du même ordre de grandeur'
            write(lTra,'(a)') trim(message) ; write(error_unit,'(a)') trim(message)
            message = ' que celui du secteur déversant car il doit avoir le même nombre de sections.'
            write(lTra,'(a)') trim(message) ; write(error_unit,'(a)') trim(message)
            call echec_initialisation
            stop 229
         endif
         xt1 = xgeo(ks1) ; xt2 = xgeo(ks2)
      endif
      !---Initialisations des indicateurs USELAT (externe) et BOOL (interne)
      if (cd > 0.0000001_long) uselat = .true.
      bool = .false.
      !---recherche de la zone de réception (casier ou bief) : définition de MCLAT
      !   qui permettra de définir NCLAT plus loin
      if (ligne == 'Y' .and. nom == '   ') then
         mclat = 0
      elseif (ligne == 'Y') then
         mclat = 0
         do n = 1, nomax  !recherche du noeud de nom nom
            if (la_topo%nodes(n)%name == nom) then
               mclat = n
               exit
            endif
         enddo
      elseif (ligne == 'Z' .and. kb == 0 .and. ks1 == 0 .and. ks2 == 0) then
         mclat = 0
      elseif (ligne == 'Z') then
         mclat = -1
      endif

      !---Si BOOL1 faux alors déversement à la cote de berge : pas d'interpolation
      bool1 = (.not.is_zero(z1)) .or. (.not.is_zero(z2))
      if (bool1) then
         y1 = z1-zfd(js1) ; y2 = z2-zfd(js2)
       else
         select case (rive)
            case ('D')    ;  y1 = la_topo%sections(js1)%ybdro ; y2 = la_topo%sections(js2)%ybdro
            case ('G')    ;  y1 = la_topo%sections(js1)%ybgau ; y2 = la_topo%sections(js2)%ybgau
            case default  ;  y1 = la_topo%sections(js1)%ybmin ; y2 = la_topo%sections(js2)%ybmin
         end select
      endif
      !---Détection d'erreurs sur les cotes de déversement
      if (y1 < 0._long) then
         bool = .true.
         write(lTra,'(2(a,f7.2))') &
         '>>>> ERREUR dans .DEV : cote de déversement inférieure à la cote du fond : ',z1,' < ',zfd(js1)
      else
         select case (rive)
            case ('G')    ;  y1 = min(y1,la_topo%sections(js1)%ybgau+100._long)
            case ('D')    ;  y1 = min(y1,la_topo%sections(js1)%ybdro+100._long)
            case default  ;  y1 = min(y1,la_topo%sections(js1)%ybmin+100._long)
         end select
      endif
      if (y2 < 0._long) then
         bool = .true.
         write(lTra,'(2(a,f7.2))') &
         '>>>> ERREUR dans .DEV : cote de déversement inférieure à la cote du fond : ',z1,' < ',zfd(js2)
      else
         select case (rive)
            case ('G')    ;  y2 = min(y2,la_topo%sections(js2)%ybgau+100._long)
            case ('D')    ;  y2 = min(y2,la_topo%sections(js2)%ybdro+100._long)
            case default  ;  y2 = min(y2,la_topo%sections(js2)%ybmin+100._long)
         end select
      endif
      if (bool) then
         message = '>>>> ERREUR dans la définition des déversements latéraux'
         write(lTra,'(a)') trim(message) ; write(error_unit,'(a)') trim(message)
         call echec_initialisation
         stop 169
      endif
      !---fin de vérification et fin des valeurs par défaut

      !---Interpolations linéaires
      x1 = xgeo(js1)    ! abscisse de la section amont du tronçon dans le repère du bief considéré
      dx = xgeo(js2)-x1 ! longueur du tronçon = longueur max de déversement
      dydeb = z2-z1     ! différence entre cote aval et cote amont de déversement
      if (bool1) then
         !--- on interpole la cote de déversement
         select case (rive)
            case ('G')
               do is = js1,js2-1
                  ydlat(rivg,is) = y1 + dydeb/dx*(xgeo(is)-x1) + zfd(js1)-zfd(is)
                  cdlat(rivg,is) = cd
                  htraff(rivg,is) = ydlat(rivg,is)-htaff
                  pceaff(rivg,is) = pcaff
               enddo
               ydlat(rivg,js2) = y2  ;  cdlat(rivg,js2) = 0._long
            case ('D')
               do is = js1,js2-1
                  ydlat(rivd,is) = y1 + dydeb/dx*(xgeo(is)-x1) + zfd(js1)-zfd(is)
                  cdlat(rivd,is) = cd
                  htraff(rivd,is) = ydlat(rivd,is)-htaff
                  pceaff(rivd,is) = pcaff
               enddo
               ydlat(rivd,js2) = y2  ;  cdlat(rivd,js2) = 0._long
            case default  ! si la rive n'est pas definie on prend la gauche
               do is = js1,js2-1
                  ydlat(rivg,is) = y1 + dydeb/dx*(xgeo(is)-x1) + zfd(js1)-zfd(is)
                  cdlat(rivg,is) = cd
                  htraff(rivg,is) = ydlat(rivg,is)-htaff
                  pceaff(rivg,is) = pcaff
               enddo
               ydlat(rivg,js2) = y2  ;  cdlat(rivg,js2) = 0._long
         end select
      else
         !--- déversement à la cote de berge : pas d'interpolation
         select case (rive)
            case ('G')
               ydlat(rivg,js1:js2) = la_topo%sections(js1:js2)%ybgau
               cdlat(rivg,js1:(js2-1)) = cd
               htraff(rivg,js1:(js2-1)) = ydlat(rivg,js1:(js2-1))-htaff
               pceaff(rivg,js1:(js2-1)) = pcaff
               cdlat(rivg,js2) = 0._long
            case ('D')
               ydlat(rivd,js1:js2) = la_topo%sections(js1:js2)%ybdro
               cdlat(rivd,js1:(js2-1)) = cd
               htraff(rivd,js1:(js2-1)) = ydlat(rivd,js1:(js2-1))-htaff
               pceaff(rivd,js1:(js2-1)) = pcaff
               cdlat(rivd,js2) = 0._long
            case default  ! si la rive n'est pas définie on prend la gauche
               ydlat(rivg,js1:js2) = la_topo%sections(js1:js2)%ybmin
               cdlat(rivg,js1:(js2-1)) = cd
               htraff(rivg,js1:(js2-1)) = ydlat(rivg,js1:(js2-1))-htaff
               pceaff(rivg,js1:(js2-1)) = pcaff
               cdlat(rivg,js2) = 0._long
         end select
      endif
      !
      !---Remplissage du tableau NCLAT :
      !           correspondance sections deversantes / sections receptrices
      !---Remplissage du tableau RVCLAT :
      !           correspondance rives deversantes / rives receptrices
      !   JS1, JS2, KS1 et KS2 sont des numéros de section absolus
      if (mclat<0) then   ! cas d'un déversement vers un autre bief
         do i = 1,js2-js1
            j = js1+i-1   ! numéro de la section deversante
            k = ks1+i-1   ! numéro de la section receptrice
            select case (rive)
               case ('G')
                  nclat(rivg,j) = -k
                  if (kb == ib) then ! déversement dans le même bief  => même rive
                     rvclat(rivg,j) = rivg  ;  rvclat(rivg,k) = rivg
                     nclat(rivg,k) = -j
                  else
                     rvclat(rivg,j) = rivd  ;  rvclat(rivd,k) = rivg
                     nclat(rivd,k) = -j
                  endif
               case ('D')
                  nclat(rivd,j) = -k
                  if (kb == ib) then ! déversement dans le même bief  => même rive
                     rvclat(rivd,j) = rivd  ;  rvclat(rivd,k) = rivd
                     nclat(rivd,k) = -j
                  else
                     rvclat(rivd,j) = rivg  ;  rvclat(rivg,k) = rivd
                     nclat(rivg,k) = -j
                  endif
            end select
         enddo
      else                  ! cas d'un déversement vers un casier
         do is = js1,js2-1
            select case (rive)
               case ('G')
                  nclat(rivg,is) = mclat
               case ('D')
                  nclat(rivd,is) = mclat
            end select
         enddo
      endif

      !---Ecritures sur .TRA
      if (rive == 'G') then
         rgrd = ' gauche '
      else
         rgrd = ' droit  '
      endif
      write(l9,*) 'DEV : mclat = ',mclat
      if (mclat>0) then
         write(lTra,'(a,i3,a,2(a,f9.2),3a,f6.3,a)') ' Bief ',ib,rgrd,' Pm ',xgeo(js1), &
                     ' à ',xgeo(js2),' vers noeud ',la_topo%nodes(mclat)%name,' (coeff débit : ',cd,')'
      elseif (mclat<0) then
         write(lTra,'(a,i3,a,2(a,f9.2),/,20x,a,i3,2(a,f8.2),a,f6.3,a)') &
                     ' Bief ', ib,rgrd,' Pm ',xgeo(js1),' a ',xgeo(js2),' vers bief ',kb, &
                     ' Pm ',xt1,' à ',xt2,' (coeff débit : ',cd,')'
         !vérification : il faut que les 2 secteurs aient des longueurs voisines
!                        on rejette un écart supérieur à 15%
         tmp = min(abs(xt2-xt1),abs(xgeo(js2)-xgeo(js1)))
         tmp = abs(abs(xt2-xt1)-abs(xgeo(js2)-xgeo(js1)))/max(tmp,.1_long)
         if (tmp>0.15_long) then
            message = ' Il y a une erreur de définition du secteur récepteur à la ligne : '
            write(lTra,'(a)') trim(message) ; write(error_unit,'(a)') trim(message)
            write(lTra,'(a)') trim(u) ; write(error_unit,'(a)') trim(u)
            message = ' Les secteurs déversant et récepteur ont des longueurs sensiblement différentes.'
            write(lTra,'(a)') trim(message) ; write(error_unit,'(a)') trim(message)
            message = ' Cela vient d''une trop grande différence entre leurs pas d''espace'
            write(lTra,'(a)') trim(message) ; write(error_unit,'(a)') trim(message)
            message = ' et provoquera des erreurs d''évaluation des déversements.'
            write(lTra,'(a)') trim(message) ; write(error_unit,'(a)') trim(message)
            message = ' Vérifiez les pas d''espaces réels.'
            write(lTra,'(a)') trim(message) ; write(error_unit,'(a)') trim(message)
            call echec_initialisation
            stop 230
         endif
      else
         write(lTra,'(a,i3,a,2(a,f9.2),a,f6.3,a)') ' Bief ',ib,rgrd,' Pm ',xgeo(js1),' a ',xgeo(js2),&
                                                       ' hors réseau (coeff débit : ',cd,')'
      endif
      !---Lecture de la ligne suivante
   enddo
   close (luu)

end subroutine Lire_DEV



subroutine Erreur_Lecture_DEV(lu,nligne,u,yz)
!==============================================================================
!     Erreur de lecture du fichier DEV
!==============================================================================
   use, intrinsic :: iso_fortran_env, only: error_unit
   implicit none
   ! -- prototype --
   integer, intent(in) :: lu,nligne
   character(len=*),intent(in) :: u
   character, intent(in) :: yz
   ! -- variables locales --
   character(len=10) :: detype=''

   if (yz ==' ') then
      detype = ''
   else
      detype = ' de type '//yz
   endif
   write(lu,'(a,i3,2a)') ' >>>> ERREUR de lecture dans .DEV, ligne numéro ',nligne,detype
   write(lu,'(1x,a78)') u

   write(error_unit,'(a,i3,2a)')' >>>> ERREUR de lecture dans .DEV, ligne numéro ',nligne,detype
   write(error_unit,'(1x,a78)') u

end subroutine Erreur_Lecture_DEV



subroutine Lire_RUG(RUG_file)  !Lecture de RUG
!==============================================================================
!                          LECTURE DU FICHIER .RUG
!==============================================================================
   use parametres, only: long, lname, zero, lTra, l9
   use, intrinsic :: iso_fortran_env, only: error_unit

   use TopoGeometrie, only: la_topo, xgeo
   implicit none
   ! -- Prototype
   character(len=lname),intent(in) :: RUG_file
   ! -- Variables locales temporaires --
   integer :: ib, is, luu, l=0, nberr, ios, is1, is2
   character :: car1=''
   character(len=80) :: ligne=''
   real(kind=long) :: x1, x2, skl, skm, skr, sk1, sk2
   character(len=10) :: blanc=repeat(' ',10)
   integer,pointer :: ismax,ibmax

   ismax => la_topo%net%ns
   ibmax => la_topo%net%nb
   !initialisation à -1 partout ; permet de détecter les sections non pourvues après lecture de RUG
   do is = 1, ismax
      la_topo%sections(is)%ks(:) = -1._long
   enddo

   open(newunit=luu,file=trim(RUG_file),status='old',form='formatted')
   do
      read(luu,'(a)',iostat=ios) ligne
      if (ios /= 0) exit
      l = l+1    !compteur de lignes lues
      car1 = ligne(1:1)
      if (car1 == '*') then  !commentaire ignoré
         cycle
      elseif (car1 == 'k' .or. car1 == 'K') then  !Loi de frottement de Manning-Strickler
         read(ligne,'(1x,i3,6x,4f10.0)') ib,x1,x2,sk1,sk2
         if (ib > ibmax) then
            write(l9,'(a,3(i3,a))') ' Erreur dans .RUG à la ligne ',L,' bief ',ib, &
                                     ' non trouvé ; la ligne ',L,' est ignorée'
            cycle
         endif
         is1 = la_topo%biefs(ib)%is1
         is2 = la_topo%biefs(ib)%is2
         !On essaye de lire un 3e coefficient pour ISM, si on échoue on le prend égal au 1er
         !ATTENTION : on change l'ordre des coefficients par rapport à Mage-7
         if (len_trim(ligne) < 51) then
            !il n'y a pas de 3e coefficient
            skm = sk1 ; skl = sk2 ; skr = sk2
         else
            read(ligne(51:len_trim(ligne)),'(f10.0)',iostat=ios) skr
            if (ios /= 0) then
               skm = sk1 ; skl = sk2 ; skr = sk2 !il n'y a pas de 3e coefficient
            else
               skl = sk1 ; skm = sk2 !on a trouvé un 3e coefficient
            endif
         endif
         !------valeurs par défaut
         if (skl < 0.1_long) skl = skm
         if (skr < 0.1_long) skr = skm
         if (ligne(11:20) == blanc .and. ligne(21:30) == blanc) then
            x1 = xgeo(is1)
            x2 = xgeo(is2)
         elseif (ligne(11:20) == blanc .or. ligne(21:30) == blanc) then
            write(lTra,*) 'Définition ambigüe du secteur de rugosité à la ligne ',L
            write(error_unit,*) 'Définition ambigüe du secteur de rugosité à la ligne ',L
            call echec_initialisation
            stop 225
         endif
         !------recherche des sections entre X1 et X2
         do is = is1,is2
            if (xgeo(is) >= min(x1,x2)-0.001_long .and. xgeo(is) <= max(x1,x2)+0.001_long) then
               la_topo%sections(is)%ks(1) = skl
               la_topo%sections(is)%ks(2) = skm
               la_topo%sections(is)%ks(3) = skr
            endif
         enddo
      else
         write(l9,*) ' Type de loi de frottement inconnu à la ligne ',L
      endif
   enddo
   close (luu)

   !---Vérifications
   nberr = 0
   do ib = 1, ibmax  !parcours du réseau dans l'ordre des données
      is1 = la_topo%biefs(ib)%is1
      is2 = la_topo%biefs(ib)%is2
      do is = is1,is2
         if (la_topo%sections(is)%ks(2) < 0.1_long) then
            nberr = nberr + 1
            write(error_unit,'(a,f9.2,a,i3,a)') ' La section située au Pm ',xgeo(is), &
                    ' du bief ',ib,' n''a pas de coeff. de rugosité'
            write(l9,'(a,f9.2,a,i3,a)') ' La section située au Pm ',xgeo(is), &
                    ' du bief ',ib,' n''a pas de coeff. de rugosité'
            write(lTra,'(a,f9.2,a,i3,a)') ' La section située au Pm ',xgeo(is), &
                    ' du bief ',ib,' n''a pas de coeff. de rugosité'
         endif
      enddo
   enddo
   if (nberr > 0) then
      write(error_unit,'(a,i3,a)') ' >>> erreur : il y a ',nberr,' profil(s) sans coefficient de rugosité'
      write(l9,'(a,i3,a)') ' >>> erreur : il y a ',nberr,' profil(s) sans coefficient de rugosité'
      write(lTra,'(a,i3,a)') ' >>> erreur : il y a ',nberr,' profil(s) sans coefficient de rugosité'
      write(error_unit,'(a)')' possible problème d''arrondi des pk par pamhyr'
      write(l9,'(a)')' possible problème d''arrondi des pk par pamhyr'
      write(lTra,'(a)')' possible problème d''arrondi des pk par pamhyr'
      call echec_initialisation
      stop '>>> il manque des coefficients de rugosité !!!'
   endif
end subroutine Lire_RUG



subroutine Lire_DRG(DRG_file)  !Lecture de DRG
!==============================================================================
!                          Lecture du fichier .DRG
!==============================================================================
   use parametres, only: long, lname, zero, g2, lTra, l9
   use, intrinsic :: iso_fortran_env, only: error_unit

   use TopoGeometrie, only: la_topo, xgeo
   implicit none
   ! -- Prototype
   character(len=lname),intent(in) :: DRG_file
   ! -- Variables locales temporaires --
   integer :: ib, is, luu, l=0, ios, is1, is2
   character :: car1
   character(len=80) :: ligne=''
   real(kind=long) :: cdf_l, cdf_m, cdf_r, x1, x2
   character(len=10) :: blanc=repeat(' ',10)
   integer,pointer :: ismax,ibmax

   ismax => la_topo%net%ns
   ibmax => la_topo%net%nb

   open(newunit=luu,file=trim(DRG_file),status='old',form='formatted')
   do
      read(luu,'(a)',iostat=ios) ligne
      if (ios /= 0) exit
      l = l+1    !compteur de lignes lues
      car1 = ligne(1:1)
      if (car1 == '*') then  !commentaire ignoré
         cycle
      elseif (car1 == 'd' .or. car1 == 'D') then  !Loi de drag-force
         read(ligne,'(1x,i3,6x,5f10.0)') ib,x1,x2,cdf_l,cdf_m,cdf_r
         if (ib > ibmax) then
            write(l9,'(a,3(i3,a))') ' Erreur dans .DRG à la ligne ',L,' bief ',ib, &
                                     ' non trouvé ; la ligne ',L,' est ignorée'
            cycle
         endif
         is1 = la_topo%biefs(ib)%is1
         is2 = la_topo%biefs(ib)%is2
         !------valeurs par défaut
         if (ligne(11:20) == blanc .and. ligne(21:30) == blanc) then
            x1 = xgeo(is1)
            x2 = xgeo(is2)
         elseif (ligne(11:20) == blanc .or. ligne(21:30) == blanc) then
            write(lTra,*) 'Définition ambigüe du secteur de drag-force à la ligne ',L
            write(l9,*) 'Définition ambigüe du secteur de drag-force à la ligne ',L
            write(error_unit,*) 'Définition ambigüe du secteur de drag-force à la ligne ',L
            call echec_initialisation
            stop 225
         endif
         !------recherche des sections entre X1 et X2
         do is = is1,is2
            if (xgeo(is) >= min(x1,x2)-0.001_long .and. xgeo(is) <= max(x1,x2)+0.001_long) then
               la_topo%sections(is)%drag_force(1) = cdf_l / g2
               la_topo%sections(is)%drag_force(2) = cdf_m / g2
               la_topo%sections(is)%drag_force(3) = cdf_r / g2
            endif
         enddo
      else
         write(l9,*) ' Type de loi de drag-force inconnu à la ligne ',L
      endif
   enddo
   close (luu)

end subroutine Lire_DRG


subroutine init_drag_force
!==============================================================================
! initialisation des paramètres de trainée (drag force)
!==============================================================================
   use parametres, only: zero
   use TopoGeometrie, only: la_topo, profil
   implicit none
   ! -- variables --
   integer :: is

   ! initialisation à 0
   do is = 1, la_topo%net%ns
      la_topo%sections(is)%drag_force(:) = zero
   enddo

end subroutine init_drag_force


subroutine Lire_PSI(PSI_file)  !Lecture de PSI
!==============================================================================
!               Lecture du fichier .PSI qui définit Psi_t pour ISM
!==============================================================================
   use parametres, only: long, lname, zero, g2, lTra, l9
   use, intrinsic :: iso_fortran_env, only: error_unit

   use TopoGeometrie, only: la_topo, xgeo
   implicit none
   ! -- Prototype
   character(len=lname),intent(in) :: PSI_file
   ! -- Variables locales temporaires --
   integer :: ib, is, luu, l=0, ios, is1, is2
   character :: car1
   character(len=80) :: ligne=''
   real(kind=long) :: x1, x2, psi_t
   character(len=10) :: blanc=repeat(' ',10)
   integer,pointer :: ismax,ibmax

   ismax => la_topo%net%ns
   ibmax => la_topo%net%nb

   open(newunit=luu,file=trim(PSI_file),status='old',form='formatted')
   do
      read(luu,'(a)',iostat=ios) ligne
      if (ios /= 0) exit
      l = l+1    !compteur de lignes lues
      car1 = ligne(1:1)
      if (car1 == '*') then  !commentaire ignoré
         cycle
      elseif (car1 == 'p' .or. car1 == 'P') then  !Loi de Psi_t
         read(ligne,'(1x,i3,6x,3f10.0)') ib,x1,x2,psi_t
         if (ib > ibmax) then
            write(l9,'(a,3(i3,a))') ' Erreur dans .PSI à la ligne ',L,' bief ',ib, &
                                     ' non trouvé ; la ligne ',L,' est ignorée'
            cycle
         endif
         is1 = la_topo%biefs(ib)%is1
         is2 = la_topo%biefs(ib)%is2
         !------valeurs par défaut
         if (ligne(11:20) == blanc .and. ligne(21:30) == blanc) then
            x1 = xgeo(is1)
            x2 = xgeo(is2)
         elseif (ligne(11:20) == blanc .or. ligne(21:30) == blanc) then
            write(lTra,*) 'Définition ambigüe du secteur de Psi_t à la ligne ',L
            write(l9,*) 'Définition ambigüe du secteur de Psi_t à la ligne ',L
            write(error_unit,*) 'Définition ambigüe du secteur de Psi_t à la ligne ',L
            call echec_initialisation
            stop 225
         endif
         !------recherche des sections entre X1 et X2
         do is = is1,is2
            if (xgeo(is) >= min(x1,x2)-0.001_long .and. xgeo(is) <= max(x1,x2)+0.001_long) then
               la_topo%sections(is)%Psi_t = psi_t
            endif
         enddo
      else
         write(l9,*) ' Type de loi de Psi_t inconnu à la ligne ',L
      endif
   enddo
   close (luu)
end subroutine Lire_PSI


subroutine init_psi_t
!==============================================================================
! initialisation du paramètre Psi_t pour ISM
!==============================================================================
   use parametres, only: zero
   use TopoGeometrie, only: la_topo
   implicit none
   ! -- variables --
   integer :: is

   ! initialisation à 0
   do is = 1, la_topo%net%ns
      la_topo%sections(is)%psi_t = zero
   enddo

end subroutine init_psi_t
