!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - INRAE - 1989-2020                           #
!# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################



subroutine Lire_SIN(SIN_file,nbele)
!==============================================================================
!                          Lecture du fichier .SIN
!
! NOTE: dans cette version avec allocation dynamique nbele = size(all_OuvEle)
!==============================================================================
   use parametres, only: long, zero, un, cent, total, rivg, rivd, nesup, lTra, lname, l9
   use iso_fortran_env, only: error_unit, output_unit, compiler_version

   use casiers, only: nclat, rvclat
   use data_num_fixes, only: tmax, flag
   use mage_utilitaires, only: next_int, next_real, next_string, zegal, is_zero, get_gfortran
   use topoGeometrie, only: la_topo, iSect, zfd, numero_bief
   use Ouvrages, only: all_OuvCmp, all_OuvEle, some_ouvrages_writable, nb_mobiles, ligne_tra, ltra_size, charger_debitx
   use Erreurs_Sing, only: IniErrSing
   implicit none
   ! -- prototype --
   character(len=lname),intent(in) :: SIN_file
   integer,intent(out) :: nbele
   ! -- variables --
   logical :: bool9
   integer :: i, i11, ib, ibsa, indic, is, iuvn, js, jssa, k, l, ls, luu
   integer :: n, nbk, nk, nk1, nl, nmax, npk, ns, nsn, nss2, riv, ns_orphan
   integer :: next_field, nf0, ios, nzone
   real(kind=long) :: along, charg, coeff, cote, dbi, dbipom, dta, dtm, p1, p2, p3, p4, p5
   real(kind=long) :: qnom, xis, zal1, zal2, zal3, zf, zf0, xssa, pk
   character :: car1=''
   character(len=10) :: un_nom=''
   character(len=20) :: une_fonction=''
   character(len=250) :: une_dll='', un_fichier=''
   character(len=512) :: lect=''
   character(len=1), parameter :: separateur=''
   integer, pointer :: iss(:),ismax,nsmax,is1(:)
   integer :: ierr = huge(0)
   integer, allocatable :: jss(:)

   !On commence par compter les sections singulières (is et is+1 mêmes Pk) -> 1ère estimation de la_topo%net%nss
   call initialise_section_sing()

   iss(1:) => la_topo%sections(1:)%iss
   ismax => la_topo%net%ns
   nsmax => la_topo%net%nss
   is1(1:) => la_topo%biefs(1:)%is1

   !---lecture du fichier .sin (ouvrages élémentaires)
   flag = zero     !flag sera mis à 1. s'il y a des pompes
   bool9 = .false.
   nbele = 0       !nombre d'ouvrages élémentaires
   n = 0           !compteur d'ouvrages élémentaires
   ns = 0          !compteur des sections singulières
   l = 0

   if (SIN_file /= '') then
      open(newunit=luu,file=trim(SIN_file),status='old',form='formatted')
      nl = 0             !compteur des ouvrages élémentaires
      nk = nsmax         !compteur des ouvrages composites
      ns_orphan = nsmax  !compteur des sections singulières sans ouvrage
      allocate (jss(size(iss))) ; jss = iss
      do    ! 1ère passe pour compter les ouvrages élémentaires et faire les allocations de mémoire
         read(luu,'(a)',iostat=ios) lect
         if (ios < 0) then
            exit                             !on est arrivé à la fin du fichier
         elseif (lect(1:1) == '*') then      !ligne de commentaire : ne compte pas
            cycle
         else if (len_trim(lect) < 1) then   !ligne vide : ne compte pas
            cycle
         else
            nl = nl + 1                      !nouvel ouvrage élémentaire (pas besoin de vérifier que le code existe bien)
            !on a déjà une estimation du nombre de sections singulières, on ajoute les ouvrages composites sans section singulière
            !c'est-à-dire entre 2 sections qui ne sont pas à la même abscisse
            next_field = 2 ; ierr = 1
            ib = next_int(lect,separateur,next_field) ; ierr = min(ierr,next_field)
            xis = next_real(lect,separateur,next_field) ; ierr = min(ierr,next_field)
            if (ierr == 0) call erreur_lecture(SIN_file,lect)
            is = iSect(ib,xis,0.01_long)
            if (iss(is+1) == 0) then
               nk = nk + 1  ;  iss(is+1) = - nk  !ouvrage sans section singulière
            else if (jss(is+1) /= 0) then
               ns_orphan = ns_orphan - 1         !cet ouvrage est dans une section singulière
               jss(is+1) = 0
            endif
         endif
      enddo
      nsmax = nk
      nl = nl + ns_orphan !il faut ajouter le nombre de sections singulières sans ouvrage au décompte des ouvrages élémentaires
                          !car on va créer un ouvrage élémentaire fictif pour chaque section singulière sans ouvrage
      deallocate (jss)
      !allocations des ouvrages élémentaires et composites
      allocate (all_OuvEle(nl), all_OuvCmp(nsmax))
      !initialisation du nombre d'ouvrages élémentaires pour chaque ouvrage composite
      all_OuvCmp(1:nk)%ne = 0
      !initialisation des ouvrages élémentaires
      do i = 1, nl
         all_OuvEle(i)%cvar = ''
         all_OuvEle(i)%iuv = 99
         all_OuvEle(i)%uv1 = zero
         all_OuvEle(i)%uv2 = zero
         all_OuvEle(i)%uv3 = zero
         all_OuvEle(i)%uv4 = zero
         all_OuvEle(i)%uv5 = zero
         all_OuvEle(i)%za1 = zero
         all_OuvEle(i)%za2 = zero
         all_OuvEle(i)%za3 = zero
         all_OuvEle(i)%irf = 0
         all_OuvEle(i)%is_writable = .false.
      enddo
      !retour au début du fichier pour la vraie lecture
      rewind (luu)
      do    !boucle de lecture des singularités sur le fichier [.sin]
         read(luu,'(a)',iostat=ios) lect
         if (ios < 0) exit
         l = l+1
         car1 = lect(1:1)
         if (car1 == '*') then           !ligne de commentaire : on saute
            cycle
         else if (len_trim(lect) < 1) then   !ligne vide : on saute
            cycle
         else if (car1 == 'B' .or. car1 == 'C' .or. car1 == 'D'  &
             .or. car1 == 'I' .or. car1 == 'L' .or. car1 == 'O'  &
             .or. car1 == 'P' .or. car1 == 'A' .or. car1 == 'T'  &
             .or. car1 == 'V' .or. car1 == 'W' .or. car1 == 'X'  &
             .or. car1 == 'F') then
            next_field = 2 ; ierr = 1
            ib = next_int(lect,separateur,next_field) ; ierr = min(ierr,next_field)
            xis = next_real(lect,separateur,next_field) ; ierr = min(ierr,next_field)
            if (ierr == 0) call erreur_lecture(SIN_file,lect)
            is = iSect(ib,xis,0.01_long)
            if (is == 0) then
               write(lTra,'(a,i3,a)') ' >>>> Fichier SIN : le bief ',ib,' est inconnu sur ce réseau <<<<'
               write(error_unit,'(a,i3,a)') ' >>>> Fichier SIN : le bief ',ib,' est inconnu sur ce réseau <<<<'
               call echec_initialisation
               stop 192
            else if (is < 0) then
               write(lTra,'(2a,f9.2,a,i3,a)') ' >>>> Fichier SIN  : il n''y a pas de section à', &
                                                ' l''abscisse ',xis,' dans le bief ',ib,' <<<<'
               write(error_unit,'(2a,f9.2,a,i3,a)') ' >>>> Fichier SIN  : il n''y a pas de section à', &
                                           ' l''abscisse ',xis,' dans le bief ',ib,' <<<<'
               call echec_initialisation
               stop 193
            endif
            i11 = is-is1(ib)+1  !numéro local de la section
            if (is == la_topo%biefs(ib)%is2) then !l'ouvrage est dans la section aval du bief
               write(lTra,'(1x,a,f9.2,a,i3,a)') 'L''ouvrage au Pk ',xis,' du bief ',ib,  &
                                          ' est dans la dernière section du bief.'
               write(lTra,'(1x,a)') 'Cette section doit être dupliquée pour pouvoir continuer'
               write(error_unit,'(1x,a,f9.2,a,i3,a)') 'L''ouvrage au Pk ',xis,' du bief ',ib,  &
                                          ' est dans la dernière section du bief.'
               write(error_unit,'(1x,a)') 'Cette section doit être dupliquée pour pouvoir continuer'
               call echec_initialisation
               stop 3
            elseif (iss(is+1) == 0) then   !détection des ouvrages sans section singulière (normalement c'est déjà fait)
               nsmax = nsmax+1 ; iss(is+1) = -nsmax
            endif
         endif

         !--lecture des paramètres d'ouvrages
         ierr = huge(0)
         !along = next_real(lect,separateur,next_field) ; ierr = min(ierr,next_field)
         p1 = next_real(lect,separateur,next_field) ; ierr = min(ierr,next_field)
         !cote = next_real(lect,separateur,next_field) ; ierr = min(ierr,next_field)
         p2 = next_real(lect,separateur,next_field) ; ierr = min(ierr,next_field)
         nf0 = next_field
         !charg = next_real(lect,separateur,next_field)
         p3 = next_real(lect,separateur,next_field)
         if (next_field == 0) p3 = zero
         nf0 = next_field
         !coeff = next_real(lect,separateur,next_field)
         p4 = next_real(lect,separateur,next_field)
         if (next_field == 0) p4 = zero
         nf0 = next_field
         p5 = next_real(lect,separateur,next_field)
         if (next_field == 0) then
            !pas de paramètre P5 mais peut-être un nom quand même
            p5 = zero
            next_field = nf0
         endif
         un_nom = next_string(lect,separateur,next_field)
         if (ierr == 0) call erreur_lecture(SIN_file,trim(lect))
         n = n+1   ! numéro d'ordre de l'ouvrage élémentaire
         if (n > size(all_OuvEle)) then
            write(lTra,3000) size(all_OuvEle), n  ;  write(error_unit,3000) size(all_OuvEle), n
            call echec_initialisation
            stop 137
         endif
         js = is+1 !numéro de section contenant l'ouvrage n
         nsn = iabs(iss(js))    !numéro de section singulière contenant l'ouv. n
         if (nsn == 0) call err032(xis,ib)
         if (all_OuvCmp(nsn)%ne == 0) then  !lecture du 1er ouvrage élémentaire de la section singulière nsn
            ns = ns+1
            iss(js) = -iss(js)  !marquage de la section singulière js qui contient un ouvrage
            all_OuvCmp(nsn)%js = js  ;  all_OuvCmp(nsn)%ne = 1  ;  all_OuvCmp(nsn)%OuEl(1,1) = n
         else                       !lecture des autres ouvrages élémentaires de la section singulière nsn
            all_OuvCmp(nsn)%ne = all_OuvCmp(nsn)%ne+1
            nss2 = all_OuvCmp(nsn)%ne
            if (nss2 > nesup) then
               write(lTra,1000) xis, ib, nesup  ;  write(error_unit,1000) xis, ib, nesup
               call echec_initialisation
               stop 138
            endif
            all_OuvCmp(nsn)%OuEl(nss2,1) = n
         end if

         select case (car1)
            case ('X')               ! ouvrage défini par l'utilisateur
               une_fonction = 'debitx'
               un_fichier = trim(un_nom)//'.f90'
               une_dll = './'//trim(un_nom)//'.so'
               all_OuvEle(n)%iuv = 9
               all_OuvEle(n)%uv1 = p1
               all_OuvEle(n)%uv2 = p2
               all_OuvEle(n)%uv3 = p3
               all_OuvEle(n)%uv4 = p4
               all_OuvEle(n)%uv5 = p5
               call execute_command_line(get_gfortran()//' -shared -fPIC -o '//trim(une_dll)//' '//&
                                         trim(un_fichier) , wait=.true., exitstat=i)
               call charger_debitx(all_OuvEle(n), une_dll, une_fonction)
            case ('F')                ! orifice-voute
               all_OuvEle(n)%iuv = 11
               all_OuvEle(n)%uv1 = p1              ! largeur
               all_OuvEle(n)%uv2 = p2              ! cote de déversement
               all_OuvEle(n)%uv3 = p3 - p2         ! hauteur de la partie rectangulaire
               all_OuvEle(n)%uv4 = p5 - p3         ! hauteur de la voute
               if (is_zero(p4)) p4 = 0.4_long
               all_OuvEle(n)%uv5 = p4              ! coeff de débit
               if (p1 < 2._long*all_OuvEle(n)%uv4) then
                  write (error_unit,'(a,/,a,/,a)') ' >>>> erreur à la ligne ',lect, &
                           ' la hauteur de la voute est plus grande que la demie largeur de l''ouvrage'
                  write (lTra,'(a,/,a,/,a)') ' >>>> erreur a la ligne ',lect, &
                             ' la hauteur de la voute est plus grande que la demie largeur de l''ouvrage'
                  call echec_initialisation
                  stop 139
               endif
            case ('T')               ! déversoir trapézoïdal
               all_OuvEle(n)%iuv = 10
               all_OuvEle(n)%uv1 = p1                !longueur déversante
               all_OuvEle(n)%uv2 = p2                !cote de déversement
               if (p3 < 0.001_long) p3 = 9999._long  !cote charge
               all_OuvEle(n)%uv3 = p3 - p2           !ouverture
               if (is_zero(p4)) p4 = 0.4_long
               all_OuvEle(n)%uv4 = p4                !coeff débit
               all_OuvEle(n)%uv5 = p5                !fruit des joues du déversoir
            case ('B')               ! buse
               all_OuvEle(n)%iuv = 8
               all_OuvEle(n)%uv1 = p1                !diamètre
               charg = min(p3,p1-0.0001_long)        !hauteur d'envasement bornée par le diamètre
               cote = p2 + charg                     !cote du fond + hauteur d'envasement
               all_OuvEle(n)%uv2 = cote              !cote de déversement
               all_OuvEle(n)%uv3 = charg             !hauteur d'envasement
               if (is_zero(p4)) p4 = 0.4_long
               all_OuvEle(n)%uv4 = p4                !coeff débit
               all_OuvEle(n)%uv5 = zero
            case ('A')               ! perte de charge à la borda
               all_OuvEle(n)%iuv = 91
               all_OuvEle(n)%uv1 = max(0.1_long,p1)             !pas d'espace
               if (is_zero(p2) .or. p2 >= 1._long) p2 = 0.15_long
               all_OuvEle(n)%uv2 = max(0.0001_long,p2)          !seuil de prise en compte de l'élargissement
               all_OuvEle(n)%uv3 = zero
               if (is_zero(p3)) p3 = 1._long
               all_OuvEle(n)%uv4 = p3
            case ('D')               !Déversoir-Orifice / seuil mobile
               all_OuvEle(n)%iuv = 0
               all_OuvEle(n)%uv1 = p1                !longueur déversante
               all_OuvEle(n)%uv2 = p2                !cote de déversement
               if (is_zero(p3)) p3 = 9999.999_long
               all_OuvEle(n)%uv3 = p3 - p2           !ouverture de l'orifice
               if (is_zero(p4)) p4 = 0.4_long
               all_OuvEle(n)%uv4 = p4                !coeff de débit
               all_OuvEle(n)%uv5 = zfd(is)           !cote minimale du seuil mobile
            case ('O')               !Déversoir-Orifice / ouverture mobile
               all_OuvEle(n)%iuv = 6
               all_OuvEle(n)%uv1 = p1                !longueur déversante
               all_OuvEle(n)%uv2 = p2                !cote de déversement
               if (is_zero(p3)) p3 = 9999.999_long
               all_OuvEle(n)%uv3 = p3 - p2           !ouverture de l'orifice
               if (is_zero(p4)) p4 = 0.4_long
               all_OuvEle(n)%uv4 = p4                !coeff de débit
               if (abs(p5) < 0.0001_long) p5 = p2 + 9999.999_long
               all_OuvEle(n)%uv5 = p5 - p2           !ouverture maximale
            case ('V')               !Vanne de fond (loi standard) / ouverture mobile
               all_OuvEle(n)%iuv = 2
               all_OuvEle(n)%uv1 = p1                !longueur déversante
               all_OuvEle(n)%uv2 = p2                !cote de déversement
               all_OuvEle(n)%uv3 = p3 - p2           !ouverture de la vanne
               if (is_zero(p4)) p4 = 0.61_long
               all_OuvEle(n)%uv4 = p4                !coeff de débit
               if (abs(p5) < 0.0001_long) p5 = p3
               all_OuvEle(n)%uv5 = p5 - p2           !ouverture maximale
            case ('W')               !Vanne de fond (loi simplifiée) / ouverture mobile
               all_OuvEle(n)%iuv = 4
               all_OuvEle(n)%uv1 = p1                !longueur déversante
               all_OuvEle(n)%uv2 = p2                !cote de déversement
               all_OuvEle(n)%uv3 = p3 - p2           !ouverture de la vanne
               if (is_zero(p4)) p4 = 0.61_long
               all_OuvEle(n)%uv4 = p4                !coeff de débit
               if (abs(p5) < 0.0001_long) p5 = p3
               all_OuvEle(n)%uv5 = p5 - p2           !ouverture maximale
            case ('C','I')           !clapets normaux et inversés : supprimés
               write(output_unit,*) '>>>> ERREUR : cette modélisation des clapets a été abandonnée'
               write(output_unit,*) '              utilisez plutôt un orifice avec une règle de régulation de type CLAPET'
               call echec_initialisation
               stop 150
            case ('P')               !Pompe
               flag = 1._long
               ierr = huge(0)
               next_field = 2   ! on relit la ligne en entier
               ib = next_int(lect,separateur,next_field) ; ierr = min(ierr,next_field)
               xis = next_real(lect,separateur,next_field) ; ierr = min(ierr,next_field)
               dbipom = next_real(lect,separateur,next_field) ; ierr = min(ierr,next_field)
               dtm    = next_real(lect,separateur,next_field) ; ierr = min(ierr,next_field)
               dta    = next_real(lect,separateur,next_field) ; ierr = min(ierr,next_field)
               zal1   = next_real(lect,separateur,next_field) ; ierr = min(ierr,next_field)
               zal2   = next_real(lect,separateur,next_field) ; ierr = min(ierr,next_field)
               zal3   = next_real(lect,separateur,next_field) ; ierr = min(ierr,next_field)
               ibsa   = next_int(lect,separateur,next_field) ; ierr = min(ierr,next_field)
               if (ierr == 0) call erreur_lecture(SIN_file,trim(lect))
               nf0    = next_field
               xssa   = next_real(lect,separateur,next_field)
               if (next_field == 0) next_field = nf0
               jssa = iSect(ibsa,xssa)
               if (jssa == 0) then
                  write(lTra,'(a,i3,a)') ' >>>> Fichier SIN (pompe) : le bief ',ibsa,' est inconnu sur ce réseau <<<<'
                  write(error_unit,'(a,i3,a)') ' >>>> Fichier SIN (pompe) : le bief ',ibsa,' est inconnu sur ce réseau <<<<'
                  call echec_initialisation
                  stop 192
               else if (jssa < 0) then
                  write(lTra,'(2a,f9.2,a,i3,a)') ' >>>> Fichier SIN (pompe) : il n''y a pas de section à',  &
                                                   ' l''abscisse ',xssa,' dans le bief ',ibsa,' <<<<'
                  write(error_unit,'(2a,f9.2,a,i3,a)') ' >>>> Fichier SIN (pompe) : il n''y a pas de section à',  &
                                              ' l''abscisse ',xssa,' dans le bief ',ibsa,' <<<<'
                  call echec_initialisation
                  stop 193
               endif
               un_nom = next_string(lect,separateur,next_field)
               all_OuvEle(n)%iuv = 3  !affectation du numéro de type d'ouvrage
               !affectation des paramètres de la pompe
               all_OuvEle(n)%uv1 = dbipom  ;  all_OuvEle(n)%uv2 = 10._long*tmax  ;  all_OuvEle(n)%uv3 = 11._long*tmax
               if (.not.is_zero(dtm)) then
                  all_OuvEle(n)%uv4 = dtm
               else
                  all_OuvEle(n)%uv4 = 180._long
               end if
               if (.not.is_zero(dta)) then
                  all_OuvEle(n)%uv5 = dta
               else
                  all_OuvEle(n)%uv5 = 180._long
               end if

               all_OuvEle(n)%za1 = zal1  !cote de démarrage
               all_OuvEle(n)%za2 = zal2  !cote d'arrêt
               if (is_zero(all_OuvEle(n)%za1) .and. is_zero(all_OuvEle(n)%za2)) then
                  all_OuvEle(n)%za1 = 9999._long
                  all_OuvEle(n)%za2 = 9998._long
               endif
               all_OuvEle(n)%za3 = zal3  !tirant d'eau de désamorçage
               if (is_zero(all_OuvEle(n)%za3)) then
                  all_OuvEle(n)%za3 = 0.1_long
               endif
               !section de référence
               if (ibsa == 0 .and. jssa == 0) then
                  all_OuvEle(n)%irf = js-1
                else
                  all_OuvEle(n)%irf = is1(ibsa)+jssa-1
               endif
               !enregistrement de la pompe comme ouvrage mobile dont on va enregistrer les positions
               nb_mobiles = nb_mobiles + 1
               all_OuvEle(n)%mob = nb_mobiles
            case ('L')               !déversoir latéral
               next_field = 2   ! on relit la ligne en entier
               ib = next_int(lect,separateur,next_field) ; ierr = min(ierr,next_field)
               xis = next_real(lect,separateur,next_field) ; ierr = min(ierr,next_field)
               along = next_real(lect,separateur,next_field) ; ierr = min(ierr,next_field)
               cote  = next_real(lect,separateur,next_field) ; ierr = min(ierr,next_field)
               charg = next_real(lect,separateur,next_field) ; ierr = min(ierr,next_field)
               coeff = next_real(lect,separateur,next_field) ; ierr = min(ierr,next_field)
               p5    = next_real(lect,separateur,next_field) ; ierr = min(ierr,next_field)
               ibsa  = next_int(lect,separateur,next_field) ; ierr = min(ierr,next_field)
               xssa  = next_real(lect,separateur,next_field) ; ierr = min(ierr,next_field)
               un_nom  = next_string(lect,separateur,next_field)
               if (ierr == 0) call erreur_lecture(SIN_file,trim(lect))
               all_OuvEle(n)%iuv = 5  !affectation du numéro de type d'ouvrage
               !affectation des paramètre d'ouvrage
               all_OuvEle(n)%uv1 = along
               all_OuvEle(n)%uv2 = cote
               if (is_zero(charg)) charg = cote + 9999._long  !valeur par défaut
               all_OuvEle(n)%uv3 = charg-cote
               if (is_zero(coeff)) coeff = 0.4_long  !valeur par défaut
               all_OuvEle(n)%uv4 = coeff
               !définition de la section de rejet du déversoir latéral
               if (ibsa == 0) then
                  all_OuvEle(n)%irf = 0
               else
                  jssa = iSect(ibsa,xssa)  !calcul de jssa seulement si la section de rejet existe
                  if (jssa == 0) then
                     write(lTra,'(a,i3,a)') ' >>>> Fichier SIN (déversoir latéral) : le bief ',ibsa,&
                                              ' est inconnu sur ce réseau <<<<'
                     write(error_unit,'(a,i3,a)') ' >>>> Fichier SIN (déversoir latéral) : le bief ',ibsa,&
                                         ' est inconnu sur ce réseau <<<<'
                     call echec_initialisation
                     stop 192
                  else if (jssa < 0) then
                     write(lTra,'(2a,f9.2,a,i3,a)') ' >>>> Fichier SIN (déversoir latéral) : ',   &
                                  'il n''y a pas de section à l''abscisse ',xssa,' dans le bief ',ibsa,' <<<<'
                     write(error_unit,'(2a,f9.2,a,i3,a)') ' >>>> Fichier SIN (déversoir latéral) : ',  &
                             'il n''y a pas de section à l''abscisse ',xssa,' dans le bief ',ibsa,' <<<<'
                     call echec_initialisation
                     stop 193
                  endif
                  all_OuvEle(n)%irf = is1(ibsa)+jssa-1
                  if (nclat(rivg,all_OuvEle(n)%irf) == 0) then  !par défaut on fait le déversement depuis la rive gauche
                     nclat(rivg,all_OuvEle(n)%irf) = -(js-1)  ;  riv = rivg
                  else if (nclat(rivd,all_OuvEle(n)%irf) == 0) then
                     nclat(rivd,all_OuvEle(n)%irf) = -(js-1)  ;  riv = rivd
                  else
                     write(error_unit,'(a,i3,a)') ' >>>> le déversoir latéral à la ligne ',l,  &
                                          ' est concurrent avec un déversement latéral par les berges'
                     write(lTra,'(a,i3,2a)') ' >>>> le déversoir latéral à la ligne ',l,  &
                                            ' est concurrent avec un déversement latéral par les berges'
                     call echec_initialisation
                     stop 215
                  endif
                  if (rvclat(riv,all_OuvEle(n)%irf) == 0) then  !par défaut le déversement arrivera dans la rive droite
                     rvclat(riv,all_OuvEle(n)%irf) = rivd
                  else
                     rvclat(riv,all_OuvEle(n)%irf) = rivg
                  endif
                  if (iss(all_OuvEle(n)%irf+1) /= 0) then
                     write(error_unit,'(a,i3,a)') ' >>>> la section de rejet du déversoir latéral à la ligne ',l, &
                                         ' est singulière <<<<'
                     write(lTra,'(a,i3,a)') ' >>>> la section de rejet du déversoir latéral à la ligne ',l, &
                                            ' est singulière <<<<'
                     call echec_initialisation
                     stop 216
                  endif
               endif
            case default
               write(lTra,*) '>>>> ERREUR dans .sin : ouvrage inconnu à la ligne : '
               write(lTra,*) trim(lect)
               write(error_unit,*) '>>>> ERREUR dans .sin : ouvrage inconnu à la ligne : '
               write(error_unit,*) trim(lect)
               call echec_initialisation
               stop 142
         end select
         !fin de la lecture des paramètres d'ouvrage

         !Nommage des ouvrages
         if (un_nom == '    ') write(un_nom,'(a1,i3.3)')'#',l
         all_OuvEle(n)%is_writable = .true.
         all_OuvEle(n)%cvar = un_nom
         do i = 1, n-1
            if (all_OuvEle(i)%cvar == un_nom) then
               write(lTra,*) ' >>>> Le nom d''ouvrage ',un_nom,' existe déjà'
               write(error_unit,*) ' >>>> Le nom d''ouvrage ',un_nom,' existe déjà'
               call echec_initialisation
               stop 143
            endif
         enddo

      enddo
      close(luu)
   else
      !pas de fichier SIN
      !si on arrive ici, les allocations n'ont pas encore été faites.
      !on va juste remplir chaque section singulière avec un ouvrage composite composé d'un seul ouvrage élémentaire fictif
      !d'où l'allocation à NSmax de all_OuvEle(:) et all_OuvCmp(:) s'ils ne sont pas déjà alloués
      if (.not.allocated(all_OuvEle)) allocate (all_OuvEle(nsmax))
      if (.not.allocated(all_OuvCmp)) allocate (all_OuvCmp(nsmax))
      ! NOTE: quand on arrive ici size(all_OuvCmp) = nsmax
      !initialisation du nombre d'ouvrages élémentaires pour chaque ouvrage composite
      all_OuvCmp(1:nsmax)%ne = 0
   endif

   if (ns < nsmax) then ! Il y des sections singulières sans ouvrages
      !Définition d'ouvrages fictifs avec perte de charge égale à la perte de
      !charge singulière éventuelle pour élargissement brusque
      do is = 1, ismax
         if (iss(is) < 0) then  !recherche des sections singulières sans ouvrage
            n = n+1
            if (n > size(all_OuvEle)) then
               write(lTra,'(3(a,/),a,i3.3)') ' Erreur 144 : ',' la somme du nombre d''ouvrages élémentaires', &
                         ' et du nombre de sections singulières sans',' ouvrages dépasse ',size(all_OuvEle)
               write(error_unit,'(3(a,/),a,i3.3)') ' Erreur 144 : ',' la somme du nombre d''ouvrages élémentaires', &
                       ' et du nombre de sections singulières sans',' ouvrages dépasse ',size(all_OuvEle)
               call echec_initialisation
               stop 144
            endif
            all_OuvEle(n)%is_writable = .false.  !cet ouvrage fictif n'apparaitra pas dans .TRA
            all_OuvEle(n)%iuv = 99
            all_OuvEle(n)%uv1 = 0.01_long
            all_OuvEle(n)%uv2 = 0.15_long
            all_OuvEle(n)%uv3 = 1.00_long
            all_OuvEle(n)%uv4 = zero
            all_OuvEle(n)%uv5 = zero

            iss(is) = -iss(is)  !la section is est singulière et ne contient pas d'ouvrage
            ls = iss(is)  !numéro de la section singulière is dans nss
            all_OuvCmp(ls)%js = is  ;  all_OuvCmp(ls)%ne = 1  ;  all_OuvCmp(ls)%OuEl(1,1) = n
         endif
      enddo
   endif

   nbele = n  !nbele = Nombre total d'ouvrages élémentaires

   !---Vérifications--------------------------------------------------------------
   if (allocated(all_OuvEle) .and. nbele /= size(all_OuvEle)) then
      write(lTra,3000) size(all_OuvEle), nbele
      write(error_unit,3000) size(all_OuvEle), nbele
      call echec_initialisation
      stop 145
   endif
   if (allocated(all_OuvCmp) .and. nsmax /= size(all_OuvCmp)) then
      write(lTra,3001) size(all_OuvCmp), nsmax
      write(error_unit,3001) size(all_OuvCmp), nsmax
      call echec_initialisation
      stop 145
   endif

   !initialisation du n° de sous-section dans laquelle se trouve chaque ouvrage élémentaire
   !on initialise à 0 tant qu'on n'a pas implémenté l'info dans le fichier SIN
   do ns = 1, nsmax
      nzone = la_topo%sections(all_OuvCmp(ns)%js)%nzone
      do k = 1, all_OuvCmp(ns)%ne
         if (all_OuvCmp(ns)%OuEl(k,2) >= 1 .and. all_OuvCmp(ns)%OuEl(k,2) <= nzone) cycle
         all_OuvCmp(ns)%OuEl(k,2) = 0
      enddo
   enddo

   do is = 1, ismax  !recherche des sections singulières sans ouvrage
      if (iss(is) < 0) then
         !bug dans la routine lire_sin() (ouvrages non-definis)
         write(lTra,*) ' >>>> erreur : il reste des sections singulières sans ouvrage'
         write(lTra,'(a)') ' Merci d''envoyer un rapport de bug'
         write(error_unit,*) ' >>>> erreur : il reste des sections singulières sans ouvrage'
         write(error_unit,'(a)') ' Merci d''envoyer un rapport de bug'
         call echec_initialisation
         stop 228
      endif
   enddo

   do ns = 1, nsmax  !recherche des clapets mélés à d'autres ouvrages
      nbk = all_OuvCmp(ns)%ne  ;  nk1 = all_OuvCmp(ns)%OuEl(1,1)
      if (nbk > 1 .and. all_OuvEle(nk1)%iuv == 1) then
         is = all_OuvCmp(ns)%js-1  ;  ib = numero_bief(is)  ;  pk = la_topo%sections(is)%pk
         write(error_unit,2000) pk,ib  ;  write(l9,2000) pk,ib  ;  write(lTra,2000) pk,ib
         bool9 = .true.
      endif
      if (all_OuvEle(nk1)%iuv /= 1 .and. nbk > 1) then
         do n = 2, all_OuvCmp(ns)%ne
            nk = all_OuvCmp(ns)%OuEl(n,1)
            if (all_OuvEle(nk)%iuv == 1) then
               is = all_OuvCmp(ns)%js-1  ;  ib = numero_bief(is)  ;  pk = la_topo%sections(is)%pk
               write(error_unit,2000) pk,ib  ;  write(l9,2000) pk,ib  ;  write(lTra,2000) pk,ib
               bool9 = .true.
            endif
         enddo
      endif
   enddo

   do ns = 1, nsmax !recherche des pompes mélés à d'autres ouvrages
      nbk = all_OuvCmp(ns)%ne  ;  nk1 = all_OuvCmp(ns)%OuEl(1,1)
      npk = 0
      if (nbk > 1) then
         do k = 1, nbk
            nk = all_OuvCmp(ns)%OuEl(k,1)
            if (all_OuvEle(nk)%iuv == 3) npk = npk+1
         enddo
         if (npk > 0 .and. npk /= nbk) then
            is = all_OuvCmp(ns)%js-1  ;  ib = numero_bief(is)  ;  pk = la_topo%sections(is)%pk
            write(error_unit,2001) pk,ib  ;  write(l9,2001) pk,ib  ;  write(lTra,2001) pk,ib
            bool9 = .true.
         endif
      endif
   enddo

   if (bool9) then
      write(lTra,'(a)') ' Erreurs dans la lecture des ouvrages'
      write(error_unit,'(a)') ' Erreurs dans la lecture des ouvrages'
      call echec_initialisation
      stop 146
   endif
   !---Fin des vérifications-------------------------------------------------------

   do ns = 1, nsmax  !cotes du fond des ouvrages
      is = all_OuvCmp(ns)%js
      all_OuvCmp(ns)%zfs = zfd(is)
   enddo

   !Vérification des hauteurs de pelle des ouvrages élémentaires déversants
   indic = 0
   do ns = 1, nsmax
      is = all_OuvCmp(ns)%js
      zf = all_OuvCmp(ns)%zfs
      zf0 = zfd(is-1)
      do k = 1, all_OuvCmp(ns)%ne
         n = all_OuvCmp(ns)%OuEl(k,1)  ;  iuvn = all_OuvEle(n)%iuv
         if (iuvn /= 3 .and. iuvn /= 9 .and. iuvn < 90) then
            if (all_OuvEle(n)%uv2 < max(zf,zf0)) then
               ib = numero_bief(is-1)  ;  pk = la_topo%sections(is-1)%pk
               write(l9,2002) pk,ib,all_OuvEle(n)%cvar  ;  write(error_unit,2002) pk,ib,all_OuvEle(n)%cvar
               write(l9,2003) zf0,zf,all_OuvEle(n)%uv2  ;  write(error_unit,2003) zf0,zf,all_OuvEle(n)%uv2
               indic = indic+1
            endif
            if (all_OuvEle(n)%uv2 - max(zf,zf0) < 0.0099_long) then
               ib = numero_bief(is-1)  ;  pk = la_topo%sections(is-1)%pk
               write(l9,2004) pk,ib,all_OuvEle(n)%cvar  ;  write(error_unit,2004) pk,ib,all_OuvEle(n)%cvar
               write(l9,2003) zf0,zf,all_OuvEle(n)%uv2  ;  write(error_unit,2003) zf0,zf,all_OuvEle(n)%uv2
               indic = indic+1
            endif
         end if
      enddo
   enddo

   if (indic > 0) then
      write(lTra,'(a)') ' >>>> Erreurs sur les hauteurs déversantes des seuils'
      write(error_unit,'(a)') ' >>>> Erreurs sur les hauteurs déversantes des seuils'
      call echec_initialisation
      stop 147
   endif
   !Calcul de l'indicateur global d'écriture BOOLI : si tous les BOOLJ sont .F.
   !alors BOOLI est .F. et il n'y a pas d'écriture des résultats des ouvrages
   some_ouvrages_writable = .false.
   ltra_size = 0
   do n = 1, nbele
      if (all_OuvEle(n)%is_writable) then
         ltra_size = ltra_size + 1
      endif
   enddo
   some_ouvrages_writable = (ltra_size > 0)

   ltra_size = ltra_size + 8  ! on ajoute la taille de l'entête
   allocate (ligne_tra(ltra_size))
   continue

   !classement des pompes par ordre croissant de cote de démarrage
   ! bloc de code supprimé : ce bloc de code ne sert à rien et il est dangeureux de devoir supposer un tel classement

   !Vérification que toutes les pompes d'une même section ont un débit nominal de même signe
   indic = 0
   do ns = 1, nsmax

      if (all_OuvEle(all_OuvCmp(ns)%OuEl(1,1))%iuv /= 3) cycle !si le 1er ouvrage élémentaire de l'ouvrage composite n° ns n'est pas une pompe, on passe
      nmax = all_OuvCmp(ns)%ne
      if (nmax == 1) cycle
      qnom = zero  ;  dbi = zero
      do k = 1, nmax
         nk = all_OuvCmp(ns)%OuEl(k,1)
         qnom = qnom+abs(all_OuvEle(nk)%uv1)
         dbi = dbi+all_OuvEle(nk)%uv1
      enddo
      if (.not. zegal(qnom,abs(dbi),un)) then
         js = all_OuvCmp(ns)%js
         call war009(lTra,js-1)  ;  call war009(0,js-1)
         indic = indic+1
      endif
   enddo
   if (indic > 0) then
      write(lTra,'(a)') ' Erreur dans les débits nominaux des pompes'
      write(error_unit,'(a)') ' Erreur dans les débits nominaux des pompes'
      call echec_initialisation
      stop 148
   endif

   call IniErrSing(la_topo%net%nss)

   1000 format(' >>>> la section au Pm ',f10.3,' du bief ',i3,' contient plus de ',i2.2,' ouvrages')
   2000 format(' Erreur :',/,' La section au Pm ',f10.3,' du bief ',i3,' contient un clapet et un ouvrage de type différent')
   2001 format(' Erreur :',/,' La section au Pm ',f10.3,' du bief ',i3,' contient une pompe et un ouvrage de type différent')
   2002 format(' >>>> Erreur ouvrage au Pm ',f10.3,' du bief ',i3,' : ',A/,' >>>> cote de déversement < cote du fond')
   2003 format(' cote fond amont : ',f8.3,' cote fond aval : ',f8.3,/,' cote déversement : ',f8.3)
   2004 format(' >>>> Erreur sur l''ouvrage situé au Pm ',f10.3,' du bief ',i3,' : ',A/,  &
               ' >>>> La pelle est inférieure à 1 cm : ',' Risque de problèmes de convergence <<<<')
!   2005 format(' cote fond amont : ',f11.6,' cote fond aval : ',f11.6,/,' cote de déversement : ',f11.6)
   3000 format(' >>>> Erreur de comptage des ouvrages élémentaires : ',i0,' (1er) ',i0,' (2e)')
   3001 format(' >>>> Erreur de comptage des ouvrages composites : ',i0,' (1er) ',i0,' (2e)')
end subroutine Lire_SIN



subroutine Ecrire_SIN(NBEle)  !écriture sur TRA des paramètres d'ouvrage
!==============================================================================
!                 impression sur .TRA des paramètres des ouvrages
!
!==============================================================================
   use parametres, only: long, lTra
   use iso_fortran_env, only: error_unit

   use Ouvrages, only: all_OuvCmp, all_OuvEle, nb_mobiles, allocate_buffer_mobiles
   use TopoGeometrie, only: la_topo, xgeo, numero_bief
   implicit none
   ! -- prototype --
   integer,intent(in) :: nbele
   ! -- variables locales temporaires --
   integer :: ib,ibsa,js,k,n,np,ns
   real(kind=long) :: zch,zd

   if (la_topo%net%nss==0) return

   !allocation du buffer d'enregistrement des positions des ouvrages mobiles
   ! NOTE: on le fait ici parce que on a maintenant lu SIN et VAR.
   ! NOTE: Si on ne le fait que dans Lire_VAR() on va manquer les pompes s'il n' a pas de fichier VAR
   if (nb_mobiles > 0) call allocate_buffer_mobiles

   write(lTra,1270)
   np = 0
   write(lTra,1280) nbele
   do ns = 1, la_topo%net%nss
      js = all_OuvCmp(ns)%js
      ib = numero_bief(js)
      n = all_OuvCmp(ns)%OuEl(1,1) !1er ouvrage élémentaire de l'ouvrage composite
      if (all_OuvEle(n)%iuv /= 99) write(lTra,1231) ns,xgeo(js-1),ib,all_OuvCmp(ns)%zfs
      do k = 1, all_OuvCmp(ns)%ne
         n = all_OuvCmp(ns)%OuEl(k,1) !k-ième ouvrage élémentaire de l'ouvrage composite
         if (all_OuvEle(n)%iuv==0) then
            write(lTra,'(2a)') ' DEVERSOIR-ORIFICE (seuil mobile) : ',all_OuvEle(n)%cvar
         else if (all_OuvEle(n)%iuv==1) then
            write(lTra,'(2a)') ' CLAPET : ',all_OuvEle(n)%cvar
         else if (all_OuvEle(n)%iuv==2) then
            write(lTra,'(2a)') ' VANNE DE FOND : ',all_OuvEle(n)%cvar
         else if (all_OuvEle(n)%iuv==3) then
            write(lTra,'(2a)') ' POMPE : ',all_OuvEle(n)%cvar
         else if (all_OuvEle(n)%iuv==4) then
            write(lTra,'(2a)') ' VANNE DE FOND (loi simplifiee) : ',all_OuvEle(n)%cvar
         else if (all_OuvEle(n)%iuv==5) then
            write(lTra,'(2a)') ' DEVERSOIR LATERAL : ',all_OuvEle(n)%cvar
         else if (all_OuvEle(n)%iuv==6) then
            write(lTra,'(2a)') ' DEVERSOIR-ORIFICE (ouverture variable) : ',all_OuvEle(n)%cvar
         else if (all_OuvEle(n)%iuv==7) then
            write(lTra,'(2a)') ' CLAPET INVERSE : ',all_OuvEle(n)%cvar
         else if (all_OuvEle(n)%iuv==8) then
            write(lTra,'(2a)') ' BUSE : ',all_OuvEle(n)%cvar
         else if (all_OuvEle(n)%iuv==9) then
            write(lTra,'(2a)') ' OUVRAGE DEFINI PAR L''UTILISATEUR : ',all_OuvEle(n)%cvar
         else if (all_OuvEle(n)%iuv==10) then
            write(lTra,'(2a)') ' DEVERSOIR TRAPEZOIDAL : ',all_OuvEle(n)%cvar
         else if (all_OuvEle(n)%iuv==11) then
            write(lTra,'(2a)') ' ORIFICE VOUTE : ',all_OuvEle(n)%cvar
         else if (all_OuvEle(n)%iuv==91) then
            write(lTra,'(2a)') ' PERTE DE CHARGE SINGULIERE A LA BORDA : ',all_OuvEle(n)%cvar
         else if (all_OuvEle(n)%iuv==99) then
            !write(lTra,'(2a)') ' OUVRAGE NON DEFINI : ',all_OuvEle(n)%cvar
         else
            write(lTra,'(2a)') '  >>>> TYPE DE SINGULARITE INCONNU : ',all_OuvEle(n)%cvar
            write(error_unit,'(2a)') '  >>>> TYPE DE SINGULARITE INCONNU : ',all_OuvEle(n)%cvar
            call echec_initialisation
            stop 149
         endif
         if (all_OuvEle(n)%iuv==3) then
            np = np+1
            ibsa = numero_bief(all_OuvEle(n)%irf)
            write(lTra,1210) all_OuvEle(n)%uv1,all_OuvEle(n)%uv4,all_OuvEle(n)%uv5,all_OuvEle(n)%za1, &
                             all_OuvEle(n)%za2,all_OuvEle(n)%za3,ibsa,xgeo(all_OuvEle(n)%irf)
         else if (all_OuvEle(n)%iuv==2 .or. all_OuvEle(n)%iuv==4 .or. all_OuvEle(n)%iuv==6) then
            zd = all_OuvEle(n)%uv2
            zch = zd+all_OuvEle(n)%uv3
            write(lTra,1210) all_OuvEle(n)%uv1,zd,zch,all_OuvEle(n)%uv4,zd+all_OuvEle(n)%uv5
         else if (all_OuvEle(n)%iuv<8) then
            zd = all_OuvEle(n)%uv2
            zch = zd+all_OuvEle(n)%uv3
            write(lTra,1210) all_OuvEle(n)%uv1,zd,zch,all_OuvEle(n)%uv4,all_OuvEle(n)%uv5
         else if (all_OuvEle(n)%iuv==8) then
            zd = all_OuvEle(n)%uv2
            write(lTra,1210) all_OuvEle(n)%uv1,zd,all_OuvEle(n)%uv3,all_OuvEle(n)%uv4,all_OuvEle(n)%uv5
         else if (all_OuvEle(n)%iuv==9) then
            write(lTra,1210) all_OuvEle(n)%uv1,all_OuvEle(n)%uv2,all_OuvEle(n)%uv3,all_OuvEle(n)%uv4,all_OuvEle(n)%uv5
         else if (all_OuvEle(n)%iuv==10) then
            zd = all_OuvEle(n)%uv2
            write(lTra,1210) all_OuvEle(n)%uv1,zd,all_OuvEle(n)%uv3,all_OuvEle(n)%uv4,all_OuvEle(n)%uv5
         else if (all_OuvEle(n)%iuv==11) then
            zd = all_OuvEle(n)%uv2
            write(lTra,1210) all_OuvEle(n)%uv1,zd,all_OuvEle(n)%uv3,all_OuvEle(n)%uv4,all_OuvEle(n)%uv5
         else if (all_OuvEle(n)%iuv==91) then
            write(lTra,1210) all_OuvEle(n)%uv1,all_OuvEle(n)%uv2,all_OuvEle(n)%uv3,all_OuvEle(n)%uv4
         endif
      enddo
   enddo

   1210 format(6f10.3,i3,1x,f10.3)
!   1210 format(6(g0,1x),i3,1x,f10.3)
   1231 format(/,1x,'section singulière ',i3,'  pm ',f9.2,' du bief ',i3,'  cote du fond: ',f7.2)
   1270 format(/,1x,27('-'),' PARAMETRES DES OUVRAGES ',27('-'),/,       &
                17X,'(mêmes ordre et définition que dans etude.SIN)',/, &
                    ' Les noms d''ouvrages commençant par # sont',      &
                    ' attribués par MAGE et font référence',/,           &
                    ' au numéro de ligne dans .SIN')
   1280 format(1x,'nombre total d''ouvrages élémentaires : ',i3)
end subroutine Ecrire_SIN



subroutine initialise_section_sing
!========================================================================================
!  décompte des sections singulières définies par des couples de sections de même pk
!
! N.B.: la valeur définitive de la_topo%net%nss ne sera connue qu'après lecture de SIN
!========================================================================================
   use parametres, only : long
   use TopoGeometrie, only: la_topo, xgeo
   implicit none
   ! variables locales
   integer :: ib, is, ns

   la_topo%sections(:)%iss = 0
   ns = 0
   do ib = 1, la_topo%net%nb
      do is = la_topo%biefs(ib)%is1, la_topo%biefs(ib)%is2-1
         if (abs(xgeo(is)-xgeo(is+1)) < 0.001_long) then  !is est singulière
            ns = ns+1
            la_topo%sections(is+1)%iss = -ns
         endif
      enddo
   enddo
   la_topo%net%nss = ns
end subroutine initialise_section_sing



subroutine Lire_VAR(VAR_File)  !Lecture de VAR
!==============================================================================
!                 Lecture du fichier .VAR
!==============================================================================
   use parametres,     only: long, un, lname, lTra, zero, l9
   use iso_fortran_env, only: error_unit

   use data_num_fixes, only: tinf, dtbase, date_format
   use IO_Files,       only: repFile
   use mage_utilitaires, only: next_int, next_real, next_string, lire_date, get_tm, c_mktime, c_tm
   use TopoGeometrie, only: la_topo, iSect
   use Ouvrages, only: all_OuvCmp, all_OuvEle, nbreg, regle, nb_mobiles, allocate_buffer_mobiles
   implicit none
   ! -- Prototype --
   character(len=lname),intent(in) :: VAR_file
   ! -- Variables locales temporaires --
   integer :: ibref, is, kn, l, luu, k
   integer :: ne, nemax, ns, ib
   character :: nomvar*10, bl*120, u2*120
   real(kind=long) :: ts,tt, xref, zz

   character(len=3), parameter :: separateur = ',;'
   integer :: next_field, io_status, ierr
   character :: wregle*3,err_message*180
   logical :: bool
   integer,pointer :: nsmax
   integer :: wday, offset
   character(len=20) :: sT
   type(c_tm) :: tm

   ! -- Initialisation
   nsmax => la_topo%net%nss
   bl = repeat(' ',120)
   do ns = 1, nsmax
      do k = 1, all_OuvCmp(ns)%ne
         ne  = all_OuvCmp(ns)%OuEl(k,1)
         if(allocated(all_OuvEle(ne)%wtz))deallocate(all_OuvEle(ne)%wtz)
         if(allocated(all_OuvEle(ne)%tz))deallocate(all_OuvEle(ne)%tz)
         all_OuvEle(ne)%itw = -1 ; all_OuvEle(ne)%jtw = -1 ; all_OuvEle(ne)%irf = -1 !valeur -1 indique une programmation en temps
      enddo
   enddo

   !---1ère passe pour compter le nombre de règles et allouer le tablea regle(:)
   open(newunit=luu,file=trim(VAR_file),status='old',form='formatted')
   nbreg = 0
   do
      read(luu,'(a)',iostat=io_status) u2
      if (io_status /= 0) exit     !erreur ou fin du fichier
      if (u2(1:1) == '$') nbreg = nbreg+1
   enddo
   allocate (regle(nbreg))
   nbreg = 0  ;  regle = ''
   rewind(luu)

   !---2ème passe pour compter le nombre lignes par règle de programmation
   l = 0   ! L = compteur de lignes du fichier [.VAR]
   do
      read(luu,'(a)',iostat=io_status) u2
      if (io_status /= 0) exit     !erreur ou fin du fichier
      l = l+1
      if (u2 == bl .or. u2(1:1) == '*') cycle            ! on saute les lignes blanches
      ts = zero  !on initialise le temps de synchronisation au cas où on aurait besoin de ts
      if (u2(1:1) == '$') then
         next_field = 2
         nomvar = next_string(u2,separateur,next_field) !ouvrage élémentaire régulé
         call localise_ouvrage(nomvar,ib,is,ns,ne)  !ne = numéro de l'ouvrage en cours de lecture
         if (ib == -1 .or. is == -1 .or. ns == -1 .or. ne == -1) then
            write(err_message,*) '>>>> VAR : Ouvrage ',NOMVAR,' non trouvé'
            write(error_unit,'(a)') trim(err_message)  ;  write(lTra,'(a)') trim(err_message)
            call echec_initialisation
            stop 161
         endif
         ! enregistrement pour l'écriture des positions d'ouvrage
         ! NOTE: il faut tester parce qu'un ouvrage élémentaire apparaît en général dans plusieurs règles de régulation
         if (all_OuvEle(ne)%mob <=0) then  ! c'est un nouvel ouvrage mobile à enregistrer
            nb_mobiles = nb_mobiles + 1
            all_OuvEle(ne)%mob = nb_mobiles
         endif
         if (all_OuvEle(ne)%itw > 0) then  ! cet ouvrage a déjà une règle de programmation
            write(err_message,*) ">>>> VAR : Plusieurs règles de programmation pour l'ouvrage ",NOMVAR
            write(error_unit,'(a)') trim(err_message)  ;  write(lTra,'(a)') trim(err_message)
            call echec_initialisation
            stop 162
         endif
!----->Type de programmation
         wregle = next_string(u2,separateur,next_field)
         if (wregle == 'WDZ') then                                  ! programmation en cote
            all_OuvEle(ne)%itw = 1  ;  all_OuvEle(ne)%jtw = 0  !initialisation des pointeurs pour délimiter les données de programmation
            ierr = 1
            ibref = next_int(u2,separateur,next_field) ; ierr = min(ierr,next_field)
            xref = next_real(u2,separateur,next_field) ; ierr = min(ierr,next_field)
            if (ierr == 0) call erreur_lecture(VAR_file,u2)
            if (ibref /= 0 .and. xref>0._long) then
               all_OuvEle(ne)%irf = iSect(ibref,xref)  !section de référence
               if (all_OuvEle(ne)%irf == 0) then
                  write(err_message,'(a,i3,a)') &
                  ' >>>> Fichier VAR (règle WDZ) : le bief ',ibref,' est inconnu sur ce réseau <<<<'
                  write(error_unit,'(a)') trim(err_message)  ;  write(lTra,'(a)') trim(err_message)
                  call echec_initialisation
                  stop 192
               else if (all_OuvEle(ne)%irf < 0) then
                  write(err_message,'(a,f9.2,a,i3,a)') &
                  ' >>>> Fichier VAR (règle WDZ) : il n''y a pas de section à l''abscisse ',xref,' dans le bief ',ibref,' <<<<'
                  write(error_unit,'(a)') trim(err_message)  ;  write(lTra,'(a)') trim(err_message)
                  call echec_initialisation
                  stop 193
               endif
            else
               !par défaut on relève la cote au droit de l'ouvrage
               all_OuvEle(ne)%irf = is   !is a été obtenu par l'appel de localise_ouvrage()
            endif
         elseif (wregle == 'WDY')then ! cycle année
            all_OuvEle(ne)%itw = 1  ;  all_OuvEle(ne)%jtw = 1  !initialisation des pointeurs pour délimiter
            all_OuvEle(ne)%w_cycle = 1
         elseif (wregle == 'WDM')then ! cycle mois
            all_OuvEle(ne)%itw = 1  ;  all_OuvEle(ne)%jtw = 1  !initialisation des pointeurs pour délimiter
            all_OuvEle(ne)%w_cycle = 2
         elseif (wregle == 'WDW')then ! cycle semaine
            all_OuvEle(ne)%itw = 1  ;  all_OuvEle(ne)%jtw = 1  !initialisation des pointeurs pour délimiter
            all_OuvEle(ne)%w_cycle = 3
         elseif (wregle == 'WDD')then ! cycle jour
            all_OuvEle(ne)%itw = 1  ;  all_OuvEle(ne)%jtw = 1  !initialisation des pointeurs pour délimiter
            all_OuvEle(ne)%w_cycle = 4
         elseif (wregle == 'WDT' .or. wregle == '   ' .or.   &      ! cas par défaut
                                             len_trim(u2)<9) then   ! programmation en temps
            all_OuvEle(ne)%itw = 1  ;  all_OuvEle(ne)%jtw = 0  !initialisation des pointeurs pour délimiter
         else                                 !les données de programmation
            NBreg = NBreg+1                                          ! règles de régulation
            write(l9,'(a,i3,2a)') 'Règle ',nbreg,' : ',u2(2:len_trim(u2))
            regle(NBreg) = u2  !la règle contient le nom de l'ouvrage
                               !all_OuvEle(ne)%itw et all_OuvEle(ne)%jtw pour cet ouvrage sont définis dans la boucle DO fin_init
            cycle              !aucune info supplémentaire nécessaire -> on lit la suite
         endif
         cycle
      else if (u2(1:1) /= '*') then
         !------couple /temps ou cote / valeur/
         all_OuvEle(ne)%jtw = all_OuvEle(ne)%jtw + 1
      endif
   enddo
   rewind (luu)
   !---lecture du fichier [.VAR] pour enregistrer les regles
   l = 0   ! L = compteur de lignes du fichier [.VAR]
   do
      read(luu,'(a)',iostat=io_status) u2
      if (io_status /= 0) exit     !erreur ou fin du fichier
      l = l+1
      if (u2 == bl .or. u2(1:1) == '*'.or. u2 == '') cycle            ! on saute les lignes blanches
      ts = zero  !on initialise le temps de synchronisation au cas où on aurait besoin de ts
      if (u2(1:1) == '$') then
         next_field = 2
         nomvar = next_string(u2,separateur,next_field) !ouvrage élémentaire régulé
         call localise_ouvrage(nomvar,ib,is,ns,ne)  !ne = numéro de l'ouvrage en cours de lecture
         if (ib == -1 .or. is == -1 .or. ns == -1 .or. ne == -1) then
            write(err_message,*) '>>>> VAR : Ouvrage ',NOMVAR,' non trouvé'
            write(error_unit,'(a)') trim(err_message)  ;  write(lTra,'(a)') trim(err_message)
            call echec_initialisation
            stop 161
         endif
!----->Type de programmation
         wregle = next_string(u2,separateur,next_field)
         if (wregle == 'WDZ' .or. wregle == 'WDT' .or. wregle == '   ' .or.   &
                                             len_trim(u2)<9) then
            allocate(all_OuvEle(ne)%wtz(all_OuvEle(ne)%jtw))
            allocate(all_OuvEle(ne)%tz(all_OuvEle(ne)%jtw))
            kn = 0
         elseif(wregle == 'WDY' .or. wregle == 'WDM' .or. wregle == 'WDW' .or. wregle == 'WDD')then
            ! on rajoute une entrée pour boucler la boucle
            all_OuvEle(ne)%jtw = all_OuvEle(ne)%jtw
            allocate(all_OuvEle(ne)%wtz(all_OuvEle(ne)%jtw))
            allocate(all_OuvEle(ne)%tz(all_OuvEle(ne)%jtw))
            kn = 1
         endif
         cycle
      else if (u2(1:1) /= '*') then
         !------couple /temps ou cote / valeur/
         kn = kn+1
         if (all_OuvEle(ne)%irf > 0) then  ! lecture d'un couple (cote,valeur)
            next_field = 1  ;  ierr = 1
            all_OuvEle(ne)%tz(kn) = next_real(u2,separateur,next_field) ; ierr = min(ierr,next_field)
            zz = next_real(u2,separateur,next_field) ; ierr = min(ierr,next_field)
            if (ierr == 0) call erreur_lecture(VAR_file,u2)
         elseif(all_OuvEle(ne)%w_cycle > 0) then ! lecture d'un couple (structure c_tm,valeur)
            next_field = 1  ;  ierr = 1
            tm = get_tm(tinf)
            wday = tm%tm_wday
!             print*,"t=",c_mktime(tm)
            sT = next_string(u2,separateur,next_field)
!             print*,"sT = ", sT
            select case (all_OuvEle(ne)%w_cycle)
            case(1)
              read(sT(1:2),'(i4)',iostat=io_status) tm%tm_mon
              tm%tm_mon = tm%tm_mon-1
              read(sT(4:5),'(i4)',iostat=io_status) tm%tm_mday
              read(sT(7:8),'(i4)',iostat=io_status) tm%tm_hour
              read(sT(10:11),'(i4)',iostat=io_status) tm%tm_min
              read(sT(13:14),'(i4)',iostat=io_status) tm%tm_sec
            case(2)
              read(sT(1:2),'(i4)',iostat=io_status) tm%tm_mday
              read(sT(4:5),'(i4)',iostat=io_status) tm%tm_hour
              read(sT(7:8),'(i4)',iostat=io_status) tm%tm_min
              read(sT(10:11),'(i4)',iostat=io_status) tm%tm_sec
            case(3)
              read(sT(1:1),'(i4)',iostat=io_status) tm%tm_wday
              read(sT(3:4),'(i4)',iostat=io_status) tm%tm_hour
              read(sT(6:7),'(i4)',iostat=io_status) tm%tm_min
              read(sT(9:10),'(i4)',iostat=io_status) tm%tm_sec
              offset = tm%tm_wday - wday
              tm%tm_mday = tm%tm_mday + offset
            case(4)
              read(sT(1:2),'(i4)',iostat=io_status) tm%tm_hour
              read(sT(4:5),'(i4)',iostat=io_status) tm%tm_min
              read(sT(7:8),'(i4)',iostat=io_status) tm%tm_sec
            case default
               call erreur_lecture(VAR_file,u2)
            end select
            all_OuvEle(ne)%tz(kn) = c_mktime(tm)
            if (io_status /= 0) ierr = 0
            zz = next_real(u2,separateur,next_field) ; ierr = min(ierr,next_field)
            if (ierr == 0) call erreur_lecture(VAR_file,u2)
            if (kn == all_OuvEle(ne)%jtw) then
               select case (all_OuvEle(ne)%w_cycle)
               case(1)
                 tm%tm_year =  tm%tm_year -1
               case(2)
                 tm%tm_mon = tm%tm_mon - 1
               case(3)
                 tm%tm_mday = tm%tm_mday - 7
               case(4)
                 tm%tm_mday = tm%tm_mday - 1
               case default
                 call erreur_lecture(VAR_file,u2)
               end select
               all_OuvEle(ne)%tz(1) =  c_mktime(tm)
            endif
         else                     ! lecture d'un couple (temps,valeur)
            next_field = 1
            sT = next_string(u2,separateur,next_field)
            all_OuvEle(ne)%tz(kn) = real(lire_date(sT, tinf, date_format),kind=long)
            zz = next_real(u2,separateur,next_field) ; ierr = min(ierr,next_field)
            if (ierr == 0) call erreur_lecture(VAR_file,u2)
            all_OuvEle(ne)%tz(kn) = all_OuvEle(ne)%tz(kn)+ts  !décalage temporel
         endif
         if (all_OuvEle(ne)%iuv == 0 .or. all_OuvEle(ne)%iuv == 5) then                                     !déversoir frontal ou latéral
            continue                      !cote de déversement
         else if (all_OuvEle(ne)%iuv == 2 .or. all_OuvEle(ne)%iuv == 4 .or. all_OuvEle(ne)%iuv == 6) then   !vannes ou orifice
            zz = zz-all_OuvEle(ne)%uv2    !ouverture
         endif
         all_OuvEle(ne)%wtz(kn) = zz
         if (all_OuvEle(ne)%w_cycle > 0) all_OuvEle(ne)%wtz(1) = zz
      endif
   enddo
   close (luu)

   !on continue ici quand le fichier a été lu en entier (ou si erreur)
   !---ré-initialisation à la valeur de .SIN pour les ouvrages non présents dans .VAR
   !   ou les ouvrages présents dans VAR qui n'ont pas une loi W(z) ou W(t)
   fin_init: do ns = 1, nsmax
      nemax = all_OuvCmp(ns)%ne
      do k = 1, nemax
         ne = all_OuvCmp(ns)%OuEl(k,1)
         if (all_OuvEle(ne)%itw > -1) cycle  !on a une loi pour cet ouvrage
         if (all_OuvEle(ne)%iuv == 0 .or. all_OuvEle(ne)%iuv == 9 .or. all_OuvEle(ne)%iuv == 5) then
            !------cas des seuils mobiles (--->pelles)
            all_OuvEle(ne)%itw = 1
            all_OuvEle(ne)%jtw = 2
            allocate(all_OuvEle(ne)%wtz(2), all_OuvEle(ne)%tz(2))
            all_OuvEle(ne)%wtz(1:2) = all_OuvEle(ne)%uv2
            all_OuvEle(ne)%tz(1) = tinf
            all_OuvEle(ne)%tz(2) = tinf+dtbase
         else if (all_OuvEle(ne)%iuv == 2 .or. all_OuvEle(ne)%iuv == 4 .or. all_OuvEle(ne)%iuv == 6) then
            !------cas des vannes et orifices (--->ouvertures)
            all_OuvEle(ne)%itw = 1
            all_OuvEle(ne)%jtw = 2
            allocate(all_OuvEle(ne)%wtz(2), all_OuvEle(ne)%tz(2))
            all_OuvEle(ne)%wtz(1:2) = all_OuvEle(ne)%uv3
            all_OuvEle(ne)%tz(1) = tinf
            all_OuvEle(ne)%tz(2) = tinf+dtbase
         endif
      enddo
   enddo fin_init

end subroutine Lire_VAR


subroutine Localise_Ouvrage(myName,ib,ks,ns,ne)
!==============================================================================
!   Cette routine renvoie les indices d'un ouvrage élémentaire à partir
!   de son nom
!   les indices sont mis à -1 si l'ouvrage n'est pas trouvé.
!
!---Entrée : myName = nom de l'ouvrage
!---Sortie : ib = numéro de bief (rang de calcul)
!            ks = numéro de section AMONT
!            ns = numéro de section singulière
!            ne = numéro d'ouvrage élémentaire.
!
!==============================================================================
   use TopoGeometrie, only: la_topo
   use Ouvrages, only: all_OuvCmp, all_OuvEle
   implicit none
   ! -- Prototype --
   character(len=10), intent(in) :: myName
   integer, intent(out) :: ib, ks, ns, ne
   ! -- Variables --
   integer :: k

   do ns = 1, la_topo%net%nss              ! numéro de section singulière
      do k = 1, all_OuvCmp(ns)%ne
         ne = all_OuvCmp(ns)%OuEl(k,1)       ! numéro d'ouvrage élémentaire
         if (all_OuvEle(ne)%cvar == myName) then
            ks = all_OuvCmp(ns)%js-1         ! numéro de section amont
            do ib = 1, la_topo%net%nb     ! recherche du rang de calcul du bief
               if (la_topo%biefs(ib)%is1<=ks .and. ks<=la_topo%biefs(ib)%is2) return
            enddo
         endif
      enddo
   enddo
   !---ouvrage non trouvé
   ib = -1 ; ks = -1 ; ns = -1 ; ne = -1
end subroutine Localise_Ouvrage
