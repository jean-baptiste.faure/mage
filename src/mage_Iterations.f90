!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - INRAE - 1989-2020                           #
!# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################

subroutine ExeMage (err_code)
!==============================================================================
!                        Réalisation des calculs
!==============================================================================
   use parametres, only: long, i_ism, i_debord, i_mixte, i_mixte2, lTra, l9
   use, intrinsic :: iso_fortran_env, only: output_unit, error_unit

   use data_num_fixes, only: plus, silent, tmax, steady, fp_model, tinf, dt_char, dttra
   use data_num_mobiles, only: ttra,tbin, t, t_char
   use mage_utilitaires, only: f0p, heure
   use mage_results, only: calcul_ok
   use TopoGeometrie, only: la_topo, with_charriage
   use erreurs_stv, only: t_errmax, reste_sup, t_reste_sup
   use charriage, only: export_bottom_data, export_Qsolid_data
   use IO_Files, only: btmFile, qstFile
   implicit none
   ! -- Prototype --
   integer, intent(out) :: err_code
   ! -- Variables --
   integer values(8) ! valeurs renvoyées par la fonction date_and_time()
   integer indic     ! indicateur utilisé par apport()
   integer :: m1ok
   real(kind=long) :: debut, tmax0
   character(len= 5) :: zone
   character(len= 8) :: ddate
   character(len=10) :: time
   character(len=19) :: jhms, jhms0
   character(len=30) :: stars
   character(len=80) :: blanc
   character(len=80) :: interruption = ' (simulation interrompue par l''utilisateur)'
   character(len=120) :: post_message=''
   character(len=240) :: message
   character(len=19) :: hms
   character(len=17) :: header
   integer :: ka, kb, ith, lw

   calcul_OK = .false.
   blanc = repeat(' ',80)
   stars = repeat('*',30)

   write(l9,'(a)')' ----> Début des calculs hydrauliques'

   !---début du comptage du temps
   call date_and_time(ddate,time,zone,values)
   call cpu_time(debut)
   !---initialisation des temps de la simulation
   t = tinf ; tbin = tinf ; ttra = tinf ; t_errmax = tinf ! deja fait dans init_otherdata
   hms = heure(t)
   write(lTra,'(16x,2a)') 'ligne d''eau initiale à ',hms
   !---initialisation des apports/fuites latéraux (donnés par .lat)
   indic=1
   call apport(indic)

   if (with_charriage > 0) then
      if (tmax > 864000._long) then
         if (dttra > 86399._long) then  !affichage en jours entiers
            kb = scan(hms,':') - 1  !on ne garde que le 1er champ, celui des jours
            ka = scan(hms,'+-0123456789') !on ne veut pas les blancs du début
            header = hms(ka:kb)//'_j'
         else                          !affichage en jours décimaux
            write(header,'(a,a2)') trim(f0p(t/86400._long,2)),'_j'
         endif
      else                             !affichage en heures
         ith = floor(t / 3600._long)
         write(header,'(i0,a2)') ith,'_h'
      endif
      if (btmFile /= '') call export_bottom_data(header)
      if (qstFile /= '') call export_Qsolid_data(header)
      t_char = tinf + dt_char
   endif

   tmax0 = tmax
   !---boucle sur le temps
   select case (fp_model)
      case (i_ism)
         message = '----> Modélisation des débordements avec ISM'//trim(post_message)
         write(l9,*) trim(message)  ;  if(.not.silent) write(output_unit,*) trim(message)
         call time_iterations_ISM(m1ok)
      case (i_mixte)
         message = '----> Modélisation des débordements mixte avec Debord+ISM'//trim(post_message)
         write(l9,*) trim(message)  ;  if(.not.silent) write(output_unit,*) trim(message)
         call time_iterations_mixte(m1ok)
      case (i_mixte2)
         message = '----> Modélisation des débordements mixte avec Debord+ISM'//trim(post_message)
         write(l9,*) trim(message)  ;  if(.not.silent) write(output_unit,*) trim(message)
         fp_model = i_mixte
         call time_iterations_mixte_2(m1ok)
      case (i_debord)
         message = '----> Modélisation des débordements avec Debord'
         write(l9,*) trim(message)  ;  if(.not.silent) write(output_unit,*) trim(message)
         call time_iterations(m1ok)
!       case (i_static)
!          message = '----> champs figés, relecture fichier BIN'
!          write(l9,*) trim(message)  ;  if(.not.silent) write(output_unit,*) trim(message)
!          stop 3 ! TODO
      case default
         message = '----> Modélisation des débordements : Modèle inconnu'
         write(l9,*) trim(message)  ;  if(.not.silent) write(output_unit,*) trim(message)
         stop 3
   end select
   !---vérification des lois d'ouvrages (uniquement sur les dates affichées dans TRA)
   if (la_topo%net%nss /= 0) call recap_Ouvrages

   !---alerte si résidu maximal est grand
   if (reste_sup > 0.01_long) then
      write(message,'(a,e12.6,2a)') ' >>>> ATTENTION : résidu trop grand !!! Maximum (',reste_sup, &
                                    ') atteint à t = ',heure(t_reste_sup)
      write(post_message,'(17x,a)')     ' La solution calculée est probablement fausse autour de cette date.'
      write(l9,'(a,/,a)') trim(message),trim(post_message)
      write(lTra,'(a,/,a)') trim(message),trim(post_message)
      write(output_unit,'(a,/,a)') trim(message),trim(post_message)
   endif

   !---durée de la simulation et nombre total d'itérations et de pas de temps
   call info_performance(debut,values)

   !---fin normale de mage
   if (tmax >= tmax0 .or. m1ok==0) then
      calcul_ok = .true.
      write(lTra,'(1x,a)') stars(1:30)//' FIN NORMALE DE MAGE '//stars(1:28)
      write(l9,'(a)') plus//'---> FIN NORMALE DE MAGE'//blanc(26:79)  !bizarre mais on en a besoin pour PamHyr
   else
      write(lTra,'(1x,a)') stars(1:29)//' FIN INATTENDUE DE MAGE '//stars(1:27)
      write(l9,'(a)') plus//'---> FIN INATTENDUE DE MAGE'//blanc(26:79)  !bizarre mais on en a besoin pour PamHyr
   endif
   !---Re-écriture de .TRA (ajout de 0 devant .nnn sur PC)
   call MZero
   ! Message final
   jhms = heure(tmax)
   if (steady) then
      jhms0='état permanent'
   else
      jhms0 = heure(tmax0)
   endif
   lw = output_unit
   ! NOTE: pour fake mage7 il faut envoyer les messages suivants sur error_unit et non output_unit sinon PamHyr ne les voit pas
#ifdef fake_mage7
   lw = error_unit
#endif /* fake_mage7 */
   if (tmax >= tmax0 .OR. m1ok==0) then
      if (.not.silent) then
         write(lw,*) 'Fin de la simulation à ',jhms
         if (tmax0 > tmax) then
            write(lw,*) 'Date finale attendue : ',jhms0,trim(interruption)
         else
            write(lw,*) 'Date finale attendue : ',jhms0
         endif
         write(lw,*) 'Apparemment tout va bien, vérifiez quand même en consultant les listings de MAGE'
         write(lw,*) 'En cas de problème consulter l''analyse de la géométrie dans TRA'
         write(lw,'(a)',advance='yes')' >>>> Fin normale de MAGE <<<<'
      endif
      err_code = 0
   else
      if (.not.silent) then
         write(lw,*) 'Fin de la simulation à ',jhms
         write(lw,*) 'Date finale attendue : ',jhms0
         write(lw,*) 'MAGE s''est arrêté avant la date souhaitée !'
         write(lw,*) 'Consulter les listings de MAGE pour déterminer la source du problème'
         write(lw,*) 'En particulier l''analyse de la géométrie dans TRA'
         write(lw,'(a)',advance='yes') ' >>>> Fin inattendue de MAGE <<<<'
      endif
      err_code = 1
   endif
end subroutine ExeMage



subroutine time_iterations(m1ok)
!==============================================================================
!                       boucle sur le temps
!==============================================================================
   use parametres, only: long, zero, un, Signal_interruption, &
                         nomini_ini, l9, lTra
   use, intrinsic :: iso_fortran_env, only: error_unit

   use booleens, only: uselat
   use casiers, only: dtlat
   use data_num_fixes, only: ini_internal, tinf, tmax, dtbase, dtmin, dt_char
   use data_num_mobiles, only: t, dt ,dtold, niter, npas, ttra, tbin, dtmin0, tmelissa, t_char
   use erreurs_sing, only: dqsup, tsup, volsng
   use erreurs_stv, only: errvol,errmax,errsup,ermax0,kbmax,kbsup, reste
   use IO_Files, only: envFile
   use Regime_Permanent, only: IniPer
   use StVenant_Debord, only: Debord_discretisation
   use hydraulique, only: zt
   use Ouvrages, only: update_positions_ouvrages, add_to_buffer_mobiles, nb_mobiles, nbreg
   use mage_utilitaires, only: zegal
   use mage_regulation, only: RegLoc
   use TopoGeometrie, only: la_topo, with_charriage
   use charriage, only: update_charriage
#ifdef with_timers
   use mage_timers, only: regloc_t, calcul_dt_t, cl_amont_t, apport_t, &
                          update_positions_ouvrages_t, Debord_discretisation_t, &
                          init_CLaval_Debord_t, init_solution_Debord_t, &
                          nonLinear_iterations_t, timestep_last_actions_t
#endif /* with_timers */
   implicit none
   ! -- prototype
   integer, intent(out) :: m1ok       ! code d'erreur en sortie de M1, 0 si calcul complet
   ! -- variables locales temporaires --
   logical :: besoin_retour  ! vrai s'il faut un retour en arrière
   logical :: convergence_ok ! vrai si convergence au pas de temp précédent
   logical :: utile ! vrai si régulation utilisée ; initialise à vrai et modifié par OBSERV
   logical :: mod92  ! vrai si relecture d'un fichier VAR
   logical :: bool2, bool3
   integer :: kper       ! nb de pas de temps successifs à 1 seule itération
   integer :: niter0     ! nb d'itérations du pas de temps précédent
   integer :: iso        ! iso = 0 si sortie normale en fin de simulation
   integer :: indic = 0  ! flag pour la gestion des retours en arrière
   real(kind=long) :: dtr, dtu, dtloc, tyet
   real(kind=long), dimension(:), allocatable :: q, z, y, v
   real (kind=long) :: told
   logical :: anomalie_dt_precedent, dt_trop_grand
#ifdef with_timers
   real(kind=long) :: start_t, end_t, start_t2, end_t2
#endif /* with_timers */
   interface
      subroutine copier(tyet,q,z,y,v)
         use parametres, only: long
         real(kind=long),intent(out) :: tyet
         real(kind=long),dimension(:),intent(out) :: q,z    ! ligne d'eau
         real(kind=long),dimension(:),intent(out),optional :: y,v    ! ligne d'eau
      end subroutine copier
   end interface


   write(l9,'(a)') ' '
   m1ok = 0 ; kper = 0 ; reste = zero ; npas = 0 ; niter = 0 ; t = tinf
   call Relire_NUM   ! relecture du .NUM : ré-initialisation
   dtlat = dtbase ; mod92 = .false.
   call update_positions_ouvrages(mod92,t,zt)  ! initialisation de la position des ouvrages mobiles
   besoin_retour = .false. ; convergence_ok = .true. ; utile = .true. ; dtr = dtbase
   !---essai de procédure d'initialisation
   if (ini_internal) then
      call iniper   ! initialisation par le permanent interne
      ini_internal = .false.    ! on force la lecture du fichier au lieu de récupérer
                    ! les valeurs internes pour être sûr d'avoir le même
                    ! résultat si on reprend depuis ce fichier
      call Lire_INI(nomini_ini)
      write(l9,*) 'Relecture de l''état initial dans ',trim(nomini_ini),' : OK'
   endif

   !---Initialisation du module Erreurs_Sing
   dqsup = zero ; volsng = zero ; tsup = tinf
   errvol = zero ; errmax = zero ; errsup = zero ; ermax0 = zero
   kbsup = 1 ;  kbmax = 1
   allocate(q(la_topo%net%ns), z(la_topo%net%ns), &
          & y(la_topo%net%ns), v(la_topo%net%ns))

   !------------------------------------------------------------------------------
   !---début de la boucle sur le temps
   !------------------------------------------------------------------------------
   ! la linéarisation à t = t_ini permet de calculer le nombre de Courant et donc de
   ! calculer un 1er pas de temps correct
   call Debord_discretisation(bool2)

   boucle_temps: DO  ! ---> Début de la boucle sur le temps

      if (.not. besoin_retour .and. convergence_ok) call write_TRA_Ouvrages  !Écriture des résultats aux ouvrages.
                                                                    !À faire avant toute modification des
                                                                    !paramètres d'ouvrage par relecture de .VAR
      if (convergence_ok .and. t<tmax) then   !Si convergence au pas de temps précédent
                                              !et si on n'est pas à TMAX
         call relire_NUM                                      !Relecture de NUM
         if (ttra > tmax .and. dtbase > zero) ttra=tmax  !si t=tmax on imprime sur listing au dernier pas de temps
         if (tmelissa > tmax .and. dtbase > zero) tmelissa=tmax  !si t=tmax on envoie la solution au dernier pas de temps
         if (tbin > tmax .and. dtbase > zero) tbin=tmax  !si t=tmax on écrit la solution au dernier pas de temps
         call relire_SIN_VAR(mod92)                      !Relecture de VAR (mod92 mis à .true. dans ce cas)
         if (nb_mobiles > 0 .and. zegal(t,tbin,un)) call add_to_buffer_mobiles(t) !il peut y avoir des pompes
      endif

      if (envFile /= '') call Envlop(envFile)  !calcul des valeurs enveloppe

! QUESTION: peut-on simplifier la liste des variables utilisées dans le test suivant ?
      if (convergence_ok .and. nbreg > 0) then !Régulation locale si convergence des itérations
                                           !au pas de temps précédent
         call copier(tyet,q,z,y,v)  !on envoie des copies des variables
#ifdef with_timers
         call cpu_time(start_t)
#endif /* with_timers */
         call RegLoc(tyet,q,z,y,v,dtloc)
#ifdef with_timers
         call cpu_time(end_t)
         RegLoc_t = RegLoc_t + (end_t - start_t)
#endif /* with_timers */
!          mod92 = mod92 .or. mod920  !VAR a pu aussi être modifié à la main par l'utilisateur (MOD92)
         dtu   = dtloc
      else
         dtu = dtbase
      endif

      if (t>=tmax) exit boucle_temps  !fin de la boucle sur le temps si le TMAX est
                                      !atteint (doit se faire aprés l'observateur)

      !--Calcul du pas de temps
      dtr = min(dtr,dtu)    !DTR est fourni par timestep_last_actions à la fin du pas de temps précédent
#ifdef with_timers
      call cpu_time(start_t)
#endif /* with_timers */
      call calcul_dt(convergence_ok,dtr)   !CALCUL_DT renvoi DT < DTR et réduit / DT précédent
                                     !si convergence_ok est .FALSE. (divergence des
                                     !itérations au pas de temps précédent)
#ifdef with_timers
      call cpu_time(end_t)
      calcul_dt_t = calcul_dt_t + (end_t - start_t)
#endif /* with_timers */
      if (dt < dtmin0) then
         iso = 1 ; call sortie_echec(mod92)
         m1ok = 200          !err020 : pas de temps < dtmin
         exit boucle_temps   !sortie prématurée en sautant l'appel à calcul_dt
      else
         told = t
         iso = 0 ; t = t+dt ; npas = npas+1   !incrémentation du temps et du nb total de pas de temps
      endif

      !--DÉBUT Actualisation des données variables avec le temps
#ifdef with_timers
      call cpu_time(start_t)
#endif /* with_timers */
      call cl_Amont        !Conditions aux limites amont en débit
#ifdef with_timers
      call cpu_time(end_t)
      cl_amont_t = cl_amont_t + (end_t - start_t)
      call cpu_time(start_t)
#endif /* with_timers */
      call apport(indic)   !Variation des apports/fuites (Q latéral) : QE et DQE
#ifdef with_timers
      call cpu_time(end_t)
      apport_t = apport_t + (end_t - start_t)
#endif /* with_timers */

      !------Position des vannes : manoeuvres, automatisme
      !      BOOL3 est mis à .TRUE. si : nomEtude_var (unité 92, ouvert par Lire_VAR()) a été modifié
      !                                  par l'observateur (MOD921), la régulation (MOD920) ou la lecture de VAR (MOD92)
      !                          ou si : il faut faire un retour en arrière (besoin_retour)
      !                          ou si : il y a divergence (convergence_ok)
      bool3 = mod92 .or. besoin_retour .or. (.not. convergence_ok)
#ifdef with_timers
      call cpu_time(start_t)
#endif /* with_timers */
      call update_positions_ouvrages(bool3,t,zt)
#ifdef with_timers
      call cpu_time(end_t)
      update_positions_ouvrages_t = update_positions_ouvrages_t + (end_t - start_t)
#endif /* with_timers */
      !--FIN Actualisation des données variables avec le temps

      !---Discrétisation de Saint-Venant (ss-pg BIEF)
      reste = zero
      if (uselat) then
         call lateral_exchange(dt_trop_grand)
      else
         dt_trop_grand = .false.
      endif
#ifdef with_timers
      call cpu_time(start_t)
#endif /* with_timers */
      call Debord_discretisation(anomalie_dt_precedent)
#ifdef with_timers
      call cpu_time(end_t)
      Debord_discretisation_t = Debord_discretisation_t + (end_t - start_t)
#endif /* with_timers */
      !anomalie_dt_precedent est vrai si Debord_discretisation et discretise_bief
      !ont détecté une anomalie sur le résultat du pas de temps précédent
      !(par ex y < 0 ou Froude > FRmax)

      if (anomalie_dt_precedent .or. dt_trop_grand) then
         call retour_arriere(convergence_ok,indic) !anomalie (divergence)
         cycle                             !-> il faut revenir en arrière et réduire dt
      else
         call save_QD                     !sauvegarde des débits perdus par déversement latéral
      endif

      !---Impression des résultats du pas de temps précédent sur .BIN et .TRA
      !   sauf si on l'a interrompu (manoeuvre de pompe ou non-convergence)
      niter0 = niter
      if ((.not. besoin_retour) .and. convergence_ok) call Ecrire

      !---Pilotages des itérations : lois d'ouvrage et Saint-Venant
         !initialisations
#ifdef with_timers
      call cpu_time(start_t)
#endif /* with_timers */
      call init_CLaval_Debord
#ifdef with_timers
      call cpu_time(end_t)
      init_CLaval_Debord_t = init_CLaval_Debord_t + (end_t - start_t)
      call cpu_time(start_t)
#endif /* with_timers */
      call init_solution_Debord
#ifdef with_timers
      call cpu_time(end_t)
      init_solution_Debord_t = init_solution_Debord_t + (end_t - start_t)
      call cpu_time(start_t)
#endif /* with_timers */
         !itérations sur les non-linéarités (Newton-Raphson ou point fixe)
      call nonLinear_iterations(convergence_ok)
#ifdef with_timers
      call cpu_time(end_t)
      nonLinear_iterations_t = nonLinear_iterations_t + (end_t - start_t)
      call cpu_time(start_t)
#endif /* with_timers */

      !---convergence des itérations : mise à jour des variables Q, Z et Y ;
      !                                vérification de la conservation des volumes
      call timestep_last_actions(convergence_ok,besoin_retour,kper,niter0,indic,dtr)
#ifdef with_timers
      call cpu_time(end_t)
      timestep_last_actions_t = timestep_last_actions_t + (end_t - start_t)
#endif /* with_timers */

      if (with_charriage > 0 .and. t > t_char-dtmin) then
         call update_charriage(t, dt_char)
         t_char = t_char + dt_char
      endif

   enddo  boucle_temps  ! fin de la boucle sur le temps

   !------------------------------------------------------------------------------
   !---Fin de la boucle sur le temps
   !------------------------------------------------------------------------------
   ! on annule l'interception du signal SIGINT envoyé par ctrl+C
#ifdef linux
   call signal(Signal_interruption,0)
#endif

   !---impression du dernier pas de temps
   if (iso<=0) call calcul_dt(besoin_retour,dtr)
   t = tmax+dt
   !---calcul des yt, vt
   if (iso<=0) call Debord_discretisation(bool2)
   call devers(dtr,bool2)
   !---impression
   call write_TRA_Ouvrages
   call Ecrire
   if (nb_mobiles > 0) call add_to_buffer_mobiles(tmax)

   t = tmax
   dt = dtold
   if (iso > 0) then
      !---affichage erreur sur la conservation des volumes
      write(lTra,'(a,e8.2,a,i3.3,a,/,a,e8.2,a,i3.3,a)') &
                 ' Erreur (relative par bief) en volume à l''instant final :  ',errsup, &
                 ' (Bief ',kbsup,')',' Erreur maximale sur la simulation :  ',errmax,   &
                 ' (Bief ',kbmax,')'
      call err020
   else
      !---vérification de la conservation des volumes
      if (kper >= 10) then
         write(lTra,'(/,20x,a,/)') '>>>> Le régime permanent est atteint <<<<'
         write(error_unit,'(/,20x,a,/)') '>>>> Le régime permanent est atteint <<<<'
      endif
      call bilan_volume(un,convergence_ok)
   endif
   deallocate(q, z, y, v)
end subroutine time_iterations



subroutine time_iterations_ISM(m1ok)
!==============================================================================
!  boucle sur le temps dans le cas où on utilise ISM à la place de Debord
!==============================================================================
   use parametres, only: long, zero, un, Signal_interruption, nomini_ini, &
                         l9, lTra
   use, intrinsic :: iso_fortran_env, only: output_unit, error_unit

   use booleens, only: uselat
   use casiers, only: dtlat
   use data_num_fixes, only: ini_internal, tinf, tmax, dtbase, dtmin, dt_char
   use data_num_mobiles, only: t, dt ,dtold, niter, npas, ttra, tbin, dtmin0, tmelissa, t_char
   use erreurs_sing, only: dqsup, tsup, volsng
   use erreurs_stv, only: errvol,errmax,errsup,ermax0,kbmax,kbsup, reste
   use IO_Files, only: envFile
   use Regime_Permanent, only: IniPer
   use StVenant_ISM, only: dq_ism => dq, dz_ism => dz, with_trace_full, q_total, ISM_discretisation, &
                           retour_arriere_ISM, init_solution_ISM
   use solution, only: dq, dz
   use hydraulique, only: zt
   use Ouvrages, only: update_positions_ouvrages, add_to_buffer_mobiles, nb_mobiles
   use TopoGeometrie, only: la_topo, with_charriage
   use charriage, only: update_charriage
   use mage_utilitaires, only: zegal
   implicit none
   ! -- prototype
   integer, intent(out) :: m1ok       ! code d'erreur en sortie de M1, 0 si calcul complet
   ! -- variables locales temporaires --
   logical :: besoin_retour  ! vrai s'il faut un retour en arrière
   logical :: convergence_ok ! vrai si convergence au pas de temp précédent
   logical :: utile ! vrai si régulation utilisée ; initialise à vrai et modifié par OBSERV
   logical :: mod92  ! vrai si relecture d'un fichier VAR
   integer :: kper       ! nb de pas de temps successifs à 1 seule itération
   integer :: niter0     ! nb d'itérations du pas de temps précédent
   integer :: iso        ! iso = 0 si sortie normale en fin de simulation
   integer :: indic = 0  ! flag pour la gestion des retours en arrière
   real(kind=long) :: dtr
   !real(kind=long), dimension(issup) :: q, z, y, v
   real (kind=long) :: told
   logical :: anomalie_dt_precedent, dt_trop_grand
   integer :: is
   character(len=80) :: message


   write(l9,'(a)') ' '
   m1ok = 0 ; kper = 0 ; reste = zero ; npas = 0 ; niter = 0 ; t = tinf
   call Relire_NUM   ! relecture du .NUM : ré-initialisation
   dtlat = dtbase ; mod92 = .false.
   call update_positions_ouvrages(mod92,t,zt)  ! initialisation de la position des ouvrages mobiles
   besoin_retour = .false. ; convergence_ok = .true. ; utile = .true. ; dtr = dtbase

   !---essai de procédure d'initialisation
   if (ini_internal) then
      call iniper   ! initialisation par le permanent interne, type Debord
      ini_internal = .false.    ! on force la lecture du fichier au lieu de récupérer
                                ! les valeurs internes pour être sûr d'avoir le même
                                ! résultat si on reprend depuis ce fichier
      !on pourrait appeler directement Lire_INI() puisque iniper() calcule une ligne d'eau simple avec Debord
      !Lire_INI_ISM() bascule automatiquement sur Lire_INI() car il reconnait que ce Mage_ini.ini n'est pas fait pour ISM
      !cependant si un jour on fait une version de iniper() adaptée à ISM alors il faudra passer par Lire_INI_ISM(), autant
      !le faire tout de suite pour ne pas oublier
      write(message,'(2a)') 'Relecture de l''état initial dans ',trim(nomini_ini)
      write(l9,*) trim(message)  ;  write(output_unit,*) trim(message)
      call Lire_INI_ISM(nomini_ini)
      !on remet ini_internal à vrai parce qu'il ne faut pas mentir
      ini_internal = .true.
   endif

   !---Initialisations diverses
   dqsup = zero ; volsng = zero ; tsup = tinf
   errvol = zero ; errmax = zero ; errsup = zero ; ermax0 = zero
   kbsup = 1 ;  kbmax = 1

   !------------------------------------------------------------------------------
   !---début de la boucle sur le temps
   !------------------------------------------------------------------------------
   !L'appel de ISM_discretisation() avant la boucle sur le temps permet d'initialiser
   !le nombre de Courant et donc de calculer un 1er pas de temps correct
   call ISM_discretisation(anomalie_dt_precedent)

   write(l9,*) 'Début de la boucle sur le temps'
   write(output_unit,*) 'Début de la boucle sur le temps'
   boucle_temps: DO  ! ---> Début de la boucle sur le temps

      if (.not. besoin_retour .and. convergence_ok) call write_TRA_Ouvrages  !Écriture des résultats aux ouvrages.
                                                                    !À faire avant toute modification des
                                                                    !paramètres d'ouvrage par relecture de .VAR
      if (convergence_ok .and. t<tmax) then   !Si convergence au pas de temps précédent
                                              !et si on n'est pas à TMAX
         call relire_NUM                                      !Relecture de NUM
         if (ttra > tmax .and. dtbase > zero) ttra=tmax  !si t=tmax on imprime sur listing au dernier pas de temps
         if (tmelissa > tmax .and. dtbase > zero) tmelissa=tmax  !si t=tmax on envoie la solution au dernier pas de temps
         if (tbin > tmax .and. dtbase > zero) tbin=tmax  !si t=tmax on écrit la solution au dernier pas de temps
         call relire_SIN_VAR(mod92)                              !Relecture de VAR (mod92 mis à .true. dans ce cas)
         if (nb_mobiles > 0 .and. zegal(t,tbin,un)) call add_to_buffer_mobiles(t) !il peut y avoir des pompes
         with_trace_full = .false.
      else
#ifdef debug
         with_trace_full = .true.
#endif /* debug */
      endif

      if (envFile /= '') call Envlop(envFile)  !calcul des valeurs enveloppe
      ! TODO: remettre la régulation locale des ouvrages pour ISM

      if (t>=tmax) exit boucle_temps  !fin de la boucle sur le temps si le TMAX est
                                      !atteint (doit se faire aprés l'observateur)

      !--Calcul du pas de temps
      dtr = dtbase
      call calcul_dt(convergence_ok,dtr)   !CALCUL_DT renvoi DT < DTR et réduit / DT précédent
                                     !si convergence_ok est .FALSE. (divergence des
                                     !itérations au pas de temps précédent)
      if (dt < dtmin0) then
         iso = 1 ; call sortie_echec(mod92)
         m1ok = 200          !err020 : pas de temps < dtmin
         exit boucle_temps   !sortie prématurée en sautant l'appel à calcul_dt
      else
         told = t
         iso = 0 ; t = t+dt ; npas = npas+1   !incrémentation du temps et du nb total de pas de temps
      endif

      !--DÉBUT Actualisation des données variables avec le temps
      call cl_Amont        !Conditions aux limites amont en débit
      call apport(indic)   !Variation des apports/fuites (Q latéral) : QE et DQE
      ! TODO: remettre le calcul de la position des vannes quand on saura comment modéliser les ouvrages dans le cadre ISM
      !--FIN Actualisation des données variables avec le temps

      !---Discrétisation de Saint-Venant (ss-pg BIEF)
      reste = zero
      uselat = .false.
      ! TODO: remettre les échanges latéraux entre biefs ou de bief à casier pour ISM
      if (uselat) then
         call lateral_exchange(dt_trop_grand)
      else
         dt_trop_grand = .false.
      endif

      call ISM_discretisation(anomalie_dt_precedent)
      !anomalie_dt_precedent est vrai si ISM_discretisation et ISM_discretise_bief
      !ont détecté une anomalie sur le résultat du pas de temps précédent (par ex y < 0 ou Froude > FRmax)

      if (anomalie_dt_precedent .or. dt_trop_grand) then
#ifdef debug
         with_trace_full = .true.
#endif /* debug */
         call retour_arriere_ISM(convergence_ok,indic) !anomalie (divergence)
         cycle                             !-> il faut revenir en arrière et réduire dt
      else
         ! TODO: remettre une version de save_QD() adaptée à ISM
         !call save_QD                     !sauvegarde des débits perdus par déversement latéral
      endif

      !---Impression des résultats du pas de temps précédent sur .BIN et .TRA
      !   sauf si on l'a interrompu (manoeuvre de pompe ou non-convergence)
      niter0 = niter
      if ((.not. besoin_retour) .and. convergence_ok) call Ecrire

      !---Pilotages des itérations : lois d'ouvrage et Saint-Venant
         !initialisation
      call init_solution_ISM
      ! TODO: remettre l'initialisation des ouvrages adaptée pour ISM
      !call init_ouvrages_and_claval

      !itérations de Newton-Raphson sur les non-linéarités
      call nonLinear_iterations_ISM(convergence_ok)
      if (convergence_OK) then
         !on met à jour la solution standard avec la solution ISM
         do is = 1, la_topo%net%ns
            dq(is) = Q_total(dq_ism(is))
            dz(is) = dz_ism(is)
         enddo
      endif

      !---convergence des itérations : vérification de la conservation des volumes
      call timestep_last_actions(convergence_ok,besoin_retour,kper,niter0,indic,dtr)

      if (with_charriage > 0 .and. t > t_char-dtmin) then
         call update_charriage(t, dt_char)
         t_char = t_char + dt_char
      endif

   enddo  boucle_temps  ! fin de la boucle sur le temps

   !------------------------------------------------------------------------------
   !---Fin de la boucle sur le temps
   !------------------------------------------------------------------------------
#ifdef linux
   ! on annule l'interception du signal SIGINT envoyé par ctrl+C
   call signal(Signal_interruption,0)
#endif
   !---impression du dernier pas de temps
   if (iso<=0) call calcul_dt(besoin_retour,dtr)
   t = tmax+dt
   !---calcul des yt, vt
   !if (iso<=0) call Debord_discretisation(bool2)
   !call devers(dtr,bool2)
   !---impression
   call write_TRA_Ouvrages
   call Ecrire
   if (nb_mobiles > 0) call add_to_buffer_mobiles(tmax)

   t = tmax
   dt = dtold
   if (iso > 0) then
      !---affichage erreur sur la conservation des volumes
      write(lTra,'(a,e8.2,a,i3.3,a,/,a,e8.2,a,i3.3,a)') &
                 ' Erreur (relative par bief) en volume à l''instant final :  ',errsup, &
                 ' (Bief ',kbsup,')',' Erreur maximale sur la simulation :  ',errmax,   &
                 ' (Bief ',kbmax,')'
      call err020
   else
      !---vérification de la conservation des volumes
      if (kper >= 10) then
         write(lTra,'(/,20x,a,/)') '>>>> Le régime permanent est atteint <<<<'
         write(error_unit,'(/,20x,a,/)') '>>>> Le régime permanent est atteint <<<<'
      endif
      call bilan_volume(un,convergence_ok)
   endif
end subroutine time_iterations_ISM



subroutine time_iterations_mixte(m1ok)
!==============================================================================
!                         boucle sur le temps
!
!   À chaque pas de temps on fait d'abord un calcul avec Debord puis on tente
!   le même calcul avec ISM.
!   Si le calcul avec ISM réussit, on garde le résultat ISM, sinon on conserve
!   le résultat Debord, puis on passe au pas de temps suivant.
!==============================================================================
   use parametres, only: long, zero, un, Signal_interruption, nomini_ini, l9, lTra
   use, intrinsic :: iso_fortran_env, only: error_unit

   use mage_utilitaires, only: heure, zegal
   use IO_Files, only: envFile
   use booleens, only: uselat
   use casiers, only: dtlat
   use data_num_fixes, only: ini_internal, tinf, tmax, dtbase, dtmin, dtmin, dt_char
   use data_num_mobiles, only: t, dt ,dtold, niter, npas, ttra, tbin, dtmin0, ISM_calcul_OK, tmelissa, t_char
   use erreurs_sing, only: dqsup, tsup, volsng
   use erreurs_stv, only: errvol,errmax,errsup,ermax0,kbmax,kbsup, reste
   use Regime_Permanent, only: IniPer
   use StVenant_ISM, only: dq_ism => dq, dz_ism => dz, Q_total, ISM_discretisation, retour_arriere_ISM, &
                           init_solution_ISM
   use solution, only: dq, dz
   use TopoGeometrie, only: la_topo, with_charriage
   use charriage, only: update_charriage
   use StVenant_Debord, only: Debord_discretisation
   use hydraulique, only: zt
   use Ouvrages, only: update_positions_ouvrages, add_to_buffer_mobiles, nb_mobiles, NBreg
   use mage_regulation, only: RegLoc
   implicit none
   ! -- prototype
   integer, intent(out) :: m1ok       ! code d'erreur en sortie de M1, 0 si calcul complet
   ! -- interface explicite --
   interface
      function ISM_compatibility()
         integer :: ISM_compatibility
      end function ISM_compatibility
   end interface
   ! -- variables locales temporaires --
   logical :: besoin_retour  ! vrai s'il faut un retour en arrière
   logical :: convergence_ok ! vrai si convergence au pas de temp précédent
   logical :: utile ! vrai si régulation utilisée ; initialise à vrai et modifié par OBSERV
   logical :: mod92  ! vrai si relecture d'un fichier VAR
   logical :: bool2, bool3
   integer :: kper       ! nb de pas de temps successifs à 1 seule itération
   integer :: niter0     ! nb d'itérations du pas de temps précédent
   integer :: iso        ! iso = 0 si sortie normale en fin de simulation
   integer :: indic = 0  ! flag pour la gestion des retours en arrière
   real(kind=long) :: dtr, dtu, dtloc, tyet
   real(kind=long), dimension(:), allocatable :: q, z, y, v
   real (kind=long) :: told
   logical :: anomalie_dt_precedent, dt_trop_grand, ISM_succes, Debord_ok
   integer :: is
   character(len=19) :: hms
   logical :: previous_TimeStep_Was_Debord
   interface
      subroutine copier(tyet,q,z,y,v)
         use parametres, only: long
         real(kind=long),intent(out) :: tyet
         real(kind=long),dimension(:),intent(out) :: q,z    ! ligne d'eau
         real(kind=long),dimension(:),intent(out),optional :: y,v    ! ligne d'eau
      end subroutine copier
   end interface


   write(l9,'(a)') ' '
   m1ok = 0 ; kper = 0 ; reste = zero ; npas = 0 ; niter = 0 ; t = tinf
   call Relire_NUM   ! relecture du .NUM : ré-initialisation
   dtlat = dtbase ; mod92 = .false.
   call update_positions_ouvrages(mod92,t,zt)  ! initialisation de la position des ouvrages mobiles
   besoin_retour = .false. ; convergence_ok = .true. ; utile = .true. ; dtr = dtbase
   !---essai de procédure d'initialisation
   if (ini_internal) then
      call iniper   ! initialisation par le permanent interne
      ini_internal = .false.    ! on force la lecture du fichier au lieu de récupérer
                    ! les valeurs internes pour être sûr d'avoir le même
                    ! résultat si on reprend depuis ce fichier
      call Lire_INI(nomini_ini)
      write(l9,*) 'Relecture de l''état initial dans ',trim(nomini_ini),' : OK'
      call Lire_INI_ISM(nomini_ini)
      !on remet ini_internal à vrai parce qu'il ne faut pas mentir
      ini_internal = .true.
   endif

   !---Initialisation diverses
   dqsup = zero ; volsng = zero ; tsup = tinf
   errvol = zero ; errmax = zero ; errsup = zero ; ermax0 = zero
   kbsup = 1 ;  kbmax = 1
   previous_TimeStep_Was_Debord = .true.
   allocate(q(la_topo%net%ns), z(la_topo%net%ns), &
          & y(la_topo%net%ns), v(la_topo%net%ns))

   !------------------------------------------------------------------------------
   !---début de la boucle sur le temps
   !------------------------------------------------------------------------------
   !Les appels de Debord_discretisation() et de ISM_discretisation() avant la boucle sur le temps permettent
   !d'initialiser le nombre de Courant et donc de calculer un 1er pas de temps correct
   call Debord_discretisation(bool2)
   call ISM_discretisation(anomalie_dt_precedent)

   boucle_temps: DO  ! ---> Début de la boucle sur le temps

      if (.not. besoin_retour .and. convergence_ok) call write_TRA_Ouvrages  !Écriture des résultats aux ouvrages.
                                                                    !À faire avant toute modification des
                                                                    !paramètres d'ouvrage par relecture de .VAR
      if (convergence_ok .and. t<tmax) then   !Si convergence au pas de temps précédent
                                              !et si on n'est pas à TMAX
         call relire_NUM                                      !Relecture de NUM
         if (ttra > tmax .and. dtbase > zero) ttra=tmax  !si t=tmax on imprime sur listing au dernier pas de temps
         if (tmelissa > tmax .and. dtbase > zero) tmelissa=tmax  !si t=tmax on envoie la solution au dernier pas de temps
         if (tbin > tmax .and. dtbase > zero) tbin=tmax  !si t=tmax on écrit la solution au dernier pas de temps
         call relire_SIN_VAR(mod92)                              !Relecture de VAR (mod92 mis à .true. dans ce cas)
         if (nb_mobiles > 0 .and. zegal(t,tbin,un)) call add_to_buffer_mobiles(t) !il peut y avoir des pompes
      endif

      if (envFile /= '') call Envlop(envFile)  !calcul des valeurs enveloppe

      ! QUESTION: peut-on simplifier la liste des variables utilisées dans le test suivant ?
      if (convergence_ok .and. NBreg > 0) then !Régulation locale si convergence des itérations
                                               !au pas de temps précédent
         call copier(tyet,q,z,y,v)  !on envoie des copies des variables
         call RegLoc(tyet,q,z,y,v,dtloc)
!          mod92 = mod92 .or. mod920  !VAR a pu aussi être modifié à la main par l'utilisateur (MOD92)
         dtu   = dtloc
      else
         dtu = dtbase
      endif

      if (t>=tmax) exit boucle_temps  !fin de la boucle sur le temps si le TMAX est
                                      !atteint (doit se faire aprés l'observateur)

      !--Calcul du pas de temps
      dtr = min(dtr,dtu)    !DTR est fourni par timestep_last_actions à la fin du pas de temps précédent
      call calcul_dt(convergence_ok,dtr)   !CALCUL_DT renvoi DT < DTR et réduit / DT précédent
                                     !si convergence_ok est .FALSE. (divergence des
                                     !itérations au pas de temps précédent)
      if (dt < dtmin0) then
         iso = 1 ; call sortie_echec(mod92)
         m1ok = 200          !err020 : pas de temps < dtmin
         exit boucle_temps   !sortie prématurée en sautant l'appel à calcul_dt
      else
         told = t
         iso = 0 ; t = t+dt ; npas = npas+1   !incrémentation du temps et du nb total de pas de temps
      endif

      !--DÉBUT Actualisation des données variables avec le temps
      call cl_Amont        !Conditions aux limites amont en débit
      call apport(indic)   !Variation des apports/fuites (Q latéral) : QE et DQE

      !------Position des vannes : manoeuvres, automatisme
      !      BOOL3 est mis à .TRUE. si : nomEtude_var (unité 92, ouvert par Lire_VAR()) a été modifié
      !                                  par l'observateur (MOD921), la régulation (MOD920) ou la lecture de VAR (MOD92)
      !                          ou si : il faut faire un retour en arrière (besoin_retour)
      !                          ou si : il y a divergence (convergence_ok)
      bool3 = mod92 .or. besoin_retour .or. (.not. convergence_ok)
      call update_positions_ouvrages(bool3,t,zt)
      !--FIN Actualisation des données variables avec le temps

      !---Discrétisation de Saint-Venant (ss-pg BIEF)
      reste = zero
      if (uselat) then
         call lateral_exchange(dt_trop_grand)
      else
         dt_trop_grand = .false.
      endif
      call Debord_discretisation(anomalie_dt_precedent)
      !anomalie_dt_precedent est vrai si Debord_discretisation et discretise_bief
      !ont détecté une anomalie sur le résultat du pas de temps précédent
      !(par ex y < 0 ou Froude > FRmax)

      if (anomalie_dt_precedent .or. dt_trop_grand) then
         call retour_arriere(convergence_ok,indic) !anomalie (divergence)
         call retour_arriere_ISM(convergence_ok,indic)
         cycle                            !-> il faut revenir en arrière et réduire dt
      else
         call save_QD                     !sauvegarde des débits perdus par déversement latéral
      endif

      !---Impression des résultats du pas de temps précédent sur .BIN et .TRA
      !   sauf si on l'a interrompu (manoeuvre de pompe ou non-convergence)
      niter0 = niter
      if ((.not. besoin_retour) .and. convergence_ok) call Ecrire

      !---Pilotages des itérations : lois d'ouvrage et Saint-Venant
      call init_CLaval_Debord
      call init_solution_Debord
      call nonLinear_iterations(debord_ok)
      if (debord_ok) then
         ! on tente ISM
         convergence_ok = .true.
         call init_solution_ISM
         if (ISM_compatibility() /= 0) then
            call one_timestep_ISM(ISM_succes,previous_TimeStep_Was_Debord)
         else
            ISM_succes = .false.
         endif
         if (ISM_succes) then
            !on met à jour la solution standard avec la solution ISM
            hms = heure(t,dtmin)
            !write(l9,'(2a)') ' >>>> ISM : pas de temps réussi à ',hms
            do is = 1, la_topo%net%ns
               dq(is) = Q_total(dq_ism(is))
               dz(is) = dz_ism(is)
            enddo
            ISM_calcul_OK = .true.
            previous_TimeStep_Was_Debord = .false.
         else
            hms = heure(t,dtmin)
            !write(l9,'(2a)') ' //// ISM : pas de temps échoué à ',hms
            ISM_calcul_OK = .false.
            !call retour_arriere_ISM
            previous_TimeStep_Was_Debord = .true.
         endif
      else
         convergence_ok = .false.
      endif

      !---convergence des itérations : mise à jour des variables Q, Z et Y ;
      !                                vérification de la conservation des volumes
      call timestep_last_actions(convergence_ok,besoin_retour,kper,niter0,indic,dtr)

      if (with_charriage > 0 .and. t > t_char-dtmin) then
         call update_charriage(t, dt_char)
         t_char = t_char + dt_char
      endif

   enddo  boucle_temps  ! fin de la boucle sur le temps

   !------------------------------------------------------------------------------
   !---Fin de la boucle sur le temps
   !------------------------------------------------------------------------------
   ! on annule l'interception du signal SIGINT envoyé par ctrl+C
#ifdef linux
   call signal(Signal_interruption,0)
#endif
   !---impression du dernier pas de temps
   if (iso<=0) call calcul_dt(besoin_retour,dtr)
   t = tmax+dt
   !---calcul des yt, vt
   if (iso<=0) call Debord_discretisation(bool2)
   call devers(dtr,bool2)
   !---impression
   call write_TRA_Ouvrages
   call Ecrire
   if (nb_mobiles > 0) call add_to_buffer_mobiles(tmax)

   t = tmax
   dt = dtold
   if (iso > 0) then
      !---affichage erreur sur la conservation des volumes
      write(lTra,'(a,e8.2,a,i3.3,a,/,a,e8.2,a,i3.3,a)') &
                 ' Erreur (relative par bief) en volume à l''instant final :  ',errsup, &
                 ' (Bief ',kbsup,')',' Erreur maximale sur la simulation :  ',errmax,   &
                 ' (Bief ',kbmax,')'
      call err020
   else
      !---vérification de la conservation des volumes
      if (kper >= 10) then
         write(lTra,'(/,20x,a,/)') '>>>> Le régime permanent est atteint <<<<'
         write(error_unit,'(/,20x,a,/)') '>>>> Le régime permanent est atteint <<<<'
      endif
      call bilan_volume(un,convergence_ok)
   endif
   deallocate(q, z, y, v)
end subroutine time_iterations_mixte



subroutine time_iterations_mixte_2(m1ok)
!==============================================================================
!                         boucle sur le temps
!
!   On gère un basculement entre Debord et ISM pour réserver l'usage de ISM
!   quand le débordement est généralisé
!==============================================================================
   use parametres, only: long, zero, un, Signal_interruption, nomini_ini, l9, lTra
   use, intrinsic :: iso_fortran_env, only: error_unit

   use booleens, only: uselat
   use casiers, only: dtlat
   use data_num_fixes, only: ini_internal, tinf, tmax, dtbase, dtmin, dt_char
   use data_num_mobiles, only: t, dt ,dtold, niter, npas, ttra, tbin, dtmin0, ISM_calcul_OK, &
                               consecutive_ISM_timesteps, tmelissa, t_char
   use erreurs_sing, only: dqsup, tsup, volsng
   use erreurs_stv, only: errvol,errmax,errsup,ermax0,kbmax,kbsup, reste
   use hydraulique, only: qt,zt
   use IO_Files, only: envFile
   use Regime_Permanent, only: IniPer
   use StVenant_ISM, only: Q_total, ISM_discretisation, retour_arriere_ISM, &
                           convert_state_Debord_to_ISM, dq_ism => dq, dz_ism => dz, &
                           init_solution_ISM
   use StVenant_Debord, only: Debord_discretisation
   use TopoGeometrie, only: la_topo, with_charriage
   use Ouvrages, only: update_positions_ouvrages, add_to_buffer_mobiles, nb_mobiles, NBreg
   use solution, only: dq, dz
   use mage_utilitaires, only: zegal, is_zero
   use mage_regulation, only: RegLoc
   use charriage, only: update_charriage
   implicit none
   ! -- prototype
   integer, intent(out) :: m1ok       ! code d'erreur en sortie de M1, 0 si calcul complet
   ! -- interface explicite --
   interface
      function ISM_compatibility()
         integer :: ISM_compatibility
      end function ISM_compatibility
   end interface
   ! -- variables locales temporaires --
   logical :: besoin_retour  ! vrai s'il faut un retour en arrière
   logical :: convergence_ok ! vrai si convergence au pas de temp précédent
   logical :: utile ! vrai si régulation utilisée ; initialise à vrai et modifié par OBSERV
   logical :: mod92  ! vrai si relecture d'un fichier VAR
   logical :: bool2, bool3
   integer :: kper       ! nb de pas de temps successifs à 1 seule itération
   integer :: niter0     ! nb d'itérations du pas de temps précédent
   integer :: iso        ! iso = 0 si sortie normale en fin de simulation
   integer :: indic = 0  ! flag pour la gestion des retours en arrière
   real(kind=long) :: dtr, dtu, dtloc, tyet
   real(kind=long), dimension(:), allocatable :: q, z, y, v
   real (kind=long) :: told
   logical :: anomalie_dt_precedent, dt_trop_grand
   logical :: previous_TimeStep_Was_Debord, Do_ISM
   integer :: is, ism_c
   integer, save :: nb_dt_reductions
   interface
      subroutine copier(tyet,q,z,y,v)
         use parametres, only: long
         real(kind=long),intent(out) :: tyet
         real(kind=long),dimension(:),intent(out) :: q,z    ! ligne d'eau
         real(kind=long),dimension(:),intent(out),optional :: y,v    ! ligne d'eau
      end subroutine copier
   end interface


   write(l9,'(a)') ' '
   m1ok = 0 ; kper = 0 ; reste = zero ; npas = 0 ; niter = 0 ; t = tinf
   call Relire_NUM   ! relecture du .NUM : ré-initialisation
   dtlat = dtbase ; mod92 = .false.
   call update_positions_ouvrages(mod92,t,zt)  ! initialisation de la position des ouvrages mobiles
   besoin_retour = .false. ; convergence_ok = .true. ; utile = .true. ; dtr = dtbase
   !---essai de procédure d'initialisation
   if (ini_internal) then
      call iniper   ! initialisation par le permanent interne
      ini_internal = .false.    ! on force la lecture du fichier au lieu de récupérer
                    ! les valeurs internes pour être sûr d'avoir le même
                    ! résultat si on reprend depuis ce fichier
      call Lire_INI(nomini_ini)
      write(l9,*) 'Relecture de l''état initial dans ',trim(nomini_ini),' : OK'
      call Lire_INI_ISM(nomini_ini)
      !on remet ini_internal à vrai parce qu'il ne faut pas mentir
      ini_internal = .true.
      previous_TimeStep_Was_Debord = .false.  !on commence probablement par ISM
   else
      previous_TimeStep_Was_Debord = .true.  !on commence probablement par Debord
   endif

   !---Initialisation diverses
   dqsup = zero ; volsng = zero ; tsup = tinf
   errvol = zero ; errmax = zero ; errsup = zero ; ermax0 = zero
   kbsup = 1 ;  kbmax = 1
   nb_dt_reductions = 0
   consecutive_ISM_timesteps = 0
   allocate(q(la_topo%net%ns), z(la_topo%net%ns), &
          & y(la_topo%net%ns), v(la_topo%net%ns))

   !------------------------------------------------------------------------------
   !---début de la boucle sur le temps
   !------------------------------------------------------------------------------
   !Les appels de Debord_discretisation() et de ISM_discretisation() avant la boucle sur le temps permettent
   !d'initialiser le nombre de Courant et donc de calculer un 1er pas de temps correct
   call Debord_discretisation(bool2)
   call ISM_discretisation(anomalie_dt_precedent)

   boucle_temps: DO  ! ---> Début de la boucle sur le temps

      if (.not. besoin_retour .and. convergence_ok) call write_TRA_Ouvrages  !Écriture des résultats aux ouvrages.
                                                                    !À faire avant toute modification des
                                                                    !paramètres d'ouvrage par relecture de .VAR
      if (convergence_ok .and. t<tmax) then
         call relire_NUM
         if (ttra > tmax .and. dtbase > zero) ttra=tmax  !si t=tmax on imprime sur listing au dernier pas de temps
         if (tmelissa > tmax .and. dtbase > zero) tmelissa=tmax  !si t=tmax on envoie la solution au dernier pas de temps
         if (tbin > tmax .and. dtbase > zero) tbin=tmax  !si t=tmax on écrit la solution au dernier pas de temps
         call relire_SIN_VAR(mod92)                      !mod92 mis à .true. si un fichier VAR est effectivement lu
         if (nb_mobiles > 0 .and. zegal(t,tbin,un)) call add_to_buffer_mobiles(t) !il peut y avoir des pompes
      endif

      if (envFile /= '') call Envlop(envFile)  !calcul des valeurs enveloppe

      ! QUESTION: peut-on simplifier la liste des variables utilisées dans le test suivant ?
      if (convergence_ok .and. NBreg > 0) then !Régulation locale si convergence des itérations
                                           !au pas de temps précédent
         call copier(tyet,q,z,y,v)  !on envoie des copies des variables
         call RegLoc(tyet,q,z,y,v,dtloc)
!          mod92 = mod92 .or. mod920  !VAR a pu aussi être modifié à la main par l'utilisateur (MOD92)
         dtu   = dtloc
      else
         dtu = dtbase
      endif

      if (t>=tmax) exit boucle_temps  !fin de la boucle sur le temps si le TMAX est
                                      !atteint (doit se faire aprés l'observateur)

      !--Calcul du pas de temps
      dtr = min(dtr,dtu)    !DTR est fourni par timestep_last_actions à la fin du pas de temps précédent
      call calcul_dt(convergence_ok,dtr)   !CALCUL_DT renvoi DT < DTR et réduit / DT précédent
                                     !si convergence_ok est .FALSE. (divergence des
                                     !itérations au pas de temps précédent)
      if (dt < dtmin0) then
         iso = 1 ; call sortie_echec(mod92)
         m1ok = 200          !err020 : pas de temps < dtmin
         exit boucle_temps   !sortie prématurée en sautant l'appel à calcul_dt
      else
         told = t
         iso = 0 ; t = t+dt ; npas = npas+1   !incrémentation du temps et du nb total de pas de temps
      endif

      if (dt < dtold .and. .not.convergence_ok) then
         nb_dt_reductions = nb_dt_reductions + 1
      else if (dt > dtold .and. convergence_ok) then
         nb_dt_reductions = max(0,nb_dt_reductions - 1)
      else if (is_zero(dt-dtbase) .and. convergence_ok) then
         nb_dt_reductions = max(0,nb_dt_reductions - 1)
      else
         continue
      endif



      !--DÉBUT Actualisation des données variables avec le temps
      call cl_Amont        !Conditions aux limites amont en débit
      call apport(indic)   !Variation des apports/fuites (Q latéral) : QE et DQE

      !------Position des vannes : manoeuvres, automatisme
      !      BOOL3 est mis à .TRUE. si : nomEtude_var (unité 92, ouvert par Lire_VAR()) a été modifié
      !                                  par l'observateur (MOD921), la régulation (MOD920) ou la lecture de VAR (MOD92)
      !                          ou si : il faut faire un retour en arrière (besoin_retour)
      !                          ou si : il y a divergence (convergence_ok)
      bool3 = mod92 .or. besoin_retour .or. (.not. convergence_ok)
      call update_positions_ouvrages(bool3,t,zt)
      !--FIN Actualisation des données variables avec le temps

      !---Discrétisation de Saint-Venant (ss-pg BIEF)
      reste = zero
      if (uselat) then
         call lateral_exchange(dt_trop_grand)
      else
         dt_trop_grand = .false.
      endif

      ism_c = ISM_compatibility()
      if ((ism_c > 0 .and. nb_dt_reductions <= 6) .or. ism_c < 0) then
         !débordement généralisé ou pas de débordement du tout -> on peut utiliser ISM
         !on ajoute la contrainte sur le pas de temps pour forcer la bascule sur Debord si ISM diverge
         !et tenter ainsi d'éviter l'arrêt du programme

         if (previous_TimeStep_Was_Debord) call convert_state_Debord_to_ISM(qt,zt)
         call ISM_discretisation(anomalie_dt_precedent)

         if (anomalie_dt_precedent .or. dt_trop_grand) then
            ! anomalie_dt_precedent est vrai si discretise_bief() a détecté une anomalie sur le résultat
            ! du pas de temps précédent (par ex y < 0 ou Froude > FRmax)
            if (previous_TimeStep_Was_Debord) then
            ! le pas de temps précédent était Debord -> on laisse tomber ISM et on reprend Debord
               call Debord_discretisation(anomalie_dt_precedent)
               if (anomalie_dt_precedent .or. dt_trop_grand) then
                  ! anomalie_dt_precedent est vrai si Debord_discretisation a détecté une anomalie sur le résultat
                  ! du pas de temps précédent (par ex y < 0 ou Froude > FRmax)
                  call retour_arriere(convergence_ok,indic) !anomalie (divergence)
                  cycle                            !-> il faut revenir en arrière et réduire dt
               else
                  previous_TimeStep_Was_Debord = .true. !ça c'est pour le pas de temps suivant
                  Do_ISM = .false. !ISM : même pas la peine d'essayer
               endif
            else
               ! le pas de temps précédent était ISM -> il faut donc revenir en arrière sur ISM
               call retour_arriere_ISM(convergence_ok,indic)
               cycle                            !-> il faut revenir en arrière et réduire dt
            endif
         else
            !La discrétisation pour ISM s'est bien passée -> on peut continuer avec ISM
            previous_TimeStep_Was_Debord = .false.
            Do_ISM = .true.  !ISM : OK on peut essayer
         endif
      else
         !débordement partiel ou pas de débordement -> on reste avec Debord
         consecutive_ISM_timesteps = 0
         call Debord_discretisation(anomalie_dt_precedent)
         if (anomalie_dt_precedent .or. dt_trop_grand) then
            ! anomalie_dt_precedent est vrai si Debord_discretisation a détecté une anomalie sur le résultat
            ! du pas de temps précédent (par ex y < 0 ou Froude > FRmax)
            call retour_arriere(convergence_ok,indic) !anomalie (divergence)
            cycle                            !-> il faut revenir en arrière et réduire dt
         else
            previous_TimeStep_Was_Debord = .true. !ça c'est pour le pas de temps suivant
            Do_ISM = .false. !ISM : même pas la peine d'essayer
         endif
      endif
      call save_QD                     !sauvegarde des débits perdus par déversement latéral

      !---Impression des résultats du pas de temps précédent sur .BIN et .TRA
      !   sauf si on l'a interrompu (manoeuvre de pompe ou non-convergence)
      niter0 = niter
      if ((.not. besoin_retour) .and. convergence_ok) call Ecrire

      !---Pilotages des itérations : lois d'ouvrage et Saint-Venant
      if (Do_ISM) then
         ! on peut tenter ISM
         call init_solution_ISM
         call nonLinear_iterations_ISM(convergence_ok)
         if (convergence_OK) then
            !on met à jour la solution standard avec la solution ISM
            do is = 1, la_topo%net%ns
               dq(is) = Q_total(dq_ism(is))
               dz(is) = dz_ism(is)
            enddo
            ISM_calcul_OK = .true.
         else
            ISM_calcul_OK = .false.
         endif
      else
         ! pas la peine d'essayer ISM -> Debord
         call init_CLaval_Debord
         call init_solution_Debord
         call nonLinear_iterations(convergence_ok)
         ISM_calcul_OK = .false.
      endif
      if (ISM_calcul_OK) then
         consecutive_ISM_timesteps = consecutive_ISM_timesteps +1
      else
         consecutive_ISM_timesteps = 0
      endif
      !
      !---convergence des itérations : mise à jour des variables Q, Z et Y ;
      !                                vérification de la conservation des volumes
      call timestep_last_actions(convergence_ok,besoin_retour,kper,niter0,indic,dtr)

      if (with_charriage > 0 .and. t > t_char-dtmin) then
         call update_charriage(t, dt_char)
         t_char = t_char + dt_char
      endif

   enddo  boucle_temps  ! fin de la boucle sur le temps

   !------------------------------------------------------------------------------
   !---Fin de la boucle sur le temps
   !------------------------------------------------------------------------------
   ! on annule l'interception du signal SIGINT envoyé par ctrl+C
#ifdef linux
   call signal(Signal_interruption,0)
#endif
   !---impression du dernier pas de temps
   if (iso<=0) call calcul_dt(besoin_retour,dtr)
   t = tmax+dt
   !---calcul des yt, vt
   if (iso<=0) call Debord_discretisation(bool2)
   call devers(dtr,bool2)
   !---impression
   call write_TRA_Ouvrages
   call Ecrire
   if (nb_mobiles > 0) call add_to_buffer_mobiles(tmax)

   t = tmax
   dt = dtold
   if (iso > 0) then
      !---affichage erreur sur la conservation des volumes
      write(lTra,'(a,e8.2,a,i3.3,a,/,a,e8.2,a,i3.3,a)') &
                 ' Erreur (relative par bief) en volume à l''instant final :  ',errsup, &
                 ' (Bief ',kbsup,')',' Erreur maximale sur la simulation :  ',errmax,   &
                 ' (Bief ',kbmax,')'
      call err020
   else
      !---vérification de la conservation des volumes
      if (kper >= 10) then
         write(lTra,'(/,20x,a,/)') '>>>> Le régime permanent est atteint <<<<'
         write(error_unit,'(/,20x,a,/)') '>>>> Le régime permanent est atteint <<<<'
      endif
      call bilan_volume(un,convergence_ok)
   endif
   deallocate(q, z, y, v)
end subroutine time_iterations_mixte_2



subroutine one_timeStep_ISM(ISM_succes,previous_TimeStep_Was_Debord)
!==============================================================================
!  Réalisation d'un pas de temps avec ISM
!  On part de Debord si ISM a échoué au pas de temps précédent
!==============================================================================
   use parametres, only: long

   use StVenant_ISM, only: ISM_discretisation, convert_state_Debord_to_ISM
   use hydraulique, only: qt,zt
   use data_num_mobiles, only: ifrsup, frsup
   implicit none
   ! -- prototype
   logical, intent(out) :: ISM_succes       ! renvoie VRAI si ISM a échoué
   logical, intent(in)  :: previous_TimeStep_Was_Debord
   ! -- variables locales temporaires --
   logical :: convergence_ok ! vrai si convergence au pas de temp précédent
   logical :: anomalie_dt_precedent
   integer :: ifrsup_bak
   real(kind=long) :: frsup_bak

   ifrsup_bak = ifrsup  ;  frsup_bak = frsup

   ISM_succes = .true.
   if (previous_TimeStep_Was_Debord) call convert_state_Debord_to_ISM(qt,zt)
   call ISM_discretisation(anomalie_dt_precedent)
   !anomalie_dt_precedent est vrai si ISM_discretisation et ISM_discretise_bief
   !ont détecté une anomalie sur le résultat du pas de temps précédent (par ex y < 0 ou Froude > FRmax)

   if (anomalie_dt_precedent) then
      ISM_succes = .false.
      ifrsup = ifrsup_bak  ;  frsup = frsup_bak
      return
   endif

   !itérations de Newton-Raphson sur les non-linéarités
   call nonLinear_iterations_ISM(convergence_ok)
   ISM_succes = convergence_OK
   if (.not.ISM_succes) then
      ifrsup = ifrsup_bak  ;  frsup = frsup_bak
   endif
end subroutine one_timeStep_ISM



subroutine timestep_last_actions(convergence_ok,besoin_retour,kper,niter0,indic,dtr)
!==============================================================================
!               travaux pour terminer le pas de temps en cours
!
! --> bilan de volume / masse
! --> ajustement du pas de temps pour les besoins de la régulation
! --> mise à jour des variables de débit et niveau
! --> vérification du régime permanent s'il est recherché
!==============================================================================
   use parametres, only: long, zero, l9
   use data_num_fixes, only: tmax, dtbase, steady, dtmin, dt_char
   use data_num_mobiles, only: t, dt, niter, ISM_available, t_char, tmax_cl
   use StVenant_ISM, only: ISM_actualize_Q_Z
   use TopoGeometrie, only: with_charriage
   use charriage, only: update_charriage
   !--Prototype
   logical,intent(inout) :: convergence_ok ! vrai si convergence au pas de temp précédent
   logical,intent(inout) :: besoin_retour  ! vrai s'il faut un retour en arrière
   integer,intent(inout) :: kper           ! nb de pas de temps successifs à 1 seule itération
   integer,intent(in)    :: niter0         ! nb d'itérations du pas de temps précédent
   integer,intent(inout) :: indic          ! flag pour la gestion des retours en arrière
   real(kind=long),intent(inout) :: dtr
   integer, save :: kper2 = 0


   if (convergence_ok) call bilan_volume(zero,convergence_ok)

   !---Convergence et conservation des volumes : Régulation locale (pompes et
   !   déversements latéraux) : DTR pas de temps réduit fourni par REGUL
   !                            besoin_retour = .True. : il faut revenir en arrière
   if (convergence_ok) call ajust_DT(besoin_retour,dtr)

   if (.not. convergence_ok) then
      indic = 0 ; t = t-dt    !non-convergence : annulation du pas de temps et réduction de dt
   elseif (besoin_retour) then
      indic = 1 ; t = t-dt    !annulation du pas de temps (idem non-convergence) et réduction de dt
   else
      indic = 1               !validation de ce pas de temps : actualisation des variables d'état
      if (ISM_available()) then
         call ISM_actualize_Q_Z
         call actualize_Q_Z
         ! TODO: remettre actualize_QD dans le cas ISM quand il y aura des déversements latéraux
      else
         call actualize_Q_Z      !convergence + conservation des volumes + ok pour régulation
         call actualize_QD
      endif
   endif

   !call write_Screen_dZdQ

   if (steady .and. niter-niter0==1) then
      kper = kper+1
   else
      kper = 0
      kper2 = 0
   endif
   if (kper>=10) then
        if (t > tmax_cl .and. kper2 == 0) then
            kper2 = 1
            tmax = t+dtbase+dtbase
            write(l9,*) ' >>>> Le régime permanent est atteint <<<<'
        endif
   endif

end subroutine timestep_last_actions




subroutine sortie_echec(mod92)
!==============================================================================
!                   sortie prématurée
!==============================================================================
   use parametres, only: long, zero, lTra

   use data_num_fixes, only: tmax, date_format
   use data_num_mobiles, only: t, ttra, tbin
   use solution, only: dq, dz
   use hydraulique, only: zt
   use mage_utilitaires, only: heure
   use TopoGeometrie, only: la_topo
   use Ouvrages, only: update_positions_ouvrages
   implicit none
   ! -- prototype --
   logical,intent(in) :: mod92    !indicateur de modif du fichier inimage.ouv (unite 92)
   ! -- variables locales temporaires --
   character(len=19) :: hms !chaine pour formatter la date jjj:hh:mm:ss,cc
   character(len=80) :: ligne

   hms = heure(t)
   ligne = repeat('=',80) ; ligne(1:1) = ' '
   if (date_format) then
      write(lTra,'(///,3a)') ' ########  DATE : ',hms, &
                             '                (             )  ########'
   else
      write(lTra,'(///,3a)') ' ########  DATE : ',hms, &
                             ' (jjj:hh:mn:ss) (             )  ########'
   endif
   write(lTra,'(a,/,a)') ligne,''
   dq = zero ; dz = zero
   tmax = t ; ttra = t ; tbin = t
   if (la_topo%net%nss>0) call update_positions_ouvrages(mod92,t,zt)
   call write_TRA_Ouvrages
end subroutine sortie_echec



subroutine annule(Bool1,Indic)
!==============================================================================
!                   Retour en arrière après observateur
!==============================================================================
   use parametres, only: long, un, l9

   use mage_utilitaires, only: zegal
   use solution, only: dqa, dza
   use erreurs_stv, only: errmax,errsup,ermax0
   use hydraulique, only: qt, zt, yt
   use TopoGeometrie, only: la_topo
   implicit none
   ! -- prototype --
   logical,intent(out) :: bool1
   integer,intent(out) :: indic
   ! -- variables
   integer, pointer :: ismax

   write(l9,'(2x,a)') ' >>>> Retour en arrière demandé par l''observateur <<<<'
   ismax => la_topo%net%ns
   indic = -1
   qt(1:ismax) = qt(1:ismax) - dqa(1:ismax)
   zt(1:ismax) = zt(1:ismax) - dza(1:ismax)
   !yt(1:ismax) = yt(1:ismax) - dza(1:ismax)
   yt(1:ismax) = zt(1:ismax) - la_topo%sections(1:ismax)%zf
   !---calcul des cotes moyennes aux nœuds
   call cotes_noeuds

   if (zegal(errmax,errsup,un)) errmax = ermax0
   bool1 = .false.

end subroutine annule



subroutine retour_arriere(bool1,indic)
!==============================================================================
!                   retour en arrière
!==============================================================================
   use, intrinsic :: iso_fortran_env, only: error_unit

   use parametres, only: long, zero, un, total, rivg, rivd, lTra, l9
   use mage_utilitaires, only: zegal, heure
   use data_num_fixes, only: plus, tinf, dtmin
   use data_num_mobiles, only: t, dt, dtold
   use solution, only: dqa, dza
   use erreurs_stv, only: errmax,errsup,ermax0
   use hydraulique, only: qt, zt, yt
   use casiers, only: nclat,qds0,qds1,qdb0,qdb1,qclat,vd
   use TopoGeometrie, only: la_topo, xgeo
   implicit none
   ! -- prototype --
   logical,intent(out) :: bool1
   integer,intent(out) :: indic
   ! -- variables locales temporaires --
   integer :: ib, is, neu
   integer :: typdev !indice de boucle sur le type de deversement
                     !ici ca ne concerne que les rives droite et gauche
   real(kind=long) :: dxx
   character(len=180) :: err_message
   integer, pointer :: ismax
   character(len=19) :: hms


   hms = heure(t,dtmin)
   write(l9,'(3a)') plus//'>>>> RETOUR EN ARRIERE (Debord) : mauvaise discrétisation à ',hms,' <<<<'
   ismax => la_topo%net%ns
   indic = -1
   bool1 = .false.
   qt(1:ismax) = qt(1:ismax) - dqa(1:ismax)
   zt(1:ismax) = zt(1:ismax) - dza(1:ismax)
   !yt(1:ismax) = yt(1:ismax) - dza(1:ismax)
   yt(1:ismax) = zt(1:ismax) - la_topo%sections(1:ismax)%zf
   !---rétablissement des volumes et débit échanges par biefs
   !   vd est ramené à sa valeur à t-dt-dtold : on est au début de [t ; t+dt]
   !   et on revient au début de [t-dt-dtold ; t-dt]
   do ib = 1,la_topo%net%nb
      do typdev = rivg,rivd           ! on tient compte des deux rives
         vd(typdev,ib) = vd(typdev,ib)-qdb1(typdev,ib)*dtold
      enddo
      vd(total,ib) = vd(rivg,ib)+vd(rivd,ib)
      do typdev = total,rivd
         qdb1(typdev,ib) = qdb0(typdev,ib)
         qdb0(typdev,ib) = zero
      enddo
      do is = la_topo%biefs(ib)%is1,la_topo%biefs(ib)%is2-1
         dxx = abs(xgeo(is+1)-xgeo(is))
         do typdev = rivg,rivd
            qdb0(typdev,ib) = qdb0(typdev,ib)+qds0(typdev,is)*dxx
         enddo
         qdb0(total,ib) = qdb0(total,ib)+qdb0(rivg,ib)+qdb0(rivd,ib)
      enddo
   enddo
   !---rétablissement des débits des échanges latéraux vers les casiers
   !   les débits par section sont calculés à la volée
   qclat(1:la_topo%net%nn) = zero
   do ib = 1, la_topo%net%nb
      do is = la_topo%biefs(ib)%is1, la_topo%biefs(ib)%is2-1
         do typdev = rivg,rivd
            neu = nclat(typdev,is)
            dxx = abs(xgeo(is+1)-xgeo(is))
            if (neu > 0) then
               !cas d'un échange avec nœud à surface non-nulle
               qclat(neu) = qclat(neu)+qds0(typdev,is)*dxx
            elseif (neu == 0) then
               !cas d'un échange avec l'extérieur du réseau
               qclat(neu) = qclat(neu)-qds1(typdev,is)*dxx*dtold  ! pourquoi qds1 ici ?
            endif
         enddo
      enddo
   enddo

   !---calcul des cotes moyennes aux noeuds
   call cotes_noeuds

   if (zegal(errmax,errsup,un)) errmax=ermax0
   t = t-dt-dtold
   dt = dtold
   if (t < tinf) then
      write(err_message,'(a)') ' >>>> La ligne d''eau initiale ne permet pas de démarrer le calcul'
      write(lTra,'(a)') trim(err_message)
      write(error_unit,'(a)') trim(err_message)
      stop 103
   endif
end subroutine retour_arriere



subroutine nonLinear_iterations(Bool)
!==============================================================================
!
!            itérations de Newton-Raphson dans le cadre Debord
!
!         Itération :  X(N+1) = F(X(N)) (équation de type F(X) = X)
!
!---> Les résultats sont dans le module SOLUTION pour l'estimation
!     définitive des DQ et DZ dans le cas NEWT < ou = 0
!---> La variable de sortie Bool renvoie faux s'il y a divergence ou
!     retour en arrière nécessaire
!
!---> si NEWT=1  l'itération ne concerne que les lois d'ouvrage
!
!---> si NEWT=0  l'itération est un point fixe pour toutes les lois d'ouvrage
!                et de type N-R pour Saint-Venant
!---> si NEWT=-1 l'itération de N-R sur Saint-Venant et les lois d'ouvrage commence
!                par INR itérations de type N-R pour les déversoirs_orifices et les vannes
!                puis bascule en point fixe
!
!==============================================================================
   use parametres, only: long, zero, un, lnode, l9

   use mage_utilitaires, only: zegal, f0p
   use data_num_fixes, only: iter, inr, iter1, dfrp1, dfrp2, dfrp3, &
! #ifdef fake_mage7
                             dtmin, &
! #endif /* fake_mage7 */
                             newt, plus
   use data_num_mobiles, only: niter, kb0=>isi, t, dt
   use Data_Iterations_loc, only: ez,eq,frpz,frpq,frpr,izmax,iqmax

   implicit none
   ! -- Prototype --
   logical,intent(out) :: Bool  !faux si divergence ou retour en arrière nécessaire
   ! -- Variables locales temporaires --
   logical :: ZERR, BOOL1, ZCON
   integer :: I,I0,IZCON, Newt1
   character(len=lnode)    :: Nom
   character(len=  5)      :: CDT
   character(len= 10)      :: Chaine
   character(len=100)      :: Ligne !chaîne faite de - + et = pour montrer la convergence
   real(kind=Long) :: frpz1, frpq1, frpr1, ez0, eq0
   character(len=16) :: f3001 = '(a,25x,a,i2.2,a)'
#ifndef fake_mage7
   character(len=9) :: prefix = ' ### T: '
#endif /* fake_mage7 */
#ifdef fake_mage7
   character(len=4) :: prefix = ' T : '
#endif /* fake_mage7 */

   !---Initialisation
   ligne = repeat(' ',100)
   if (dt >= 1000._long) then
      write(cdt,'(i4.4,a)') int(dt),' '
   else if (dt >= un) then
      write(cdt,'(i3.3,a)') int(dt),'  '
   else
      cdt = trim(f0p(dt,3))
   endif

   !---initialisation des écarts entre itérées successives
   ez = 1.e+30_long
   eq = 1.e+30_long

   !---initialisation des facteurs de réduction de la precision
   !   FRPx passe a FRPx1 apres ITER1 iterations et vaut 1 avant
   frpz = un
   frpq = un
   frpr = un
   frpz1 = dfrp1
   frpq1 = dfrp2
   frpr1 = dfrp3
! #ifdef fake_mage7
   if (dt < dtmin+0.001_long) then
      !on ne fait pas plus de ITER1 itérations si DT est à sa valeur plancher
      frpz1 = 1.e+10_long
      frpq1 = 1.e+10_long
   endif
! #endif /* fake_mage7 */

   newt1 = newt  !sauvegarde de NEWT
   izcon = 0
   !---Fin de l'initialisation


   !===Itérations : boucle de <1> à <ITER> ========================================
   i = 1 ; do
      niter = niter+1  !comptage du nombre total d'itérations
      bool = .true.
      if (newt < 0  .and. i > inr) newt = 0 !Basculement de NEWT : on continue en point fixe

      if (i > iter1) then
         !on modifie le test d'arrêt (a priori il devient moins strict)
         frpz = frpz1
         frpq = frpq1
         frpr = frpr1
      endif

      call save_previous_iteration(ez0,eq0)  !sauvegarde de l'itérée précédente

      !---double balayage -----------------------------------------------------------
      kb0 = i-1 !fixation de kb0 (signal pour BAL32 : 1ère itération faite si kb0>0)
      call bal

      !---Vérification tirant d'eau positif et pas trop grand -----------------------
      call verif_HauteurEau(bool,chaine)
      if (.not.bool) then
         i0 = max(1,i-14)
         write(l9,f3001) plus,'DT = '//cdt//' << ',i,' >>  '//chaine//ligne(i0:i)
         bool = .false.  ;  newt = newt1  ; exit
      endif

      !---vérification de l'égalité des cotes aux noeuds ----------------------------
      call verif_CotesNoeuds(bool,nom)
      if (.not.bool) then
         write(l9,f3001) plus,'DT = '//cdt//' << ',i,' >>  Inégalité des cotes au noeud '//nom
         bool = .false.  ;  newt = newt1  ; exit
      endif

      !---test d'arrêt --(zerr est vrai si convergence)------------------------------
      call testArret_NL_iteration(zerr)
      ligne(i:i) = '-'
      if (zerr) then  !écritures écran et sortie
         newt = newt1 ; call write_Screen(prefix,ligne,i) ; exit
      endif

      !===>à partir d'ici on traite les cas où l'itération n'a pas encore convergé

      !---Test sur la divergence des itérations (écarts croissants) -----------------
      zcon = i<3
      if (.not.zcon) then
         zcon =     (ez<1.01_long*ez0 .or. zegal(ez,zero,un)) &
               .and.(eq<1.01_long*eq0 .or. zegal(eq,zero,un))
      endif
      if (zcon) then
         izcon = 0
      else if (i > 2) then
         izcon = izcon+1
         ligne(i:i) = '+'
      endif
      if (izcon > 5) then
         call err001(t,i,ez,ez0,eq,eq0,izmax,iqmax,'Debord')
         i0 = max(1,i-14)
         write(l9,f3001) plus,'DT = '//cdt//' << ',i,' >>  DIVERGENCE '//ligne(i0:i)
         bool = .false.  ;  newt = newt1  ; exit
      endif

      !---Test sur le bouclage des itérations ---------------------------------------
#ifdef fake_mage7
      if (zegal(ez,ez0,ez0) .and. zegal(eq,eq0,eq0)) then
         !    on fait un test d'arrêt en réduisant la précision tout de suite sauf si la réduction de précision est désactivée
         !    par un nombre d'itérations avant réduction supérieur ou égal au nombre maximum d'itération
         !-NB-->il faut laisser les tests suivants : il reste inutile de continuer
         !      après UN bouclage. Vérifié à nouveau le 22/01/2008 sur l'Yzeron
         zerr = zegal(ez,zero,frpz1) .and. zegal(eq,zero,frpq1)
         if (zerr) then !écritures écran et sortie
            newt = newt1 ; call write_Screen(prefix,ligne,i) ; exit
         else
            !--->inutile de continuer : il faut réduire le pas de temps
            call err003(t,i,ez,ez0,eq,eq0,izmax,iqmax)
            i0 = max(1,i-14)
            write(l9,f3001) plus,'DT = '//cdt//' << ',i,' >>  BOUCLAGE '//ligne(i0:i)
            bool = .false.  ;  newt = newt1  ; exit
         endif
      endif
#endif /* fake_mage7 */
#ifndef fake_mage7
      if (((iter1 < iter .and. i > iter1) .or. iter1 >= iter) .and. zegal(ez,ez0,ez0) .and. zegal(eq,eq0,eq0)) then
         ligne(i:i) = '='
         zerr = zegal(ez,zero,frpz1) .and. zegal(eq,zero,frpq1)
         if (zerr) then !écritures écran et sortie
            newt = newt1 ; call write_Screen(prefix,ligne,i) ; exit
         else
            !--->inutile de continuer : il faut réduire le pas de temps
            call err003(t,i,ez,ez0,eq,eq0,izmax,iqmax)
            i0 = max(1,i-14)
            write(l9,f3001) plus,'DT = '//cdt//' << ',i,' >>  BOUCLAGE '//ligne(i0:i)
            bool = .false.  ;  newt = newt1  ; exit
         endif
      endif
#endif /* fake_mage7 */

      !---actualisation du second membre ou fin des itérations
      if (i == iter) then             !non convergence
         call err002(t,iter,ez0,ez,eq0,eq,izmax,iqmax)
         i0 = max(1,i-14)
         write(l9,f3001) plus,'DT = '//cdt//' << ',i,' >>  NON-CONVERGENCE '//ligne(i0:i)
         bool = .false.  ;  newt = newt1  ; exit
      else                            !actualisation du second membre
         call update_NL_iteration(bool1)
         if (bool1) then
            !--->on a détecté un passage en TORRENTIEL du à une DIVERGENCE des itérations
            !    ou un pb de validité du pas de temps pour les déversements latéraux
            !    ===> inutile de continuer, il faut réduire le pas de temps
            i0 = max(1,i-14)
            write(l9,f3001) plus,'DT = '//cdt//' << ',i,' >>  INSTABILITE '//ligne(i0:i)
            bool = .false.  ;  newt = newt1  ; exit
         else
            i = i+1 !itération suivante
         endif
      endif
   enddo
   !===Fin de la boucle des itérations ===========================================
end subroutine nonLinear_iterations



subroutine nonLinear_iterations_ISM(Bool)
!==============================================================================
!
!                 itérations de Newton-Raphson dans le cadre ISM
!
!==============================================================================
   use parametres, only: long, zero, un, l9

   use mage_utilitaires, only: zegal
   use data_num_fixes, only: iter, iter1, dfrp1, dfrp2, dfrp3, newt, plus
   use data_num_mobiles, only: niter,t,dt
   use Data_Iterations_loc, only: ez,eq,frpz,frpq,frpr,izmax,iqmax

   use StVenant_ISM, only: ISM_update_NR, save_previous_iteration_ISM, ISM_resolution

   implicit none
   ! -- Prototype --
   logical,intent(out) :: Bool  !faux si divergence ou retour en arrière nécessaire
   ! -- Variables locales temporaires --
   logical :: ZERR, BOOL1, ZCON
   integer :: I,I0,IZCON, Newt1
   character(len=  4)      :: CDT
   character(len= 10)      :: Chaine
   character(len=100)      :: Ligne !chaîne faite de - + et = pour montrer la convergence
   real(kind=Long) :: ez0, eq0
   character(len=16) :: f3001 = '(a,25x,a,i2.2,a)'
   logical :: bFail
   character(len=9) :: prefix = ' \\\ T: '

   !---Initialisation
   ligne = repeat(' ',100)
   if (dt >= 1000._long) then
      write(cdt,'(i4.4)') int(dt)
   else if (dt >= un) then
      write(cdt,'(i3.3,a)') int(dt),' '
   else
      write(cdt,'(a,f3.2)') '0',dt
   endif

   !---initialisation des écarts entre itérées successives
   ez = 1.e+30_long
   eq = 1.e+30_long

   !---initialisation des facteurs de réduction de la precision
   !   FRPx passe a FRPx1 apres ITER1 iterations et vaut 1 avant
   frpz = un
   frpq = un
   frpr = un

   newt1 = newt  !sauvegarde de NEWT
   izcon = 0
   !---Fin de l'initialisation


   !===Itérations : boucle de <1> à <ITER> ========================================
   i = 1 ; do
      niter = niter+1  !comptage du nombre total d'itérations
      bool = .true.

      if (i > iter1) then
         !on modifie le test d'arrêt (a priori il devient moins strict)
         frpz = dfrp1
         frpq = dfrp2
         frpr = dfrp3
      endif

      call save_previous_iteration_ISM(ez0,eq0)  !sauvegarde de l'itérée précédente

      !---double balayage -----------------------------------------------------------
      call ISM_resolution(bFail)
      if (bFail) then
         bool = .false.  ;  newt = newt1  ; exit
      endif

      !---Vérification tirant d'eau positif et pas trop grand -----------------------
      call verif_HauteurEau(bool,chaine)
      if (.not.bool) then
         i0 = max(1,i-14)
         write(l9,f3001) plus,'DT = '//cdt//' << ',i,' >>  '//chaine//ligne(i0:i)
         bool = .false.  ;  newt = newt1  ; exit
      endif

      !---vérification de l'égalité des cotes aux noeuds ----------------------------
      ! TODO: remettre verif_CotesNoeuds() quand on aura le réseau maillé avec ISM
!      call verif_CotesNoeuds(bool,nom)
!      if (.not.bool) then
!         write(l9,f3001) plus,'DT = '//cdt//' << ',i,' >>  Inégalité des cotes au noeud '//nom
!         bool = .false.  ;  newt = newt1  ; exit
!      endif

      !---test d'arrêt --(zerr est vrai si convergence)------------------------------
      call testArret_NL_iteration_ISM(zerr)
      ligne(i:i) = '-'
      if (zerr) then  !écritures écran et sortie
         newt = newt1 ; call write_Screen(prefix,ligne,i) ; exit
      endif

      !===>à partir d'ici on traite les cas où l'itération n'a pas encore convergé

      !---Test sur la divergence des itérations (écarts croissants) -----------------
      zcon = i<3
      if (.not.zcon) then
         zcon =     (ez<1.01_long*ez0 .or. zegal(ez,zero,un)) &
               .and.(eq<1.01_long*eq0 .or. zegal(eq,zero,un))
      endif
      if (zcon) then
         izcon = 0
      else if (i > 2) then
         izcon = izcon+1
         ligne(i:i) = '+'
      endif
      if (izcon > 5 .OR. ez > 1.E+05_long .OR. eq > 1.E+05_long) then
         call err001(t,i,ez,ez0,eq,eq0,izmax,iqmax,'ISM')
         i0 = max(1,i-14)
         write(l9,f3001) plus,'DT = '//cdt//' << ',i,' >>  DIVERGENCE (ISM) '//ligne(i0:i)
         bool = .false.  ;  newt = newt1  ; exit
      endif

      !---Test sur le bouclage des itérations ---------------------------------------
      if (zegal(ez,ez0,ez0) .and. zegal(eq,eq0,eq0)) then
         ligne(i:i) = '='
         !--->inutile de continuer : il faut réduire le pas de temps
         call err003(t,i,ez,ez0,eq,eq0,izmax,iqmax)
         i0 = max(1,i-14)
         write(l9,f3001) plus,'DT = '//cdt//' << ',i,' >>  BOUCLAGE (ISM) '//ligne(i0:i)
         bool = .false.  ;  newt = newt1  ; exit
      endif

      !---actualisation du second membre ou fin des itérations
      if (i == iter) then             !non convergence
         call err002(t,iter,ez0,ez,eq0,eq,izmax,iqmax)
         i0 = max(1,i-14)
         write(l9,f3001) plus,'DT = '//cdt//' << ',i,' >>  NON-CONVERGENCE (ISM) '//ligne(i0:i)
         bool = .false.  ;  newt = newt1  ; exit
      else                            !actualisation du second membre
         call ISM_update_NR(bool1)
         if (bool1) then
            !--->on a détecté un passage en TORRENTIEL du à une DIVERGENCE des itérations
            !    ou un pb de validité du pas de temps pour les déversements latéraux
            !    ===> inutile de continuer, il faut réduire le pas de temps
            i0 = max(1,i-14)
            write(l9,f3001) plus,'DT = '//cdt//' << ',i,' >>  INSTABILITE (ISM) '//ligne(i0:i)
            bool = .false.  ;  newt = newt1  ; exit
         else
            i = i+1 !itération suivante
         endif
      endif
   enddo
   !===Fin de la boucle des itérations ===========================================
end subroutine nonLinear_iterations_ISM



subroutine testArret_NL_iteration(ZErr)
!==============================================================================
!       Calcul des écarts entre 2 itérées successives et test d'arrêt
!
! SCH3 > 0 : test en valeur absolue
! SCH3 = 0 : test en valeur relative global
! SCH3 < 0 : test en valeur relative par bief
! ECZ et ECQ : ecarts max en Z et Q entre 2 iterees successives
! IZ et IQ : numeros de sections ou ces ecarts sont max
! FRPZ et FRPQ : facteurs de reduction de la precision en Z et Q
! ZERR est vrai si la convergence est atteinte
!==============================================================================
   use Parametres, only: Long,Zero,Un

   use Mage_Utilitaires, only: zegal
   use data_num_fixes, only: newt, sch3
   use hydraulique, only: qt, yt
   use solution, only: dq, dz
   use Data_Iterations_loc, only: dz0,dq0,ecz=>ez,ecq=>eq,frpz,frpq,frpr,IZ=>IZMax,IQ=>IQMax
   use TopoGeometrie, only: la_topo
   use erreurs_stv, only: reste
   implicit none
   ! -- Prototype --
   logical,intent(out) :: ZErr
   ! -- Variables locales temporaires --
   integer :: ib, iq0, is, iz0
   real(kind=Long) :: A, ymax, ecarz, ecarq, qmax
   integer, pointer :: ismax, ibmax, nsmax

   ismax => la_topo%net%ns
   ibmax => la_topo%net%nb
   nsmax => la_topo%net%nss

   !---sortie directe si on ne fait pas d'itérations
   if (newt > 0) then
      zerr = .true.  ;  return
   endif
   !---initialisations
   ecz = zero ; ecq =zero ; iz = 1 ; iq = 1

   !---calcul des écarts maximaux en Z et Q
   if (sch3 > zero) then               !écarts en valeurs absolues
      do is = 1, ismax
         a = abs(dz(is)-dz0(is))
         if (a > ecz) then
            ecz = a ; iz = is
         endif
         a = abs(dq(is)-dq0(is))
         if (a > ecq) then
            ecq = a ; iq = is
         endif
      enddo
   elseif (sch3 < zero) then           !écarts en valeurs relatives par bief
      qmax = zero  ;  ymax = zero
      ecarz = zero ;  ecarq = zero
      iz0 = -1 ; iq0 = -1
      do ib = 1, ibmax
         do is = la_topo%biefs(ib)%is1, la_topo%biefs(ib)%is2
            a = abs(dz(is)-dz0(is))
            if (a > ecarz) then
               ecarz = a ; iz0 = is
            endif
            a = abs(dq(is)-dq0(is))
            if (a > ecarq) then
               ecarq = a ; iq0 = is
            endif
            qmax = max(qmax,abs(qt(is)))
            ymax = max(ymax,yt(is))
         enddo
         a = ecarz/ymax
         if (a > ecz) then
            if (iz0 < 0) stop '>>>> BUG dans testArret_NL_iteration_ISM()'
            iz = iz0 ; ecz = a
         endif
         a = ecarq/max(qmax,un)
         if (a > ecq) then
            if (iq0 < 0) stop '>>>> BUG dans testArret_NL_iteration_ISM()'
            iq = iq0 ; ecq = a
         endif
      enddo
   else                                !écarts en valeurs relatives globales
      qmax = zero ; ymax = zero
      do is = 1, ismax
         a = abs(dz(is)-dz0(is))
         if (a > ecz) then
            ecz = a ; iz = is
         endif
         a = abs(dq(is)-dq0(is))
         if (a > ecq) then
            ecq = a ; iq = is
         endif
         qmax = max(qmax,abs(qt(is)))
         ymax = max(ymax,yt(is))
      enddo
      ecz = ecz/ymax
      ecq = ecq/max(qmax,un)
   endif

   !---test de convergence
#ifndef fake_mage7
   zerr = zegal(ecz,zero,frpz) .and. zegal(ecq,zero,frpq) .and. zegal(reste,zero,frpr)
#endif /* fake_mage7 */
#ifdef fake_mage7
   zerr = zegal(ecz,zero,frpz) .and. zegal(ecq,zero,frpq)
#endif /* fake_mage7 */

end subroutine testArret_NL_iteration



subroutine testArret_NL_iteration_ISM(ZErr)
!==============================================================================
!       Calcul des écarts entre 2 itérées successives et test d'arrêt
!
! SCH3 > 0 : test en valeur absolue
! SCH3 = 0 : test en valeur relative global
! SCH3 < 0 : test en valeur relative par bief
! ECZ et ECQ : ecarts max en Z et Q entre 2 iterees successives
! IZ et IQ : numeros de sections ou ces ecarts sont max
! FRPZ et FRPQ : facteurs de reduction de la precision en Z et Q
! ZERR est vrai si la convergence est atteinte
!==============================================================================
   use Parametres, only: Long, Zero, Un

   use Mage_Utilitaires, only: zegal
   use data_num_fixes, only: newt, sch3
   use Data_Iterations_loc, only: ecz=>ez,ecq=>eq,frpz,frpq,frpr,IZ=>IZMax,IQ=>IQMax
   use TopoGeometrie, only: la_topo
   use StVenant_ISM, only: z, q, dz, dq, dz0, dq0, Q_total, operator(-)
   use erreurs_stv, only: reste
   implicit none
   ! -- Prototype --
   logical,intent(out) :: ZErr
   ! -- Variables locales temporaires --
   integer :: ib, iq0, is, iz0
   real(kind=Long) :: A, ymax, ecarz, ecarq, qmax
   integer, pointer :: ismax, ibmax, nsmax
   real(kind=long), dimension(:), allocatable :: yt

   ismax => la_topo%net%ns
   ibmax => la_topo%net%nb
   nsmax => la_topo%net%nss
   allocate(yt(ismax))

   !---sortie directe si on ne fait pas d'itérations
   if (newt > 0) then
      zerr = .true.  ;  return
   endif
!   write(l9,*) 'Entrée dans testArret_NL_iteration_ISM : ',sch3
   !---initialisations
   ecz = zero ; ecq = zero ; iz = 1 ; iq = 1
   do is = 1, ismax
      yt(is) = z(is) - la_topo%sections(is)%zf
   enddo

   !---calcul des écarts maximaux en Z et Q
   if (sch3 > zero) then               !écarts en valeurs absolues
      do is = 1, ismax
         a = abs(dz(is)-dz0(is))
         if (a > ecz) then
            ecz = a ; iz = is
         endif
         a = abs(Q_total(dq(is)-dq0(is)))
         if (a > ecq) then
            ecq = a ; iq = is
         endif
      enddo
   elseif (sch3 < zero) then           !écarts en valeurs relatives par bief
      qmax = zero  ;  ymax = zero
      ecarz = zero ;  ecarq = zero
      iz0 = -1 ; iq0 = -1
      do ib = 1, ibmax
         do is = la_topo%biefs(ib)%is1, la_topo%biefs(ib)%is2
            a = abs(dz(is)-dz0(is))
            if (a > ecarz) then
               ecarz = a ; iz0 = is
            endif
            a = abs(Q_total(dq(is)-dq0(is)))
            if (a > ecarq) then
               ecarq = a ; iq0 = is
            endif
            qmax = max(qmax,abs(Q_total(q(is))))
            ymax = max(ymax,yt(is))
         enddo
         a = ecarz/ymax
         if (a > ecz) then
            if (iz0 < 0) stop '>>>> BUG 1 dans testArret_NL_iteration_ISM()'
            iz = iz0 ; ecz = a
         endif
         a = ecarq/max(qmax,un)
         if (a > ecq) then
            if (iq0 < 0) stop '>>>> BUG 2 dans testArret_NL_iteration_ISM()'
            iq = iq0 ; ecq = a
         endif
      enddo
   else                                !écarts en valeurs relatives globales
      qmax = zero ; ymax = zero
      do is = 1, ismax
         a = abs(dz(is)-dz0(is))
         if (a > ecz) then
            ecz = a ; iz = is
         endif
         a = abs(Q_total(dq(is)-dq0(is)))
         if (a > ecq) then
            ecq = a ; iq = is
         endif
         qmax = max(qmax,abs(Q_total(q(is))))
         ymax = max(ymax,yt(is))
      enddo
      ecz = ecz/ymax
      ecq = ecq/max(qmax,un)
   endif

   !---test de convergence
   zerr = zegal(ecz,zero,frpz) .and. zegal(ecq,zero,frpq) .and. zegal(reste,zero,frpr)
   deallocate(yt)

end subroutine testArret_NL_iteration_ISM



subroutine update_NL_iteration(Bool)
!==============================================================================
!      Actualisation de l'itération de Newton-Raphson sur Saint-Venant
!          et du point fixe sur les ouvrages et les C.L. aval
!
!--->iflag(ns) vaut +1 si un clapet est fermé en ns
!                   -1 si la discrétisation est de type N-R
!                    0 si la discrétisation est de type point fixe
!==============================================================================
   use parametres, only: long,zero,cent,un, l9

   use mage_utilitaires, only:
   use data_num_fixes, only: newt
   use data_num_mobiles, only: t
   use hydraulique, only: qt,zt
   use solution, only: dq, dz
   use conditions_limites, only: tmv, cl
   use erreurs_stv, only: reste
   use casiers, only : dtlat
   use TopoGeometrie, only: la_topo
   use STVenant_Debord, only: Debord_update_NR
   implicit none
   ! -- Prototype --
   logical,intent(out) :: Bool
   ! -- Variables locales temporaires --
   integer :: n, isn
   integer, pointer :: nomax

   nomax => la_topo%net%nn

   !---initialisation
   reste = zero  ;  bool = .false.
   call cl_amont
   call lateral_Exchange(bool)
   if (bool) then
      write(l9,*) '>>>> Réduction du pas de temps forcée par Lateral_Exchange : ',dtlat
      return  !ll faut réduire DT
   endif
   !---Cas des sections NON singulières
   if (newt < 1) then !calcul du nouveau second membre si on fait des itérations
      call Debord_update_NR(bool)
      if (bool) return
   end if
   !---cas des sections singulières traitées par point-fixe
   ! NOTE:  code supprimé pour le point-fixe
   !---cas des C.L. aval
   do n = 1,nomax
      if (la_topo%nodes(n)%cl < 0) then  ! n est nœud aval dans ce cas
         isn = la_topo%biefs(la_topo%net%numero(la_topo%net%lbamv(n)))%is2
         if (la_topo%nodes(n)%cl /= -3) then  ! c.l. en q(z) ou régime uniforme
            tmv(n) = tmv(n)+(cl(n,zt(isn)+dz(isn))-(qt(isn)+dq(isn)))
         else                     ! c.l. en z(t)
            tmv(n) = cl(n,t)-zt(isn)
         endif
      endif
   enddo
end subroutine update_NL_iteration



subroutine save_previous_iteration(ez0,eq0)
!==============================================================================
!             Sauvegarde de l'itérée précédente
!==============================================================================
   use Parametres, only: Long
   use solution, only: dq, dz, dzn
   use Data_Iterations_loc, only: dz0, dq0, dzn0, ez, eq
   use TopoGeometrie, only: la_topo
   implicit none
   ! -- Prototype --
   real(kind=Long),intent(out) :: ez0, eq0
   ! -- variables
   integer, pointer :: ismax, nomax

   ismax => la_topo%net%ns
   nomax => la_topo%net%nn

   ez0 = ez  ;  eq0 = eq
   dz0(1:ismax)  = dz(1:ismax)
   dq0(1:ismax)  = dq(1:ismax)
   dzn0(1:nomax) = dzn(1:nomax)
end subroutine save_previous_iteration
