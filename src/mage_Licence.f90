!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - INRAE - 1989-2020                           #
!# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################
!******************************************************************************
!role: licence
!******************************************************************************
module Licence

contains

subroutine print_license(output_unit)
   integer, intent(in) :: output_unit

   write(output_unit,*) ''
   write(output_unit,*) 'Copyright (C) 2023 INRAE'
   write(output_unit,*) ''
   write(output_unit,*) 'Mage is free software; you can redistribute it and/or'
   write(output_unit,*) 'modify it under the terms of the GNU Lesser General Public'
   write(output_unit,*) 'License as published by the Free Software Foundation; either'
   write(output_unit,*) 'version 3 of the License, or (at your option) any later version.'
   write(output_unit,*) ''
   write(output_unit,*) 'Alternatively, you can redistribute it and/or'
   write(output_unit,*) 'modify it under the terms of the GNU General Public License as'
   write(output_unit,*) 'published by the Free Software Foundation; either version 2 of'
   write(output_unit,*) 'the License, or (at your option) any later version.'
   write(output_unit,*) ''
   write(output_unit,*) 'Mage is distributed in the hope that it will be useful, but'
   write(output_unit,*) 'WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or'
   write(output_unit,*) 'FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License'
   write(output_unit,*) 'or the GNU General Public License for more details.'
   write(output_unit,*) ''
   write(output_unit,*) 'You should have received a copy of the GNU Lesser General Public'
   write(output_unit,*) 'License and a copy of the GNU General Public License along with'
   write(output_unit,*) 'Mage. If not, see <http://www.gnu.org/licenses/>.'

end subroutine print_license

end module Licence
