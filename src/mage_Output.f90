!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - INRAE - 1989-2020                           #
!# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################
!##############################################################################
!
!        BIBLIOTHÈQUE DE SOUS-PROGRAMMES POUR LE PROGRAMME MAGE
!
!                          routines d'écriture
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine copier(tyet,q,z,y,v)
!==============================================================================
!
!        copie de la ligne d'eau pour transmission a l'observateur
!
!==============================================================================
   use parametres, only: long, i_ism, i_debord, i_mixte
   use hydraulique, only : qt, zt, yt, vt
   use data_num_mobiles, only: t, ISM_available
   use TopoGeometrie, only: la_topo, zfd
   use StVenant_ISM, only: q_ism => q, z_ism => z, Q_total
   implicit none
   ! -- prototype --
   real(kind=long),intent(out) :: tyet
   real(kind=long),dimension(:),intent(out) :: q,z    ! ligne d'eau
   real(kind=long),dimension(:),intent(out),optional :: y,v    ! ligne d'eau
   ! -- variable --
   integer, pointer :: ismax
   integer :: is

   ismax => la_topo%net%ns
   tyet = t
   !TODO : possibilité de parallélisation ici
   if (ISM_available()) then
      ! si on utilise ISM et que son calcul a réussi, alors on l'utilise
      do is = 1, ismax
         q(is) = q_total(q_ism(is))
      enddo
      if (present(y)) then
         do is = 1, ismax
            y(is) = z_ism(is)-zfd(is)
         enddo
      endif
      if (present(v)) then
         do is = 1, ismax
            v(is) = q(is) / la_topo%sections(is)%surface_mouillee_cote(z_ism(is))
         enddo
      endif
      z(1:ismax) = z_ism(1:ismax)
   else
      q(1:ismax) = qt(1:ismax)
      z(1:ismax) = zt(1:ismax)
      if (present(y)) y(1:ismax) = yt(1:ismax)
      if (present(v)) v(1:ismax) = vt(1:ismax)
   endif
end subroutine copier
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine SinFin(NomSin)
!==============================================================================
!
!        sauvegarde des positions des ouvrages par écriture
!                  d'une nouvelle version de .SIN
!
!==============================================================================
   use parametres, only: long, zero
   use Ouvrages, only: all_OuvEle, all_OuvCmp
   use TopoGeometrie, only: la_topo, xgeo, numero_bief
   use data_num_mobiles, only: t,dt
   use Mage_Utilitaires, only: heure
   implicit none
   ! -- prototype --
   character(len=12), intent(in) :: nomsin
   ! -- variables --
   character(len=1) :: ctyp
   character(len=10) :: imp
   character(len=9) :: pm
   integer :: ib,ibref,is,iuvnk,js,k,kb,nk,ns,ltmp
   real(kind=long) :: zarret,zch,zdev,zmarch, pk
   character(len=27) :: f1000 = '(a1,i3,1x,a9,5f10.3,16x,a)'
   character(len=34) :: f1010 = '(a1,i3,1x,a9,6f10.3,i3,f10.3,1x,a)'
   character(len=19) :: hms
   integer :: values(8)
   character(len=8) :: ddate
   character(len=10) :: ttime
   character(len=5) :: zone

   open(newunit=ltmp,file=NomSin,status='unknown',form='formatted')
   call date_and_time(ddate,ttime,zone,values)
   hms = heure(t-dt)
   write(ltmp,'(3a,i4.4,5(a1,i2.2))') '* État final des ouvrages par Mage à ',trim(hms),' Date simulation : ', &
                                      values(1),'-',values(2),'-',values(3),' ',values(5),':',values(6),':',values(7)
   write(ltmp,'(a)') '*'
   do ib = 1, la_topo%net%nb !parcours du réseau dans l'ordre des données
      !boucle sur les biefs
      do js = la_topo%biefs(ib)%is1+1, la_topo%biefs(ib)%is2
         !boucle sur les sections singulières du bief courant
         if (la_topo%sections(js)%iss > 0) then
            ns = la_topo%sections(js)%iss
            is = js-1
            pk = xgeo(is)
            if (pk < -99999.99_long .or. pk > 999999.99_long) then
               write(pm,'(f9.1)') pk
            else
               write(pm,'(f9.2)') pk
            endif
         else
            cycle
         endif
         kb = numero_bief(is)
         do k = 1, all_OuvCmp(ns)%ne
            !boucle sur les ouvrages élémentaires de la section singulière courante
            nk = all_OuvCmp(ns)%OuEl(k,1)
            iuvnk = all_OuvEle(nk)%iuv
            imp = all_OuvEle(nk)%cvar
            zdev = all_OuvEle(nk)%uv2  !cote déversement
            if (all_OuvEle(nk)%uv3 > 100._long) then   !cote charge
               zch = 9999.999_long
            else
               zch = zdev+all_OuvEle(nk)%uv3
            endif
            if (iuvnk == 0) then
               ctyp = 'D'
               write(ltmp,'(a)') '*---Déversoir-Orifice à seuil mobile'
               write(ltmp,f1000) ctyp,kb,pm,all_OuvEle(nk)%uv1,zdev,zch,all_OuvEle(nk)%uv4,all_OuvEle(nk)%uv5,imp
            else if (iuvnk == 1) then
               ctyp = 'C'
               write(ltmp,'(a)') '*---Clapet ou porte à flot'
               write(ltmp,f1000) ctyp,kb,pm,all_OuvEle(nk)%uv1,zdev,zch,all_OuvEle(nk)%uv4,all_OuvEle(nk)%uv5,imp
            else if (iuvnk == 2) then
               ctyp = 'V'
               write(ltmp,'(a)') '*---Vanne de fond (loi standard)'
               write(ltmp,f1000) ctyp,kb,pm,all_OuvEle(nk)%uv1,zdev,zch,all_OuvEle(nk)%uv4,zdev+all_OuvEle(nk)%uv5,imp
            else if (iuvnk == 3) then
               ctyp = 'P'
               zmarch = all_OuvEle(nk)%za1
               zarret = all_OuvEle(nk)%za2
               ibref = numero_bief(all_OuvEle(nk)%irf)
               write(ltmp,'(a)') '*---Pompe'
               write(ltmp,f1010) ctyp,kb,pm,all_OuvEle(nk)%uv1,all_OuvEle(nk)%uv4,all_OuvEle(nk)%uv5,all_OuvEle(nk)%za1, &
                                all_OuvEle(nk)%za2,all_OuvEle(nk)%za3,ibref,xgeo(all_OuvEle(nk)%irf),imp
            else if (iuvnk == 4) then
               ctyp = 'W'
               write(ltmp,'(a)') '*---Vanne de fond (loi simplifiée)'
               write(ltmp,f1000) ctyp,kb,pm,all_OuvEle(nk)%uv1,zdev,zch,all_OuvEle(nk)%uv4,zdev+all_OuvEle(nk)%uv5,imp
            else if (iuvnk == 5) then
               ctyp = 'L'
               write(ltmp,'(a)') '*---Déversoir latéral'
               write(ltmp,f1000) ctyp,kb,pm,all_OuvEle(nk)%uv1,zdev,zch,all_OuvEle(nk)%uv4,all_OuvEle(nk)%uv5,imp
            else if (iuvnk == 6) then
               ctyp = 'O'
               write(ltmp,'(a)')'*---Déversoir-orifice à ouverture variable'
               write(ltmp,f1000) ctyp,kb,pm,all_OuvEle(nk)%uv1,zdev,zch,all_OuvEle(nk)%uv4,zdev+all_OuvEle(nk)%uv5,imp
            else if (iuvnk == 7) then
               ctyp = 'I'
               write(ltmp,'(a)') '*---Clapet ou porte à flot inversée'
               write(ltmp,f1000) ctyp,kb,pm,all_OuvEle(nk)%uv1,zdev,zch,all_OuvEle(nk)%uv4,all_OuvEle(nk)%uv5,imp
            else if (iuvnk == 8) then
               ctyp = 'B'
               zch = zdev+all_OuvEle(nk)%uv1-all_OuvEle(nk)%uv3
               write(ltmp,'(a)') '*---Buse circulaire'
               write(ltmp,f1000) ctyp,kb,pm,all_OuvEle(nk)%uv1,zdev,zch,all_OuvEle(nk)%uv4,all_OuvEle(nk)%uv5,imp
            else if (iuvnk == 9) then
               ctyp = 'X'
               write(ltmp,'(a)') '*---Ouvrage défini par l''utilisateur'
               write(ltmp,f1000) ctyp,kb,pm,all_OuvEle(nk)%uv1,all_OuvEle(nk)%uv2,all_OuvEle(nk)%uv3,all_OuvEle(nk)%uv4, &
                                            all_OuvEle(nk)%uv5,imp
            else if (iuvnk == 10) then
               ctyp='T'
               write(ltmp,'(a)') '*---Déversoir trapézoïdal'
               write(ltmp,f1000) ctyp,kb,pm,all_OuvEle(nk)%uv1,zdev,zch,all_OuvEle(nk)%uv4,all_OuvEle(nk)%uv5,imp
            else if (iuvnk == 91) then
               ctyp = 'A'
               write(ltmp,'(a)') '*---Perte de charge à la Borda'
               write(ltmp,f1000) ctyp,kb,pm,all_OuvEle(nk)%uv1,all_OuvEle(nk)%uv2,zero,all_OuvEle(nk)%uv4,zero,imp
            end if
         enddo
      enddo
   enddo
   close (ltmp)
end subroutine SinFin
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine MZero
!==============================================================================
!
!  Moulinette de remplacement des blancs qui devraient être des 0
!===============================================================================
   use parametres, only: long, lTra
   use, intrinsic :: iso_fortran_env, only: error_unit

   use IO_Files, only: traFile
   use data_num_fixes, only: encodage
   implicit none
   ! -- les variables --
   integer,parameter :: line_size=218
   character(len=line_size) :: a, err_message
   integer :: k, k1, k2, k3,lg, ios, ltmp
   !---------------------------------------------------------------------------
   rewind lTra
   open(newunit=ltmp,status='scratch',form='formatted')
   !---recopie de .TRA sur fichier temporaire
   do
      read(lTra,'(a)',iostat=ios) a
      if (ios < 0) then
         exit
      elseif (ios > 0) then
         write(err_message,'(2A)') '>>>> Erreur dans MZERO : ',a
         write(error_unit,'(a)') trim(err_message) ; write(lTra,'(a)') trim(err_message)
         write(err_message,'(a)') 'Merci d''envoyer un rapport de bug'
         write(error_unit,'(a)') trim(err_message) ; write(lTra,'(a)') trim(err_message)
         stop 209
      endif
      write(ltmp,'(a)') a
   enddo

   !traitement du fichier temporaire et ré-écriture de .TRA
   rewind ltmp
   close(lTra)
   open(unit=lTra,file=traFile,status='unknown',form='formatted',encoding=encodage)
   do
      read(ltmp,'(a)',iostat=ios) a
      if (ios /= 0) exit  !sortie en fin de fichier
      do k = 1, line_size-3
         k1 = k+1 ; k2 = k+2 ; k3 = k+3
         if ( a(k:k)==' '.and.a(k1:k1)==' '.and.a(k2:k2)=='.') then
            a(k1:k1) = '0'
            a(k2:k2) = '.'
         elseif ( a(k:k)=='('.and.a(k1:k1)==' '.and.a(k2:k2)=='.') then
            a(k1:k1) = '0'
            a(k2:k2) = '.'
         else if (a(k:k)==' '.and.a(k1:k1)==' '.and.a(k2:k2)=='-'.and.a(k3:k3)=='.') then
            a(k1:k1) = '-'
            a(k2:k2) = '0'
         else if (a(k:k)==' '.and.a(k1:k1)==' '.and.a(k2:k2)==' '.and.a(k3:k3)=='.') then
            a(k1:k1) = '+'
            a(k2:k2) = '0'
         endif
      enddo
      lg = max(1,len_trim(a))
      write(lTra,'(a)') a(1:lg)
   enddo
   close (ltmp)
end subroutine MZero
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine Ecrire
!==============================================================================
!
!          Sous-programme d'écriture des résultats sur les fichiers
!                       etude.BIN ET etude.TRA
!
!==============================================================================
   use parametres, only: long, nosup, lBin, lTra, nomfin_ini, &
                         nomfin_sin, rivg, rivd, total, un, zero, lBinGra
   use casiers, only: nclat,qds1,qclat,su,vo,vd,vb,devlat
   use data_num_fixes, only: tmax,dttra,dtbin,dtmelissa,with_melissa
   use data_num_mobiles, only: t,dt,ttra,tbin,ISM_available,tmelissa
   use hydraulique, only: qt,zt
   use IO_Files, only: sinFile, cgnsFile
   use mage_utilitaires, only: ecr0, zegal, do_crash, is_zero
   use TopoGeometrie, only: la_topo, aire_noeud, volume_noeud, xgeo, with_charriage
   use StVenant_ISM, only: q_ism => q, z_ism => z, Q_total, ISM_IniFin
   use StVenant_Debord, only: Debord_IniFin
   use Ouvrages, only: ltra_size, ligne_tra
   use iso_c_binding, only: C_CHAR, C_NULL_CHAR
#if WITH_CGNS==1
   use mage_cgns, only: mage_ecrire_cgns
#endif

   implicit none
   ! -- les variables --
   character(len=14) :: chain,chain1
   logical :: bool4(nosup)
   integer :: ib,is,jsa,k,k0,neu,nn,l,rive,ijk,is1,is2
   real(kind=long) :: qdev,qn,temp
   real(kind=long) :: vn,volu,vres,xsa,xsb,zn
   real(kind=long),allocatable :: q(:), z(:)
   character(len=9) :: km3
   logical :: ISM_OK
   ! -- les appels --
   interface
      subroutine copier(tyet,q,z,y,v)
         use parametres, only: long
         real(kind=long),intent(out) :: tyet
         real(kind=long),dimension(:),intent(out) :: q,z    ! ligne d'eau
         real(kind=long),dimension(:),intent(out),optional :: y,v    ! ligne d'eau
      end subroutine copier
   end interface

   allocate(q(la_topo%net%ns),z(la_topo%net%ns))

   ISM_OK = ISM_available()
   !---------------------------------------------------------------------------
   !écriture des débits et des cotes sur les fichiers csv
   !---------------------------------------------------------------------------
   call write_csv
   !---------------------------------------------------------------------------
   !écriture des débits et des cotes sur etude.bin
   !---------------------------------------------------------------------------

   if (zegal(t-dt,tbin,un)) then
      call ecrbin(lBin)
#if WITH_CGNS==1
      if (cgnsFile /= '') then
         call mage_ecrire_cgns()
      endif
#endif
      !calcul de la prochaine date d'écriture sur fichier résultat
      if (with_charriage > 0) then
         call ecrgra2(lBinGra)
      endif
      if (dtbin > zero) then
         tbin = tbin+dtbin
      else
         tbin = t
      endif
      !si t=tmax on écrit la solution au dernier pas de temps
      if (tbin > tmax) tbin = tmax
   endif
#ifdef mpi
#ifdef melissa
   if (with_melissa) then
      if (zegal(t-dt,tmelissa,un)) then
          call EcrMelissa()
          !calcul de la prochaine date d'envoie a Melissa server
          if (dtmelissa > zero) then
            tmelissa = tmelissa+dtmelissa
          else
            tmelissa = t
          endif
          !si t=tmax on écrit la solution au dernier pas de temps
          if (tmelissa > tmax) tmelissa = tmax
      endif
   endif
#endif /* melissa */
#endif /* mpi */
   !---------------------------------------------------------------------------
   !sauvegarde de l'état courant sous forme d'un fichier INI et SIN en vue
   !d'un re-démarrage après plantage
   !
   !préparation de l'écriture des résultats sur TRA
   !---------------------------------------------------------------------------
   if (zegal(t-dt,ttra,un)) then
      if (ISM_OK) then
         call ISM_IniFin(nomfin_ini)
      else
         temp = t-dt
         call copier(temp,q,z)
         call Debord_IniFin(nomfin_ini,q,z)
      endif
      if (sinFile /= '') call sinfin(nomfin_sin)

      !---calcul de la prochaine date d'écriture sur le listing
      ttra = ttra+dttra
      !---si t=tmax on écrit sur listing au dernier pas de temps
      if (ttra>tmax .and. dttra>zero) ttra = tmax
      if (ttra<tmax .and. dttra<zero) ttra = tmax
   else
      return
   endif
   !---------------------------------------------------------------------------
   !écriture des résultats aux noeuds pour chaque bief sur .tra
   !---------------------------------------------------------------------------
   !initialisation volume total du réseau
   vres = zero
   bool4(1:la_topo%net%nn) = .true.
   do l = 1, ltra_size
      write(lTra,'(a)') ligne_tra(l)
   enddo
   write(lTra,9100)
   do ib = 1,la_topo%net%nb !parcours du réseau dans l'ordre des données
      vres = vres+vb(ib)
      call ecr0(chain,vb(ib),k0)
      ! NOTE: La ligne suivante permet d'écrire le volume du bief en gardant les décimales, par exemple si le volume est petit.
      !write(chain,'(f14.3)') vb(ib) ; k0 = 14-4-floor(log10(vb(ib)))
   if (su(ib)>zero .or. vo(ib)>zero .or. .not.is_zero(vd(total,ib))) then
         call ecr0(chain1,vd(total,ib),k)
         if (su(ib) < 1.e7_long) then
            write(lTra,9300) ib,chain(k0:),su(ib)*1.e-4_long,vo(ib),chain1(k:)
         else
            write(lTra,9301) ib,chain(k0:),su(ib)*1.e-4_long,vo(ib),chain1(k:)
         endif
      else
         if (chain(1:1) == '!') then
            km3 = ' 10**6 m3'
          else
            km3 = ' m3'
         endif
         write(lTra,9310) ib,chain(k0:),km3
      endif
      do ijk = 1, 2
         select case (ijk)
         case (1)  !Résultats au noeud amont du bief
            is1 = la_topo%biefs(ib)%is1
            if (ism_OK) then
               zn = z_ism(is1)  ;  qn = q_total(q_ism(is1))  ;  nn = la_topo%biefs(ib)%nam
            else
               zn = zt(is1)     ;  qn = qt(is1)              ;  nn = la_topo%biefs(ib)%nam
            endif
         case (2)  !Résultats au noeud aval du bief
            is2 = la_topo%biefs(ib)%is2
            if (ism_OK) then
               zn = z_ism(is2)  ;  qn = q_total(q_ism(is2))  ;  nn = la_topo%biefs(ib)%nav
            else
               zn = zt(is2)     ;  qn = qt(is2)              ;  nn = la_topo%biefs(ib)%nav
            endif
         case default
            call do_crash('Ecrire()') ; stop 1
         end select
         vn = aire_noeud(la_topo%nodes(nn),zn)*0.0001_long  ;  volu = volume_noeud(la_topo%nodes(nn),zn)
         if (bool4(nn)) then
            bool4(nn) = .false.
            vres = vres+volu
         endif
         if (vn>zero) then
            call ecr0(chain,volu,k)
            k0 = 15-k
            chain1(1:k0) = chain(k:14)  ;  chain1(k0+1:k0+4) = ' m3)'
            if (k0+4<14) then
               do k = k0+5,14  ;  chain1(k:k) = ' '  ;  enddo
            endif
            if (vn < 1000._long) then
               if (qn > -1000._long .and. qn < 10000._long) then
                  write(lTra,9000) la_topo%nodes(nn)%name,zn,vn,chain1,qn
               else
                  write(lTra,9010) la_topo%nodes(nn)%name,zn,vn,chain1,qn
               endif
            else
               if (qn > -1000._long .and. qn < 10000._long) then
                  write(lTra,9001) la_topo%nodes(nn)%name,zn,vn,chain1,qn
               else
                  write(lTra,9011) la_topo%nodes(nn)%name,zn,vn,chain1,qn
               endif
            endif
         else
            if (qn > -1000._long .and. qn < 10000._long) then
               write(lTra,9200) la_topo%nodes(nn)%name,zn,qn
            else
               write(lTra,9201) la_topo%nodes(nn)%name,zn,qn
            endif
         end if
      enddo
   enddo
   write(lTra,9100)
   call ecr0(chain,vres,k)
   if (chain(1:1) == '!') then
      km3 = ' 10**6 m3'
    else
      km3 = ' m3'
   endif
   write(lTra,'(3a)') ' Volume total du réseau : ',chain(k:),km3
   write(lTra,9100)

   !---Recherche d'un éventuel déversement latéral
   if (.not.devlat(RivG) .and. .not.devlat(RivD)) then
      return
   else
      !---Impression sur etude.TRA du débit perdu par zone de déversement latéral
      do ib = 1, la_topo%net%nb
         do rive = rivg,rivd
            jsa = 0 !index de début d'un secteur déversant
            do is = la_topo%biefs(ib)%is1, la_topo%biefs(ib)%is2
               if (abs(qds1(rive,is)) > 1.e-07_long) then
                  if (jsa /= 0) then             !-->on est dans un secteur déversant
                     if (nclat(rive,is)/=nclat(rive,jsa) .and. nclat(rive,is)>0 .and. nclat(rive,jsa)>0) then
                        ! on passe à un secteur déversant en casier contigu : écriture du 1er
                        neu = nclat(rive,jsa)  ;  xsa = xgeo(jsa)  ;  xsb = xgeo(is)
                        call ecrdev(neu, ib, xsa, xsb, qdev, rive)
                        ! début du secteur déversant suivant contigu
                        qdev = qds1(rive,is)*abs(xgeo(is)-xgeo(is+1))
                        jsa = is  !mise à jour de jsa=index début secteur
                     else
                        qdev=qdev+qds1(rive,is)*abs(xgeo(is)-xgeo(is+1))
                     endif
                  else                           !-->début d'un secteur déversant
                     jsa = is
                     qdev = qds1(rive,is)*abs(xgeo(is)-xgeo(is+1))
                  endif
               elseif (jsa /= 0) then  !on est sorti d'un secteur déversant : écriture sur .tra
                  neu = nclat(rive,jsa)  ;  xsa = xgeo(jsa)  ;  xsb = xgeo(is)
                  call ecrdev(neu, ib, xsa, xsb, qdev, rive)
                  jsa=0  ! remise à zéro pour secteur suivant
               endif
            enddo
         enddo
      enddo
      write(lTra,9100)
      volu = qclat(0)
      call ecr0(chain,volu,k)
      write(lTra,'(3a)') ' Volume total déversé hors réseau : ', CHAIN(K:),' m3'
      write(lTra,9100)
   endif

   9000 format(4x,'Noeud : ',A3,' Cote : ',F8.3,' (',F6.2,' ha *** ',A14,'  Débit : ',F8.3)
   9010 format(4x,'Noeud : ',A3,' Cote : ',F8.3,' (',F6.2,' ha *** ',A14,'  Débit : ',F9.3)
   9001 format(4x,'Noeud : ',A3,' Cote : ',F8.3,' (',F6.1,' ha *** ',A14,'  Débit : ',F8.3)
   9011 format(4x,'Noeud : ',A3,' Cote : ',F8.3,' (',F6.1,' ha *** ',A14,'  Débit : ',F9.3)
   9100 format(1x,79('='))
   9200 format(4x,'Noeud : ',A3,' Cote : ',F8.3,31X,' Débit : ',F8.3)
   9201 format(4x,'Noeud : ',A3,' Cote : ',F8.3,31X,' Débit : ',F9.3)
   9300 format(1x,'BIEF ',I3,' : ',A,' m3 [',F6.2,' ha (',E9.3,' m3) inondés, ',A,' m3 déversés]')
   9301 format(1x,'BIEF ',I3,' : ',A,' m3 [',F7.1,' ha (',E9.3,' m3) inondés, ',A,' m3 déversés]')
   9310 format(1x,'BIEF ',I3,' : ',2A,57X)
   deallocate(q,z)
end subroutine Ecrire
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine EcrDev(NEU, KB, XSA, XSB, QDEV, rive)
!==============================================================================
!              Ecriture des déversements
!==============================================================================
   use parametres, only: long, rivg, rivd, zero, lTra
   use, intrinsic :: iso_fortran_env, only: error_unit

   use TopoGeometrie, only: la_topo, xgeo, numero_bief
   implicit none
   !---prototype
   integer :: neu, kb, rive
   real(kind=long) :: xsa, xsb, qdev
   !---variables locales temporaires
   integer :: ks1, jb
   real(kind=long) :: xs1
   character(len=4) :: riv
   character(len=180) :: err_message
   !-------------------------------------------------------------------------------
   if (rive == rivg) then
      riv = ' RG '
   elseif (rive == rivd) then
      riv = ' RD '
   else
      write(err_message,'(a)') '>>>> erreur de rive dans EcrDev <<<<'
      write(lTra,'(a)') trim(err_message)  ;  write(error_unit,'(a)') trim(err_message)
      write(err_message,'(a)') 'Merci d''envoyer un rapport de bug'
      write(lTra,'(a)') trim(err_message)  ;  write(error_unit,'(a)') trim(err_message)
      stop 119
   endif
   if (neu > 0) then
      if ( qdev > zero) then
         write(lTra,'(a,i3.3,2(a,f8.2),a,f7.3,2a)') ' Bief ',KB,' Pm ',XSA,' a ',XSB, &
                          ' déverse ',QDEV,' m3/s vers ',la_topo%nodes(neu)%name
       else
         write(lTra,'(a,i3.3,2(a,f8.2),a,f7.3,2a)') ' Bief ',KB,' Pm ',XSA,' a ',XSB,  &
                        ' reçoit ',-QDEV,' m3/s depuis ',la_topo%nodes(neu)%name
      endif
   elseif (neu<0) then
      ks1 = -neu  ;  xs1 = xgeo(ks1)  ;  jb = numero_bief(ks1)
      if (qdev > zero) then
         write(lTra,'(a,i3.3,a,2(a,f8.2),a,f7.3,a,i3.3,a,f7.1)') ' Bief ',KB,riv,' Pm ',XSA,' a ',XSB,  &
                                                       ' déverse ',QDEV,' m3/s vers bief ',JB,':',XS1
       else
         write(lTra,'(a,i3.3,a,2(a,f8.2),a,f7.3,a,i3.3,a,f7.1)') ' Bief ',KB,riv,' Pm ',XSA,' a ',XSB,  &
                                                     ' reçoit ',-QDEV,' m3/s depuis bief ',JB,':',XS1
      endif
   else
      write(lTra,'(a,i3.3,a,2(a,f8.2),a,f7.3,a)') ' Bief ',KB,riv,' Pm ',XSA,' a ',XSB,  &
                                                  ' déverse ',QDEV,' m3/s hors réseau'
   endif
end subroutine EcrDev
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
#ifdef mpi
#ifdef melissa
subroutine EcrMelissa()
!==============================================================================
!                 Envoie des donnees a Melissa
!
! NOTE: Les temps sont écrits en double précision dans BIN
! NOTE: c'est nécessaire pour stocker correctement les grands temps en secondes
! NOTE: Mage_Extraire assure la rétro-compatibilité avec les fichiers BIN anciens.
!==============================================================================
   use, intrinsic :: iso_fortran_env, only: output_unit, real64
   use parametres, only: long, sp, i_ism, i_debord, i_mixte, i_mixte2, zero
   use data_num_fixes, only: fp_model, long_bin
   use data_num_mobiles, only: t, ISM_calcul_OK
   use hydraulique, only: qt, zt, vt
   use objet_section, only: profil
   use TopoGeometrie, only: la_topo, zfd
   use StVenant_ISM, only: q_total, q_ism => q, z_ism => z, qml_ism => qml, qmr_ism => qmr
   use StVenant_Debord, only: debord
   use mage_utilitaires, only: is_zero
   use iso_c_binding, only: c_null_char
   implicit none
   ! -- variables --
   integer :: is
   real(kind=long), dimension(:), allocatable :: q,v,z,qm,ql,qr,vm,vl,vr
   real(kind=long) :: qmin, qmoy, vmin, vmoy, deb, beta, akmin, akmoy
   real(kind=long), dimension(:), allocatable :: qml, qmr
   real(kind=long), parameter :: jours = 86400._long
   integer,pointer :: ns
   type(profil),pointer :: prfl

   include "melissa_api.f90"

   ns => la_topo%net%ns
   if (long_BIN) then
      allocate(q(ns), &
             & v(ns), &
             & z(ns), &
             & qm(ns), &
             & ql(ns), &
             & qr(ns), &
             & vm(ns), &
             & vl(ns), &
             & vr(ns), &
             & qml(ns), &
             & qmr(ns))
      if (fp_model == i_debord) then
         do is = 1, ns  !les sections sont dans l'ordre des données
            prfl => la_topo%sections(is)
            q(is) = qt(is)
            z(is) = zt(is)
            v(is) = vt(is)
            akmin = prfl%ks(prfl%main)
            akmoy = prfl%strickler_lit_moyen(zt(is))
            call debord(is,zt(is)-zfd(is),qt(is),akmin,akmoy,Qmin, Qmoy, Vmin, Vmoy, Deb, beta)
            ql(is) = 0.5 * Qmoy
            qr(is) = 0.5 * Qmoy
            qm(is) = q(is) - ql(is) - qr(is)
            Vl(is) = Vmoy
            Vm(is) = Vmin
            Vr(is) = Vmoy
            qml(is)= 0._sp
            qmr(is)= 0._sp
         enddo
      elseif ((fp_model == i_mixte .or. fp_model == i_mixte2) .and. .not.ISM_calcul_OK) then
         !mixte + échec ISM avec bascule sur Debord
         do is = 1, ns  !les sections sont dans l'ordre des données
            prfl => la_topo%sections(is)
            q(is) = qt(is)
            z(is) = zt(is)
            v(is) = vt(is)
            akmin = prfl%ks(prfl%main)
            akmoy = prfl%strickler_lit_moyen(zt(is))
            call debord(is,zt(is)-zfd(is),qt(is),akmin,akmoy,Qmin, Qmoy, Vmin, Vmoy, Deb, beta)
            qm(is) = Qmin
            ql(is) = 0.5 * Qmoy
            qr(is) = 0.5 * Qmoy
            vm(is) = Vmin
            vl(is) = Vmoy
            vr(is) = Vmoy
            qml(is)= 0._sp
            qmr(is)= 0._sp
         enddo
      else
         !ISM réussi
         do is = 1, ns  !les sections sont dans l'ordre des données
            prfl => la_topo%sections(is)
            q(is) = q_total(q_ism(is))
            z(is) = z_ism(is)
            v(is) = q_total(q_ism(is))/prfl%section_mouillee(zt(is))
            qm(is) = q_ism(is)%qm
            ql(is) = q_ism(is)%ql
            qr(is) = q_ism(is)%qr
            vm(is) = q_ism(is)%qm/prfl%section_mouillee(z_ism(is),2)
            if (is_zero(prfl%section_mouillee(z_ism(is),1))) then
               vl(is) = 0._sp
            else
               vl(is) = q_ism(is)%ql/prfl%section_mouillee(z_ism(is),1)
            endif
            if (is_zero(prfl%section_mouillee(z_ism(is),3))) then
               vr(is) = 0._sp
            else
               vr(is) = q_ism(is)%qr/prfl%section_mouillee(z_ism(is),3)
            endif
            qml(is)= qml_ism(is)
            qmr(is)= qmr_ism(is)
         enddo
      endif
      call melissa_send("Q"//c_null_char,q)
      call melissa_send("Z"//c_null_char,z)
      call melissa_send("S"//c_null_char,v)
      call melissa_send("L"//c_null_char,ql)
      call melissa_send("M"//c_null_char,qm)
      call melissa_send("R"//c_null_char,qr)
      call melissa_send("U"//c_null_char,vl)
      call melissa_send("V"//c_null_char,vm)
      call melissa_send("W"//c_null_char,vr)
      call melissa_send("G"//c_null_char,qml)
      call melissa_send("D"//c_null_char,qmr)

      deallocate(q,v,z,qm,ql,qr,vm,vl,vr,qml,qmr)
   else
      if (fp_model == i_debord) then
         write(output_unit,'(a)')"melissa_send"
         call melissa_send("Q"//c_null_char,qt)
         call melissa_send("Z"//c_null_char,zt)
      elseif ((fp_model == i_mixte .or. fp_model == i_mixte2) .and. .not.ISM_calcul_OK) then
         !mixte + échec ISM avec bascule sur Debord
         call melissa_send("Q"//c_null_char,qt)
         call melissa_send("Z"//c_null_char,zt)
      else
         !ISM réussi
         allocate(q(ns))
         do is = 1, ns  !les sections sont dans l'ordre des données
            q(is) = q_total(q_ism(is))
         enddo
         call melissa_send("Q"//c_null_char,q)
         call melissa_send("Z"//c_null_char,z_ism)
         deallocate(q)
      endif

   endif

end subroutine EcrMelissa
#endif
#endif
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine EcrBin(UBin)
!==============================================================================
!                 Ecriture du fichier BIN
!
! NOTE: Les temps sont écrits en double précision dans BIN
! NOTE: c'est nécessaire pour stocker correctement les grands temps en secondes
! NOTE: Mage_Extraire assure la rétro-compatibilité avec les fichiers BIN anciens.
!==============================================================================
   use, intrinsic :: iso_fortran_env, only: real64
   use parametres, only: long, sp, i_ism, i_debord, i_mixte, i_mixte2, zero
   use data_num_fixes, only: fp_model, long_bin
   use data_num_mobiles, only: t, dt, ISM_calcul_OK
   use hydraulique, only: qt, zt, vt
   use objet_section, only: profil
   use TopoGeometrie, only: la_topo, zfd
   use StVenant_ISM, only: q_total, q_ism => q, z_ism => z, qml_ism => qml, qmr_ism => qmr
   use StVenant_Debord, only: debord
   use mage_utilitaires, only: is_zero
   implicit none
   ! -- prototype --
   integer,intent(in) :: ubin
   ! -- variables --
   integer :: is
   real(kind=sp), dimension(:), allocatable :: q,v,z,qm,ql,qr,vm,vl,vr
   real(kind=long) :: qmin, qmoy, vmin, vmoy, deb, beta, akmin, akmoy
   real(kind=real64) :: tr
   real(kind=sp), dimension(:), allocatable :: qml, qmr
   real(kind=long), parameter :: jours = 86400._long
   integer,pointer :: ismax
   type(profil),pointer :: prfl

   ismax => la_topo%net%ns
   allocate(q(ismax), &
          & v(ismax), &
          & z(ismax), &
          & qm(ismax), &
          & ql(ismax), &
          & qr(ismax), &
          & vm(ismax), &
          & vl(ismax), &
          & vr(ismax), &
          & qml(ismax), &
          & qmr(ismax))
   if (long_BIN) then
      if (fp_model == i_debord) then
         do is = 1, ismax  !les sections sont dans l'ordre des données
            prfl => la_topo%sections(is)
            q(is) = real(qt(is),kind=sp)
            z(is) = real(zt(is),kind=sp)
            v(is) = real(vt(is),kind=sp)
            akmin = prfl%ks(prfl%main)
            akmoy = prfl%strickler_lit_moyen(zt(is))
            call debord(is,zt(is)-zfd(is),qt(is),akmin,akmoy,Qmin, Qmoy, Vmin, Vmoy, Deb, beta)
            ql(is) = 0.5 * real(Qmoy,kind=sp)
            qr(is) = 0.5 * real(Qmoy,kind=sp)
            qm(is) = q(is) - ql(is) - qr(is)
            Vl(is) = real(Vmoy,kind=sp)
            Vm(is) = real(Vmin,kind=sp)
            Vr(is) = real(Vmoy,kind=sp)
            qml(is)= 0._sp
            qmr(is)= 0._sp
         enddo
      elseif ((fp_model == i_mixte .or. fp_model == i_mixte2) .and. .not.ISM_calcul_OK) then
         !mixte + échec ISM avec bascule sur Debord
         do is = 1, ismax  !les sections sont dans l'ordre des données
            prfl => la_topo%sections(is)
            q(is) = real(qt(is),kind=sp)
            z(is) = real(zt(is),kind=sp)
            v(is) = real(vt(is),kind=sp)
            akmin = prfl%ks(prfl%main)
            akmoy = prfl%strickler_lit_moyen(zt(is))
            call debord(is,zt(is)-zfd(is),qt(is),akmin,akmoy,Qmin, Qmoy, Vmin, Vmoy, Deb, beta)
            qm(is) = real(Qmin,kind=sp)
            ql(is) = 0.5 * real(Qmoy,kind=sp)
            qr(is) = 0.5 * real(Qmoy,kind=sp)
            vm(is) = real(Vmin,kind=sp)
            vl(is) = real(Vmoy,kind=sp)
            vr(is) = real(Vmoy,kind=sp)
            qml(is)= 0._sp
            qmr(is)= 0._sp
         enddo
      else
         !ISM réussi
         do is = 1, ismax  !les sections sont dans l'ordre des données
            prfl => la_topo%sections(is)
            q(is) = real(q_total(q_ism(is)),kind=sp)
            z(is) = real(z_ism(is),kind=sp)
            v(is) = real(q_total(q_ism(is))/prfl%section_mouillee(zt(is)),kind=sp)
            qm(is) = real(q_ism(is)%qm,kind=sp)
            ql(is) = real(q_ism(is)%ql,kind=sp)
            qr(is) = real(q_ism(is)%qr,kind=sp)
            vm(is) = real(q_ism(is)%qm/prfl%section_mouillee(z_ism(is),2),kind=sp)
            if (is_zero(prfl%section_mouillee(z_ism(is),1))) then
               vl(is) = 0._sp
            else
               vl(is) = real(q_ism(is)%ql/prfl%section_mouillee(z_ism(is),1),kind=sp)
            endif
            if (is_zero(prfl%section_mouillee(z_ism(is),3))) then
               vr(is) = 0._sp
            else
               vr(is) = real(q_ism(is)%qr/prfl%section_mouillee(z_ism(is),3),kind=sp)
            endif
            qml(is)= real(qml_ism(is),kind=sp)
            qmr(is)= real(qmr_ism(is),kind=sp)
         enddo
      endif
      tr = t-dt
      write(ubin) ismax,tr,'Q',(q(is),is=1,ismax)
      write(ubin) ismax,tr,'Z',(z(is),is=1,ismax)
      write(ubin) ismax,tr,'S',(v(is),is=1,ismax)
      write(ubin) ismax,tr,'L',(ql(is),is=1,ismax)
      write(ubin) ismax,tr,'M',(qm(is),is=1,ismax)
      write(ubin) ismax,tr,'R',(qr(is),is=1,ismax)
      write(ubin) ismax,tr,'U',(vl(is),is=1,ismax)
      write(ubin) ismax,tr,'V',(vm(is),is=1,ismax)
      write(ubin) ismax,tr,'W',(vr(is),is=1,ismax)
      write(ubin) ismax,tr,'G',(qml(is),is=1,ismax)
      write(ubin) ismax,tr,'D',(qmr(is),is=1,ismax)
!       if (with_charriage > 0) then
!          write(ubin) ismax,tr,'G',((la_topo%sections(is)%xyz(ip),is=1,la_topo%net%ismax),ip=1,la_topo%sections(is)%np)
!       endif

   else

      if (fp_model == i_debord) then
         do is = 1, ismax  !les sections sont dans l'ordre des données
            z(is) = real(zt(is),kind=sp)
            q(is) = real(qt(is),kind=sp)
         enddo
      elseif ((fp_model == i_mixte .or. fp_model == i_mixte2) .and. .not.ISM_calcul_OK) then
         !mixte + échec ISM avec bascule sur Debord
         do is = 1, ismax  !les sections sont dans l'ordre des données
            q(is) = real(qt(is),kind=sp)
            z(is) = real(zt(is),kind=sp)
         enddo
      else
         !ISM réussi
         do is = 1, ismax  !les sections sont dans l'ordre des données
            q(is) = real(q_total(q_ism(is)),kind=sp)
            z(is) = real(z_ism(is),kind=sp)
         enddo
      endif
      tr = t-dt
      write(ubin) ismax,tr,'Q',(q(is),is=1,ismax)
      write(ubin) ismax,tr,'Z',(z(is),is=1,ismax)

   endif
   deallocate(q,v,z,qm,ql,qr,vm,vl,vr,qml,qmr)

end subroutine ecrbin

subroutine ecrgra(UBin)
   use, intrinsic :: iso_fortran_env, only: error_unit, output_unit, real64
   use hydraulique, only: zt
   use data_num_mobiles, only: t, dt
   use objet_point, only: point3D
   use TopoGeometrie, only: la_topo, with_charriage
   use charriage, only: compartiment_sedimentaire, porosity, rhos
   use Parametres, only: long
   implicit none
   ! -- prototype --
   integer,intent(in) :: ubin
   ! -- variables --
   integer :: is, ip, ics, ncs
   real(kind=real64) :: tr, h, d
   real(kind=long) :: toit
   type(point3D), pointer :: xyz

   tr = t-dt
   write(ubin) la_topo%net%ns,tr
   do is=1, la_topo%net%ns
      if (with_charriage == 1) then
         write(ubin) la_topo%sections(is)%np
         do ip=1, la_topo%sections(is)%np
           xyz => la_topo%sections(is)%xyz(ip)
           write(ubin) xyz%nbcs
           do ics=1, xyz%nbcs
              if (ics == 1) then ! couche active
                 h = xyz%z - xyz%cs(ics)%zc
              else
                 h = xyz%cs(ics-1)%zc - xyz%cs(ics)%zc
              endif
              write(ubin) h, xyz%cs(ics)%d50, xyz%cs(ics)%sigma
           enddo
         enddo
      else
         write(ubin) 1
         write(ubin) la_topo%sections(is)%nbcs
         do ics=1, la_topo%sections(is)%nbcs
            write(ubin) la_topo%sections(is)%cs(ics)%hc, &
                        la_topo%sections(is)%cs(ics)%d50, &
                        la_topo%sections(is)%cs(ics)%sigma
         enddo
      endif
   enddo

end subroutine ecrgra

subroutine ecrgra2(UBin)
   use, intrinsic :: iso_fortran_env, only: error_unit, output_unit, real64
   use hydraulique, only: zt
   use data_num_mobiles, only: t, dt
   use objet_point, only: point3D
   use TopoGeometrie, only: la_topo, with_charriage
   use charriage, only: compartiment_sedimentaire, porosity, rhos
   use Parametres, only: long
   implicit none
   ! -- prototype --
   integer,intent(in) :: ubin
   ! -- variables --
   integer :: is, ip, ics, ncs
   real(kind=real64) :: tr, h, d
   real(kind=long) :: toit
   type(point3D), pointer :: xyz

   tr = t-dt
   write(ubin) la_topo%net%ns,tr,with_charriage
   if (with_charriage == 1) then ! aux noeuds
      write(ubin) (la_topo%sections(is)%np, is=1,la_topo%net%ns)
      write(ubin) ((la_topo%sections(is)%xyz(ip)%nbcs, ip=1, la_topo%sections(is)%np), &
                                                       is=1, la_topo%net%ns)
      write(ubin) (((la_topo%sections(is)%xyz(ip)%cs(ics)%zc, &
                     la_topo%sections(is)%xyz(ip)%cs(ics)%d50, &
                     la_topo%sections(is)%xyz(ip)%cs(ics)%sigma, ics=1, la_topo%sections(is)%xyz(ip)%nbcs), &
                                                                 ip=1, la_topo%sections(is)%np), &
                                                                 is=1, la_topo%net%ns)
   else ! aux sections
      write(ubin) (la_topo%sections(is)%nbcs, is=1, la_topo%net%ns)
      write(ubin) ((la_topo%sections(is)%cs(ics)%hc, &
                    la_topo%sections(is)%cs(ics)%d50, &
                    la_topo%sections(is)%cs(ics)%sigma, ics = 1, la_topo%sections(is)%nbcs), &
                                                        is=1, la_topo%net%ns)
   endif

end subroutine ecrgra2

!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine Envlop(NomEnv)
!==============================================================================
!   Ecriture des valeurs enveloppe pour Q, Z et V et des dates correspondantes
!
!==============================================================================
   use mage_utilitaires, only: heure
   use parametres, only: long, total, rivg, rivd, lname, zero
   use data_num_fixes, only: tmax, encodage
   use data_num_mobiles, only: t
   use casiers, only: qds1, ydev, cdlat
   use mage_results, only: type_resultat, largeur_totale, largeur_mineur, z_max, qtot, qfp, pm, z_fd
   use premierappel, only : nap_envlop
   use TopoGeometrie, only: la_topo, zfond, alfn, xgeo, zfd, profil
   implicit none
   ! -- le prototype --
   character(len=lname),intent(in) :: nomenv
   ! -- les constantes --
   integer, parameter :: nl=168
   ! -- les variables --
   integer :: ib,is,rive, ltmp
   real(kind=long),allocatable,dimension(:) :: q,z,v,y
   real(kind=long) :: tyet,hauteur,largeur_miroir,zdev
   real(kind=long),allocatable,dimension(:),save :: zenv, qenv, venv
   real(kind=long),allocatable,dimension(:),save :: tzenv, tqenv, tvenv
   real(kind=long),allocatable,dimension(:),save :: qdenv, tqdenv
   character :: debt*2, devt*2, hq*17, hz*17, hv*17, hqd*17
   character(len=nl) :: ligne,ligne0
   integer, pointer :: ismax
   type(profil), pointer :: prof
   ! --  les appels --
   interface
      subroutine copier(tyet,q,z,y,v)
         use parametres, only: long
         real(kind=long),intent(out) :: tyet
         real(kind=long),dimension(:),intent(out) :: q,z    ! ligne d'eau
         real(kind=long),dimension(:),intent(out),optional :: y,v    ! ligne d'eau
      end subroutine copier
   end interface

   ismax => la_topo%net%ns
   allocate(q(ismax),z(ismax),v(ismax),y(ismax))
   ! TODO deallocate all these arrays somewhere
   if(.not.allocated(zenv))allocate(zenv(ismax))
   if(.not.allocated(qenv))allocate(qenv(ismax))
   if(.not.allocated(venv))allocate(venv(ismax))
   if(.not.allocated(tzenv))allocate(tzenv(ismax))
   if(.not.allocated(tqenv))allocate(tqenv(ismax))
   if(.not.allocated(tvenv))allocate(tvenv(ismax))
   if(.not.allocated(qdenv))allocate(qdenv(ismax))
   if(.not.allocated(tqdenv))allocate(tqdenv(ismax))
   if (nap_envlop < 1) then
   ! -- Initialisation de Qenv et Zenv
      Zenv(1:ismax)  = zero ; Qenv(1:ismax)   = zero
      Venv(1:ismax)  = zero ; QDenv(1:ismax)  = zero
      TZenv(1:ismax) = zero ; TQenv(1:ismax)  = zero
      TVenv(1:ismax) = zero ; TQDenv(1:ismax) = zero
      nap_envlop = nap_envlop+1
   endif
   call copier(tyet,q,z,y,v)

   !---Recherche des enveloppes (fait à chaque pas de temps)
   do is = 1, ismax
      !cote enveloppe
      if (abs(Z(is)) > Zenv(is)) then
         Zenv(is) = Z(is)
         TZenv(is) = T
      endif
      !débit enveloppe
      if (abs(Q(is)) > Qenv(is)) then
         Qenv(is) = Q(is)
         TQenv(is) = T
      endif
      !Vitesse enveloppe
      if (abs(V(is)) > Venv(is)) then
         Venv(is) = V(is)
         TVenv(is) = T
      endif
      !Qdev enveloppe
      if (abs(Qds1(Total,is)) > QDenv(is)) then
         QDenv(is) = Qds1(Total,is)
         TQDenv(is) = T
      endif
   enddo

   !---Ecriture des enveloppes si T > Tmax
   if (T > Tmax-1._Long) then
      !Entête du fichier
      do ib = 1, nl
         ligne0(ib:ib) = '*'
      enddo
      open(newunit=ltmp,file=nomenv,status='unknown',form='formatted',encoding=encodage)
      write(Ltmp,'(a)') Ligne0
      Ligne = '* Enveloppe des Débits, Cotes et Vitesses moyennes calculés par MAGE'
      write(Ltmp,'(a)') Ligne
      Ligne = ' ' ; Ligne(1:1) = '*'
      write(Ltmp,'(a)') Ligne
      Ligne = '* un # à coté d''une cote d''eau indique un débordement en lit moyen'
      write(Ltmp,'(a)') Ligne
      Ligne = '* un ! à coté d''une cote d''eau indique un dépassement de la cote de berge la plus basse'
      write(Ltmp,'(a)') Ligne
      write(Ltmp,'(a)') Ligne0
      write(ltmp,1001)
      !Valeurs enveloppes et dates correspondantes
      do ib = 1, la_topo%net%nb
         do is = la_topo%biefs(ib)%is1, la_topo%biefs(ib)%is2
            prof => la_topo%sections(is)
            Hauteur = Zenv(is)-Zfond(is)
            if (Hauteur > prof%Ymoy) then
               Debt = ' #'  ! débordement en lit moyen
             else
               Debt = '  '
            endif
            if (Hauteur > prof%Ybmin) then
               Devt = ' !'  ! dépassement de la cote de berge la plus basse
             else
               Devt = '  '
            endif
            Largeur_miroir = alfn(Hauteur,is)              !largeur au miroir
            if (is < la_topo%biefs(ib)%is2) then
               QDenv(is) = QDenv(is)*abs(xgeo(is+1)-xgeo(is)) ! débit déversé
            else
               QDenv(is) = zero
            endif
            HQ = Heure(TQenv(is))
            HZ = Heure(TZenv(is))
            HV = Heure(TVenv(is))
            HQD = Heure(TQDenv(is))
            Zdev = 1.e+6_Long
            do rive = rivg,rivd
               if (CDlat(rive,is) > 0.0001_Long) then
                  Zdev = min(Zdev,Zfond(is)+Ydev(rive,is))  ! cote de déversement
               else
                  Zdev = min(Zdev,Zfond(is)+prof%YBmin)       ! cote de berge
               endif
            enddo
            if (xgeo(is) < 100000._long) then
               write(ltmp,1000) ib,is-la_topo%biefs(ib)%is1+1,xgeo(is),zfond(is),zfond(is)+prof%ymoy,zdev, &
                     zenv(is),debt,devt,largeur_miroir,hz,qenv(is),hq,venv(is),hv,qdenv(is),hqd,prof%almy
            else
               write(ltmp,1002) ib,is-la_topo%biefs(ib)%is1+1,xgeo(is),zfond(is),zfond(is)+prof%ymoy,zdev, &
                     zenv(is),debt,devt,largeur_miroir,hz,qenv(is),hq,venv(is),hv,qdenv(is),hqd,prof%almy
            endif
            ! sauvegarde des résultats dans Mage_Results
            if (type_resultat == 'ENV') then
               pm(is) = xgeo(is)
               z_fd(is) = zfd(is)
               largeur_totale(is) = largeur_miroir
               largeur_mineur(is) = prof%almy
               z_max(is) = zenv(is)
               qtot(is) = qenv(is)
               qfp(is) = -9999.999_long  !données absente
            endif
         enddo
      enddo
      close(ltmp)
   endif

   1000 format(1x,i3,1x,i3,1x,f8.2,3(1x,f7.2,1x),4x,1x,f8.3,2a2,1x, &
             f7.2,1x,a,1x,f9.3,1x,a,1x,f7.3,1x,a,1x,f7.3,1x,a,f7.2)
   1002 format(1x,i3,1x,i3,1x,f8.1,3(1x,f7.2,1x),4x,1x,f8.3,2a2,1x, &
             f7.2,1x,a,1x,f9.3,1x,a,1x,f7.3,1x,a,1x,f7.3,1x,a,f7.2)
   1001 format('*',' IB','  IS','    Pm','     Z_Fond','  Z_moyen','  Z_Berge',4X,'     Cote',  &
             '      Largeur','   Date_Cote','       Débit','    Date_Débit','         V',     &
             '     Date_Vmax','       Qdev','   Date_QDmax','    L_mineur')
   deallocate(q,z,v,y)
end subroutine Envlop



subroutine info_performance(debut,values0)
   !==============================================================================
   !                   Fin du comptage du temps de calcul
   !        et écriture du nombre total d'itérations et pas de temps
   !==============================================================================
   use parametres, only: long, zero, lTra
   use data_num_mobiles, only: niter,npas
   use mage_results, only: cpu_total
   implicit none
   ! -- Prototype --
   integer,intent(in) :: values0(8)
   real(kind=long), intent(in) :: debut
   ! -- Variables --
   integer :: values(8)
   integer :: annee0,mois0,jour0,heure0,minute0,seconde0,centieme0
   integer :: annee1,mois1,jour1,heure1,minute1,seconde1,centieme1
   integer :: heure,minute,seconde
   integer :: heure_cpu,minute_cpu,seconde_cpu, mille_cpu
   integer :: timesec0,timesec1,duree,duree_cpu
   character(len=8) :: ddate
   character(len=10) :: time
   character(len=5) :: zone
   real(kind=long) :: fin

   write(lTra,1040) niter,npas

   call date_and_time(ddate,time,zone,values)
   !heure de fin de calcul
   annee1 = values(1)    ;  mois1 = values(2)  ;  jour1 = values(3)
   heure1 = values(5)    ;  minute1 = values(6)
   seconde1 = values(7)  ;  centieme1 = values(8)
   !heure de début de calcul
   annee0 = values0(1)    ;  mois0 = values0(2)  ;  jour0 = values0(3)
   heure0 = values0(5)    ;  minute0 = values0(6)
   seconde0 = values0(7)  ;  centieme0 = values0(8)

   !---Durée apparente du calcul
   heure = heure1+24*(jour1-jour0)  ! changement de jour éventuel
   timesec0 = seconde0+60*minute0+3600*heure0
   timesec1 = seconde1+60*minute1+3600*heure
   duree = timesec1-timesec0
   if (centieme1-centieme0 > 50) duree = duree+1
   heure = duree/3600
   duree = duree - heure*3600
   minute = duree/60
   seconde = duree - minute*60

   !---Durée CPU
   call cpu_time(fin)
   cpu_total = fin - debut                     !durée CPU en secondes décimales
   duree_cpu = floor(cpu_total)                !durée CPU en secondes entières (partie entière de cpu_total)
   mille_cpu = int(1000._long*(cpu_total-real(duree_cpu,kind=long))) !reste en millièmes de secondes

   heure_cpu = duree_cpu/3600                  !nombre d'heures entières
   duree_cpu = duree_cpu-3600*heure_cpu
   minute_cpu = duree_cpu/60                   !nombre de minutes entières
   seconde_cpu = duree_cpu-60*minute_cpu       !nombre de secondes entières
   write(lTra,1030) jour0,mois0,annee0,heure0,minute0,seconde0,heure,minute, &
                      seconde,heure_cpu,minute_cpu,seconde_cpu,mille_cpu

   1030 format(1x,'Date simulation : ',i2.2,'/',i2.2,'/',i4,' (',i2.2,':',i2.2,':',i2.2,')',&
                  ' Durée : ',i2.2,':',i2.2,':',i2.2,' CPU : ',i2.2,':',i2.2,':',i2.2,',',i3.3)
   1040 format(1x,'Nombre total d''itérations : ',i8,/,1x,'Nombre total de pas de temps : ',i8)
end subroutine info_performance



subroutine write_Screen(prefix,ligne,i)
!==============================================================================
!       Ecriture écran : date, Nb itérations, Cr, Résidu
!==============================================================================
   use parametres, only: long, l9
   use, intrinsic :: iso_fortran_env, only: output_unit

   use data_num_fixes, only: silent
   use data_num_mobiles, only: t, dt, cr, iscr, frsup, ifrsup
   use erreurs_stv, only: reste, ireste, reste_sup, t_reste_sup
   use mage_utilitaires, only: heure
   use PremierAppel, only : appel1 => nap_m1436
   use TopoGeometrie, only: numero_bief, xgeo
   implicit none
   ! -- Prototype --
   character(len=*),intent(in) :: prefix
   character(len=100),intent(inout) :: ligne
   integer,intent(in) :: i
   ! -- Variables locales temporaires --
   integer :: values(8), i0, ibrest, ib
   real(kind=long) :: crdt, xcr, xrest
   character(len=19) :: hms
   character(len=132) :: ligne1
   character(len=8) :: ddate
   character(len=10) :: time
   character(len=5) :: zone
   integer :: ibfr
   real(kind=long) :: xfrsup
   integer :: tscreen1, day_shift
   ! -- Variables locales permanentes --
   integer, save :: tscreen0, nblignes, heure0
   integer, parameter :: nbligneSup = 100000
   character(len=180),save :: f3101, f3102, f3103, f3104

   if (appel1) then
      call date_and_time(ddate,time,zone,values)
      tscreen0 = 100*(values(7)+60*values(6)+3600*values(5) )+values(8)
      heure0 = values(5)
      nblignes = 0
      appel1 = .false.
   endif
   ! Limitation de la taille de OUTPUT (utile dans les cas avec torrentiel
   ! c'est à dire avec tout petits donc très nombreux pas de temps)
   if (nblignes > nbLigneSup) then
      rewind(9)
      nbLignes = 1
   endif

   if (reste > reste_sup) then
      reste_sup = reste
      t_reste_sup = t
   endif

   hms = heure(t)
   i0 = max(1,i-09)
   ib = numero_bief(iscr)
   xcr = xgeo(iscr)
   crdt = cr*dt
   if (ireste == 0) then
      ibrest = 0
      xrest = -99999.99_long
   else
      ibrest = numero_bief(ireste)
      xrest = xgeo(ireste)
   endif
   ibfr = numero_bief(ifrsup)
   xfrsup = xgeo(ifrsup)

   f3101 = '(a,a,'' <'',i2.2,''>'',a,''Cr: '',i4,'' ('',' // &
           'i3.3,'':'',f9.2,'') Résidu: '',e9.3,'' ('',i3.3,'':'',f9.2,'')'',' // &
           ' '' Fr: '',f6.3,'' ('',i3.3,'':'',f9.2,'')'')'
   f3102 = '(a,a,'' <'',i2.2,''>'',a,''Cr: '',f4.0,'' ('',' // &
           'i3.3,'':'',f9.2,'') Résidu: '',e9.3,'' ('',i3.3,'':'',f9.2,'')'','// &
           ' '' Fr: '',f6.3,'' ('',i3.3,'':'',f9.2,'')'')'
   f3103 = '(a,a,'' <'',i2.2,''>'',a,''Cr: '',f4.1,'' ('',' // &
           'i3.3,'':'',f9.2,'') Résidu: '',e9.3,'' ('',i3.3,'':'',f9.2,'')'',' // &
           ' '' Fr: '',f6.3,'' ('',i3.3,'':'',f9.2,'')'')'
   f3104 = '(a,a,'' <'',i2.2,''>'',a,''Cr: '',f4.2,'' ('',' // &
           'i3.3,'':'',f9.2,'') Résidu: '',e9.3,'' ('',i3.3,'':'',f9.2,'')'',' // &
           ' '' Fr: '',f6.3,'' ('',i3.3,'':'',f9.2,'')'')'

   if (crdt > 999.9_long) then
      crdt = min(real(crdt,kind=long),9999._long)
      write(ligne1,f3101) prefix,hms,i,ligne(i0:i0+09),int(crdt),ib,xcr,reste,ibrest, &
                         xrest,frsup,ibfr,xfrsup
   elseif (crdt > 99.9_long) then
      write(ligne1,f3102) prefix,hms,i,ligne(i0:i0+09),crdt,ib,xcr,reste,ibrest, &
                         xrest,frsup,ibfr,xfrsup
   elseif (crdt > 9.9_long) then
      write(ligne1,f3103) prefix,hms,i,ligne(i0:i0+09),crdt,ib,xcr,reste,ibrest, &
                         xrest,frsup,ibfr,xfrsup
   elseif (crdt > 0.01_long) then
      write(ligne1,f3104) prefix,hms,i,ligne(i0:i0+09),crdt,ib,xcr,reste,ibrest, &
                         xrest,frsup,ibfr,xfrsup
   else
      crdt = 0.01_long
      write(ligne1,f3104) prefix,hms,i,ligne(i0:i0+09),crdt,ib,xcr,reste,ibrest, &
                         xrest,frsup,ibfr,xfrsup
   endif
   write(l9,'(a)') ligne1(:len_trim(ligne1))
   nblignes = nblignes+1
   !---Écriture à l'écran toutes les 0,5 secondes environ
   if (.not.silent) then
      call date_and_time(ddate,time,zone,values)
      tscreen1 = 100*(values(7)+60*values(6)+3600*values(5))+values(8)
      if (values(5) < heure0) then
         day_shift = 86400
      else
         day_shift = 0
      endif
      if (tscreen1-tscreen0+day_shift > 250) then
         tscreen0 = tscreen1 ; heure0 = values(5)
         write(output_unit,'(a)') ligne1(1:len_trim(ligne1))
      endif
   endif

!---Écriture écran : taux de variations maximaux en Z et Q
   call write_Screen_dZdQ

end subroutine write_Screen



subroutine write_Screen_dZdQ
!==============================================================================
!       Ecriture écran : Taux de variations maximaux en Z et Q
!==============================================================================
   use Parametres, only: long, zero, l9, un
   use Data_Num_Fixes, only: plus
   use Data_Num_Mobiles, only: dt
   use Solution, only: dq, dz
   use mage_utilitaires, only: f0p
   use TopoGeometrie, only: la_topo, numero_bief, xgeo
   implicit none
   ! -- Variables locales temporaires --
   integer :: idzdt, idqdt, is, ibdzdt, ibdqdt
   real(kind=long) :: dzdt, dqdt, xdz, xdq
   !character(len=120) ::  ligne

   ! Initialisations
   dzdt = zero ; idzdt = 1 ; dqdt = zero ; idqdt = 1
   ! Recherche des variations maxi et de leur localisation
   do is = 1, la_topo%net%ns
      if (abs(dz(is)) > abs(dzdt)) then
         dzdt = dz(is) ; idzdt = is
      endif
      if (abs(dq(is)) > abs(dqdt)) then
         dqdt = dq(is) ; idqdt = is
      endif
   enddo
   dzdt = dzdt/dt*1000._long  ;  dqdt = dqdt/dt*1000._long

   xdz = xgeo(idzdt) ; xdq = xgeo(idqdt)
   ibdzdt = numero_bief(idzdt)
   ibdqdt = numero_bief(idqdt)
   write(l9,1000) plus,trim(f0p(dt,3)),trim(f0p(dzdt,3)),ibdzdt,xdz,trim(f0p(dqdt,3)),ibdqdt,xdq

   !write(l9,'(a)') ligne
   1000 format(a1,'    DT = ',a,' dZ/dt = ',a,' mm/s (',i3.3,':',f0.2,') ', &
                  ' dQ/dt = ',a,' l/s/s (',i3.3,':',f0.2,')')

end subroutine write_Screen_dZdQ



subroutine write_TRA_Ouvrages
!==============================================================================
!        Impression sur TRA de résultats pour les ouvrages
!
!-->ATTTENTION : si on tient compte de la vitesse dans le calcul des débits
!                il faut calculer VAM et VAV et les donner en argument
!                à la fonction DEBIT()
!==============================================================================
   use Parametres, only: long, G2, Zero, Un
   use, intrinsic :: iso_fortran_env, only: output_unit

   use Mage_Utilitaires, only: zegal, heure
   use Ouvrages, only: all_OuvCmp, all_OuvEle, debit, debit3, some_ouvrages_writable, regime, ltra_size, ligne_tra
   use hydraulique, only: qt, zt
   use data_num_mobiles, only: t, ttra, niter, dtold
   use erreurs_sing, only: dqmax =>dqsup, tmax =>tsup, vol =>volsng
   use PremierAppel, only : niter0 => niter_m15, first_call => nap_write_TRA_Ouvrages
   use TopoGeometrie, only: la_topo
   use Data_Num_Fixes, only: dtmin, t_final => tmax, tinf, date_format
   implicit none
   ! -- Variables locales temporaires --
   character :: car*6, ctyp*1, hms*19
   real(kind=long) :: t0, qtt, q1, z1, z2, dbith, zdev, h1, cvn, zmarch, zarret, &
                      zz1, zz2, q2, sl, su, vam,ham, vav, hav, dq0
   integer :: ns, iuvnk, it, js, is, ks, k, k2, nk, ink, isref, ib, lt
   integer, pointer :: ibmax, nsmax
   character(len=8) :: zch
   ! -- Variables locales permanentes --
   character(len=79), parameter :: header1 = ':  Nom     :COTE DEV:COTE VAN:COTE AMO:COTE AVA: DEBIT  : DEBIT  :REGIME:TAUX :'
   character(len=79), parameter :: header2 = ':          :   ou   :L.Dev./T:ou REF/P:ou POMPE:TOTAL ou:DEV,VAN,:      :DENOY:'
   character(len=79), parameter :: header3 = ': Ouvrage  :MARCHE/P:ARRET/P :ou Dev/L:ou EAU/L:AMON/P/L:OUT /P/L:      :DBITH:'

   nsmax => la_topo%net%nss
   ibmax => la_topo%net%nb
   !---calcul du volume passant sur les singularités
   if (first_call) then
      do ns = 1, nsmax
         vol(ns) = zero
      enddo
      first_call = .false.
   elseif (t <= t_final) then
      do ns = 1, nsmax
         iuvnk = all_OuvEle(all_OuvCmp(ns)%OuEl(1,1))%iuv
         if (iuvnk /= 3  .and.  iuvnk /= 99  .and.  iuvnk /= 5) then
            vol(ns) = vol(ns) + dtold * qt(all_OuvCmp(ns)%js)
         else if (iuvnk == 5 .or. iuvnk == 3) then
            vol(ns) = vol(ns) + dtold * (qt(all_OuvCmp(ns)%js-1)-qt(all_OuvCmp(ns)%js))
         endif
      enddo
   endif

   !---impression sur listing (.TRA) de l'instant de calcul et du nombre d'itérations
   !---synthèse sur le fonctionnement des ouvrages
   it = niter-niter0 ; niter0 = niter ; t0 = t
   if (zegal(t0,ttra,un)) then  !c'est l'heure d'écrire sur .tra
      hms = heure(t0,dtmin)
      ! NOTE: ligne_tra() et ltra_size sont définis dans le module Mage_Utilitaires
      ltra_size = 0
      do ltra_size = 1,3
         ligne_tra(ltra_size) = ' '
      enddo
      ltra_size = 4
      if (date_format) then
         write(ligne_tra(ltra_size),2002) hms,it
      else
         write(ligne_tra(ltra_size),2001) hms,it
      endif
      if (some_ouvrages_writable) then
         ltra_size = ltra_size+1
         write(ligne_tra(ltra_size),3000)
         ltra_size = ltra_size+1
         write(ligne_tra(ltra_size),'(1x,a)') header1
         ltra_size = ltra_size+1
         write(ligne_tra(ltra_size),'(1x,a)') header2
         ltra_size = ltra_size+1
         write(ligne_tra(ltra_size),'(1x,a)') header3
      endif
   else
      return   !return:ce n'est pas l'heure d'écrire sur TRA
   endif

   !---boucle sur les sections singulières
   iuvnk = -1
   do ib = 1, ibmax
      do js = la_topo%biefs(ib)%is1, la_topo%biefs(ib)%is2
         ns = la_topo%sections(js)%iss
         if (ns <= 0) cycle  !pas d'ouvrage ici
         qtt = zero ;  is = js-1 ; ks = is
         q1 = qt(is)                 ! débit dans la section singulière courante
         z1 = zt(is) ; z2 = zt(js)   ! niveaux amont et aval (routine débit() )
         do k = 1, all_OuvCmp(ns)%ne
            !boucle sur les ouvrages élémentaires de la section singulière courante
            k2 = k-2 ; nk = all_OuvCmp(ns)%OuEl(k,1) ; iuvnk = all_OuvEle(nk)%iuv
            if (iuvnk == 5) then    ! déversoir latéral : niveau aval
               if (all_OuvEle(nk)%irf > 0) then
                  ink = all_OuvEle(nk)%irf ; z2 = zt(ink)
               else
                  z2 = zero
               endif
            endif
            lt = len_trim(all_OuvEle(nk)%cvar)
            if (all_OuvEle(nk)%iuv == 3) then
               dbith = debit3(nk,t)
            else
               dbith = debit(nk,z1,z2)  !débit théorique sur l'ouvrage élémentaire
            endif
            qtt = qtt+dbith  !débit théorique total dans la section
                             !dans le cas de déversoirs latéraux, c'est le débit total prélévé théorique

            !on passe si on ne veut pas d'information sur cet ouvrage
            if (all_OuvEle(nk)%is_writable) then !écriture des infos pour cet ouvrage élémentaire
               !--->régime de fonctionnement de l'ouvrage élémentaire
               call regime(nk,t,z1,z2,dbith,car,cvn)
               !--->cote déversement, cote charge, hauteurs amont et aval
               zdev = all_OuvEle(nk)%uv2
               if (all_OuvEle(nk)%uv3 > 100._long) then
                  zch = '        '
               else
                  write(zch,'(f8.3)') zdev+all_OuvEle(nk)%uv3
               endif
               if (iuvnk == 0) then  !déversoir-orifice à seuil mobile
                  ctyp = 'D'
                  ltra_size = ltra_size+1
                  write(ligne_tra(ltra_size),1000) all_OuvEle(nk)%cvar,zdev,zch,z1,z2,q1,dbith,car,cvn
               else if (iuvnk == 1) then  !clapet ou porte a flot
!                  ctyp = 'C'
!                  ltra_size = ltra_size+1
!                  write(ligne_tra(ltra_size),1000) all_OuvEle(nk)%cvar,zdev,zch,z1,z2,q1,dbith,car,cvn
                  write(output_unit,*) '>>>> ERREUR : cette modélisation des clapets a été abandonnée'
                  write(output_unit,*) '              utilisez plutôt un orifice avec une règle de régulation de type CLAPET'
                  stop 150
               else if (iuvnk == 2) then  !vanne de fond (loi standard)
                  ctyp = 'V'
                  ltra_size = ltra_size+1
                  write(ligne_tra(ltra_size),1000) all_OuvEle(nk)%cvar,zdev,zch,z1,z2,q1,dbith,car,cvn
               else if (iuvnk == 3) then  !pompe
                  ctyp = 'P' ; isref = all_OuvEle(nk)%irf
                  zmarch = all_OuvEle(nk)%za1 ; zarret = all_OuvEle(nk)%za2
                  zz1 = zt(isref) ; zz2 = zt(js)
                  q2 = q1 - qt(js) ! débit prélevé entre is et is+1 ; en principe c'est le débit de la pompe
                  ltra_size = ltra_size+1
                  write(ligne_tra(ltra_size),1001) all_OuvEle(nk)%cvar,zmarch,zarret,zz1,zz2,q1,q2,car,cvn
               else if (iuvnk == 4) then  !vanne de fond (loi simplifiée)
                  ctyp = 'W'
                  ltra_size = ltra_size+1
                  write(ligne_tra(ltra_size),1000) all_OuvEle(nk)%cvar,zdev,zch,z1,z2,q1,dbith,car,cvn
               else if (iuvnk == 5) then  !déversoir latéral
                  ctyp = 'L' ; isref=all_OuvEle(nk)%irf
                  if (isref>0) then
                     zz1 = zt(isref)
                  else
                     zz1 = -999.999_long
                  endif
                  write(zch,'(f8.3)') all_OuvEle(nk)%uv1
                  zz2 = zt(js) ; q2 = q1 - qt(js)
                  ltra_size = ltra_size+1
                  write(ligne_tra(ltra_size),1000) all_OuvEle(nk)%cvar,zdev,zch,zz1,zz2,q1,q2,car,cvn
               else if (iuvnk == 6) then  !déversoir-orifice à ouverture variable
                  ctyp = 'O'
                  ltra_size = ltra_size+1
                  write(ligne_tra(ltra_size),1000) all_OuvEle(nk)%cvar,zdev,zch,z1,z2,q1,dbith,car,cvn
               else if (iuvnk == 7) then  !clapet ou porte à flot inverse
!                  ctyp = 'I'
!                  ltra_size = ltra_size+1
!                  write(ligne_tra(ltra_size),1000) all_OuvEle(nk)%cvar,zdev,zch,z1,z2,q1,dbith,car,cvn
                  write(output_unit,*) '>>>> ERREUR : cette modélisation des clapets inversés a été abandonnée'
                  write(output_unit,*) '              utilisez plutôt un orifice avec une règle de régulation de type CLAPET'
                  stop 150
               else if (iuvnk == 8) then  !buse circulaire partiellement envasée
                  ctyp = 'B'
                  write(zch,'(f8.3)') zdev+all_OuvEle(nk)%uv1-all_OuvEle(nk)%uv3
                  ltra_size = ltra_size+1
                  write(ligne_tra(ltra_size),1000) all_OuvEle(nk)%cvar,zdev,zch,z1,z2,q1,dbith,car,cvn
               else if (iuvnk == 9) then  !ouvrage élémentaire défini par l'utilisateur
                  ctyp = 'X'
                  ltra_size = ltra_size+1
                  write(ligne_tra(ltra_size),1001) all_OuvEle(nk)%cvar,all_OuvEle(nk)%uv1,all_OuvEle(nk)%uv2,z1, &
                                              z2,q1,dbith,car,cvn
               else if (iuvnk == 10) then  !déversoir trapézoïdal
                  ctyp = 'T'
                  h1 = zt(is) - all_OuvEle(nk)%uv2
                  sl = max(zero,all_OuvEle(nk)%uv1 + 2._long*min(all_OuvEle(nk)%uv3,h1)/all_OuvEle(nk)%uv5) !correction jbf 15/02/2008
                  ltra_size = ltra_size+1
                  write(ligne_tra(ltra_size),1001) all_OuvEle(nk)%cvar,zdev,sl,z1,z2,q1,dbith,car,cvn
               else if (iuvnk == 11) then  !Orifice-Voute
                  ctyp = 'F'
                  zdev = all_OuvCmp(ns)%zfs+all_OuvEle(nk)%uv2
                  write(zch,'(f8.3)') zdev+all_OuvEle(nk)%uv3+all_OuvEle(nk)%uv4
                  ltra_size = ltra_size+1
                  write(ligne_tra(ltra_size),1000) all_OuvEle(nk)%cvar,zdev,zch,z1,z2,q1,dbith,car,cvn
               else if (iuvnk == 91) then  !perte de charge singulière à la Borda
                  ctyp = 'A'
                  su = la_topo%sections(is)%surface_mouillee_cote(z1)  ;  vam = q1/su
                  ham = z1+vam*vam/g2
                  su = la_topo%sections(js)%surface_mouillee_cote(z2)  ;  q2 = qt(js)  ;  vav = q2/su
                  hav = z2+vav*vav/g2
                  write(zch,'(f8.3)') all_OuvEle(nk)%uv4  !jbf 26/11/2002
                  ltra_size = ltra_size+1
                  write(ligne_tra(ltra_size),1000) all_OuvEle(nk)%cvar,all_OuvEle(nk)%uv1,zch,ham,hav,q1,dbith,car,cvn
               end if
            endif
         enddo

         !   ICI iuvnk = type du dernier ouvrage elementaire de la section
         if (iuvnk < 0) stop '>>>> BUG 1 dans write_TRA_Ouvrages'
         if (iuvnk /= 3  .and.  iuvnk /= 5  .and.  iuvnk <= 90) then
         ! la section courante ne contient pas de pompe ni de déversoir latéral ni de perte
         ! de charge à la borda : calcul d'erreur sur les ouvrages en travers standard
            if (abs(qtt) < 0.01_long .and. abs(q1) < 0.01_long) then
               dq0 = zero
            else if (.not.zegal(qtt,zero,un)) then
               dq0 = abs(q1-qtt)/qtt
            else if (.not.zegal(q1,zero,un)) then
               dq0 = abs(q1-qtt)/q1
            else
               dq0 = huge(zero)
            end if
         elseif (iuvnk == 3 .or. iuvnk == 5) then
         ! cas des pompes et déversoirs latéraux
            q2 = qt(js)
            if (abs(qtt) < 0.01_long .and. abs(q1) < 0.01_long) then
               dq0 = zero
            else if (.not.zegal(qtt,zero,un)) then
               dq0 = abs(q1-q2-qtt)/qtt
            else if (.not.zegal(q1,zero,un)) then
               dq0 = abs(q1-q2-qtt)/q1
            else
               dq0 = huge(zero)
            end if
         else
            dq0 = zero
         endif
         if (iuvnk <= 90 .and. t > tinf) then
            if (dq0 > huge(zero)/2) stop '>>>> BUG 2 dans write_TRA_Ouvrages'
            if (abs(dq0) > dqmax(ns)) then
               dqmax(ns) = abs(dq0)
               tmax(ns) = t
            endif
         endif
      enddo
   enddo

   1000 format(' :',a10,':',(f8.3,':'),a8,':',4(f8.3,':'),a6,':',f5.2,':')
   1001 format(' :',a10,':',6(f8.3,':'),a6,':',f5.2,':')
   2001 format(1x,'########  DATE : ',a,' (jjj:hh:mn:ss) (',i2.2,' itérations)  ########')
   2002 format(1x,'########  DATE : ',a,'                (',i2.2,' itérations)  ########')
   3000 format(1x,79('='))
end subroutine write_TRA_Ouvrages



subroutine recap_Ouvrages
!==============================================================================
!                  Récapitulatif sur les lois d'ouvrage
!==============================================================================
   use parametres, only: nfich, long, lTra
   use Ouvrages, only: all_OuvCmp, all_OuvEle
   use erreurs_sing, only: dqmax=>dqsup,tmax=>tsup,vol=>volsng
   use mage_utilitaires, only: ecr0, heure
   use TopoGeometrie, only: la_topo, xgeo, numero_bief
   implicit none
   ! -- Variables --
   logical :: bool
   integer :: ib,indic,js,k,n,nk,ns
   character(len=19) :: hms
   real(kind=long) :: pm   !abscisse de la section singulière
   integer,pointer :: nsmax
   character(len=14) :: chain

   nsmax => la_topo%net%nss

   !impression des erreurs sur les débits passant sur les singularités
   indic = 0
   do ns = 1, nsmax
      if (all_OuvEle(all_OuvCmp(ns)%OuEl(1,1))%iuv /= 99)  then
         bool = .false.
         do k = 1, all_OuvCmp(ns)%ne
            nk = all_OuvCmp(ns)%OuEl(k,1)
            bool = bool.or.all_OuvEle(nk)%is_writable
         enddo
         js = all_OuvCmp(ns)%js-1
         pm = xgeo(js)
         ib = numero_bief(js)
         hms = heure(tmax(ns))
         if (indic < 1) then
            indic = 1
            write(lTra,1006)
            write(lTra,1002)
            write(lTra,1001)
            if (bool) write(lTra,1000) ib,pm,dqmax(ns),hms
         else
            if (bool) write(lTra,1000) ib,pm,dqmax(ns),hms
         endif
      endif
   enddo
   n = 0
   do ns = 1, nsmax
      if (dqmax(ns) > 0.01_long) n=n+1
   enddo
   if (n == 1) then
      write(lTra,1013)
   elseif (n > 1) then
      write(lTra,1003) n
   endif
   !impression des volumes passant sur les singularités
   if (indic > 0) then
      write(lTra,1006)
      write(lTra,1005)
      do ns = 1, nsmax
         if (all_OuvEle(all_OuvCmp(ns)%OuEl(1,1))%iuv /= 99) then
            js = all_OuvCmp(ns)%js-1
            pm = xgeo(js)
            bool = .false.
            do k = 1, all_OuvCmp(ns)%ne
               nk = all_OuvCmp(ns)%OuEl(k,1)
               bool = bool.or.all_OuvEle(nk)%is_writable
            enddo
            ib = numero_bief(js)
            if (bool) then
               call ecr0(chain,vol(ns),k)
               if (chain(1:1) /= '!') then
                  write(lTra,1004) ib,pm,chain(k:),' m3'
               else
                  write(lTra,1004) ib,pm,chain(k:),' e+6 m3'
               endif
            endif
         endif
      enddo
      write(lTra,1006)
   endif

   1000 format(1x,'Bief ',i3,' Pm',f10.2,' : Erreur rel. maxi. =  ',e9.4,' à ',a)
   1001 format(1x,'Vérification des lois d''ouvrage (Dates affichées dans TRA uniquement)',/,  &
                ' --> On compare le débit total dans la section singulière à la somme des',/,  &
                ' --> débits partiels sur les ouvrages de la section')
   1002 format(1x)
   1003 format(1x,'>>>> Il y a ',i3.3,' sections singulières avec une erreur supérieure à 1/100',/,  &
                ' >>>> Il est possible que le pas de temps soit trop grand ',/,  &
                '      ou alors les débits sont très faibles')
   1013 format(1x,'>>>> Il y a 1 section singulière avec une erreur supérieure à 1/100',/,  &
                ' >>>> Il est possible que le pas de temps soit trop grand ',/,  &
                '      ou alors les débits sont très faibles')
   1004 format(1x,'Bief ',i3,' Pm',f10.2,' : Volume total =  ',2a)
   1005 format(1x,'Calcul des volumes passant dans les sections singulières')
   1006 format(1x,79('-'))
end subroutine recap_Ouvrages


subroutine write_csv
!==============================================================================
!                 Ecriture des fichiers CSV
!
!==============================================================================
   use parametres, only: long, un, i_debord
   use, intrinsic :: iso_fortran_env, only: output_unit

   use data_num_fixes, only: tinf, tmax, dtcsv, fp_model, dtbin, date_format
   use data_num_mobiles, only: t, dt, tcsv, tbin
   use mage_utilitaires, only: simplification_lineaire, zegal
   use data_csv, only: filtre, ncsv
   use TopoGeometrie, only: iSect
   implicit none
   interface
      function select_var(var,is)
         use parametres, only: long
         character(len=3), intent(in) :: var
         integer, intent(in) :: is
         real(kind=long) :: select_var
      end function select_var
   end interface
   ! -- prototype --
   ! -- variables --
   integer, save :: nap = 0
   integer :: n, nd, nl
   integer, save :: bn
   integer, pointer :: is
   integer, allocatable, save :: lu(:)
   real(kind=long) :: th !temps en heures
   real(kind=long), save :: nbdt !compteur de pas de temps pour le calcul des moyennes
   real(kind=long), allocatable :: xd(:), yd(:)
   integer, save :: nligne !compteur de lignes des fichiers csv
   real(kind=long) :: alpha !tolérance de la simplification linéaire
   real(kind=long), parameter :: fact = 1._long/3600._long !facteur d'échelle pour la simplification linéaire
   ! -- buffer --
   integer,parameter :: bsize = 1000
   real(kind=long),allocatable,save :: bxd(:,:), byd(:,:)
   !------------------------------------------------------------------------------
   if (ncsv == 0) return
   if (nap == 0) then
   ! initialisation du buffer
      allocate(bxd(ncsv,bsize),byd(ncsv,bsize))
      bn = 0
   ! ouverture des fichiers et affectation des unités logiques
      allocate (lu(ncsv))
      do n = 1, ncsv
         ! NOTE: le nom filtre(n)%filename a été défini dans Ecrire_REP()
         open(newunit=lu(n),file=trim(filtre(n)%filename),form='formatted',status='unknown')
         filtre(n)%is = iSect(filtre(n)%ib,filtre(n)%pk)
         ! entête
         ! NOTE: si on ajoute des variables dans le select case suivant il faut aussi compléter celui de select_var()
         select case (trim(filtre(n)%var))
            ! NOTE: ne pas mettre d'espace dans les entêtes de colonnes, Gnuplot n'aime pas ça.
            case ('Q','q')
               if (fp_model /= i_debord) then
                  write(lu(n),'(a)') '#       Temps(s) ; Q_total(m3/s) '
               else
                  write(lu(n),'(a)') '#       Temps(s) ; Débits(m3/s)  '
               endif
            case ('Z','z')
               write(lu(n),'(a)') '#       Temps(s)  ;  Cotes(m)  '
            case ('V','v')
               write(lu(n),'(a)') '#       Temps(s) ; Vitesses(m/s) '
            case ('Y','y')
               write(lu(n),'(a)') '#       Temps(s) ; Hauteurs(m) '
            case ('QM','qm')
               if (fp_model == i_debord) then
                  write(lu(n),'(a)') '#       Temps(s) ;  Débits(m3/s)  '
               else
                  write(lu(n),'(a)') '#       Temps(s) ; Q_mineur(m3/s) '
               endif
            case ('QL','ql')
               if (fp_model == i_debord) then
                  write(lu(n),'(a)') '#       Temps(s) ;  Débits(m3/s)  '
               else
                  write(lu(n),'(a)') '#       Temps(s) ; Q_gauche(m3/s) '
               endif
            case ('QR','qr')
               if (fp_model == i_debord) then
                  write(lu(n),'(a)') '#       Temps(s) ;  Débits(m3/s)  '
               else
                  write(lu(n),'(a)') '#       Temps(s) ; Q_droit(m3/s) '
               endif
            case ('QT','qt')
               if (fp_model == i_debord) then
                  write(lu(n),'(a)') '#       Temps(s) ;  Débits(m3/s)  '
               else
                  write(lu(n),'(a)') '#       Temps(s) ; Q_total(m3/s) '
               endif
            case ('QML','qml')
               if (fp_model == i_debord) then
                  write(lu(n),'(a)') '#       Temps(s) ;  Débits(m3/s)  '
               else
                  write(lu(n),'(a)') '#       Temps(s) ; Q_transfert_Gauche(m2/s) '
               endif
            case ('QMR','qmr')
               if (fp_model == i_debord) then
                  write(lu(n),'(a)') '#       Temps(s) ;  Débits(m3/s)  '
               else
                  write(lu(n),'(a)') '#       Temps(s) ; Q_transfert_Droit(m2/s) '
               endif
            case default
               stop 'write_csv : variable non permise'
         end select
         is => filtre(n)%is
         filtre(n)%w = select_var(filtre(n)%var,is)
         th = tinf / 3600._long
         filtre(n)%w = 0._long
      enddo
      nap = 1
      nbdt = 0._long
      nligne = 0
   !correction de dtcsv pour le ramener à un multiple de dtbin
      if (dtcsv > 1.e10_long) dtcsv = dtbin  !tous les dtcsv lus sont nuls, on prend dtbin
      dtcsv = max(dtbin , dtbin * aint(dtcsv/dtbin))
      tcsv = tbin
   endif

   !dtcsv = dtbin
   !tcsv = tbin
   nbdt = nbdt + 1._long

   do n = 1, ncsv
      is => filtre(n)%is
      if (filtre(n)%mode == 0) then  !valeur instantanée
         filtre(n)%w = select_var(filtre(n)%var,is)
      else if (filtre(n)%mode == 1) then ! moyenne
         filtre(n)%w = filtre(n)%w + select_var(filtre(n)%var,is)
      else
         stop 'write_csv : mode non permis'
      endif
   enddo

   if (bn > bsize) then
      print*,bsize,bn
      stop ' >>>> Erreur de buffer pour les CSV'
   elseif (bn == bsize) then
      ! vidage du buffer
      do n = 1, ncsv
         do nd = 1, bsize
            write(lu(n),'(f16.1,a3,f11.4)') bxd(n,nd),' ; ',byd(n,nd)
         enddo
      enddo
      bn = 0
   endif

   if (zegal(t-dt,tcsv,un)) then
      !écrire les fichiers csv
      th = t-dt
      bn = bn+1
      do n = 1, ncsv
         if (filtre(n)%mode == 1) filtre(n)%w = filtre(n)%w / nbdt
         if (date_format) then
            bxd(n,bn) = th-tinf  ;  byd(n,bn) = filtre(n)%w  !écriture dans le buffer
         else
            bxd(n,bn) = th  ;  byd(n,bn) = filtre(n)%w  !écriture dans le buffer
         endif
         filtre(n)%w = 0._long
      enddo
      nligne = nligne + 1
      tcsv = min(tcsv+dtcsv,tmax)  !prochaine date d'écriture, si t=tmax on écrit la solution au dernier pas de temps
      nbdt = 0._long
   endif

   if (t > tmax) then
      !simplification linéaire des courbes F(t)
      !on relit et réécrit les fichiers csv car on veut avoir quelque chose
      !si la simulation échoue et donc ne passe pas par ici
      ! vidage du dernier buffer
      do n = 1, ncsv
         do nd = 1, bn
            write(lu(n),'(f16.1,a3,f11.4)') bxd(n,nd),' ; ',byd(n,nd)
         enddo
      enddo
      allocate(xd(nligne),yd(nligne))
      do n = 1, ncsv
         select case (trim(filtre(n)%var))
            case ('q', 'z', 'v', 'y', 'qt', 'qm', 'ql', 'qr', 'qmr', 'qml')
               !pas de simplification linéaire si la variable est donnée en minuscule
               cycle
            case default
               rewind(lu(n))
               read(lu(n),'(a)') !on ignore la ligne d'entête
               do nl = 1, nligne
                  read(lu(n),'(f16.0,3x,f11.0)') xd(nl),yd(nl)
               enddo
               alpha = 0.00001_long  ;  nd = nligne
               if (filtre(n)%var == 'Z') alpha = alpha*sqrt(alpha)
               write(filtre(n)%filename,'(4a,i0,a1,i0,a)') trim(filtre(n)%prefix),'_',filtre(n)%var,'_', &
                                                 filtre(n)%ib,'_',int(filtre(n)%pk),'.csv'
               write(output_unit,'(2a)') ' -> simplification linéaire de ',trim(filtre(n)%filename)
               call simplification_lineaire(alpha,xd,yd,nd,fact)
               rewind(lu(n))
               read(lu(n),'(a)') !on ignore la ligne d'entête
               do nl = 1, nd
                  write(lu(n),'(f16.1,a3,f11.4)') xd(nl),' ; ',yd(nl)
               enddo
               endfile(lu(n))
         end select
         close(lu(n))
      enddo
      deallocate(xd,yd,lu)
   endif
end subroutine write_csv


function select_var(var,is)
   use parametres, only: long, i_debord
   use hydraulique, only: qt,zt,yt,vt
   use StVenant_ISM, only: q, Q_total, qml, qmr
   use Data_Num_Fixes, only: fp_model
   implicit none
   ! -- prototype --
   character(len=3), intent(in) :: var
   integer, intent(in) :: is
   real(kind=long) :: select_var

   select case (trim(var))
      case ('Q','q')
         if (fp_model /= i_debord) then
            select_var = Q_total(q(is))
         else
            select_var = qt(is)
         endif
      case ('Z','z')
         select_var = zt(is)
      case ('V','v')
         select_var = vt(is)
      case ('Y','y')
         select_var = yt(is)
      case ('QM','qm')
         if (fp_model == i_debord) then
            select_var = qt(is)
         else
            select_var = q(is)%qm
         endif
      case ('QL','ql')
         if (fp_model == i_debord) then
            select_var = qt(is)
         else
            select_var = q(is)%ql
         endif
      case ('QR','qr')
         if (fp_model == i_debord) then
            select_var = qt(is)
         else
            select_var = q(is)%qr
         endif
      case ('QML','qml')
         if (fp_model == i_debord) then
            select_var = qt(is)
         else
            select_var = qml(is)
         endif
      case ('QMR','qmr')
         if (fp_model == i_debord) then
            select_var = qt(is)
         else
            select_var = qmr(is)
         endif
      case ('QT','qt')
         if (fp_model == i_debord) then
            select_var = qt(is)
         else
            select_var = Q_total(q(is))
         endif
      case default
         stop 'write_csv : variable non permise'
   end select
end function select_var
