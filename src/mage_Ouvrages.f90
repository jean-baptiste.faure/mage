!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - INRAE - 1989-2020                           #
!# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################
!
module Ouvrages
!=====================================================================================
!          Description des ouvrages élémentaires et composites
!
! NOTE: en principe ce module ne devrait utiliser aucun autre module à part Parametres
!
! NOTE: stop numérotés à partir de 1000
!=====================================================================================
use Parametres, only: long, g, zero, un, Pi, NEsup, lTra, deuxtiers
use, intrinsic :: iso_fortran_env, only: output_unit, error_unit
use :: iso_c_binding, only : c_double
use Mage_Utilitaires, only: sqrt2, c_tm, c_mktime, get_tm, RTLD_LAZY,c_ptr, c_funptr, &
#ifdef linux
                    c_dlopen, c_dlsym, c_dlclose, &
#endif
                    c_null_char, c_associated, c_f_procpointer, c_associated, c_f_procpointer
use IO_Files, only: varFile

implicit none

type ouvrage_elementaire
   character(len=10) :: cvar  !nom de l'ouvrage élémentaire
   integer :: iuv ! type de l'ouvrage
   !                     0  : déversoir_orifice à seuil mobile
   !                     1  : porte à flot ou clapet (supprimé)
   !                     2  : seuil_vanne de fond
   !                     3  : pompe
   !                     4  : vanne_loi simplifiée
   !                     5  : déversoir latéral
   !                     6  : déversoir orifice à ouverture variable
   !                     7  : clapet inversé (supprimé)
   !                     8  : buse
   !                     9  : ouvrage défini par l'utilisateur (DEBITX)
   !                     10 : déversoir trapézoïdal
   !                     11 : orifice-voute
   !                     91 : perte de charge à la Borda
   !                     99 : ouvrage non-défini
   real(kind=long) :: uv1, uv2, uv3, uv4, uv5 !paramètres descriptifs de l'ouvrage
   ! NOTE: changement par rapport à Mage-7 : on garde les cotes de déversement au lieu de passer par les pelles
   ! NOTE: comme les ouvrages sont placés dans l'un des 3 lits, la cote du fond pour le calcul de la pelle est délicat
   ! NOTE: il vaut mieux demander à l'utilisateur de fournir la cote de déversement minimale pour les seuils mobiles
   ! cas d'un deversoir, seuil mobile
   !             uv1 = largeur de l'ouvrage elementaire N
   !             uv2 = cote de déversement
   !             uv3 = ouverture de la vanne ou de l'orifice
   !             uv4 = coefficient de débit (orifice) ou de contraction (vanne)
   !             uv5 = cote de déversement minimale = cote du fond locale dans la section
   ! cas d'un orifice, d'un seuil-vanne de fond ou d'un deversoir lateral
   !             uv1 = largeur de l'ouvrage elementaire N
   !             uv2 = cote de déversement
   !             uv3 = ouverture de la vanne ou de l'orifice
   !             uv4 = coefficient de débit (orifice) ou de contraction (vanne)
   !             uv5 = ouverture maximale
   ! cas d'un déversoir-orifice trapézoïdal
   !             uv1 = largeur de base du déversoir
   !             uv2 = cote de déversement
   !             uv3 = ouverture / hauteur de mise en charge
   !             uv4 = coefficient de débit
   !             uv5 = fruit des joues du déversoir
   ! cas d'un orifice-voute
   !             uv1 = largeur de base de l'orifice
   !             uv2 = cote de déversement
   !             uv3 = hauteur de la partie rectangulaire
   !             uv4 = hauteur de la voute
   !             uv5 = coefficient de débit
   ! cas d'une buse
   !             uv1 = diamètre
   !             uv2 = cote de déversement
   !             uv3 = hauteur d'envasement
   !             uv4 = coefficient de débit
   !             uv5 = 0 (non utilisé)
   ! cas d'une pompe :
   !             uv1 = debit nominal de la pompe
   !             uv2 = prochain instant de mise en route
   !             uv3 = prochain instant d'arrêt
   !             uv4 = durée de la montée en puissance
   !             uv5 = durée de la descente en puissance
   real(kind=long) :: za1, za2, za3  !paramètres supplémentaires pour l'instant seulement utilisés pour les pompes -> Zalert(1:3,:)
   !              za1 = cote de démarrage de la pompe
   !              za2 = cote d'arrêt de la pompe
   !              za3 = tirant d'eau de désamorçage
   integer :: irf
   !              irf = n° de la section où est mesurée la cote pour le contrôle des pompes
   !                    ou n° de le section de référence pour les lois de régulation W(Z)

   ! DONE: il faudrait renommer boolj. Par exemple en is_writable (mais c'est long)
   logical :: is_writable = .false.  !indicateur d'écriture des ouvrages élémentaires
                                     !is_writable est faux si CVAR est blanc mais on peut définir un autre critère

   !programmation des ouvrages mobiles : index dans le fichier temporaire à accès direct <nom_etude>_var (créé par Lire_VAR())
   !                                     utilisé par change_position() et position_ouvrage()
   integer :: itw = 0 !premier point de la courbe w(tz) pour cet ouvrage
   integer :: jtw = 0 !dernier point de la courbe w(tz) pour cet ouvrage
   integer :: w_cycle = 0 !0: pas de cycle, 1: années, 2: mois, 3:semaines, 4: jours

   real(kind=long), dimension(:), allocatable :: wtz, tz
   logical :: reg = .false. ! false : position dans wtz, true: position dans wtz_reg
   real(kind=long), dimension(2) :: wtz_reg, tz_reg ! pour que la régulation n'interfère pas avec la programmation

   !index dans le tampon d'enregistrement des positions des ouvrages mobiles c'est-à-dire ceux présents dans le fichier VAR
   integer :: mob = -1 !initialisation par défaut à -1 (ouvrage non mobile)

   !unité logique pour le fichier CSV d'enregistrement des positions de l'ouvrage mobile
   integer :: lu = -1 !initialisation par défaut à -1 (ouvrage non mobile)

   type(c_funptr) :: debitX_addr
   type(c_ptr) :: debitX_handle
   procedure(called_debitX), nopass, pointer :: debitX

end type ouvrage_elementaire


type ouvrage_composite
   integer :: js !index de la section (profil) qui contient cet ouvrage composite
   integer :: ne !nombre d'ouvrages élémentaires qui composent l'ouvrage composite
   integer :: OuEl(NEsup,2)   !liste des ouvrages élémentaires,
                              ! OuEl(n,1) : index dans la liste générale
                              ! OuEl(n,2) : n° de lit du profil prf dans lequel est placé l'ouvrage élémentaire
                              ! NOTE: NEsup est fourni par le module Parametres
   real(kind=long) :: zfs  !cote du fond de la section singulière associée (section aval)
end type ouvrage_composite


type(ouvrage_elementaire), allocatable :: all_OuvEle(:) !liste de tous les ouvrages élémentaires lus dans le fichier SIN

type(ouvrage_composite), allocatable :: all_OuvCmp(:)   !liste des ouvrages composites

logical :: some_ouvrages_writable   !indicateur global d'écriture des ouvrages : si tous les all_OuvEle(:)%is_writable
                                    !sont faux, alors some_ouvrages_writable est faux et il n'y a pas d'écriture

! Données pour la régulation des ouvrages
integer :: nbreg = 0       ! nombre de règles de régulation
logical :: position_ouvrage_first_call = .true.
character(len=120), allocatable :: regle(:) ! règles de régulation, dimension = nbreg


!buffer d'enregistrement des positions des ouvrages mobiles
integer :: nb_mobiles = 0    !nombre d'ouvrages mobiles régulés
integer, parameter :: buffer_mobiles_size = 1000
integer :: buffer_current_index    !niveau de remplissage actuel du buffer
real(kind=long), private, allocatable :: buffer_mobiles(:,:)
integer :: CSV_files_size = 0

!buffer d'écriture sur TRA
character(len=80), allocatable :: ligne_tra(:)  !buffer d'écriture sur TRA pour les ouvrages
                                      !dont l'écriture est décalée de celles du reste de TRA
integer :: ltra_size

abstract interface
    function called_debitX (z1,z2,p1,p2,p3,p4,p5) bind(c)
        use, intrinsic :: iso_c_binding, only: c_double
        real(kind=c_double) :: called_debitX
        real(kind=c_double),intent(in) :: z1,z2,p1,p2,p3,p4,p5
    end function called_debitX
end interface

contains

subroutine charger_debitX(ouv, libname, funcname)
    type(ouvrage_elementaire) :: ouv
    character(len=250) :: libname
    character(len=20) :: funcname

#ifdef linux
    ouv%debitX_handle=c_dlopen(trim(libname)//c_null_char, RTLD_LAZY)
    if (.not. c_associated(ouv%debitX_handle))then
        print*, 'Unable to load DLL ',libname
        stop
    end if

    ouv%debitX_addr=c_dlsym(ouv%debitX_handle, trim(funcname)//c_null_char)
    if (.not. c_associated(ouv%debitX_addr))then
        write(*,*) 'Unable to load the procedure ',funcname
        stop
    end if

    call c_f_procpointer( ouv%debitX_addr, ouv%debitX )
#else
    write(*,*) 'DebitX only available on Linux'
    stop
#endif
end subroutine charger_debitX

subroutine allocate_buffer_mobiles
! allocation de buffer_mobiles

   allocate (buffer_mobiles(0:nb_mobiles,buffer_mobiles_size))
end subroutine allocate_buffer_mobiles


subroutine add_to_buffer_mobiles(t)
   use data_num_fixes, only: tinf, date_format
! ajout d'une ligne au buffer
   ! -- prototype --
   real(kind=long), intent(in) :: t !temps actuel
   ! -- variables locales --
   integer :: nk

   !Si le buffer est plein on le vide avant de continuer
   if (buffer_current_index == buffer_mobiles_size) call write_buffer_mobiles()

   buffer_current_index = buffer_current_index + 1
   do nk = 1, size(all_OuvEle)
      if (date_format) then
         buffer_mobiles(0,buffer_current_index) = t-tinf
      else
         buffer_mobiles(0,buffer_current_index) = t
      endif
      if (all_OuvEle(nk)%mob > 0) then
         select case (all_OuvEle(nk)%iuv)
            case (2, 4, 6)  !vannes et orifices à ouverture variable
               buffer_mobiles(all_OuvEle(nk)%mob,buffer_current_index) = all_OuvEle(nk)%uv3
            case (0, 5, 9)     !seuils mobiles à hauteur variable
               buffer_mobiles(all_OuvEle(nk)%mob,buffer_current_index) = all_OuvEle(nk)%uv2
            case (3)
               buffer_mobiles(all_OuvEle(nk)%mob,buffer_current_index) = debit3(nk,t)
            case default
               write(error_unit,*) ' >>>> ERREUR dans add_to_buffer_mobiles() : cas non prévu :-( '
               stop 1
         end select
      endif
   enddo
end subroutine add_to_buffer_mobiles


subroutine write_buffer_mobiles ()
! vidange du buffer dans les fichiers CSV associés aux ouvrages mobiles régulés
   ! -- variables locales --
   integer :: n, nk
   logical, save :: first_call = .true.

   if (first_call) then
      do nk = 1, size(all_OuvEle)
         if (all_OuvEle(nk)%mob > 0) then
            open(newunit=all_OuvEle(nk)%lu, file=trim(all_OuvEle(nk)%cvar)//'.csv', form='formatted', status='unknown')
            write(all_OuvEle(nk)%lu,'(2a)') '# Positions de ',trim(all_OuvEle(nk)%cvar)
            write(all_OuvEle(nk)%lu,'(a)') '#      Temps(s)  ;  Position'
         endif
      enddo
      first_call = .false.
   endif

   CSV_files_size = CSV_files_size + buffer_current_index
   do n = 1, buffer_current_index
      do nk = 1, size(all_OuvEle)
         if (all_OuvEle(nk)%mob > 0) then
            write(all_OuvEle(nk)%lu,'(f16.1,a3,f11.6)') buffer_mobiles(0,n),' ; ',buffer_mobiles(all_OuvEle(nk)%mob,n)
         endif
      enddo
   enddo

   buffer_current_index = 0 !remise à 0 de l'index puisque le buffer est vide
end subroutine write_buffer_mobiles


function debit_total(ns,zam,zav)
!==============================================================================
!       Calcul du débit total à travers un ouvrage composite
!==============================================================================
   implicit none
   !-- prototype
   real(kind=long) :: debit_total
   integer, intent(in) :: ns   !index de l'ouvrage composite dans all_OuvCmp()
   real(kind=long), intent(in) :: zam, zav  !niveaux d'eau amont et aval
   !-- variables locales
   integer :: k, nk
   character(len=180) :: err_message
   !------------------------------------------------------------------------------
   if (ns <= 0 .or. ns > size(all_OuvCmp)) then
      write(err_message,'(a)') 'erreur dans debit_total() : section non singulière'
      write(error_unit,'(a)') trim(err_message)
      write(err_message,'(a)') 'Merci d''envoyer un rapport de bug'
      write(error_unit,'(a)') trim(err_message)
      stop 1003
   endif
   debit_total = zero
   do k = 1, all_OuvCmp(ns)%ne
      nk = all_OuvCmp(ns)%OuEl(k,1)
      debit_total = debit_total+debit(nk,zam,zav)
   enddo
end function debit_total


function debit_pompe_total(ns,t)
!==============================================================================
!   Calcul du débit total au temps T d'un ouvrage composite composé de pompes
!==============================================================================
   implicit none
   !-- prototype
   real(kind=long) :: debit_pompe_total
   integer, intent(in) :: ns   !index de l'ouvrage composite dans all_OuvCmp()
   real(kind=long), intent(in) :: t  !date en secondes
   !-- variables locales
   integer :: k, nk
   character(len=180) :: err_message
   !------------------------------------------------------------------------------
   if (ns <= 0 .or. ns > size(all_OuvCmp)) then
      write(err_message,'(a)') 'erreur dans debit_total() : section non singulière'
      write(error_unit,'(a)') trim(err_message)
      write(err_message,'(a)') 'Merci d''envoyer un rapport de bug'
      write(error_unit,'(a)') trim(err_message)
      stop 1004
   elseif (all_OuvEle(all_OuvCmp(ns)%OuEl(1,1))%iuv /= 3) then
      write(err_message,'(a)') 'erreur dans debit_total() : il n''y a pas de pompe ici'
      write(error_unit,'(a)') trim(err_message)
      write(err_message,'(a)') 'Merci d''envoyer un rapport de bug'
      write(error_unit,'(a)') trim(err_message)
      stop 1004
   endif
   debit_pompe_total = zero
   do k = 1, all_OuvCmp(ns)%ne
      nk = all_OuvCmp(ns)%OuEl(k,1)
      debit_pompe_total = debit_pompe_total+debit3(nk,t)
   enddo
end function debit_pompe_total


function debit(nk,Zam,Zav)
!==============================================================================
!           calcul des débits sur les ouvrages élémentaires
!
!--->Zam = niveau d'eau amont (cote)
!--->Zav = niveau d'eau aval (cote)
!--->H1 = hauteur d'eau amont au dessus de la cote de déversement
!--->H2 = hauteur d'eau aval au dessus de la cote de déversement
!
!==============================================================================
   implicit none
   ! -- prototype --
   real(kind=long) :: debit
   integer,intent(in) :: nk
   real(kind=long),intent(in) :: Zam,Zav
   ! -- variables --
   real(kind=long) :: coeff,ctg,h1,h2,hr,hv,p1,p2,p3,p4,p5,sl,w
   !------------------------------------------------------------------------------
   h1 = Zam-all_OuvEle(nk)%uv2
   h2 = Zav-all_OuvEle(nk)%uv2
   sl = all_OuvEle(nk)%uv1
   w = all_OuvEle(nk)%uv3
   coeff = all_OuvEle(nk)%uv4

   select case (all_OuvEle(nk)%iuv)
      case (99)                                    !ouvrage non défini
         debit = zero
      case (0,6)                                   !déversoir-orifice (seuil mobile)
         debit = debitd(h1,h2,sl,w,coeff)
!      case (1)                                     !porte à flot ou clapet
!         debit = debitc(h1,h2,sl,w,coeff)
      case (2)                                     !vanne de fond en charge (formulation complète)
         if (h1 < w) then
            ! FIXME: élaborer le message d'erreur
            write(output_unit,*) '>>>> ERREUR : vanne de fond à surface libre'
            stop 1006
         else
            debit = debit2(h1,h2,sl,w,coeff)
         endif
      case (4)                                     !vanne de fond en charge (formulation simplifiée)
         if (h1 > h2) then
            if (h1 < w) then
               ! FIXME: élaborer le message d'erreur
               write(output_unit,*) '>>>> ERREUR : vanne de fond à surface libre'
               stop 1007
            else
               debit = debit4(h1,h2,sl,w,coeff)
            endif
         else
            if (h2 < w) then
               ! FIXME: élaborer le message d'erreur
               write(output_unit,*) '>>>> ERREUR : vanne de fond à surface libre'
               stop 1008
            else
               debit = -debit4(h2,h1,sl,w,coeff)
            endif
         endif
      case (5)                                     !déversoir latéral
         debit = debitl(h1,h2,sl,w,coeff)
!      case (7)                                     !porte à flot ou clapet inversé
!         debit = debiti(h2,h1,sl,w,coeff)
      case (8)                                     !buse
         debit = debitb(h1,h2,sl,w,coeff)
      case (9)                                     !ouvrage X
         !---remarque : dans ce cas c'est l'utilisateur qui gère le sens d'écoulement

         if (.not. c_associated(all_OuvEle(nk)%debitX_addr))then
             write(output_unit,*) '>>>> Fonction débit ouvrage utilisateur debitx non trouvée'
             stop
         end if
         p1 = all_OuvEle(nk)%uv1
         p2 = all_OuvEle(nk)%uv2
         p3 = all_OuvEle(nk)%uv3
         p4 = all_OuvEle(nk)%uv4
         p5 = all_OuvEle(nk)%uv5
         debit = all_OuvEle(nk)%debitx(zam,zav,p1,p2,p3,p4,p5)
      case (10)                                    !déversoir trapézoïdal
         ctg = all_OuvEle(nk)%uv5
         debit = debitt(h1,h2,sl,w,coeff,ctg)
      case (11)                                    !orifice voute (fer à cheval)
         sl = all_OuvEle(nk)%uv1
         hr = all_OuvEle(nk)%uv3
         hv = all_OuvEle(nk)%uv4
         coeff = all_OuvEle(nk)%uv5
         debit = debitf(h1,h2,sl,hr,hv,coeff)
      case (91)                                    !perte de charge à la Borda
         debit = all_OuvEle(nk)%uv3
      case default
         write(output_unit,*) '>>>> BUG dans debit() : on ne devrait pas arriver ici !'
         write(output_unit,*) '     Type d''ouvrage inconnu : ',all_OuvEle(nk)%iuv
         stop 1000
   end select

end function debit


function debitB(h1,h2,sl,w,coeff)
!==============================================================================
!                calcul du débit à travers une buse
!                   gestion du sens d'écoulement
!
!--->h1 = hauteur d'eau amont au dessus du déversoir - charge amont
!--->h2 = hauteur d'eau aval au desus du déversoir - hauteur aval
!--->sl = longueur du déversoir
!--->w  = ouverture de l'orifice (vanne de surface)
!--->coeff = coefficient de debit
!==============================================================================
   implicit none
   ! -- le prototype --
   real(kind=long) :: debitb
   real(kind=long),intent(in) :: h1,h2,sl,w,coeff

   !-------------------------------------------------------------------------------
   if (h1>h2) then  !sens normal du courant
      debitb = debit8(h1,h2,sl,w,coeff)
   else             !inversion du sens du courant
      debitb = -debit8(h2,h1,sl,w,coeff)
   endif
end function debitb


function debitC(h1,h2,sl,w,coeff)
!==============================================================================
!           calcul du débit à travers un clapet
!            (déversoir avec vanne de surface)
!               gestion du sens d'écoulement
!
!--->h1 = hauteur d'eau amont au dessus du déversoir - charge amont
!--->h2 = hauteur d'eau aval au desus du déversoir - hauteur aval
!--->sl = longueur du déversoir
!--->w  = ouverture de l'orifice (vanne de surface)
!--->coeff = coefficient de débit
!==============================================================================
   implicit none
   ! -- prototype --
   real(kind=long) :: debitc
   real(kind=long),intent(in) :: h1,h2,sl,w,coeff
   !------------------------------------------------------------------------------
   if (h1>h2) then  !porte ouverte
      debitc = debit0(h1,h2,sl,w,coeff)
   else             !porte fermée
      debitc = zero
   endif
end function debitC


function debitD(H1,H2,SL,W,Coeff)
!==============================================================================
! calcul du débit sur un déversoir-orifice (déversoir avec vanne de surface)
!                   gestion du sens d'ecoulement
!
!--->h1 = hauteur d'eau amont au dessus du déversoir - charge amont
!--->h2 = hauteur d'eau aval au desus du déversoir - hauteur aval
!--->sl = longueur du déversoir
!--->w  = ouverture de l'orifice (vanne de surface)
!--->coeff = coefficient de débit
!==============================================================================
   implicit none
   ! -- le prototype --
   real(kind=long) :: debitd
   real(kind=long),intent(in) :: h1,h2,sl,w,coeff
   !------------------------------------------------------------------------------
   if (h1>h2) then  !sens normal du courant
      debitd = debit0(h1,h2,sl,w,coeff)
   else             !inversion du sens du courant
      debitd = -debit0(h2,h1,sl,w,coeff)
   endif
end function debitD


function debitI(H1,H2,SL,W,Coeff)
!==============================================================================
!        calcul du débit à travers un clapet inversé
!            (déversoir avec vanne de surface)
!             gestion du sens d'écoulement
!
!--->h1 = hauteur d'eau amont au dessus du déversoir - charge amont
!--->h2 = hauteur d'eau aval au desus du déversoir - hauteur aval
!--->sl = longueur du déversoir
!--->w  = ouverture de l'orifice (vanne de surface)
!--->coeff = coefficient de débit
!==============================================================================
   implicit none
   ! -- le prototype --
   real(kind=long) :: debiti
   real(kind=long),intent(in) :: h1,h2,sl,w,coeff
   !------------------------------------------------------------------------------
   if (h1>h2) then  !porte ouverte
      debiti = zero
   else             !porte fermée
      debiti = -debit0(h2,h1,sl,w,coeff)
   endif
end function debitI


function debitF(H1,H2,SL,HR,HV,Coeff)
!==============================================================================
!                calcul du débit a travers un orifice voute (fer a cheval)
!                   gestion du sens d'ecoulement
!
!--->h1 = hauteur d'eau amont au dessus du déversoir - charge amont
!--->h2 = hauteur d'eau aval au desus du déversoir - hauteur aval
!--->sl = longueur du déversoir
!--->w  = ouverture de l'orifice (vanne de surface)
!--->coeff = coefficient de debit
!==============================================================================
   implicit none
   ! -- prototype --
   real(kind=long) :: debitf
   real(kind=long),intent(in) :: h1,h2,sl,hr,hv,coeff
   !------------------------------------------------------------------------------
   if (h1>h2) then  !sens normal du courant
      debitf = debit7(h1,h2,sl,hr,hv,coeff)
   else             !inversion du sens du courant
      debitf = -debit7(h2,h1,sl,hr,hv,coeff)
   endif
end function debitF


function debitL(H1,H2,SL,W,Coeff)
!==============================================================================
!           calcul du débit sur un déversoir latéral
!               (déversoir avec vanne de surface)
!                 gestion du sens d'écoulement
!
!--->h1 = hauteur d'eau amont au dessus du déversoir - charge amont
!--->h2 = hauteur d'eau aval au desus du déversoir - hauteur aval
!--->sl = longueur du déversoir
!--->w  = ouverture de l'orifice (vanne de surface)
!--->coeff = coefficient de débit
!==============================================================================
   implicit none
   ! -- le prototype --
   real(kind=long) :: debitl
   real(kind=long),intent(in) :: h1,h2,sl,w,coeff
   !------------------------------------------------------------------------------
   if (h1>h2) then
      debitl = +debit0(h1,h2,sl,w,coeff)
   else
      debitl = -debit0(h2,h1,sl,w,coeff)
   endif
end function debitL


function debitT(H1,H2,SL,W,Coeff,CTG)
!==============================================================================
!           calcul du débit sur un déversoir trapézoïdal
!                 (déversoir avec vanne de surface)
!                   gestion du sens d'écoulement
!
!--->h1 = hauteur d'eau amont au dessus du déversoir - charge amont
!--->h2 = hauteur d'eau aval au desus du déversoir - hauteur aval
!--->sl = longueur du déversoir
!--->w  = ouverture de l'orifice (vanne de surface)
!--->coeff = coefficient de debit
!==============================================================================
   implicit none
   ! -- le prototype --
   real(kind=long) :: debitt
   real(kind=long),intent(in) :: h1,h2,sl,w,coeff,ctg
   !------------------------------------------------------------------------------
   if (h1>h2) then  !sens normal du courant
      debitT = +debit9(h1,h2,sl,w,coeff,ctg)
   else             !inversion du sens du courant
      debitT = -debit9(h2,h1,sl,w,coeff,ctg)
   endif
end function debitT


function debit0(H1,H2,SL,W,Coeff)
!==============================================================================
!           calcul du débit sur un déversoir-orifice
!                 (déversoir avec vanne de surface)
!
!--->h1 = hauteur d'eau amont au dessus du déversoir - charge amont
!--->h2 = hauteur d'eau aval au desus du déversoir - hauteur aval
!--->sl = longueur du déversoir
!--->w  = ouverture de l'orifice (vanne de surface)
!--->coeff = coefficient de débit
!==============================================================================
   implicit none
   ! -- prototype --
   real(kind=long) :: debit0
   real(kind=long),intent(in) :: h1,h2,sl,w,coeff
   ! -- constantes --
   real(kind=long),parameter :: c0 = sqrt(2._long*g)
   real(kind=long),parameter :: c2 = 3._long*sqrt(3._long)/2._long
   real(kind=long),parameter :: c1 = c0*c2
   ! -- variables --
   real(kind=long) :: hdn,hpn
   !------------------------------------------------------------------------------
   if (h1 < zero) then
      debit0 = zero  ;  return
   end if
   hdn = h1*deuxtiers
   if (h1<w) then  !surface libre
      if (h2<hdn) then  !surface libre dénoyé
         debit0 = coeff*c0*sl*h1*sqrt2(h1)
      else  !surface libre noyé
         debit0 = coeff*c1*sl*h2*sqrt2(h1-h2)
      end if
   else  !charge
      hpn = (2._long*h1 + w)/3._long
      if (h2<hdn) then  !charge dénoyé
         debit0 = coeff*c0*sl*(h1*sqrt2(h1)-(h1-w)**1.5_long)
      else if (h2<hpn) then  !régime charge partiellement noyé
         debit0 = coeff*c0*sl*(c2*h2*sqrt2(h1-h2)-(h1-w)**1.5_long)
      else
         debit0 = coeff*c1*sl*w*sqrt2(h1-h2)
      endif
   endif
end function debit0


function ddebi0(H1,H2,SL,W,Coeff,Indic)
!==============================================================================
!           calcul de la dérivée du débit sur un déversoir-orifice
!                 (déversoir avec vanne de surface)
!
!--->h1 = hauteur d'eau amont au dessus du déversoir
!--->h2 = hauteur d'eau aval au desus du déversoir
!--->sl = longueur du déversoir
!--->w  = ouverture de l'orifice (vanne de surface)
!--->coeff = coefficient de débit
!--->indic = numero de la variable de dérivation (1 ou 2)
!------------------------------------------------------------------------------
   implicit none
   ! -- prototype --
   real(kind=long) :: ddebi0
   real(kind=long),intent(in) :: h1,h2,sl,w,coeff
   integer,intent(in) :: indic
   ! -- constantes --
   real(kind=long),parameter :: c0 = sqrt(2._long*g)
   real(kind=long),parameter :: c2 = 3._long*sqrt(3._long)/2._long
   real(kind=long),parameter :: c1 = c0*c2
   real(kind=long),parameter :: dhmax = 1.0e-6_long
   ! -- variables --
   real(kind=long) :: b,dh,sqrtdh,hdn,hpn
   !------------------------------------------------------------------------------
   if (h1<zero) then
      ddebi0 = zero
      return
   end if
   hdn = deuxtiers*h1
   if (h1<w) then  !surface libre
      if (h2<hdn) then  !surface libre dénoyé > debit0=coeff*c0*sl*h1**1.5
         if (indic == 1) then
            ddebi0 = coeff*c0*sl*1.5_long*sqrt(h1)
         else
            ddebi0 = zero
         endif
      else  !surface libre noyé > debit0=coeff*c1*sl*h2*sqrt(h1-h2)
         dh = max(h1-h2,dhmax)
         if (indic==1) then
            ddebi0 = coeff*c1*sl*0.5_long*h2/sqrt(dh)
         else
            sqrtdh = sqrt(dh)
            ddebi0 = coeff*c1*sl*(sqrtdh-0.5_long*h2/sqrtdh)
         endif
      end if
   else  !charge
      hpn = deuxtiers*h1+w/3._long
      if (h2<hdn) then  !charge dénoyé > debit0=coeff*c0*sl*(h1**1.5-(h1-w)**1.5)
         if (indic==1) then
            ddebi0 = coeff*c0*sl*1.5_long*(sqrt(h1)-sqrt(h1-w))
         else
            ddebi0 = zero
         endif
      else if (h2<hpn) then  !charge partiellement noyé > debit0=a*(sqrt(h1-h2)*h2*c2-(h1-w)**1.5)
         dh = max(h1-h2,dhmax)
         if (indic==1) then
            ddebi0 = coeff*c0*sl*(c2*h2/sqrt(dh)-3._long*sqrt(h1-w))*0.5_long
         else
            b = sqrt(dh)
            ddebi0 = coeff*c0*sl*c2*(b-0.5_long*h2/b)
         endif
      else  !charge totalement noyé > debit0=a*w*sqrt(h1-h2)
         dh=max(h1-h2,dhmax)
         if (indic==1) then
            ddebi0 = 0.5_long*coeff*c1*sl*w/sqrt(dh)
         else
            ddebi0 = -0.5_long*coeff*c1*sl*w/sqrt(dh)
         endif
      end if
   end if
end function ddebi0


function debit2(H1,H2,SL,W,CC)
!==============================================================================
!           calcul du débit sous une vanne de fond en charge
!                       formule complète
!
!--->h1 = hauteur d'eau amont au dessus du seuil
!--->h2 = hauteur d'eau aval au dessus du seuil
!--->sl = longueur de la vanne
!--->w = ouverture de la vanne
!--->cc = coefficient de contraction
!==============================================================================
   implicit none
   ! -- le prototype --
   real(kind=long) :: debit2
   real(kind=long),intent(in) :: h1,h2,sl,w,cc
   ! -- les variables --
   real(kind=long), parameter :: rg2 = sqrt(2._long*g)
   real(kind=long) :: ccw,hv,x1
   !------------------------------------------------------------------------------
   ccw = cc*w
   hv = hw(h1,h2,ccw)
   if (hv>ccw+0.001_long) then  !vanne noyée
      x1 = ccw/h1
      debit2 = rg2*sl*ccw*sqrt(h1-hv)/sqrt(1._long-x1*x1)
   else  !vanne dénoyée
      debit2 = rg2*sl*ccw*sqrt(h1-ccw)
   endif
end function debit2


function debit3(nk,t)
!==============================================================================
!    Évaluation du débit de la pompe nk au temps t
!==============================================================================
   implicit none
   ! -- le prototype --
   real(kind=long) :: debit3
   integer,intent(in) :: nk
   real(kind=long),intent(in) :: t
   ! -- les variables --
   real(kind=long) :: Qnom, next_start, next_stop, dt_up, dt_down
   !------------------------------------------------------------------------------
   if (all_OuvEle(nk)%iuv /= 3) then
      write(error_unit,*) '>>>> ERREUR : l''ouvrage élémentaire ',all_OuvEle(nk)%cvar,' n''est pas une pompe'
      stop 1005
   else
      Qnom = all_OuvEle(nk)%uv1
      next_start = all_OuvEle(nk)%uv2
      next_stop = all_OuvEle(nk)%uv3
      dt_up = all_OuvEle(nk)%uv4
      dt_down = all_OuvEle(nk)%uv5
      if (t>next_start .and. t<next_stop+dt_down) then
         if (t<next_start+dt_up) then
            debit3 = Qnom*(t-next_start)/dt_up
         else if (t<next_stop) then
            debit3 = Qnom
         else
            debit3 = Qnom*(next_stop+dt_down-t)/dt_down
         endif
      else
         debit3 = zero
      endif
   endif
end function debit3


function debit4(H1,H2,SL,W,CD)
!==============================================================================
!           calcul du débit sous une vanne de fond en charge
!                       formule simplifiée
!
!--->h1 = hauteur d'eau amont au dessus du seuil
!--->h2 = hauteur d'eau aval au dessus du seuil
!--->sl = longueur de la vanne
!--->w = ouverture de la vanne
!--->cd = coefficient de débit et de transition noyé/dénoyé
!         (h2 > cd*w ===> noyé)
!==============================================================================
   implicit none
   ! -- le prototype --
   real(kind=long) :: debit4
   real(kind=long),intent(in) :: h1,h2,sl,w,cd
   ! -- les variables --
   real(kind=long), parameter :: rg2 = sqrt(2._long*g)
   real(kind=long) :: cw
   !------------------------------------------------------------------------------
   cw = cd*w
   if (h2<cw) then  !vanne dénoyée
      debit4 = cw*rg2*sl*sqrt(h1-cw)
   else  !vanne noyée
      debit4 = cw*rg2*sl*sqrt(h1-h2)
   endif
end function debit4


function debit5(H,SL,CD,W)
!==============================================================================
!           calcul du débit sur un déversoir latéral
!
!--->h = hauteur d'eau au dessus du déversoir
!--->sl = longueur du déversoir
!--->cd = coefficient de débit
!--->w = ouverture de l'orifice
!==============================================================================
   implicit none
   ! -- le prototype --
   real(kind=long) :: debit5
   real(kind=long),intent(in) :: h,sl,cd,w
   ! -- les constantes --
   real(kind=long), parameter :: rg2 = sqrt(2._long*g)
   real(kind=long),parameter :: alp = 1.5_long
   !------------------------------------------------------------------------------
   if (h>0.001_long .and. h<w) then
      !--->le déversoir fonctionne à surface libre
      debit5 = cd*rg2*sl*h**alp
   else if (h>0.001_long .and. h>=w) then
      !--->le déversoir fonctionne en charge
      debit5 = cd*rg2*sl*(h**alp-(h-w)**alp)
   else
       !--->le déversoir ne fonctionne pas
      debit5 = zero
   endif
end function debit5


function debit7(H1,H2,SL,HR,HV,Coeff)
!==============================================================================
!            calcul du débit à travers un orifice voute (fer à cheval)
!
!--->h1 = hauteur d'eau amont au dessus du radier de l'orifice
!--->h2 = hauteur d'eau aval au dessus du radier de l'orifice
!--->sl = largeur déversante de l'écoulement
!--->hr = hauteur de la partie rectangulaire
!--->hv = hauteur de la partie voutée (hauteur totale = hr + hv)
!--->coeff = coefficient de débit
!
!------Principe de calcul : idem que pour un déversoir-orifice rectangulaire
!                           en remplacant le produit L*H1 par la surface
!                           de la section d'écoulement
!==============================================================================
   implicit none
   ! -- prototype --
   real(kind=long) :: debit7
   real(kind=long),intent(in) :: h1,h2,sl,hr,hv,coeff
   ! -- constante --
   real(kind=long),parameter :: c0 = sqrt(2._long*g)
   real(kind=long),parameter :: c1 = c0 * 3._long*sqrt(3._long)/2._long
   ! -- variables --
   real(kind=long) :: temp
   !-----------------------------------------------------------------------------
   if (h1<zero) then
      debit7 = zero
   else
      temp = hr+hv
      if (h2 < h1*deuxtiers) then
         if (h1 < temp) then
            !---régime surface libre dénoyé
            debit7 = coeff*c0*svoute(sl,hr,hv,h1)*sqrt(h1)
         else
            !---régime charge dénoyé
            debit7 = coeff*c0*svoute(sl,hr,hv,temp)*sqrt(h1)
         end if
      else
         if (h1 < temp) then
            !---régime surface libre noyé
            debit7 = coeff*c1*svoute(sl,hr,hv,h1)*sqrt(h1-h2)
         else
            !---régime charge noyé
            debit7 = coeff*c1*svoute(sl,hr,hv,temp)*sqrt(h1-h2)
         end if
      end if
   endif
end function debit7


function SVoute(SL,HR,HV,H1)
!==============================================================================
!          calcul de la surface d'écoulement dans un orifice-voute
!
!--->SL = largeur de la partie rectangulaire
!--->HR = Hauteur de la partie rectangulaire
!--->HV = hauteur de la partie voutée circulaire (hauteur totale = HR + HV)
!--->H1 = hauteur
!--->Contrainte : 2 * HV < SL sinon la voute se referme !
!==============================================================================
   implicit none
   ! -- prototype --
   real(kind=long) :: svoute
   real(kind=long),intent(in) :: sl,hr,hv,h1
   ! -- variables --
   real(kind=long) :: deltah, deltas
   real(kind=long) :: rayon, theta0, theta1
   !------------------------------------------------------------------------------
   if (HV < 0.001_long) then   ! cas voute rectangulaire !!!
      SVoute = SL*min(H1,HR)
   else
      if (2._long*HV < SL) then ! angle d'ouverture de la section circulaire
         Theta0 = 2._long*atan((4._long*HV*SL)/(SL*SL-4._long*HV*HV))
      else
         Theta0 = Pi  ! voute en demi cercle => angle = Pi (max possible)
      endif
      Rayon = SL*SL/(8._long*HV)+0.5_long*HV   ! rayon de courbure de la voute
      if (H1 > HR) then   ! eau au dessus de la partie rectangulaire
         DeltaH = max(Zero,HR+HV-H1)  ! = 0 si H1 > HR+HV
         Theta1 = 2._long*acos(1._long-DeltaH/Rayon)
         DeltaS = 0.5_long*Rayon*Rayon
         !  DeltaS est l'aire comprise entre le niveau HR (sommet du rectangle) et le
         !  niveau de l'eau H1. C'est l'aire entre la voute et le niveau HR moins l'aire
         !  entre la voute et le niveau H1.
         DeltaS = DeltaS*(Theta0-Theta1-sin(Theta0)+sin(Theta1))
         SVoute = SL*HR + DeltaS
      else                      ! eau seulement dans la partie rectangulaire
         SVoute = SL*H1
      endif
   endif
end function SVoute


function debit8(H1,H2,D,HB,Coeff)
!==============================================================================
!                  calcul du débit à travers une buse
!
!--->h1 = hauteur d'eau amont au dessus de l'envasement de la buse
!--->h2 = hauteur d'eau aval au dessus de l'envasement de la buse
!--->d = diamètre de la buse
!--->hb = hauteur envasée de la buse
!--->coeff = coefficient de débit
!
!------principe de calcul : idem que pour un déversoir-orifice rectangulaire
!                           en remplacant le produit l*h1 par la surface
!                           de la section d'écoulement

! TODO: Vérifier qu'on a bien la continuité au changement de régime. Cf. déversoir trapézoïdal.
!==============================================================================
   implicit none
   ! -- prototype --
   real(kind=long) :: debit8
   real(kind=long),intent(in) :: h1,h2,d,hb,coeff
   ! -- constantes --
   real(kind=long),parameter :: c0 = sqrt(2._long*g)
   real(kind=long),parameter :: c1 = c0 * 3._long*sqrt(3._long)/2._long
   real(kind=long),parameter :: eps = 0.1_long
   ! -- variables --
   real(kind=long) :: dh
   real(kind=long) :: he
   real(kind=long) :: sb
   !------------------------------------------------------------------------------
   if (h1 < zero) then
      debit8 = zero
   else
      he = h1+hb  !hauteur d'eau par rapport au fond de la buse
      if (h2 < h1*deuxtiers) then
         if (he < d) then  !régime surface libre dénoyé
            sb = sbuse(d,he)-sbuse(d,hb)
            debit8 = coeff*c0*sb*sqrt(h1)
         else              !régime charge dénoyé
            sb = pi*d*d*0.25_long-sbuse(d,hb)
            debit8 = coeff*c0*sb*sqrt(h1)
         end if
      else
         dh = h1-h2
         if (he < d) then  !régime surface libre noyé
            sb = sbuse(d,he)-sbuse(d,hb)
            debit8 = coeff*c1*sb*sqrt(dh)
         else              !régime charge noyé
            sb = pi*d*d*0.25_long-sbuse(d,hb)
            debit8 = coeff*c1*sb*sqrt(dh)
         end if
      end if
   endif
end function debit8


function sbuse(d,h)
!==============================================================================
!   calcul de la surface comprise entre un cercle et une corde
! Référence : Formules et tables de mathématiques (serie Schaum), numero 4.21
!
!--->D = diamètre du cercle
!--->H = distance entre la circonférence et la corde
!==============================================================================
   implicit none
   ! -- le prototype --
   real(kind=long) :: sbuse
   real(kind=long),intent(in) :: d,h
   ! -- les variables --
   real(kind=long) :: a,sinus, signe
   real(kind=long) :: r
   real(kind=long) :: angle
   real(kind=long), parameter :: rac2 = 1._long/sqrt(2._long)
   !------------------------------------------------------------------------------
   r = 0.5_long*d
   if (h < r) then   !calcul direct de la surface
      a = (r-h)/r
      sbuse = zero
      signe = 1._long
   else             !calcul intermédiaire de la surface complementaire
      a = (h-r)/r
      sbuse = pi*r*r
      signe = -1._long
   endif
   sinus = 2._long*a*sqrt(1._long-a*a)
   if (a > rac2) then  ! a>rac2 correspond à un angle > pi/2
      angle = asin(sinus)! attention asin() a ses valeurs entre -pi/2 et +pi/2
   else
      angle = pi-asin(sinus)
   endif
   sbuse = sbuse+signe*r*r*(angle-sinus)*0.5_long
end function sbuse


function debit9(H1,H2,AL0,W,Coeff,B)
!==============================================================================
!                  calcul du débit sur un déversoir trapézoïdal
!
!---> h1    = hauteur d'eau amont
!---> h2    = hauteur d'eau aval
!---> al0   = longueur déversante minimale
!---> w     = hauteur de mise en charge
!---> coeff = coefficient de débit
!---> b     = pente des berges supposées identiques = 1/2 * dl/dh
!
!------principe de calcul : idem que pour un déversoir-orifice rectangulaire
!                           en remplacant le produit l*h1 par la surface
!                           de la section d'écoulement
!      la transition noyé / dénoyé a lieu pour la valeur de h2 qui réalise le
!      maximum du débit avec la formule du régime noyé.
!
!==============================================================================
   implicit none
   ! -- le prototype --
   real(kind=long) :: debit9
   real(kind=long),intent(in) :: h1,h2,al0,w,coeff,b
   ! -- les variables --
   real(kind=long) :: c,d,d0,h2min,sb,cn
   ! -- les constantes --
   real(kind=long),parameter :: c0 = sqrt(2._long*g)
   !------------------------------------------------------------------------------
   if (h1 < zero) then
      debit9 = zero
      return
   else
      c  = b*h1
      d0 = 4._long*c-3._long*al0
      d  = d0*d0+40._long*c*al0
      h2min = (d0+sqrt(d))/(10._long*b)
      if (h2min < zero) then
         write(output_unit,*) ' >>>> ERREUR dans le calcul de la transition', &
                         ' noyé/dénoyé pour un déversoir trapézoïdal'
         write(output_unit,*) ' H2(dénoyé)  = ',h2min
         write(output_unit,*) ' Hamont      = ',h1
         write(output_unit,*) ' Haval       = ',h2
         write(output_unit,*) ' Longueur    = ',al0
         write(output_unit,*) ' Ouverture   = ',w
         write(output_unit,*) ' Coeff debit = ',coeff
         write(output_unit,*) ' Cotangente  = ',b
         write(error_unit,*) ' >>>> ERREUR dans le calcul de la transition noyé', &
                    '/dénoyé pour un déversoir trapézoïdal => voir fichier TRA'
         stop 1009
      elseif (h2min > h1) then
         write(output_unit,*) ' >>>> ERREUR dans le calcul de la transition', &
                         ' noyé/dénoyé pour un déversoir trapézoïdal'
         write(output_unit,*) ' H2(dénoyé)  = ',h2min
         write(output_unit,*) ' Hamont      = ',h1
         write(output_unit,*) ' Haval       = ',h2
         write(output_unit,*) ' Longueur    = ',al0
         write(output_unit,*) ' Ouverture   = ',w
         write(output_unit,*) ' Coeff debit = ',coeff
         write(output_unit,*) ' Cotangente  = ',b
         write(error_unit,*) ' >>>> ERREUR dans le calcul de la transition noyé', &
                    '/dénoyé pour un déversoir trapézoïdal => voir fichier TRA'
         stop 1010
      elseif (h2 < h2min) then  !dénoyé
         sb = strap(al0,b,h1,w)
         debit9 = coeff*c0*sb*sqrt(h1)
      else                  !noyé
         sb = strap(al0,b,h2,w)
         cn = strap(al0,b,h1,w)*sqrt(h1) / (strap(al0,b,h2min,w)*sqrt(h1-h2min))
         debit9 = cn*coeff*c0*sb*sqrt(h1-h2)
      endif
   endif
end function debit9


function sTrap(AL0,B,H,W)
!==============================================================================
!   calcul de la surface déversante sur un déversoir trapézoïdal
!
!--->AL0 = longueur déversante minimale
!--->B = fruit (inverse de la pente) des joues du déversoir = (0.5*DL)/DH
!--->H = hauteur déversante
!--->W = hauteur de mise en charge
!==============================================================================
   implicit none
! -- le prototype --
   real(kind=long) :: strap
   real(kind=long),intent(in) :: al0, b, h, w
! -- les variables --
   real(kind=long) :: h0
!------------------------------------------------------------------------------
   h0 = min(h,w)
   strap = h0*(al0+h0*b)
end function sTrap


! function debitx(z1,z2,p1,p2,p3,p4,p5)
! !==============================================================================
! !
! !                   ouvrage défini par l'utilisateur
! !
! !==============================================================================
!    implicit none
!    ! -- prototype --
!    real(kind=long) :: debitx
!    real(kind=long),intent(in) :: z1,z2,p1,p2,p3,p4,p5
!    !------------------------------------------------------------------------------
!    write(output_unit,*) '>>>> ERREUR : fonction debitx() non définie. Liste des paramètres : ',z1,z2,p1,p2,p3,p4,p5
!    debitx = zero
!    if (z1 < z2) debitx = -debitx
!    stop 1001
! end function debitx


subroutine regime(NK,t,Zam,Zav,DBITH,CAR,CVN)
!==============================================================================
!   Détermination du régime de fonctionnement des ouvrages élémentaires
!
!--->T   = date en secondes (nécessaire pour les pompes)
!--->Zam = niveau d'eau amont (cote)
!--->Zav = niveau d'eau aval (cote)
!--->DbiTh = débit théorique sur l'ouvrage
!--->CAR = étiquette indiquant le régime de fonctionnement
!--->CVN = rapport du débit théorique au débit dénoyé
!          débit théorique pour les pompes et déversoirs latéraux
!==============================================================================
   implicit none
   ! -- le prototype --
   integer,intent(in) :: nk
   character(len=6),intent(out) :: car
   real(kind=long),intent(in) :: t,Zam,Zav,dbith
   real(kind=long),intent(out) :: cvn
   ! -- les variables --
   real(kind=long) :: cd,h1,h2,hr,hv,sl,w
   !------------------------------------------------------------------------------
   sl = all_OuvEle(nk)%uv1
   w  = all_OuvEle(nk)%uv3
   cd = all_OuvEle(nk)%uv4
   h1 = Zam-all_OuvEle(nk)%uv2  !hauteur d'eau amont au dessus de la cote de déversement
   h2 = Zav-all_OuvEle(nk)%uv2  !hauteur d'eau aval au dessus de la cote de déversement

   select case (all_OuvEle(nk)%iuv)
      case (0,6)  !déversoir-orifice
         if (h1<=zero .and. h2<=zero) then
            car = ' RIEN '
            cvn = zero
         else if (h1>h2) then
            call regim0(h1,h2,w,car)
            cvn = dbith/debit0(h1,zero,sl,w,cd)
         else
            call regim0(h2,h1,w,car)
            cvn = -dbith/debit0(h2,zero,sl,w,cd)
         endif
      case (1)   !porte à flot ou clapet
         if (h1>h2) then
            car = 'OUVERT'
            cvn = dbith/debit0(h1,zero,sl,w,cd)
         else
            car = 'FERMÉ'
            cvn = zero
         endif
      case (2)   !vanne de fond en charge
         call regim2(h1,h2,w,cd,car)
         cvn=dbith/debit2(max(h1,h2),cd*w,sl,w,cd)
      case (3)   !pompe
         call regim3(nk,t,car,cvn)
      case (4)   !vanne de fond en charge (formulation simplifiée)
         call regim4(h1,h2,w,cd,car)
         cvn=dbith/debit4(max(h1,h2),zero,sl,w,cd)
      case (5)   !déversoir latéral
         if (h1<zero .and. h2<zero) then
            car = ' RIEN '
            cvn = zero
         elseif (h1>h2) then
            car = 'SORTIE'
            cvn = dbith
         else
            car = 'RETOUR'
            cvn = dbith
         endif
      case (7)   !porte à flot ou clapet inversé
         if (h2>h1) then
            car = 'OUVERT'
            cvn = -dbith/debit0(h2,zero,sl,w,cd)
         else
            car = 'FERMÉ'
            cvn = zero
         endif
      case (8)   !buse
         if (h1<=zero .and. h2<=zero) then
            car = ' RIEN '
            cvn = zero
         else if (h1>h2) then
            call regim8(h1,h2,sl,w,car)
            cvn = dbith/debit8(h1,zero,sl,w,cd)
         else
            call regim8(h2,h1,sl,w,car)
            cvn = -dbith/debit8(h2,zero,sl,w,cd)
         endif
      case (9)   !ouvrage X
         car = '      '
         cvn = zero
      case (10)  !déversoir trapézoïdal
         if (h1<=zero .and. h2<=zero) then
            car = ' RIEN '
            cvn = zero
         else if (h1>h2) then
            cvn = dbith/debit9(h1,zero,sl,w,cd,all_OuvEle(nk)%uv5)
            if (abs(cvn) < 0.99_long) then
               car = ' SLNY '
            else
               car = ' SLDN '
            endif
         else
            cvn = -dbith/debit9(h2,zero,sl,w,cd,all_OuvEle(nk)%uv5)
            if (abs(cvn) < 1.0_long) then
               car = ' SLNY '
            else
               car = ' SLDN '
            endif
         endif
      case (11)  !orifice voute (fer à cheval)
         sl = all_OuvEle(nk)%uv1
         hr = all_OuvEle(nk)%uv3
         hv = all_OuvEle(nk)%uv4
         cd = all_OuvEle(nk)%uv5
         if (h1<=zero .and. h2<=zero) then
            car = ' RIEN '
            cvn = zero
         else if (h1>h2) then
            call regim7(h1,h2,hr,hv,car)
            cvn = dbith/debit7(h1,zero,sl,hr,hv,cd)
         else
            call regim7(h2,h1,hr,hv,car)
            cvn = -dbith/debit7(h2,zero,sl,hr,hv,cd)
         endif
      case (91)  !perte de charge singulière à la Borda
         car = ' BORDA'
         cvn = all_OuvEle(nk)%uv5
      case default
         write(output_unit,*) '>>>> BUG dans regime() : on ne devrait pas arriver ici !'
         stop 1002
   end select
end subroutine regime


subroutine regim0(H1,H2,W,Car)
!==============================================================================
!           Régime de fonctionnement d'un déversoir-orifice
!                 (déversoir avec vanne de surface)
!
!--->h1 = hauteur d'eau amont au dessus du déversoir
!--->h2 = hauteur d'eau aval au desus du déversoir
!--->w  = ouverture de l'orifice (vanne de surface)
!==============================================================================
   implicit none
   ! -- le prototype --
   real(kind=long),intent(in) :: h1,h2,w
   character(len=6),intent(out) :: car
   ! -- les variables --
   real(kind=long) :: hdn,hpn
   !------------------------------------------------------------------------------
   hdn = h1*deuxtiers
   if (h1 < w) then  !écoulement à surface libre
      if (h2 < hdn) then     !surface libre dénoyé
         car = ' SLDN '
      else                   !surface libre noyé
         car = ' SLNY '
      end if
   else               !écoulement en charge
      hpn = (2._long*h1 + w)/3._long
      if (h2<hdn) then       !charge dénoyé
         car = ' CHDN '
      else if (h2<hpn) then  !charge partiellement noyé
         car = ' CHPN '
      else                   !charge totalement noyé
         car = ' CHNY '
      end if
   end if
end subroutine regim0


subroutine regim2(H1,H2,W,CC,Car)
!==============================================================================
!       Régime de fonctionnement d'une vanne de fond en charge
!                       formule complète
!
!--->h1 = hauteur d'eau amont au dessus du seuil
!--->h2 = hauteur d'eau aval au dessus du seuil
!--->w = ouverture de la vanne
!--->cc = coefficient de contraction
!==============================================================================
   implicit none
   ! -- prototype --
   real(kind=long),intent(in) :: h1,h2,w,cc
   character(len=6),intent(out) :: car
   ! -- variables --
   real(kind=long) :: ccw,hv
   !------------------------------------------------------------------------------
   ccw = cc*w
   if (h1 > h2) then
      hv = hw(h1,h2,ccw)
    else
      hv = hw(h2,h1,ccw)
   endif
   if (hv > ccw+0.001_long) then  !vanne noyée
      car = ' CHNY '
   else                           !vanne dénoyée
      car = ' CHDN '
   endif
end subroutine regim2


subroutine regim3(NK,t,Car,CVN)
!==============================================================================
!                  Régime de fonctionnement d'une pompe
!==============================================================================
   implicit none
   ! -- le prototype --
   integer,intent(in) :: nk
   real(kind=long),intent(in) :: t
   character(len=6),intent(out) :: car
   real(kind=long),intent(out) :: cvn
   ! -- les variables --
   real(kind=long) :: Qnom, next_start, next_stop, dt_up, dt_down
   !------------------------------------------------------------------------------
   Qnom = all_OuvEle(nk)%uv1
   next_start = all_OuvEle(nk)%uv2
   next_stop = all_OuvEle(nk)%uv3
   dt_up = all_OuvEle(nk)%uv4
   dt_down = all_OuvEle(nk)%uv5
   if (t>next_start .and. t<next_stop+dt_down) then
      car = 'MARCHE'
      if (t<next_start+dt_up) then
         cvn = Qnom*(t-next_start)/dt_up
      else if (t<next_stop) then
         cvn = Qnom
      else
         cvn = Qnom*(next_stop+dt_down-t)/dt_down
      endif
   else
      car = ' ARRET'
      cvn = zero
   endif
end subroutine regim3


subroutine regim4(H1,H2,W,CD,Car)
!==============================================================================
!       Régime de fonctionnement d'une vanne de fond en charge
!                       formule simplifiée
!
!--->h1 = hauteur d'eau amont au dessus du seuil
!--->h2 = hauteur d'eau aval au dessus du seuil
!--->w = ouverture de la vanne
!--->cd = coefficient de debit et de transition noyé/dénoyé
!         (h2 > cd*w ===> noye)
!==============================================================================
   implicit none
   ! -- le prototype --
   real(kind=long),intent(in) :: h1,h2,w,cd
   character(len=6),intent(out) :: car
   ! -- les variables --
   real(kind=long) :: cw
   real(kind=long) :: h
   !------------------------------------------------------------------------------
   cw = cd*w
   h = max(h1,h2)
   if (h < cw) then  !vanne dénoyée
      car = ' CHDN '
   else              !vanne noyée
      car = ' CHNY '
   endif
end subroutine regim4


subroutine regim7(H1,H2,HR,HV,Car)
!==============================================================================
!           Régime de fonctionnement d'un orifice voute (fer à cheval)
!
!--->h1 = hauteur d'eau amont
!--->h2 = hauteur d'eau aval
!--->sl = largeur déversante de l'écoulement
!--->hr = hauteur de la partie rectangulaire
!--->hv = hauteur de la partie voutée (hauteur totale = hr + hv)
!==============================================================================
   implicit none
   ! -- prototype --
   character(len=6),intent(out) :: car
   real(kind=long),intent(in) :: h1,h2,hr,hv
   !------------------------------------------------------------------------------
   if (h1 < zero) then
      car = ' RIEN '
      return
   end if
   if (h2 < h1*deuxtiers) then
      if (h1 < hr+hv) then  !régime surface libre dénoyé
         car = ' SLDN '
      else                  !régime charge dénoyé
         car = ' CHDN '
      end if
   else
      if (h1 < hr+hv) then  !régime surface libre noyé
         car = ' SLNY '
      else                  !régime charge noyé
         car = ' CHNY '
      end if
   end if
end subroutine regim7


subroutine regim8(H1,H2,D,HB,CAR)
!==============================================================================
!                  Régime de fonctionnement d'une buse
!
!--->h1 = hauteur d'eau amont au dessus de l'envasement de la buse
!--->h2 = hauteur d'eau aval au dessus de l'envasement de la buse
!--->d  = diametre de la buse
!--->hb = hauteur envasée de la buse
!==============================================================================
   implicit none
   ! -- prototype --
   real(kind=long),intent(in) :: h1,h2,d,hb
   character(len=6),intent(out) :: car
   ! -- variables --
   real(kind=long) :: he
   !------------------------------------------------------------------------------
   he = h1+hb  !hauteur d'eau par rapport au fond de la buse
   if (h1 < zero) then
      car = ' RIEN '
      return
   end if
   if (h2 < h1*deuxtiers) then
      if (he < d) then    !régime surface libre dénoyé
         car = ' SLDN '
      else                !régime charge dénoyé
         car = ' CHDN '
      end if
   else
      if (he < d) then    !régime surface libre noyé
         car = ' SLNY '
      else                !régime charge noyé
         car = ' CHNY '
      end if
   end if
end subroutine regim8


function hw(h1,h2,ccw)
!==============================================================================
!         calcul de la hauteur d'eau intermédiaire pour une vanne de fond
!         en fonction des hauteurs amont et aval et de la lame contractée
!
!--->h1=hauteur d'eau amont au dessus du déversoir
!--->h2=hauteur d'eau aval au dessus du déversoir
!--->ccw=lame contractée
!==============================================================================
   implicit none
   ! -- prototype --
   real(kind=long) :: hw
   real(kind=long),intent(in) :: h1,h2,ccw
   ! -- variables --
   real(kind=long) :: a,alpha,b,cv,delta,dhp
   real(kind=long) :: fp,h12,h22,hvm,hvp,sdelta,x1,x2
   !------------------------------------------------------------------------------
   x1 = ccw/h1
   x2 = ccw/h2
   cv = un/(un-x1*x1)
   alpha = cv
   a = 2._long*ccw*(un-x2)*cv
   b = 2._long*a*h1-h2*h2

   delta = a*a-b  !résolution du trinome x*x-2*a*x+b = 0
   if (delta < zero) then  !vanne dénoyée
      hw = ccw
      return
   else           !2 solutions possibles
      sdelta = sqrt(delta)
      hvp = a+sdelta
      hvm = a-sdelta
      if (hvp < ccw) then !vanne dénoyée
         hw = ccw
         return
      else if (hvm < ccw) then  !vanne noyée : élimination de la solution non-physique
         hw = hvp
         return
      else  !choix selon perte de charge et hauteur intermédiaire
         hw = hvp
         fp = cv*ccw*ccw*(h1-hvp)
         h12 = h1*h1       ;  h22 = h2*h2
         dhp = h1+fp/h12 - h2+fp/h22
         if (dhp < hvp*alpha) then
            hw = hvp
         else
            hw = hvm
         end if
      end if
   end if
end function hw


function position_ouvrage(time,n,mod92)
!==============================================================================
!            position des singularités en fonction du temps
!==============================================================================
   implicit none
   ! -- le prototype --
   real(kind=long) :: position_ouvrage
   real(kind=long),intent(in) :: time
   integer,intent(in) :: n
   logical,intent(in) :: mod92
   ! -- les variables --
   integer :: ir, irec1, irec2, nw, i
   real(kind=long),allocatable,save :: dwn(:), wn(:), t1(:), t2(:)
   real(kind=long) :: tin, tjn, tmn, t0n
   real(kind=long) :: zin, zjn, zmn, z0n
   character(len=180) :: err_message
   type(c_tm) :: tm
   !---------------------------------------------------------------------------
   position_ouvrage = huge(zero)
   if (position_ouvrage_first_call) then   !initialisation
      nw = size(all_OuvEle)
      allocate (t1(nw),t2(nw),wn(nw),dwn(nw))
      do nw = 1, size(all_OuvEle)
         if (all_OuvEle(nw)%itw > -1) then
            t1(nw) = -1.e+30_long
            t2(nw) = -1.e+20_long
            wn(nw) = zero
            dwn(nw) = zero
         endif
      enddo
      position_ouvrage_first_call = .false.
   endif
   if (all_OuvEle(n)%itw < 0) then  !vérification
      write(err_message,*) ' >>>> erreur dans position_ouvrage (bug)',n,all_OuvEle(n)%itw
      write(error_unit,'(a)') trim(err_message) ; write(lTra,'(a)') trim(err_message)
      write(err_message,'(a)') 'Merci d''envoyer un rapport de bug'
      write(error_unit,'(a)') trim(err_message) ; write(lTra,'(a)') trim(err_message)
      stop 202
   endif

   if (mod92) then   !VAR a été relu
      t2(n) = -1.675e+20_long     !on oublie les pointeurs actuels
   else if (all_OuvEle(n)%irf > 0) then   !régulation en cote
      t2(n) = -1.675e+20_long     !on oublie les pointeurs actuels
   else if (all_OuvEle(n)%reg) then ! la régulation a modifié les données
      if (all_OuvEle(n)%iuv == 3) then
         position_ouvrage = zjn  !interpolation inutile pour une pompe
      else
         dwn(n) = (all_OuvEle(n)%wtz_reg(2)-all_OuvEle(n)%wtz_reg(1))/(all_OuvEle(n)%tz_reg(2)-all_OuvEle(n)%tz_reg(1))
         position_ouvrage = all_OuvEle(n)%wtz_reg(1)+dwn(n)*(time-all_OuvEle(n)%tz_reg(1))
         all_OuvEle(n)%reg = .false.
         ! on ne change pas les pointeurs
         return
      endif
   else
      continue
   endif
   if (time >= t1(n) .and. time < t2(n)) then !inutile de chercher plus loin
      if (all_OuvEle(n)%iuv == 3) then
         position_ouvrage = wn(n)  !interpolation inutile pour une pompe
      else if (abs(dwn(n)) > 0.00001_long) then
         position_ouvrage = wn(n)+dwn(n)*(time-t1(n))
      else
         position_ouvrage = -99999.9_long  !--->cet ouvrage ne bouge pas
      endif
      return
   endif
   !---recherche des pointeurs qui encadrent time
   ! DONE: il faudrait supprimer le fichier à accès direct 92 comme on l'a fait pour inimage.geo
   ! NOTE: si cela est fait il faut récrire la routine change_position() DONE
   irec1 = all_OuvEle(n)%itw
   irec2 = all_OuvEle(n)%jtw
   if (all_OuvEle(n)%w_cycle <= 0) then
      t0n = all_OuvEle(n)%tz(irec1)
      tmn = all_OuvEle(n)%tz(irec2)
      z0n = all_OuvEle(n)%wtz(irec1)
      zmn = all_OuvEle(n)%wtz(irec2)
      if (time < t0n) then
          position_ouvrage = z0n
          wn(n) = z0n
          dwn(n) = zero
          t1(n) = -1.e+30_long  !nouveau pointeur
          t2(n) = t0n           !nouveau pointeur
          return
      else if (time >= tmn) then
          position_ouvrage = zmn
          wn(n) = zmn
          dwn(n) = zero
          t1(n) = -1.e+30_long
          t2(n) = 1.e+30_long
          return
      endif
   else
      ! si initialisation bien faite, on n'a jamais
      ! all_OuvEle(n)%tz(irec1) >= time
      do while (all_OuvEle(n)%tz(irec2) <= time)
        select case (all_OuvEle(n)%w_cycle)
        case(1)
           do i=irec1,irec2
              tm = get_tm(all_OuvEle(n)%tz(i))
              tm%tm_year = tm%tm_year + 1
              all_OuvEle(n)%tz(i) = c_mktime(tm)
           enddo
        case(2)
           do i=irec1,irec2
              tm = get_tm(all_OuvEle(n)%tz(i))
              tm%tm_mon = tm%tm_mon + 1
              all_OuvEle(n)%tz(i) = c_mktime(tm)
           enddo
        case(3)
           do i=irec1,irec2
              tm = get_tm(all_OuvEle(n)%tz(i))
              tm%tm_mday = tm%tm_mday + 7
              all_OuvEle(n)%tz(i) = c_mktime(tm)
           enddo
        case(4)
           do i=irec1,irec2
              tm = get_tm(all_OuvEle(n)%tz(i))
              tm%tm_mday = tm%tm_mday + 1
              all_OuvEle(n)%tz(i) = c_mktime(tm)
           enddo
        end select
      end do
      t0n = all_OuvEle(n)%tz(irec1)
      tmn = all_OuvEle(n)%tz(irec2)
      z0n = all_OuvEle(n)%wtz(irec1)
      zmn = all_OuvEle(n)%wtz(irec2)
   endif
   ! portion commune
   tjn = t0n
   zjn = z0n
   do ir = irec1+1, irec2
     tin = all_OuvEle(n)%tz(ir)
     zin = all_OuvEle(n)%wtz(ir)
     if (time >= tjn .and. time < tin) then
         if (all_OuvEle(n)%iuv == 3) then
           position_ouvrage = zjn  !interpolation inutile pour une pompe
         else
           dwn(n) = (zin-zjn)/(tin-tjn)
           position_ouvrage = zjn+dwn(n)*(time-tjn)
         endif
         wn(n) = zjn
         t1(n) = tjn  !nouveau pointeur
         t2(n) = tin  !nouveau pointeur
         return
     else
         tjn = tin
         zjn = zin
     endif
   enddo

   if (position_ouvrage > huge(zero)/2) stop '>>>> BUG dans position_ouvrage()'
end function position_ouvrage


subroutine change_position(ne,w1,w2,t1,t2)
!==============================================================================
!        modification de la position d'un ouvrage mobile
!==============================================================================
   implicit none
   ! -- le prototype --
   integer,intent(in) :: ne
   real(kind=long),intent(in) :: t1, t2, w1, w2
   !------------------------------------------------------------------------------

   all_OuvEle(ne)%wtz_reg(1) = w1
   all_OuvEle(ne)%wtz_reg(2) = w2
   all_OuvEle(ne)%tz_reg(1) = t1
   all_OuvEle(ne)%tz_reg(2) = t2
   all_OuvEle(ne)%reg = .true.
end subroutine change_position


subroutine update_positions_ouvrages(mod92,t,zt)
!==============================================================================
!            position des singularités en fonction du temps
!
!==============================================================================
   implicit none
   ! -- le prototype --
   logical, intent(in) :: mod92
   real(kind=long), intent(in) :: t
   real(kind=long), intent(in) :: zt(*)
   ! -- les variables --
   logical :: bool
   integer :: k, nk, ns
   real(kind=long) :: a, p1
   real(kind=long), parameter :: w_inf = 0.001_long
   !---------------------------------------------------------------------------
   if (varFile == '') return

   do ns = 1, size(all_OuvCmp)
      do k = 1, all_OuvCmp(ns)%ne
         nk = all_OuvCmp(ns)%OuEl(k,1)  !numéro de l'ouvrage élémentaire
         if (all_OuvEle(nk)%itw < 0) cycle
         bool = all_OuvEle(nk)%iuv == 0 .or. all_OuvEle(nk)%iuv == 2 .or. all_OuvEle(nk)%iuv == 4 &
                             .or. all_OuvEle(nk)%iuv == 6 .or. all_OuvEle(nk)%iuv == 9
         if (bool .and. all_OuvEle(nk)%irf > 0) then
            !---cas d'un ouvrage asservi à une cote
            a = position_ouvrage(zt(all_OuvEle(nk)%irf),nk,mod92)
         else
            !---cas d'un ouvrage programmé en temps
            a = position_ouvrage(t,nk,mod92)
         endif
         !--->on ne recalcule que les ouvrages élémentaires qui bougent
         if (a < -99999._long) cycle
         if (all_OuvEle(nk)%iuv == 0 .or. all_OuvEle(nk)%iuv == 5) then
            !--->déversoir-orifice à seuil mobile
            p1 = all_OuvEle(nk)%uv2
            all_OuvEle(nk)%uv2 = a
            all_OuvEle(nk)%uv3 = all_OuvEle(nk)%uv3+p1-all_OuvEle(nk)%uv2
         else if (all_OuvEle(nk)%iuv == 2 .or. all_OuvEle(nk)%iuv == 4) then
            !----vanne de fond
            all_OuvEle(nk)%uv3 = max(a, w_inf)  ! a est une ouverture qui doit être >0
         else if (all_OuvEle(nk)%iuv == 6 .or. all_OuvEle(nk)%iuv == 1) then
            !----orifice ou clapet à ouverture variable
            all_OuvEle(nk)%uv3 = max(a, w_inf)  ! a est une ouverture qui doit être >0
         else if (all_OuvEle(nk)%iuv == 7) then
            !----clapet inversé à ouverture variable
            all_OuvEle(nk)%uv3 = max(a, w_inf)  ! a est une ouverture qui doit être >0
         else if (all_OuvEle(nk)%iuv == 3) then
            !----pompes
            if (a > zero) then              !démarrage immédiat
               all_OuvEle(nk)%za1 = -990._long
               all_OuvEle(nk)%za2 = -999._long
            else                            !arrêt immédiat
               all_OuvEle(nk)%za1 = +9999._long
               all_OuvEle(nk)%za2 = +9990._long
            endif
         else if (all_OuvEle(nk)%iuv == 9) then
            !----ouvrage défini par l'utilisateur
            all_OuvEle(nk)%uv2 = a
         endif
      enddo
   enddo
end subroutine update_positions_ouvrages

end module Ouvrages
