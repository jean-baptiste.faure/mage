!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - INRAE - 1989-2020                           #
!# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################
module Parametres
!==============================================================================
!       Definition des dimensions maximales des tableaux, de constantes
!       numeriques et de divers constantes physiques et mathematiques.
!
!==============================================================================
   use, intrinsic :: iso_fortran_env, only: real32, real64, real128
   implicit none

   ! Numéro de version du programme : Majeure.Mineure.Révision - date
   character(len=20), parameter :: version_number='8.3.6 - 2024-05-17'
   integer, parameter :: mage_version = 83 !valeur numérique pour l'écriture de BIN
   character(len=80), parameter :: Authors='Jean-Baptiste Faure - INRAE - 1989-2022, Theophile Terraz - INRAE - 2022-2024'
   character(len=80), parameter :: DepotAPP='IDDN.FR.001.180002.000.S.P.2013.000.30200'
#ifdef lang_EN
   character(len=90), parameter :: Garantie1 = 'MAGE is provided without '// &
               'warranty of any kind, within the limits provided by law.'
   character(len=80), parameter :: Garantie2 = 'The results are the sole '// &
                                               'responsibility of the user.'
#else
   character(len=90), parameter :: Garantie1 = 'MAGE est fourni sans garantie '// &
                          'd''aucune sorte dans les limites prévues par la loi'
   character(len=80), parameter :: Garantie2 = 'Les résultats obtenus sont de '// &
                                     'la seule responsabilité de l''utilisateur'
#endif /* lang_EN */
   ! Nom du Systeme d'exploitation géré désormais par le makefile
   !      character(len=14),parameter :: OS = .....
!    include 'version.fi'  !gestion du type d'OS depuis le makefile
   character(len=1) :: slash

   !unités logiques prédéfinies
   integer, parameter :: lTra = 1  !fichier TRA
   integer, parameter :: lBin = 2  !fichier BIN
   integer, parameter :: lErr = 3  !fichier ERR
   integer, parameter :: lBinGra = 4  !fichier GRA
   integer, parameter :: l91 = 91  !fichier temporaire à accés direct inimage.qla
   integer, parameter :: l9 = 9    !fichier OUTPUT

   ! précision des réels (kind)
!  integer, parameter :: long=real128  !quadruple précision
   integer, parameter :: long=real64   !double précision
   integer, parameter :: sp=real32     !simple précision
   ! nb max de sections ou points de calcul
   integer, parameter :: issup=5501
   ! nb max de biefs ou branches
   integer, parameter :: ibsup=150
   integer, parameter :: ibsup2=2*ibsup
   ! nb max de nœuds = ibsup+1
   integer, parameter :: nosup=ibsup+1
   ! nb max d'ouvrages élémentaires par section singulière
   integer, parameter :: nesup=15
   ! nb max de fichiers de données
   integer, parameter :: nfich=23
   ! nb max de secteurs déversants
   integer, parameter :: nlasup=ibsup
   ! nombre de sous-sections par profil (== zones de strickler)
   ! NOTE: si on veut définir plus de 3 sous-sections par profil, il faut redéfinir la notion de lit moyen pour Debord et le nombre d'équations pour ISM
   integer, parameter :: nzsup = 3
   ! mnémoniques des indices de type de déversement : 0,1,2,...,
   integer, parameter :: total = 1     ! bilan des déversements
   integer, parameter :: rivg  = 2     ! déversement en rive gauche
   integer, parameter :: rivd  = 3     ! déversement en riv droite
   ! nb max de points pour décrire les lois hauteur-surface des nœuds à surface non nulle
   integer, parameter :: nnsup=20
   integer, parameter :: nosup1=nosup+1
   ! longueur des noms de fichier
   integer, parameter :: lname = 60
   ! nombre de caractères autorisés pour les noms des nœuds
   integer, parameter :: lnode = 10

! -- Constantes chaînes de caractères --
   character(len=12), parameter :: nomini_ini = 'Mage_ini.ini'
   character(len=12), parameter :: nomfin_ini = 'Mage_fin.ini'
   character(len=12), parameter :: nomfin_sin = 'Mage_fin.sin'
   character(len=12), parameter :: param_iniper = 'Param_Iniper'
   character(len=16), parameter :: param_torrentiel = 'Param_Torrentiel'

! -- Constantes Numériques --
   ! précision de comparaison des réels (fonction ZEgal)
#ifndef fake_mage7
   real(kind=long), parameter :: toutpetit_default= 1.0e-9_long
#endif /* fake_mage7 */
#ifdef fake_mage7
   real(kind=long), parameter :: toutpetit_default= 1.0e-5_long
#endif /* fake_mage7 */
   real(kind=long)            :: toutpetit
   real(kind=long), parameter :: zero = 0.0_long
   real(kind=long), parameter :: untier = 1._long/3._long
   real(kind=long), parameter :: deuxtiers = 2._long/3._long
   real(kind=long), parameter :: un = 1.0_long
   real(kind=long), parameter :: cent = 100.0_long
   real(kind=long), parameter :: pi = 3.14159265359_long ! mage 7 : 3.141592_long
   !on suppose que l'utilisateur ne peut pas définir des cotes avec une précision
   !inférieure à precision_alti
   real(kind=long), parameter :: precision_alti_default = 0.00001_long
   real(kind=long)            :: precision_alti
! -- Constantes liées à G, Accelération de la pesanteur --
   real(kind=long), parameter :: g = 9.81_long
   real(kind=long), parameter :: g2 = 2.0_long*g
   real(kind=long), parameter :: gs2 = g/2.0_long
   real(kind=long), parameter :: racg2 = sqrt(2._long*g) !4.42945691807_long ! sqrt(g2) mage 7 : 4.42945_long

! -- constantes pour le choix du modèle de débordement (fp_model : floodplain model)
   integer, parameter :: i_debord = 0, i_mixte = -1, i_ism = +1, i_mixte2 = -2, i_static = 2

! -- valeur du signal SIGTSTP (ask suspension from keyboard, ctrl+Z)
   integer, parameter :: SIGTSTP = 20
! -- valeur du signal SIGINT (ask suspension from keyboard, ctrl+C)
   integer, parameter :: SIGINT = 2
   integer, parameter :: Signal_interruption = SIGINT

! -- Constantes nécessaires pour le charriage
   ! FIXME: certaines de ces variables devraient être lues dans un fichier fourni par l'utilisateur
   real(kind=long), parameter :: rho = 1000._long     !masse volumique de l'eau
   real(kind=long), parameter :: nu = 1.e-06_long     !viscosité cinématique
   real(kind=long), parameter :: gnu2 = g/(rho*nu*nu)

end module Parametres


module basic_Types
! déclaration des types dérivés de base dont on a besoin dans plusieurs modules
! permet d'éviter les dépendences circulaires
use parametres, only: long

type debits_ISM
   !triplet de débits mineur, moyen gauche et moyen droit
   real(kind=long) :: Qm, Ql, Qr
end type debits_ISM

type point2D
   real(kind=long) :: x
   real(kind=long) :: y
   contains
   procedure :: init => init_pt2
end type point2D

type, extends(point2D) :: point4D
   real(kind=long) :: u
   real(kind=long) :: v
   contains
   procedure :: init => init_pt4
end type point4D

type courbe2D
   integer :: np ! nombre de points de la courbe
   integer :: ip ! indice courant
   type(point2D), allocatable :: yx(:)
end type courbe2D

type ptr_courbe2D
   integer, pointer :: np => null()
   integer, pointer :: ip => null()
   real(kind=long), pointer :: x(:) => null()
   real(kind=long), pointer :: y(:) => null()
end type ptr_courbe2D

contains

subroutine init_pt2(my_pt)
   class (point2D), intent(inout) :: my_pt

   my_pt%x = 0._long
   my_pt%y = 0._long
end subroutine init_pt2

subroutine init_pt4(my_pt)
   class (point4D), intent(inout) :: my_pt

   my_pt%x = 0._long
   my_pt%y = 0._long
   my_pt%u = 0._long
   my_pt%v = 0._long
end subroutine init_pt4

function init_courbe2D(xx,yy,n)
!===============================================================================
!         construit une courbe 2D à partir de 2 listes d'abscisses
!                 et ordonnées et du nombre de valeurs
! entrées :
!     - xx : tableau de réels pour les abscisses
!     - yy : tableau de réels pour les ordonnées
!     - n  : entier, le nombre de points de la courbe
! résultat : de type courbe2D
! usage : ma_courbe2D = init_courbe2D(x,y,n)
!===============================================================================
   integer, intent(in) :: n
   real(kind=long) :: xx(:), yy(:)
   type(courbe2D) :: init_courbe2D
   integer :: i, err

   allocate(init_courbe2D%yx(n),stat=err)
   if (err > 0) then
      stop 911
   else
      init_courbe2D%np = n
      init_courbe2D%ip = 1
      do i = 1, n
         init_courbe2D%yx(i)%x = xx(i)
         init_courbe2D%yx(i)%y = yy(i)
      enddo
   endif
end function init_courbe2D


function map_courbe2D(xx, yy, n, irg)
!===============================================================================
! défini une courbe2D sous forme de pointeurs sur des listes d'abscisses
! et d'ordonnées
! ce n'est pas un pointeur vers une courbe2D mais un type dérivé composé
! de pointeurs
!
! entrées :
!     - xx  : tableau de réels pour les abscisses
!     - yy  : tableau de réels pour les ordonnées
!     - n   : entier, le nombre de points de la courbe
!     - irg : entier, l'indice courant initial
! résultat : de type ptr_courbe2D
! usage : ma_courbe2D = map_courbe2D(x,y,n,i)
!===============================================================================
   integer, intent(in), target :: n, irg
   real(kind=long), intent(in), target :: xx(:), yy(:)
   type(ptr_courbe2D) :: map_courbe2D
   ! il faudrait sans doute vérifier que xx et yy ont le même nombre d'éléments
   map_courbe2D%np => n
   map_courbe2D%x => xx
   map_courbe2D%y => yy
   map_courbe2D%ip => irg
end function map_courbe2D


subroutine remove_courbe2D(c2D)
!===============================================================================
!                    désallocation d'une courbe2D
!
! entrées :
!     - c2D : type courbe2D, la courbe à désallouer
! résultat : aucun
! usage : call remove_courbe2D(ma_courbe2D)
! renvoie une erreur fatale si la désallocation échoue.
!
! historique :
!     - 14/03/2008 : création du commentaire d'entête
!===============================================================================
   implicit none
   ! -- prototype --
   type(courbe2D) :: c2D
   ! -- variable --
   integer :: err

   deallocate(c2D%yx,stat=err)
   if (err > 0) stop 915
end subroutine remove_courbe2D


! DONE: vérifier que c'est bien la même routine INTERPOLE() que pour Adis-TS
function interpole(fxy,xx) result(yy)
   !===============================================================================
   !              interpolation linéaire sur une courbe2D
   ! entrées :
   !     - fxy : la courbe sur laquelle on interpole
   !     - xx           : la valeur pour laquelle on interpole
   ! résultat : réel
   ! usage : y = interpole(ma_courbe2D,x)
   !===============================================================================
   type(courbe2D) :: fxy
   real(kind=long),intent(in) :: xx
   real(kind=long) :: yy
   integer :: ip0, i
   real(kind=long) :: dx, dy

   if (fxy%np == 1) then  !cas d'un point unique
      yy = fxy%yx(1)%y
      return
   endif
   ip0 = fxy%ip
   if (xx >= fxy%yx(ip0+1)%x) then  !recherche vers le haut
      if (xx >= fxy%yx(fxy%np)%x) then
         yy = fxy%yx(fxy%np)%y  !on prolonge par une constante
         return
      else
         do i = ip0+1, fxy%np-1
            if (xx >= fxy%yx(i)%x .AND. xx < fxy%yx(i+1)%x) then
               fxy%ip = i  ! intervalle trouvé !
               exit
            endif
         enddo
      endif
   elseif (xx < fxy%yx(ip0)%x) then  !recherche vers le bas
      if (xx < fxy%yx(1)%x) then
         yy = fxy%yx(1)%y  !on prolonge par une constante
         fxy%ip = 1
      else
         do i = ip0-1, 1, -1
            if (xx >= fxy%yx(i)%x .AND. xx < fxy%yx(i+1)%x) then
               fxy%ip = i  ! intervalle trouvé !
               exit
            endif
         enddo
      endif
   endif
   ip0 = fxy%ip
   dy = fxy%yx(ip0+1)%y - fxy%yx(ip0)%y
   dx = fxy%yx(ip0+1)%x - fxy%yx(ip0)%x
   yy = fxy%yx(ip0)%y + dy/dx*(xx-fxy%yx(ip0)%x)
end function interpole


function ptr_interpole(une_courbe2D,xx) result(yy)
!==============================================================================
!         interpolation linéaire sur une courbe2D version pointeur.
!
! entrées :
!     - une_courbe2D : type ptr_courbe2D, la courbe sur laquelle on interpole
!     - xx           : la valeur pour laquelle on interpole
! résultat : réel
! usage : y = ptr_interpole(ma_courbe2D,x)
!==============================================================================
   type(ptr_courbe2D) :: une_courbe2D
   real(kind=long),intent(in) :: xx
   real(kind=long) :: yy
   integer :: ip0, i
   real(kind=long) :: dx, dy

   ip0 = une_courbe2D%ip
   if (xx >= une_courbe2D%x(ip0+1)) then  !recherche vers le haut
      if (xx >= une_courbe2D%x(une_courbe2D%np)) then
         yy = une_courbe2D%y(une_courbe2D%np)  !on prolonge par une constante
         return
      else
         do i = ip0+1, une_courbe2D%np-1
            if (xx >= une_courbe2D%x(i) .AND. xx < une_courbe2D%x(i+1)) then
               une_courbe2D%ip = i  ! intervalle trouvé !
               exit
            endif
         enddo
      endif
   elseif (xx < une_courbe2D%x(ip0)) then  !recherche vers le bas
      if (xx < une_courbe2D%x(1)) then
         !stop 913  ! on déborde par en bas
         yy = une_courbe2D%y(1)
         une_courbe2D%ip = 1
      else
         do i = ip0-1, 1, -1
            if (xx >= une_courbe2D%x(i) .AND. xx < une_courbe2D%x(i+1)) then
               une_courbe2D%ip = i  ! intervalle trouvé !
               exit
            endif
         enddo
      endif
   endif
   ip0 = une_courbe2D%ip
   dy = une_courbe2D%y(ip0+1) - une_courbe2D%y(ip0)
   dx = une_courbe2D%x(ip0+1) - une_courbe2D%x(ip0)
   yy = une_courbe2D%y(ip0) + dy/dx*(xx-une_courbe2D%x(ip0))
end function ptr_interpole


end module basic_Types
