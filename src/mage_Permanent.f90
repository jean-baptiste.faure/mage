!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - INRAE - 1989-2020                           #
!# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################

module Regime_Permanent
!==============================================================================
!            Module d'initialisation en régime permanent
!==============================================================================
   use Parametres, only: ibsup, nosup, long, zero, un, g, lErr, lTra, l9, param_iniper, nomini_ini
   use, intrinsic :: iso_fortran_env, only: output_unit, error_unit

   use StVenant_Debord, only: section_values, sec_val_1, sec_val_2, section_data, Debord_IniFin
   use objet_section, only: profil
   use TopoGeometrie, only: la_topo, zfd, xgeo

   implicit none
   real(kind=Long) :: Fr,QZZ
   integer :: isp
   type(section_values),pointer :: s_amont, s_aval

contains

subroutine IniRegPerm
! DONE: on a vérifié que ces variables sont encore utilisées
   isp = 0
   Fr = zero
   QZZ = zero
end subroutine IniRegPerm


function Euler(z)
!==============================================================================
!               Calcul de ligne d'eau en permanent (Saint-Venant)
! Discrétisation : voir la doc théorique
!==============================================================================
   use hydraulique, only: zt, yt, qe
   implicit none
   ! -- prototype --
   real(kind=long) :: euler
   real(kind=long),intent(in) :: z
   ! -- variables --
   logical :: bool
   real(kind=long) :: beta2, tmp, zt1, yt1, dx
   !------------------------------------------------------------------------------
   !write(l9,*) 'Appel de Euler() pour ',la_topo%sections(isp)%name,z
   zt1 = zt(isp)  ;  yt1 = yt(isp)  !sauvegarde : section_data() calcule pour zt() et yt()

   zt(isp) = z  ;  yt(isp) = z-zfd(isp)
   call section_data(-1,isp,bool,s_amont)

   dx = abs(xgeo(isp)-xgeo(isp+1))

   tmp = (s_amont%aj+s_aval%aj)+(s_amont%q*s_amont%v+s_aval%q*s_aval%v)*(s_aval%psi-s_amont%psi)/dx/g
   ! TODO: vérifier le sens de la discrétisation : (s_aval%psi-s_amont%psi) ou (s_amont%psi-s_aval%psi) ?

   beta2 = (s_amont%psi*s_amont%s+s_aval%psi*s_aval%s)  ! = 2 * beta
   if (qe(isp) < zero) then
      tmp = tmp + (beta2-un)*qe(isp)*(s_amont%vs+s_aval%vs)/g
   else
      tmp = tmp + beta2*qe(isp)*(s_amont%vs+s_aval%vs)/g
   endif

   euler = z-(zt(isp+1)+0.5_long*dx*tmp)

   zt(isp) = zt1  ;  yt(isp) = yt1  !rétablissement
end function Euler
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
function Debi(z)
!==============================================================================
!               Calcul de ligne d'eau en permanent (St Venant)
!
!     Debi(z) = Écart, pour la cote z, du débit sur l'ouvrage composite situé
!               entre les profils isi et isj par rapport au débit dans la
!               section aval (isj)
!==============================================================================
   use hydraulique, only: qt,zt
   use Ouvrages, only: debit_total
   implicit none
   ! -- prototype --
   real(kind=long) :: debi
   real(kind=long),intent(in) :: z
   ! -- variables --
   integer :: ns
   real(kind=long) :: zav
   !------------------------------------------------------------------------------
   ns = la_topo%sections(isp+1)%iss
   zav = zt(isp+1)
   debi = debit_total(ns,z,zav)-qt(isp+1)
end function Debi
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
function F_critique(Z)
!==============================================================================
!     Différence Q_critique - QZZ sous forme de fonction utilisable par dichotomie
!==============================================================================
   implicit none
   ! -- prototype --
   real(kind=long) :: F_critique
   real(kind=long),intent(in) :: z

   F_critique = Q_critique(isp,z)-qzz

end function F_critique
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
function F_uniforme(Z)
!==============================================================================
!   Différence Q_Uniforme - QZZ sous forme de fonction utilisable par dichotomie
!==============================================================================
   use Conditions_Limites, only: q_uniforme
   implicit none
   ! -- prototype --
   real(kind=long) :: F_uniforme
   real(kind=long),intent(in) :: z

   F_uniforme = q_uniforme(isp,z)-qzz
end function F_uniforme
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
function Q_critique(is,z)
!==============================================================================
!   Calcul du débit en fonction de la cote sous la contrainte Froude=1
!==============================================================================
   implicit none
   ! -- prototype --
   real(kind=long) :: Q_critique
   integer,intent(in) :: is
   real(kind=long),intent(in) :: z
   ! -- variables --
   real(kind=long) :: al, s, y, ymax
   !---------------------------------------------------------------------------
   Q_critique = huge(zero)
   y = z-zfd(is) !tirant d'eau
   if (y > zero) then
      ymax = la_topo%sections(is)%ybmax+100._long  ! ybmax(is)= tirant d'eau maximal de la section is
      if (y > ymax) then
         !dépassement du Y maximal ---> interpolation impossible
         call war001(is,y,ymax,4)
      endif

      al = la_topo%sections(is)%largeur(z)  !largeur au miroir
      s  = la_topo%sections(is)%section_mouillee(z)  !section mouillée

      Q_critique = s*sqrt(g*s/al)  !débit critique
   else
      call err025(is,y,7)
   endif
   if (Q_critique > huge(zero)/2) stop '>>>> BUG dans Q_critique()'
end function Q_critique
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
function dichotomie(f,x1,x2,eps) result(x12)
!==============================================================================
!   Résout F(x) = 0 par dichotomie entre X1 et X2 ; tolérance : eps
!
!   si f(x1)*f(x2) > 0 il y a 2 solutions au moins (ou 0) et on réduit
!   l'intervalle pour trouver un nouvel x2 tel que f(x1)*f(x2) < 0
!
!   si x1 > x2 ou permute x1 et x2
!==============================================================================
   use mage_utilitaires, only: is_zero
   implicit none
   ! -- Prototype --
   real(kind=long) :: x12
   real(kind=long), intent(inout) :: x1, x2
   real(kind=long), intent(in) :: eps
   interface
      function f(x)
         use parametres, only: long
         real(kind=long) :: f
         real(kind=long),intent(in) :: x
      end function f
   end interface
   ! -- Constantes --
   integer, parameter :: jmax = 40
   ! -- Variables --
   integer :: j
   real(kind=long) :: alfa, dx, f1, f2, f12, x0, f1_0, f2_0
   character(len=120) :: err_message
   !------------------------------------------------------------------------------
   if (x2 < x1) then
      x0 = x1 ; x1 = x2 ; x2 = x0
   endif
   f1 = f(x1)  ;  f1_0 = f1
   f2 = f(x2)  ;  f2_0 = f2
   alfa = (x2-x1)* 0.01_long
   x0 = x2

   do while (f1*f2 >= zero)
      ! recherche d'un intervalle [x1 ; x2] tel que f(x1)*f(x2) < 0
      ! l'intervalle fourni en argument ne convenant pas
      x0 = x0 - alfa
      if (x0 < x1) then
         err_message = 'Dichotomie : impossible de trouver un intervalle encadrant la solution'
         write(error_unit,'(a)') trim(err_message)  ;  write(lTra,'(a)') trim(err_message)
         write(err_message,'(4(a,g14.6))') 'x1 = ',x1,'F(x1) = ',f1_0,'x2 = ',x2,'F(x2) = ',f2_0
         write(error_unit,'(a)') trim(err_message)  ;  write(lTra,'(a)') trim(err_message)
         !call do_Crash('dichotomie')
         stop 120
      else
         f2 = f(x0)
      endif
   enddo
   x2 = x0

   ! on a un intervalle [x1 ; x2] tel que f(x1)*f(x2) < 0 --> on peut résoudre
   ! on part coté négatif
   if (f1 < zero) then
      x0 = x1  ;  dx = x2-x1
   else
      x0 = x2  ;  dx = x1-x2
   endif
   ! boucle
   do j = 1, jmax
      dx = dx*0.5_long
      x12 = x0+dx !milieu de l'intervalle de recherche
      f12 = f(x12)
      if (abs(dx) < eps .or. is_zero(f12)) return
      if (f12 <= zero) x0 = x12 ! si f12 > 0 on a dépassé la solution
   enddo
   err_message = 'Dichotomie : non convergence des itérations'
   write(error_unit,'(a)') trim(err_message)  ;  write(lTra,'(a)') trim(err_message)
   stop 121
end function dichotomie
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine IniPer
!==============================================================================
!
!                 Procédure d'initialisation
!
!==============================================================================
   use conditions_limites, only: allCL, cl
   use data_num_fixes, only: tinf,yinf,silent
   use data_num_mobiles, only: t,dt,dx,ib
   use hydraulique, only: qt,zt,yt,qe,iub,ibu
   use Ouvrages, only: all_OuvCmp, all_OuvEle
   implicit none
   ! -- constantes --
   real(kind=long),parameter :: zacc = 1.e-5_long
   ! -- variables --
   logical :: bool,zfound
   integer :: is,itemax,iter,iuk, jb,js, k,kb, ncl,n,nk,ns,nerror,ink
   real(kind=long) :: alfa,beta,e1,e2,eps,hc,hd,pente,sjb,sjs,som,somq,soms,z1,z2,zcritic,zber,zerror
   real(kind=long) :: qamo(ibsup),qava(ibsup),werror(ibsup),zbare(nosup)
   real(kind=long) :: repq(ibsup) !indexation selon le rang de calcul
   character(len=120) :: message_titre,err_message

   integer :: nam_ib,nav_ib,is2_ib,lbz1,lbz2,ltmp,ibb,jbb
   real(kind=long) :: tz0, qz0, tz1, qz1
   integer,pointer :: nsmax
   type(profil), pointer :: prfl

   nsmax => la_topo%net%nss
   call IniRegPerm

   message_titre = ' État Initial : estimation du régime permanent'
   if(.not.silent) write(output_unit,'(a)') trim(message_titre)
   write(l9,'(a)') trim(message_titre)
   t = tinf
   iter = 0
   inquire(file=param_iniper,exist=bool)
   if (bool) then
      open(newunit=ltmp,file=param_iniper,status='unknown')
      read(ltmp,'(f10.0)') alfa
      read(ltmp,'(f10.0)') eps
      read(ltmp,'(i3)') itemax
   else  !création du fichier s'il n'existe pas
      open(newunit=ltmp,file=param_iniper,status='new')
      alfa = 0.25_long
      eps = 0.01_long
      itemax = 99
      write(ltmp,'(f4.2)') alfa
      write(ltmp,'(f4.2)') eps
      write(ltmp,'(i3)') itemax
   endif
   close(ltmp)
!
!---recherche des débits dans chaque bief
   write(l9,*) ' Recherche des débits dans chaque bief'
   do ib = 1, la_topo%net%nb  !parcours dans l'ordre de calcul amont -> aval
      ibb = la_topo%net%numero(ib)
      nam_ib = la_topo%biefs(ibb)%nam
      ncl = la_topo%nodes(nam_ib)%cl
      repq(ib) = un  !part du débit provenant de l'amont entrant dans le bief ib
      if (ncl > 0) then
         qamo(ibb) = cl(nam_ib,t)
      elseif (ncl == 0) then
         somq = cl(nam_ib,t,zt(la_topo%biefs(ibb)%is1))
         !---sommation des débits amont du bief ib
         do jb = 1,ib-1
            jbb = la_topo%net%numero(jb)
            if (la_topo%biefs(jbb)%nav == nam_ib) then
               somq = somq+qava(jbb)
            endif
         enddo
         if (la_topo%net%lbz(nam_ib,2) == la_topo%net%lbz(nam_ib,1)) then
            !ib est le bief aval d'un confluent
            repq(ib) = un
            qamo(ibb) = somq
         else
            !ib appartient à un défluent : recherche des biefs aval
            soms = zero
            kb = 0
            is2_ib = la_topo%biefs(ibb)%is2
            zber = zfd(is2_ib)+la_topo%sections(is2_ib)%ybmin
            do jbb = la_topo%net%lbz(nam_ib,1),la_topo%net%lbz(nam_ib,2)
               jb = la_topo%net%numero(jbb)
               js = la_topo%biefs(jb)%is1
               zber = max(zber,zfd(js)+la_topo%sections(js)%ybmin)
            enddo
            sjb = -1._long
            do jb = la_topo%net%lbz(nam_ib,1),la_topo%net%lbz(nam_ib,2)
               jbb = la_topo%net%numero(jb)
               js = la_topo%biefs(jbb)%is1
               prfl => la_topo%sections(js)
               ns = la_topo%sections(js+1)%iss
               if (ns /= 0) then
                  nk = all_OuvCmp(ns)%OuEl(1,1)
                  iuk = all_OuvEle(nk)%iuv
                  if (iuk/=99 .and. iuk/=3 .and. iuk/=5) then
                     !calcul de la section d'écoulement si c'est un ouvrage composite
                     sjs = zero
                     do k = 1, all_OuvCmp(ns)%ne
                        nk = all_OuvCmp(ns)%OuEl(k,1)
                        hc = min(all_OuvCmp(ns)%zfs + all_OuvEle(nk)%uv3 , zber)
                        hd = all_OuvCmp(ns)%zfs + all_OuvEle(nk)%uv2
                        sjs = sjs + all_OuvEle(nk)%uv1 * max(zero,hc-hd)
                     enddo
                  else
                     !section d'écoulement pour les ouvrages sans perte de charge (pompe, etc.)
                     sjs = prfl%surface_mouillee_tirant(zber-zfd(js))
                  endif
               else
                  !section d'écoulement - cas général (géométrie)
                  sjs = prfl%surface_mouillee_tirant(zber-zfd(js))
               endif
               soms = soms + sjs
               if (jb == ib) then
                  kb = kb + 1
                  sjb = sjs !on met de coté la section d'écoulement du bief ib
               endif
            enddo
            if (sjb < zero) stop '>>>> BUG 1 dans IniPer()'
            repq(ib) = sjb/soms
            qamo(ibb) = repq(ib)*somq
            if (kb /= 1) then
               err_message = ' INIPER : erreur dans la routine IniPer. Veuillez envoyer un rapport de bug'
               write(error_unit,'(a)') trim(err_message)  ;  write(lTra,'(a)') trim(err_message)
               stop 204
            endif
         endif
      endif

      !---ajout des apports latéraux
      write(l9,*) ' Ajout des apports latéraux'
      qava(ibb) = qamo(ibb)
      do is = la_topo%biefs(ibb)%is1,la_topo%biefs(ibb)%is2-1
         qava(ibb) = qava(ibb)+qe(is)*abs(xgeo(is+1)-xgeo(is))
      enddo

      write(l9,'(a,i3,a,f8.3)') ' Débit amont du bief ',ibb,' = ',qamo(ibb)
      write(l9,'(a,i3,a,f8.3)') ' Débit aval du bief ',ibb,'  = ',qava(ibb)
   enddo

   if (la_topo%net%iba == la_topo%net%nb) then
      write(message_titre,*) '     Résolution aval -> amont sur le réseau ramifié'
   else
      write(message_titre,*) '     Début de la boucle d''itération réseau maillé'
   endif
   !if(.not.silent) write(output_unit,'(a,i3)') trim(message_titre)
   write(l9,'(a,i3)') trim(message_titre)

   do                    !Début de la boucle d'itération "réseau maillé"
                         !un seul tour si le réseau est ramifié
      write(l9,*) '   cote et débit à l''aval de chaque bief'
      iter = iter+1
      beta = un-alfa
      do ib = 1, la_topo%net%nb   ! cotes aux noeuds aval
         ibb = la_topo%net%numero(ib)
         is2_ib = la_topo%biefs(ibb)%is2
         n = la_topo%biefs(ibb)%nav
         if (la_topo%nodes(n)%cl == 0) then  ! nœud interne
            continue
         elseif (la_topo%nodes(n)%cl == -1) then     !inversion de la loi Q(z) ; on la suppose strictement croissante
            do k = 1, allCL(n)%np-1
               tz0 = allCL(n)%bc(k)%x
               tz1 = allCL(n)%bc(k+1)%x
               qz0 = allCL(n)%bc(k)%y
               qz1 = allCL(n)%bc(k+1)%y
               if (qz0 <= qava(ibb) .and. qava(ibb) < qz1) then
                  pente = (tz1-tz0)/(qz1-qz0)
                  zt(is2_ib) = tz0+(qava(ibb)-qz0)*pente
                  exit
               endif
            enddo
         elseif (la_topo%nodes(n)%cl == -2) then !Régime uniforme
            qzz = qava(ibb)  !info "cachée" pour F_uniforme()
            isp = is2_ib     !info "cachée" pour F_uniforme()
            z1 = zfd(is2_ib)+0.01_long
            z2 = zfd(is2_ib)+la_topo%sections(is2_ib)%ybmin+9.0_long
            zt(is2_ib) = dichotomie(F_uniforme,z1,z2,zacc)
         elseif (la_topo%nodes(n)%cl == -3) then !loi Z(t)
            zt(is2_ib) = cl(n,t)
         else
            write(output_unit,*) '>>>> ERREUR : le nœud aval du bief ',ibb,' a une CL amont'
            stop 3
         endif
         write(l9,'(a,i3,3(a,f10.3))') ' Calcul permanent bief : ',ibb,&
                                      ' Qamont = ',qamo(ibb),         &
                                      ' Qaval = ',qava(ibb),          &
                                      ' Zaval = ',zt(is2_ib)
      enddo
      ! Maintenant on a cote et débit à l'aval de chaque bief
      ! on peut donc faire un calcul de courbe de remous
      !---initialisation des cotes : calcul permanent
      do ibb = la_topo%net%nb,1,-1  !parcours dans l'ordre de calcul inverse (aval -> amont)
         ib = la_topo%net%numero(ibb)
         write(message_titre,'(6x,a,i3,a,i3,a)') '-->initialisation de la courbe de remous dans le bief ', &
                                              ib,' (rang : ',ibb,')'
         !if(.not.silent) write(output_unit,'(a,i3)') trim(message_titre)
         write(l9,'(a,i3)') trim(message_titre)
         is = la_topo%biefs(ib)%is2
         qt(is) = qava(ib)
         nav_ib = la_topo%biefs(ib)%nav
         lbz1 = la_topo%net%lbz(nav_ib,1)
         lbz2 = la_topo%net%lbz(nav_ib,2)
         if (lbz1 /= lbz2) then ! le bief ib arrive sur une diffluence : cote moyenne
            zt(is) = zero
            do jbb = lbz1,lbz2 !boucle sur les rangs de calcul
               jb = la_topo%net%numero(jbb)
               zt(is) = zt(is)+zt(la_topo%biefs(jb)%is1)
            enddo
            zt(is) = zt(is)/real(lbz2-lbz1+1,kind=long)
         elseif (la_topo%nodes(nav_ib)%cl >= 0) then            ! un seul bief à l'aval : cote amont du bief aval
            jb = la_topo%net%numero(lbz1)
            zt(is) = zt(la_topo%biefs(jb)%is1)
         endif
         yt(is) = zt(is)-zfd(is)  !tirant d'eau
         if (yt(is) < zero) then  !initialisation alternative par la cote critique
            !write(output_unit,*) ' Initialisation par cote critique : ',ib,la_topo%sections(is)%pk,zt(is),zfd(is),lbz1,lbz2
            isp = is
            qzz = qt(is)
            z1 = zfd(is)+0.005_long
            z2 = zfd(is)+la_topo%sections(is)%ybmin
            zt(is) = dichotomie(F_critique,z1,z2,zacc)
            zt(is) = max(zfd(is)+yinf,zt(is))
            yt(is) = zt(is)-zfd(is)
         endif
         write(l9,'(a,i3,f10.2,3f10.3)') 'Ligne d''eau : ',ib,xgeo(is),qt(is),zt(is),yt(is)

         write(message_titre,'(6x,a,i3)') '-->calcul de la courbe de remous dans le bief ',ib
         !if(.not.silent) write(output_unit,'(a,i3)') trim(message_titre)
         write(l9,'(a,i3)') trim(message_titre)
         !initialisation des pointeurs
         s_amont => sec_val_1  ;  s_aval => sec_val_2
         do is = la_topo%biefs(ib)%is2-1, la_topo%biefs(ib)%is1,-1   ! calcul de la courbe de remous
            qt(is) = qt(is+1)-qe(is)*abs(xgeo(is+1)-xgeo(is))
            ns = la_topo%sections(is+1)%iss
            if (ns /= 0) then  !section singulière
               ink = all_OuvEle(all_OuvCmp(ns)%OuEl(1,1))%iuv
               if (ink/=99 .and. ink/=3 .and. ink/=5) then !ouvrage avec perte de charge
                  isp = is
                  z1 = zfd(is)+0.005_long
                  z2 = zfd(is)+la_topo%sections(is)%ybmin+9._long
                  zt(is) = dichotomie(debi,z1,z2,zacc)
               else
                  zt(is) = zt(is+1)  !ouvrage sans perte de charge
               endif
            else                    !section St-Venant
               dx = abs(xgeo(is+1)-xgeo(is))
               !-->section aval (is+1)
               isp = is+1
               !write(l9,*) 'Appel de section_data() pour ',la_topo%sections(isp)%name
               call section_data(0,isp,bool,s_aval)
               !-->section amont (is)
               !recherche de la cote critique comme extrémité de l'intervalle
               !dans lequel on va chercher la solution Z telle que Euler(Z)=0
               !par la méthode de dichotomie avec DICHOTOMIE()
               z1 = zfd(is)+0.01_long
               z2 = zfd(is)+la_topo%sections(is)%ybmin+9._long
               isp = is      !paramètre caché de F_critique() (équation FR=1)
               qzz = qt(is)  !paramètre caché de F_critique() (équation FR=1)
               fr = -1000._long
               Zcritic = huge(zero)
               if (abs(qzz) < 0.05_long) then
                  z1 = Zfd(is)+0.001_long
               else
                  !write(l9,*) '   Recherche de la cote critique ',z1,z2,qzz
                  Zcritic = dichotomie(F_critique,z1,z2,zacc)
                  z1 = Zcritic
                  !write(l9,*) '   Cote critique trouvée : ',zcritic
               endif
               !recherche cote mini z2 telle que euler(z2)*euler(z1) < 0
               !on part de Z1 = Zcritic et on cherche vers le haut car a priori
               !l'écoulement est fluvial (cas torrentiel : voir plus loin)
               e1 = euler(z1)
               zt(is) = z1
               z2 = z1+0.001_long
               Zfound = .false.
               do while (z2 <= zfd(is)+la_topo%sections(is)%ybmin+9._long)
                  e2 = euler(z2)
                  !write(l9,*) '  recherche vers le haut : ',z1,z2,e1,e2
                  if (e1*e2 < zero) then
                     zt(is) = dichotomie(euler,z1,z2,zacc) !résolution de EULER(Z)=0 entre Z1 et Z2
                     Zfound = .true.
                     exit
                  endif
                  z2 = z2+0.01_long
               enddo
               if (.NOT.Zfound) then !torrentiel : il faut chercher sous Zcritic
                  ! dans la boucle précédente sortie par la fin et non par exit, sauf si bug
                  if (Zcritic > huge(zero)/2) stop '>>>> BUG 2 dans IniPer()'
                  z1 = Zcritic
                  e1 = euler(z1)
                  zt(is) = z1
                  z2 = z1-0.0001_Long  ! on cherche maintenant vers le fond
                  Zfound = .false.
                  do while (z2 >= zfd(is))
                     e2 = euler(z2)
                     !write(l9,*) '  recherche vers le bas : ',z1,z2,e1,e2
                     if (e1*e2<zero) then
                        zt(is) = dichotomie(euler,z1,z2,zacc) !résolution de EULER(Z)=0 entre Z1 et Z2
                        zfound = .true.
                        exit
                     endif
                     z2 = z2-0.005_Long
                  enddo
                  if (.NOT.Zfound) then
                     write(lTra,*) ' >>>> Échec du calcul de la ligne d''eau initiale',    &
                                     ib,xgeo(is),Zcritic,e1,                          &
                                     z2,e2,euler(zfd(is)+la_topo%sections(is)%ymoy),  &
                                     euler(zfd(is)+la_topo%sections(is)%ybmin),qt(is)
                     if(.not.silent) write(error_unit,*) ' >>>> Échec du calcul de la ligne d''eau initiale'
                     stop 207
                  endif
               endif
            endif
            zt(is) = max(zfd(is)+yinf,zt(is))
            yt(is) = zt(is)-zfd(is)
            write(l9,'(a,i3,f10.2,3f10.3)') 'Ligne d''eau : ',ib,xgeo(is),qt(is),zt(is),yt(is)
         enddo                          ! fin du calcul de la courbe de remous
      enddo                             ! fin de la boucle sur tous les biefs

      if (la_topo%net%iba == la_topo%net%nb) then  !réseau ramifié --> on a fini
         message_titre = ' Fin du calcul d''initialisation en permanent'
         if(.not.silent) write(output_unit,'(a)') trim(message_titre)
         write(l9,'(a)') trim(message_titre)
         exit
      else
         werror(1:la_topo%net%nb) = 0.0_long
         ! relecture de Param_Iniper pour pilotage manuel des itérations
         open(newunit=ltmp,file=param_iniper,status='unknown')
         read(ltmp,'(f10.0)') alfa
         read(ltmp,'(f10.0)') eps
         read(ltmp,'(i3)') itemax
         close(ltmp)

         ! on calcule maintenant les écarts entre cotes aux nœuds qui sont nœud amont d'un bief
         zerror = zero
         nerror = 1
         do n = 1, la_topo%net%nn
            lbz1 = la_topo%net%lbz(n,1)
            lbz2 = la_topo%net%lbz(n,2)
            if (lbz2 > lbz1) then !ce nœud est une diffluence
               !moyenne des cotes amont des biefs qui partent du nœud
               zbare(n) = zero
               do jb = lbz1,lbz2
                  jbb = la_topo%net%numero(jb)
                  zbare(n) = zbare(n)+zt(la_topo%biefs(jbb)%is1)
               enddo
               zbare(n) = zbare(n)/real(lbz2-lbz1+1,kind=long)
               !calcul des écarts à la cote moyenne
               do jb = lbz1,lbz2  !rang de calcul
                  jbb = la_topo%net%numero(jb)
                  werror(jbb) = abs(zbare(n)-zt(la_topo%biefs(jbb)%is1))
                  if (zerror < werror(jbb)) then
                     zerror = werror(jbb)
                     nerror = n !c'est à ce nœud que se produit l'erreur maximale
                  endif
               enddo
            endif
         enddo
         if(.not.silent) then
            write(output_unit,'(3x,a,i3.2,a,g12.6,a,a3,a)') ' Recherche état initial - Itération ',iter,  &
                                       ' : Écart Max aux Nœuds (mm) = ',zerror*1000._long,  &
                                       ' (nœud ',la_topo%nodes(nerror)%name,')'
         endif
         write(l9,'(a,i3.2,2(a,g12.6))') ' Itération ',iter,' : Écart Maximal aux Nœuds (mm) = ',  &
                       zerror*1000._long,'--- beta = ',beta
         if (zerror<eps .or. iter>itemax) then
            message_titre = ' Fin du calcul d''initialisation en permanent'
            if(.not.silent) write(output_unit,'(a)') trim(message_titre)
            write(l9,'(a)') trim(message_titre)
            exit
         endif

         !--- Correction de la répartition des débits (réseau maillé)
         beta = max(0.5_long,un-zerror*alfa)
         do ib = 1, la_topo%net%nb  !rang de calcul
            ibb = la_topo%net%numero(ib)
            if (zt(la_topo%biefs(ibb)%is1) > (zbare(la_topo%biefs(ibb)%nam)+0.0001_long)) then
               repq(ib) = repq(ib)*beta
            elseif (zt(la_topo%biefs(ibb)%is1) < (zbare(la_topo%biefs(ibb)%nam)-0.0001_long)) then
               repq(ib) = repq(ib)/beta
            endif
         enddo
         do n = 1,la_topo%net%nn   ! normalisation pour conservation de la masse
            lbz1 = la_topo%net%lbz(n,1)
            lbz2 = la_topo%net%lbz(n,2)
            if (lbz1 /= lbz2) then
               som = sum( repq(lbz1:lbz2) )
               repq(lbz1:lbz2) = repq(lbz1:lbz2) / som
            endif
         enddo

         do ib = 1,la_topo%net%nb  ! remise en place des CL
            ibb = la_topo%net%numero(ib)
            nam_ib = la_topo%biefs(ibb)%nam
            ncl = la_topo%nodes(nam_ib)%cl
            if (ncl > 0) then            ! CL amont
               qamo(ibb) = cl(nam_ib,t)
            elseif (ncl == 0) then       ! noeud interne
               somq = cl(nam_ib,t,zt(la_topo%biefs(ibb)%is1))
               do jb = 1, ib-1   !---sommation des débits amont du bief ib
                  jbb = la_topo%net%numero(jb)
                  if (la_topo%biefs(jbb)%nav == nam_ib) somq = somq+qava(jbb)
               enddo
               if (la_topo%net%lbz(nam_ib,2) == la_topo%net%lbz(nam_ib,1)) then  !aval d'un confluent
                  qamo(ibb) = somq
               else                                    !diffluence : recherche des biefs aval
                  qamo(ibb) = repq(ib)*somq
               endif
            endif

            qava(ibb) = qamo(ibb)             ! ajout des apports latéraux
            do is = la_topo%biefs(ibb)%is1,la_topo%biefs(ibb)%is2-1
               qava(ibb) = qava(ibb)+qe(is)*abs(xgeo(is+1)-xgeo(is))
            enddo
         enddo

         rewind(lErr)
         write(lErr,'(a)') ' Ré-initialisation du fichier ERR'
      endif
   enddo

   !sauvegarde de l'état initial
   T = Tinf+DT
   write(l9,*) 'sauvegarde de l''état initial dans ',trim(nomini_ini)
   call Debord_IniFin(nomini_ini,QT,ZT)
   T = Tinf
end subroutine iniper


end module Regime_Permanent
