!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - INRAE - 1989-2020                           #
!# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################
!##############################################################################
!
!                 REGULATION LOCALE STANDARD
!
!##############################################################################
module mage_regulation
implicit none
contains
subroutine RegLoc(t,q,z,y,v,dtu)
!==============================================================================
!        Bibliothèque de Régulation Locale standard
!==============================================================================
   use parametres, only: long, lTra, l9
   use, intrinsic :: iso_fortran_env, only: error_unit

   use mage_utilitaires, only: next_int, next_real, next_string, lire_date, capitalize, &
   is_zero, c_tm, get_t1t2_annual, get_t1t2_daily, get_t1t2_monthly, get_t1t2_weekly, get_tm
   use data_num_fixes, only: dtbase, tinf, date_format
   use Ouvrages, only: all_OuvEle, regle, NBreg, change_position
   use TopoGeometrie, only: la_topo, iSect
   implicit none

   ! -- prototype --
   real(kind=long),intent(in) :: t
   real(kind=long),dimension(:),intent(in) :: q,z,y,v
   real(kind=long),intent(out) :: dtu

   ! -- variables --
   character(len=9) :: c_pos
   real(kind=long) :: dtu1,t1,q1,z1,t2,q2,z2,qq1,qq2,qqref
   real(kind=long) :: zref,gain,power,pmrf,qmax,qrsv
   real(kind=long) :: dt0, dh, dt_on, dt_off, dtmove
   real(kind=long) :: qptr_min, zptr_min
   character :: nom_ouvrage*10, type_regle*10, ctmp*10
   character :: nom_regle*25
   integer :: i, ib, is, ns, ne, nbrf, np(10), nnp, u, isrf

   character(len=*), parameter :: separateur=',;'
   character(len=*), parameter :: no_comment='no_comment'
   character(len=80) :: comment
   integer :: next_field, ierr, iuv_ne, wday, offset
   character(len=20) :: sT1, sT2
   type(c_tm) :: tm1, tm2
   !------------------------------------------------------------------------------
   if (NBreg == 0) then
      !aucune règle de régulation en vigueur
      dtu = dtbase
      return
   endif
!------------------------------------------------------------------------------
!--->Décodage des règles de régulation de VAR
!------------------------------------------------------------------------------
   write(l9,*) ' '
   write(l9,'(a)') ' État de la régulation des ouvrages :'

   dtu = dtbase
   do i = 1, NBreg
      next_field = 2
      nom_ouvrage = next_string(regle(i),separateur,next_field)
      call localise_ouvrage(nom_ouvrage,ib,is,ns,ne) !ib est le rang de calcul
      iuv_ne = all_OuvEle(ne)%iuv  !type de l'ouvrage élémentaire
      if (ib < 0) then
         write(lTra,*)' >>>> La règle suivante désigne un ouvrage inconnu',regle(i)
         write(error_unit,*)' >>>> La règle suivante désigne un ouvrage inconnu',regle(i)
         stop 208
      endif
      comment = ''
      !------interprétation des règles
      ctmp = next_string(regle(i),separateur,next_field)
      call capitalize(ctmp,type_regle)
      nom_regle = next_string(regle(i),separateur,next_field)
      ierr = 1
      if (type_regle(1:6)=='CLAPET') then
         dt0 = next_real(regle(i),separateur,next_field)
         if (is_zero(dt0)) dt0 = 60._long
         dh = 0.01_long ; np = 0 ; nnp = 0
         call pflot(ne,z(is),z(is+1),t,dt0,dtu1,dh,np,nnp,c_pos) !is = amont
         if (dtu1 < dtbase) then
            write(comment,'(a,f5.1,4f10.5,2l3)') c_pos,dtu1,all_OuvEle(ne)%uv3,all_OuvEle(ne)%uv5,z(is),z(is+1)
         else
            write(comment,'(a)') c_pos
         endif
         dtu = min(dtu,dtu1)

      elseif (type_regle(1:6)=='ZAMONT') then
         nbrf = next_int(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Pmrf = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Zref = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Gain = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Power = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         dtmove = next_real(regle(i),separateur,next_field)
         if (ierr == 0) call erreur_lecture('VAR',regle(i))
         if (is_zero(dtmove)) dtmove = 1._long
         if (iuv_ne /= 2 .and. iuv_ne /= 4 .and. iuv_ne /= 6) call err040(u,all_OuvEle(ne)%cvar,regle(i))
         call Vanne_Zcst(ne,nbrf,Pmrf,Zref,Gain,Power,Z,.false.,dtmove,comment)

      elseif (type_regle(1:5)=='ZAVAL') then
         nbrf = next_int(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Pmrf = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Zref = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Gain = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Power = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         dtmove = next_real(regle(i),separateur,next_field)
         if (ierr == 0) call erreur_lecture('VAR',regle(i))
         if (is_zero(dtmove)) dtmove = 1._long
         if (iuv_ne /= 2 .and. iuv_ne /= 4 .and. iuv_ne /= 6) call err040(u,all_OuvEle(ne)%cvar,regle(i))
         call Vanne_Zcst(ne,nbrf,Pmrf,Zref,Gain,Power,Z,.true.,dtmove,comment)

      elseif (type_regle(1:4)=='QMAX') then
         nbrf = next_int(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Pmrf = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Qmax = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Qrsv = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Gain = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Power = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         dtmove = next_real(regle(i),separateur,next_field)
         sT1 = next_string(regle(i),separateur,next_field)
         if (sT1 == ' ') then
            T1 = -1.e30_long  ;  T2 = +1.e30_long
         else
            T1 = real(lire_date(sT1,tinf,date_format),kind=long)
            sT2 = next_string(regle(i),separateur,next_field)
            if (sT2 == ' ') then
               T2 = +1.e30_long
            else
               T2 = real(lire_date(sT2,tinf,date_format),kind=long)
            endif
         endif
         if (ierr == 0) call erreur_lecture('VAR',regle(i))
         if (T < T1 .or. T > T2) then
            comment = no_comment
         else
            if (is_zero(dtmove)) dtmove = 1._long
            if (iuv_ne /= 2 .and. iuv_ne /= 4 .and. iuv_ne /= 6) call err040(u,all_OuvEle(ne)%cvar,regle(i))
            call Vanne_Qmax(is,ne,nbrf,pmrf,qmax,qrsv,gain,power,dtmove,q,comment)
         endif

      elseif (type_regle(1:3)=='Q2Q') then
         nbrf = next_int(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Pmrf = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Q1 = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         QQ1 = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Q2 = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         QQ2 = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Gain = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Power = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         dtmove = next_real(regle(i),separateur,next_field)
         sT1 = next_string(regle(i),separateur,next_field)
         if (sT1 == ' ') then
            T1 = -1.e30_long  ;  T2 = +1.e30_long
         else
            T1 = real(lire_date(sT1,tinf,date_format),kind=long)
            sT2 = next_string(regle(i),separateur,next_field)
            if (sT2 == ' ') then
               T2 = +1.e30_long
            else
               T2 = real(lire_date(sT2,tinf,date_format),kind=long)
            endif
         endif
         if (ierr == 0) call erreur_lecture('VAR',regle(i))
         if (is_zero(dtmove)) dtmove = 1._long
         if (iuv_ne /= 2 .and. iuv_ne /= 4 .and. iuv_ne /= 6) call err040(u,all_OuvEle(ne)%cvar,regle(i))
         isrf = iSect(nbrf,pmrf)  !section où on observe le débit, en principe située à l'amont de l'ouvrage
         if (T < T1 .or. T > T2) then
            comment = no_comment
         elseif (Q(isrf) < Q1 .or. Q(isrf) > Q2) then
            comment = no_comment
         else
            QQref = QQ1 + (QQ2-QQ1)/(Q2-Q1)*(Q(isrf)-Q1)
            call Vanne_Qaval(is,ne,QQref,Gain,Power,Q,dtmove,comment)
         endif

      elseif (type_regle(1:3)=='W2Q') then
         nbrf = next_int(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Pmrf = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Q1 = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Z1 = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Q2 = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Z2 = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         sT1 = next_string(regle(i),separateur,next_field)
         if (sT1 == ' ') then
            T1 = -1.e30_long  ;  T2 = +1.e30_long
         else
            T1 = real(lire_date(sT1,tinf,date_format),kind=long)
            sT2 = next_string(regle(i),separateur,next_field)
            if (sT2 == ' ') then
               T2 = +1.e30_long
            else
               T2 = real(lire_date(sT2,tinf,date_format),kind=long)
            endif
         endif
         if (ierr == 0) call erreur_lecture('VAR',regle(i))
         isrf = la_topo%biefs(la_topo%net%rang(nbrf))%is1 !On observe le débit à l'entrée du bief où se trouve le point régulé.
         if (T < T1 .or. T > T2) then
            comment = no_comment
         elseif (Q(isrf) < Q1 .or. Q(isrf) > Q2) then
            comment = no_comment
         else
            call change_position(ne, Z1, Z2, T1, T2)
         endif

      elseif (type_regle(1:3)=='Z2Q') then
         nbrf = next_int(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Pmrf = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Q1 = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Z1 = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Q2 = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Z2 = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Gain = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Power = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         dtmove = next_real(regle(i),separateur,next_field)
         sT1 = next_string(regle(i),separateur,next_field)
         if (sT1 == ' ') then
            T1 = -1.e30_long  ;  T2 = +1.e30_long
         else
            T1 = real(lire_date(sT1,tinf,date_format),kind=long)
            sT2 = next_string(regle(i),separateur,next_field)
            if (sT2 == ' ') then
               T2 = +1.e30_long
            else
               T2 = real(lire_date(sT2,tinf,date_format),kind=long)
            endif
         endif
         if (ierr == 0) call erreur_lecture('VAR',regle(i))
         if (is_zero(dtmove)) dtmove = 1._long
         if (iuv_ne /= 2 .and. iuv_ne /= 4 .and. iuv_ne /= 6) call err040(u,all_OuvEle(ne)%cvar,regle(i))
         isrf = la_topo%biefs(la_topo%net%rang(nbrf))%is1 !On observe le débit à l'entrée du bief où se trouve le point régulé.
         if (T < T1 .or. T > T2) then
            comment = no_comment
         elseif (Q(isrf) < Q1 .or. Q(isrf) > Q2) then
            comment = no_comment
         else
            Zref = Z1 + (Z2-Z1)/(Q2-Q1)*(Q(isrf)-Q1)
            call Vanne_Zcst(ne,nbrf,Pmrf,Zref,Gain,Power,Z,.false.,dtmove,comment)
         endif

      elseif (type_regle(1:3)=='W2T') then
         nbrf = next_int(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Pmrf = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         sT1 = next_string(regle(i),separateur,next_field)
         T1 = real(lire_date(sT1,tinf,date_format),kind=long)
         Z1 = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         sT2 = next_string(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         T2 = real(lire_date(sT2,tinf,date_format),kind=long)
         if (ierr == 0) call erreur_lecture('VAR',regle(i))
         if (t < t1 .OR. t > t2) then
            comment = no_comment
         else
            call change_position(ne, Z1, Z2, T1, T2)
         endif

      elseif (type_regle(1:3)=='Z2T') then
         nbrf = next_int(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Pmrf = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         sT1 = next_string(regle(i),separateur,next_field)
         T1 = real(lire_date(sT1,tinf,date_format),kind=long)
         Z1 = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         sT2 = next_string(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         T2 = real(lire_date(sT2,tinf,date_format),kind=long)
         Z2 = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Gain = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Power = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         dtmove = next_real(regle(i),separateur,next_field)
         if (ierr == 0) call erreur_lecture('VAR',regle(i))
         if (is_zero(dtmove)) dtmove = 1._long
         if (iuv_ne /= 2 .and. iuv_ne /= 4 .and. iuv_ne /= 6) call err040(u,all_OuvEle(ne)%cvar,regle(i))
         if (t < t1 .OR. t > t2) then
            comment = no_comment
         else
            Zref = Z1 + (Z2-Z1)/(T2-T1)*(T-T1)
            call Vanne_Z2t(ne,nbrf,Pmrf,Zref,Gain,Power,Z,dtmove,comment)
         endif

      elseif (type_regle(1:3)=='Z2Y' .or. type_regle(1:3)=='Z2M' .or. &
              type_regle(1:3)=='Z2W' .or. type_regle(1:3)=='Z2D') then
         nbrf = next_int(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Pmrf = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         sT1 = next_string(regle(i),separateur,next_field)
         Z1 = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         sT2 = next_string(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Z2 = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Gain = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Power = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         dtmove = next_real(regle(i),separateur,next_field)
         if (ierr == 0) call erreur_lecture('VAR',regle(i))
         tm1 = get_tm(t)
         tm2 = get_tm(t)
         wday = tm1%tm_wday
         select case (type_regle(1:3))
         case('Z2Y')
           read(sT2(1:2),'(i4)',iostat=ierr) tm1%tm_mon
           tm1%tm_mon = tm1%tm_mon-1
           read(sT1(4:5),'(i4)',iostat=ierr) tm1%tm_mday
           read(sT1(7:8),'(i4)',iostat=ierr) tm1%tm_hour
           read(sT1(10:11),'(i4)',iostat=ierr) tm1%tm_min
           read(sT1(13:14),'(i4)',iostat=ierr) tm1%tm_sec
           read(sT2(1:2),'(i4)',iostat=ierr) tm2%tm_mon
           tm2%tm_mon = tm2%tm_mon-1
           read(sT2(4:5),'(i4)',iostat=ierr) tm2%tm_mday
           read(sT2(7:8),'(i4)',iostat=ierr) tm2%tm_hour
           read(sT2(10:11),'(i4)',iostat=ierr) tm2%tm_min
           read(sT2(13:14),'(i4)',iostat=ierr) tm2%tm_sec
           call get_t1t2_annual(t, tm1, tm2, t1, t2)
         case('Z2M')
           read(sT1(1:2),'(i4)',iostat=ierr) tm1%tm_mday
           read(sT1(4:5),'(i4)',iostat=ierr) tm1%tm_hour
           read(sT1(7:8),'(i4)',iostat=ierr) tm1%tm_min
           read(sT1(10:11),'(i4)',iostat=ierr) tm1%tm_sec
           read(sT2(1:2),'(i4)',iostat=ierr) tm2%tm_mday
           read(sT2(4:5),'(i4)',iostat=ierr) tm2%tm_hour
           read(sT2(7:8),'(i4)',iostat=ierr) tm2%tm_min
           read(sT2(10:11),'(i4)',iostat=ierr) tm2%tm_sec
           call get_t1t2_monthly(t, tm1, tm2, t1, t2)
         case('Z2W')
           read(sT1(1:2),'(i4)',iostat=ierr) tm1%tm_wday
           read(sT1(4:5),'(i4)',iostat=ierr) tm1%tm_hour
           read(sT1(7:8),'(i4)',iostat=ierr) tm1%tm_min
           read(sT1(10:11),'(i4)',iostat=ierr) tm1%tm_sec
           offset = tm1%tm_wday - wday
           tm1%tm_mday = tm1%tm_mday + offset
           read(sT2(1:2),'(i4)',iostat=ierr) tm2%tm_wday
           read(sT2(4:5),'(i4)',iostat=ierr) tm2%tm_hour
           read(sT2(7:8),'(i4)',iostat=ierr) tm2%tm_min
           read(sT2(10:11),'(i4)',iostat=ierr) tm2%tm_sec
           offset = tm2%tm_wday - wday
           tm2%tm_mday = tm2%tm_mday + offset
           call get_t1t2_weekly(t, tm1, tm2, t1, t2)
         case('Z2D')
           read(sT1(1:2),'(i4)',iostat=ierr) tm1%tm_hour
           read(sT1(4:5),'(i4)',iostat=ierr) tm1%tm_min
           read(sT1(7:8),'(i4)',iostat=ierr) tm1%tm_sec
           read(sT2(1:2),'(i4)',iostat=ierr) tm2%tm_hour
           read(sT2(4:5),'(i4)',iostat=ierr) tm2%tm_min
           read(sT2(7:8),'(i4)',iostat=ierr) tm2%tm_sec
           call get_t1t2_daily(t, tm1, tm2, t1, t2)
         end select

         if (is_zero(dtmove)) dtmove = 1._long
         if (iuv_ne /= 2 .and. iuv_ne /= 4 .and. iuv_ne /= 6) call err040(u,all_OuvEle(ne)%cvar,regle(i))
         if (t < t1 .OR. t > t2) then
            comment = no_comment
         else
            Zref = Z1 + (Z2-Z1)/(T2-T1)*(T-T1)
            call Vanne_Z2t(ne,nbrf,Pmrf,Zref,Gain,Power,Z,dtmove,comment)
         endif

      elseif (type_regle(1:3)=='Q2Y' .or. type_regle(1:3)=='Q2M' .or. &
              type_regle(1:3)=='Q2W' .or. type_regle(1:3)=='Q2D') then
         nbrf = next_int(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Pmrf = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         sT1 = next_string(regle(i),separateur,next_field)
         Z1 = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         sT2 = next_string(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Z2 = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Gain = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Power = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         dtmove = next_real(regle(i),separateur,next_field)
         if (ierr == 0) call erreur_lecture('VAR',regle(i))
         tm1 = get_tm(t)
         tm2 = get_tm(t)
         wday = tm1%tm_wday
         select case (type_regle(1:3))
         case('Z2Y')
           read(sT2(1:2),'(i4)',iostat=ierr) tm1%tm_mon
           tm1%tm_mon = tm1%tm_mon-1
           read(sT1(4:5),'(i4)',iostat=ierr) tm1%tm_mday
           read(sT1(7:8),'(i4)',iostat=ierr) tm1%tm_hour
           read(sT1(10:11),'(i4)',iostat=ierr) tm1%tm_min
           read(sT1(13:14),'(i4)',iostat=ierr) tm1%tm_sec
           read(sT2(1:2),'(i4)',iostat=ierr) tm2%tm_mon
           tm2%tm_mon = tm2%tm_mon-1
           read(sT2(4:5),'(i4)',iostat=ierr) tm2%tm_mday
           read(sT2(7:8),'(i4)',iostat=ierr) tm2%tm_hour
           read(sT2(10:11),'(i4)',iostat=ierr) tm2%tm_min
           read(sT2(13:14),'(i4)',iostat=ierr) tm2%tm_sec
           call get_t1t2_annual(t, tm1, tm2, t1, t2)
         case('Z2M')
           read(sT1(1:2),'(i4)',iostat=ierr) tm1%tm_mday
           read(sT1(4:5),'(i4)',iostat=ierr) tm1%tm_hour
           read(sT1(7:8),'(i4)',iostat=ierr) tm1%tm_min
           read(sT1(10:11),'(i4)',iostat=ierr) tm1%tm_sec
           read(sT2(1:2),'(i4)',iostat=ierr) tm2%tm_mday
           read(sT2(4:5),'(i4)',iostat=ierr) tm2%tm_hour
           read(sT2(7:8),'(i4)',iostat=ierr) tm2%tm_min
           read(sT2(10:11),'(i4)',iostat=ierr) tm2%tm_sec
           call get_t1t2_monthly(t, tm1, tm2, t1, t2)
         case('Z2W')
           read(sT1(1:2),'(i4)',iostat=ierr) tm1%tm_wday
           read(sT1(4:5),'(i4)',iostat=ierr) tm1%tm_hour
           read(sT1(7:8),'(i4)',iostat=ierr) tm1%tm_min
           read(sT1(10:11),'(i4)',iostat=ierr) tm1%tm_sec
           offset = tm1%tm_wday - wday
           tm1%tm_mday = tm1%tm_mday + offset
           read(sT2(1:2),'(i4)',iostat=ierr) tm2%tm_wday
           read(sT2(4:5),'(i4)',iostat=ierr) tm2%tm_hour
           read(sT2(7:8),'(i4)',iostat=ierr) tm2%tm_min
           read(sT2(10:11),'(i4)',iostat=ierr) tm2%tm_sec
           offset = tm2%tm_wday - wday
           tm2%tm_mday = tm2%tm_mday + offset
           call get_t1t2_weekly(t, tm1, tm2, t1, t2)
         case('Z2D')
           read(sT1(1:2),'(i4)',iostat=ierr) tm1%tm_hour
           read(sT1(4:5),'(i4)',iostat=ierr) tm1%tm_min
           read(sT1(7:8),'(i4)',iostat=ierr) tm1%tm_sec
           read(sT2(1:2),'(i4)',iostat=ierr) tm2%tm_hour
           read(sT2(4:5),'(i4)',iostat=ierr) tm2%tm_min
           read(sT2(7:8),'(i4)',iostat=ierr) tm2%tm_sec
           call get_t1t2_daily(t, tm1, tm2, t1, t2)
         end select

         if (is_zero(dtmove)) dtmove = 1._long
         if (iuv_ne /= 2 .and. iuv_ne /= 4 .and. iuv_ne /= 6) call err040(u,all_OuvEle(ne)%cvar,regle(i))
         if (t < t1 .OR. t > t2) then
            comment = no_comment
         else
            QQref = Q1 + (Q2-Q1)/(T2-T1)*(T-T1)
            call Vanne_Qaval(is,ne,QQref,Gain,Power,Q,dtmove,comment)
         endif

      elseif (type_regle(1:3)=='Q2T') then
         sT1 = next_string(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         T1 = real(lire_date(sT1,tinf,date_format),kind=long)
         Q1 = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         sT2 = next_string(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         T2 = real(lire_date(sT2,tinf,date_format),kind=long)
         Q2 = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Gain = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Power = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         dtmove = next_real(regle(i),separateur,next_field)
         if (ierr == 0) call erreur_lecture('VAR',regle(i))
         if (is_zero(dtmove)) dtmove = 1._long
         if (iuv_ne /= 2 .and. iuv_ne /= 4 .and. iuv_ne /= 6) call err040(u,all_OuvEle(ne)%cvar,regle(i))
         if (t < t1 .OR. t > t2) then
            comment = no_comment
         else
            QQref = Q1 + (Q2-Q1)/(T2-T1)*(T-T1)
            call Vanne_Qaval(is,ne,QQref,Gain,Power,Q,dtmove,comment)
         endif

      elseif (type_regle(1:7)=='TURBINE') then
         nbrf = next_int(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Pmrf = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Q1 = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Z1 = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Q2 = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Z2 = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         dt_off = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         dt_on = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         qptr_min = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         zptr_min = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         if (ierr == 0) call erreur_lecture('VAR',regle(i))
         if (iuv_ne /= 2 .and. iuv_ne /= 4 .and. iuv_ne /= 6) call err040(u,all_OuvEle(ne)%cvar,regle(i))
         call Vanne_Usine(ib,nbrf,pmrf,ne,z,q,q1,z1,q2,z2,dt_on,dt_off,qptr_min,zptr_min,comment)

      elseif (type_regle(1:10)=='PLAGELIBRE') then
         nbrf = next_int(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Pmrf = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Q1 = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Z1 = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Q2 = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Z2 = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Gain = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         Power = next_real(regle(i),separateur,next_field) ; ierr = min(ierr,next_field)
         dtmove = next_real(regle(i),separateur,next_field)
         if (ierr == 0) call erreur_lecture('VAR',regle(i))
         if (is_zero(dtmove)) dtmove = 1._long
         if (iuv_ne /= 2 .and. iuv_ne /= 4 .and. iuv_ne /= 6) call err040(u,all_OuvEle(ne)%cvar,regle(i))
         isrf = la_topo%biefs(la_topo%net%rang(nbrf))%is1 !On observe le débit à l'entrée du bief où se trouve le point régulé.
         if (Q(isrf) < Q1 .or. Q(isrf) > Q2) then
            comment = no_comment
         else
            isrf = iSect(nbrf,pmrf) !point de réglage du niveau
            if (z(isrf) < Z1 .OR. z(isrf) > Z2) then
               Zref = Z1 + 0.5_long * (Z2-Z1)
               call Vanne_Zcst(ne,nbrf,Pmrf,Zref,Gain,Power,Z,.false.,dtmove,comment)
            else
               comment = no_comment
            endif
         endif

      endif
      if (trim(comment) /= no_comment) write(l9,'(8a)') '   ',trim(Nom_Ouvrage),' ', &
                                       trim(Nom_Regle),' ',trim(Type_Regle),' ',trim(comment)
   enddo
end subroutine RegLoc
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine PFlot(ne,z1,z2,t,dt0,dt1,dh,np,nnp,c_pos)
!==============================================================================
!          simulation d'une porte à flot par la fermeture d'un orifice
! Le clapet est ouvert si la cote amont Z1 est superieure à la cote aval Z2
!
! INDIC est l'indicateur de la position du clapet :
!       indic = 1  : clapet complètement ouvert
!       indic = 2  : clapet en cours d'ouverture
!       indic = 3  : clapet complètement fermé
!       indic = 4  : clapet en cours de fermeture
! NNP = nombre de pompes couplées au clapet (nnp = 0 : pas de pompe)
! NP  = indices des pompes couplées au clapet
!
! C_POS = position du clapet <- correspond à la valeur d'INDIC
!==============================================================================
   use parametres, only: long, lTra, l9
   use, intrinsic :: iso_fortran_env, only: error_unit

   use data_num_fixes, only: dtbase
   use premierappel, only : nap => nap_pflot
   use Ouvrages, only: all_OuvEle, change_position
   use TopoGeometrie, only: la_topo
   implicit none

   ! -- Prototype --
   integer,intent(in) :: ne,np(10),nnp
   real(kind=long),intent(in) :: z1,z2,t,dt0,dh
   real(kind=long),intent(out) :: dt1
   character(len=9) :: c_pos
   ! -- Constantes --
   real(kind=long),parameter :: fact = 1._long/3._long  ! facteur de reduction du pas de temps
   ! -- Variables --
   integer,allocatable,save :: indic(:)
   real(kind=long),allocatable,save :: wmin(:)  ! = ouverture minimale du clapet ne
   real(kind=long),allocatable,save :: wmax(:)  ! = ouverture maximale du clapet ne
   integer :: u, i
   integer :: nsmax

   nsmax = size(all_OuvEle)
   u = lTra
   !---vérification que l'ouvrage élémentaire NE est bien un orifice
   if (all_OuvEle(ne)%iuv /= 6) then
      do i = 1, 2
         if (i==2) u = error_unit
         write(u,*) ne,all_OuvEle(ne)%iuv
         write(u,'(a)') ' PFLOT : erreur clapet 001'
         write(u,'(1x,2a)') all_OuvEle(ne)%cvar,' doit être de type orifice pour pouvoir fonctionner en clapet'
      enddo
      stop 122
   endif
   !---initialisation
   if (nap<1) then
      allocate (indic(nsmax), wmin(nsmax), wmax(nsmax))
      indic(1:nsmax) = 1
      wmax(1:nsmax) = -1._long
      wmin(1:nsmax) = -1._long
      nap=nap+1
   endif
   !---sauvegarde de l'ouverture de base du clapet ne et état initial du clapet
   if (wmax(ne) < 0._long) then
      wmin(ne)=min(all_OuvEle(ne)%uv3,all_OuvEle(ne)%uv5)
      wmax(ne)=max(all_OuvEle(ne)%uv3,all_OuvEle(ne)%uv5)
      write(l9,'(2a,2(a,f6.3))') ' Clapet : ',all_OuvEle(ne)%cvar,'   Ouverture minimale : ',wmin(ne), &
                                                      '   Ouverture maximale : ',wmax(ne)
      if (z1>z2) then   !le clapet est ouvert
         indic(ne) = 1
         c_pos = 'Ouvert   '
         call change_position(ne,all_OuvEle(ne)%uv3,wmax(ne),t,t+dt0)
         all_OuvEle(ne)%uv5 = wmin(ne)
         if (nnp>0) all_OuvEle(np(1:nnp))%za3 = 999._long  !blocage des pompes np(1) à np(nnp)
      else              !le clapet est fermé
         indic(ne) = 3
         c_pos = 'Fermé   '
         call change_position(ne,all_OuvEle(ne)%uv3,wmin(ne),t,t+dt0)
         all_OuvEle(ne)%uv5 = wmax(ne)
         if (nnp>0) all_OuvEle(np(1:nnp))%za3 = 0.1_long  !déblocage des pompes np(1) à np(nnp)
      endif
   endif

   !---détermination de la nouvelle position du clapet
   if (indic(ne)==1 .and. z1>z2) then
      !---le clapet est ouvert et doit le rester >>> on ne fait rien
      c_pos = 'Ouvert   '
      dt1 = dtbase
   else if (indic(ne)==3 .and. z1<=z2+dh) then
      !---le clapet est fermé et doit le rester >>> on ne fait rien
      c_pos = 'Fermé   '
      dt1 = dtbase
   else if (indic(ne)==3 .and. z1>z2+dh) then
      !---le clapet est fermé et doit être ouvert
      call change_position(ne,wmin(ne),wmax(ne),t,t+dt0)
      indic(ne) = 2
      c_pos = 'Ouverture'
      all_OuvEle(ne)%uv5 = wmin(ne)
      dt1 = dt0*fact
      write(l9,*) ' Ouverture du clapet ',all_OuvEle(ne)%cvar,wmin(ne),wmax(ne)
      if (nnp>0) all_OuvEle(np(1:nnp))%za3 = 999._Long  !blocage des pompes np(1) à np(nnp)
   else if (indic(ne)==1 .and. z1<=z2) then
      !---le clapet est ouvert et doit être fermé
      call change_position(ne,wmax(ne),wmin(ne),t,t+dt0)
      indic(ne) = 4
      c_pos = 'Fermeture'
      all_OuvEle(ne)%uv5 = wmax(ne)
      dt1 = dt0*fact
      write(l9,*) ' Fermeture du clapet ',all_OuvEle(ne)%cvar,wmin(ne),wmax(ne)
      if (nnp>0) all_OuvEle(np(1:nnp))%za3 = 0.1_Long  !déblocage des pompes np(1) à np(nnp)
   else if (indic(ne)==2) then
      if (all_OuvEle(ne)%uv3 < wmax(ne)*0.999_long) then
         !---le clapet est en cours d'ouverture >>> on réduit le pas de temps
         c_pos = 'Ouverture'
         dt1 = dt0*fact
      else
         !---le clapet est en fin d'ouverture >>> on met indic(ne) a 1
         indic(ne) = 1
         c_pos = 'Ouvert   '
         dt1 = dtbase
         if (nnp>0) all_OuvEle(np(1:nnp))%za3 = 999._Long  !blocage des pompes np(1) à np(nnp)
      endif
   else if (indic(ne)==4) then
      if (all_OuvEle(ne)%uv3>wmin(ne)*1.001_long) then
         !---le clapet est en cours de fermeture >>> on réduit le pas de temps
         c_pos = 'Fermeture'
         dt1 = dt0*fact
      else
         !---le clapet est en fin de fermeture >>> on met indic(ne) à 3
         indic(ne) = 3
         c_pos = 'Fermé   '
         dt1 = dtbase
         if (nnp>0) all_OuvEle(np(1:nnp))%za3 = 0.1_Long  !déblocage des pompes np(1) à np(nnp)
      endif
   else
      u = lTra
      do i = 1, 2
         if (i==2) u = error_unit
         write(u,'(a)') ' ERREUR dans PFLOT : cas non prévu'
         write(u,*) ne,indic(ne),z1,z2,wmax(ne),wmin(ne)
         write(u,'(a)') ' Merci d''envoyer un rapport de bug'
      enddo
      stop 123
   endif
end subroutine PFlot
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine Vanne_Zcst(nevm,nbrf,Pmrf,ZRef,Gain,Power,Z,TypeAval,dtmove,comment)
!==============================================================================
!         Régulation d'un niveau amont ou aval par déplacement d'une vanne
!
!
!  ISVM : numéro de section de la vanne mobile
!  NEVM : numéro de l'ouvrage élémentaire de la vanne mobile
!  NBRF : numéro du bief du point contrôlé
!  PMRF : Pk du point contrôlé
!  ZREF : valeur de la cote à garder constante au point contrôlé
!  GAIN : gain du regulateur
!  POWER : exposant du regulateur
!  Z : ligne d'eau
!  TypeAval : Type de la régulation : vrai si vanne à niveau aval constant
!                                     faux si vanne à niveau amont constant
!  dtmove : vitesse de la manœuvre (défaut = 1 s/m)
!  comment : texte à afficher sur OUTPUT
!==============================================================================
   use parametres, only: long, lTra
   use, intrinsic :: iso_fortran_env, only: error_unit

   use data_num_fixes, only: tmax
   use data_num_mobiles, only: t
   use solution, only: dz
   use Ouvrages, only: all_OuvEle, change_position
   use TopoGeometrie, only: iSect
   implicit none

   ! -- Prototype --
   integer,intent(in) :: nevm, nbrf
   real(kind=long),intent(in) :: zref, gain, power, z(:), pmrf
   real(kind=long),intent(in) :: dtmove
   logical,intent(in) :: typeaval
   character(len=80), intent(out) :: comment

   ! -- Variables --
   real(kind=Long) :: DW
   real(kind=Long) :: TMove ! instant de fin de la manœuvre de correction
   real(kind=Long) :: W     ! ouverture actuelle
   real(kind=Long) :: Zreg  ! niveau amont ou aval de l'ouvrage
   integer :: isrf
   character(len=3) :: OK
   character(len=180) :: err_message

   !------------------------------------------------------------------------------
   isrf = iSect(nbrf,pmrf)  !localisation de la section de référence
   if (isrf == 0) then
      write(err_message,'(a,i3,a)') ' >>>> Fichier VAR (Zcst) :  le bief ',nbrf,' est inconnu sur ce réseau <<<<'
      write(lTra,'(a)') trim(err_message)
      write(error_unit,'(a)') trim(err_message)
      stop 192
   else if (isrf < 0) then
      write(err_message,'(2a,f9.2,a,i3,a)')' >>>> Fichier VAR (Zcst) : il n''y a pas de', &
                                           ' section à l''abscisse ',pmrf,' dans le bief ',nbrf,' <<<<'
      write(lTra,'(a)') trim(err_message)
      write(error_unit,'(a)') trim(err_message)
      stop 193
   endif

   ok = '!!!'
   w = all_OuvEle(nevm)%uv3
   zreg = z(isrf)                      ! cote régulée
   if (zreg > zref .and. dz(isrf) < 0._long) then
      dw = 0._long  !on ne fait rien car le niveau va dans le bon sens
      ok = '\\\'
   elseif (zreg < zref .and. dz(isrf) > 0._long) then
      dw = 0._long  !on ne fait rien car le niveau va dans le bon sens
      ok = '\\\'
   else
      dw = gain*abs(zreg-zref)**power   ! correction de l'ouverture
      if (typeaval) then
         dw = sign(dw,zref-zreg)             ! définition du sens de la correction
      else
         dw = sign(dw,zreg-zref)             ! définition du sens de la correction
      endif
      if (dw < 0._long .and. w+dw < 0._long) then  !mise à zéro des ouvertures négatives
         dw = 0.001_long-w  ! la vanne sera fermée
      endif
      if (w+dw > abs(all_OuvEle(nevm)%uv5)) then  !limitation de l'ouverture à sa valeur maximale
         dw = all_OuvEle(nevm)%uv5-w  ! la vanne sera ouverte
      endif
   endif
   if (abs(zreg-zref) < 0.05_long) ok = 'OK '
   write(comment,'(2a,f8.3,a,f8.3,a,2f8.4)') '==> ', ok, zreg,' (cible :',zref,')', w, dw

   !---application de la correction
   w = w+dw                           ! nouvelle ouverture
   tmove = t+1._long+abs(dw)*dtmove   !on utilise dtmove (s/m) pour définir la vitesse de la vanne
   if (t < tmax .and. abs(dw) > 1.e-04_long) call change_position(nevm,all_OuvEle(nevm)%uv3,w,t,tmove)

end subroutine Vanne_Zcst
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine Vanne_Z2t(nevm,nbrf,Pmrf,ZRef,Gain,Power,Z,dtmove,comment)
!==============================================================================
!         Régulation d'un niveau amont ou aval par déplacement d'une vanne
!
!
!  Cette variante de Vanne_Zcst() ne se pose pas de question pour savoir si
!  ça vaut la peine de corriger la position de la vanne en regardant si le
!  niveau va dans le bon sens.
!
!  ISVM : numéro de section de la vanne mobile
!  NEVM : numéro de l'ouvrage élémentaire de la vanne mobile
!  NBRF : numéro du bief du point contrôlé
!  PMRF : Pk du point contrôlé
!  ZREF : valeur de la cote à garder constante au point contrôlé
!  GAIN : gain du regulateur
!  POWER : exposant du regulateur
!  Z : ligne d'eau
!  dtmove : vitesse de la manœuvre (défaut = 1 s/m)
!  comment : texte à afficher sur OUTPUT
!==============================================================================
   use parametres, only: long, lTra
   use, intrinsic :: iso_fortran_env, only: error_unit

   use data_num_fixes, only: tmax
   use data_num_mobiles, only: t
   use Ouvrages, only: all_OuvEle, change_position
   use TopoGeometrie, only: iSect
   implicit none

   ! -- Prototype --
   integer,intent(in) :: nevm, nbrf
   real(kind=long),intent(in) :: zref, gain, power, z(:), pmrf
   real(kind=long),intent(in) :: dtmove
   character(len=80), intent(out) :: comment

   ! -- Variables --
   real(kind=Long) :: DW
   real(kind=Long) :: TMove ! instant de fin de la manœuvre de correction
   real(kind=Long) :: W     ! ouverture actuelle
   real(kind=Long) :: Zreg  ! niveau amont ou aval de l'ouvrage
   integer :: isrf
   character(len=3) :: OK
   character(len=180) :: err_message

   !------------------------------------------------------------------------------
   isrf = iSect(nbrf,pmrf)  !localisation de la section de référence
   if (isrf == 0) then
      write(err_message,'(a,i3,a)') ' >>>> Fichier VAR (Z2t) :  le bief ',nbrf,' est inconnu sur ce réseau <<<<'
      write(lTra,'(a)') trim(err_message)
      write(error_unit,'(a)') trim(err_message)
      stop 192
   else if (isrf < 0) then
      write(err_message,'(2a,f9.2,a,i3,a)')' >>>> Fichier VAR (Z2t) : il n''y a pas de', &
                                           ' section à l''abscisse ',pmrf,' dans le bief ',nbrf,' <<<<'
      write(lTra,'(a)') trim(err_message)
      write(error_unit,'(a)') trim(err_message)
      stop 193
   endif

   ok = '!!!'
   w = all_OuvEle(nevm)%uv3
   zreg = z(isrf)                      ! cote régulée
   dw = gain*abs(zreg-zref)**power     ! correction de l'ouverture
   dw = sign(dw,zreg-zref)             ! définition du sens de la correction
   if (dw < 0._long .and. w+dw < 0._long) then  !mise à zéro des ouvertures négatives
      dw = 0.001_long-w  ! la vanne sera fermée
   endif
   if (w+dw > abs(all_OuvEle(nevm)%uv5)) then  !limitation de l'ouverture à sa valeur maximale
      dw = all_OuvEle(nevm)%uv5-w  ! la vanne sera ouverte
   endif

   if (abs(zreg-zref) < 0.05_long) ok = 'OK '
   write(comment,'(2a,f8.3,a,f8.3,a,2f8.4)') '==> ', ok, zreg,' (cible :',zref,')', w, dw

   !---application de la correction
   w = w+dw                           ! nouvelle ouverture
   tmove = t+1._long+abs(dw)*dtmove   !on utilise dtmove (s/m) pour définir la vitesse de la vanne
   if (t < tmax .and. abs(dw) > 1.e-04_long) call change_position(nevm,all_OuvEle(nevm)%uv3,w,t,tmove)

end subroutine Vanne_Z2t
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine Vanne_Qaval(isvm,nevm,QRef,Gain,Power,Q,dtmove,comment)
!==============================================================================
!                 Régulation des vannes à débit constant
!
!  ISVM : numéro de section de la vanne mobile
!  NEVM : numéro de l'ouvrage élémentaire de la vanne mobile
!  QREF : valeur du débit qui doit passer dans la section de l'ouvrage
!  GAIN : gain du régulateur
!  POWER : exposant du régulateur
!  Q : ligne d'eau
!  dtmove : durée de la manœuvre (défaut = 1s)
!  comment : texte à afficher sur OUTPUT
!==============================================================================
   use parametres, only: long
   use data_num_fixes, only: tmax
   use data_num_mobiles, only: t
   use solution, only: dq
   use Ouvrages, only: all_OuvEle, change_position
   implicit none

   ! -- Prototype --
   integer,intent(in) :: isvm, nevm
   real(kind=long),intent(in) :: qref, gain, power, q(:)
   real(kind=long),intent(in) :: dtmove
   character(len=80), intent(out) :: comment

   ! -- Variables --
   real(kind=Long) :: DW
   real(kind=Long) :: TMove ! instant de fin de la manœuvre de correction
   real(kind=Long) :: W     ! ouverture actuelle
   real(kind=Long) :: Qreg  ! débit amont ou aval de l'ouvrage
   character(len=3) :: OK

   !------------------------------------------------------------------------------
   ok = '!!!'
   w = all_OuvEle(nevm)%uv3
   qreg = q(isvm)                      ! débit régulé
   if (Qreg > Qref .and. dq(isvm) < 0._long) then
      dw = 0._long  !on ne fait rien car le débit va dans le bon sens
      ok = '\\\'
   elseif (Qreg < Qref .and. dq(isvm) > 0._long) then
      dw = 0._long  !on ne fait rien car le débit va dans le bon sens
      ok = '\\\'
   else
      dw = gain*(abs(Qreg-Qref))**power   ! correction de l'ouverture
      dw = sign(dw,Qref-Qreg)             ! définition du sens de la correction
      if (dw < 0._long .and. w+dw < 0._long) then  !mise à zéro des ouvertures négatives
         dw = 0.001_long-w  ! la vanne sera fermée
      endif
      if (w+dw > abs(all_OuvEle(nevm)%uv5)) then  !limitation de l'ouverture à sa valeur maximale
         dw = all_OuvEle(nevm)%uv5-w  ! la vanne sera ouverte
      endif
   endif
   if (abs(Qreg-Qref) < 0.01_long*Qref) ok = 'OK '
   write(comment,'(2a,f8.3,a,f8.3,a,2f8.4)') '==> ', ok, Qreg,' (cible :',Qref,')', w, dw

   !---application de la correction
   w = w+dw                           ! nouvelle ouverture
   tmove = t+1._long+abs(dw)*dtmove   !on utilise dtmove (s/m) pour définir la vitesse de la vanne
   if (t < tmax .and. abs(dw) > 1.e-04_long) call change_position(nevm,all_OuvEle(nevm)%uv3,w,t,tmove)

end subroutine Vanne_Qaval
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine Vanne_Qmax(isvm,nevm,NBRF,PMRF,Qmax,Qrsv,Gain,Power,dtmove,Q,comment)
!==============================================================================
!           Régulation des vannes de dérivation de débit
!               correspond à Vanne_Qmax2() de Mage-7
!
!  L'objectif est de laisser passer à travers la vanne le débit nécessaire
!  pour que le débit dans la section de référence (bief nbvm et Pm local pmvm)
!  ne dépasse pas un débit maximum prescrit (Qmax)
!  Principe de la régulation :
!           si Qref < Qmax : la vanne est fermée
!           si Qref > Qmax : on ouvre la vanne proportionnellement à l'écart
!                            de débit
!           si le débit est en train de varier dans le bon sens on ne change rien
!
!  ISVM : numéro de section où se trouve la vanne de régulation
!  NEVM : numéro d'ouvrage élémentaire de la vanne
!  NBRF : numéro du bief contenant la section de référence
!  PMRF : Pm de la section de référence
!  Qmax : Débit maximum admis dans la section de référence
!  Qrsv : Débit réservé à laisser traverser l'ouvrage (minimum)
!  GAIN : gain du régulateur
!  POWER : exposant du régulateur
!  dtmove : durée de la manœuvre (défaut = 1s)
!  Q : débit
!  comment : texte à afficher sur OUTPUT
!==============================================================================
   use parametres, only: long, lTra
   use, intrinsic :: iso_fortran_env, only: error_unit

   use data_num_fixes, only: tmax
   use data_num_mobiles, only: t
   use solution, only: dq
   use Ouvrages, only: all_OuvEle, change_position
   use TopoGeometrie, only: iSect
   implicit none
   ! -- Prototype --
   integer,intent(in) :: isvm, nevm, nbrf
   real(kind=long),intent(in) :: pmrf,qmax,gain,power,q(:),qrsv
   real(kind=long),intent(in) :: dtmove
   character(len=80), intent(out) :: comment

   ! -- Variables --
   integer :: isrf          ! numero de la section de reference
   real(kind=Long) :: dw, dw1, dw2
   real(kind=Long) :: TMove ! instant de fin de la manoeuvre de correction
   real(kind=Long) :: W     ! ouverture actuelle
   character(len=3):: Qmax_alert, Qrsv_Alert, OK
   character(len=180) :: err_message

   isrf = iSect(nbrf,pmrf)  !localisation de la section de référence
   if (isrf == 0) then
      write(err_message,'(a,i3,a)') ' >>>> Fichier VAR (loi QMAX) : le bief ',nbrf,' est inconnu sur ce réseau <<<<'
      write(error_unit,'(a)') trim(err_message)  ;  write(lTra,'(a)') trim(err_message)
      stop 192
   else if (isrf < 0) then
      write(err_message,'(2a,f9.2,a,i3,a)') ' >>>> Fichier VAR (loi QMAX)  : il n''y a pas de', &
                                            ' section à l''abscisse ',pmrf,' dans le bief ',nbrf,' <<<<'
      write(error_unit,'(a)') trim(err_message)  ;  write(lTra,'(a)') trim(err_message)
      stop 193
   endif
   !---position actuelle de la vanne
   OK = '!!!'
   w = all_OuvEle(nevm)%uv3 ; dw1 = 0._long ; dw2 = 0._long
   !---calcul de la correction
   if (Q(isrf) > Qmax) then      !débit max dépassé -> il faut ouvrir la vanne
      if (dq(isrf) < 0._long) then
         dw = 0._long  !le débit est déjà en train de baisser
         ok = '!!!'
      else
         dw = w*gain*((Q(isrf)-Qmax)/Qrsv)**power
      endif
   elseif (Q(isvm) < Qrsv) then  !débit réservé non satisfait -> il faut ouvrir la vanne
      if (dq(isvm) > 0._long) then
         dw = 0._long  !le débit est déjà en train de monter
         ok = '!!!'
      else
         dw = w*gain*((Qrsv-Q(isvm))/Qrsv)**power
      endif
   else
      dw1 = -gain*w*((Qmax-Q(isrf))/Qrsv)**power
      dw2 = -gain*w*((Q(isvm)-Qrsv)/Qrsv)**power
      dw = max(dw1,dw2)          !on peut fermer la vanne mais pas trop
   endif
   !---mise à zéro des ouvertures négatives
   if (dw < 0._long .and. w+dw < 0._long) then
      dw = 0.001_long-w  ! la vanne est fermée
   endif
   !---limitation de l'ouverture à sa valeur maximale
   if (w+dw > all_OuvEle(nevm)%uv5) then
      dw = all_OuvEle(nevm)%uv5-w
   endif
   Qmax_Alert = '   ' ; if (Q(isrf) > 1.01_long*Qmax) Qmax_Alert = '>>!'
   Qrsv_Alert = '   ' ; if (Q(isvm) < 0.99_long*Qrsv) Qrsv_Alert = '<<!'
   if (Q(isrf) < 1.01_long*Qmax .and. Q(isvm) > 0.99_long*Qrsv) OK = 'OK '
   write(comment,'(2a,2(f10.3,a),4f8.4)') '==> ', OK, Q(isrf),Qmax_Alert, Q(isvm), Qrsv_Alert, w, dw, dw1, dw2

   !---application de la correction
   w = w+dw  ! nouvelle ouverture
   !tmove = t+dtmove
   tmove = t+1._long+abs(dw)*dtmove !on utilise dtmove (s/m) pour définir la vitesse de la vanne
   if (t < tmax .and. abs(dw) > 1.e-04_long) call change_position(nevm,all_OuvEle(nevm)%uv3,w,t,tmove)

end subroutine Vanne_Qmax
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine Vanne_Usine(ib,nbrf,pmrf,ne,Z,Q,Q1,Z1,Q2,Z2,dt_on,dt_off,qptr_min,zptr_min,comment)
!==============================================================================
!                     Régulation d'une usine type CNR
!
! Paramètres :
!     ib : bief à l'entrée duquel on observe le débit
!     nbrf (bief) et ptrf (pm) : point de réglage du niveau
!     ne : ouvrage élémentaire de type vanne qui modélise l'usine
!     Z et Q : niveaux et débits
!     (Q1,Z1) et (Q2,Z2) : fuseau de fonctionnement de l'usine
!     dt_on et dt_off : pas de temps d'ouverture et de fermeture de la vanne
!     qptr_min et zptr_min : débit et cote minimaux au point de réglage
!==============================================================================
   use parametres, only: long, lTra
   use, intrinsic :: iso_fortran_env, only: error_unit

   use data_num_mobiles, only: t
   use Ouvrages, only: all_OuvEle, change_position
   use TopoGeometrie, only: la_topo, iSect
   implicit none
   ! -- Prototype --
   integer,intent(in) :: ib, ne, nbrf
   real(kind=long),dimension(:),intent(in) :: q,z
   real(kind=long), intent(in) :: Q1, Z1, Q2, Z2
   real(kind=long),intent(in) :: pmrf,dt_on,dt_off,qptr_min,zptr_min
   character(len=80), intent(out) :: comment

   ! -- Variables --
   integer :: iptr, isrf
   real(kind=Long) :: w0=0.001_long     ! ouverture minimale
   real(kind=Long) :: Zref
   real(kind=Long) :: t1, w1, t2, w2
   character(len=180) :: err_message
!------------------------------------------------------------------------------
   iptr = iSect(nbrf,pmrf) !point de réglage du niveau
   if (iptr == 0) then
      write(err_message,'(a,i3,a)') ' >>>> Fichier VAR (Usine) : le bief ',nbrf,' est inconnu sur ce réseau <<<<'
      write(error_unit,'(a)') trim(err_message)  ;  write(lTra,'(a)') trim(err_message)
      stop 192
   else if (iptr < 0) then
      write(err_message,'(a,f9.2,a,i3,a)') ' >>>> Fichier VAR (Usine)  : il n''y a pas de section à l''abscisse ',&
                                           pmrf,' dans le bief ',nbrf,' <<<<'
      write(error_unit,'(a)') trim(err_message)  ;  write(lTra,'(a)') trim(err_message)
      stop 193
   endif

   isrf = la_topo%biefs(la_topo%net%rang(ib))%is1  !on observe le débit à l'entrée du bief qui contient l'ouvrage

   if (Q(isrf) < Q1 .or. Q(isrf) > Q2) then                          !on ne fait rien
      write(comment,'(a,f10.3)') '==> hors plage de débit : ',Q(isrf)
   else
      Zref = Z1 + (Z2-Z1)/(Q2-Q1)*(Q(isrf)-Q1)
      if ((z(iptr) < Zptr_min+0.05_long .or. q(iptr) < 2._long*qptr_min) .and. &
           all_OuvEle(ne)%uv3 > all_OuvEle(ne)%uv5-2._long*w0) then !vanne ouverte, on ferme la vanne
         call change_position(ne,all_OuvEle(ne)%uv3,w0,t,t+dt_off)
         write(comment,'(a,f10.3,3f8.3)') '==> fermeture ', Q(iptr),Z(iptr),Zref,all_OuvEle(ne)%uv3
      else if (z(iptr) > Zref .and. all_OuvEle(ne)%uv3 < 2._long*w0) then            !vanne fermée, on ouvre la vanne
         call change_position(ne,all_OuvEle(ne)%uv3,all_OuvEle(ne)%uv5,t,t+dt_on)
         write(comment,'(a,f10.3,3f8.3)') '==> ouverture ',Q(iptr),Z(iptr),Zref,all_OuvEle(ne)%uv3
      else                                                            !on laisse la vanne en l'état
         t1 = all_OuvEle(ne)%tz(all_OuvEle(ne)%itw)
         t2 = all_OuvEle(ne)%tz(all_OuvEle(ne)%jtw)
         w1 = all_OuvEle(ne)%wtz(all_OuvEle(ne)%itw)
         w2 = all_OuvEle(ne)%wtz(all_OuvEle(ne)%jtw)
         if (t < t1 .or. t > t2) then
            write(comment,'(a,f10.3,3f8.3)') '==> immobile  ',Q(iptr),Z(iptr),Zref,all_OuvEle(ne)%uv3
         else
            if (w1 < w2) then
               write(comment,'(a,f10.3,3f8.3)') '==> ouvrante  ',Q(iptr),Z(iptr),Zref,all_OuvEle(ne)%uv3
            else if (w1 > w2) then
               write(comment,'(a,f10.3,3f8.3)') '==> fermante  ',Q(iptr),Z(iptr),Zref,all_OuvEle(ne)%uv3
            else
               write(comment,'(a,f10.3,3f8.3)') '==> immobile  ',Q(iptr),Z(iptr),Zref,all_OuvEle(ne)%uv3
            endif
         endif
      endif
   endif
end subroutine Vanne_Usine


subroutine Vanne_Turbine(ib,nbrf,pmrf,ne,Z,Q,Q1,Z1,Q2,Z2,dt_on,dt_off,comment)
!==============================================================================
!     régulation d'une usine type CNR : on maintient le niveau au point de
!                                       consigne entre Z1 et Z2 si le débit
!                                       est entre Q1 et Q2
! Paramètres :
!     ib : bief à l'entrée duquel on observe le débit
!     nbrf (bief) et ptrf (pm) : point de réglage du niveau
!     ne : ouvrage élémentaire de type vanne qui modélise l'usine
!     Z et Q : niveaux et débits
!     (Q1,Z1) et (Q2,Z2) : fuseau de fonctionnement de l'usine
!     dt_on et dt_off : pas de temps d'ouverture et de fermeture de la vanne
!==============================================================================
   use parametres, only: long, lTra
   use, intrinsic :: iso_fortran_env, only: error_unit

   use data_num_mobiles, only: t
   use Ouvrages, only: all_OuvEle, change_position
   use TopoGeometrie, only: la_topo, iSect
   implicit none
   ! -- Prototype --
   integer,intent(in) :: ib, ne, nbrf
   real(kind=long),dimension(:),intent(in) :: q,z
   real(kind=long), intent(in) :: Q1, Z1, Q2, Z2
   real(kind=long),intent(in) :: pmrf,dt_on,dt_off
   character(len=80), intent(out) :: comment
   ! -- Variables --
   integer :: iptr, isrf
   real(kind=Long) :: w0=0.001_long     ! ouverture minimale
   real(kind=Long) :: t1, w1, t2, w2
   character(len=180) :: err_message
!------------------------------------------------------------------------------
   iptr = iSect(nbrf,pmrf) !point de réglage du niveau
   if (iptr == 0) then
      write(err_message,'(a,i3,a)') ' >>>> Fichier VAR (Turbine) : le bief ',nbrf,' est inconnu sur ce réseau <<<<'
      write(error_unit,'(a)') trim(err_message)  ;  write(lTra,'(a)') trim(err_message)
      stop 192
   else if (iptr < 0) then
      write(err_message,'(a,f9.2,a,i3,a)') ' >>>> Fichier VAR (Turbine)  : il n''y a pas de section à l''abscisse ',&
                                           pmrf,' dans le bief ',nbrf,' <<<<'
      write(error_unit,'(a)') trim(err_message)  ;  write(lTra,'(a)') trim(err_message)
      stop 193
   endif

   isrf = la_topo%biefs(la_topo%net%rang(ib))%is1  !on observe le débit à l'entrée du bief qui contient l'ouvrage

   if (Q(isrf) < Q1 .or. Q(isrf) > Q2) then                          !on ne fait rien
      write(comment,'(a,f10.3)') '==> hors plage de débit : ',Q(isrf)
   else
      if (z(iptr) < Z1 .and. all_OuvEle(ne)%uv3 > all_OuvEle(ne)%uv5-2._long*w0) then !trop bas et vanne ouverte, on ferme la vanne
         call change_position(ne,all_OuvEle(ne)%uv3,w0,t,t+dt_off)
         write(comment,'(a,f10.3,4f8.3)') '==> fermeture ', Q(isrf),Z(iptr),Z1,Z2,all_OuvEle(ne)%uv3
      else if (z(iptr) > Z2 .and. all_OuvEle(ne)%uv3 < 2._long*w0) then     !trop haut et vanne fermée, on ouvre la vanne
         call change_position(ne,all_OuvEle(ne)%uv3,all_OuvEle(ne)%uv5,t,t+dt_on)
         write(comment,'(a,f10.3,4f8.3)') '==> ouverture ',Q(isrf),Z(iptr),Z1,Z2,all_OuvEle(ne)%uv3
      else                                                            !on laisse la vanne en l'état
         t1 = all_OuvEle(ne)%tz(all_OuvEle(ne)%itw)
         t2 = all_OuvEle(ne)%tz(all_OuvEle(ne)%jtw)
         w1 = all_OuvEle(ne)%wtz(all_OuvEle(ne)%itw)
         w2 = all_OuvEle(ne)%wtz(all_OuvEle(ne)%jtw)
         if (t < t1 .or. t > t2) then
            write(comment,'(a,f10.3,4f8.3)') '==> immobile  ',Q(isrf),Z(iptr),Z1,Z2,all_OuvEle(ne)%uv3
         else
            if (w1 < w2) then
               write(comment,'(a,f10.3,4f8.3)') '==> ouvrante  ',Q(isrf),Z(iptr),Z1,Z2,all_OuvEle(ne)%uv3
            else if (w1 > w2) then
               write(comment,'(a,f10.3,4f8.3)') '==> fermante  ',Q(isrf),Z(iptr),Z1,Z2,all_OuvEle(ne)%uv3
            else
               write(comment,'(a,f10.3,4f8.3)') '==> immobile  ',Q(isrf),Z(iptr),Z1,Z2,all_OuvEle(ne)%uv3
            endif
         endif
      endif
   endif
end subroutine Vanne_Turbine
end module mage_regulation
