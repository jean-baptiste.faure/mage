!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - INRAE - 1989-2020                           #
!# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################


subroutine calcul_dt(convergence_OK,DTR)
!==============================================================================
!       Calcul du pas de temps avec ajustement en fonction de :
!           - convergence ou non des itérations
!           - nombre de Courant max CRMAX
!           - pas de temps minimum DTMIN pour avoir un multiple de dtmin
!           - pas de temps proposé par la régulation DTR
!           - dates d'écriture sur TRA et BIN (TTRA et TBIN)
!==============================================================================
   use Parametres, only: Long, Zero, Un, lTra, l9
   use, intrinsic :: iso_fortran_env, only: error_unit

   use Mage_Utilitaires, only: zegal
   use data_num_fixes, only: dttra,dtbin,ndtf=>ndt,tmax,crmax,dtbase,dtmin, &
                             CRnormal, relax_a, CRtor,relax_b, dtcsv, dtmelissa, with_melissa
   use data_num_mobiles, only: ttra,tbin,t,dt,dtold,cr,dtmin0, FRsup, tcsv, tmelissa
   use casiers, only: dtlat
   use PremierAppel, only : nap => nap_m11
   use data_csv, only: ncsv
   implicit none
   ! -- Prototype --
   logical,intent(in) :: convergence_OK
   real(kind=Long),intent(in) :: DTR
   ! -- Variables locales permanentes --
   logical,save :: ecrit
   integer,save :: idt
   real(kind=long),save :: dtant
   ! -- Variables locales temporaires --
   real(kind=Long) :: ADT, ttra1, tbin1, tmelissa1
   character(len=120) :: message

   !write(l9,*) ' Calcul du pas de temps - entrée : ', dt, t, convergence_OK, ndtf

   if (FRsup > relax_a .AND. relax_a < relax_b) then  !contrôle du CR en phase torrentielle
      CRmax = CRtor
   else
      CRmax = CRnormal
   endif
   if (NAP == 0) then
      idt = ndtf ; nap = nap+1 ; dtant = dtbase ; ecrit = .false.
   endif
   dtmin0 = dtmin
   ! cas où il n'y a pas de fichier csv à écrire
   if (ncsv == 0) tcsv = 10._long * tmax
   !---Sauvegarde de l'ancien pas de temps
   dtold = dt

   !---Si .NOT.convergence_OK : réduction de dt pour non-convergence des itérations
   if (convergence_OK) then
      !---Convergence des itérations
      if (ecrit) dtold = dtant
      if (idt<ndtf) then
         !on n'a pas encore fait ndtf pas de temps réduits > on garde dt
         idt = idt+1 ; dt = dtold
      elseif (idt == ndtf) then
         !on a fait ndtf pas de temps réduits > on augmente dt
         idt = 1 ; dt = dtold*real(ndtf,kind=long)
      else
         write(message,*) ' >>>> Erreur 106 dans CALCUL_DT : ',idt,ndtf
         write(error_unit,'(a)') trim(message) ; write(l9,'(a)') trim(message)
         write(message,*) ' Merci d''envoyer un rapport de bug'
         write(error_unit,'(a)') trim(message) ; write(l9,'(a)') trim(message)
         stop 106
      endif
   else
      !---Divergence des itérations
      idt = 1 ; dt = dt/real(ndtf,kind=long)
   endif
   !write(l9,*) ' Calcul du pas de temps avant ajustement : ',idt, dt, t, convergence_OK, ndtf

   !---Ajustement du pas de temps
   dt = min(dt,dtbase)  !ajustement en fonction de DTBASE
   if (convergence_OK .and. crmax>zero .and. cr>zero) then
      !Si convergence alors ajustement en fonction du nombre de Courant et ré-initialisation de Cr
      !CRmax est soit CRnormal soit CRtor
      !CRmax < 0 signifie pas de limite supérieure et donc pas d'ajustement de dt en fonction de CR
      dt = min(crmax/cr,dt)
      cr = zero
   endif
   dt = min(dtr,dt,dtlat)  !ajustement en fonction de la régulation locale
   !write(l9,*) ' Calcul du pas de temps après ajustement : ',idt, dt, t, dtr, dtlat, crmax

   !---Arrondis et Vérification DT > DTMIN
   if (dt >= dtmin ) then
      dt = dtmin*aint(dt/dtmin,kind=long)
   elseif (dtold>dtmin) then
      dt = dtmin
   else
      !pas la peine de continuer
      return
   endif
   !---Ajustement de dt en tenant compte des écritures sur .TRA et .BIN
   if (dtbin < 0.00001_long) tbin=t
   if (zegal(t,ttra,un)) then
      ttra1 = ttra+dttra
   else
      ttra1 = ttra
   endif
!   if (zegal(t,tcsv,un)) then
!      tcsv1 = tcsv+dtcsv
!   else
!      tcsv1 = tcsv
!   endif
   if (zegal(t,tbin,un)) then
      if (dtbin > zero) then
         tbin1 = tbin+dtbin
       else
         tbin1 = 1.e+30_long
      endif
   else
      if (dtbin > zero) then
         tbin1 = tbin
      else
         tbin1 = 1.e+30_long
      endif
   endif
   if (zegal(t,tmelissa,un)) then
      if (dtmelissa > zero) then
         tmelissa1 = tmelissa+dtmelissa
       else
         tmelissa1 = 1.e+30_long
      endif
   else
      if (dtmelissa > zero) then
         tmelissa1 = tmelissa
      else
         tmelissa1 = 1.e+30_long
      endif
   endif
   !adt = min(ttra1,tbin1,tcsv1)-t
   if (with_melissa) then
      adt = min(ttra1,tbin1,tmelissa1)-t
   else
      adt = min(ttra1,tbin1)-t
   endif
   dtant = dt
   if (adt < dt) then
      dt = adt
      ecrit = .true.
      if (dt < dtmin) dtmin0 = dt
   else
      ecrit = .false.
   endif
   !write(l9,*) ' Calcul du pas de temps après arrondi : ',idt, dt, t

   if (t>ttra .or. t>tbin .or. zegal(dt,zero,dtmin)) then
      write(lTra,'(a)') ' >>>> ERREUR dans le calcul de dt <<<<'
      write(lTra,*) ' dtmin = ',dtmin
      write(lTra,*) ' dtold = ',dtold
      write(lTra,*) ' dt = ',dt
      write(lTra,*) ' adt = ',adt
      write(lTra,*) ' dtr = ',dtr
      write(lTra,*) ' t    = ',t
      write(lTra,*) ' tbin = ',tbin
      write(lTra,*) ' ttra = ',ttra
      write(lTra,*) ' tbin1 = ',tbin1
      write(lTra,*) ' ttra1 = ',ttra1
      write(lTra,*) ' dtbin = ',dtbin
      write(lTra,*) ' dttra = ',dttra
      write(lTra,*) ' dtlat = ',dtlat
      write(lTra,*) ' crmax = ',crmax
      write(lTra,*) ' dtcsv = ',dtcsv
      write(error_unit,'(a)') ' >>>> ERREUR dans le calcul de dt <<<<'
      write(error_unit,'(a)') ' >>>> Voir le fichier TRA (Mage complet) <<<<'
      write(error_unit,'(a)') ' >>>> Merci d''envoyer un rapport de bug <<<<'
      !call do_Crash('calcul_dt')
      stop 181
   endif

   !---Modification éventuelle de DT pour tomber juste sur TMAX
   if (t<tmax .and. t+dt>=tmax) then
      dt = tmax-t
   else if (t>tmax) then
      t = t-dt ; dt = tmax-t ; t = tmax
   else if (t+dt > tmax-dtmin .and. t+dt < tmax) then
      dt = tmax-t
   end if
end subroutine calcul_dt



subroutine actualize_Q_Z
!==============================================================================
!                         Actualisation de qt et zt
!
! Si bilan en volume correct et si BOOL est faux (convergence des itérations)
! Opérateur de diffusion si coeff c_lissage > 0
!==============================================================================
   use Parametres, only: long, un
   use hydraulique, only: qt, zt, yt
   use data_num_fixes, only: c_lissage
   use solution, only: dq, dz, dzn, qn, zn, dqa, dza, dzna, qna, zna
   use TopoGeometrie, only: la_topo, xgeo, zfd
   implicit none
   ! -- variables locales temporaires --
   integer :: ib, is
   integer :: isa, isz, isa1, isz1, isp, ism
   real(kind=long) :: dxp, dxm
   integer, pointer :: ismax, nomax

   ismax => la_topo%net%ns

   dqa(1:ismax) = dq(1:ismax)
   dza(1:ismax) = dz(1:ismax)

   qt(1:ismax) = qt(1:ismax)+dqa(1:ismax)
   zt(1:ismax) = zt(1:ismax)+dza(1:ismax)
   !yt(1:ismax) = yt(1:ismax)+dza(1:ismax)
   yt(1:ismax) = zt(1:ismax)-la_topo%sections(1:ismax)%zf

   if (c_lissage > 1.e-05_long) then
      !---Lissage bief par bief pour simuler les chocs faibles (pseudo-viscosité)
      do ib = 1,la_topo%net%nb
         isa = la_topo%biefs(ib)%is1 ; isz = la_topo%biefs(ib)%is2
         isa1 = isa+1 ; do while(la_topo%sections(isa1)%iss /= 0)   ; isa1 = isa1+1 ; enddo
         isz1 = isz-1 ; do while(la_topo%sections(isz1+1)%iss /= 0) ; isz1 = isz1-1 ; enddo
         do is = isa1,isz1
            ism = is-1 ; do while(la_topo%sections(ism+1)%iss /= 0) ; ism = ism-1 ; enddo
            isp = is+1 ; do while(la_topo%sections(isp)%iss /= 0)   ; isp = isp+1 ; enddo
            dxp = abs(xgeo(isp)-xgeo(is))
            dxm = abs(xgeo(is)-xgeo(ism))
            qt(is) = (un-c_lissage)*qt(is)+c_lissage*(dxm*qt(isp)+dxp*qt(ism))/(dxm+dxp)
            zt(is) = (un-c_lissage)*zt(is)+c_lissage*(dxm*zt(isp)+dxp*zt(ism))/(dxm+dxp)
            yt(is) = zt(is)-zfd(is)
         enddo
      enddo
   endif

   nomax => la_topo%net%nn
   dzna(1:nomax) = dzn(1:nomax)
   qna(1:nomax)  = qn(1:nomax)
   zna(1:nomax)  = zn(1:nomax)
end subroutine actualize_Q_Z


! FIXME: doublon partiel entre actualize_QD() et actu_Qlat_Casier()
subroutine actualize_QD
!==============================================================================
!     Actualisation des débits et volumes perdus par déversement latéral
!
! QDS0(IS) = débit déversé à T-DT
! QDS1(IS) = débit déversé à T
! QDB0(IB) = Volume déversé à T-DT
! QDB1(IB) = Volume déversé à T
!==============================================================================
   use Parametres, only: long,Zero,ToutPetit, Total, RivG, RIvD
   use data_num_mobiles, only: dt
   use casiers, only: nclat, qds1, qdb0, qdb1, qclat, vd, devlat
   use TopoGeometrie, only: la_topo, xgeo
   implicit none
   ! -- variables locales --
   integer :: ib,is,neu,k
   integer :: TypDev !indice de boucle sur le type de deversement
                     !ici ça ne concerne que les rives droite et gauche
   integer, pointer :: ismax
   !------------------------------------------------------------------------------
   !--NB-->ici le calcul entre T-DT et T est valide (conservation des volumes
   !       et régulation
   !       SAUF pour la discrétisation à T (M12) qui permet d'éliminer QDS0
   !--->sauvegarde du débit déversé par bief : on pourra récupérer QDB0 en intégrant QDS0
   do ib = 1, la_topo%net%nb
      do k = total, rivd
         qdb0(k,ib) = qdb1(k,ib)
      enddo
   enddo

   !--->Actualisation de QD et de VD
   do ib = 1,la_topo%net%nb
      do k = total, rivd ; qdb1(k,ib) = zero ; enddo
      do is = la_topo%biefs(ib)%is1, la_topo%biefs(ib)%is2-1
         qdb1(Rivg,ib) = qdb1(rivg,ib) + qds1(rivg,is)*abs(xgeo(is)-xgeo(is+1))
         qdb1(Rivd,ib) = qdb1(rivd,ib) + qds1(rivd,is)*abs(xgeo(is)-xgeo(is+1))
      enddo
      qdb1(total,ib) = qdb1(rivg,ib)+qdb1(rivd,ib)
      vd(rivg,ib)    = vd(rivg,ib)+qdb1(rivg,ib)*dt
      vd(rivd,ib)    = vd(rivd,ib)+qdb1(rivd,ib)*dt
      vd(total,ib)   = vd(rivg,ib)+vd(rivd,ib)
   enddo
   !--->détection de la présence d'un déversement latéral
   ismax => la_topo%net%ns
   devlat(rivg) = maxval(abs(qds1(RivG,1:ismax))) > ToutPetit
   devlat(rivd) = maxval(abs(qds1(RivD,1:ismax))) > ToutPetit
   !--->Actualisation des débits ou volumes déversés par casier
   qclat(1:la_topo%net%nn) = zero
   do TypDev = RivG, RivD
      if (devlat(TypDev)) then
         do ib = 1, la_topo%net%nb
            do is = la_topo%biefs(ib)%is1, la_topo%biefs(ib)%is2-1
               neu = nclat(typdev,is)
               if (neu > 0) then           !debit déversé vers le nœud NEU
                  qclat(neu) = qclat(neu) + qds1(typdev,is)*abs(xgeo(is+1)-xgeo(is))
               elseif (neu == 0) then      !volume déversé vers l'exterieur
                  qclat(neu) = qclat(neu) + dt*qds1(typdev,is)*abs(xgeo(is+1)-xgeo(is))
               else
                  continue
               endif
            enddo
         enddo
      endif
   enddo
end subroutine actualize_QD



subroutine bilan_volume(APL,BBilan)
!==============================================================================
!                   calcul du bilan relatif en volume
! BBILAN est vrai si la conservation des volumes est satisfaite
! APL : 0 si l'appel vient de timestep_last_actions()
!       1 si l'appel vient de time_iterations_*() après la boucle temporelle
!==============================================================================
   use Parametres, only: long, zero, lErr, lTra, l9

   use mage_utilitaires, only: heure
   use IO_Files, only: errFile
   use booleens, only: donotComputeVolumeBalance
   use data_num_fixes, only: err_volume_maxAllowed, plus, tmax, date_format
   use data_num_mobiles, only: ib, t, dt
   use erreurs_stv, only: errmax, errsup, kbmax, kbsup, t_errmax
   use TopoGeometrie, only: la_topo
   implicit none
   ! -- Prototype --
   logical,intent(out) :: BBilan
   real(kind=Long),intent(in) :: APL
   ! -- Variables locales temporaires --
   character(len=19) :: hms
   real(kind=long) :: t0

   bbilan = .true.
   if (donotComputeVolumeBalance) then
      return
   elseif (apl > zero) then
      !appel après la sortie des itérations en temps
      hms = heure(t_errmax)
      if (date_format) then
         write(lTra,1003) errsup,kbsup, errmax,kbmax,trim(hms)
      else
         write(lTra,1001) errsup,kbsup, errmax,kbmax,trim(hms)
      endif
      return
   else
      !ici on est encore dans la boucle temporelle
      errsup = zero ; kbsup = 1
      do ib = 1, la_topo%net%nb !parcours dans l'ordre des données
         call bilan_bief(ib)
      enddo
      bbilan = abs(errsup) < err_volume_maxAllowed
      if (.not.bbilan) then
         write(l9,'(a)') ' >>>> Conservation des volumes non satisfaite : divergence <<<<'
         write(l9,1000) plus, errsup, kbsup, errmax, kbmax
         if (errFile /= '') then
            t0 = t
            if (t < tmax) t0 = t0-dt !si t=tmax c'est le dernier appel à bilan_volume après la mise à jour des variables Q et Z
            hms = heure(t0)
            write(lErr,1002) hms, errsup, kbsup, errmax, kbmax
         endif
      endif
   endif
   1000 format(a,'Err. volume :  ',e8.2,' (',i3.3,') ','MAX :  ',e8.2,' (',i3.3,')')
   1001 format(' Erreur (relative par bief) en volume à l''instant final :  ',       &
               e8.2,' (Bief ',i3.3,')',/,' Erreur maximale sur la simulation :  ',   &
               e8.2,' (Bief ',i3.3,' Date : ',a,')')
   1002 format(' Date : ',a,' > Err. Vol.: ',e8.2,' (',i3.3,') ','MAX : ',e8.2,' (',i3.3,')')
   1003 format(' Erreur (relative par bief) en volume à l''instant final :  ',       &
               e8.2,' (Bief ',i3.3,')',/,' Erreur max sur la simulation :  ',   &
               e8.2,' (Bief ',i3.3,' Date : ',a,')')
end subroutine bilan_volume



subroutine bilan_bief(ib)
!==============================================================================
!                  Calcul du bilan relatif en volume
!==============================================================================
   use Parametres, only: long, RG2=>RacG2, Total, zero

   use hydraulique, only: qt,yt,qe,dqe
   use Ouvrages, only: all_OuvCmp, all_OuvEle
   use data_num_mobiles, only: dt,dx,t
   use data_num_fixes, only: vbmin
   use solution, only: dq, dz
   use erreurs_stv, only: errvol,errmax,errsup,ermax0,kbmax,kbsup,t_errmax
   use casiers, only: qds0, qds1
   use objet_section, only: profil
   use TopoGeometrie, only: la_topo, xgeo
   implicit none
   ! -- prototype --
   integer,intent(in) :: ib !numéro du bief dans l'ordre des données
   ! -- Variables locales temporaires --
   integer :: nk, ns, isi, isj, isa, isz
   real(kind=Long) :: vb,dvb,dqb,dqdev,qdij,dqij,tmp,sti,dsti,stj,dstj
   type(profil), pointer :: prfl

   vb = zero  ;  dvb = zero
   isa = la_topo%biefs(ib)%is1 ; isz = la_topo%biefs(ib)%is2
   dqb = dt*( qt(isa)-qt(isz) + 0.5_long*(dq(isa)-dq(isz)) )  !bilan entrée/sortie en volume
   !---Initialisation de STI et DSTI
   prfl => la_topo%sections(isa)
   sti  = prfl%surface_mouillee_tirant(yt(isa)+dz(isa))
   dsti = sti-prfl%surface_mouillee_tirant(yt(isa))
   !---Boucle sur les sections du bief IB
   do isj = isa+1, isz
      isi = isj-1
      prfl => la_topo%sections(isj)
      stj  = prfl%surface_mouillee_tirant(yt(isj)+dz(isj))
      dstj = stj-prfl%surface_mouillee_tirant(yt(isj))
      !---Cas d'une section singuliere
      ns = la_topo%sections(isj)%iss
      if ( ns/=0 ) then
         nk = all_OuvCmp(ns)%OuEl(1,1)
         if (all_OuvEle(nk)%iuv == 3 .or. all_OuvEle(nk)%iuv == 5) then
            !---Contribution des pompes et des déversoirs latéraux au bilan entrée/sortie
            dqij = (qt(isi)-qt(isj))+0.5_long*(dq(isi)-dq(isj))
            dqb = dqb-dt*dqij
         endif
      else
         dx = abs(xgeo(isj)-xgeo(isi))
         dvb = dvb+0.5_long*dx*(dsti+dstj)             !variation du volume stocké
         vb = vb+0.5_long*dx*(sti+stj)                 !volume stocké à la fin du pas de temps [T-DT ; T]
         dqdev = qds1(total,isi)-qds0(total,isi)  !Contribution des déversements latéraux entre t et t+dt
         !---Fin du calcul de DVB : prise en compte des Apports/fuites
         qdij = qe(isi)-qds0(total,isi) + 0.5_long*(dqe(isi)-dqdev)
         dvb = dvb-dt*dx*qdij
      end if
      !---Passage à la section suivante
      sti  = stj
      dsti = dstj
   enddo
   !---Erreur en volume et recherche du max de l'erreur
   if (vb > vbmin) then
      errvol(ib) = (dqb - dvb)/vb
   else
      errvol(ib) = zero
   endif
   if ( abs(errsup)<abs(errvol(ib)) ) then
      errsup = errvol(ib)
      kbsup = ib
   endif
   tmp = abs(errvol(ib))
   !if ( abs(errmax)<tmp .and. tmp<err_volume_maxAllowed ) then
   if ( abs(errmax)<tmp) then
      ermax0 = errmax
      errmax = errvol(ib)
      kbmax = ib
      t_errmax = t
   endif
end subroutine bilan_bief



subroutine ajust_DT(Bool,DTR)
!==============================================================================
!  Ajustement du pas de temps en fonction des pompes, déversoirs latéraux
!
!--->BOOL = .TRUE. : il faut revenir en arrière et repartir avec un pas de
!                    temps réduit < DTR
!==============================================================================
   use Parametres, only: long, l9
   use data_num_fixes, only: dtmin
   implicit none
   ! -- Prototype --
   logical,intent(out) :: Bool
   real(kind=Long),intent(out) :: DTR
   ! -- Appels --
   interface
      subroutine Devers(DTR,Bool)
         use Parametres, only: Long
         real(kind=Long), intent(out) :: DTR
         logical,intent(out) :: Bool
      end subroutine Devers
   end interface
   ! -- Variables locales temporaires --
   logical :: bool0, bool1
   real(kind=long) :: dtp

   bool = .false.
   !---Estimation du pas de temps DTR, compatible avec le volume disponible pour
   !   un déversement latéral
   call devers(dtr,bool0)
   !---Régulation des pompes : renvoie le pas de temps DTP adapté aux pompes
   call ajust_DT_pompes(bool1,dtp)
   !---pas de temps de régulation au moins égal au DTMIN donné par l'utilisateur
   dtr = max(dtmin,min(dtr,dtp))
   bool = bool0 .or. bool1
   if (bool) write(l9,'(2x,a)')'>>>> RETOUR EN ARRIERE POUR LA REGULATION (ouvrages ou casiers) <<<<'
end subroutine ajust_DT



subroutine ajust_DT_pompes(Bool,DTP)
!==============================================================================
!                Examen de la situation s'il y a une pompe
!
!    Si FLAG = 0 il n'y a pas de pompe
!    BOOL = .TRUE. si l'on démarre ou arrête une pompe et on réduit dt
!    BOOL = .FALSE. sinon et si DT a déjà été réduit pour non convergence
!==============================================================================
   use Parametres, only: long,Zero,Un

   use Mage_Utilitaires, only: zegal
   use data_num_fixes, only: dtbase, flag
   use data_num_mobiles, only: t
   use hydraulique, only: zt
   use Ouvrages, only: all_OuvCmp, all_OuvEle
   use solution, only: dz,  & ! solution actuelle
                       dza   ! solution du pas de temps precedent
   use TopoGeometrie, only: la_topo
   implicit none
   ! -- Prototype --
   logical,intent(out) :: bool
   real(kind=Long),intent(out) :: DTP
   ! -- Variables locales temporaires --
   integer :: nk, ns, k, is, js
   real(kind=long) :: td, tdd, ta, taa, zsup, zinf, zstop, ztyet , ztold, ztnew, dtnk

   !--->Initialisation
   bool = .false.  ;  dtp = dtbase
   if (zegal(flag,zero,un)) return
   !--->Boucle sur les singularités : recherche des pompes
   do ns = 1,la_topo%net%nss
      do k = 1, all_OuvCmp(ns)%ne
         nk = all_OuvCmp(ns)%OuEl(k,1)
         if (all_OuvEle(nk)%iuv /= 3) exit    ! l'ouvrage n'est pas une pompe
         td = all_OuvEle(nk)%uv2 ; tdd = td+all_OuvEle(nk)%uv4 ! date démarrage et date plein régime
         ta = all_OuvEle(nk)%uv3 ; taa = ta+all_OuvEle(nk)%uv5 ! date d'arrêt et date arrêt complet

         zsup = all_OuvEle(nk)%za1    ! cote de mise en route de la pompe
         zinf = all_OuvEle(nk)%za2    ! cote d'arrêt de la pompe
         zstop = all_OuvEle(nk)%za3+all_OuvCmp(ns)%zfs ! cote de désammorcage de la pompe
         is = all_OuvCmp(ns)%js   ! section contenant la pompe
         js = all_OuvEle(nk)%irf    ! section où est l'asservissement de la pompe
         ztyet = zt(js)
         ztold = ztyet-dza(js)
         ztnew = ztyet+dz(js)
         if (t<=tdd .and. t>=td) then
            !la pompe est en train de monter en puissance
            !DTNK = pas de temps proposé pour cette pompe
            dtnk = all_OuvEle(nk)%uv4/3._long
         else if (t<=tdd+un .and. t>tdd) then
            dtnk = dtbase
         else if (t>=ta .and. t<=taa) then
            !la pompe est en train de s'arrêter
            dtnk = all_OuvEle(nk)%uv5/3._long
         else if (t>tdd .and. t<ta) then
            !la pompe fonctionne à pleine puissance : modif. du fonctionnement possible
            if ( ztnew < zinf .or. zt(is)+dz(is) < zstop ) then
               !la pompe aurait due être arrêtée : recherche de l'instant de dépassement
               !                                   de zinf ou zstop
               call pompe_stop(is,nk,dtnk,bool)
            else
               !la pompe doit continuer à fonctionner
               dtnk = dtbase
            end if
         else if (t<td .or. t>taa) then
            !la pompe ne fonctionne pas encore ou plus : modif. du fonctionnement possib
            if (ztnew>zsup .and. zt(is)+dz(is)>zstop) then
               !la pompe aurait due être mise en marche : recherche de l'instant de
               !                                          dépassement de zsup
               call pompe_start(nk,dtnk,bool)
            else
               !la pompe reste arrêtée
               dtnk = dtbase
            end if
         end if
         dtp = min(dtp,dtnk)
      enddo
      !--->on arrive ici si on veut interdire le démarrage simultané de
      !    plusieurs pompes situées dans la même section singulière
   enddo
   if (dtp < dtbase) then
      !il y a des pompes en transition de mode de fonctionnement (dt reduit)
      flag = -1._long
   endif
end subroutine ajust_DT_pompes



subroutine pompe_stop(IS,NK,DTNK,BOOL)
!==============================================================================
!             Arrêt de la pompe
!==============================================================================
   use Parametres, only: long, l9
   use data_num_fixes, only: dtbase
   use data_num_mobiles, only: t, dt
   use hydraulique, only: zt
   use Ouvrages, only: all_OuvEle
   use solution, only: dz
   use TopoGeometrie, only: zfd
   implicit none
   ! -- Prototype --
   integer,intent(in) :: IS
   integer,intent(in) :: NK
   real(kind=Long),intent(inout) :: DTNK
   logical,intent(inout) :: Bool
   ! -- Constantes --
   real(kind=long), parameter :: dtmin=30._long
   ! -- Variables locales temporaires --
   integer :: js
   real(kind=long) :: tyet, tnew, zinf, zstop, tprevu


   write(l9,*) 'Arrêt de la pompe ',all_OuvEle(nk)%cvar
   tyet = t-dt
   tnew = t-dtmin
   !---ZINF = cote d'arrêt de la pompe
   zinf = all_OuvEle(nk)%za2
   !---ZSTOP = cote de désammorcage de la pompe
   zstop = all_OuvEle(nk)%za3+zfd(is)
   !---JS = section de régulation (IS = section où est située la pompe)
   js = all_OuvEle(nk)%irf
   !---Estimation (linéaire) de l'instant de dépassement de ZINF ou de ZSTOP
   !   en partant de l'hypothèse qu'au pas de temps précédent elles n'étaient
   !   pas dépassées. Cette hypothèse peut être fausse si on force l'arrêt de
   !   la pompe en modifiant les cotes d'arrêt ou de désamorcage.
   if (zt(js)+dz(js) < zinf) then
      tprevu = tyet+(zinf-zt(js))/dz(js)*dt
   else
      tprevu = 1.e+30_long
   endif
   if (zt(is)+dz(is) < zstop) then
      tprevu = min(tprevu,tyet+(zstop-zt(is))/dz(is)*dt)
   endif
   if (zstop < zt(is) .or. zinf < zt(js) .or. tprevu < tyet) then
      !---cas où on a modifié les cotes d'arrêt ou de désamorçage
      !   on arrête la pompe immédiatement sans revenir en arrière
      all_OuvEle(nk)%uv3 = t-0.001_long
      if (all_OuvEle(nk)%uv5 > 3._long) then
         dtnk = max(all_OuvEle(nk)%uv5/3._long,dtmin)
       else
         dtnk = dtbase
      endif
   else if (tprevu < tnew) then
      !---on aurait du arrêter la pompe : il faut revenir en arrière
      all_OuvEle(nk)%uv3 = tprevu-0.001_long
      dtnk = tprevu-tyet
      bool = .true.
   else
      !---on arrête la pompe immédiatement sans revenir en arrière
      all_OuvEle(nk)%uv3 = t-0.001_long
      if (all_OuvEle(nk)%uv5>3._long) then
         dtnk = max(all_OuvEle(nk)%uv5/3._long,dtmin)
      else
         dtnk = dtbase
      endif
   end if
   !---on met l'instant de démarrage à -infini
   all_OuvEle(nk)%uv2 = -1.e+30_long
end subroutine pompe_stop



subroutine pompe_start(NK,DTNK,BOOL)
!==============================================================================
!             Démarrage de la pompe
!==============================================================================
   use Parametres, only: long, l9
   use data_num_fixes, only: dtbase
   use data_num_mobiles, only: t, dt
   use hydraulique, only: zt
   use Ouvrages, only: all_OuvEle
   use solution, only: dz
   implicit none
   ! -- Prototype --
   integer,intent(in) :: NK
   real(kind=Long),intent(inout) :: DTNK
   logical,intent(inout) :: bool
   ! -- Constantes --
   real(kind=long), parameter :: dtmin=30._long
   ! -- Variables locales temporaires --
   integer :: js
   real(kind=long) :: tyet, tnew, zsup, tprevu


   write(l9,*) 'Démarrage de la pompe ',all_OuvEle(nk)%cvar
   tyet = t-dt
   tnew = t-dtmin
   !---ZSUP = cote de mise en route de la pompe
   zsup = all_OuvEle(nk)%za1
   !---JS = section de régulation
   js = all_OuvEle(nk)%irf
   !---Estimation (linéaire) de l'instant de dépassement de ZSUP
   !   en partant de l'hypothèse qu'au pas de temps précédent elle n'était
   !   pas dépassée. Cette hypothèse peut être fausse si on force le démarrage
   !   de la pompe en modifiant sa cote de mise en route
   tprevu = tyet+(zsup-zt(js))/dz(js)*dt
   if (zt(js)>zsup .or. tprevu < tyet) then
      !---cas où on a modifié les cotes d'arrêt ou de désamorçage
      !   on démarre la pompe immédiatement sans revenir en arrière
      all_OuvEle(nk)%uv2 = t-0.001_long
      if (all_OuvEle(nk)%uv4>3._long) then
         dtnk = max(all_OuvEle(nk)%uv4/3._long,dtmin)
      else
         dtnk = dtbase
      endif
   else if (tprevu < tnew) then
      !---on aurait dû démarrer la pompe : il faut revenir en arrière
      all_OuvEle(nk)%uv2 = tprevu-0.001_long
      dtnk = tprevu-tyet
      bool = .true.
   else
      !---on démarre la pompe immédiatement sans revenir en arrière
      all_OuvEle(nk)%uv2 = t-0.001_long
      if (all_OuvEle(nk)%uv4>3._long) then
         dtnk = max(all_OuvEle(nk)%uv4/3._long,dtmin)
      else
         dtnk = dtbase
      endif
   endif
   !---on met le prochain instant d'arrêt à +infini
   all_OuvEle(nk)%uv3 = 1.e+30_long
end subroutine pompe_start



subroutine save_QD
!==============================================================================
!        sauvegarde des débits perdus par déversement latéral
!==============================================================================
   use parametres, only: long, total, rivg, rivd, toutpetit
   use casiers, only: devlat, qds0, qds1
   use TopoGeometrie, only: la_topo
   implicit none
   ! -- Variables locales temporaires --
   integer :: is
   integer, pointer :: ismax


   ismax => la_topo%net%ns
   !--->sauvegarde du débit déversé par section
   !    Il ne faut pas remettre QDS1 à 0 : estimation pour le pas de temps en cours
   qds0(total:rivd,1:ismax) = qds1(total:rivd,1:ismax)

   !--->détection de la présence d'un déversement latéral :
   !     on sort si on trouve un déversement sur chaque rive
   devlat(rivg:rivd) = .false.
   do is = 1,ismax
      if (abs(qds1(RivG,is)) > ToutPetit) then
         devlat(RivG) = .true.
      endif
      if (abs(qds1(RivD,is)) > ToutPetit) then
         devlat(RivD) = .true.
      endif
      if (devlat(RivG).and.devlat(RivD)) exit
   enddo
end subroutine save_QD



subroutine relire_NUM
!==============================================================================
!   Lecture du fichier de paramètres NUM clone de .NUM
!     ré-initialisation des paramètres numériques
!==============================================================================
   use parametres, only: long, l9
   use, intrinsic :: iso_fortran_env, only: output_unit

   use mage_utilitaires, only: heure, backup_file
   use Data_Num_Fixes, only: tmax
   use Data_Num_Mobiles, only: t
   implicit none
   ! -- Variables locales temporaires --
   logical :: bool
   integer :: lu14, ios
   character(len=30) :: filename
   character(len=16) :: commande
   character(len=19) :: hms

   !Arrêt anticipé si on trouve un fichier stop
   inquire(file='stop',exist=bool)
   if (bool) then  !arrêt anticipé
      open(newunit=lu14,file='stop',status='unknown')
      read(lu14,'(a)',iostat=ios) commande
      if (ios == 0) then
         if (trim(commande) == 'stop next minute') then
            tmax = real(int(t/60._long)+1,kind=long)*60._long
         else if (trim(commande) == 'stop next hour') then
            tmax = real(int(t/3600._long)+1,kind=long)*3600._long
         else if (trim(commande) == 'stop next day') then
            tmax = real(int(t/86400._long)+1,kind=long)*86400._long
         else
            tmax = t
         endif
      else
         tmax = t
      endif
      close(lu14,status='delete')  !suppression du fichier 'stop'
      hms = heure(tmax)
      write(output_unit,*) ' >>> interruption par l''utilisateur à ',trim(hms),' !!!'
      write(l9,*) ' >>> interruption par l''utilisateur !!! ',trim(hms),' !!!'
      return
   endif

   !---fichier .num
   inquire(file='NUM',exist=bool,iostat=ios)
   if (ios > 0) then
      return
   else if (bool) then
      filename = 'NUM'  ;  call backup_file(filename)
      write(l9,'(a)') '---> Modification des paramètres numériques (lecture d''un fichier NUM'
      call lire_NUM(filename,.true.)
      ! réouverture du fichier pour pouvoir le détruire
      open(newunit=lu14,file='NUM',form='formatted',status='old')
      close(lu14,status='delete')
   else
      return
   endif

   !---fichier .par
   inquire(file='PAR',exist=bool,iostat=ios)
   if (ios > 0) then
      return
   else if (bool) then
      filename = 'PAR'  ;  call backup_file(filename)
      write(l9,'(a)') '---> Modification des paramètres numériques (lecture d''un fichier PAR'
      call lire_PAR(filename,.true.)
      ! réouverture du fichier pour pouvoir le détruire
      open(newunit=lu14,file='PAR',form='formatted',status='old')
      close(lu14,status='delete')
   else
      return
   endif
end subroutine relire_NUM



subroutine relire_SIN_VAR(mod92)
!==============================================================================
!                 Lecture des fichiers SIN et VAR
!==============================================================================
   use, intrinsic :: iso_fortran_env, only: output_unit

   use IO_Files, only: varFile
   use mage_utilitaires, only: backup_file
   implicit none
   ! -- Prototype --
   logical,intent(out) :: mod92
   ! -- Variables locales temporaires --
   logical :: bool
   integer :: lu6, lu11, nbele
   character :: filename*30

   !---fichier SIN
   inquire(file='SIN',exist=bool)
   if (bool) then
      filename = 'SIN'  ;  call backup_file(filename)
      call lire_sin(filename,nbele)
      !suppression de SIN après lecture ; on boucle tant que SIN n'a pas disparu
      do
         inquire(file='SIN',exist=bool)
         if (bool) then
            open(newunit=lu6,file='SIN',form='formatted',status='old')
            close(lu6,status='delete')  !fermeture avec destruction
            cycle
         else
            exit
         endif
      enddo
      !---relecture du fichier etude.VAR initial s'il n'y a pas de clone VAR en attente
      inquire(file='VAR',exist=bool)
      if (varFile /= '' .and. .not. bool) then
         write(output_unit,'(2a)') ' lecture de ',trim(varFile)
         call Lire_VAR(varFile)
      endif
   endif

   !---fichier VAR
   if (varFile == '') return  !il n'y a pas de fichier [.VAR] déclaré dans REP
   inquire(file='VAR',exist=bool)
   if (.not.bool) then        !il n'y a pas de fichier VAR
      mod92 = .false.  ;  return
   endif
   filename = 'VAR'  ;  call backup_file(filename)
   mod92 = .true.
   call Lire_VAR(filename)
   do
      inquire(file='VAR',exist=bool)
      if (bool) then
         open(newunit=lu11,file='VAR',form='formatted',status='old')
         close(lu11,status='delete')  !fermeture avec destruction
         cycle                        !on attend que VAR ait bien disparu
      else
         exit
      endif
   enddo
end subroutine relire_SIN_VAR



subroutine cotes_noeuds
!==============================================================================
!            Calcul des cotes moyennes aux nœuds
!==============================================================================
   use Parametres, only: Long, zero
   use hydraulique, only: qt, zt
   use solution, only: dza, dzna, qna, zna
   use TopoGeometrie, only: la_topo
   implicit none
   ! -- Variables locales temporaires --
   integer :: n, ib, nm, nv, is1, is2
   integer, pointer :: nomax

   !---initialisation
   nomax => la_topo%net%nn
   dzna(1:nomax) = zero ; qna(1:nomax) = zero ; zna(1:nomax) = zero
   !---calcul de la somme des dz
   do ib = 1,la_topo%net%nb
      nm = la_topo%biefs(ib)%nam ; nv = la_topo%biefs(ib)%nav
      is1 = la_topo%biefs(ib)%is1 ; is2 = la_topo%biefs(ib)%is2
      dzna(nm) = dzna(nm)+dza(is1)
      dzna(nv) = dzna(nv)+dza(is2)
      qna(nm)  = qna(nm)-qt(is1)
      qna(nv)  = qna(nv)+qt(is2)
      zna(nm)  = zna(nm)+zt(is1)
      zna(nv)  = zna(nv)+zt(is2)
   enddo
   !---calcul des moyennes
   do n = 1,nomax
      dzna(n) = dzna(n)/real(la_topo%net%nzno(n),kind=long)
      zna(n)  =  zna(n)/real(la_topo%net%nzno(n),kind=long)
   enddo
end subroutine cotes_noeuds



subroutine init_CLaval_Debord
!==============================================================================
!          Initialisation des C.L. aval dans le cas Debord
!==============================================================================
   use Parametres, only: long, zero, un

   use conditions_limites, only: rv=>rmv,sv=>smv,tv=>tmv, cl
   use data_num_mobiles, only: t, dt, area_factor
   use hydraulique, only: qt, zt
   use TopoGeometrie, only: la_topo
   implicit none
   ! -- Variables locales temporaires --
   integer :: ib, isn, n
   real(kind=long) :: temp1, temp2


   !pour convertir les aires des nœuds à surface non-nulle
   area_factor = un/dt !10000._long/dt

   !---initialisation des c.l. aval selon leur type
   do n = 1,la_topo%net%nn
      if (la_topo%nodes(n)%cl >= 0) cycle
      ib = la_topo%net%numero(la_topo%net%lbamv(n))
      isn = la_topo%biefs(ib)%is2
      if (la_topo%nodes(n)%cl /= -3) then
         !--->loi Q(Z) ou cote critique
         rv(n) = un
         temp1 = zt(isn)+0.001_long ; temp2 = zt(isn)-0.001_long
         sv(n) = -(cl(n,temp1)-cl(n,temp2)) / 0.002_long
         tv(n) = cl(n,zt(isn))-qt(isn)
      else
         !--->loi Z(T)
         if (cl(n,t) > la_topo%sections(isn)%zf) then
            rv(n) = zero ; sv(n) = un
            tv(n) = cl(n,t)-zt(isn)
         else
            call err012(isn)
         endif
      end if
   enddo
end subroutine init_claval_Debord



subroutine init_solution_Debord
!==============================================================================
!          Initialisation de la solution dans le cas Debord
!==============================================================================
   use Parametres, only: long, zero,cent,un
   use data_num_mobiles, only: dt, area_factor
   use hydraulique, only: qt, zt
   use solution, only: qna,zna, dq,dz
   use PremierAppel, only: nap => nap_m142
   use TopoGeometrie, only: la_topo
   implicit none
   ! -- Variables locales temporaires --
   integer :: ib, na, nz
   integer, pointer :: ismax, ibmax, nomax


   nap = nap+1
   ismax => la_topo%net%ns
   ibmax => la_topo%net%nb
   nomax => la_topo%net%nn

   !pour convertir les aires des nœuds à surface non-nulle
   area_factor = un/dt !10000._long/dt

   !---initialisation de la solution : mise a 0
   dq(1:ismax) = zero ; dz(1:ismax) = zero

   !---initialisation de qna et zna : calcul des cotes moyennes et des bilans de
   !                                  débit aux nœuds
   !--->fait une seule fois (re-initialisation par cotes_noeuds())
   if (nap < 2) then
      do ib = 1,ibmax
         na = la_topo%biefs(ib)%nam ; nz = la_topo%biefs(ib)%nav
         zna(na) = zna(na)+zt(la_topo%biefs(ib)%is1)
         zna(nz) = zna(nz)+zt(la_topo%biefs(ib)%is2)
         qna(na) = qna(na)-qt(la_topo%biefs(ib)%is1)
         qna(nz) = qna(nz)+qt(la_topo%biefs(ib)%is2)
      enddo
      zna(1:nomax) = zna(1:nomax)/real(la_topo%net%nzno(1:nomax),kind=long)
   endif
end subroutine init_solution_Debord


subroutine verif_CotesNoeuds(Bool,Nom)
!==============================================================================
!      Vérification de l'égalité des cotes aux nœuds de maille
!         on ne fait rien si FMA < 0 ou si FMA > 100.
! Renvoie Bool = FAUX s'il y a au moins un nœud où l'écart des cotes dépasse FMA
!         Nom = est le nom du nœud où l'écart est maximal s'il dépasse FMA
!         Nom est vide ('   ') sinon
!==============================================================================
   use Parametres, only: Long, nosup, zero, lnode
   use data_num_fixes, only: fma
   use data_num_mobiles, only: t
   use hydraulique, only: zt
   use solution, only: dz
   use TopoGeometrie, only: la_topo
   implicit none
   ! -- Prototype --
   logical,intent(out) :: Bool
   character(len=lnode),intent(out) :: Nom
   ! -- Variables locales temporaires --
   real(kind=Long) :: znmin(nosup),znmax(nosup), zt1, zt2, erzmax
   integer :: ib, na, nz, n, nmax, is1, is2
   integer, pointer :: nomax, ibmax

   nomax => la_topo%net%nn
   ibmax => la_topo%net%nb

   if (fma<zero .or. fma>100._long) then
      bool = .true.  ;  nom = '   '
      return
   endif
   !---initialisation
   znmin(1:nomax) = 1.e+30_long ; znmax(1:nomax) = -1.e+30_long
   !---boucle sur les noeuds amont et aval de chaque bief
   do ib = 1,ibmax
      na = la_topo%biefs(ib)%nam
      is1 = la_topo%biefs(ib)%is1
      zt1 =  zt(is1)+dz(is1)
      znmax(na) = max( znmax(na) , zt1 )
      znmin(na) = min( znmin(na) , zt1 )

      nz = la_topo%biefs(ib)%nav
      is2 = la_topo%biefs(ib)%is2
      zt2 =  zt(is2)+dz(is2)
      znmax(nz) = max( znmax(nz) , zt2 )
      znmin(nz) = min( znmin(nz) , zt2 )
   enddo
   erzmax = znmax(1)-znmin(1)
   nmax = 1
   do n = 2,nomax !il y a au moins 2 nœuds
      if (znmax(n)-znmin(n) > erzmax) then
         erzmax = znmax(n)-znmin(n)
         nmax = n
      endif
   enddo
   bool = erzmax < fma
   if (.not.bool) then
      nom = la_topo%nodes(nmax)%name !nœud où l'erreur est maximale
      !recherche du premier nœud où il n'y a pas égalité des cotes
      do n = 1,nomax
         if (znmax(n)-znmin(n) > fma) call err011(t,la_topo%nodes(n)%name)
      enddo
   else
      nom = '   '
   endif
end subroutine verif_CotesNoeuds



subroutine verif_HauteurEau(bool,chaine)
!==============================================================================
!            Vérification tirant d'eau positif et pas trop grand
!
! bool   : logique (faux si problème)
! chaine : description du problème
!==============================================================================
   use parametres, only: long, zero, i_ism, i_mixte, i_debord
   use hydraulique, only: z_debord => zt
   use solution, only: dz_debord => dz
   use StVenant_ISM, only: z_ISM => z, dz_ISM => dz
   use data_num_fixes, only: yinf
   use data_num_mobiles, only: ISM_available
   use TopoGeometrie, only: la_topo, zfd
   implicit none
   ! -- prototype --
   logical,intent(out) :: bool
   character(len=10),intent(out) :: chaine
   ! -- variables locales temporaires --
   integer :: ityp, is
   real(kind=long) :: ynew, ymax
   real(kind=long),dimension(:),pointer :: zt, dz

   if (ISM_available()) then
      zt(1:) => z_ISM(1:)
      dz(1:) => dz_ISM(1:)
   else
      zt(1:) => z_Debord(1:)
      dz(1:) => dz_Debord(1:)
   endif

   ityp = 2  ! permet de tracer l'origine de l'appel des routines d'erreur
   if (yinf < 1.e-06_long) then
   !---exception traitée par message d'erreur de divergence
      do is = la_topo%net%ns, 1, -1
         ynew = zt(is)+dz(is)-zfd(is)
         if (ynew > zero) then
            continue
         else if (ynew <= zero) then
            call err025(is,ynew,ityp)
            chaine = 'Y NÉGATIF'  ;  bool = .false.  ;  return
         else
            !si ynew est NaN la comparaison avec zero est toujours fausse
            call err027(is,ityp)
            chaine = 'Y NaN !'  ;  bool = .false.  ;  return
         endif
         ymax = la_topo%sections(is)%ybmax+10._long
         if (ynew > ymax) then
            call war001(is,ynew,ymax,ityp)
            chaine = 'Y GÉANT !'  ;  bool = .false.  ;  return
         endif
      enddo
   else
   !------exception traitée par modification de la solution
      do is = la_topo%net%ns, 1, -1
         ynew = zt(is)+dz(is)-zfd(is)
         ymax = la_topo%sections(is)%ybmax+10._long
         if (.not.(dz(is)<=zero) .and. .not.(dz(is)>=zero)) then
            ! pas sûr que ce soit une bonne stratégie !?!?
            dz(is) = zero
         elseif (.not.(ynew<=zero) .and. .not.(ynew>=zero)) then
            !si ynew est NaN la comparaison avec zero est toujours fausse
            ! on ne peut pas corriger avec Yinf car on n'a pas d'indication sur la valeur du tirant d'eau
            ! les problèmes numériques peuvent se produire au moment du débordement, dans ce cas le tirant d'eau
            ! est loin d'être voisin de Yinf
            call err027(is,ityp)
            chaine = 'Y NaN ! '  ;  bool = .false.  ;  return
         elseif (ynew <= yinf) then
            dz(is) = yinf+zfd(is)-zt(is)
         elseif (ynew > ymax) then
            dz(is) = ymax+zfd(is)-zt(is)
         endif
      enddo
   endif
   bool = .true.
end subroutine verif_HauteurEau


function ISM_compatibility()
!  renvoie VRAI si, pour chaque bief, toutes les sections débordent ou aucune ne déborde
!  valeurs de retour :
! +1 : pas de débordement partiel, on peut avoir des biefs débordant partout et d'autres nulle part
!  0 : débordement partiel : il y a au moins un bief qui déborde partiellement
! -1 : pas de débordement du tout
   use parametres, only: long
   use hydraulique, only: z_debord => zt
   use solution, only: dz_debord => dz
   use StVenant_ISM, only: z_ISM => z, dz_ISM => dz
   use TopoGeometrie, only: la_topo, profil, bief
   use Data_Num_Mobiles, only: ISM_available
   implicit none
   ! -- prototype --
   integer :: ISM_compatibility
   ! -- variables locales temporaires --
   integer :: isi, li1, li2, ib, nb_section_deb, nb_bief_nodeb, nb_section_nodeb, nbsec
   real(kind=long) :: zdeb_full, zb_max
   real(kind=long),dimension(:),pointer :: zt, dz
   type(profil), pointer :: prof
   real(kind=long), parameter :: alpha = 0.015_long
   type(bief), pointer :: les_biefs(:)

   if (ISM_available()) then
      zt(1:) => z_ISM(1:)
      dz(1:) => dz_ISM(1:)
   else
      zt(1:) => z_Debord(1:)
      dz(1:) => dz_Debord(1:)
   endif

   les_biefs(1:) => la_topo%biefs(:)

   nb_bief_nodeb = 0
   do ib = 1, la_topo%net%nb
      nb_section_deb = 0
      nb_section_nodeb = 0
      do isi = les_biefs(ib)%is1, les_biefs(ib)%is2
         prof => la_topo%sections(isi)
         ! NOTE: ici on fait l'hypothèse qu'il y a 3 lits : lit majeur gauche, lit mineur, lit majeur droit
         li1 = prof%li(1) ; li2 = prof%li(2)  !limites du lit mineur

         zb_max = max(prof%xyz(li1)%z,prof%xyz(li2)%z)
         !critère : (Z - Zb)/(Z - Zf) = hauteur relative de débordement > alpha
         zdeb_full = (zb_max - alpha * prof%zf) / (1._long - alpha)
         if (zt(isi)+dz(isi) > zdeb_full) then
            nb_section_deb = nb_section_deb + 1   !ça déborde franchement
         elseif (zt(isi)+dz(isi) < zb_max) then
            nb_section_nodeb = nb_section_nodeb + 1  !ça ne déborde vraiment pas
         endif
      enddo
      nbsec = les_biefs(ib)%is2 - les_biefs(ib)%is1 + 1
      if (nb_section_nodeb == nbsec) then   !pas de débordement du tout sur ce bief
         nb_bief_nodeb = nb_bief_nodeb + 1
      elseif (nb_section_deb < nbsec) then  !il y a au moins une section où le niveau dépasse zb_max
                                            !mais le débordement n'est pas généralisé sur le bief
         ! perdu pour ISM : il y a un bief où le débordement n'est pas généralisé
         ISM_compatibility = 0
         return  !pas la peine de continuer : incompatible avec ISM
      endif
   enddo
   if (nb_bief_nodeb == la_topo%net%nb) then
      !aucun débordement nulle part -> compatible ISM
      ISM_compatibility = -1
   else
      !tous les biefs ont soit un débordement généralisé, soit pas de débordement du tout
      !et il y a au moins un bief avec débordement généralisé -> compatible ISM
      ISM_compatibility = +1
   endif
end function ISM_compatibility


function is_full_overflowing(ib)
!  renvoie VRAI si toutes les sections du bief ib débordent
   use parametres, only: long
   use hydraulique, only: z_debord => zt
   use solution, only: dz_debord => dz
   use StVenant_ISM, only: z_ISM => z, dz_ISM => dz
   use TopoGeometrie, only: la_topo, profil, bief
   use Data_Num_Mobiles, only: ISM_available
   implicit none
   ! -- prototype --
   integer,intent(in) :: ib
   logical :: is_full_overflowing
   ! -- variables locales temporaires --
   integer :: isi, li1, li2
   real(kind=long) :: zdeb_full, zb_max
   real(kind=long),dimension(:),pointer :: zt, dz
   type(profil), pointer :: prof
   real(kind=long), parameter :: alpha = 0.015_long
   type(bief), pointer :: les_biefs(:)

   if (ISM_available()) then
      zt(1:) => z_ISM(1:)
      dz(1:) => dz_ISM(1:)
   else
      zt(1:) => z_Debord(1:)
      dz(1:) => dz_Debord(1:)
   endif

   les_biefs(1:) => la_topo%biefs(:)

   is_full_overflowing = .true.
   do isi = les_biefs(ib)%is1, les_biefs(ib)%is2
      prof => la_topo%sections(isi)
      ! NOTE: ici on fait l'hypothèse qu'il y a 3 lits : lit majeur gauche, lit mineur, lit majeur droit
      li1 = prof%li(1) ; li2 = prof%li(2)  !limites du lit mineur

      zb_max = max(prof%xyz(li1)%z,prof%xyz(li2)%z)
      !critère : (Z - Zb)/(Z - Zf) = hauteur relative de débordement > alpha
      zdeb_full = (zb_max - alpha * prof%zf) / (1._long - alpha)
      is_full_overflowing = is_full_overflowing .AND. (zt(isi)+dz(isi) > zdeb_full)
   enddo
end function is_full_overflowing


function is_nowhere_overflowing(ib)
!  renvoie VRAI si aucune section du bief ib ne déborde
   use parametres, only: long
   use hydraulique, only: z_debord => zt
   use solution, only: dz_debord => dz
   use StVenant_ISM, only: z_ISM => z, dz_ISM => dz
   use TopoGeometrie, only: la_topo, profil
   use Data_Num_Mobiles, only: ISM_available
   implicit none
   ! -- prototype --
   integer,intent(in) :: ib
   logical :: is_nowhere_overflowing
   ! -- variables locales temporaires --
   integer :: isi, li1, li2
   real(kind=long) :: zb_max
   real(kind=long),dimension(:),pointer :: zt, dz
   type(profil), pointer :: prof

   if (ISM_available()) then
      zt(1:) => z_ISM(1:)
      dz(1:) => dz_ISM(1:)
   else
      zt(1:) => z_Debord(1:)
      dz(1:) => dz_Debord(1:)
   endif

   is_nowhere_overflowing = .true.
   do isi = la_topo%biefs(ib)%is1, la_topo%biefs(ib)%is2
      prof => la_topo%sections(isi)
      ! NOTE: ici on fait l'hypothèse qu'il y a 3 lits : lit majeur gauche, lit mineur, lit majeur droit
      li1 = prof%li(1) ; li2 = prof%li(2)  !limites du lit mineur

      zb_max = max(prof%xyz(li1)%z,prof%xyz(li2)%z)
      is_nowhere_overflowing = is_nowhere_overflowing .AND. (zt(isi)+dz(isi) < zb_max)
   enddo
end function is_nowhere_overflowing
