!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - INRAE - 1989-2020                           #
!# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################
!
module Tests_Unitaires
!===============================================================================
!   regroupement des tests unitaires
!===============================================================================
use parametres, only: long
use, intrinsic :: iso_fortran_env, only: output_unit

use Mage_Utilitaires, only: init_random_seed, produit_matrice_inverse, permut_4123
use objet_point, only: point3D
use objet_section, only: profil
use StVenant_ISM, only: debits_ISM, operator(-), operator(+)

integer :: l_out

contains

subroutine alltests
   character(len=13) :: logfile = 'test_mage.txt'
   integer :: i
   type(point3D) :: ptX(14), ptY(19), ptZ(49)
   logical :: bexist

   ptX(:)%x = 0._long
   ptX(:)%tag = ''
   ptX(1)%y = 0._long   ;  ptX(1)%z = 10._long
   ptX(2)%y = 2._long   ;  ptX(2)%z = 7._long
   ptX(3)%y = 11._long  ;  ptX(3)%z = 5._long  ;  ptX(3)%tag='RG'
   ptX(4)%y = 12._long  ;  ptX(4)%z = 0._long
   ptX(5)%y = 13._long  ;  ptX(5)%z = 0._long
   ptX(6)%y = 13.5_long  ;  ptX(6)%z = 2._long
   ptX(7)%y = 14._long  ;  ptX(7)%z = 0._long
   ptX(8)%y = 15._long  ;  ptX(8)%z = 0._long
   ptX(9)%y = 16._long  ;  ptX(9)%z = 4._long  ;  ptX(9)%tag='RD'
   ptX(10)%y = 20._long  ;  ptX(10)%z = 4.5_long
   ptX(11)%y = 22._long  ;  ptX(11)%z = 6.5_long
   ptX(12)%y = 24._long  ;  ptX(12)%z = 7._long
   ptX(13)%y = 28._long  ;  ptX(13)%z = 6._long
   ptX(14)%y = 29._long  ;  ptX(14)%z = 11._long
   ptX(1:14)%nbcs = 1
   do i = 1, 14
      allocate(ptX(i)%cs(1))
   enddo

   ptY(:)%tag = ''
   ptY(1)%x = 35135.0000_long  ; ptY(1)%y = 33.0000_long  ; ptY(1)%z =  101.0100_long
   ptY(2)%x = 35135.0000_long  ; ptY(2)%y = 56.9000_long  ; ptY(2)%z =  101.0000_long
   ptY(3)%x = 35135.0000_long  ; ptY(3)%y = 49.3000_long  ; ptY(3)%z =   91.0000_long
   ptY(4)%x = 35135.0000_long  ; ptY(4)%y = 45.2000_long  ; ptY(4)%z =   87.0000_long
   ptY(5)%x = 35135.0000_long  ; ptY(5)%y = 35.0000_long  ; ptY(5)%z =   77.0000_long
   ptY(6)%x = 35135.0000_long  ; ptY(6)%y = 33.0000_long  ; ptY(6)%z =   75.0000_long ; ptY(6)%tag='_rg'
   ptY(7)%x = 35135.0000_long  ; ptY(7)%y = 33.0000_long  ; ptY(7)%z =   71.3000_long ; ptY(7)%tag='fon'
   ptY(8)%x = 35135.0000_long  ; ptY(8)%y = 35.0000_long  ; ptY(8)%z =   71.5000_long
   ptY(9)%x = 35135.0000_long  ; ptY(9)%y = 45.2000_long  ; ptY(9)%z =   71.6000_long
   ptY(10)%x = 35135.0000_long ; ptY(10)%y = 56.9000_long ; ptY(10)%z =   71.7000_long
   ptY(11)%x = 35135.0000_long ; ptY(11)%y = 71.3000_long ; ptY(11)%z =   71.7000_long
   ptY(12)%x = 35135.0000_long ; ptY(12)%y = 86.5000_long ; ptY(12)%z =   71.9000_long
   ptY(13)%x = 35135.0000_long ; ptY(13)%y = 91.5000_long ; ptY(13)%z =   72.0000_long
   ptY(14)%x = 35135.0000_long ; ptY(14)%y = 90.4000_long ; ptY(14)%z =   75.5000_long ; ptY(14)%tag='_rd'
   ptY(15)%x = 35135.0000_long ; ptY(15)%y = 87.0000_long ; ptY(15)%z =   79.5000_long
   ptY(16)%x = 35135.0000_long ; ptY(16)%y = 86.0000_long ; ptY(16)%z =   90.4000_long
   ptY(17)%x = 35135.0000_long ; ptY(17)%y = 78.0000_long ; ptY(17)%z =   99.0000_long
   ptY(18)%x = 35135.0000_long ; ptY(18)%y = 78.0000_long ; ptY(18)%z =  101.0000_long
   ptY(19)%x = 35135.0000_long ; ptY(19)%y = 91.5000_long ; ptY(19)%z =  101.0100_long
   ptY(1:19)%nbcs = 1
   do i = 1, 19
      allocate(ptY(i)%cs(1))
   enddo

   ptZ(:)%tag = ''
   ptZ(1)%x  = 758705.0000_long ; ptZ(1)%y  = 238472.5000_long  ;  ptZ(1)%z  = 110.0000_long
   ptZ(2)%x  = 758658.9375_long ; ptZ(2)%y  = 238454.9182_long  ;  ptZ(2)%z  = 104.6763_long
   ptZ(3)%x  = 758612.6193_long ; ptZ(3)%y  = 238437.2388_long  ;  ptZ(3)%z  = 103.6968_long
   ptZ(4)%x  = 758566.2683_long ; ptZ(4)%y  = 238419.5469_long  ;  ptZ(4)%z  = 103.5751_long
   ptZ(5)%x  = 758519.9219_long ; ptZ(5)%y  = 238401.8567_long  ;  ptZ(5)%z  = 102.7483_long
   ptZ(6)%x  = 758473.5755_long ; ptZ(6)%y  = 238384.1665_long  ;  ptZ(6)%z  = 101.9215_long
   ptZ(7)%x  = 758427.2291_long ; ptZ(7)%y  = 238366.4763_long  ;  ptZ(7)%z  = 101.0947_long
   ptZ(8)%x  = 758380.8792_long ; ptZ(8)%y  = 238348.7848_long  ;  ptZ(8)%z  = 100.5765_long
   ptZ(9)%x  = 758334.5275_long ; ptZ(9)%y  = 238331.0926_long  ;  ptZ(9)%z  = 100.2292_long
   ptZ(10)%x = 758288.1752_long ; ptZ(10)%y = 238313.4001_long  ;  ptZ(10)%z = 100.1890_long
   ptZ(11)%x = 758241.8226_long ; ptZ(11)%y = 238295.7075_long  ;  ptZ(11)%z = 100.2706_long
   ptZ(12)%x = 758195.4699_long ; ptZ(12)%y = 238278.0150_long  ;  ptZ(12)%z = 100.1353_long
   ptZ(13)%x = 758149.1172_long ; ptZ(13)%y = 238260.3224_long  ;  ptZ(13)%z = 100.0000_long ; ptZ(13)%tag = 'rg'
   ptZ(14)%x = 758141.2774_long ; ptZ(14)%y = 238257.3300_long  ;  ptZ(14)%z =  99.6940_long
   ptZ(15)%x = 758133.4377_long ; ptZ(15)%y = 238254.3376_long  ;  ptZ(15)%z =  99.3879_long
   ptZ(16)%x = 758125.5979_long ; ptZ(16)%y = 238251.3452_long  ;  ptZ(16)%z =  99.0819_long
   ptZ(17)%x = 758117.7582_long ; ptZ(17)%y = 238248.3528_long  ;  ptZ(17)%z =  98.7758_long
   ptZ(18)%x = 758109.9184_long ; ptZ(18)%y = 238245.3604_long  ;  ptZ(18)%z =  98.4698_long
   ptZ(19)%x = 758102.0787_long ; ptZ(19)%y = 238242.3680_long  ;  ptZ(19)%z =  98.1638_long
   ptZ(20)%x = 758094.2389_long ; ptZ(20)%y = 238239.3756_long  ;  ptZ(20)%z =  97.8577_long
   ptZ(21)%x = 758086.3992_long ; ptZ(21)%y = 238236.3832_long  ;  ptZ(21)%z =  97.5517_long
   ptZ(22)%x = 758078.5594_long ; ptZ(22)%y = 238233.3908_long  ;  ptZ(22)%z =  97.2456_long
   ptZ(23)%x = 758070.7197_long ; ptZ(23)%y = 238230.3984_long  ;  ptZ(23)%z =  96.9396_long
   ptZ(24)%x = 758062.9240_long ; ptZ(24)%y = 238227.4229_long  ;  ptZ(24)%z =  96.0290_long
   ptZ(25)%x = 758055.6915_long ; ptZ(25)%y = 238224.6623_long  ;  ptZ(25)%z =  94.5000_long ; ptZ(25)%tag = 'fon'
   ptZ(26)%x = 758052.4419_long ; ptZ(26)%y = 238223.4219_long  ;  ptZ(26)%z =  94.5000_long
   ptZ(27)%x = 758049.1922_long ; ptZ(27)%y = 238222.1816_long  ;  ptZ(27)%z =  94.5000_long
   ptZ(28)%x = 758045.9426_long ; ptZ(28)%y = 238220.9412_long  ;  ptZ(28)%z =  94.5000_long
   ptZ(29)%x = 758042.6930_long ; ptZ(29)%y = 238219.7008_long  ;  ptZ(29)%z =  94.5000_long
   ptZ(30)%x = 758039.4433_long ; ptZ(30)%y = 238218.4605_long  ;  ptZ(30)%z =  94.5000_long
   ptZ(31)%x = 758036.1937_long ; ptZ(31)%y = 238217.2201_long  ;  ptZ(31)%z =  94.5000_long
   ptZ(32)%x = 758032.9441_long ; ptZ(32)%y = 238215.9798_long  ;  ptZ(32)%z =  94.5000_long
   ptZ(33)%x = 758029.6945_long ; ptZ(33)%y = 238214.7394_long  ;  ptZ(33)%z =  94.5000_long
   ptZ(34)%x = 758027.0727_long ; ptZ(34)%y = 238213.7382_long  ;  ptZ(34)%z =  95.4100_long
   ptZ(35)%x = 758024.1551_long ; ptZ(35)%y = 238212.6221_long  ;  ptZ(35)%z =  96.9400_long
   ptZ(36)%x = 758021.2376_long ; ptZ(36)%y = 238211.5061_long  ;  ptZ(36)%z =  98.4700_long
   ptZ(37)%x = 758018.3200_long ; ptZ(37)%y = 238210.3900_long  ;  ptZ(37)%z = 100.0000_long ; ptZ(37)%tag = 'rd'
   ptZ(38)%x = 757993.0384_long ; ptZ(38)%y = 238168.2076_long  ;  ptZ(38)%z = 100.7508_long
   ptZ(39)%x = 757967.7560_long ; ptZ(39)%y = 238126.0239_long  ;  ptZ(39)%z = 101.3939_long
   ptZ(40)%x = 757942.4736_long ; ptZ(40)%y = 238083.8402_long  ;  ptZ(40)%z = 102.0370_long
   ptZ(41)%x = 757917.1897_long ; ptZ(41)%y = 238041.6540_long  ;  ptZ(41)%z = 102.3469_long
   ptZ(42)%x = 757891.9057_long ; ptZ(42)%y = 237999.4675_long  ;  ptZ(42)%z = 102.1435_long
   ptZ(43)%x = 757866.6237_long ; ptZ(43)%y = 237957.2844_long  ;  ptZ(43)%z = 102.2499_long
   ptZ(44)%x = 757841.3459_long ; ptZ(44)%y = 237915.1084_long  ;  ptZ(44)%z = 103.3907_long
   ptZ(45)%x = 757816.0682_long ; ptZ(45)%y = 237872.9325_long  ;  ptZ(45)%z = 104.5315_long
   ptZ(46)%x = 757790.7854_long ; ptZ(46)%y = 237830.7481_long  ;  ptZ(46)%z = 104.9635_long
   ptZ(47)%x = 757765.5011_long ; ptZ(47)%y = 237788.5612_long  ;  ptZ(47)%z = 105.1773_long
   ptZ(48)%x = 757740.2409_long ; ptZ(48)%y = 237746.4145_long  ;  ptZ(48)%z = 106.7051_long
   ptZ(49)%x = 757715.0131_long ; ptZ(49)%y = 237704.3219_long  ;  ptZ(49)%z = 110.0000_long
   ptZ(1:49)%nbcs = 1
   do i = 1, 49
      allocate(ptZ(i)%cs(1))
   enddo



   write(output_unit,'(a)') ' Lancement des tests unitaires'
   inquire(file=logfile,exist=bexist)
   if (bexist) then
      open (newunit=l_out,file=logfile,form='formatted',status='old')
      close(unit=l_out,status='delete')
   endif
   open (newunit=l_out,file=logfile,form='formatted',status='new')
   call test_limite_eau(ptX)
   call test_largeur(ptX)
   call test_perimetre(ptX)
   call test_section(ptX)

   call test_limite_eau(ptY)
   call test_largeur(ptY)
   call test_perimetre(ptY)
   call test_section(ptY)

   call test_limite_eau(ptZ)
   call test_largeur(ptZ)
   call test_perimetre(ptZ)
   call test_section(ptZ)

   call test_permut_4123
   do i = 1, 5
      call test_produit_matrice_inverse
   enddo

   call test_operateur_plus
   call test_operateur_moins
   write(output_unit,'(2a)') ' Tests unitaires terminés, résultats complets dans le fichier ',logfile
end subroutine alltests


subroutine test_limite_eau(ptX)
! test unitaire pour la routine limite_eau() du module objet_section
   type(point3D), intent(in) :: ptX(:)
   type(profil) :: prf
   real(kind=long) :: z, dh
   integer :: i,k, nval
   !titre du test unitaire
   write(output_unit,'(a)') '   Test unitaire sur la fonction limite_eau()'
   write(l_out,'(/,a)') 'Test unitaire sur la fonction limite_eau()'
   !création du profil
   call prf%init(0._long, 'toto' ,ptX)
   !calculs
   write(l_out,*) 'Limites des sous-sections : ',(prf%li(k),k=0,prf%nzone)
   nval = 25
   dh = (maxval(prf%xyz(:)%z) - prf%zf) /real(nval,kind=long)
   z = prf%zf
   do i = 1, nval+1
      z = z + dh
      call prf%limite_Eau(z)
      if (prf%irg == 1 .and. prf%ird == prf%np) then
         write(l_out,'(f10.4,2i3,*(f10.4))') z,prf%irg,prf%ird,prf%xyz(prf%irg)%z+1000._long,prf%xyz(prf%irg)%z, &
                                                               prf%xyz(prf%ird)%z,prf%xyz(prf%ird)%z+1000._long
      elseif (prf%irg == 1) then
         write(l_out,'(f10.4,2i3,*(f10.4))') z,prf%irg,prf%ird,prf%xyz(prf%irg)%z+1000._long,prf%xyz(prf%irg)%z, &
                                                               prf%xyz(prf%ird)%z,prf%xyz(prf%ird+1)%z
      elseif (prf%ird == prf%np) then
         write(l_out,'(f10.4,2i3,*(f10.4))') z,prf%irg,prf%ird,prf%xyz(prf%irg-1)%z,prf%xyz(prf%irg)%z, &
                                                               prf%xyz(prf%ird)%z,prf%xyz(prf%ird)%z+1000._long
      else
         write(l_out,'(f10.4,2i3,*(f10.4))') z,prf%irg,prf%ird,prf%xyz(prf%irg-1)%z,prf%xyz(prf%irg)%z, &
                                                               prf%xyz(prf%ird)%z,prf%xyz(prf%ird+1)%z
      endif
   enddo

end subroutine test_limite_eau


subroutine test_largeur(ptX)
! test unitaire pour la routine largeur() du module objet_section
   type(point3D), intent(in) :: ptX(:)
   type(profil) :: prf
   real(kind=long) :: z, lg(-1:10), s, test1, test2, dh
   integer :: i,k, nval
   !titre du test unitaire
   write(output_unit,'(a)') '   Test unitaire sur la fonction Largeur()'
   write(l_out,'(/,a)') 'Test unitaire sur la fonction Largeur()'
   !création du profil
   call prf%init(0._long, 'toto' ,ptX)
   !calculs
   write(l_out,*) 'Limites des sous-sections : ',(prf%li(k),k=0,prf%nzone)
   test1 = 0._long ; test2 = 0.0_long
   nval = 25
   dh = (maxval(prf%xyz(:)%z) - prf%zf) /real(nval,kind=long)
   z = prf%zf
   do i = 1, nval
      z = z + dh
      lg(-1) = prf%largeur_LC(z)
      do k = 0, prf%nzone
         lg(k) = prf%largeur(z,k)
      enddo
      s = sum(lg(1:prf%nzone))
      call prf%limite_Eau(z)
      write(l_out,'(3f12.5,5f12.5,2i3)') z,lg(-1)-s,lg(0)-s,(lg(k), k=-1,prf%nzone),prf%irg,prf%ird
      test1 = max(test1,abs(lg(-1)-s))
      test2 = max(test2,abs(lg(0)-s))
   enddo
   write(l_out,*) 'Résultat du test : ',test1,' (doit être très voisin de zéro en mètres si la trace du profil est rectiligne)'
   write(l_out,*) '                   ',test2,' (doit être très voisin de zéro en mètres)'

end subroutine test_largeur


subroutine test_perimetre(ptX)
! test unitaire pour la routine perimetre() du module objet_section
   type(point3D), intent(in) :: ptX(:)
   type(profil) :: prf
   real(kind=long) :: z, pm(-1:10), s, test1, test2, dh
   integer :: i,k, nval
   !titre du test unitaire
   write(output_unit,'(a)') '   Test unitaire sur la fonction Perimetre()'
   write(l_out,'(/,a)') 'Test unitaire sur la fonction Perimetre()'
   !création du profil
   call prf%init(0._long, 'toto' ,ptX)
   !calculs
   write(l_out,*) 'Limites des sous-sections : ',(prf%li(k),k=0,prf%nzone)
   test1 = 0._long ; test2 = 0.0_long
   nval = 25
   dh = (maxval(prf%xyz(:)%z) - prf%zf) /real(nval,kind=long)
   z = prf%zf
   do i = 1, nval
      z = z + dh
      pm(-1) = prf%perimetre_LC(z)
      do k = 0, prf%nzone
         pm(k) = prf%perimetre(z,k)
      enddo
      s = sum(pm(1:prf%nzone))
      call prf%limite_Eau(z)
      write(l_out,'(3f12.5,5f12.5,2i3)') z,pm(-1)-s,pm(0)-s,(pm(k), k=-1,prf%nzone),prf%irg,prf%ird
      test1 = max(test1,abs(pm(-1)-s))
      test2 = max(test2,abs(pm(0)-s))
   enddo
   write(l_out,*) 'Résultat du test : ',test1,' (doit être très voisin de zéro en mètres si la trace du profil est rectiligne)'
   write(l_out,*) '                   ',test2,' (doit être très voisin de zéro en mètres)'

end subroutine test_perimetre


subroutine test_section(ptX)
! test unitaire pour la routine section() du module objet_section
   type(point3D), intent(in) :: ptX(:)
   type(profil) :: prf
   real(kind=long) :: z, st(-1:10), s, test1, dh, test2
   integer :: i,k, nval
   !titre du test unitaire
   write(output_unit,'(a)') '   Test unitaire sur la fonction Section()'
   write(l_out,'(/,a)') 'Test unitaire sur la fonction Section()'
   !création du profil
   call prf%init(0._long, 'toto' ,ptX)
   !calculs
   write(l_out,*) 'Limites des sous-sections : ',(prf%li(k),k=0,prf%nzone)
   test1 = 0._long ; test2 = 0.0_long
   nval = 25
   dh = (maxval(prf%xyz(:)%z) - prf%zf) /real(nval,kind=long)
   z = prf%zf
   do i = 1, nval
      z = z + dh
      st(-1) = prf%section_LC(z)
      do k = 0, prf%nzone
         st(k) = prf%section_mouillee(z,k)
      enddo
      s = sum(st(1:prf%nzone))
      call prf%limite_Eau(z)
      write(l_out,'(3f12.5,5f12.5,2i3)') z,st(-1)-s,st(0)-s,(st(k), k=-1,prf%nzone),prf%irg,prf%ird
      test1 = max(test1,abs(st(-1)-s))
      test2 = max(test2,abs(st(0)-s))
   enddo
   write(l_out,*) 'Résultat du test : ',test1,' (doit être très voisin de zéro en m² si la trace du profil est rectiligne)'
   write(l_out,*) '                   ',test2,' (doit être très voisin de zéro en m²)'

end subroutine test_section

subroutine test_permut_4123
   real(kind=long),dimension(4,4) :: A
   real(kind=long),dimension(4) :: B
   integer :: k

   write(output_unit,'(a)') '   Test unitaire sur la fonction permut_4123()'
   write(l_out,'(/,a)') 'Test unitaire sur la fonction permut_4123()'
   A = 0._long ; B = 0._long
   do k = 1, 4
      a(k,k) = 1._long
      b(k) = real(k,kind=long)
   enddo
   call permut_4123(A,B)
   do k = 1, 4
      write(output_unit,'(4i3,a,i3)') int(a(k,:)),'  ',int(b(k))
      write(l_out,'(4i3,a,i3)') int(a(k,:)),'  ',int(b(k))
   enddo
end subroutine test_permut_4123

subroutine test_produit_matrice_inverse
   !méthode de test : on veut calculer E = A^-1 * B et F = A^-1 * C
   !                  on se donne A, E et F et on définit B = A*E et C = A*F
   real(kind=long),dimension(4,4) :: A, B, E, EE
   real(kind=long),dimension(4) :: C, F, FF
   integer :: i,j,n
   real(kind=long) :: rdom, err1, err2

   n = 4
   write(output_unit,'(a)') '   Test unitaire du sous-programme produit_matrice_inverse()'
   write(l_out,'(/,a)') 'Test unitaire du sous-programme produit_matrice_inverse()'
   call init_random_seed()
   do i = 1, n
      do j = 1, n
         call random_number(rdom)
         a(i,j) = 1000._long*rdom - 500._long
         call random_number(rdom)
         e(i,j) = 10._long*rdom - 5._long
      enddo
      call random_number(rdom)
      f(i) = 1000._long*rdom - 500._long
   enddo
   EE = E ; FF = F
   B = matmul(A,E)
   C = matmul(A,F)
   call produit_matrice_inverse(A,B,E,C,F,n)
   do i = 1, n
      write(output_unit,'(2(4f8.2,a),2(f8.2,a))') ee(i,:),'  ',e(i,:),'  ',ff(i),'  ',f(i)
      write(l_out,'(2(4f8.2,a),2(f8.2,a))') ee(i,:),'  ',e(i,:),'  ',ff(i),'  ',f(i)
   enddo
   err1 = sum(abs(ee-e))
   err2 = sum(abs(ff-f))
   write(output_unit,'(2(a,g14.6,/))') ' Erreur sur les matrices : ',err1,' Erreur sur les vecteurs : ',err2
   write(l_out,'(2(a,g14.6,/))') ' Erreur sur les matrices : ',err1,' Erreur sur les vecteurs : ',err2
end subroutine test_produit_matrice_inverse


subroutine test_operateur_plus
   !test de l'opérateur + pour le type-dérivé debits_ISM
   type(debits_ISM) :: q0, q1, q2
   type(debits_ISM),dimension(10) :: qq0, qq1, qq2
   integer :: n

   write(output_unit,'(a)') '   Test unitaire de l''opérateur + pour le type-dérivé debits_ISM'
   write(l_out,'(/,a)') 'Test unitaire de l''opérateur + pour le type-dérivé debits_ISM'
   write(output_unit,'(a)') '     cas scalaire : on doit voir des 0 partout'
   write(l_out,'(a)') '     cas scalaire : on doit voir des 0 partout'
   q1%qm = 5._long ; q1%ql = 1._long ; q1%qr = 2._long
   q2%qm = -5._long ; q2%ql = -1._long ; q2%qr = -2._long
   q0 = q1 + q2
   write(output_unit,'(3f8.2)') q0%qm,q0%ql,q0%qr
   write(l_out,'(3f8.2)') q0%qm,q0%ql,q0%qr

   write(output_unit,'(a)') '     cas des tableaux : on doit voir des 0 partout'
   write(l_out,'(a)') '     cas des tableaux : on doit voir des 0 partout'
   do n = 1, 10
      qq1(n)%qm = 5._long ; qq1(n)%ql = 1._long ; qq1(n)%qr = 2._long
      qq2(n)%qm = -5._long ; qq2(n)%ql = -1._long ; qq2(n)%qr = -2._long
   enddo
   qq0 = qq1+qq2
   do n = 1, 10
      write(output_unit,'(3f8.2)') qq0(n)%qm,qq0(n)%ql,qq0(n)%qr
      write(l_out,'(3f8.2)') qq0(n)%qm,qq0(n)%ql,qq0(n)%qr
   enddo
end subroutine test_operateur_plus


subroutine test_operateur_moins
   !test de l'opérateur + pour le type-dérivé debits_ISM
   type(debits_ISM) :: q0, q1, q2
   type(debits_ISM),dimension(10) :: qq0, qq1, qq2
   integer :: n

   write(output_unit,'(a)') '   Test unitaire de l''opérateur - pour le type-dérivé debits_ISM'
   write(l_out,'(/,a)') 'Test unitaire de l''opérateur - pour le type-dérivé debits_ISM'
   write(output_unit,'(a)') '     cas scalaire : on doit voir des 0 partout'
   write(l_out,'(a)') '     cas scalaire : on doit voir des 0 partout'
   q1%qm = 5._long ; q1%ql = 1._long ; q1%qr = 2._long
   q2%qm = 5._long ; q2%ql = 1._long ; q2%qr = 2._long
   q0 = q1 - q2
   write(output_unit,'(3f8.2)') q0%qm,q0%ql,q0%qr
   write(l_out,'(3f8.2)') q0%qm,q0%ql,q0%qr

   write(output_unit,'(a)') '     cas des tableaux : on doit voir des 0 partout'
   write(l_out,'(a)') '     cas des tableaux : on doit voir des 0 partout'
   do n = 1, 10
      qq1(n)%qm = 5._long ; qq1(n)%ql = 1._long ; qq1(n)%qr = 2._long
      qq2(n)%qm = 5._long ; qq2(n)%ql = 1._long ; qq2(n)%qr = 2._long
   enddo
   qq0 = qq1 - qq2
   do n = 1, 10
      write(output_unit,'(3f8.2)') qq0(n)%qm,qq0(n)%ql,qq0(n)%qr
      write(l_out,'(3f8.2)') qq0(n)%qm,qq0(n)%ql,qq0(n)%qr
   enddo
end subroutine test_operateur_moins


end module Tests_Unitaires
