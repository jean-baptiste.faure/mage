!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - INRAE - 1989-2020                           #
!# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################
!
! DONE: passer en allocation dynamique pour ne plus dépendre de IBsup, ISsup et NOsup

module TopoGeometrie
!===============================================================================
!                      Description de la topologie du réseau
!                     Données géométriques de base des nœuds

!--> Contenu
! types dérivés
!  - pointAC : couple abscisse - cote ; utilisé pour profilAC
!  - pointLC : point de tabulation largeur - cote ; 4 valeurs :
!              - tirant d'eau
!              - largeur au miroir
!              - section mouillée
!              - périmètre mouillé
!  - point3D : point nommé en coordonnées géographiques, y compris les couches sédimentaires
!  - profilAC : profil abscisses-cotes : liste de pointAC + Pk
!  - profil   : profil TS complet avec :
!               - nom, Pk, cote du fond et Strickler global
!               - données géométriques brutes :
!                         - tableau de points (+ nb de points)
!                         - découpage en lits (+ nb de lits)
!               - données tabulées en largeurs-cotes pour la couche supérieure
!                         - tableau de pointLC (+ nb de points)
!                         - drapeau (booléen) pour savoir si la géométrie LC
!                           correspond à la géométrie XYZ (modification possible)
!
!
! routines
!  - calculerRang : calcul du rang "hydraulique" de chaque bief
!  - creerNoeuds : création de la liste des noeuds à partir de la
!                  liste des biefs
!  - initTopoGeometrie : création d'une topologie et de la géométrie
!                        à partir d'un fichier de topologie
!  - init_from_file_CAS : initialisation de Topologie/nodes(*)
!  - init_nodes_to_zero : initialise Topologie/nodes(*) à zéro
!  - lireReseau : lecture du fichier NET et initialisation du
!                 tableau biefs
!
!  - addBief       : ajoute un bief à la collection jeuSection
!  - copyProfil    : copie un profil complet ; remplace l'affectation
!  - largeursCotes : tabulation d'un profil en travers à partir de ses
!                    données brutes
!  - lireSediment  : lecture d'un fichier de données sédimentaires
!  - newBief       : construction d'un jeu de profils TS par lecture d'un fichier
!                    de profils XYZ au format ST2 (couches sédimentaires
!                    comprises) et d'un fichier des classes granulométriques
!                    (CGfile lu par lireSediment)
!  - removeProfil  : désallocation d'un profil TS
!  - triZ          : classement d'un profil LC par Z croissants avec élimination
!                    des doublons
!
! fonctions
!  NB : les routines préfixées par ptr utilisent des pointeurs
!        - ptr_surface : renvoie la surface d'un noeud pour une cote donnée
!        - surface     : renvoie la surface d'un noeud pour une cote donnée
!        - ptr_volume  : renvoie le volume d'un noeud pour une cote donnée
!        - volume     : renvoie le volume d'un noeud pour une cote donnée
!
!              Données géométriques des sections (profils en travers)
!  - distance2D : distance euclidienne entre 2 pointAC
!  - distance3D : distance euclidienne entre 2 point3D
!  - largeur    : interpolation de la largeur au miroir dans un profil TS
!                 pour une cote donnée
!  - newProfil  : création d'un profil TS à partir d'un tableau de points TS
!                 et d'un pk
!  - perimetre  : interpolation du périmètre mouillé dans un profil TS
!                 pour une cote donnée
!  - projectionOrtho : projection orthogonale du point xyz sur la droite [xyz1,xyz2]
!  - projectionPlan  : conversion d'un profil XYZ en profil AC par projection
!                      sur le plan joignant les 2 points extrêmes
!  - removeDoublons  : suppression des doublons, c'est à dire les points
!                      identiques et successifs
!  - section         : interpolation de la section mouillée dans un profil TS
!                      pour une cote donnée
!
!===============================================================================
use Parametres, only: long, lnode, lname, nnsup, zero, precision_alti, &
                      l9, un, toutpetit
use, intrinsic :: iso_fortran_env, only: error_unit, output_unit

use basic_Types, only: ptr_courbe2D, map_courbe2D, ptr_interpole

use Mage_Utilitaires, only: between, next_real, next_string, next_int, zegal, do_crash

use objet_bief, only: bief, newBief, seg_sed

use objet_section, only: profil_brut, profil, profilAC, verbose

use objet_point, only: pointLC, pointAC, point3D,&
                       distance3D, distanceH, distance2D, p_scalaire_H, interpol3D

implicit none

integer, parameter :: lbief = 15  !nombre de caractères autorisés pour les noms des biefs
!integer, parameter :: lnode = 10  !nombre de caractères autorisés pour les noms des nœuds
! NOTE: la définition de verbose est nécessaire pour éviter une dépendance circulaire avec la module Data_Num_Fixes
! logical :: verbose ! clone inversé du silent de Data_Num_Fixes
logical :: is_ISM  ! indicateur pour savoir si on utilise ISM ou non ; permet de ne pas être dépendant de Data_Num_Fixes
! logical :: is_LC   ! indicateur pour savoir s'il faut utiliser la variante largeur-cote des routines largeur(), perimetre() et surface()
! integer :: LC_type ! type de construction de la représentation largeurs-cotes des profils.
                   ! 0 -> méthode d'origine mise en œuvre par largeursCotes()
                   ! 1 -> calcul en utilisant largeur_XYZ(), perimetre_XYZ() et section_XYZ() sur le profil 3D
                   ! 2 -> calcul en utilisant largeur_XYZ(), perimetre_XYZ() et section_XYZ() sur la projection
                   !      abscisses-cotes du profil original (dans un monde parfait 2 == 0)
! Contrôle du charriage
integer :: with_charriage ! 0 -> pas de charriage
                          ! 1 -> couches sédimentaires lues dans ST
                          ! 2 -> couches sédimentaires lues dans SED


! Nœud : les nœuds du réseau ; contient la discrétisation altitude - surface
type node
   character(len=lnode) :: name  ! nom du nœud
   integer :: cl !position dans le réseau et type de C.L. (1 seule C.L. par nœud) :
                 ! > 0 : nœud amont : le nœud est nœud aval d'aucun bief
                 !      +1 : hydrogramme global
                 !      +2 : 2 hydrogrammes, un pour le lit mineur et un pour le lit majeur
                 !      +3 : 3 hydrogrammes, un pour chaque sous-section, type debits_ISM
                 ! < 0 : nœud aval : le nœud est nœud amont d'aucun bief
                 !      -1 : loi q(z) (discretisée dans tableau)
                 !      -2 : régime uniforme
                 !      -3 : loi z(t) (discretisée dans tableau)
                 ! = 0 : nœud intérieur, ni nœud amont ni nœud aval ; hydrogramme d'apport ponctuel
   integer :: np    ! nombre de niveaux du profil = taille utile des tableaux
                    ! si np = 0 : pas de surface
   real(kind=long),allocatable :: zne(:) ! tabulation des cotes pour décrire la géométrie du nœud
   real(kind=Long),allocatable :: sne(:) ! tabulation des surfaces pour décrire la géométrie du nœud
                                 ! sne(n) est la surface à la cote zne(n)
   real(kind=Long),allocatable :: vne(:) ! tabulation des volumes pour décrire la géométrie du nœud
                                 ! vne(n) est le volume sous la cote zne(n)
   integer :: ip ! niveau d'interpolation courant (dernier utilisé)
                 ! la dernière cote utilisée est entre zne(ip) et zne(ip+1)
end type node


! Réseau : c'est l'objet qui fait les liens entre les nœuds et les biefs
!          contient les dimensions effectives et variables globales du réseau
!          les tables d'index permettant de parcourir les biefs et les nœuds
type rezo
   ! dimensions effectives
   integer :: nb    ! nb de biefs ou branches du réseau
   integer :: nn    ! nb de noeuds
   integer :: NCLM     ! nb de C.L. amont
   integer :: NCLV     ! nb de C.L. aval
   integer :: nss    ! nb de sections singulieres
   integer :: ns    ! nb de sections ou points de calcul

   ! topologie
   integer, allocatable :: nivoH(:) ! niveau hydraulique des biefs du réseau ; dimension ibmax
   integer, allocatable :: nzno(:)  ! nzno(n) = nombre de biefs qui touchent le noeud n ; dimension nomax
   integer :: iba          ! rang du dernier bief à l'amont de la maille
                           ! iba = -1 ==> aucun bief a l'amont de la maille ==> ne sait pas faire
                           ! iba > 0 ==> iba = nombre de biefs amonts de la maille
                           !            iba=ibmax s'il n'y a pas de maille
                           ! iba = 0 ==> impossible
   integer :: iby          ! rang du premier bief aval de maille (=/= à l'aval de la maille donc en dehors de la maille)
                           ! remarque : si le réseau est maillé, il y a au moins 2 biefs aval de maille, ce sont les biefs qui
                           ! ferment la maille ou qui portent les CL aval si la maille est ouverte
                           ! = ibmax+1 si le réseau est ramifié
   integer :: ibz          ! rang du premier bief à l'aval de la maille
                           ! = ibmax+1-nombre de biefs à l'aval de la maille
                           ! = ibmax+1 si la maille reste ouverte ou si le réseau est ramifié
   integer, allocatable :: lbz(:,:) ! dimensions (nomax,2)
                           ! lbz(n,1) = rang du premier bief qui part du noeud n
                           ! lbz(n,2) = rang du dernier bief qui part du noeud n
                           ! si n est nœud aval on pose lbz(n,1) = lbz(n,2) = ibmax+1
                           ! ainsi, si n est nœud aval de maille lbz(n,1) = lbz(n,2) = ibz, que la maille soit fermée ou ouverte
   integer, allocatable :: numero(:)! numero(ib) = numéro dans l'ordre des données du bief de rang ib
                           ! dans l'ordre de calcul de la ligne d'eau.
                           ! En particulier :
                           ! numero(1) = numéro du premier bief amont du modèle
                           ! numero(ibmax) = numéro du dernier bief aval du modèle
   integer, allocatable :: rang(:)  ! rang(kb) = rang de calcul du bief de numéro kb dans l'ordre
                           !            des données

   integer, allocatable :: lbamv(:) ! liste des biefs amont et aval (repérés par leur rang)
                           ! ces biefs portent les c.l. :
                           ! Si le noeud n est noeud amont du réseau,
                           !               lbamv(n) = rang du bief qui part de n
                           ! Si le noeud n est noeud aval du réseau,
                           !               lbamv(n) = rang du bief qui arrive en n
                           ! Si le noeud n est un noeud intérieur,
                           !               lbamv(n) = 0
end type rezo

type confluence
    integer, dimension(2) :: amont
    integer :: aval
    real(kind=Long), dimension(3) :: angles
end type confluence

type defluence
    integer :: amont
    integer, dimension(2) :: aval
    real(kind=Long), dimension(3) :: angles
end type defluence


! dataset : un jeu de données topologie et géométrie complet
type dataset
   type(rezo) :: net                              !le réseau : ensemble des données topologiques à l'exception
                                                  !des biefs et des noeuds eux-mêmes
   type (bief), allocatable :: biefs(:)           !les biefs du réseau indexés dans l'ordre de
                                                  !lecture dans le fichier NET
   type (node), allocatable :: nodes(:)           !les noeuds du réseau globaux
   type (profil), allocatable :: sections(:)      !collection des profils globaux
end type dataset

type(dataset), target :: la_topo


contains

subroutine initTopoGeometrie(netFile)
!==============================================================================
!     Création d'une topologie et de la géométrie à partir
!                d'un fichier de topologie
!
!   cette routine remplit les tableaux biefs, nodes et jeuSection
!
! entrée : fichier au format NET
!          pour le format NET voir l'entête de lireReseau
! sortie : dataset la_topo
!
!==============================================================================
   !prototype
   character(len=lname), intent(in) :: netfile

   !variables locales
!    type(profil), allocatable :: jeu_profils(:)
   integer :: ib, debut, fin, ibb, nprof, ismax, luu, ierr
   class(profil_brut), dimension(:), allocatable :: les_profils

   ! calculs
   if(verbose) write(l9,*) 'Entrée dans initTopoGeometrie()'
   if(verbose) write(l9,*) 'Fichier de définition du réseau : ',netfile
   call lireReseau(netfile, la_topo%biefs, la_topo%net%nb)
   if(verbose) write(l9,*) '==> Lecture du réseau : OK'
   call creerNoeuds(la_topo%biefs, la_topo%net%nb, la_topo%nodes, la_topo%net%nn)
   if(verbose) write(l9,*) '==> Création des nœuds : OK'
   call calculerRang(la_topo%biefs, la_topo%nodes, la_topo%net)
   if(verbose) write(l9,*) '==> Calcul du rang hydraulique : OK'

   if(verbose) write(l9,'(a)') ' ==> Comptage des profils de tous les biefs'
   ismax = 0
   do ib = 1, la_topo%net%nb
      call compte_profils(la_topo%biefs(ib)%fichier_geo,nprof)
      ismax = ismax + nprof
   enddo
   allocate(la_topo%sections(ismax))
   ! NOTE: la_topo%net%ns est calculé par addBief()
   if(verbose) write(l9,'(a)') ' ==> Liste des biefs et Rangs de calcul'
   do ib = 1, la_topo%net%nb  !parcours dans l'ordre des données (.NET)
   ismax = 0
      call compte_profils(la_topo%biefs(ib)%fichier_geo,nprof)
      la_topo%biefs(ib)%sections_ptr(1:) => la_topo%sections(ismax+1:ismax + nprof)
      la_topo%biefs(ib)%nprof = nprof
      la_topo%biefs(ib)%sections_mem_size = 0 ! pointer to a slice of la_topo%sections
      ismax = ismax + nprof
      allocate (profil::les_profils(nprof))
      open(newunit=luu,file=trim(la_topo%biefs(ib)%fichier_geo),status='old',form='formatted',iostat=ierr)
      if (ierr > 0) then
          write(output_unit,*) '>>>> Ouverture du fichier ST ',trim(la_topo%biefs(ib)%fichier_geo),' impossible'
          stop ' Ouverture du fichier ST impossible'
      endif
      call newBief(la_topo%biefs(ib)%fichier_geo, luu, les_profils,la_topo%biefs(ib)%name, with_charriage)
      close(luu)
      select type(les_profils)
         type is (profil)
         call addBief(les_profils,debut,fin, la_topo%sections, la_topo%net%ns)
      end select
      la_topo%biefs(ib)%is1 = debut; la_topo%biefs(ib)%is2 = fin
      if(verbose) write(l9,'(a,i3,7a,i4,a,i4,2(a,i3),2f10.3)') &
              ' Bief n°',ib,' : ',la_topo%biefs(ib)%name,' ; ',la_topo%biefs(ib)%amont,' ; ', &
              la_topo%biefs(ib)%aval,' ; ',debut, ' - ',fin, ' ; Rang = ', la_topo%net%rang(ib),&
              ' ; Niveau Hyd. = ',la_topo%net%nivoH(ib),la_topo%sections(debut)%pk,la_topo%sections(fin)%pk
      do ibb = 1, size(les_profils)
         call les_profils(ibb)%remove_profil()
      enddo
      deallocate (les_profils)
   enddo
   !!définition des biefs amont gauche droit et centré pour chaque bief du réseau
   if (is_ISM) call recherche_biefs_amont

   !!correction des pk pour tenir compte des arrondis fait par PamHyr lors de l'export en TAL
!   do is = 1, la_topo%net%ns
!      if (la_topo%sections(is)%pk > 99999.9_long .or. la_topo%sections(is)%pk < -9999.9_long) then
!         write(wtmp,'(f7.0)') la_topo%sections(is)%pk  ;  read(wtmp,'(f7.0)') la_topo%sections(is)%pk
!      elseif (la_topo%sections(is)%pk > 9999.9_long .or. la_topo%sections(is)%pk < -999.9_long) then
!         write(wtmp,'(f6.0)') la_topo%sections(is)%pk  ;  read(wtmp,'(f6.0)') la_topo%sections(is)%pk
!      elseif (la_topo%sections(is)%pk > 999.99_long .or. la_topo%sections(is)%pk < -99.99_long) then
!         write(wtmp,'(f6.1)') la_topo%sections(is)%pk ; read(wtmp,'(f6.0)') la_topo%sections(is)%pk
!      else
!         write(wtmp,'(f6.2)') la_topo%sections(is)%pk ; read(wtmp,'(f6.0)') la_topo%sections(is)%pk
!      endif
!   enddo

   !!autres données géométriques
   call init_autres_data_geom

   if(verbose) write(l9,*) 'Sortie de initTopoGeometrie()'
end subroutine initTopoGeometrie



subroutine lireReseau(netFile,biefs,ibmax)
!==============================================================================
!         lecture du fichier NET et initialisation du tableau biefs(*)
!
! format NET
!     - * en colonne 1 : commentaire
!     - une ligne par bief, séparateur espace, avec :
!        - nom du bief
!        - nom du noeud amont
!        - nom du noeud aval
!        - nom du fichier de géométrie (format ST2) associé
!     - exemple :
!                <-------------------------
!                *exemple de fichier NET
!                bief1 AAA BBB saar.st
!                *
! entrée : fichier au format NET
! sortie : le tableau biefs(:) indexé par l'ordre de lecture
!          le nombre de biefs
!
!==============================================================================
   ! prototype
   character(len=lname), intent(in) :: netFile  ! fichier NET
   type(bief), intent(inout), allocatable :: biefs(:)
   integer, intent(out) :: ibmax
   ! variables locales
   integer :: luu, ierr = 0, next = 0, ib = 0, nligne = 0, nxt0
   integer :: err_track
   character(len=80) :: ligne=''
   character(len=1) :: gcd=''

   ! 1ère passe : on compte le nombre de lignes qui ne sont pas des commentaires
   !              pour estimer le nombre de biefs
   open(newunit=luu,file=trim(netFile),form='formatted',status='old')
   nligne = 0
   ibmax = 0
   do while (ierr == 0)
      nligne = nligne+1
      read(luu,'(a)',iostat=ierr) ligne
      if (ierr > 0) then
         write(error_unit,*) 'Erreur 1 de lecture de ',trim(netFile),' a la ligne ',nligne
         stop 6
      elseif (ierr < 0) then
         exit
      else
         if (ligne(1:1) == '*' .or. trim(ligne)=='') cycle
         if (trim(ligne) == '') cycle
         ibmax  = ibmax + 1
      endif
   enddo
   allocate (biefs(ibmax))
   ! calculs
   nligne = 0
   !open(newunit=luu,file=trim(netFile),form='formatted',status='old')
   rewind(luu)
   ierr = 0
   do while (ierr == 0)
      nligne = nligne + 1
      read(luu,'(a)',iostat=ierr) ligne
      if (ierr > 0) then
         write(error_unit,*) 'Erreur 1 de lecture de ',trim(netFile),' a la ligne ',nligne
         stop 6
      elseif (ierr < 0) then
         exit
      else
         if (ligne(1:1) == '*' .or. trim(ligne)=='') cycle
         if (trim(ligne) == '') cycle
         ib = ib+1 ; next = 1 ; nxt0 = next ; err_track = 0
         biefs(ib)%name = next_string(ligne,'',next) ; nxt0 = next
         biefs(ib)%amont = next_string(ligne,'',next)
         if (next-nxt0+1 > lnode) err_track = err_track+1 ; nxt0 = next
         biefs(ib)%aval = next_string(ligne,'',next)
         if (next-nxt0+1 > lnode) err_track = err_track+1
         biefs(ib)%fichier_geo = next_string(ligne,'',next)
         gcd = next_string(ligne,'',next)
         select case (gcd)
            case ('G','g','-1')
               biefs(ib)%gcd = -1
            case ('D','d','1')
               biefs(ib)%gcd = 1
            case ('C','c','0',' ')
               biefs(ib)%gcd = 0
            case default
               biefs(ib)%gcd = 0
         end select

         if (err_track > 0) then
            if (verbose) then
               write(l9,'(a,i3,a)') ' >>>> ERREUR dans fichier NET à la ligne ',nligne,' :'
               write(l9,'(2a)') ' >>>> ',trim(ligne)
               write(l9,'(a,i3,a,i2,a)') ' >>>> il y a ',err_track,' noeud(s) dont le nom a plus de ',lnode,' caractères'
               write(l9,'(a)') ' >>>> Vérifier les lignes suivantes du fichier'
            endif
            stop ' >>>> ERREUR dans fichier NET'
         endif
      endif
   enddo
   !ibmax = ib
   close (luu)
end subroutine lireReseau



subroutine creerNoeuds(biefs, ibmax, nodes, nomax)
!==============================================================================
!       création de la liste des nœuds à partir de la liste des biefs
!       affectation des numéros de nœud dans biefs() en fonction des
!       noms de nœud
!
! entrée : un jeu de biefs
!          le nombre de biefs
! sortie : le jeu de nœuds correspondant
!          les biefs pour faire le lien biefs <-> nœuds
!          le nombre de nœuds
!
!==============================================================================
   ! prototype
   type(bief), intent(inout) :: biefs(*)
   type(node), intent(inout), allocatable :: nodes(:)
   integer, intent(out) :: nomax
   integer, intent(in) :: ibmax
   ! variables locales
   integer :: ib, nn, nbm, nbv

   !on alloue nodes à ibmax+1 faute de pouvoir faire plus précis.
   allocate (nodes(ibmax+1))
   ! calculs
   ! 1er bief qui existe toujours
   nodes(1)%name = biefs(1)%amont  ;  biefs(1)%nam = 1
   nodes(2)%name = biefs(1)%aval  ;  biefs(1)%nav = 2
   nomax = 2
   ! biefs suivants : création des nœuds et affectation des
   !                  numéros de nœuds
   do ib = 2, ibmax
      nbm = 0  ;  nbv = 0
      do nn = 1, nomax
         if (trim(biefs(ib)%amont) == trim(nodes(nn)%name))  nbm = nn
         if (trim(biefs(ib)%aval)  == trim(nodes(nn)%name))  nbv = nn
      enddo
      if (nbm == 0) then
         nomax = nomax+1
         nodes(nomax)%name = biefs(ib)%amont
         biefs(ib)%nam = nomax
      else
         biefs(ib)%nam = nbm
      endif
      if (nbv == 0) then
         nomax = nomax+1
         nodes(nomax)%name = biefs(ib)%aval
         biefs(ib)%nav = nomax
      else
         biefs(ib)%nav = nbv
      endif
   enddo

   do nn = 1, nomax
      !identification des nœuds amont et aval, ceux qui portent les CL
      nodes(nn)%cl = 0  !initialisation
      nbm = 0  ;  nbv = 0
      do ib = 1, ibmax
         if (biefs(ib)%nam == nn) nbm = nbm+1
         if (biefs(ib)%nav == nn) nbv = nbv+1
      enddo
      if (nbm == 0) nodes(nn)%cl = -1
      if (nbv == 0) nodes(nn)%cl = +1
      !initialisation des nœuds comme nœuds à surface nulle
      nodes(nn)%np = 0
      nodes(nn)%ip = 0
   enddo
end subroutine creerNoeuds



subroutine calculerRang (biefs, nodes, net)
!==============================================================================
!            calcul du rang "hydraulique" de chaque bief
!
! entrée : le tableau des biefs
! sortie : le réseau avec les tableaux numero(*) et rang(*), lbamv(*), lbz(*), nzno(*)
!          iba et ibz (maille), nclv
!
!==============================================================================
   ! prototype
   type(bief), intent(in) :: biefs(*)
   type(node), intent(in) :: nodes(*)
   type(rezo), intent(inout) :: net
   ! variables locales
   integer, allocatable :: niveau(:), connexion(:,:)
   integer :: ib, kb, nbb, nivo, iubtmp, ne, jb
   logical :: permute

   !initialisation
   allocate (niveau(net%nb),connexion(net%nb,net%nb))
   !calculs
   !construction de la table de connexion : liste des biefs amont d'un bief donné
   connexion = 0
   do ib = 1, net%nb
      do kb = 1, net%nb
         if (biefs(kb)%nav == biefs(ib)%nam) connexion(kb,ib) = 1
      enddo
   enddo
   !calcul des niveaux hydrauliques
   niveau = 0   !niveau hydraulique => niveau(ib) = max(niveaux biefs amont de ib) +1
   nbb = net%nb  ! nb de biefs sans niveau
   do ib = 1, net%nb
      !if (nodes(biefs(ib)%nam)%cl > 0) then
      if (maxval(connexion(:,ib)) == 0) then
         niveau(ib) = 1
         nbb = nbb-1
      endif
   enddo
   do while (nbb > 0)
      !if(verbose) write(output_unit,*) 'nbb = ',nbb  ;  pause
      do ib = 1, net%nb
         if (niveau(ib) == 0) then
            nivo = 0
            do kb = 1, net%nb
               if (connexion(kb,ib) > 0 .and. niveau(kb) == 0) then
                  nivo = 0
                  exit     !on ne remplit pas un niveau si le niveau inférieur est vide
               else if (connexion(kb,ib) > 0 .and. niveau(kb) > 0) then
                  nivo = max (nivo, niveau(kb))
               endif
            enddo
            if (nivo > 0) then
               niveau(ib) = nivo+1
               !if(verbose) write(output_unit,*) 'niveaux : ',(niveau(kb),kb=1,net%nb)
               nbb = nbb-1
            endif
         !else
         !   if(verbose) write(output_unit,*) 'bief ',ib,' niveau = ',niveau(ib)
         endif
      enddo
   enddo
   if(verbose) write(l9,*) 'calcul des niveaux hydrauliques : OK'
   !

   allocate (net%nivoH(net%nb),net%numero(net%nb))
   net%nivoH(1:net%nb) = niveau(1:net%nb)
   !calcul des rangs hydrauliques
   permute = .true.
   do ib = 1, net%nb
      net%numero(ib) = ib
   enddo
   do while (permute)
      permute = .false.
      !if(verbose) write(output_unit,*) 'permute 1 = ',permute, (net%numero(ib),ib = 1,net%nb),(niveau(net%numero(ib)),ib = 1,net%nb)
      do ib = 2, net%nb
         if (niveau(net%numero(ib)) < niveau(net%numero(ib-1)) .and. net%numero(ib) > net%numero(ib-1)) then
            permute = .true.
            iubtmp = net%numero(ib)
            net%numero(ib) = net%numero(ib-1)
            net%numero(ib-1) = iubtmp
         endif
      enddo
      !if(verbose) write(output_unit,*) 'permute 2 = ',permute, (net%numero(ib),ib = 1,net%nb),(niveau(ib),ib = 1,net%nb)
   enddo
   allocate (net%rang(net%nb))
   do ib = 1, net%nb
      do kb = 1, net%nb
         if (net%numero(kb) == ib) net%rang(ib) = kb
      enddo
   enddo
   if(verbose) write(l9,*) 'calcul des rangs hydrauliques : OK'

   !tableau nzno
   allocate (net%nzno(net%nn))
   net%nzno(1:net%nn) = 0
   do ib = 1, net%nb
      ne = biefs(ib)%nam ; net%nzno(ne) = net%nzno(ne)+1
      ne = biefs(ib)%nav ; net%nzno(ne) = net%nzno(ne)+1
   enddo

   !tableau lbamv
   allocate (net%lbamv(net%nn))
   do ne = 1, net%nn
      if (nodes(ne)%cl > 0) then  !recherche du bief qui part de ne
         do ib = 1, net%nb
            if (biefs(ib)%nam == ne) then
               net%lbamv(ne) = net%rang(ib)
               exit
            endif
         enddo
      else if (nodes(ne)%cl < 0) then  !recherche du bief qui arrive en ne
         do ib = 1, net%nb
            if (biefs(ib)%nav == ne) then
               net%lbamv(ne) = net%rang(ib)
               exit
            endif
         enddo
      else
         net%lbamv(ne) = 0
      endif
   enddo

   !tableau lbz
   allocate (net%lbz(net%nn,2))
   net%lbz(1:net%nn,1:2) = net%nb+1 !initialisation différente de Mage-7
   ! NOTE: les lbz d'un nœud aval valent ibmax+1 (0 dans Mage-7)
   do ne = 1, net%nn
      do ib = 1, net%nb  !boucle sur le rang de calcul
         if (biefs(net%numero(ib))%nam == ne) then
            net%lbz(ne,1) = ib
            exit
         endif
      enddo
      do ib = net%nb, 1, -1  !boucle sur le rang de calcul
         if (biefs(net%numero(ib))%nam == ne) then
            net%lbz(ne,2) = ib
            exit
         endif
      enddo
   enddo

   !recherche du dernier bief à l'amont de la maille = celui qui précède le premier dont le noeud amont est un défluent
   net%iba = net%nb  !on reste à cette valeur s'il n'y a pas de maille (réseau ramifié)
   do ib = 1, net%nb  !boucle sur le rang de calcul
      kb = net%numero(ib)  ;  ne = biefs(kb)%nam
      if (net%lbz(ne,1) /= net%lbz(ne,2)) then
         net%iba = ib-1
         exit
      endif
   enddo
   !recherche du premier bief à l'aval de la maille (après la maille)
   !si plusieurs CL aval => net%ibz = net%nb+1
   net%nclv = 0
   do ne = 1, net%nn
      if (nodes(ne)%cl < 0) net%nclv = net%nclv+1
   enddo
   if (net%nclv > 1 .or. net%iba == net%nb) then
      net%ibz = net%nb+1
   else
      do ib = net%nb, 1, -1  !boucle sur le rang de calcul
         kb = net%numero(ib)  ;  ne = biefs(kb)%nav  ; nbb = 0  ! on compte les biefs qui arrivent en ne
         do jb = 1, net%nb
            if (biefs(jb)%nav == ne) nbb = nbb+1
         enddo
         if (nbb > 1) then
            net%ibz = ib+1
            exit
         endif
      enddo
   endif
   !recherche du premier bief aval de maille (dans la maille)
   do ib = net%iba+1, net%ibz-1
      if (is_bief_aval_maille(ib)) then
         net%iby = ib
         exit
      endif
   enddo

   !correction de net%numero() pour indiquer les biefs de la maille
! NOTE: il faut utiliser is_in_loop(ib) à la place
!   do ib = net%iba+1, net%ibz-1  !boucle sur le rang de calcul
!      net%numero(ib) = -net%numero(ib)
!   enddo

   if (verbose) then
      if (net%iba < net%nb) then
         if (net%ibz <= net%nb) then
            write(l9,*) 'Réseau maillé fermé',net%iba,net%iby,net%ibz
         else
            write(l9,*) 'Réseau maillé ouvert',net%iba,net%iby,net%ibz
         endif
      else
         write(l9,*) 'Réseau ramifié',net%iba,net%iby,net%ibz
      endif
      !Listage des biefs dans l'ordre de calcul (selon leur rang)
      write(l9,*) 'Liste des biefs dans l''ordre de calcul (selon leur rang)'
      write(l9,'(10(1x,a,2x))') (trim(biefs(net%numero(ib))%name), ib=1,net%nb)
   endif
end subroutine calculerRang



subroutine init_from_file_CAS(filename, nodes, nomax)
!==============================================================================
!    initialisation des données à partir de la lecture d'un fichier CAS
!
!            met à jour nodes(*) avec les données de filename
!
! entrée : filename, nom de fichier
!          tableau des noeuds nodes(:)
!          nombre de noeuds nomax
! sortie : nodes(:) mise à jour
!
!==============================================================================
   ! prototype
   character(len=*), intent(in) :: filename
   type(node), intent(inout) :: nodes(*)
   integer, intent(in) :: nomax
   ! variables locales
   integer :: luu, ierr, neu, npneu, next_field, ios
   character :: a1*1, u0*3, a2*80
   real(kind=long) :: u1, u2, dz, ds

   open(newunit=luu,file=filename,status='old',form='formatted',iostat=ierr)
   if (ierr > 0) stop ' >>>> Ouverture du fichier CAS impossible'
   npneu = -1
   do neu = 1, nomax
      rewind luu
      read(luu,'(a1,a3)',iostat=ios) a1, u0
      ! recherche d'une loi S(h) pour le noeud courant
      do while (.not.(a1 == '$'.and. u0 == nodes(neu)%name) .and. ios == 0)
         read(luu,'(a1,a3)',iostat=ios) a1, u0
         npneu = 0  ! initialisation du compteur de niveaux
      enddo
      if (npneu < 0) stop '>>>> BUG dans init_from_file_CAS()'
      if (ios == 0) then
         if (verbose) write(l9,*) '    Lecture de la loi S(h) pour le noeud ',nodes(neu)%name
         ! loi S(h) trouvée => lecture de la loi
         allocate(nodes(neu)%zne(nnsup),nodes(neu)%sne(nnsup),nodes(neu)%vne(nnsup))
         a2 = ' '
         do while (a2(1:1) /= '$')
            read(luu,'(a)', iostat=ios) a2
            if (ios /= 0) then
               exit
            elseif (a2(1:1) == '$') then
               continue
            else if (a2(1:1) /= '*') then
               next_field = 1  ;  ierr = 1
               u1 = next_real(a2,';',next_field) ; ierr = min(ierr,next_field)
               u2 = next_real(a2,';',next_field) ; ierr = min(ierr,next_field)
               if (ierr <= 0) then
                  call erreur_lecture(filename,a2)
               else
                  ierr = 0
                  npneu = npneu+1
                  nodes(neu)%zne(npneu) = u1              ! cote
                  nodes(neu)%sne(npneu) = u2*10000._long  ! surface horizontale en m2
                  if (npneu > 1) then  ! calcul du volume par intégration de la surface
                     dz = nodes(neu)%zne(npneu) - nodes(neu)%zne(npneu-1)
                     ds = nodes(neu)%sne(npneu) + nodes(neu)%sne(npneu-1)
                     nodes(neu)%vne(npneu) = nodes(neu)%vne(npneu-1) + 0.5_long*ds*dz
                  else
                     nodes(neu)%vne(npneu) = 0._long  ! initialisation du volume
                  endif
                  nodes(neu)%np = npneu
                  nodes(neu)%ip = 1
               endif
            endif
         enddo
      endif
   enddo
   close(luu)
end subroutine init_from_file_CAS



function aire_noeud(un_noeud,z)
!==============================================================================
!      renvoie la surface (horizontale) d'un noeud pour une cote donnée
!
! utilise des pointeurs pour éviter l'allocation et la copie d'une courbe2D
!
! entrée :
!           - un_noeud, type noeud
!           - z, réel, la cote à laquelle on veut la surface (horizontale)
! sortie :
!           - la surface (réel) interpolée sur les données brutes du noeud
!
!==============================================================================
   ! prototype
   type(node), intent(in) :: un_noeud
   real(kind=long), intent(in) :: z
   real(kind=long) :: aire_noeud
   ! variables locales
   type(ptr_courbe2D) :: c2D

   ! calculs
   if (un_noeud%np == 0) then
      aire_noeud = zero
   elseif (z < un_noeud%zne(1)) then
     aire_noeud = zero
   else
      c2D = map_courbe2D(un_noeud%zne, un_noeud%sne, un_noeud%np, un_noeud%ip)
      aire_noeud = ptr_interpole(c2D,z)
   endif
   if (aire_noeud < zero) then
      write(error_unit,'(3a)') '>>>> ERREUR : le nœud ',un_noeud%name,' a une surface négative'
      write(error_unit,'(a)')  '              veuillez envoyer un rapport de bug'
      stop 1
   endif
end function aire_noeud



function volume_noeud(un_noeud,z)
!==============================================================================
!      renvoie le volume d'un noeud pour une cote donnée
!
! utilise des pointeurs pour éviter l'allocation et la copie d'une courbe2D
!
! entrée :
!           - un_noeud, type noeud
!           - z, réel, la cote à laquelle on veut le volume
! sortie :
!           - le volume (réel) interpolé sur les données brutes du nœud
!             il ne peut pas être plus grand que le volume max du nœud
!
!==============================================================================
   ! prototype
   type(node), intent(in) :: un_noeud
   real(kind=long), intent(in) :: z
   real(kind=long) :: volume_noeud
   ! variables locales
   type(ptr_courbe2D) :: c2D
   real(kind=long) :: srfc !surface du noeud

   ! calculs
   if (un_noeud%np == 0) then
      volume_noeud = 0._long
   elseif (z < un_noeud%zne(1)) then
     volume_noeud = zero
   else
      c2D = map_courbe2D(un_noeud%zne, un_noeud%sne, un_noeud%np, un_noeud%ip)
      if (z > un_noeud%zne(un_noeud%np)) then
         !il n'y a pas de mur qui prolonge le casier verticalement et permet de stocker indéfiniment
         volume_noeud = un_noeud%vne(un_noeud%np)
      else
         srfc = ptr_interpole(c2D,z)
         volume_noeud = un_noeud%vne(c2D%ip) + 0.5_long*(srfc+un_noeud%sne(c2D%ip))*(z-un_noeud%zne(c2D%ip))
      endif
   endif
   if (volume_noeud < zero) then
      write(error_unit,'(3a)') '>>>> ERREUR : le nœud ',un_noeud%name,' a un volume négatif'
      write(error_unit,'(a)')  '              veuillez envoyer un rapport de bug'
      stop 1
   endif
end function volume_noeud



!function volume_noeud(n,z)
!!==============================================================================
!!   volume du nœud N à la cote Z
!!
!!==============================================================================
!   implicit none
!   ! -- prototype --
!   real(kind=long) :: volume_noeud
!   real(kind=long),intent(in) :: z
!   integer,intent(in) :: n

!   volume_noeud = ptr_volume(la_topo%nodes(n),z)
!end function volume_noeud



function index_by_biefName(name, biefs, ibmax)
!==============================================================================
!      renvoie le numéro (index) dans le tableau biefs[] d'un bief désigné
!      par son nom
!      renvoie 0 si le nom n'existe pas.
!
!
! entrée :
!           - name : chaîne de caractères
!           - biefs : tableau des biefs
!           - ibmax : nombre de biefs
! sortie :
!           - index_by_biefName : entier
!==============================================================================
   ! prototype
   integer :: index_by_biefName
   character(len=*), intent(in) :: name
   type(bief), intent(in) :: biefs(*)
   integer, intent(in) :: ibmax
   ! variables
   integer :: n

   index_by_biefName = 0
   do n = 1, ibmax
      if (trim(biefs(n)%name) == trim(name)) then
         index_by_biefName = n
         exit
      endif
   enddo
end function index_by_biefName


function index_by_nodeName(name, nodes, nomax)
!==============================================================================
!      renvoie le numéro (index) dans le tableau nodes[] d'un noeud désigné
!      par son nom
!      renvoie 0 si le nom n'existe pas.
!
!
! entrée :
!           - name : chaîne de caractères
!           - nodes : tableau des noeuds
!           - nomax : nombre de noeuds
! sortie :
!           - index_by_nodeName : entier
!
!==============================================================================
   ! prototype
   integer :: index_by_nodeName
   character(len=*), intent(in) :: name
   type(node), intent(in) :: nodes(*)
   integer, intent(in) :: nomax
   ! variables
   integer :: n

   index_by_nodeName = 0
   do n = 1, nomax
      if (trim(nodes(n)%name) == trim(name)) then
         index_by_nodeName = n
         exit
      endif
   enddo
end function index_by_nodeName

subroutine addBief(profils, debut, fin, jeuSection, ismax)
!==============================================================================
!        ajoute un bief à la collection jeuSection et met à jour ISmax
!
!entrée : profils ; tableau de type profil : la liste des profils qui constituent
!         le nouveau bief
!         ISmax : la valeur courante
!         jeuSection : collection des profils
!sortie : DEBUT, FIN ; entiers : index de début et de fin de la sous-liste des
!         nouveaux profils dans jeuSection
!         jeuSection(debut) = profils(1) et jeuSection(fin) = profils(size(profils))
!==============================================================================
   ! prototype
   type(profil), intent(in), dimension(:) :: profils
!    class(profil), intent(in), dimension(:), pointer :: profils_ptr
   type(profil), intent(inout) :: jeuSection(:)
   integer, intent(out) :: debut, fin
   integer, intent(inout) :: ismax
   ! variables locales
   integer :: nb_profils, is
   ! procédure

   if (size(profils)>0) then
      nb_profils = size(profils)
      debut = ismax+1  ;  fin = ismax+nb_profils
   else
      write(error_unit,*) "addBief : profils() non associé, appeler newBief en premier"
      stop 6
   endif
   !if (fin > issup) then
   if (debut-fin > size(jeuSection)) then
      write(error_unit,*) "addBief : nombre de profils maxi dépassé : ",fin,size(jeuSection)
      stop 6
   else
      do is = 1, nb_profils
         jeuSection(ismax+is) = profils(is)
      enddo
      ismax = fin
   endif
end subroutine addBief


subroutine compte_profils(STfile,nprof)
! compte le nombre de profil dans le fichier ST
   implicit none
   ! -- prototype --
   character(len=*), intent(in) :: STfile
!    integer, intent(inout) :: luu
   integer, intent(out) :: nprof
   ! -- variables locales --
   integer :: ierr, nligne, nf, luu
   character(len=80) :: ligne
   real(kind=long) :: x, y

   !lecture des données géométrique (STfile)

   nligne = 0
   nprof = 0
   open(newunit=luu,file=trim(STfile),status='old',form='formatted',iostat=ierr)
   if (ierr > 0) then
      write(output_unit,*) '>>>> Ouverture du fichier ST ',trim(STfile),' impossible'
      stop ' Ouverture du fichier ST impossible'
   endif
   do !1ère lecture du fichier : comptage des profils
      read(luu,'(a)',iostat=ierr) ligne
      if (ierr > 0) then
         write(error_unit,'(3a,i0,a)') '>>>> Erreur de lecture de ',trim(STfile),' a la ligne ',nligne, &
                                      ' dans compte_profils()'
         stop 6
      elseif (ierr < 0) then
         exit
      else
         nligne = nligne+1
         if (ligne(1:1) /= '#' .and. ligne(1:1) /= '*') then
            nf = 1  ;  x = next_real(ligne,'',nf)
                       y = next_real(ligne,'',nf)
            if (abs(x-999.999_long)+abs(y-999.999_long) < 0.001_long) nprof = nprof+1
         endif
      endif
   enddo
   close (luu)
end subroutine compte_profils

function section_id_from_pk (all, bief_id, pk, seuil)
! retrouve le numéro de la section se trouvant à l'abscisse pk du bief bief_id)
    ! prototype
   type(dataset), intent(in) :: all
   integer, intent(in) :: bief_id
   real(kind=long), intent(in) :: pk, seuil
   integer :: section_id_from_pk
   ! variables locales
   integer :: ib, kb, is01, is02

   ib = bief_id
   is01 = all%biefs(ib)%is1  ;  is02 = all%biefs(ib)%is2
   if (pk < min(all%sections(is01)%pk,all%sections(is02)%pk)) then
      section_id_from_pk = all%biefs(ib)%is1  ;  return
   elseif (pk > max(all%sections(is01)%pk,all%sections(is02)%pk)) then
      section_id_from_pk = all%biefs(ib)%is2  ;  return
   else
      do kb = is01, is02
         if (abs(all%sections(kb)%pk - pk) < seuil) then
            section_id_from_pk = kb
            return
         endif
      enddo
      write(error_unit,'(a,f9.2,2a)') ' >>>> ERREUR : aucune section ne correspond au pk ',pk,&
                             ' du bief ',all%biefs(ib)%name
      stop 222
   endif
end function section_id_from_pk


function zfond(is)
   !==============================================================================
   !        cote (absolue) du fond de la section is (numéro absolu)
   !==============================================================================
   implicit none
   ! -- le prototytpe --
   real(kind=long) :: zfond
   integer,intent(in) :: is

   zfond = la_topo%sections(is)%zf
end function zfond


function alfn(y,is)
   !==============================================================================
   !   calcul de la largeur au miroir en fonction du tirant d'eau
   !==============================================================================
   implicit none

   ! -- prototype --
   real(kind=long) :: alfn
   integer,intent(in) :: is
   real(kind=long),intent(in) :: y

   alfn = la_topo%sections(is)%largeur(y+la_topo%sections(is)%zf)

end function alfn


! DONE: renommer la routine num_tal()
subroutine num_user(is,kb)
   !==============================================================================
   !   Détermination du numéro de bief et de section dasn l'ordre des données
   !   à partir du numéro de section absolu de MAGE
   !
   !  NB : l'argument IS est modifié par la routine.
   !==============================================================================
   implicit none
   ! -- le prototype --
   integer,intent(inout) :: is
   integer,intent(out) :: kb
   ! -- les variables --
   integer :: ib

   kb = 0
   do ib = 1, la_topo%net%nb
      if (is>=la_topo%biefs(ib)%is1 .and. is<=la_topo%biefs(ib)%is2) then
         is = is-la_topo%biefs(ib)%is1+1
         kb = abs(la_topo%net%numero(ib))
         return
      endif
   enddo
end subroutine num_user


function numero_bief(is)
!==============================================================================
!  Renvoie le numéro du bief (ordre des données de .NET) de la section
!  de numéro IS dans la collection la_topo%sections(:)
!  Renvoie -1 si le numéro de bief n'est pas trouvé
!==============================================================================
   implicit none
   ! -- le prototype --
   integer,intent(in) :: is
   integer :: numero_bief
   ! -- les variables --
   integer :: ib

   do ib = 1, la_topo%net%nb
      if (is >= la_topo%biefs(ib)%is1 .and. is <= la_topo%biefs(ib)%is2) then
         numero_bief = ib
         return
      endif
   enddo
   numero_bief = -1
end function numero_bief


function iSect(ib,x,y)  !numéro
   !==============================================================================
   !   Cette fonction retourne le numero absolu de la section située a
   !       l'abscisse X du bief de numero IB dans .NET
   !   L'argument optionnel Y permet de définir la tolérance de correspondance
   !   des abscisses.
   !   Si Y est absent on cherche une correspondance à 10**-2 près (soit au cm)
   !   Renvoie -1 si le pk x n'est pas dans le bief.
   !
   !==============================================================================
   implicit none
   ! -- le prototype --
   integer :: isect
   integer,intent(in) :: ib
   real(kind=long),intent(in) :: x
   real(kind=long),intent(in), optional :: y
   ! -- les variables --
   integer :: is1, is2

   if (ib <= 0 .OR. ib > la_topo%net%nb) then
      iSect = 0  ;  return
   else
      is1 = la_topo%biefs(ib)%is1  ;  is2 = la_topo%biefs(ib)%is2
   endif

   if (x < min(xgeo(is1),xgeo(is2)) .OR. x > max(xgeo(is1),xgeo(is2))) then
      iSect = -1  ;  return
   elseif (present(y)) then
      iSect = section_id_from_pk (la_topo, ib, x, y)
   else
      iSect = section_id_from_pk (la_topo, ib, x, 1.e-02_long)
   endif

end function ISect


subroutine init_autres_data_geom
!==============================================================================
!   définition des autres données géométriques de section
!==============================================================================
   ! -- variables --
   integer :: is

   do is = 1, la_topo%net%ns
      call la_topo%sections(is)%init_autres_data_geom
   enddo
end subroutine init_autres_data_geom


!function aire_noeud(n,z)
!!==============================================================================
!!   surface du nœud N à la cote Z
!!
!!==============================================================================
!   implicit none
!   ! -- prototype --
!   real(kind=long) :: aire_noeud
!   real(kind=long),intent(in) :: z
!   integer,intent(in) :: n

!   aire_noeud = ptr_surface(la_topo%nodes(n),z)
!end function aire_noeud


pure function xgeo(is)
!==============================================================================
! remplace le tableau XGEO des versions 7 et précédentes
! abscisse en long (pk) de la section courante
!==============================================================================
   implicit none
   ! -- prototype --
   real(kind=long) :: xgeo
   integer, intent(in) :: is

   xgeo = la_topo%sections(is)%pk
end function xgeo


pure function xcentremaille(is)
   implicit none
   ! -- prototype --
   real(kind=long) :: xcentremaille
   integer, intent(in) :: is

   xcentremaille = (la_topo%sections(is)%pk + la_topo%sections(is+1)%pk) * 0.5
end function xcentremaille


pure function zfd(is)
!==============================================================================
! remplace le tableau ZFD des versions 7 et précédentes
! cote du fond de la section courante
!==============================================================================
   implicit none
   ! -- prototype --
   real(kind=long) :: zfd
   integer, intent(in) :: is

   zfd = la_topo%sections(is)%zf
end function zfd


logical function is_in_loop(ib)
!===============================================================================
!   détermine si le bief numéro IB appartient à la maille ou non
!===============================================================================
   implicit none
   ! -- prototype --
   integer,intent(in) :: ib
   ! -- variable --
   integer :: ibb

   ibb = la_topo%net%rang(ib)

   is_in_loop = (ibb > la_topo%net%iba) .AND. (ibb < la_topo%net%ibz)
end function is_in_loop


logical function is_bief_aval_maille(kb)
!===============================================================================
!   détermine si le bief de rang KB est un bief aval de la maille
!   c'est-à-dire si son nœud aval est nœud aval de la maille
!
! NOTE: on pourra envisager de mettre les résultats de cette fonction en cache
!       comme dans Mage-7 où le tableau bool(:) du module Maille donne la même information
!       l'inconvénient de la mise en cache est qu'il faut penser à la refaire s'il
!       nous prend l'envie de permettre de modifier la structure du réseau en cours
!       de simulation.
!===============================================================================
   implicit none
   ! -- prototype --
   integer,intent(in) :: kb
   ! -- variable --
   integer :: ib, neu

   ib = la_topo%net%numero(kb)
   neu = la_topo%biefs(ib)%nav

   ! NOTE: si le bief de rang kb est dans la maille donc le nœud n ne peut pas être nœud amont
   is_bief_aval_maille = is_in_loop(ib) .AND. (la_topo%net%lbamv(neu)==kb)
   !ou bien :
   !  si la maille est fermée il y a un seul nœud aval de maille et un seul bief qui part de ce nœud (ibz)
   !  si la maille est ouverte, les nœuds aval de la maille sont les nœuds aval du réseau (ibz=ibmax+1)
   is_bief_aval_maille = is_in_loop(ib) .AND. (la_topo%net%lbz(neu,1)==la_topo%net%ibz)
end function is_bief_aval_maille


subroutine recherche_biefs_amont
!liste les biefs amont gauche, droit et centre de chaque bief
   ! -- variables --
   integer :: ib, nb, lb

   do ib = 1, la_topo%net%nb
      la_topo%biefs(ib)%ibg = 0
      la_topo%biefs(ib)%ibd = 0
      la_topo%biefs(ib)%ibc = 0
      nb = la_topo%biefs(ib)%nam  !nœud amont du bief courant
      do lb = 1, la_topo%net%nb !parcours des biefs dont le nœud aval est le nœud amont du bief courant
         if (la_topo%biefs(lb)%nav == nb) then
            if (la_topo%biefs(lb)%gcd == -1) then !bief gauche
               if (la_topo%biefs(ib)%ibg == 0) then
                  la_topo%biefs(ib)%ibg = lb
               else
                  write(error_unit,*) ' >>>> ERREUR : bief amont gauche déjà défini pour le bief ',la_topo%biefs(ib)%name
                  stop 6
               endif
            elseif (la_topo%biefs(lb)%gcd == +1) then !bief droit
               if (la_topo%biefs(ib)%ibd == 0) then
                  la_topo%biefs(ib)%ibd = lb
               else
                  write(error_unit,*) ' >>>> ERREUR : bief amont droit déjà défini pour le bief ',la_topo%biefs(ib)%name
                  stop 6
               endif
            elseif (la_topo%biefs(lb)%gcd == 0) then !bief central
               if (la_topo%biefs(ib)%ibc == 0) then
                  la_topo%biefs(ib)%ibc = lb
               else
                  write(error_unit,*) ' >>>> ERREUR : bief amont centre déjà défini pour le bief ',la_topo%biefs(ib)%name
                  stop 6
               endif
            endif
         endif
      enddo
   enddo
end subroutine recherche_biefs_amont


  subroutine lire_Sediments(filename)
  !==============================================================================
  !          Lecture d'un fichier de description des sédiments
  !             des couches sédimentaires pour le charriage
  !
  !   le fichier SED est une liste de lignes de la forme suivante :
  !   pk1 pk2 nb hc(1) d50(1) sigma(1) tau(1) hc(2) d50(2) sigma(2) tau(2) ...
  !   où :
  !     pk1 et pk2 sont les pk de début et de fin du tronçon
  !     nb_couches est le nombre de couches sédimentaires
  !     hc est l'épaisseur de la couche
  !     d50 est le diamètre médian
  !     sigma est l'étendue granulométrique
  !     tau est la contrainte de mise en mouvement
  !     il y a nb_couches groupes {hc d50 sigma tau}
  !
  !    on peut aussi définir une ligne de paramètres par défaut pour définir
  !    une couche sédimentaire où il n'y a pas de tronçon explicite
  !    syntaxe : defaut hc d50 sigma tau
  !
  !   les lignes commençant par * ou vides sont ignorées
  !
  ! NOTE: on a choisi le d50 plutôt que le d90 parce que le d50 est utilisé par PamHyr-RubarBE
  !
  !==============================================================================
    ! -- prototype --
    character(len=*), intent(in) :: filename  !nom du fichier de données
    ! -- variables --
    integer :: luu, ierr, nligne, n, nf, k, nbcs
    character(len=80) :: ligne
    logical :: defaut_defini

    open(newunit=luu, file=trim(filename), status='old', form='formatted',iostat=ierr)
    if (ierr > 0) then
        write(error_unit,'(3a)') ' Ouverture du fichier des couches sédimentaires ', trim(filename),' impossible'
        stop 7
    endif

    nligne = 0
    do
        !1er parcours du fichier pour compter les lignes utiles == nombre de tronçons
        read(luu,'(a)',iostat=ierr) ligne
        if (ierr < 0) exit !fin de fichier
        if (ligne(1:1) == '*' .or. trim(ligne) == '') then
          cycle
        elseif (ligne(1:6) == 'defaut') then  !on ne compte que les vrais tronçons
          cycle
        else
          nligne = nligne+1
        endif
    enddo
    !write(output_unit,*) 'Nb de lignes dans le fichier ',trim(filename),' : ',nligne
    allocate(seg_sed(0:nligne)) !on prévoit la place pour le sédiment par défaut s'il n'est pas fourni
    rewind(luu)
    n = 0  ;  defaut_defini = .false.
    do
        !2e parcours du fichier pour lire les données
        read(luu,'(a)',iostat=ierr) ligne
        if (ierr < 0) exit !fin de fichier
        if (ligne(1:1) == '*' .or. trim(ligne)=='') then
          cycle
        elseif (ligne(1:6) == 'defaut') then
          nf = 7
          seg_sed(0)%nom_bief = ''
          seg_sed(0)%pk_deb = 0._long
          seg_sed(0)%pk_fin = 0._long
          nbcs = next_int(ligne,'',nf)
          seg_sed(0)%nbcs = nbcs
          allocate (seg_sed(0)%cs(nbcs))
          do k = 1, nbcs
              seg_sed(0)%cs(k)%hc = next_real(ligne,'',nf)
              seg_sed(0)%cs(k)%d50 = next_real(ligne,'',nf)
              seg_sed(0)%cs(k)%sigma = next_real(ligne,'',nf)
              seg_sed(0)%cs(k)%tau = next_real(ligne,'',nf)
              if (seg_sed(0)%cs(k)%tau > 1000._long) seg_sed(0)%cs(k)%tau = 1.e+99_long
          enddo
          defaut_defini = .true.
        else
          n = n+1  ;  nf = 1
          seg_sed(n)%nom_bief = next_string(ligne,'',nf)
          seg_sed(n)%pk_deb = next_real(ligne,'',nf)
          seg_sed(n)%pk_fin = next_real(ligne,'',nf)
          nbcs = next_int(ligne,'',nf)
          seg_sed(n)%nbcs = nbcs
          allocate (seg_sed(n)%cs(nbcs))
          do k = 1, nbcs
              seg_sed(n)%cs(k)%hc = next_real(ligne,'',nf)
              seg_sed(n)%cs(k)%d50 = next_real(ligne,'',nf)
              seg_sed(n)%cs(k)%sigma = next_real(ligne,'',nf)
              seg_sed(n)%cs(k)%tau = next_real(ligne,'',nf)
              if (seg_sed(n)%cs(k)%tau > 1000._long) seg_sed(n)%cs(k)%tau = 1.e+99_long
          enddo
        endif
    enddo
    !On vérifie qu'une valeur par défaut a bien été définie, sinon on la définit
    if (.not.defaut_defini) then
        seg_sed(0)%nom_bief = ''
        seg_sed(0)%pk_deb = 0._long
        seg_sed(0)%pk_fin = 0._long
        seg_sed(0)%nbcs = 1
        allocate (seg_sed(0)%cs(1))
        seg_sed(0)%cs(1)%hc = 1000._long
        seg_sed(0)%cs(1)%d50 = 0.001_long
        seg_sed(0)%cs(1)%sigma = 3._long
        seg_sed(0)%cs(1)%tau = -1._long
        write(output_unit,*) '>>>> ATTENTION : il manque la définition de la valeur par défaut dans le fichier SED'
        write(output_unit,*) '                 MAGE a défini les valeurs par défaut suivantes : '
        write(output_unit,'(a,f10.3)') '                 épaisseur = ',seg_sed(0)%cs(1)%hc
        write(output_unit,'(a,f10.3)') '                 d50 = ',seg_sed(0)%cs(1)%d50
        write(output_unit,'(a,f10.3)') '                 étendue granulométrique = ',seg_sed(0)%cs(1)%sigma
        write(output_unit,'(a,f10.3)') '                 contrainte de mise en mouvement = ',seg_sed(0)%cs(1)%tau
    endif

  end subroutine lire_Sediments


subroutine export_ST_final(suffixe)
! export de la géométrie au format ST avec les couches sédimentaires définies point par point
   implicit none
   ! -- prototype --
   character(len=*), intent(in) :: suffixe
   ! -- variables locales --
   real(kind=long), parameter :: xy_end = 999.999_long
   integer :: ib, n, lw, ios, is, nc, i
   integer, parameter :: inull=0
   character(len=lname+6) :: filename
   type(profil), pointer :: prof

   character(len=8) :: ddate
   character(len=10) :: ttime
   character(len=5) :: zone
   integer :: values(8)
   type(point3D), pointer :: pt

   call date_and_time(ddate,ttime,zone,values)


   do ib = 1, la_topo%net%nb
      !export de la géométrie du bief ib
      n = scan(la_topo%biefs(ib)%fichier_geo,'.',back=.true.)
      filename = la_topo%biefs(ib)%fichier_geo(1:n-1)//trim(suffixe)//la_topo%biefs(ib)%fichier_geo(n:)
      open(newunit=lw,file=trim(filename),form='formatted',status='unknown',iostat=ios)
      if (ios > 0) then
         write(error_unit,*) '>>>ERREUR : impossible de créer le fichier ',trim(filename)
         stop 'erreur fatale dans export_ST_final()'
      endif
      write(lw,'(3a,i4.4,5(a1,i2.2))') '# Fichier de géométrie généré par MAGE-8 pour le bief ',trim(la_topo%biefs(ib)%name), &
               ' -- Date simulation : ',values(1),'-',values(2),'-',values(3),' ',values(5),':',values(6),':',values(7)
      do is = la_topo%biefs(ib)%is1, la_topo%biefs(ib)%is2
         prof => la_topo%sections(is)
         !ligne d'entête du profil
         if (with_charriage == 2 .or. with_charriage == 3) then
            write(lw,'(4i6,f13.4,1x,a,1x,i2,1x,*(f10.3,f10.6,2f10.3))') &
            is-la_topo%biefs(ib)%is1+1, inull, inull, prof%np, prof%pk, &
            trim(prof%name), prof%nbcs, (prof%cs(i)%hc, prof%cs(i)%d50, &
            prof%cs(i)%sigma, min(prof%cs(i)%tau,9999._long), i=1,prof%nbcs)
         else
            write(lw,'(4i6,f13.4,1x,a)') is-la_topo%biefs(ib)%is1+1, inull, inull, prof%np, prof%pk, trim(prof%name)
         endif
         !coordonnées x,y,z du profil
         do n = 1, prof%np
            pt => prof%xyz(n)
            nc = pt%nbcs
            if (pt%nbcs == 0)then
                  write(lw,'(2(f12.4,1x),f12.6,1x,a3)') pt%x, pt%y, pt%z, pt%tag
            else
                  write(lw,'(2(f12.4,1x),f12.6,1x,a3,1x,i2,*(f10.3,f10.6,2f10.3))') &
                  pt%x, pt%y, pt%z, pt%tag, pt%nbcs, (pt%cs(i)%zc, pt%cs(i)%d50, pt%cs(i)%sigma, &
                  min(pt%cs(i)%tau,9999._long), i=1,nc)
            endif
         enddo
         write(lw,'(2(f12.4,1x),f12.6)') xy_end, xy_end, zero
      enddo
      close(lw)
   enddo
end subroutine export_ST_final

end module TopoGeometrie
