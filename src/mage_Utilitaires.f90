!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - INRAE - 1989-2020                           #
!# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################
!
module Mage_Utilitaires
!==============================================================================
!     Bibliotheque de sous-programmes utilisateur pour le programme MAGE
!==============================================================================
#ifdef openmp
   use omp_lib
#endif /* openmp */
   use Parametres, only : long, l9, toutpetit, un, zero
   use, intrinsic :: iso_fortran_env, only: output_unit, error_unit, int64, real128
   use,intrinsic :: iso_c_binding

   implicit none

!   character(len=80) :: ligne_tra(1000)  !buffer d'écriture sur TRA pour les ouvrages
!                                         !dont l'écriture est décalée de celles du reste de TRA
!   integer :: ltra_size
   procedure(heure_func), pointer :: heure

   public

     type, bind(c), public :: c_tm
        integer(kind=c_int) :: tm_sec   ! Seconds after minute (0 - 59).
        integer(kind=c_int) :: tm_min   ! Minutes after hour (0 - 59).
        integer(kind=c_int) :: tm_hour  ! Hours since midnight (0 - 23).
        integer(kind=c_int) :: tm_mday  ! Day of month (1 - 31).
        integer(kind=c_int) :: tm_mon   ! Month (0 - 11).
        integer(kind=c_int) :: tm_year  ! Year (current year minus 1900).
        integer(kind=c_int) :: tm_wday  ! Day of week (0 - 6; Sunday = 0).
        integer(kind=c_int) :: tm_yday  ! Day of year (0 - 365).
        integer(kind=c_int) :: tm_isdst ! Positive if daylight saving time is in effect.
     end type c_tm

     integer(c_int), parameter :: rtld_lazy=1 ! value extracte from the C header file
     integer(c_int), parameter :: rtld_now=2 ! value extracte from the C header file

   interface
     function c_difftime(time1, time2) bind(c, name='difftime')
        import :: c_double, c_long
        implicit none
        integer(kind=c_long),value,intent(in) :: time1, time2
        real(kind=c_double)                   :: c_difftime
     end function c_difftime

     function c_gmtime(timeptr) bind(c, name='gmtime')
        import :: c_long, c_ptr
        implicit none
        integer(kind=c_long), intent(in) :: timeptr
        type(c_ptr)                      :: c_gmtime
     end function c_gmtime

     function c_localtime(timeptr) bind(c, name='localtime')
        import :: c_long, c_ptr
        implicit none
        integer(kind=c_long),intent(in) :: timeptr
        type(c_ptr)                     :: c_localtime
     end function c_localtime

     function c_mktime(timeptr) bind(c, name='mktime')
        import :: c_long, c_tm
        implicit none
        type(c_tm),intent(in) :: timeptr
        integer(kind=c_long)  :: c_mktime
     end function c_mktime

     function c_ctime(timeptr) bind(c, name='ctime')
        import :: c_ptr, c_long
        implicit none
        integer(kind=c_long),intent(in) :: timeptr
        type(c_ptr)                     :: c_ctime
     end function c_ctime

     function heure_func(tt,dt_bottom)
        use Parametres, only : long
        implicit none
        character(len=19):: heure_func  !+jjj:hh:mm:ss,cc
        real(kind=long),intent(in) :: tt  !secondes
        real(kind=long),intent(in), optional :: dt_bottom  !secondes, pas de temps minimum
     end function heure_func

#ifdef linux
     function c_dlopen(filename,mode) bind(c,name="dlopen")
         ! void *dlopen(const char *filename, int mode);
         import :: c_char, c_ptr, c_int
         implicit none
         type(c_ptr) :: c_dlopen
         character(c_char), intent(in) :: filename(*)
         integer(c_int), value :: mode
     end function

     function c_dlsym(handle,name) bind(c,name="dlsym")
         ! void *dlsym(void *handle, const char *name);
         import :: c_char, c_ptr, c_funptr
         implicit none
         type(c_funptr) :: c_dlsym
         type(c_ptr), value :: handle
         character(c_char), intent(in) :: name(*)
     end function

     function c_dlclose(handle) bind(c,name="dlclose")
         ! int dlclose(void *handle);
         import :: c_int, c_ptr
         implicit none
         integer(c_int) :: c_dlclose
         type(c_ptr), value :: handle
     end function
#endif
   end interface

   contains

function get_gfortran()
   character(len=11) :: get_gfortran
   integer :: exitstat, i
   do i=11, 9, -1
     write(get_gfortran,'(a9,i0)')"gfortran-",i ! "gfortran-9 "
     call execute_command_line('which '//get_gfortran//" > /dev/null" , .true., exitstat)
     if (exitstat == 0) then
       exit
     endif
   enddo
   write(get_gfortran,'(a9)')"gfortran"
   call execute_command_line('which '//get_gfortran//" > /dev/null" , .true., exitstat)
   if (exitstat == 0) then
     return
   else
      write(l9,'(3a)') '>>> Aucun compilateur gfortran'
      write(error_unit,'(3a)') '>>> Aucun compilateur gfortran'
      stop 199               ! aucun gfortran
   endif
end function get_gfortran

function lire_date(message, tinf, date_format)
   !==============================================================================
   !     lecture d'une date en JJ:HH:MM:SS sur la chaine message
   !     l'utilisation du séparateur permet de ne pas avoir de limite sur la
   !     taille des champs
   !==============================================================================
! TODO gestion des erreurs
   implicit none
   ! prototype
   integer(kind=int64) :: lire_date
   character(len=*),intent(in) :: message
   ! variables locales
   integer :: next_field
   integer(kind=int64) :: j, h, m, s
   character(len=1), parameter :: separateur=':'
   type(c_tm) :: tm
   logical,optional :: date_format
   real(kind=Long),optional :: tinf
   integer :: tri1, ierr

   tm%tm_sec    = 0
   tm%tm_min    = 0
   tm%tm_hour   = 0
   tm%tm_mday   = 0
   tm%tm_mon    = 0
   tm%tm_year   = 0
   tm%tm_wday   = -1
   tm%tm_yday   = -1
   tm%tm_isdst  = -1

   next_field = 1
   tri1 = scan(message,'-') + 1
!   if (scan(message,'/').ne.0) then
   if (tri1 > 1 .and. scan(message(tri1:),'-').ne.0) then
      !s'il y a 2 tirets on suppose qu'on a un format de date ISO
      read(message(1:4),'(i4)',iostat=ierr) tm%tm_year
      tm%tm_year = tm%tm_year-1900
      read(message(6:7),'(i4)',iostat=ierr) tm%tm_mon
      tm%tm_mon = tm%tm_mon-1
      read(message(9:10),'(i4)',iostat=ierr) tm%tm_mday
      read(message(12:13),'(i4)',iostat=ierr) tm%tm_hour
      read(message(15:16),'(i4)',iostat=ierr) tm%tm_min
      read(message(18:19),'(i4)',iostat=ierr) tm%tm_sec
      lire_date = c_mktime(tm)
   else
      if (scan(message,':').ne.0) then
         ! format jjj:hh:mm
         j = int(abs(next_int(message,separateur,next_field)),kind=int64)
         h = int(abs(next_int(message,separateur,next_field)),kind=int64)
         m = int(abs(next_int(message,separateur,next_field)),kind=int64)
         s = 0
         if (next_field .ne. 0) then
            s = int(abs(next_int(message,separateur,next_field)),kind=int64)
         endif
         lire_date = ((j*24_int64 + h) * 60_int64 + m) * 60_int64 + s
         if (scan(message,'-') /= 0) lire_date = -lire_date
      else
         ! juste des minutes
         lire_date=int(60*abs(next_real(message,'',next_field)),kind=int64)
         if (scan(message,'-') /= 0) lire_date = -lire_date
      endif
      if (present(date_format) .and. present(tinf)) then
         if (date_format) then ! tinf was  DD/MM/YY HH:MM:SS
            ! the date is relative to tinf
            lire_date = lire_date + int(tinf,kind=int64)
         endif
      endif
   endif

end function lire_date
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine backup_file(filename)
   !==============================================================================
   !   recopie le fichier texte filename = nom.ext sous un autre nom nom_nnn.ext
   !
   !Historique des modifications
   ! 02/02/2007 : documentation
   !==============================================================================
   implicit none
   ! prototype
   character(len=*),intent(in) :: filename
   ! variables locales
   integer :: next_field, n, istat, k, lu1, lu2
   character(len=1), parameter :: separateur='.'
   character(len=30) :: nom, nombis
   character(len=3) :: ext
   character(len=132) :: ligne
   logical :: bexist

   inquire(file=filename,exist=bexist)
   if (.not.bexist) return

   next_field = 1
   nom = next_string(trim(filename),separateur,next_field)
   ext = next_string(trim(filename),separateur,next_field)

   do n = 0, 999
      if (next_field == len_trim(nom)) then
         write(nombis,'(2a,i3.3)') trim(nom),'_',n
      else
         write(nombis,'(2a,i3.3,2a)') trim(nom),'_',n,'.',trim(ext)
      endif
      inquire(file=nombis,exist=bexist)
      if (.not. bexist) then
         istat = 1 ; k = 0
         do while (istat /= 0)
            open(newunit=lu1,file=filename,status='old',form='formatted',iostat=istat)
            k = k+1
            if (k>1000) then
               write(l9,*) '>>>> Fichier '//filename//' inaccessible'
               write(error_unit,*) '>>>> Fichier '//filename//' inaccessible'
               stop 187   !échec sauvegarde
            endif
         enddo
         open(newunit=lu2,file=nombis,status='new',form='formatted')
         do
            read(lu1,'(a)',iostat=istat) ligne
            if (istat < 0) then  !fin du fichier
               close(lu1)  ;  close(lu2)
               return     !sortie normale
            elseif (istat > 0) then !erreur de lecture
               write(error_unit,*) '>>> Erreur de lecture dans backup_file pour ',trim(filename),' : ',istat
               stop 187
            endif
            write(lu2,'(a)') trim(ligne)
         enddo
      endif
   enddo
   write(l9,'(3a)') '>>> Aucun nom de fichier disponible (> 1000) pour sauvegarder ', &
                         filename,' => supprimer des sauvegardes'
   write(error_unit,'(3a)') '>>> Aucun nom de fichier disponible (> 1000) pour sauvegarder ', &
                    filename,' => supprimer des sauvegardes'
   stop 188               ! trop de sauvegardes deja faites
end subroutine backup_file
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
function next_int(str,separator,next_field)
   !==============================================================================
   !      Renvoie le prochain entier dans la chaîne STR
   !      STR est divisée en champs par des séparateurs ; en plus de la liste
   !      indiquée dans separator, on a toujours au moins l'espace comme séparateur
   !
   !  En entrée next_field est l'index de départ de la recherche
   !  En sortie next_field est l'index du prochain champ à lire
   !
   !  En cas d'erreur la fonction renvoie next_int = 0 et next_field = 0
   !  De la sorte si on appelle une fonction next_<xxx> à la suite en lui
   !  cette valeur de next_field, elle échouera à son tour.
   !
   !==============================================================================
   implicit none
   !---prototype --
   integer :: next_int
   character(*), intent(in) :: str       ! chaîne à scanner
   character(*), intent(in) :: separator ! liste des séparateurs valides en plus de l'espace
   integer, intent(inout) :: next_field  ! index du début du champ suivant
   !---variables locales
   integer :: i, j, k, start, ierr, lt
   character(len=10) :: fmt
   character(len=5) :: full_sep

   start = next_field
   ! début de la recherche avant le 1er caractère -> erreur
   if (start <= 0 .or. start > len_trim(str)) then
      next_int = 0 ; next_field = 0  ;  return !entier non trouvé
   endif
   ! vérification de la liste des séparateurs
   i = scan('0123456789'//'abcdefghijklmnopqrstuvwxyz'//'ABCDEFGHIJKLMNOPQRSTUVWXYZ'//'.&#$§@_%!?*|~',separator)
   if (i > 0) then
      write(error_unit,*) '>>>> ERREUR dans next_int() : liste de séparateurs invalide : '//trim(separator)
      write(error_unit,*) '>>>> Les séparateurs autorisés en plus de l''espace sont , ; :'
      call do_crash('next_int()')
   endif
   ! détection d'un champ vide : 1 séparateur non espace ou un caractère
   !                            non numérique avant caractères numérique
   i = scan(str(start:),'-+0123456789')
   j = scan(str(start:),'abcdefghijklmnopqrstuvwxyz'//'ABCDEFGHIJKLMNOPQRSTUVWXYZ'//'.&#$§@_%!?*|~')
   if (i==0 .OR. (j>0 .and. i>j)) then
      !il n'y a pas de caractère numérique avant le prochain caractère alphabétique ou spécial
      next_int = 0 ; next_field = 0 ; return !entier non trouvé
   else
      i = start-1+i   !index du 1er caractère numérique avant le prochain caractère alphabétique ou spécial
   endif

   j = scan(str(start:),trim(separator))
   if (j>0 .and. start-1+j<i) then
      ! il y a un séparateur avant le 1er caractère numérique -> champ vide == entier nul
      j = start-1+j      !index du 1er séparateur
      next_int = 0  ;  next_field = j+1
   else
      ! cas normal : pas de séparateur non espace avant le 1er caractère numérique
      !k = scan(str(i:),separator//' ')-1  !nombre de caractères précédant le 1er espace ou séparateur
      lt = len_trim(separator)
      if (lt >= 5) then
         stop '>>>> BUG dans Next_Int()'
      else
         full_sep(1:lt) = separator(1:lt) ; full_sep(lt+1:) = repeat(' ',5-lt)
         k = scan(str(i:),full_sep)-1  !nombre de caractères précédant le 1er espace ou séparateur
      endif
      if (k < 0) then
         k = len_trim(str)-i+1 !pas de séparateur -> fin de la chaîne
         j = len_trim(str)+1
      else
         !recherche de la fin du champ : on passe les espaces qui suivent l'entier à lire
         do j=i+k,len_trim(str) !on cherche le 1er caractère non espace
            if (str(j:j)/=' ') exit
         enddo
         if (scan(trim(separator),str(j:j)) >0) j=j+1 !si le 1er caractère non espace est un séparateur, le champ suivant commence après
      endif
      write(fmt,'(a,i1,a)') '(i',k,')'
      next_field = max(i+k,j)
      !lecture de l'entier dans la sous-chaîne str(i:i+k-1)
      read(str(i:i+k-1),fmt,iostat=ierr) next_int
      if (ierr /= 0) then
         write(error_unit,'(4a)') ' >>>> Erreur dans NEXT_INT() en lecture de ',str(i:i+k-1),' dans ',trim(str)
         stop 189
      endif
   endif
end function next_int
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
function next_real(str,separator,next_field)
   !==============================================================================
   !      Renvoie le prochain réel dans la chaîne STR
   !      STR est divisée en champs par des séparateurs ; en plus de la liste
   !      indiquée dans separator, on a toujours au moins l'espace comme séparateur
   !
   !  En entrée next_field est l'index de départ de la recherche
   !  En sortie next_field est l'index du prochain champ à lire
   !
   !  En cas d'erreur la fonction renvoie next_real = 0 et next_field = 0
   !  De la sorte si on appelle une fonction next_<xxx> à la suite en lui
   !  donnant cette valeur de next_field, elle échouera à son tour.
   !==============================================================================
   implicit none
   !---prototype --
   real(kind=long) :: next_real
   character(*), intent(in) :: str       ! chaîne à scanner
   character(*), intent(in) :: separator ! liste des séparateurs valides en plus de l'espace
   integer, intent(inout) :: next_field  ! index du début du champ suivant c'est à dire après le séparateur
   !---variables locales
   integer :: i, j, k, start, ierr, lt
   character(len=10) :: fmt
   character(len=5) :: full_sep

   start = next_field
   ! début de la recherche avant le 1er caractère -> erreur
   if (start <= 0 .or. start > len_trim(str)) then
      !print*,' >>>> Next_Real() : non trouvé 1 : ',trim(str(start:)),' # ',trim(str)
      next_real = zero ; next_field = 0  ;  return !réel non trouvé
   endif
   ! vérification de la liste des séparateurs
   i = scan('0123456789'//'abcdefghijklmnopqrstuvwxyz'//'ABCDEFGHIJKLMNOPQRSTUVWXYZ'//'.&#$§@_%!?*|~',separator)
   if (i > 0) then
      write(error_unit,*) '>>>> ERREUR dans next_real() : liste de séparateurs invalide : '//trim(separator)
      write(error_unit,*) '>>>> Les séparateurs autorisés en plus de l''espace sont , ; :'
      call do_crash('next_real()')
   endif
   ! détection d'un champ vide : 1 séparateur non espace ou un caractère
   !                            non numérique avant caractères numérique
   i = scan(str(start:),'-+.0123456789')
   j = scan(str(start:),'abcdefghijklmnopqrstuvwxyz'//'ABCDEFGHIJKLMNOPQRSTUVWXYZ'//'&#$§@_%!?*|~')
   if (i==0 .OR. (j>0 .and. i>j)) then
      !il n'y a pas de caractère numérique avant le prochain caractère alphabétique ou spécial
      !print*,' >>>> Next_Real() : non trouvé 2 : ',trim(str(start:)),' # ',trim(str)
      next_real = zero ; next_field = 0  ;  return !réel non trouvé
   else
      i = start-1+i   !index du 1er caractère numérique avant le prochain caractère alphabétique ou spécial
   endif

   j = scan(str(start:),separator)
   if (j>0 .and. start-1+j<i) then
      ! il y a un séparateur avant le 1er caractère numérique -> champ vide == réel nul
      j = start-1+j      !index du 1er séparateur
      !print*,' >>>> Next_Real() : champ vide : ',trim(str(start:)),' # ',trim(str)
      next_real = zero  ;  next_field = j+1
   else
      ! cas normal : pas de séparateur non espace avant le 1er caractère numérique
      !k = scan(str(i:),separator//' ')-1  !nombre de caractères précédant le 1er espace ou séparateur
      lt = len_trim(separator)
      if (lt >= 5) then
         stop '>>>> BUG dans Next_Real()'
      else
         full_sep(1:lt) = separator(1:lt) ; full_sep(lt+1:) = repeat(' ',5-lt)
         k = scan(str(i:),full_sep)-1  !nombre de caractères précédant le 1er espace ou séparateur
         if (k == -1) k = len_trim(str(i:)) !dernier champ de la chaîne
         write(fmt,'(a,i2.2,a)') '(f',k,'.0)'
      endif
      !recherche de la fin du champ : on passe les espaces qui suivent le réel à lire
      do j=i+k,len_trim(str) !on cherche le 1er caractère non espace
         if (str(j:j) /= ' ') exit
      enddo
      if (scan(separator,str(j:j)) >0) j=j+1 !si le 1er caractère non espace est un séparateur, le champ suivant commence après
      next_field = max(i+k,j)
      !lecture du réel dans la sous-chaîne str(i:i+k-1)
      read(str(i:i+k-1),fmt,iostat=ierr) next_real
      if (ierr /= 0) then
         write(error_unit,'(4a)') ' >>>> Erreur dans NEXT_REAL() en lecture de ',str(i:i+k-1),' dans ',trim(str)
         stop 190
      endif
      !print*,' >>>> Next_Real() réussi : ',next_real,'#',trim(str(start:)),' # ',trim(str)
   endif
end function next_real
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
function next_string(str,separator,next_field)
   !==============================================================================
   !   Renvoie la prochaine sous-chaîne dans la chaîne STR
   !
   !  STR est divisée en champs par des séparateurs ; en plus de la liste
   !  indiquée dans separator, on a toujours au moins l'espace comme séparateur
   !
   !  En entrée next_field est l'index de départ de la recherche
   !  En sortie next_field est l'index du prochain champ à lire
   !
   !  En cas d'erreur la fonction renvoie next_string = ' ' et next_field = 0
   !  De la sorte si on appelle une fonction next_<xxx> à la suite en lui
   !  cette valeur de next_field, elle échouera à son tour.
   !==============================================================================
   implicit none
   !---prototype --
   character(len=:), allocatable :: next_string
   character(len=*), intent(in) :: str       ! chaîne à scanner
   character(len=*), intent(in) :: separator ! liste des séparateurs valides
   integer, intent(inout) :: next_field  ! index du début du champ suivant c'est à dire après le séparateur
   !---variables locales
   integer :: i, j, k, start, lt
   character(len=5) :: full_sep = repeat(' ',5)

   i = len_trim(str)
   allocate(character(len=i) :: next_string)

   start = next_field
   ! cas pathologiques
   if (start > len_trim(str)) then
      next_string = ' '  ;  next_field = start  ;  return
   else if (start == len_trim(str)) then
      next_string = str(start:)  ;  next_field = start  ;  return
   else if (start == 0) then
      next_string = ' ' ;  next_field = 0  ;  return
   else
      next_string = ' '
   endif
   ! vérification de la liste des séparateurs
   i = scan('0123456789'//'abcdefghijklmnopqrstuvwxyz'//'ABCDEFGHIJKLMNOPQRSTUVWXYZ'//'&#$§@_%!?*|~',separator)
   if (i > 0) then
      write(error_unit,*) '>>>> ERREUR dans next_string() : liste de séparateurs invalide : '//trim(separator)
      write(error_unit,*) '>>>> Les séparateurs autorisés en plus de l''espace sont . , ; :'
      call do_crash('next_string()')
   endif
   ! détection d'un champ vide : 1 séparateur non espace avant caractère alphanumérique ou spécial
   i = scan(str(start:),trim(separator))  !recherche du 1er séparateur
   j = scan(str(start:),'0123456789'//'abcdefghijklmnopqrstuvwxyz'//'ABCDEFGHIJKLMNOPQRSTUVWXYZ'//'&#$§@_%!?*|~')
   if (i>0 .and. i<j) then
      !il n'y a pas de caractère alphabétique ou spécial avant le prochain séparateur
      next_string = ' ' ; next_field = i+1  ;  return ! champ vide
   endif
   lt = len_trim(separator)
   full_sep(1:lt) = separator(1:lt) ; full_sep(lt+1:lt+2) = ' '
   i = scan(str(start:),full_sep)
   if (i == 0) then  !on est à la fin de la chaîne
      ! ni séparateur ni espace -> on prend tout le reste de la chaîne
      next_field = len_trim(str)
      next_string = str(start:)
      return
   endif
   ! recherche du 1er caractère admissible
   i = scan(str(start:),'0123456789'//'abcdefghijklmnopqrstuvwxyz'//'ABCDEFGHIJKLMNOPQRSTUVWXYZ'//'.&#$§@+-_%!?')
   if (i>0) then
      i = start-1+i   !index du 1er caractère admissible
   else
      next_string = ' ' ; next_field = start ; return ! string non trouvée
   endif
   ! recherche de la fin du champ
   j = scan(str(start:),trim(separator))
   if (j>0 .and. start-1+j<i) then
      ! il y a un séparateur avant le 1er caractère admissible -> champ vide
      j = start-1+j      !index du 1er séparateur
      next_string = ' '  ;  next_field = j+1
   else
      ! cas normal : pas de séparateur non espace avant le 1er caractère admissible
      lt = len_trim(separator)
      if (lt >= 5) then
         stop '>>>> BUG dans Next_String()'
      else
         full_sep(1:lt) = separator(1:lt) ; full_sep(lt+1:) = repeat(' ',5-lt)
         k = scan(str(i:),full_sep)-1  !nombre de caractères précédant le 1er espace ou séparateur
      endif
      ! recherche de la fin du champ : 1er caractère non espace après la fin du champ
      do j=i+k,len_trim(str) !on cherche le 1er caractère non espace
         if (str(j:j)/=' ') exit
      enddo
      if (scan(separator,str(j:j)) >0) j=j+1 !si le caractère cherché est un séparateur, le champ suivant commence après
      next_field = max(i+k,j)
      ! récupération de la sous-chaîne
      next_string = str(i:i+k-1)
   endif
end function next_string
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
function zegal(a,b,c)
   !==============================================================================
   !           Test de la pseudo égalité de a et b devant c
   !                 Le test est fait à 10**-5 près
   !==============================================================================
   implicit none
   ! -- le prototype --
   logical :: zegal
   real(kind=long),intent(in) :: a,b,c

   zegal = abs(a-b)<=toutpetit*abs(c)

end function zegal
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! TODO: reprendre les appels de HEURE() quand dtmin < 1.
! subroutine heure(tt,hms,dt_bottom)
!    !==============================================================================
!    !        écriture de l'heure sous le format hh:mm:ss
!    !  input  : tt et dt_bottom en secondes
!    !  output : hms en format +jjj:hh:mm:ss,cc
!    !
!    !==============================================================================
!    implicit none
!    ! -- le prototype --
!    real(kind=long),intent(in) :: tt  !secondes
!    character(len=17),intent(out) :: hms  !+jjj:hh:mm:ss,cc
!    real(kind=long),intent(in), optional :: dt_bottom  !secondes, pas de temps minimum
!    ! -- les variables --
! !   integer, parameter :: quad = kind(1.Q0)
!    integer, parameter :: quad = real128
!    character(len=3) :: ccent
!    character(len=1) :: signe
!    integer :: icent,ijj,ith,itmin,itsec
!    real(kind=quad) :: th,t,tmin,tsec,dtmin
!    character(len=3), dimension(0:6) :: days=(/'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'/)
!    type(c_ptr) :: ptr
!    type(c_tm),pointer::tm
!
!    if (date .and. .not. steady) then
!        ptr = c_gmtime(int(floor(tt), kind=long))
!        call c_f_pointer(ptr, tm)
!        write(hms,'(i2.2,5(a1,i2.2))')tm%tm_mday,'/',tm%tm_mon+1,'/',tm%tm_year,' ',tm%tm_hour,':',tm%tm_min,':',tm%tm_sec
!    else
!       if (tt < 0._long) then
!          signe = '-'
!       else
!          signe = ' '
!       endif
!       if (present(dt_bottom)) then
!          dtmin = real(dt_bottom,kind=quad)
!       else
!          dtmin = 1._quad
!       endif
!       t = real(abs(tt),kind=quad)
!       ijj = iabs(floor(t/86400._quad))
!       th = t-real(ijj,kind=quad)*86400._quad
!       ith = floor(th/3600._quad)
!       tmin = th-real(ith,kind=quad)*3600._quad
!       itmin = floor(tmin/60._quad)
!       tsec = tmin-real(itmin,kind=quad)*60._quad
!       itsec = floor(tsec)
!       !parfois on trouve quand même itsec = 60
!       if (itsec == 60) then
!          itmin = itmin+1
!          itsec = 0
!       endif
!       icent = nint((t-real(floor(t),kind=quad))*100._quad)
!       if (icent > 99) then
!          if (dtmin < 1._quad) then
!             ccent = ',00'
!          else
!             ccent = '   '
!          endif
!          itsec = itsec+1
!       elseif (icent > 0) then
!          write(ccent,'(a,i2.2)') ',',icent
!       else
!          if (dtmin < 1._quad) then
!             ccent = ',00'
!          else
!             ccent = '   '
!          endif
!       endif
!       if (ijj < 100) then  !on affiche les centiemes de seconde
!          write(hms,'(a1,i2.2,3(a1,i2.2),a3)') signe,ijj,':',ith,':',itmin,':',itsec,ccent
!       elseif (ijj<1000) then
!          write(hms,'(a1,i3.2,3(a1,i2.2),a2)') signe,ijj,':',ith,':',itmin,':',itsec,'  '
!       elseif (ijj<10000) then
!          write(hms,'(a1,i4.2,3(a1,i2.2),a1)') signe,ijj,':',ith,':',itmin,':',itsec,' '
!       else
!          write(hms,'(a1,i5.2,3(a1,i2.2))') signe,ijj,':',ith,':',itmin,':',itsec
!       endif
!    endif ! date
!
! end subroutine heure

function heure1(tt,dt_bottom)
   ! -- le prototype --
   character(len=19) :: heure1  !+jjj:hh:mm:ss,cc
   real(kind=long),intent(in) :: tt  !secondes
   real(kind=long),intent(in), optional :: dt_bottom  !secondes, pas de temps minimum
   ! -- les variables --
!   integer, parameter :: quad = kind(1.Q0)
   integer, parameter :: quad = real128
   character(len=3) :: ccent
   character(len=1) :: signe
   integer :: icent,ijj,ith,itmin,itsec
   real(kind=quad) :: th,t,tmin,tsec,dtmin
   if (tt < 0._long) then
      signe = '-'
   else
      signe = ' '
   endif
   if (present(dt_bottom)) then
      dtmin = real(dt_bottom,kind=quad)
   else
      dtmin = 1._quad
   endif
   t = real(abs(tt),kind=quad)
   ijj = iabs(floor(t/86400._quad))
   th = t-real(ijj,kind=quad)*86400._quad
   ith = floor(th/3600._quad)
   tmin = th-real(ith,kind=quad)*3600._quad
   itmin = floor(tmin/60._quad)
   tsec = tmin-real(itmin,kind=quad)*60._quad
   itsec = floor(tsec)
   !parfois on trouve quand même itsec = 60
   if (itsec == 60) then
      itmin = itmin+1
      itsec = 0
   endif
   icent = nint((t-real(floor(t),kind=quad))*100._quad)
   if (icent > 99) then
      if (dtmin < 1._quad) then
         ccent = ',00'
      else
         ccent = '   '
      endif
      itsec = itsec+1
   elseif (icent > 0) then
      write(ccent,'(a,i2.2)') ',',icent
   else
      if (dtmin < 1._quad) then
         ccent = ',00'
      else
         ccent = '   '
      endif
   endif
   if (ijj < 100) then  !on affiche les centiemes de seconde
      write(heure1,'(a1,i2.2,3(a1,i2.2),a3)') signe,ijj,':',ith,':',itmin,':',itsec,ccent
   elseif (ijj<1000) then
      write(heure1,'(a1,i3.2,3(a1,i2.2),a2)') signe,ijj,':',ith,':',itmin,':',itsec,'  '
   elseif (ijj<10000) then
      write(heure1,'(a1,i4.2,3(a1,i2.2),a1)') signe,ijj,':',ith,':',itmin,':',itsec,' '
   else
      write(heure1,'(a1,i5.2,3(a1,i2.2))') signe,ijj,':',ith,':',itmin,':',itsec
   endif
end function heure1

function heure2(tt,dt_bottom)
   ! -- le prototype --
   character(len=19) :: heure2  !+jjj:hh:mm:ss,cc
   real(kind=long),intent(in) :: tt  !secondes
   real(kind=long),intent(in), optional :: dt_bottom  !secondes, pas de temps minimum
   ! -- les variables --
   type(c_tm) :: tm
   tm = get_tm(tt)
!   write(heure2,'(i2.2,5(a1,i2.2))')tm%tm_mday,'/',tm%tm_mon+1,'/',tm%tm_year,' ',tm%tm_hour,':',tm%tm_min,':',tm%tm_sec
   write(heure2,'(i4.4,5(a1,i2.2))')tm%tm_year+1900,'-',tm%tm_mon+1,'-',tm%tm_mday,' ', &
                                    tm%tm_hour,':',tm%tm_min,':',tm%tm_sec
end function heure2

function get_hour(tt) ! heures depuis minuit
   ! -- le prototype --
   integer :: get_hour
   real(kind=long),intent(in) :: tt  !secondes
   ! -- les variables --
   type(c_tm) :: tm
   tm = get_tm(tt)
   get_hour = tm%tm_hour
end function get_hour

function get_yday(tt) ! jour de l'année
   ! -- le prototype --
   integer :: get_yday
   real(kind=long),intent(in) :: tt  !secondes
   ! -- les variables --
   type(c_tm) :: tm
   tm = get_tm(tt)
   get_yday = tm%tm_yday
end function get_yday

function get_mday(tt) ! jour du mois
   ! -- le prototype --
   integer :: get_mday
   real(kind=long),intent(in) :: tt  !secondes
   ! -- les variables --
   type(c_tm) :: tm
   tm = get_tm(tt)
   get_mday = tm%tm_mday
end function get_mday

function get_wday(tt) ! jour de la semaine
   ! -- le prototype --
   integer :: get_wday
   real(kind=long),intent(in) :: tt  !secondes
   ! -- les variables --
   type(c_tm) :: tm
   tm = get_tm(tt)
   get_wday = tm%tm_wday
end function get_wday

function get_month(tt) ! mois de l'année
   ! -- le prototype --
   integer :: get_month
   real(kind=long),intent(in) :: tt  !secondes
   ! -- les variables --
   type(c_tm) :: tm
   tm = get_tm(tt)
   get_month = tm%tm_mon+1
end function get_month

function get_time_from_date(yy,mm,dd,hh,mn,ss)
   ! -- le prototype --
   real(kind=long) :: get_time_from_date  !secondes
   integer,intent(in) :: yy,mm,dd
   integer,intent(in),optional :: hh,mn,ss
   integer :: h, m, s
   type(c_tm) :: tm
   if(present(hh))then
      h = hh
   else
      h = 0
   endif
   if(present(mn))then
      m = mn
   else
      m = 0
   endif
   if(present(ss))then
      s = ss
   else
      s = 0
   endif

   tm%tm_year = yy-1900
   tm%tm_mon = mm-1
   tm%tm_mday = dd
   tm%tm_hour = h
   tm%tm_min = m
   tm%tm_sec = s
   get_time_from_date = c_mktime(tm)
end function get_time_from_date

function get_tm(tt)
   ! -- le prototype --
   type(c_tm) :: get_tm
   real(kind=long),intent(in) :: tt  !secondes
   ! -- les variables --
   type(c_ptr) :: ptr
   type(c_tm),pointer::tm
!   ptr = c_gmtime(int(floor(tt), kind=c_long))
   ptr = c_localtime(int(floor(tt), kind=c_long))
   call c_f_pointer(ptr, tm)
   get_tm = tm
end function get_tm

subroutine get_t1t2_annual(tt, tm1, tm2, t1, t2)
   ! -- le prototype --
   logical :: is_between_annual
   real(kind=long),intent(in) :: tt  !secondes
   type(c_tm),intent(in) :: tm1, tm2
   real(kind=long),intent(out) :: t1, t2  !secondes
   ! -- les variables --
   type(c_tm) :: tm, tm1_tmp, tm2_tmp
   real(kind=long) :: t_tmp

   tm = get_tm(tt)
   tm1_tmp = tm1
   tm2_tmp = tm2
   t1 = c_mktime(tm1_tmp)
   t2 = c_mktime(tm2_tmp)
   if (t2 < t1) then
      if(tt > t1) then
         tm2_tmp%tm_hour = tm2_tmp%tm_year+1
         t2 = c_mktime(tm2_tmp)
      else if(tt < t2) then
         tm1_tmp%tm_hour = tm1_tmp%tm_year-1
         t1 = c_mktime(tm1_tmp)
      else
         t_tmp = t1
         t1 = t2
         t2 = t_tmp
      endif
   endif
end subroutine get_t1t2_annual

subroutine get_t1t2_monthly(tt, tm1, tm2, t1, t2)
   ! -- le prototype --
   logical :: is_between_monthly
   real(kind=long),intent(in) :: tt  !secondes
   type(c_tm),intent(in) :: tm1, tm2
   real(kind=long),intent(out) :: t1, t2  !secondes
   ! -- les variables --
   type(c_tm) :: tm, tm1_tmp, tm2_tmp
   real(kind=long) :: t_tmp

   tm = get_tm(tt)
   tm1_tmp = tm1
   tm2_tmp = tm2
   t1 = c_mktime(tm1_tmp)
   t2 = c_mktime(tm2_tmp)
   if (t2 < t1) then
      if(tt > t1) then
         tm2_tmp%tm_hour = tm2_tmp%tm_mon+1
         t2 = c_mktime(tm2_tmp)
      else if(tt < t2) then
         tm1_tmp%tm_hour = tm1_tmp%tm_mon-1
         t1 = c_mktime(tm1_tmp)
      else
         t_tmp = t1
         t1 = t2
         t2 = t_tmp
      endif
   endif
end subroutine get_t1t2_monthly

subroutine get_t1t2_weekly(tt, tm1, tm2, t1, t2)
   ! -- le prototype --
   logical :: is_between_weekly
   real(kind=long),intent(in) :: tt  !secondes
   type(c_tm),intent(in) :: tm1, tm2
   real(kind=long),intent(out) :: t1, t2  !secondes
   ! -- les variables --
   type(c_tm) :: tm, tm1_tmp, tm2_tmp
   real(kind=long) :: t_tmp

   tm = get_tm(tt)
   tm1_tmp = tm1
   tm2_tmp = tm2
   t1 = c_mktime(tm1_tmp)
   t2 = c_mktime(tm2_tmp)
   if (t2 < t1) then
      if(tt > t1) then
         tm2_tmp%tm_hour = tm2_tmp%tm_mday+7
         t2 = c_mktime(tm2_tmp)
      else if(tt < t2) then
         tm1_tmp%tm_hour = tm1_tmp%tm_mday-7
         t1 = c_mktime(tm1_tmp)
      else
         t_tmp = t1
         t1 = t2
         t2 = t_tmp
      endif
   endif
end subroutine get_t1t2_weekly

subroutine get_t1t2_daily(tt, tm1, tm2, t1, t2)
   ! -- le prototype --
   logical :: is_between_daily
   real(kind=long),intent(in) :: tt  !secondes
   type(c_tm),intent(in) :: tm1, tm2
   real(kind=long),intent(out) :: t1, t2  !secondes
   ! -- les variables --
   type(c_tm) :: tm, tm1_tmp, tm2_tmp
   real(kind=long) :: t_tmp

   tm = get_tm(tt)
   tm1_tmp = tm1
   tm2_tmp = tm2
   t1 = c_mktime(tm1_tmp)
   t2 = c_mktime(tm2_tmp)
   if (t2 < t1) then
      if(tt > t1) then
         tm2_tmp%tm_hour = tm2_tmp%tm_mday+1
         t2 = c_mktime(tm2_tmp)
      else if(tt < t2) then
         tm1_tmp%tm_hour = tm1_tmp%tm_mday-1
         t1 = c_mktime(tm1_tmp)
      else
         t_tmp = t1
         t1 = t2
         t2 = t_tmp
      endif
   endif
end subroutine get_t1t2_daily

subroutine get_t1t2_hourly(tt, tm1, tm2, t1, t2)
   ! -- le prototype --
   real(kind=long),intent(in) :: tt  !secondes
   type(c_tm),intent(in) :: tm1, tm2
   real(kind=long),intent(out) :: t1, t2  !secondes
   ! -- les variables --
   type(c_tm) :: tm, tm1_tmp, tm2_tmp
   real(kind=long) :: t_tmp

   tm = get_tm(tt)
   tm1_tmp = tm1
   tm2_tmp = tm2
   t1 = c_mktime(tm1_tmp)
   t2 = c_mktime(tm2_tmp)
   if (t2 < t1) then
      if(tt > t1) then
         tm2_tmp%tm_hour = tm2_tmp%tm_hour+1
         t2 = c_mktime(tm2_tmp)
      else if(tt < t2) then
         tm1_tmp%tm_hour = tm1_tmp%tm_hour-1
         t1 = c_mktime(tm1_tmp)
      else
         t_tmp = t1
         t1 = t2
         t2 = t_tmp
      endif
   endif
end subroutine get_t1t2_hourly

!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! function c_heure(tt)
!    implicit none
!    ! -- le prototype --
!    real(kind=long),intent(in) :: tt  !secondes
!    character(len=17) :: c_heure  !+jjjjj:hh:mm:ss
!    ! -- les variables --
!    character(len=19) :: hms  !+jjjjj:hh:mm:ss
!
!
!    call heure(tt,hms)
!    c_heure = hms
! end function c_heure
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine LU_factorisation(a,n,np,indx,id)
   !==============================================================================
   !        Décomposition LU de la matrice A : algorithme de CROUT
   !               routine adaptée de Numerical Recipes
   !
   ! A : matrice de dimension utile N*N et de dimension physique NP*NP
   ! INDX : tableau de dimension N (comptabilise les permutations de lignes de A)
   ! ID : indicateur de singularité (0) ou parité du nombre de permutations (1)
   !
   !==============================================================================
   implicit none
   ! -- le prototype --
   integer,intent(in) :: n,np
   real(kind=long),intent(inout) :: a(np,np)
   integer, intent(out) :: id,indx(*)
   ! -- les constantes --
   real(kind=long),parameter :: eps = 1.0e-20_long
   ! -- les variables --
   integer :: i,j,k,imax
   real(kind=long) :: valeur_max,tmp,somme,vv(n)

   if (n < 1) then
      write(error_unit,*) ' >>>> Erreur dans LU_factorisation : la taille de la matrice est négative !'
#ifndef debug
      write(error_unit,*) '      Probablement un bug -> recompiler en mode debug et relancer le programme'
#endif /* debug */
#ifdef debug
      call do_crash('LU_factorisation') !on fait planter Mage pour forcer la backtrace
#endif /* debug */
      stop 198
   endif
   id = 0 ; imax = -1
   do i = 1,n
      valeur_max = maxval(abs(a(i,1:n)))
      if (is_zero(valeur_max)) then
         id = i ; return !matrice singulière (ligne nulle) : on sort
      endif
      vv(i) = un/valeur_max
   enddo
   do j = 1,n
      do i = 1,j-1
         somme = a(i,j) - dot_product(a(i,1:i-1),a(1:i-1,j))
         a(i,j) = somme
      enddo
      valeur_max = zero
      do i = j,n
         somme = a(i,j) - dot_product(a(i,1:j-1),a(1:j-1,j))
         a(i,j) = somme
         tmp = vv(i)*abs(somme)
         if (tmp >= valeur_max) then
            imax = i
            valeur_max = tmp
         endif
      enddo
      if (j /= imax) then
         do k = 1,n
            tmp = a(imax,k)
            a(imax,k) = a(j,k)
            a(j,k) = tmp
         enddo
         id = 0
         vv(imax) = vv(j)
      endif
      indx(j) = imax
      if (j /= n) then
         if (abs(a(j,j)) < eps) then
            id = j ; return !matrice singulière : on sort
         endif
         tmp = un/a(j,j)
         a(j+1:n,j) = a(j+1:n,j) * tmp
      endif
   enddo
   if (abs(a(n,n)) < eps) id = n !matrice singulière : on sort

end subroutine LU_factorisation
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine LU_solution(a,n,np,indx,b,mode)
   !==============================================================================
   !        Résolution du système A*X=B pour A sous forme LU
   !               routine adaptée de Numerical Recipes
   !
   ! A : matrice sous forme LU de dimension utile N*N et de dimension physique NP*NP
   ! INDX : tableau de dimension N (comptabilise les permutations de lignes de A)
   ! B : tableau de dimension N contient le second membre en entrée et la
   !     solution en sortie
   ! mode : si non-nul on ne résout que U*X=b
   !
   !==============================================================================
   implicit none
   ! -- le prototype --
   integer,intent(in) :: n,np,indx(np)
   real(kind=long),intent(inout) :: a(np,np),b(np)
   integer,intent(in),optional :: mode
   ! -- les variables --
   integer :: i,ii,ll,mode2
   real(kind=long) :: somme

   mode2 = 0 ; if (present(mode)) mode2 = mode

   if (mode2 == 0) then  !si mode /= 0 alors on résoud seulement U*X=b
   !résolution de L*y = b, la solution est dans b
      ii = 0
      do i = 1,n
         ll = indx(i)
         somme = b(ll)
         b(ll) = b(i)
         if (ii /= 0) then
            somme = somme - dot_product(a(i,ii:i-1),b(ii:i-1))
         elseif (abs(somme) > zero) then
            ii = i
         endif
         b(i) = somme
      enddo
   endif
   !---résolution de U*x  =  y, la solution est dans b
   do i = n,1,-1
      somme = b(i) - dot_product(a(i,i+1:n),b(i+1:n))
      b(i) = somme/a(i,i)
   enddo
end subroutine LU_solution
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
function sqrt2(x)
   !==============================================================================
   !   version de sqrt adoucie au voisinage de 0 par un polynome de degrés 2
   !   tangent à sqrt() en alpha et de pente finie en 0.
   !
   !==============================================================================
   implicit none
   !prototype
   real(kind=long), intent(in) :: x
   real(kind=long) :: sqrt2
   !variables locales
   real(kind=long), parameter :: alpha=0.005_long
   character(len=180) :: err_message

   if (x > alpha) then
      sqrt2 = sqrt(x)
   elseif (x < zero) then
      write(err_message,*) ' >>>> Erreur dans SQRT2() : argument négatif => ',x
      write(l9,'(a)') trim(err_message)
      write(error_unit,'(a)') trim(err_message)
      stop 197
   else
      sqrt2 = x * (3._long -x/alpha) / (2._long*sqrt(alpha))
   endif
end function sqrt2
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine capitalize(in, out)
   !==============================================================================
   !   Met en capitales la chaîne de caractères fournie en entrée
   !==============================================================================
   implicit none
   ! prototype
   character(len=*), intent(in) :: in
   character(len=*), intent(out) :: out
   ! variables locales
   integer :: offset, ic, i

   out = in
   offset = ichar('A') - ichar('a')
   do i = 1, len_trim(in)
      ic = ichar(in(i:i))
      if (ic >= ichar('a') .and. ic <= ichar('z')) then
         out(i:i) = char(ic+offset)
      endif
   enddo
end subroutine capitalize


!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! Routines utilitaires reprises d'Adis-TS pour les modules Topologie
!  et Geometrie_Section
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


logical function between(z, z1, z2)
   !==============================================================================
   !               Teste si un réel est compris entre 2 autres
   ! renvoie vrai si z est compris entre z1 et z2
   ! Ne fait pas d'hypothèse sur l'ordre de z1 et z2
   !
   ! entrées : z1 et z2 réels
   ! usage : bool = between (z,z1,z2)
   ! historique :
   !     - 14/03/2008 : création du commentaire d'entête
   !==============================================================================
   ! prototype
   real(kind=long), intent(in) :: z, z1, z2

   between = z > min(z1,z2) .and. z < max(z1,z2)

end function between
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine ecr0(Chain,Vol0,K)
!==============================================================================
!
!                                      12345678901234
!   sous-programme d'écriture d'un entier 99999999999 sous la forme
!                                      99 999 999 999
!==============================================================================
   implicit none
   ! -- le prototype --
   character(len=14),intent(inout) :: chain
   real(kind=long),intent(in) :: vol0
   integer,intent(out) :: k
   ! -- les variables --
   integer :: l
   real(kind=long) :: vol
   character(len=14) :: chain1
   character(len=13) :: bl
   !----------------------------------------------------------------------------
   bl = repeat(' ',13)
   !si vol0 > 100 E+06 on compte en millions
   if (vol0 > 1.e+08_long .or. vol0 < -1.e+07_long) then
      vol = vol0 * 1.e-06_long
      chain1 = '!'//bl
      chain  = '!'//bl
   else
      vol = vol0
      chain1 = ' '
      chain  = ' '
   endif
   write(chain1(4:),'(i11)') nint(vol)
   write(chain(4:),'(i11)') nint(vol)
   k = 15
   do l = 14, 4, -1
      if (chain1(l:l)==' ') then
         exit
      else if (l==11 .or. l==8 .or. l==5) then
         k = k-1
         chain(k:k) = ' '
         k = k-1
         chain(k:k) = chain1(l:l)
      else
         k = k-1
         chain(k:k) = chain1(l:l)
      endif
   enddo
end subroutine Ecr0


subroutine do_crash(from)
!  provoque une division par zéro pour obtenir un back-trace
!  à utiliser pour identifier l'arbre d'appel d'une procédure qui échoue.

   !prototype
   character(len=*),intent(in) :: from  !nom de la procédure appelante
   !variables
   integer :: nap=0

   nap = nap+1
   write(error_unit,'(2a)') ' >>>> Do_Crash : appel par ',trim(from)
   flush(unit=l9)
   write(error_unit,*) ' Do_Crash : ',1/(nap-1)
   stop 9
end subroutine do_crash


function mean(x,y)
! calcul de la moyenne de 2 nombres
   !prototype
   real(kind=long),intent(in) :: x, y
   real(kind=long) :: mean

   mean = min(x,y) + 0.5_long*abs(x-y)
end function mean


subroutine init_random_seed()
   implicit none
   integer, allocatable :: seed(:)
   integer :: i, n, dt(8), pid
   integer(kind=int64) :: t

   call random_seed(size = n)
   allocate(seed(n))

   call system_clock(t)
   if (t == 0) then
      call date_and_time(values=dt)
      t = int(dt(1) - 1970,kind=int64) * 365_int64 * 24_int64 * 3600_int64 * 1000_int64 &
           + int(dt(2),kind=int64) * 31_int64 * 24_int64 * 3600_int64 * 1000_int64 &
           + int(dt(3),kind=int64) * 24_int64 * 3600_int64 * 1000_int64 &
           + int(dt(5),kind=int64) * 3600_int64 * 1000_int64 &
           + int(dt(6),kind=int64) * 60_int64 * 1000_int64 + int(dt(7),kind=int64) * 1000_int64 &
           + int(dt(8),kind=int64)
   end if
   ! NOTE: extension Gfortran : getpid()
   pid = getpid()
   t = ieor(t, int(pid, kind(t)))
   do i = 1, n
      seed(i) = lcg(t)
   end do
   call random_seed(put=seed)
end subroutine init_random_seed


function lcg(s)
! This simple PRNG might not be good enough for real work, but is
! sufficient for seeding a better PRNG.
   implicit none
   integer :: lcg
   integer(kind=int64), intent(in) :: s
   integer(kind=int64) :: ss
   if (s == 0) then
     ss = 104729
   else
     ss = mod(s, 4294967296_int64)
   end if
   ss = mod(ss * 279470273_int64, 4294967291_int64)
   lcg = int(mod(ss, int(huge(0), int64)), kind(0))
end function lcg


logical function is_NaN(x)
! renvoie vrai si x est NaN
   implicit none
   real(kind=long), intent(in) :: x

   if (x <= zero .or. x > zero) then
      is_NaN = .false.
   else
      is_NaN = .true.
   endif
end function is_NaN


subroutine sgefa_c_omp (a, n, ipvt, info, nb_cpu)

!*****************************************************************************80
!
!! SGEFA_C_OMP factors a matrix by gaussian elimination.
!
!  Discussion:
!
!    This variant of SGEFA uses OpenMP for improved parallel execution.
!
!    The step in which multiples of the pivot row are added to individual
!    rows has been replaced by a single call which updates the entire
!    matrix sub-block.
!
!  Licensing:
!
!    This code is distributed under the GNU LGPL license.
!
!  Modified:
!
!    27 April 2008
!    2016 - 2017 by JBF
!
!  Author:
!
!    John Burkardt
!
!  Reference:
!
!    Jack Dongarra, Jim Bunch, Cleve Moler, Pete Stewart,
!    LINPACK User's Guide,
!    SIAM, 1979,
!    ISBN13: 978-0-898711-72-1,
!    LC: QA214.L56.
!
!  Parameters:
!
!    Input/output, real (kind = long) A(N,N).  On input, the matrix to be
!    factored.  On output, an upper triangular matrix and the multipliers which
!    were used to obtain it.  The factorization can be written A = L * U where
!    L is a product of permutation and unit lower triangular matrices and
!    U is upper triangular.
!
!    Input, integer :: N, the order of the matrix.
!
!    Output, integer :: IPVT(N), the pivot indices.
!
!    Output, integer :: INFO, indicates singularity.
!    If 0, this is the normal value, and the algorithm succeeded.
!    If K, then on the K-th elimination step, a zero pivot was encountered.
!    The matrix is numerically not invertible.
!
   implicit none
! -- Prototype --
   integer, intent(in) :: n
   integer, intent(inout) :: info
   integer, intent(inout) :: ipvt(n)
   real (kind = long), intent(inout) :: a(n,n)
   integer, intent(in) :: nb_cpu
   ! -- Variables locales
   integer :: j, k, l
   real (kind = long) :: t, akk
   real (kind=long),parameter :: almost_zero = 2._long*tiny(zero)

!  Gaussian elimination with partial pivoting.
   info = 0
   do k = 1, n - 1
!
!  Find the pivot index L.
      l = k - 1 + maxloc(abs(a(k:n,k)),dim=1)
      ipvt(k) = l
      !if (a(l,k) == zero) then
      if (abs(a(l,k)) < almost_zero) then
         info = k
         write(error_unit,'(1x,a,i0,a,i0,a,i0,a,e12.6)') &
              '>>>> pivot nul dans sgefa_c_omp() à l''étape ',k,'/',n,' : ligne ',l,' valeur : ',a(l,k)
         return
      end if

!$omp parallel shared (a, k, n, akk) private (j, t) num_threads(nb_cpu)
!  Interchange rows K and L.
      if (l /= k) then
!$omp do schedule(auto)
         do j = k, n
            t = a(l,j)
            a(l,j) = a(k,j)
            a(k,j) = t
         enddo
!$omp end do
      end if
!  Compute column K of the lower triangular factor.
      akk = 1._long / a(k,k)
!$omp do schedule(auto)
      do j = k+1, n
         a(j,k) = -a(j,k) * akk
      enddo
!$omp end do

!  Add multiples of the pivot row to the remaining rows.
!$omp do schedule(auto)
      do j = k+1, n
         a(k+1:n,j) = a(k+1:n,j) + a(k+1:n,k) * a(k,j)
      end do
!$omp end do
!$omp end parallel
   end do
   ipvt(n) = n
   !if (a(n,n) == zero) then
   if (abs(a(n,n)) < almost_zero) then
      write(error_unit,'(1x,a,i0,a,i0,a,i0,a,e12.6)') &
           '>>>> pivot nul dans sgefa_c_omp() à l''étape ',n,'/',n,' : ligne ',n,' valeur : ',a(n,n)
      info = n
   end if
end subroutine sgefa_c_omp


subroutine sgesl (a, n, ipvt, b)

!*****************************************************************************80
!
!! SGESL solves a real general linear system A * X = B.
!
!  Discussion:
!
!    SGESL can solve either of the systems A * X = B or A' * X = B.
!
!    The system matrix must have been factored by SGECO or SGEFA.
!
!    A division by zero will occur if the input factor contains a
!    zero on the diagonal.  Technically this indicates singularity
!    but it is often caused by improper arguments or improper
!    setting of LDA.  It will not occur if the subroutines are
!    called correctly and if SGECO has set 0.0 < RCOND
!    or SGEFA has set INFO == 0.
!
!  Modified:
!
!    13 May 2007
!
!  Author:
!
!    Original FORTRAN77 version by Dongarra, Bunch, Moler, Stewart.
!    FORTRAN90 version by John Burkardt.
!
!  Reference:
!
!    Jack Dongarra, Jim Bunch, Cleve Moler, Pete Stewart,
!    LINPACK User's Guide,
!    SIAM, 1979,
!    ISBN13: 978-0-898711-72-1,
!    LC: QA214.L56.
!
!  Parameters:
!
!    Input, real (kind = long) A(N,N), the output from SGECO or SGEFA.
!
!    Input, integer :: N, the order of the matrix A.
!
!    Input, integer :: IPVT(N), the pivot vector from SGECO or SGEFA.
!
!    Input/output, real (kind = long) B(N).
!    On input, the right hand side vector.
!    On output, the solution vector.
!
   implicit none
   ! -- prototype --
   integer, intent(in) :: n, ipvt(n)
   real (kind = long), intent(in) :: a(n,n)
   real (kind = long), intent(inout) :: b(n)
   ! -- variables locales --
   integer :: k, l
   real (kind = long) t
!
!  Solve A * X = B.
   do k = 1, n - 1
      l = ipvt(k)
      t = b(l)
      if (l /= k) then
         b(l) = b(k)
         b(k) = t
      end if
      b(k+1:n) = b(k+1:n) + t * a(k+1:n,k)
   end do

   do k = n, 1, -1
      b(k) = b(k) / a(k,k)
      t = - b(k)
      b(1:k-1) = b(1:k-1) + t * a(1:k-1,k)
   end do
end subroutine sgesl


subroutine produit_matrice_inverse(A,B,C,X,Y,n)
   !calcule C = A^-1*B et Y = A^-1*X
   !où A, B et C sont des matrices n x n
   ! -- prototype --
   integer, intent(in) :: n
   real(kind=long),intent(in),dimension(n,n) :: A, B
   real(kind=long),intent(out),dimension(n,n) :: C
   real(kind=long),intent(in),dimension(n) :: X
   real(kind=long),intent(out),dimension(n) :: Y
   ! -- variables --
   integer :: id, k, j
   integer,dimension(n) :: indx
   real(kind=long),dimension(n,n) :: AA

   !étape n°1 : factorisation LU de A (en fait de sa copie AA)
   AA = A
   call LU_factorisation(AA,n,n,indx,id)
   if (id /= 0) then
      write(error_unit,*) ' >>>> Matrice singulière dans produit_matrice_inverse()'
      write(error_unit,*) '      Rang de la matrice : ',n
      if (n < 11) then
         do k = 1, n
            write(error_unit,'(10g14.6)') (a(k,j),j=1,n),(b(k,j),j=1,n),x(k)
         enddo
      endif
      !call do_crash('produit_matrice_inverse')
      stop 9
   endif
   !étape n°2 : résolution pour chacun des vecteurs colonne de B
   !            copie préalable dans C pour ne pas modifier B
   C = B
   do k = 1, n
      call LU_solution(AA,n,n,indx,C(:,k))
   enddo
   !étape n°3 : résolution pour le vecteur X
   !            copie préalable dans Y pour ne pas modifier X
   Y = X
   call LU_solution(AA,n,n,indx,Y)
end subroutine produit_matrice_inverse


subroutine permut_4123(E,F)
!permute les lignes de la matrice 4*4 E et du vecteur F dans l'ordre 4,1,2,3
   ! -- prototype --
   real(kind=long),intent(inout),dimension(4,4) :: E
   real(kind=long),intent(inout),dimension(4) :: F
   ! -- variables --
   real(kind=long),dimension(4) :: E4
   real(kind=long) :: F4
   integer :: k

   E4 = E(4,:)
   F4 = F(4)
   do k = 4, 2, -1
      E(k,:) = E(k-1,:) ; F(k) = F(k-1)
   enddo
   E(1,:) = E4 ; F(1) = F4
end subroutine permut_4123


subroutine permut_2341(A,B,C)
!permute les lignes des matrices 4*4 A et B et du vecteur C dans l'ordre 2,3,4,1
   ! -- prototype --
   real(kind=long),intent(inout),dimension(4,4) :: A,B
   real(kind=long),intent(inout),dimension(4) :: C
   ! -- variables --
   real(kind=long),dimension(4) :: A2, B2
   real(kind=long) :: C2

   A2 = A(1,:)  ;  B2= B(1,:)  ;  C2 = C(1)
   A(1,:) = A(2,:)  ;  B(1,:) = B(2,:)  ;  C(1) = C(2)
   A(2,:) = A(3,:)  ;  B(2,:) = B(3,:)  ;  C(2) = C(3)
   A(3,:) = A(4,:)  ;  B(3,:) = B(4,:)  ;  C(3) = C(4)
   A(4,:) = A2      ;  B(4,:) = B2      ;  C(4) = C2
end subroutine permut_2341


subroutine solve_systlin(A,b,n,restore_matrice)
!résout le système linéaire A*X=b de taille n
! NB : on suppose que la taille utile de la matrice est la même que sa taille physique
!      sinon ça ne marche pas avec LU_factorisation()
!la solution est dans b
   ! -- prototype --
   integer, intent(in) :: n
   real(kind=long),intent(inout),dimension(n,n) :: A
   real(kind=long),intent(inout),dimension(n) :: b
   logical,intent(in),optional :: restore_matrice
   ! -- variables --
   integer :: info, i, j
   integer,dimension(n) :: indx
   real(kind=long),dimension(n,n) :: A_copy
   logical :: bRestore

   bRestore = .false. ; if (present(restore_matrice)) bRestore = restore_matrice
   if (bRestore) then
      A_copy = A(1:n,1:n) !copie de A car on ne veut pas modifier A
   endif
   call LU_factorisation(A,n,n,indx,info)
   if (info /= 0) then
      write(error_unit,*) ' >>>> Matrice singulière dans solve_systlin()'
      write(error_unit,*) '      Rang de la matrice : ',n,' Pivot nul à la ligne ',info
      if (n < 11) then
         write(error_unit,*) '      matrice triangularisée'
         do i = 1, n
            write(error_unit,'(10e25.16)') (A(i,j),j=1,n)
         enddo
         if (bRestore) then
            write(error_unit,*) '      matrice originale'
            do i = 1, n
               write(error_unit,'(10e25.16)') (A_copy(i,j),j=1,n)
            enddo
         endif
      endif
      !call do_crash('solve_systlin')
      stop 9
   endif
   call LU_solution(A(1:n,1:n),n,n,indx(1:n),B)
   if (bRestore) then
      A(1:n,1:n) = A_copy(1:n,1:n)
   endif
end subroutine solve_systlin


logical function is_diagonal_dominant(A,n)
   implicit none
   ! -- Prototype --
   integer, intent(in) :: n
   real (kind = long), intent(inout) :: a(n,n)
   ! -- Variables --
   integer :: i

   is_diagonal_dominant = .true.
   do i = 1, n
      is_diagonal_dominant = is_diagonal_dominant .and. ( abs(a(i,i)) > sum(abs(a(i,1:i-1))) + sum(abs(a(i,i+1:n))) )
   enddo
end function is_diagonal_dominant


subroutine simplification_lineaire(alpha,x, y, npt, fact)
   implicit none
   ! prototype
   integer,intent(inout) :: npt
   real(kind=long),intent(in) :: alpha !hauteur minimale du triangle constitué par 3 points successifs
                                       !pour garder le point intermédiaire
   real(kind=long),intent(in) :: fact  !facteur d'échelle sur les abscisses. Par exemple on fait la simplification
                                       !sur des heures alors que les données sont des secondes
   real(kind=long),intent(inout) :: x(:), y(:)
   ! variables locales
   integer :: i, j, k, n, n_sup
   real(kind=long) :: htmin, ht, xi, xj, xk

   htmin = 1.e+30_long
   do
      i = 1  ;  j = 2  ;  k = 3  ;  n = 1  ;  n_sup = 0
      do while (k <= npt)
         xi = x(i)*fact ; xj = x(j)*fact ; xk = x(k)*fact
         ht = hauteur_triangle(xi,y(i),xj,y(j),xk,y(k))
         htmin = min(htmin,ht)
         if (ht < alpha) then
            ! on oublie le point j
            j = k  ;  k = k+1  ;  n_sup = n_sup+1
         else
            ! on garde le point j
            n = n+1
            if (n > j) stop ">>>> Erreur dans simplification_lineaire()"
            x(n) = x(j)  ;  y(n) = y(j)
            i = j  ;  j = j+1  ;  k = k+1
         endif
      enddo
      n = n+1
      x(n) = x(npt)  ;  y(n) = y(npt)
      write(output_unit,'(a,i0,a,i0,2x,i0,a)') ' Nombre de points à lisser : ',npt,' -- Gain : ',n_sup, n_sup*100/npt,'%'
      if (n_sup*100 < npt) then
         npt = n  ;  exit
      else
         npt = n
      endif
   enddo
   write(output_unit,'(a,g0)') ' Hauteur triangle minimale : ',htmin
   write(output_unit,'(a,i0)') ' Nombre de points conservés : ',npt
end subroutine simplification_lineaire


function hauteur_triangle(x1,y1,x2,y2,x3,y3)
   implicit none
!   integer,parameter :: q4=kind(1.0q0)
   integer,parameter :: q4=real128
   ! prototype
   real(kind=long) :: hauteur_triangle
   real(kind=long),intent(in) :: x1,y1,x2,y2,x3,y3
   ! variables locales
   real(kind=q4) :: a, b, c, x14, y14, x24, y24, x34, y34

   x14 = real(x1,kind=q4)  ;  y14 = real(y1,kind=q4)
   x24 = real(x2,kind=q4)  ;  y24 = real(y2,kind=q4)
   x34 = real(x3,kind=q4)  ;  y34 = real(y3,kind=q4)

   a = sqrt((x24-x14)**2+(y14-y24)**2)
   b = sqrt((x34-x24)**2+(y24-y34)**2)
   c = sqrt((x34-x14)**2+(y14-y34)**2)
   hauteur_triangle = real((a+b)/c,kind=long) - 1._long
end function hauteur_triangle


subroutine block_tridiag (B,A,C,D,X,n,p)
! résolution d'un système linéaire tridiagonal par blocs
! taille des blocs = p x p
! nombre de blocs = n
! matrice : B, A, C (diagonale = A)
! second membre : D
! solution : X
! -- prototype --
   integer, intent(in) :: n, p
   real(kind=long), intent(in) :: B(p,p,n), A(p,p,n), C(p,p,n)
   real(kind=long), intent(inout) :: D(p,n) ! second membre
   real(kind=long), intent(out) :: X(p,n)   ! solution
! -- variables locales --
   real(kind=long) :: AA(p,p), CC(p,p,n), Y(p,n), DD(p)
   integer :: i

   ! descente
   AA = A(:,:,1)
   call produit_matrice_inverse(AA,C(:,:,1),CC(:,:,1),D(:,1),Y(:,1),p)
   do i = 2, n
      AA = A(:,:,i) - matmul(B(:,:,i),CC(:,:,i-1))
      DD = D(:,i) - matmul(B(:,:,i),Y(:,i-1))
      call produit_matrice_inverse(AA,C(:,:,i),CC(:,:,i),DD,Y(:,i),p)
   enddo

   ! remontée
   X(:,n) = Y(:,n)
   do i = n-1, 1, -1
      X(:,i) = Y(:,i) - matmul(CC(:,:,i),X(:,i+1))
   enddo
end subroutine block_tridiag


logical function is_zero(x)
   real(kind=long), intent(in) :: x

   is_zero = abs(x) < 1.e-29_long

end function is_zero


pure function f0p(x,p)
! hack tordu pour forcer l'écriture du zéro avant le séparateur décimal pour un format f0.p
   implicit none
   ! -- prototype --
   real(kind=long), intent(in) :: x
   integer, intent(in) :: p
   character(len=20) :: f0p
   ! -- variable --
   character(len=18) :: tmp
   character(len=6) :: myfmt

   write(myfmt,'(a4,i1,a1)') '(f0.',p,')'
   write(tmp,fmt=myfmt) x
   if (tmp(1:1) == '.') then
      f0p = '0'//tmp(1:)
   elseif (tmp(1:2) == '-.') then
      f0p = '-0'//tmp(2:)
   else
      f0p = tmp(1:)
   endif
end function f0p

end module Mage_Utilitaires
