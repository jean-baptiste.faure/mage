subroutine IniGeo(IS)  !initialisation de la géométrie
!==============================================================================
!   initialisation de GEOM_COURANTE
!==============================================================================
   use parametres, only: long
   use geom_courante, only: yk=>yk0,alk=>alk0,sk=>sk0,pk=>pk0,yk1, alk1,sk1,pk1,irec
   use geometrie_section, only: ibs, ybmin, ymoy, ybmax, zfd, point_geo 
   use hydraulique, only: zt, yt
   implicit none
   ! -- Prototype --
   integer,intent(in) :: is
   ! -- Variables locales temporaires --
   integer :: k,kisa,kisb,kisy
   real(kind=long) :: y

   !yt(is) = tirant d'eau dans la section courante
   !recherche des cotes données encadrant yt(is) ----> indice k
   !indices des points-données de début et de fin de la section courante
   kisa = ibs(is-1)+1
   kisb = kisa+1
   kisy = ibs(is)-1
   yt(is)  = zt(is)-zfd(is)
   yk1(is) = point_geo(kisb)%y ; alk1(is) = point_geo(kisb)%al
   sk1(is) = point_geo(kisb)%s ; pk1(is)  = point_geo(kisb)%p
   if (yt(is) < yk1(is)) then
      !--->yt(is) < premier tirant d'eau donne
      yk(is)   = point_geo(kisa)%y ; alk(is) = point_geo(kisa)%al
      sk(is)   = point_geo(kisa)%s ; pk(is)  = point_geo(kisa)%p
      irec(is) = kisa
      return
   end if
   if ( yt(is) > ybmin(is) ) then  !ybmin(is) =  tirant d'eau maximal de la section
      yk(is)   = point_geo(kisy)%y ;   alk(is)  = point_geo(kisy)%al
      sk(is)   = point_geo(kisy)%s ;   pk(is)   = point_geo(kisy)%p
      yk1(is)  = point_geo(kisy+1)%y ; alk1(is) = point_geo(kisy+1)%al
      sk1(is)  = point_geo(kisy+1)%s ; pk1(is)  = point_geo(kisy+1)%p
      irec(is) = kisy
      return
   endif
   do k = kisb, kisy
      yk(is)  = yk1(is)
      alk(is) = alk1(is)
      sk(is)  = sk1(is)
      pk(is)  = pk1(is)
      yk1(is) = point_geo(k+1)%y ; alk1(is) = point_geo(k+1)%al
      sk1(is) = point_geo(k+1)%s ; pk1(is)  = point_geo(k+1)%p
      if (yt(is) < yk1(is)) then
         irec(is) = k
         return
      endif
   enddo
end subroutine IniGeo

subroutine NormaliseCotes
!==============================================================================
!                       Normalisation des cotes
!             on soustrait la cote du fond la plus basse du modèle
!==============================================================================
   use parametres, only: long, zero
   use data_num_fixes, only: zmin
   use dim_reelles, only: ismax, nsmax
   use geometrie_section, only: zfd, ybmin, ymoy, ybmax
   use singularites, only: uv, zfs
   implicit none

! TODO: à refaire car zfd() n'est plus un tableau
   zmin = minval( zfd(1:ismax) )
   zmin = max(zmin,zero)

   !---Cote du fond normalisée pour les sections régulières
! NOTE: attention ici zfd() n'est pas un tableau
   zfd(1:ismax) = zfd(1:ismax)-zmin

   !---Cote du fond normalisée pour les sections singulières
   if (nsmax /= 0) then
      zfs(1:nsmax) = zfs(1:nsmax)-zmin
   end if
end subroutine NormaliseCotes


subroutine Lire_MIN
!==============================================================================
!                      Lecture du fichier .MIN
!==============================================================================
      use parametres, only: long, zero, un, cent, issup, sp
      use parametres_tal, only: idsup => zidmax, zip
      use conditions_limites, only: nc,lbamv
      use dim_reelles, only: ismax,ibmax,nomax
      use geometrie_noeud, only: tc
      use geometrie_section, only: ibs,kd, xgeo, is1a=>is1,is2a=>is2,   &
                                   ybmin,ymoy,ybmax, zfd, almy,smy,pmy, &
                                   ybgau,ybdro,point_geo
      use hydraulique, only: iba,ibz,na=>nam,nz=>nav,lbz,iub,ibu
      use singularites, only: iss
      use IO_Files, only: lu, boolf, nom
      use mage_utilitaires
      use data_num_fixes, only : silent
      implicit none
! -- Variables --
      integer :: ib,ific0,indic,is,isa,isb
      integer :: j,jbs(issup),jbr(issup),jsa,jsz
      integer :: k,k0,k1,k2
      integer :: l,lsc(100),lsd,lu3,lu8
      integer :: n,n030,n033,n034,nerr37,npage
      real(kind=long) :: alge, fic, pge, sge, xx1l,xx2l,xx3l,xx4l
      real(kind=sp),dimension(idsup) :: xx1(idsup),xx2(idsup),xx3(idsup),xx4(idsup)   ! toujours en simple précision
      integer, parameter :: izsup = zip*issup
      real(kind=long),dimension(idsup) :: xx1d(izsup),xx2d(izsup),xx3d(izsup),xx4d(izsup)
      real(kind=long) :: yge,ygeo
      logical :: ouvert
      integer :: ierr

      lu8 = lu(1)   !fichier .MIN
      lu3 = lTra  !fichier listing

      k0 = 0
      npage = 1
      ierr = 0
      do while (ierr == 0)
         read(lu8,iostat=ierr) lsd,(xx1(l),l=1,lsd)
         lsc(npage) = lsd
         if (ierr < 0) exit
         read(lu8) lsd,(xx2(l),l=1,lsc(npage))
         read(lu8) lsd,(xx3(l),l=1,lsc(npage))
         read(lu8) lsd,(xx4(l),l=1,lsc(npage))
         do l = 1, lsc(npage)
            k = k0+l
            xx1d(k) = xx1(l)  ! nécessaire quand MAGE est en double précision
            xx2d(k) = xx2(l)  ! car MIN est toujours en simple précision
            xx3d(k) = xx3(l)
            xx4d(k) = xx4(l)
         enddo
         k0 = k0 + lsc(npage)
         npage = npage+1
      enddo
      npage = npage-1  !Nombre total de page_mémoire
      
!---remplissage du tableau point_geo() des tableaux xgeo, ibs et kd
      jsa = 1
      jsz = 0  !indice dans xx1d du dernier point de la section courante
      k = 1
      k0 = 0   !indice dans xx1d du dernier point de page precedente
      ib = ibmax
      is = is2a(ib)+1
      do n = 1, npage
         k1 = k0+lsc(n)  !indice dans xx1d du dernier point de page courante
         indic = 1
 210     is = is-1
!--->kd(is)=indice de position dans la page en cours du nombre de points
!           de la section precedente + 1 = indice dans la page n du premier
!           point de la section en cours
         if (indic/=1) then  !on est dans une page
            kd(is) = nint(xx1d(k))+1
         else               !on commence une page
            kd(is) = 1  ;  indic = 0
         end if
         k = jsa
         jsz = k0+nint(xx1d(jsa))
         if (is>=is1a(ib)) then
            continue
         else if (ib>=2) then
            ib = ib-1
            is = is2a(ib)
         else if (ib==1) then
            exit
         else
            write(lu3,'(a)') ' >>>> Erreur de lecture de MIN'
            write(0,'(a)') ' >>>> Erreur de lecture de MIN'
            stop 135
         end if
         jbr(is) = jsa  !indice dans xx1d du premier point de la section courante
         jbs(is) = jsz  !indice dans xx du dernier point de la section courante
         jsa = jsz+1  !indice dans xx du premier point de la section suivant la section courante
         if (jsz<k1) goto 210

         k0 = k1  !on change de page
      enddo

      ific0 = 0
      k0 = 1
      do is = 1, ismax
         k1 = jbr(is)+1
         k2 = jbs(is)
         do k = k1, k2
            alge = xx1d(k) ; yge = xx2d(k)
            sge  = xx3d(k) ; pge = xx4d(k)
            if (k==k1) then
               zfd(is) = yge
               yge = zero
            endif
            if (k0 > zip*issup) then
               write(lu3,*) ' Erreur dans Lire_MIN'
               write(lu3,'(a)') ' Merci d''envoyer un rapport de bug'
               write(0,*) ' Erreur dans Lire_MIN'
               write(0,'(a)') ' Merci d''envoyer un rapport de bug'
               stop 136
            endif
            point_geo(k0)%y = yge  ;  point_geo(k0)%al = alge
            point_geo(k0)%s = sge  ;  point_geo(k0)%p = pge
            k0 = k0+1
         enddo
         k1 = jbr(is)
         ybmin(is) = xx2d(k1) ; xgeo(is) = xx3d(k1) ; pge = xx4d(k1)
         kd(is) = ibs(is-1)+nint(pge)-kd(is)
         ibs(is) = k0-1
      enddo

      do is = 1,ismax
         almy(is) = point_geo(kd(is))%al
         smy(is) = point_geo(kd(is))%s
         pmy(is) = point_geo(kd(is))%p
      enddo

!---Tirant d'eau de plein bord, tirant d'eau de la berge la plus basse
!   et tirant d'eau de débordement mineur-moyen
      do is = 1, ismax
         ybmax(is) = point_geo(ibs(is)-1)%y
         ybmin(is) = ybmin(is)-zfd(is)
         ybgau(is) = ybmin(is)                                                                     
         ybdro(is) = ybmin(is)                                                                     
         ymoy(is) = point_geo(kd(is))%y
      enddo
!--->Fin de lecture du fichier .MIN

      if (silent) then
         write(9,'(a)') ' Vérification de la géométrie'
      else
         write(6,'(a)') ' Vérification de la géométrie'
      endif
!
!--->Vérification : on recherche les sections à la même abscisse qui ne sont
!                   pas singulières (DX=0)
      n030 = 0
      do ib = 1, ibmax
         do is = is1a(ib)+1, is2a(ib)
            if (zegal(xgeo(is),xgeo(is-1),un) .and. iss(is)==0) then
               call err030(is-1)
               n030 = n030+1
            endif
         enddo
      enddo
      if (n030/=0) then
         write(lu3,*) ' >>>> Erreur 030 : arrêt du programme'
         write(0,*) ' >>>> Erreur 030 : voir le fichier .TRA'
         stop 030
      endif

!--->Vérification : on recherche les sections qui se rétrécissent au dessus
!                   de la cote de débordement mineur-moyen
      n033 = 0
      do is = 1, ismax
         do k = kd(is)+1,ibs(is)
            alge = point_geo(k)%al
            if (alge<almy(is)) then
               call err033(is)
               n033 = n033+1
               exit
            endif
         enddo
      enddo
      if (n033/=0) then
         write(lu3,*) ' >>>> Erreur 033 : arrêt du programme'
         write(0,*) ' >>>> Erreur 033 : voir le fichier .TRA'
         stop 033
      endif

!--->Vérification : on recherche les nœuds amont défluents
      n034 = 0
      do n = 1, nomax
         if (nc(n)>0 .and. lbz(n,1)/=lbz(n,2)) then
            n034 = n034+1
            call err034(n)
         endif
      enddo
      if (n034>0) then
         write(lu3,*) ' >>>> Erreur 034 : arrêt du programme'
         write(0,*) ' >>>> Erreur 034 : voir le fichier .TRA'
         stop 034
      endif
!
!--->Vérification : on recherche les sections avec profondeur du mineur nulle
      nerr37 = 0
      do is = 1, ismax
         if (ymoy(is)<0.01) then
            call err037(is)
            nerr37 = nerr37+1
         endif
      enddo
      if (nerr37>0) then
         write(lu3,*) ' >>>> Erreur 037 : il y a des sections où la profondeur du mineur est nulle'
         write(lu3,*) ' Voir le fichier .TRA'
         write(0,*) ' >>>> Erreur 037 : il y a des sections où la profondeur du mineur est nulle'
         write(0,*) ' Voir le fichier .TRA'
         stop 037
      endif

!--->Écriture sur .TRA de la longueur totale du réseau
      fic = zero
      do ib = 1, ibmax
         fic = fic+abs(xgeo(is1a(ib))-xgeo(is2a(ib)))
      enddo
      write(lu3,'(a,f10.2,a)') ' Longueur totale du réseau = ',fic,' m'

end subroutine Lire_MIN

subroutine Lire_TIT
!==============================================================================
!                      Lecture du fichier .TIT
!==============================================================================
      use parametres, only: long,ibsup,issup,nssup
      use conditions_limites, only: nc,lbamv
      use dim_reelles, only: ismax,ibmax,nomax,nclm,nclv,nsmax
      use geometrie_noeud, only: tc,nzno
      use geometrie_section, only: is1a=>is1,is2a=>is2
      use hydraulique, only: iba,ibz,na=>nam,nz=>nav,lbz,iub,ibu
      use singularites, only: iss
      use IO_Files, only: lu,boolf
      implicit none
! -- Variables --
      integer :: i,ib,ific,is, j, kb
      integer :: l,l1,l2,lb1,lb3,lss(2,nssup),lu3,lu7
      integer,dimension(ibsup) :: ma,mz
      integer :: n,n031,naa,nai,nbsec,neu,nosec,ns,nso(ibsup),nzi,nzz
      integer n1,n2,n3,n4
      character :: cfic


      lu7=lu(2)   !fichier .tit
      lu3=lTra  !fichier .tra

!---lecture du fichier .tit
      read(lu7,err=999,end=999) n1,n2,n3,n4
      if (n1/=ibsup) then
         write(lu3,'(a,2(i3,a))') ' >>>> Nombre de biefs maximum ',ibsup, &
                                  ' incompatible avec TALWEG (',n1,') <<<<'
         write(lu3,*) n1,n2,n3,n4
         write(0,'(a,2(i3,a))') ' >>>> Nombre de biefs maximum ',ibsup, &
                                ' incompatible avec TALWEG (',n1,') <<<<'
         write(0,*) n1,n2,n3,n4
         stop 129
      endif
      read(lu7) cfic

!   lecture des noeuds amont et aval des biefs (selon numero dans .TAL)
      read(lu7) nomax,(tc(n),n=1,nomax)
!   comptage des biefs
      ibmax = 0
      do ib = 1, ibsup+1
         read(lu7) i, nai, nzi, cfic
!        stockage temporaire dans MA et MZ avant reclassement (rang de calcul)
         if (i/=0) then
            ma(i) = nai  ;  mz(i) = nzi  ;  ibmax = ibmax+1
         else
            exit
         end if
      enddo 
!   lecture du type des noeuds
      read(lu7) neu,(nc(n),n=1,nomax)

!   comptages des c.l. amont et aval
      nclm=0  ;  nclv=0
      do n = 1, nomax
         if (nc(n)==1) then  !--->noeud amont
            nclm = nclm+1
         else if (nc(n)==-1) then  !--->noeud aval
            nclv = nclv+1
         else
            nc(n) = 0
         end if
      end do

!   inversion du classement des biefs (sens amont-->aval pour le transitoire)
!   création de la table ibu inverse de iub
      read(lu7) (iub(ib),ib=ibmax,1,-1),l
      do ib = 1, ibmax
         do kb = 1, ibmax
            if (abs(iub(kb))/=ib) then
               continue
            else
               ibu(ib) = kb
            endif
         enddo
      enddo

!   éléments pour remplir les matrices : Poirson février 89
      call Ini_MatriceMaille(lb1,lb3,lu7)
      if (lb1==0) lb1 = -1
      iba = lb1
      ibz = lb3
      if (iba<0) then
!     il n'y a pas de bief à l'amont de la maille : arrêt du programme
         call err035
      endif

!   lecture des numéros des sections de calcul du bief de rang i
!   lecture du nombre de sections singulières (nsmax)
!   remplissage de is1a et is2a
!   calcul du nombre total de sections (ismax)
      read(lu7) (nso(ib),ib=ibmax,1,-1),nsmax
      is1a(1) = 1
      is2a(1) = nso(1)
      do ib = 2, ibmax
         is1a(ib) = is2a(ib-1)+1
         is2a(ib) = is1a(ib)+nso(ib)-1
      enddo
      ismax = is2a(ibmax)

      if (l==2) then  !lecture vide
         read(lu7) ific
      end if

!   quelque écritures sur TRA
      write(lu3,'(1x,a,i3)') 'Nombre total de biefs : ',ibmax
      write(lu3,'(1x,a,i3)') 'Nombre total de noeuds : ',nomax
      write(lu3,'(1x,a,i4)') 'Nombre total de sections de calcul : ',ismax
      write(lu3,'(1x,a,i3)') 'Nombre total de sections singulières : ',nsmax
      if (ibz>ibmax) then
         if(iba==ibmax) then
            write(lu3,'(a)') ' Réseau ramifié'
         else
            write(lu3,'(a)') ' Réseau maillé ouvert (plusieurs CL aval)'
            write(lu3,'(1x,a,i3,a,i3,a)') 'Numéro du dernier bief amont de maille : ',iub(iba),' (',iba,')'
            write(lu3,'(a,i4)') ' Nombre de biefs dans la maille :',ibmax-iba
         endif
      else 
         write(lu3,'(a)') ' Réseau maillé fermé'
         write(lu3,'(1x,a,i3,a,i3,a)') 'Numéro du dernier bief amont de maille : ',iub(iba),' (',iba,')'
         write(lu3,'(1x,a,i3,a,i3,a)') 'Numéro du premier bief aval de maille : ',iub(ibz),' (',ibz,')'
         write(lu3,'(a,i4)') ' Nombre de biefs dans la maille :',ibmax-iba
      endif

!   Vérification des ressources de mémoire disponibles
      if (ismax>issup) then
         write(lu3,1390)
         write(0,1390)
         stop 130
      else if (ibmax>ibsup) then
         write(lu3,1400)
         write(0,1400)
         stop 131
      else if (nsmax>nssup) then
         write(lu3,1410)
         write(0,1410)
         stop 132
      endif

!   Vérification de la cohérence de la maille et des c.l. aval
      if (iba/=ibmax .and. ibz>ibmax .and. nclv<2) then
         write(lu3,1500)
         write(0,1500)
         stop 133
      endif

!   remplissage de na et nz indexés par le rang de calcul
      do kb = 1, ibmax
         ib = ibu(kb)
         na(ib) = ma(kb)
         nz(ib) = mz(kb)
      enddo

!   calcul de nzno(n) = nombre de biefs qui touchent le noeud n
      do ib = 1, ibmax
         n = na(ib)
         nzno(n) = nzno(n)+1
         n = nz(ib)
         nzno(n) = nzno(n)+1
      enddo

!   repérage des biefs amont et des biefs aval du modèle
!   remplissage de lbamv et lbz
      do ib = 1, ibmax
         naa = na(ib)  !noeud amont du bief de rang ib dans l'ordre de calcul
         nzz = nz(ib)  !noeud aval du bief de rang ib dans l'ordre de calcul
         if (nc(naa)>0) lbamv(naa) = ib  !--->noeud amont du modèle
         if (nc(nzz)<0) lbamv(nzz) = ib  !--->noeud aval du modèle
      enddo
      do n = 1, nomax
         do ib = 1, ibmax
            if (n==na(ib)) then
               lbz(n,1) = ib  ;  exit
            endif
         enddo
         do ib = ibmax,1,-1
            if (n==na(ib)) then
               lbz(n,2) = ib  ;  exit
            endif
         enddo
      enddo

!   lecture des sections singulières
      if (nsmax/=0) then
         j = (nsmax-1)/20+1  ;  n = 20
         do i = 1, j
            l1 = (i-1)*n+1  ;  l2 = i*n
            if (i==j) l2 = nsmax
            read(lu7) (lss(1,l),lss(2,l),l=l1,l2)
         enddo
      end if

!   tableau iss(is) : repérage des sections singulières
      iss(1:ismax) = 0
      n031 = 0
      if (nsmax/=0) then
         do ns = 1, nsmax
!        rang amont--->aval
            ib = ibmax-lss(1,ns)+1
            nbsec = is2a(ib)-is1a(ib)+1
            nosec = nbsec-lss(2,ns)+1
            is = is1a(ib)+nosec
            if (is>is2a(ib)) then
!           Vérification : on recherche les sections singulières situées
!                    à l'aval d'un bief qui ne sont pas redoublées
               call err031(ib)
               n031 = n031+1
            endif
            iss(is) = -(nsmax-ns+1)
         enddo
      end if
      if (n031>0) then
         write(lu3,*) ' >>>> Erreur 031 : Voir le fichier .TRA'
         write(0,*) ' >>>> Erreur 031 : Voir le fichier .TRA'
         stop 031
      endif
      return
 999  write(lu3,'(a)') '>>>> Erreur de lecture de .TIT <<<<'
      write(0,'(a)') '>>>> Erreur de lecture de .TIT <<<<'
      stop 134

!--->fin de lecture du fichier .tit

 1390 format(1x,'nombre de sections de calcul trop grand : exécution impossible')
 1400 format(1x,'nombre de biefs trop grand : exécution impossible')
 1410 format(1x,'nombre d''ouvrages élémentaires trop grand : exécution impossible')
 1500 format(//,' >>>> ERREUR à la CL aval : quand la maille est fermée'      &
             ,/,' il faut obligatoirement un bief à l''aval de la maille'  &
             ,/,' La C.L. aval ne peut être liée à un noeud qui est '      &
             ,/,' un confluent : si nécessaire ajouter un bief fictif')
end subroutine Lire_TIT


subroutine Lire_CAS(CAS_file)
!==============================================================================
!         Définition des casiers : lecture de .CAS
!
! TODO: à récrire en supprimant les GOTO et les étiquettes 10, 20, 30 et 40
! NOTE: les tableaux zne, sne et vne doivent être ceux de l'objet node
!==============================================================================
   use parametres, only: long, sizename, zero
   use dim_reelles, only: ismax, ibmax, nomax
   use geometrie_section, only: ibs
   use geometrie_noeud, only: nbs, zne, sne, vne, tc
   use IO_Files, only: lu, boolf
   implicit none
   ! -- Prototype
   character(len=sizename) :: CAS_file   
   ! -- appels --
   interface
      subroutine ecr0(chain,vol0,k)
      use parametres, only: long
         character(len=14),intent(inout) :: chain
         real(kind=long),intent(in) :: vol0
         integer,intent(out) :: k
      end subroutine ecr0
   end interface
   ! -- Variables locales temporaires --
   integer :: i, k, k0, lu3, luu, n, n0, na, nbss, neu, neuneg, nz
   character :: a1
   character(len=3) :: u0
   character(len=14) :: chain, chain1
   character(len=80) :: a2
   real(kind=long) :: sneu, u1, u2, vneu, volu, zneu
!------------------------------------------------------------------------------
   neuneg = 0
   i = 4
   nbs(0) = 0
   sne(0) = zero
   zne(0) = zero
   if (CAS_file /= '') then
      open(newunit=luu,file=trim(CAS_file),status='old',form='formatted')
      ! 'nbss'+1 est le nombre de lignes du fichier de description des casiers
      nbss = 0
      do neu = 1, nomax
         vneu = zero
         rewind luu
         ! lecture du fichier jusqu'à épuisement
         30 continue
         read(luu,'(a1,a3)',end=40) a1, u0
         if (a1=='$' .and. u0==tc(neu)) then
            sneu = -9999._long
            ! en cas d'erreur (en particulier,u1 non réel) on passe au neu suivant
            ! en fin de fichier on passe au neu suivant
            10 continue
            read(luu,'(a)',end=20) a2
            read(a2,'(a1)') a1
            if (a1=='*') then
               goto 10
            else if (a1=='$') then
               goto 20
            else
               read(a2,'(2f10.0)',end=20,err=20) u1, u2
            endif
            if (sneu < zero) then
               sneu = u2
               zneu = u1
            endif
            nbss = nbss+1
            zne(nbss) = u1
            ! la surface est stockée en HECTARES
            sne(nbss) = u2
            if (u2 < zero) then
               write(6,'(3a)') ' >>>> ERREUR : le nœud ',tc(neu),' a une surface négative'
               neuneg = neuneg+1
            endif
            ! Volume du noeud
            vne(nbss) = vneu+(sneu+0.5_long*(u2-sneu))*(u1-zneu)
            zneu = u1
            sneu = u2
            vneu = vne(nbss)
            ! aprés avoir entré 1 ligne de données,on passe à la suivante
            goto 10
            ! dans le cas ou la première ligne du fichier lu ne convient pas,
            ! on continue de lire les lignes suivantes
         else
            goto 30
         endif
         ! si pas de données,on entre 0 pour le nœud
         40 nbss = nbss+1
         zne(nbss) = zero
         sne(nbss) = zero
         vne(nbss) = zero
         !  avant de passer au neu suivant,on inscrit le nbs
         20 nbs(neu) = nbss
      enddo
      close (luu)
   else
      !si le fichier '.cas' n'existe pas,on met 0 partout
      nbs(1:nomax) = (/ (neu, neu=1,nomax) /)
      zne(1:nomax) = zero
      sne(1:nomax) = zero
      vne(1:nomax) = zero
   endif

   !--->écriture de la géométrie des nœuds à surface non-nulle sur .TRA
   if (boolf(i)) then
      write(lTra,1030)
      write(lTra,1020)
      do n = 1, nomax
         na = nbs(n-1)+1
         nz = nbs(n)
         if (nz/=na .or. sne(na)>0._long) then
            write(lTra,1000) tc(n)
            do n0 = na, nz
               volu = vne(n0)*10000._long
               call ecr0(chain,volu,k)
               k0 = 15-k
               chain1(1:k0) = chain(k:14)
               chain1(k0+1:k0+4) = ' m3 '
               if (k0+4<14) then
                  do k = k0+5, 14
                     chain1(k:k) = ' '
                  enddo
               endif
               write(lTra,1010) zne(n0), sne(n0), chain1
            enddo
         endif
      enddo
      write(lTra,1030)
      !--->diagnostic des nœuds à surface négative
      if (neuneg>0) call err036(lTra)
   endif

   1000 format(1x,'Nœud : ',a3)
   1010 format(1x,f8.3,' mètres',' ***** ',f10.1,' hectares',' ***** ',a)
   1020 format(1x,15x,'Lois hauteur-surface des casiers')
   1030 format(1x,79('-'))
end subroutine Lire_CAS


subroutine Lire_MAJ
!============================================================================== 
!                 Lecture du fichier de géométrie du lit majeur
!============================================================================== 
   use parametres, only: long
   use booleens, only: uselat
   use data_num_fixes, only: zmin,tinf,tmax
   use dim_reelles, only: ismax,ibmax
   use geometrie_section, only: ybmin,ymoy,ybmax,zfd,isa =>is1,isb =>is2,ibs,xgeo,point_geo
   use hydraulique, only: ibu
   use majeur, only: almj,smj,pmj, ymaj,kmaj
   use IO_Files, only: lu,boolf
   implicit none
   ! -- appels --
   interface
      subroutine numsec(l,i,kb,xisa,xisz,isa,isz)
      use parametres, only: long
         integer,intent(in) :: l,i,kb
         integer,intent(out) :: isa,isz
         real(kind=long),intent(inout) :: xisa,xisz
      end subroutine numsec
   end interface
   ! -- variables locales temporaires --
   logical :: bool,bool1
   integer :: i,ib,is, js1,js2, kb, ligne,lu12,luu, nligne, io_status
   character :: rep
   character(len=80) :: u
   real(kind=long) :: dx,dydeb
   real(kind=long) :: x1,xs1,xs2
   real(kind=long) :: y1,y2  !,yjk
   real(kind=long) :: z1,z2,zber,zmaj,zmoy
   character(len=27) :: trait='---------------------------'

   !---Initialisation générale : pas de lit majeur
   lu12 = lTra
   i = 17
   bool = .false.
   rep = 'N'
   do is = 1,ismax
   !---on stocke la géométrie du MAJEUR
      kmaj(is) = ibs(is)-1
      ymaj(is) = 100._long + point_geo(kmaj(is))%y
      almj(is) = point_geo(kmaj(is))%al
      smj(is) = point_geo(kmaj(is))%s
      pmj(is) = point_geo(kmaj(is))%p
   enddo
   !---lecture de .MAJ s'il existe
   if (.not.boolf(i)) then !le fichier maj n'existe pas
      return
   else                   !le fichier maj existe
      uselat = .true.
      luu = lu(i)
      nligne = 0 !initialisation du compteur de lignes lues
   endif
   
   !Lecture du FICHIER .MAJ
   do
      !read(luu,'(a80)',end=100,err=896) u  !Lecture d'une nouvelle ligne
      read(luu,'(a80)',iostat=io_status) u  !Lecture d'une nouvelle ligne
      if (io_status < 0) exit
      if (io_status > 0) then
         write(lTra,'(a,i3)') ' >>>> ERREUR de lecture dans .MAJ à la ligne numéro ',nligne
         write(0,'(a,i3)') ' >>>> ERREUR de lecture dans .MAJ à la ligne numéro ',nligne
         write(lTra,'(1x,a78)') u  ;  write(0,'(1x,a78)') u
         stop 166
      endif
      nligne = nligne+1
      !décodage de la ligne u
      if (u(1:1) == '*') then
         cycle
      elseif (u(1:1) == '?') then
         read(u,'(1x,a)') rep
         cycle
      elseif (u(1:1) == 'M' .or. u(1:1) == 'm') then
         read(u,'(a1,i3,6x,4f10.0)',iostat=io_status) ligne,ib,xs1,xs2,z1,z2
         if (io_status > 0) then
            write(lTra,'(a,i3,a)') ' >>>> ERREUR de lecture dans .MAJ à la ligne numéro ',nligne,' de type M'
            write(0,'(a,i3,a)') ' >>>> ERREUR de lecture dans .MAJ à la ligne numéro ',nligne,' de type M'
            write(lTra,'(1x,a78)') u  ;  write(0,'(1x,a78)') u
            stop 167
         endif
         call numsec(nligne,i,ib,xs1,xs2,js1,js2)
      else
         !erreur : il faut une ligne m
         write(lTra,'(a,i3,a)') ' >>>> La ligne numéro ',nligne,' devrait être une ligne de type M'
         write(0,'(a,i3,a)') ' >>>> La ligne numéro ',nligne,' devrait être une ligne de type M'
         write(lTra,'(1x,a78)') u  ;  write(0,'(1x,a78)') u
         stop 164
      endif
      !Définition des tirants d'eau de débordement et de l'indicateur BOOL1
      bool1 = (z1>0._long .or. z2>0._long)
      if (bool1) then  !interpolation
         y1 = z1-zmin-zfd(js1)
         y2 = z2-zmin-zfd(js2)
      else
         !débordement moyen/majeur à la cote de berge : pas d'interpolation (valeurs par défaut)
         y1 = ybmin(js1)
         y2 = ybmin(js2)
      endif
      !---vérifications des tirants d'eau de débordement
      bool = .false.
      if (y1<0._long) then
         bool = .true.
         write(lu12,'(2(a,f7.2))') '>>>> ERREUR dans .maj : cote de débordement inférieure à la cote du fond : ',&
                                     z1,' < ',zfd(js1)+zmin
      else
         y1 = min(y1,ybmin(js1)+100._long)
      endif
      if (y2<0._long) then
         bool = .true.
         write(lu12,'(2(a,f7.2))') '>>>> ERREUR dans .maj : cote de débordement inférieure à la cote du fond : ',&
                                     z1,' < ',zfd(js2)+zmin
       else
         y2 = min(y2,ybmin(js2)+100._long)
      endif
      if (bool) then
         write(lTra,*) '>>>> ERREUR dans la définition du lit majeur'
         write(0,*) '>>>> ERREUR dans la définition du lit majeur'
         stop 165
      endif
      !---fin de vérification et fin des valeurs par défaut

      !---Interpolations linéaires
      x1 = xgeo(js1) !x1 = abscisse de la section amont du tronçon dans le repère du bief considéré
      dx = xgeo(js2)-x1  !longueur du tronçon
      dydeb = z2-z1
      if (bool1) then
         !bool1 = true : on interpole la cote de débordement
         do is = js1,js2
            ymaj(is) = y1 + dydeb/dx*(xgeo(is)-x1)+zfd(js1)-zfd(is)
         enddo
       else
          !bool1 = false : déversement à la cote de berge : pas d'interpolation
         do is = js1,js2
            ymaj(is) = ybmin(is)
         enddo
      endif
   !Lecture de la ligne suivante
   enddo

   !---Stockage de la géométrie du majeur
   CALL Ecrire_MAJ
   if (rep == 'O') then
      !écriture sur .TRA des caractéristiques du majeur
      write(Lu12,'(/,1x,3a)') trait,' Géométrie du lit majeur ',trait
      write(lu12,'(a)') ' Bief    Abscisse      Moyen        Majeur       Berge'
      do ib = 1,ibmax
         kb = ibu(ib)
         do is = isa(kb),isb(kb)
            zmoy = zfd(is)+ymoy(is)+zmin
            zmaj = zfd(is)+ymaj(is)+zmin
            zber = zfd(is)+ybmin(is)+zmin
            write(lu12,'(1X,I3.3,4(5x,F8.2))') kb,xgeo(is),zmoy,zmaj,zber
         enddo
         write(lu12,'(3a)') trait,trait,'-'
      enddo
   endif

end subroutine Lire_MAJ
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
subroutine Ecrire_MAJ
!============================================================================== 
! 
!============================================================================== 
   use parametres, only: long
   use dim_reelles, only: ismax
   use geometrie_section, only: ibs, point_geo
   use majeur, only: almj,smj,pmj, ymaj,kmaj
   implicit none
   ! -- appels --
   interface
      subroutine numsec(l,i,kb,xisa,xisz,isa,isz)
      use parametres, only: long
         integer,intent(in) :: l,i,kb
         integer,intent(out) :: isa,isz
         real(kind=long),intent(inout) :: xisa,xisz
      end subroutine numsec
   end interface
   ! -- variables locales temporaires --
   integer :: is, k
   real(kind=long) :: aljk,aljk1, dl,dp,dz, pjk,pjk1, sjk,sjk1
   real(kind=long) :: yjk,yjk1
!------------------------------------------------------------------------------
   do is = 1,ismax
      kmaj(is) = ibs(is)-1   !cote majeur a cote max par defaut
      do k = ibs(is-1)+2,ibs(is)
         if (ymaj(is) < point_geo(k)%y-0.001_long) then
            kmaj(is) = k-1   !cote majeur inferieur a cote max
            exit
         endif
      enddo
   enddo

   do is = 1,ismax
      yjk = point_geo(kmaj(is))%y ; aljk = point_geo(kmaj(is))%al
      sjk = point_geo(kmaj(is))%s ; pjk = point_geo(kmaj(is))%p
      yjk1 = point_geo(kmaj(is)+1)%y
      aljk1 = point_geo(kmaj(is)+1)%al
      sjk1 = point_geo(kmaj(is)+1)%s ; pjk1 = point_geo(kmaj(is)+1)%p

      dl = (aljk1-aljk)/(yjk1-yjk)
      dz = ymaj(is)-yjk
      dp = (pjk1-pjk)/(yjk1-yjk)
      almj(is) = aljk+dz*dl
      pmj(is) = pjk+dz*dp
      smj(is) = sjk+0.5_long*(aljk+almj(is))*dz
   enddo
end subroutine Ecrire_MAJ


subroutine NumSec(l,i,kb,xisa,xisz,isa,isz)
!============================================================================ == 
!       routine de recherche des numéros de section amont et aval d'un
!       tronçon défini par les pm amont et aval
!       on prend les sections qui encadrent au plus prés le tronçon exact.
!---> entrées
! l = numéro de la ligne du fichier correspondant
! i = numéro du fichier en cours de lecture
! kb = numéro du bief (au sens de .tal seul connu par l'utilisateur)
! xisa = pm de la section amont
! xisz = pm de la section aval
!---> sorties
! isa = numéro absolu de la section amont (et non numéro local dans le bief)
! isz = numéro absolu de la section aval (et non numéro local dans le bief)
!
! 09/12/08 : on force les valeurs aux bornes si on en est très près pour tenir
!            compte des pb d'arrondis au voisinage des limites de bief
!============================================================================ == 
   use parametres, only: long
   use dim_reelles, only: ibmax
   use geometrie_section, only: is1a =>is1,is2a =>is2, xgeo
   use hydraulique, only: ibu
   use IO_Files, only: ext,com,lu
   use singularites, only: iss
   use Mage_Utilitaires, only: isect
   implicit none
   ! -- prototype --
   integer,intent(in)  :: l,i,kb
   integer,intent(out) :: isa,isz
   real(kind=long),intent(inout) :: xisa,xisz
   ! -- variables locales temporaires --
   integer :: ib
   real(kind=long) :: xmax,xmin
   character(len=180) :: err_message

   !---vérifications préliminaires et initialisation
   if (kb>ibmax) then
      write(err_message,'(a,a3,a,i5,a,i3,a)') '>>>> ERREUR dans .',ext(i),' à la ligne ',l, &
                   ' : le bief ',kb,' n''existe pas'
      write(0,'(a)') trim(err_message)  ;  write(lTra,'(a)') trim(err_message)
      stop 173
   endif
   ib = ibu(kb)
!------cas des entrees nulles : on prend le bief entier
   if (xisa == 0._long) xisa = xgeo(is1a(ib))
   if (xisz == 0._long) xisz = xgeo(is2a(ib))
   xmin = min(xgeo(is1a(ib)),xgeo(is2a(ib)))
   xmax = max(xgeo(is1a(ib)),xgeo(is2a(ib)))
!+JBF 09/12/2008 -->
   if (abs(xmin-xisa) < 0.001_long) xisa = xmin
   if (abs(xmax-xisa) < 0.001_long) xisa = xmax
   if (abs(xmin-xisz) < 0.001_long) xisz = xmin
   if (abs(xmax-xisz) < 0.001_long) xisz = xmax
!<--
   if (xisa<xmin .or. xisa>xmax) then
      write(err_message,'(a,a3,a,i5,a,f10.2,a)') '>>>> ERREUR dans .',ext(i),' à la ligne ',l, &
                ' : la section ',xisa,' n''appartient pas au bief ',kb
      write(0,'(a)') trim(err_message)  ;  write(lTra,'(a)') trim(err_message)
      stop 174
   endif
   if (xisz<xmin .or. xisz>xmax) then
      write(err_message,'(a,a3,a,i5,a,f10.2,a)') '>>>> ERREUR dans .',ext(i),' à la ligne ',l, &
                ' : la section ',xisz,' n''appartient pas au bief ',kb
      write(0,'(a)') trim(err_message)  ;  write(lTra,'(a)') trim(err_message)
      stop 175
   endif
!---recherche des numéros de section
! modif JBF du 29/11/2005 pour trouver les sections les plus proches des pk fournis
!                         la tolérance de 0,5 est remplacée par 1000.
   isa = iSect(kb,xisa,1000._long)
   isz = iSect(kb,xisz,1000._long)
   if (isa == 0 .or. isz == 0) then
      write(err_message,'(a,i3,a)') ' >>>> ERREUR (fonction NUMSEC) : le bief ',kb,' est inconnu sur ce réseau <<<<'
      write(0,'(a)') trim(err_message)  ;  write(lTra,'(a)') trim(err_message)
      stop 192
   else if (isa < 0) then
      write(err_message,'(a,f8.2,a,i3,a)')  ' >>>> ERREUR (fonction NUMSEC)  : il n''y a pas de section à l''abscisse ', &
                                               xisa,' dans le bief ',kb,' <<<<'
      write(0,'(a)') trim(err_message)  ;  write(lTra,'(a)') trim(err_message)
      stop 193
   else if (isz < 0) then
      write(err_message,'(a,f8.2,a,i3,a)')  ' >>>> ERREUR (fonction NUMSEC)  : il n''y a pas de section à l''abscisse ', &
                                               xisz,' dans le bief ',kb,' <<<<'
      write(0,'(a)') trim(err_message)  ;  write(lTra,'(a)') trim(err_message)
      stop 193
   endif

   !------vérifications
   !---------le secteur deversant est plus court qu'un pas d'espace : on l'allonge à un pas d'espace
   if (isa == isz) isz = isz+1
   !---------la section amont est en dehors du bief : on la ramène au bord
   if (isa<is1a(ib)) then
      isa = is1a(ib)
   elseif (isa >= is2a(ib)) then
      isa = is2a(ib)-1
   endif
   !---------la section aval est en dehors du bief : on la ramène au bord
   if (isz <= is1a(ib)) then
      isz = is1a(ib)+1
   elseif (isa>is2a(ib)) then
      isz = is2a(ib)
   endif

end subroutine NumSec


subroutine exportNet(la_topo)
!==============================================================================
!   Enregistrement du classement hydraulique des biefs
!   Peut être récupéré sans ambigüité par Adis-TS 
!==============================================================================
   use IO_Files
   use hydraulique, only: nam, nav, ibu
   use geometrie_noeud, only: tc
   use dim_reelles, only: ibmax
   use TopoGeometrie
   ! -- Prototype
   type (dataset), intent(in) :: la_topo
   !variables locales
   integer :: ib, kb, i, luu
   character(len=8) :: nomBief
   character(len=3) :: nam, nav

   i = index(nom(3),'.')
   open (newunit=luu, file=repFile(1:i)//'net', status='unknown',form='formatted')
   do kb = 1, la_topo%net%nb
      ib = la_topo%net%ibu(kb)
      if (kb < 10) then
         write(nomBief,'(a,i1)') 'Bief_',kb
      elseif (kb < 100) then
         write(nomBief,'(a,i2)') 'Bief_',kb
      elseif (kb < 1000) then
         write(nomBief,'(a,i3)') 'Bief_',kb
      else
         stop 444
      endif
      nam = la_topo%nodes(la_topo%biefs(ib)%nam)%name
      nav = la_topo%nodes(la_topo%biefs(ib)%nav)%name
      write(luu,'(a,1x,2(a3,1x),2a,1x,2a)') trim(nomBief),nam,nav,trim(nomBief),'.M'
   enddo
   close (luu)
end subroutine exportNet

function volume(n,zz)
!==============================================================================
!       calcul du volume d'un nœud en fonction de sa cote
! n  = numéro du nœud
! zz = cote au nœud n
!==============================================================================
   use parametres, only: long, zero
   use data_num_fixes, only: zmin
   use geometrie_noeud, only: nbs, zne, sne, vne
   use IO_Files, only: boolf
   implicit none
   ! -- le prototype --
   real(kind=long) :: volume
   real(kind=long),intent(in) :: zz
   integer,intent(in) :: n
   ! -- les variables --
   integer :: k
   integer :: kisa, kisb, kisy, kisz
   real(kind=long) :: ds, dz, s0, surf, z,  z0
   !---------------------------------------------------------------------------
!--->z = cote d'eau au nœud
!---recherche des cotes données encadrant z ----> indice k
!   on obtient : zne(k) < z < zne(k+1)
!---------------------------------------------------------------------------
   if (.not.boolf(4) .or. n==0) then
      volume = zero ; return
   endif

   z = zz+zmin  !dénormalisation de la cote
   kisa=nbs(n-1)+1  !indice de la cote du fond du casier
   kisz=nbs(n)      !indice de la dernière cote du casier
   kisb=kisa+1

   if (z < zne(kisa)) then
      volume = zero ; return
   endif

   if (kisz==kisa) then  !un seul point d'interpolation : nœud cylindrique
      !volume = vne(kisz)*10000._long
      volume = sne(kisa)*(z-zne(kisa))*10000._long
      return
   endif
   
   !---recherche de l'encadrement de z
   do k = kisb, kisz
      if (zne(k) > z) then !interpolation linéaire sur la surface
         dz = z-zne(k-1)
         surf = sne(k-1)+dz*(sne(k)-sne(k-1))/(zne(k)-zne(k-1))  !aire du nœud
         volume = (vne(k-1)+0.5_long*(surf+sne(k-1))*dz)*10000._long
         return
      endif
   enddo
   volume = (vne(kisz)+sne(kisz)*(z-zne(kisz)))*10000._long !prolongement cylindrique

end function volume
