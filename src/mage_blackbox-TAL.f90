!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - INRAE - 1989-2020                           #
!# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################
!###############################################################################
! boîte noire permettant d'utiliser TALWEG de façon transparente pour
! générer les données topologiques nécessaires au calcul de la maille
!###############################################################################
!
module Parametres_TAL
!==============================================================================
! Définition des dimensions de tableaux dans le module MOTAL (ex-TALWEG)
!
!---Historique des modifications
! 02/02/2007 : documentation
!==============================================================================
!
   !use parametres, only : ibsup
   use TopoGeometrie, only: la_topo
   implicit none
!
   integer,parameter :: zip=24
   integer :: zib
   integer,parameter :: zidmax=2500
   integer :: zib1

   contains
   subroutine init_param_TAL
      implicit none

      zib = la_topo%net%nb
      zib1 = zib+1

   end subroutine init_param_TAL
!
end module Parametres_TAL


module FICHIERTAL
!==============================================================================
!          unités logique pour le module MOTAL (ex. TALWEG)
! NB : les numéros d'unité logique doivent être cohérents avec ceux définis dans
!      mage6e0.for/Lire_REP()

!---Historique des modifications
! 29/06/2007 : documentation et mise en format libre
!              suppression de la routine de sauvegarde non utilisée
!==============================================================================
!
   implicit none
   ! ex-common /TE0/
   integer :: LU1!=21            ! fichier intermediaire
   integer :: LU2!=52            ! fichier .GEO contenant la trace de talweg
   integer :: LU3!=23            ! fichier intermediaire
   integer :: LU4!=24            ! fichier intermediaire
   integer :: LU5!=31            ! fichier .MIN
   integer :: LU6!=25            ! fichier intermediaire
   integer :: LU7!=32            ! fichier .TIT
   integer :: LU8!=26            ! fichier intermediaire
   integer :: LU9!=27            ! fichier intermediaire
   integer :: LUA!=22            ! fichier intermediaire
   integer :: LU0!=30            ! fichier .TAL
   integer :: LU!=20             ! fichier intermediaire
end module FICHIERTAL


module Maille
!==============================================================================
!       Variables ajoutées par M. POIRSON pour la maille (1889)
!  module utilisé par mage_blackbox-TAL.f90/Ini_MatriceMaille pour l'initialisation
!  et par le double balayage (mage_e3_DoubleBalayage.f90)
!==============================================================================
   use Parametres, only: Long, zero

   use TopoGeometrie, only: la_topo
   implicit none
   ! /rbloc/
   real(kind=long),allocatable :: xk(:), xl(:), xc4(:), xq(:), xf(:), xc2(:), sno(:), qno(:)
   ! /xnoeud/
   real(kind=long),allocatable :: arv(:), asv(:), atv(:)
   ! /nbloc/
   logical,allocatable :: bool(:)
   integer :: nr, na, nb, nh, ir1, ir2, ir3
   integer,allocatable :: ir(:), ia(:), ja(:), ib(:), jb(:), ih(:), jh(:), nx(:,:)

   contains
   subroutine iniMaille
   ! --- Initialisation des variables du module MAILLE
      implicit none
      integer :: ibmax, nomax
      ibmax = la_topo%net%nb
      nomax = la_topo%net%nn

      allocate (ir(ibmax),ia(2*ibmax),ja(2*ibmax),ib(ibmax),jb(ibmax),ih(2*ibmax),jh(2*ibmax))
      allocate (bool(ibmax),nx(nomax,nomax+1))
      allocate (xk(ibmax),xl(ibmax),xc4(ibmax),xq(ibmax),xf(ibmax),xc2(ibmax),sno(nomax),qno(nomax))
      allocate (arv(nomax),asv(nomax),atv(nomax))

      nr = 0 ; na = 0 ; nb = 0 ; nh = 0
      ir = 0 ; ia = 0 ; ja = 0 ; ib = 0 ; jb = 0 ; ih = 0 ; jh = 0 ; nx = 0
      bool = .false.
      ir1 = 0 ; ir2 = 0 ; ir3 = 0
      ! -- /rbloc/ --
      xk = zero ; xl = zero ; xc4 = zero
      xq = zero ; xf = zero ; xc2 = zero
      sno = zero ; qno = zero
      ! -- /xnoeud/ --
      arv = zero ; asv = zero ; atv = zero
   end subroutine iniMaille


   subroutine generate_dummy_TAL
   ! génère un fichier TAL fictif réduit à la description de la topologie
   ! du réseau
      implicit none
      character(len=10), parameter :: dummy_TAL = '_dummy.TAL'
      character(len=80), parameter :: section_titre = '* section fictive'
      character(len=80), parameter :: section1_Y = '   0.00  0.00  0.00 10.00 10.00'
      character(len=80), parameter :: section1_Z = '        10.00  0.00  0.00 10.00'
      character(len=80), parameter :: section2_Y = ' 100.00  0.00  0.00 10.00 10.00'
      character(len=80), parameter :: section2_Z = '        10.00  0.00  0.00 10.00'
      integer :: ib, lu

      open(newunit=lu,file=dummy_TAL,form='formatted',status='unknown')
      do ib = 1, la_topo%net%nb
         write(lu,'(a2,2a3,f5.0,5x,a)') '# ', la_topo%biefs(ib)%amont, la_topo%biefs(ib)%aval, 999., trim(la_topo%biefs(ib)%name)
         write(lu,'(a)') section_titre
         write(lu,'(a)') section1_Y
         write(lu,'(a)') section1_Z
         write(lu,'(a)') section_titre
         write(lu,'(a)') section2_Y
         write(lu,'(a)') section2_Z
      enddo
      close(lu)
   end subroutine generate_dummy_TAL


   subroutine init_MatriceMaille
   !==============================================================================
   !     éléments pour remplir les blocs :   M   A   B
   !                                                 H
   !==============================================================================
      use PARAMETRES_TAL, only:init_param_TAL
      implicit none

      if (la_topo%net%iba == la_topo%net%nb) then
         return !modèle ramifié
      else
         !modèle maillé -> il faut utiliser la boîte noire Talweg pour construire
         !                 la matrice de la maille
         call generate_dummy_TAL
         call init_param_TAL
         call IniVarMod_TAL
         call Lire_TAL
         call Lire_TIT
      endif
   end subroutine init_MatriceMaille
end module Maille
!
!###############################################################################
!
module TE1
!==============================================================================
!             ex-common /TE1/ de TALWEG

!---Historique des modifications
! 29/06/2007 : documentation et mise en format libre
!              suppression de la routine de sauvegarde non utilisée
!==============================================================================
!
   implicit none
   integer :: LI
   integer :: IBMAX
   integer :: LIT   !PREMIER NOEUD DEFLUENT=DEBUT DE MAILLE
   integer :: ID
   integer :: IB    !NOMBRE DE NOEUDS AVAL
                       !INDICE DE DEBORDEMENT MINEUR MOYEN
   integer :: IPA   !NOMBRE DE BIEFS DE MAILLE
   integer :: IF1   !CONTROLE LA PROGRESSION DES SECTIONS DANS LE BIEF
   integer :: IF2   !CONTROLENT LA VALIDITE DES COORDONNEES DES POINTS DE LA SECTION
   integer :: IF21  !CONTROLENT LA VALIDITE DES COORDONNEES DES POINTS DE LA SECTION
   integer :: IF3   !CONTROLE LA VALIDITE DES ABSCISSES DE POSITION
   integer :: IF4
   integer :: IB0   !NUMERO ABSOLU DE LA SECTION DE CALCUL
   integer :: IF0
!
   contains
      subroutine IniTE1
         LI = 0
         IBMAX = 0
         LIT = 0
         ID = 0
         IB = 0
         IPA = 0
         IF1 = 0
         IF2 = 0
         IF21 = 0
         IF3 = 0
         IF4 = 0
         IB0 = 0
         IF0 = 0
      end subroutine IniTE1
end module TE1
!
!###############################################################################
!
module TE2_TE3
!==============================================================================
!             ex-common /TE2/ et /TE3/ de TALWEG

!---Historique des modifications
! 29/06/2007 : documentation et mise en format libre
!              suppression de la routine de sauvegarde non utilisée
!==============================================================================
!
   use PARAMETRES_TAL,only : ZIB
   implicit none
   integer, allocatable :: LS(:,:)
   integer, allocatable :: IPAS(:)
   integer, allocatable :: N3(:)  !N3(IB) = nœud amont du bief n° IB dans TAL
   integer, allocatable :: N4(:)  !N4(IB) = nœud aval du bief n° IB dans TAL
   integer, allocatable :: N5(:)  !N5(L) = nombre de fois où L est nœud amont, puis nœuds amont des biefs de maille
   integer, allocatable :: N6(:)  !N6(L) = nombre de fois où L est nœud aval
   integer, allocatable :: N7(:,:)  !N7(J,L) = numéros du J-ème bief partant du nœud amont L
                                     !(ayant le nœud L comme nœud amont)
   integer, allocatable :: N8(:)  !N8(N2) = nombre de biefs de maille reliés au nœud N2
   integer, allocatable :: N9(:)  !liste des nœuds aval, nombre d'éléments dans N9(1)
   integer, allocatable :: IUB(:) !IUB(IB) = numéro dans .TAL du bief de rang IB dans le classement des
                                 !biefs de l'aval vers l'amont
   integer, allocatable :: NS0(:,:)  !NS0(L,K) = nombre de section du bief de rang K pour le lit L (L = 1 à 2) ;
                                    !L=2 -> lit majeur (inutilisé)

   contains
      subroutine IniTE2_TE3
         implicit none
         allocate (LS(1:ZIB,1:2)) ; LS = 0
         allocate (IPAS(1:ZIB)) ; IPAS = 0
         allocate (N3(1:ZIB+1)) ; N3 = 0
         allocate (N4(1:ZIB+1)) ; N4 = 0
         allocate (N5(1:ZIB+1)) ; N5 = 0
         allocate (N6(1:ZIB+1)) ; N6 = 0
         allocate (N7(1:ZIB,1:ZIB+1)) ; N7 = 0
         allocate (N8(1:ZIB+1)) ; N8 = 0
         allocate (N9(1:ZIB)) ; N9 = 0
         allocate (IUB(1:ZIB)) ; IUB = 0
         allocate (NS0(1:2,1:ZIB)) ; NS0 = 0
      end subroutine IniTE2_TE3
end module TE2_TE3
!
!###############################################################################
!
module TE4
!==============================================================================
!             ex-common /TE4/ de TALWEG

!---Historique des modifications
! 29/06/2007 : documentation et mise en format libre
!              suppression de la routine de sauvegarde non utilisée
!==============================================================================
!
   use PARAMETRES_TAL,only : ZIB
   implicit none
   integer, allocatable :: IC(:)
   integer, allocatable :: I1(:)      !INDICE DES LIGNES DE (A)
   integer, allocatable :: J1(:)      !INDICE DES COLONNES DE (A)
   !
   contains
      subroutine IniTE4
         implicit none
         allocate (IC(1:ZIB+1)) ; IC = 0
         allocate (I1(1:2*ZIB)) ; I1 = 0
         allocate (J1(1:2*ZIB)) ; J1 = 0
      end subroutine IniTE4
end module TE4
!
!###############################################################################
!
module TE5
!==============================================================================
!             ex-common /TE5/ de TALWEG

!---Historique des modifications
! 29/06/2007 : documentation et mise en format libre
!              suppression de la routine de sauvegarde non utilisée
!==============================================================================
   use PARAMETRES_TAL,only : ZIP
   implicit none
   integer, allocatable :: N0(:)
   integer, allocatable :: NC(:)
   !
   contains
      subroutine IniTE5
         implicit none
         allocate (N0(1:2*ZIP)) ; N0 = 0
         allocate (NC(1:2*ZIP)) ; NC = 0
      end subroutine IniTE5
end module TE5
!
!###############################################################################
!
module TR01
!==============================================================================
!             ex-common /TR0/ de TALWEG

!---Historique des modifications
! 03/07/2007 : documentation et mise en format libre
!              suppression de la routine de sauvegarde non utilisée
!==============================================================================
   use PARAMETRES,only : Long
   use PARAMETRES_TAL,only : ZIP
   implicit none
   real(kind=Long), allocatable :: X(:)
   real(kind=Long), allocatable :: Z(:)
   real(kind=Long), allocatable :: XC(:)
   real(kind=Long), allocatable :: ZC(:)
   real(kind=Long), allocatable :: PC(:)
   real(kind=Long),DIMENSION(2) :: DX
   real(kind=Long),DIMENSION(2) :: AX
   real(kind=Long),DIMENSION(2) :: AXD
   real(kind=Long) :: U           ! cote de berge gauche
   real(kind=Long) :: PP          ! cote de berge droite
   !
   contains
      subroutine IniTR01
         implicit none
         allocate (X(1:ZIP)) ; X = 0.0_Long
         allocate (Z(1:ZIP)) ; Z = 0.0_Long
         allocate (XC(1:ZIP)) ; XC = 0.0_Long
         allocate (ZC(1:ZIP)) ; ZC = 0.0_Long
         allocate (PC(1:ZIP)) ; PC = 0.0_Long
         DX(1:2) = 0.0_Long
         AX(1:2) = 0.0_Long
         AXD(1:2) = 0.0_Long
         U = 0.0_Long
         PP = 0.0_Long
      end subroutine IniTR01
!
end module TR01
!
!###############################################################################
!
module TR02
!==============================================================================
!             ex-common /TR0/ de TALWEG

!---Historique des modifications
! 03/07/2007 : documentation et mise en format libre
!              suppression de la routine de sauvegarde non utilisée
!==============================================================================
   use PARAMETRES,only : Long
   use PARAMETRES_TAL,only : ZIP
   implicit none
   real(kind=Long), allocatable :: XC(:,:)
   real(kind=Long), allocatable :: YC(:,:)  !tirants d'eau
   real(kind=Long), allocatable :: PC(:,:)
   real(kind=Long),DIMENSION(2) :: B1
   real(kind=Long),DIMENSION(2) :: B2
   real(kind=Long),DIMENSION(2) :: AC
   real(kind=Long),DIMENSION(2) :: DC
   real(kind=Long),DIMENSION(2) :: ZC      !cote de fond
   real(kind=Long) :: AX
   real(kind=Long) :: SI
   real(kind=Long), allocatable :: YE(:)    !ensemble des tirants d'eau des sections 1 et 2
   real(kind=Long), allocatable :: XE(:,:)  !largeurs des sections 1 et 2
   real(kind=Long), allocatable :: PE(:,:)  !perimetres des sections 1 et 2
   !
   contains
      subroutine IniTR02
         implicit none
         allocate (XC(1:2,1:ZIP+1)) ; XC = 0.0_Long
         allocate (YC(1:2,1:ZIP+1)) ; YC = 0.0_Long
         allocate (PC(1:2,1:ZIP+1)) ; PC = 0.0_Long
         B1(1:2) = 0.0_Long
         B2(1:2) = 0.0_Long
         AC(1:2) = 0.0_Long
         DC(1:2) = 0.0_Long
         ZC(1:2) = 0.0_Long
         AX = 0.0_Long
         SI = 0.0_Long
         allocate (YE(1:2*ZIP)) ; YE = 0.0_Long
         allocate (XE(1:2,1:2*ZIP)) ; XE = 0.0_Long
         allocate (PE(1:2,1:2*ZIP)) ; PE = 0.0_Long
      end subroutine IniTR02
end module TR02
!
!###############################################################################
!
module TR1
!==============================================================================
!             ex-common /TR1/ de TALWEG

!---Historique des modifications
! 03/07/2007 : documentation et mise en format libre
!              suppression de la routine de sauvegarde non utilisée
!==============================================================================
   use PARAMETRES,only : Long
   use PARAMETRES_TAL,only : ZIB
   implicit none
   real(kind=Long), allocatable :: PAS(:)
   real(kind=Long), allocatable :: SU(:)
   real(kind=Long), allocatable :: CO(:,:)
   !
   contains
      subroutine IniTR1
         implicit none
         allocate (PAS(1:ZIB+1)) ; PAS = 0.0_Long
         allocate (SU(1:ZIB+1)) ; SU = 0.0_Long
         allocate (CO(1:ZIB,1:2)) ; CO = 0.0_Long
      end subroutine IniTR1
end module TR1
!
!###############################################################################
!
module TR2
!==============================================================================
!             ex-common /TR2/ de TALWEG

!---Historique des modifications
! 03/07/2007 : documentation et mise en format libre
!              suppression de la routine de sauvegarde non utilisée
!==============================================================================
   use PARAMETRES,only : Long
   use PARAMETRES_TAL,only : ZIP
   implicit none
   real(kind=Long) :: ZDM
   real(kind=Long), allocatable :: X(:)
   real(kind=Long), allocatable :: Z(:)
   real(kind=Long), allocatable :: S(:)
   real(kind=Long), allocatable :: P(:)
   !
   contains
      subroutine IniTR2
         implicit none
         allocate (X(1:2*ZIP)) ; X = 0.0_Long
         allocate (Z(1:2*ZIP)) ; Z = 0.0_Long
         allocate (S(1:2*ZIP)) ; S = 0.0_Long
         allocate (P(1:2*ZIP)) ; P = 0.0_Long
      end subroutine IniTR2
end module TR2
!
!###############################################################################
!
module TR3
!==============================================================================
!             ex-common /TR3/ de TALWEG

!---Historique des modifications
! 03/07/2007 : documentation et mise en format libre
!              suppression de la routine de sauvegarde non utilisée
!==============================================================================
   use PARAMETRES,only : Long
   use PARAMETRES_TAL,only : ZIDMAX
   implicit none
   real(kind=long),Dimension(5,ZIDMAX) :: XX
   !
   contains
      subroutine IniTR3
         implicit none
         XX(1:5,1:ZIDMAX) = 0.0_Long
      end subroutine IniTR3
end module TR3
!
!###############################################################################
!
module CAR
!==============================================================================
!             ex-common /CAR/ de TALWEG

!---Historique des modifications
! 03/07/2007 : documentation et mise en format libre
!              suppression de la routine de sauvegarde non utilisée
!==============================================================================
   use parametres_tal,only : zib1
   implicit none
   character :: tit*80                            !titre de l'etude
   character(len=3), allocatable :: tc(:)         !nom du noeud
   character :: p

   contains
      subroutine IniCAR
         implicit none
         tit=repeat(' ',80)
         allocate (tc(1:zib1)) ; tc(1:zib1) = repeat(' ',3)
         p=" "
      end subroutine IniCAR
end module CAR


module JBF
!==============================================================================
!         Données pour la modification locale du pas d'espace dans TAL

!---Historique des modifications
! 03/07/2007 : documentation et mise en format libre
!              suppression de la routine de sauvegarde non utilisée
!==============================================================================
   use parametres,only : long, zero
   use parametres_tal,only : zib
   implicit none
   real(kind=long), allocatable :: pk(:)
   ! dxx(npk) est le pas d'espace que l'on veut imposer entre la section de pk=pk(npk)
   ! et la suivante dans .tal
   real(kind=long), allocatable :: dxx(:)
   integer :: npk
   integer, allocatable :: ikb(:)
   !
   contains
      subroutine IniJBF
         implicit none
         npk = 0
         ! la dimension 4*ZIP n'a pas de justification particulière sinon celle d'avoir
         ! assez de place ; l'augmenter si nécessaire (beaucoup plus de modifications
         ! locale du pas d'espace.
         allocate (ikb(1:4*zib)) ; ikb = 0
         allocate (pk(1:4*zib)) ; pk = zero
         allocate (dxx(1:4*zib)) ; dxx = zero
      end subroutine IniJBF
end module JBF


module Geometrie_Noeud
!==============================================================================
!               Grandeurs géométriques pour les nœuds
!==============================================================================
   use parametres, only: nosup, nnsup, long, zero
   implicit none
   real(kind=long) :: zne(0:nnsup) ! tabulation des cotes pour decrire les noeuds
   real(kind=long) :: sne(0:nnsup) ! tabulation des surfaces pour decrire les noeuds
   real(kind=long) :: vne(0:nnsup) ! tabulation des volumes pour decrire les noeuds
   integer :: nzno(nosup)  ! nzno(n) = nombre de biefs qui touchent le noeud n
   integer :: nbs(0:nosup) ! numero du dernier point du noeud n dans les tableaux
                           ! zne, sne et vne ( nbs(0) est vide )
   character(len=3) :: tc(nosup) ! tc(n) = nom du noeud n

   contains
      subroutine IniGeomNds
      ! --- Initialisation des variables du module Geometrie_Noeud ---      (BF)
         implicit none
         nbs(0:nosup) = 0
         zne(0:nnsup) = zero
         sne(0:nnsup) = zero
         vne(0:nnsup) = zero
         tc(1:nosup) = '   '
         nzno(1:nosup) = 0
      end subroutine IniGeomNds
end module Geometrie_Noeud
!
!###############################################################################
!
!==============================================================================
!                      LECTURE DU FICHIER .TAL
!
!    creation le 10 juillet 2000 par Cyril Giraudon.
!
!==============================================================================
!role: talweg, geometrie
!
      SUBROUTINE Lire_TAL
!
      use, intrinsic :: iso_fortran_env, only: error_unit
      USE FICHIERTAL,ONLY : LU7, lu0, lu2
      USE TE1,ONLY : LI,IBMAX,IB,IPA,IF1,IF2,IF4
      IMPLICIT NONE ; SAVE
      INTEGER :: IF8,IF81, lu0_save
!=========DECLARATION ET OUVERTURE DES FICHIERS
      open(newunit=lu0,file='_dummy.TAL',form='formatted',status='old')
      lu0_save = lu0
      open(newunit=lu2,file='_dummy.GEO',form='formatted',status='unknown')
      CALL TA0
!=========LECTURE DE LU0 ET IMPRESSION SUR LU1
      CALL TAL0
      if (IF1>=0) THEN
         IB=0
         IBMAX=0
         IPA=0
         IF1=-6
         IF4=1
         IF8=0
         LI=1
20       CONTINUE
!=========LECTURE SUR LU1 TITRE DE L'ETUDE,TITRE DU BIEF,SECTION DE DONNEE
         CALL TAL1(IF8,IF81)
         if (IF1==1 .OR. IF1==2) THEN
!=========TESTS DE CONTROLE DES DONNEES
            CALL TAL2
!=========SI LA SECTION DE DONNEE EST PARAMETREE
            if (IF2==1) CALL TAL3
!=========SI LA SECTION DE DONNEE EST EN ABSCISSES-COTES
            if (IF2==2) CALL TAL4
         ENDIF
!=========DIAGNOSTICS D'ERREUR SUR LU2,ENREGISTREMENT DES SECTIONS DE
!=========DONNEES EN LARGEURS-COTES SUR LU2,LU3,LU4
         CALL TAL5(IF81)
         if (IABS(IF1)<4) GOTO 20
!=========CLASSEMENT DES BIEFS DE L'AVAL VERS L'AMONT
         if (IF1>0.AND.IF8==0) THEN
            CALL TAL6
         ENDIF
!=========INTERPOLLATION DES SECTIONS DE CALCUL
         if (IF4/=-1.AND.IF8==0) THEN
            CALL TAL7
         ENDIF
         if (IF4==-1) IF1=0
      ENDIF
      if (IF1<=0 .OR. IF8/=0) THEN
!=========EN CAS D'ERREUR VIDANGE DU FICHIER LU7
         REWIND LU7
         WRITE(LU7) IF1,IF1,IF1,IF1
         write(error_unit,'(2A)') ' **** anomalie dans la lecture ','de la geometrie  **** '
         write(error_unit,*) '>>>> ERREUR dans TALWEG <<<<'
         write(error_unit,*) '=> Consulter le fichier GEO'
         write(error_unit,*) '=> Chercher les lignes avec le mot "erreur".'
         STOP 218
      ENDIF
!=========FERMETURE DES FICHIERS
      lu0 = lu0_save
      CALL TA1
!
      END SUBROUTINE Lire_TAL
!
!******************************************************************************
!
      SUBROUTINE TA0
!
      USE PARAMETRES,ONLY : Long
      USE PARAMETRES_TAL,ONLY : ZIP
      USE FICHIERTAL,ONLY : LU1,LU3,LU5,LU7,LU8,LUA
      IMPLICIT NONE ; SAVE
!=======================================================================
      INTEGER :: LEN
      LOGICAL :: EXISTMIN,EXISTTIT,EXISTLU1,EXISTLU3,EXISTLU8,EXISTLUA
      character(len=10) :: nom(2)
!
! -- definition des fichier MIN et TIT
      NOM(1) = '_dummy.MIN'
      NOM(2) = '_dummy.TIT'
!------ destruction des fichiers si ils existent déjà
      INQUIRE(FILE=NOM(1),EXIST=EXISTMIN)
      INQUIRE(FILE=NOM(2),EXIST=EXISTTIT)
      INQUIRE(FILE='FICLU1.tmp',EXIST=EXISTLU1)
      INQUIRE(FILE='FICLU3.tmp',EXIST=EXISTLU3)
      INQUIRE(FILE='FICLU8.tmp',EXIST=EXISTLU8)
      INQUIRE(FILE='FICLUA.tmp',EXIST=EXISTLUA)
      if (EXISTMIN) THEN
         OPEN(newUNIT=LU5,FILE=NOM(1),FORM='UNFORMATTED',STATUS='OLD')
         CLOSE(LU5,STATUS='DELETE')
      ENDIF
      if (EXISTTIT) THEN
         OPEN(newUNIT=LU7,FILE=NOM(2),FORM='UNFORMATTED',STATUS='OLD')
         CLOSE(LU7,STATUS='DELETE')
      ENDIF
      if (EXISTLU1) THEN
         OPEN(newUNIT=LU1,FILE='FICLU1.tmp',FORM='UNFORMATTED',STATUS='OLD')
         CLOSE(LU1,STATUS='DELETE')
      ENDIF
      if (EXISTLU3) THEN
         OPEN(newUNIT=LU3,FILE='FICLU3.tmp',FORM='UNFORMATTED',STATUS='OLD')
         CLOSE(LU3,STATUS='DELETE')
      ENDIF
      if (EXISTLU8) THEN
         OPEN(newUNIT=LU8,FILE='FICLU8.tmp',FORM='UNFORMATTED',STATUS='OLD')
         CLOSE(LU8,STATUS='DELETE')
      ENDIF
      if (EXISTLUA) THEN
         OPEN(newUNIT=LUA,FILE='FICLUA.tmp',FORM='UNFORMATTED',STATUS='OLD')
         CLOSE(LUA,STATUS='DELETE')
      ENDIF
!----- ouverture du fichier MIN
         OPEN(newUNIT=LU5,FILE=NOM(1),STATUS='unknown',FORM='UNFORMATTED',POSITION='REWIND')
!----- ouverture du fichier TIT
         OPEN(newUNIT=LU7,FILE=NOM(2),STATUS='unknown',FORM='UNFORMATTED',POSITION='REWIND')
!----- ouverture de fichier temporaire pour Lire_TAL
         OPEN(newUNIT=LU1,FILE='FICLU1.tmp',STATUS='UNKNOWN',FORM='UNFORMATTED',ACCESS='SEQUENTIAL',POSITION='REWIND')
         LEN=(3*ZIP+4)*long+kind(1)
         OPEN(newUNIT=LU3,FILE='FICLU3.tmp',STATUS='UNKNOWN',FORM='UNFORMATTED',ACCESS='DIRECT',RECL=LEN)
         OPEN(newUNIT=LU8,FILE='FICLU8.tmp',STATUS='UNKNOWN',FORM='UNFORMATTED',ACCESS='SEQUENTIAL',POSITION='REWIND')
         LEN=Long+2*kind(1)
         OPEN(newUNIT=LUA,FILE='FICLUA.tmp',STATUS='UNKNOWN',FORM='UNFORMATTED',ACCESS='DIRECT',RECL=len)
      END SUBROUTINE TA0
!
!******************************************************************************
      SUBROUTINE TA1
!
      USE BOOLEENS, only: DelTmp
      USE FICHIERTAL,ONLY : LU1,LU3,LU5,LU7,LU8,LUA,lu0,lu2
      IMPLICIT NONE ; SAVE
!-----fermeture des fichiers temporaires
      IF (DelTmp) THEN
         CLOSE (LU1,STATUS='DELETE')
         CLOSE (LU3,STATUS='DELETE')
         CLOSE (LU8,STATUS='DELETE')
         CLOSE (LUA,STATUS='DELETE')
      ELSE
         CLOSE (LU1)
         CLOSE (LU3)
         CLOSE (LU8)
         CLOSE (LUA)
      ENDIF
      CLOSE (LU5,status='delete')
      CLOSE (LU7)
      close (lu0,status='delete')
      close (lu2,status='delete')
!
      END SUBROUTINE TA1
!
!******************************************************************************
      SUBROUTINE TAL0

!******************************************************************************

      USE PARAMETRES, ONLY: Long
      USE PARAMETRES_TAL,ONLY : ZIP,ZIB,ZIDMAX,ZIB1
      USE FICHIERTAL,ONLY : LU1,LU2,LU3,LU0
      USE TE1,ONLY : IF1
      USE CAR,ONLY : TC,TIT
      USE JBF,ONLY : NPK,IKB,PK,DXX
      IMPLICIT NONE ; SAVE
!
! -- Appels -
!
! -- Variables --
      CHARACTER VE*62,U1*3,U2*3,U*79,E(8),V1
      real(kind=Long),DIMENSION(13) :: V
      real(kind=Long) :: P,A
      INTEGER :: ITC,K,L,LL,LF,I,II,KO,J,N
      INTEGER :: KMJ

      DATA E /'*','!','#','-',' ','+','''','M'/
!=======================================================================
!------- debut de la routine TAL0

!---initialisation pour supplement JBF
      NPK=0
!---finJBF
!---------INITIALISATION DE LA ZONE TITRE AVEC DES BLANCS
      WRITE(LU2,840) ZIB,ZIP,ZIDMAX
      ITC=0
      K=0
      L=0
      IF1=-4
      LL=0
   25 CONTINUE
!---------LECTURE ET IMPRESSION DU FICHIER D'ENTREE
!--------- ! =TITRE GENERAL
!--------- * =COMMENTAIRE
!--------- # =DEBUT DE BIEF
!--------- - =BIEF A INVERSER
      LF=0
      READ(LU0,850,END=45) V1,U
      IF1=1
      if (LL==0) WRITE(LU2,849)
      LL=1
      WRITE(LU2,851) V1,U
      if (V1==E(1)) GO TO 25
!---supplement JBF
      if (V1=='$') THEN
         NPK=NPK+1
         READ(U,1000) IKB(NPK),PK(NPK),DXX(NPK)
 1000    FORMAT(I3,6X,2F10.0)
!--->dxx(npk) est le pas d'espace que l'on veut imposer entre la section de
!    pk=pk(npk) et la suivante dans .TAL
         GOTO 25
      ENDIF
!---finJBF
      if (V1/=E(2)) GO TO 35
!----------REMPLISSAGE DE LA ZONE TITRE
      READ(U,852) TIT
      GO TO 25
   35 CONTINUE
      LF=1
      if (V1==E(3)) GO TO 45
      if (K/=0) GO TO 40
      IF1=-1
      WRITE(LU2,853)
      GO TO 70
   40 CONTINUE
!---------DECODAGE DES SECTIONS
      READ(U,854) (V(I),I=1,13)
      if (K==1) WRITE(LU1) V1,(V(I),I=1,13)
      if (K==1) GO TO 25
!---------SI LIT MAJEUR ON RETIENT DANS P LA PREMIERE DISTANCE
!---------DU BIEF A INVERSER
      if (V1==E(8)) KMJ=KMJ+1
      if (V1==E(8).AND.KMJ==1) P=V(1)
      L=L+1
      I=L
!---------LU3 FICHIER TEMPORAIRE POUR RECOPIE SUR LU1
      WRITE(LU3,REC=I) V1,(V(II),II=1,13)
      GO TO 25
   45 CONTINUE
      if (IF1==-4) WRITE(LU2,855)
      if (IF1==-4) GO TO 70
      KO=0
      if (K/=2.OR.L==0) GO TO 55
!----------RECOPIE SUR LU1 SI BIEF A INVERSER
      J=0
      DO 50 N=1,L
      J=J+1
      if (KO/=0) GO TO 48
      J=L-N-2
      V1=E(5)
      if (J==-2) GO TO 50
      if (J<=0) GO TO 47
      I=J
      READ(LU3,REC=I) V1
   47 CONTINUE
      KO=2
      if (V1==E(6)) KO=4
      if (KO==2) J=J+2
   48 CONTINUE
      KO=KO-1
      I=J
      READ(LU3,REC=I) V1,(V(II),II=1,13)
!---------INVERSION DES DISTANCES SI LIT MAJEUR
      if (V1/=E(8)) GO TO 49
      A=V(1)
      V(1)=P
      P=A
   49 CONTINUE
      WRITE(LU1) V1,(V(I),I=1,13)
   50 CONTINUE
      if (J==-2) KO=1
!---------SI KO/=0 IL MANQUE DES CARTES DANS LE BIEF A INVERSER
      if (KO/=0) WRITE(LU1) E(7),(V(I),I=1,13)
      L=0
   55 CONTINUE
!---------MARQUE * =FIN DE BIEF SUR LU1
!---------DEBUT DU NOUVEAU BIEF
      if (K/=0.AND.KO==0) WRITE(LU1) E(1),(V(I),I=1,13)
      if (LF==0) GO TO 65
      K=1
!---------DECODAGE DES CARTES #
      READ(U,856) V1,U1,U2,(V(I),I=1,2),VE
!---------(TC)= TABLE DE CORRESPONDANCE ENTRE NOEUDS SYMBOLIQUE ET NUMERIQUE
      J=0
      N=0
      if (U1==E(5)) J=-1
      if (U2==E(5)) N=-1
      if (ITC==0) GO TO 57
      DO 56 I=1,ITC
      if (J==0.AND.U1==TC(I)) J=I
      if (N==0.AND.U2==TC(I)) N=I
   56 CONTINUE
   57 CONTINUE
      if (ITC>ZIB) GO TO 59
      if (J/=0) GO TO 58
      ITC=ITC+1
      TC(ITC)=U1
      J=ITC
   58 CONTINUE
      if (ITC>ZIB.OR.N/=0) GO TO 59
      if (U1/=U2) THEN
         ITC=ITC+1
         TC(ITC)=U2
      ENDIF
      N=ITC
   59 CONTINUE
      if (V1/=E(4)) GO TO 60
!---------INVERSION DES NOEUDS
      KMJ=0
      K=2
      I=J
      J=N
      N=I
   60 CONTINUE
      WRITE(LU1) J,N,(V(I),I=1,2),VE
      GO TO 25
   65 CONTINUE
      WRITE(LU2,857)
      REWIND LU1
   70 CONTINUE
      LU0=ITC
      RETURN
  840 FORMAT(17X,'Mailleur TALWEG'///10X,'Ressources fixees a la compilation:'/12X,I5,' Biefs' &
            /12X,I5,' Points par section de donnee '/12X,I5,' Points de sections de calcul en memoire centrale'///)
  849 FORMAT(6(/),' FICHIER DE DONNEES DU PROGRAMME TALWEG'//)
  850 FORMAT(A1,A)
  851 FORMAT(1X,A1,A)
  852 FORMAT(A)
  853 FORMAT(' *** ERREUR DE DONNEE---> IL MANQUE LA CARTE # DU BIEF 1')
  854 FORMAT(F6.0,12F6.2)
  855 FORMAT(/' *** ERREUR DE DONNEE---> LE FICHIER D''ENTREE EST VIDE')
  856 FORMAT(A1,2A3,F5.0,F5.5,A)
  857 FORMAT(//' ---> FIN DU FICHIER DE DONNEES'/////)
      END

!******************************************************************************
!
      SUBROUTINE TAL1(IF8,IF81)
!
      USE PARAMETRES,ONLY : Long
      USE PARAMETRES_TAL,ONLY : ZIP,ZIB,ZIDMAX,ZIB1
      USE CAR,ONLY : TIT,TC,P
      USE TR01,ONLY : X,Z,U,DX
      USE TR1,ONLY : PAS,SU
      USE FICHIERTAL,ONLY : LU1,LU2,LU7,LU0
      USE TE1,ONLY : LI,IBMAX,LIT,ID,IB,IPA,IF1,IF21,IF3,IB0,IF0
      USE TE2_TE3,ONLY : LS,N3,N4,N5,N6,N7,N8
      use mage_utilitaires, only: is_zero
      IMPLICIT NONE ; SAVE
!=======================================================================
      INTEGER,INTENT(inout) :: IF81,IF8
      CHARACTER TITB*62,V,AUX1*3,AUX2*3
      INTEGER :: I,I2,I4,J,L
      real(kind=Long) :: U1,U2,W
!
!
      IF81=0
      if (IF1/=1 .AND. IF1/=2) THEN
         IF0=1
         IB0=IB
         IB=IB+1
         IPA=IPA+1
!=========(TIT)=TITRE DE L'ETUDE
         if (IB==1) THEN
            if (ZIP<2 .OR. ZIP>24) IF1=IF1-1
            if (ZIDMAX<ZIP+ZIP+2) IF1=IF1-4
            if (IF1<-6) RETURN
            WRITE(LU2,797) TIT
            WRITE(LU7) ZIB,ZIB*ZIB,ZIDMAX,ZIP
            WRITE(LU7) TIT
!=========(TC)=TABLE DE CORRESPONDANCE ENTRE NOEUDS SYMBOLIQUE ET NUMERIQUE
            if (LU0/=0) WRITE(LU7) LU0,(TC(I),I=1,LU0)
            I2=12
            I4=24
            if (ZIP<I2) I2=ZIP
            if (ZIP<I4) I4=ZIP
         ENDIF
!=========(N3)=NOEUD AMONT,(N4)=NOEUD AVAL,(PAS)=PAS D'ESPACE
!=========(SU)=SINUOSITE,(X)=TITRE DU BIEF
         IF1=5
         READ (LU1,END=45) N3(IB),N4(IB),PAS(IB),SU(IB),TITB
         IF1=-6
         if (IB>ZIB) RETURN
         J=N3(IB)
         L=N4(IB)
!-----detection noeud amont = noeud aval
         if (J==L) THEN
            IF81=-1
            IF8=-1
         ENDIF
!-----detection noeud blanc
         if (J>0) THEN
            AUX1=TC(J)
            ELSE
            AUX1='   '
            IF81=-1
            IF8=-1
         ENDIF
         if (L>0) THEN
            AUX2=TC(L)
         ELSE
            AUX2='   '
            IF81=-1
            IF8=-1
         ENDIF
         WRITE (LU2,799) IB,TITB,AUX1,AUX2
         WRITE (LU7) IB,N3(IB),N4(IB),TITB
         if (int(1000._long*PAS(IB)) == 0) THEN
            WRITE (LU2,800)
            PAS(IB)=500._long
         ENDIF
         if (PAS(IB)<0._long) THEN
            WRITE (LU2,801)
            PAS(IB)=-PAS(IB)
         ENDIF
         if (is_zero(SU(IB))) WRITE (LU2,802)
         if (SU(IB)<0._long .OR. SU(IB)>1._long) WRITE (LU2,803)
         if (SU(IB)<=0._long .OR. SU(IB)>1._long) SU(IB)=1._long
         WRITE (LU2,804) PAS(IB),SU(IB)
         IBMAX=IB
         if (IF8==0) THEN
!-------N5(L)=NOMBRE DE FOIS OU L EST NOEUD AMONT
!-------N6(L)=NOMBRE DE FOIS OU L EST NOEUD AVAL
!-------N7(J,L)=NUMEROS DES J BIEFS PARTANT DU NOEUD AMONT L
!-------(N8)=SAUVEGARDE DE (N5) DETRUIT ULTERIEUREMENT
            L=N3(IB)
            J=N5(L)+1
            N5(L)=J
            N8(L)=J
            N7(J,L)=IB
            L=N4(IB)
            N6(L)=N6(L)+1
         ENDIF
      ENDIF
      IF21=0
      U1=0._long
      U2=0._long
!=========(X)=ABSCISSES OU LARGEURS DE LA SECTION DE DONNEE
      if (I2==12) READ (LU1,END=45) P,U,(X(I),I=1,I2)
      if (I2<12) READ(LU1,END=45) P,U,(X(I),I=1,I2),U1
      IF1=3
      if (P=='*') RETURN
      IF1=-3
      if (P=='''') RETURN
      L=1
      ID=I2
      if (P=='+') THEN
         P=' '
         L=2
         if (I4>12) ID=I4
         if (I4>12 .AND. I4<24) READ(LU1,END=45) V,W,(X(I),I=13,I4),U1
         if (I4==24) READ (LU1,END=45) V,W,(X(I),I=13,I4)
         if (I4<=12) READ(LU1,END=45) V
         if (V=='*' .OR. V=='''') RETURN
      ENDIF
!=========(Z)=COTES DE LA SECTION DE DONNEE
      if (I2==12) READ(LU1,END=45) V,W,(Z(I),I=1,I2)
      if (I2<12) READ(LU1,END=45) V,W,(Z(I),I=1,I2),U2
      if (V=='*' .OR. V=='''') RETURN
      LIT=1
      J=1
      IF3=0
      if (V=='M') LIT=2
      if (V=='S' .OR. V==' ') J=2
!---------LU0=1 ON CONSERVE L'ABSCISSE DE POSITION DE LA SECTION DE DONNEE
!---------LU0=0 ON NE CONSERVE PAS L'ABSCISSE DE POSITION DE LA SECTION DE DONNEE
      LU0=0
      if (V==' ') LU0=1
      if (LIT/=1) THEN
         LI=2
         if (LS(IB,2)/=0) THEN
            if (is_zero(DX(2)-W) .AND. is_zero(W)) IF3=1
         ENDIF
      ENDIF
      DX(LIT)=W
      if (L==2) THEN
         if (I4>12 .AND. I4<24) READ(LU1,END=45) V,W,(Z(I),I=13,I4),U2
         if (I4==24) READ (LU1,END=45) V,W,(Z(I),I=13,I4)
         if (I4<=12) READ(LU1,END=45) V
         if (V=='*' .OR. V=='''') RETURN
         if (I4<=12) IF21=2
      ENDIF
      if (.not.is_zero(U1) .OR. .not.is_zero(U2)) IF21=2
      IF1=J
45    CONTINUE
!
797   FORMAT(1X,79('-')/1X,A/1X,79('-')///)
799   FORMAT(/////1X,32('='),'> BIEF ',I4,' <',32('=')/6X,A//6X,'NOEUD AMONT:',1X,A,35X,'NOEUD AVAL:',1X,A &
             /19X,'^^^',47X,'^^^'//)
800   FORMAT(' >>> PAS D''ESPACE NUL,ON PREND LA VALEUR IMPLICITE')
801   FORMAT(' >>> PAS D''ESPACE NEGATIF,ON PREND LA VALEUR ABSOLUE')
802   FORMAT(' >>> SINUOSITE NULLE DU LIT MOYEN,ON PREND LA VALEUR IMP','LICITE')
803   FORMAT(' >>> ERREUR DE SINUOSITE DU LIT MOYEN,ON PREND LA VALEUR',' IMPLICITE')
804   FORMAT(6X,'PAS DE CALCUL:',F10.2,5X,'SINUOSITE:',F10.6)
!
      END SUBROUTINE TAL1
!******************************************************************************

!
!
      SUBROUTINE TAL2
!
      USE PARAMETRES,ONLY:Long,ToutPetit
      USE PARAMETRES_TAL,ONLY : ZIP,ZIB,ZIB1
      USE CAR,ONLY : P
      USE TR01,ONLY : X,Z,U,PP,DX,AX,XC,ZC,PC,AXD
      USE TR1,ONLY : CO
      USE FICHIERTAL,ONLY : LUA,LU0
      USE TE1,ONLY : LIT,ID,IB,IPA,IF1,If2,IF3
      USE TE2_TE3,ONLY : LS,IPAS
      use mage_utilitaires, only: is_zero

      IMPLICIT NONE ; SAVE
!=======================================================================
      INTEGER I,J,K,L,ICAF
      real(kind=Long) :: V,W
!
      ICAF=0
      if (LS(IB,LIT)/=0) THEN
         if (is_zero(AX(LIT)-U)) then
            IF3=IF3+2
         else
!=========TEST DE MONOTONIE DES ABSCISSES DE POSITION
!---------CO(IB,LIT)=-1. SI ABSCISSES DE POSITION CROISSANTE
!---------CO(IB,LIT)=1.SI ABSCISSES DE POSITION DECROISSANTE
            W=1._long
            if (U>AX(LIT)) W=-W
            if (is_zero(CO(IB,LIT))) CO(IB,LIT)=W
            if (.not.is_zero(W-CO(IB,LIT))) IF3=IF3-2
         ENDIF
      ENDIF
      if (LIT/=1 .AND. DX(2)<0._long) IF3=IF3-5
      LS(IB,LIT)=LS(IB,LIT)+1
      if (LS(IB,LIT)==1) AXD(LIT)=U
      AX(LIT)=U
!=========U=ABSCISSES DE POSITIONNEMENT NECESSAIRES POUR LE CALCUL DES
!=========PAS D'ESPACE
      if (LIT==1) THEN
         J=IPA
         if (LS(IB,1)==1) WRITE(LUA,REC=J) U,ICAF
         if (LS(IB,1)==1) IPA=IPA+1
         J=IPA
         WRITE(LUA,REC=J) U,LU0
         IPAS(IB)=IPA
         if (IF1==2 .OR. LU0==1) THEN
            IPA=IPA+1
            J=IPA
            WRITE(LUA,REC=J) U,ICAF
            IPAS(IB)=IPA
         ENDIF
      ENDIF
      IF2=1
      if (P/=' ') RETURN
!=========ID=NOMBRE DE POINTS EFFECTIFS DE LA SECTION DE DONNEE
      J=ID
      DO I=1,J
         if (.not.is_zero(Z(ID)) .OR. .not.is_zero(X(ID))) EXIT
         ID=ID-1
      ENDDO
      IF2=-2
      if (ID<2) RETURN
!=========TEST DE MONOTONIE DES COTES
      IF2=2
      I=1
      DO J=2,ID
         if (.not.is_zero(Z(J)-Z(I))) THEN
            L=1
            if (Z(J)<Z(I)) L=-1
            if (I==1) K=L
            if (L/=K) RETURN
            I=J
         ENDIF
      ENDDO
      if (I==1) RETURN
!=========LA SECTION DE DONNEE EST EN LARGEUR COTE
!---------CLASSEMENT DES COTES DANS L'ORDRE CROISSANT
      IF2=0
      if (K==-1) IF2=-1
      I=0
      DO J=1,ID
         L=J
         if (K==-1) L=ID-I
         XC(L)=X(J)
         ZC(L)=Z(J)
         I=J
      ENDDO
!---------DECALAGE DES COTES IDENTIQUES ET CALCUL DES PERIMETRES MOUILLES
      I=1
      PC(1)=XC(1)
      DO J=2,ID
         if (ZC(J)<=ZC(I)) ZC(J)=ZC(I)+ToutPetit
         if (LIT/=2) THEN
            U=XC(J)-XC(I)
            V=ZC(J)-ZC(I)
            V=V+V
            PC(J)=PC(I)+SQRT(U*U+V*V)
            I=J
         ENDIF
      ENDDO
!---------P=COTE DE LA BERGE
      PP=ZC(ID)
      U=PP
!
      END SUBROUTINE TAL2
!
!


!=======================================================================
!=======================================================================

      SUBROUTINE TAL3

      USE PARAMETRES,ONLY: Long,Pi,ToutPetit
      USE PARAMETRES_TAL,ONLY : ZIP,ZIB,ZIB1
      USE CAR,ONLY : P
      USE TR01,ONLY : X,Z,U,XC,ZC,PC
      USE TE1,ONLY : LIT,ID,IF21
      use, intrinsic :: iso_fortran_env, only: error_unit
      use mage_utilitaires, only: is_zero
      IMPLICIT NONE ; SAVE

      CHARACTER E(7)
      INTEGER :: I,J,N,I1
      real(kind=Long) :: A,B,C,D,TETA
      DATA E/'T','R','D','P','C','F','O'/
!=======================================================================
!
!=========SECTION PARAMETREE
      IF21=0
      ID=ZIP
      N=ID-1
      ZC(1)=Z(1)
      XC(1)=0._long
      if (LIT==1) PC(1)=0._long
      if (X(1)<0._long .OR. (P==E(2) .AND. is_zero(X(1)))) IF21=-1
      D=X(1)+X(1)
      if (P==E(5) .AND. is_zero(Z(2))) Z(2)=Z(1)+D
      if (P/=E(6) .AND. Z(2)<=Z(1)) IF21=IF21-2
      U=Z(2)
      J=0
      DO I=1,7
         if (P==E(I)) THEN
            J=1
            if (I>=4) THEN
               if (is_zero(X(1))) IF21=IF21-4
            ENDIF
         ENDIF
      ENDDO
      if (J==0) IF21=-5
      if (IF21<0) RETURN
      SELECT CASE (P)
         CASE ('T','R','D')
!=========TRAPEZE,TRIANGLE,RECTANGLE,DALOT
            ID=2
            if (P==E(2)) X(2)=0._long
            B=X(1)*0.5_long
            A=B+X(2)*(Z(2)-Z(1))
            D=Z(2)
            if (A<0._long) IF21=1
            if (A<0._long) D=-B/X(2)+Z(1)
            XC(1)=X(1)
            ZC(2)=D
            B=2._long*(D-Z(1))
            XC(2)=X(1)+X(2)*B
            if (A<0._long) XC(2)=0._long
            if (A>=0._long .AND. P==E(3)) THEN
               ID=3
               XC(ID)=0._long
               ZC(ID)=D+ToutPetit
               U=ZC(ID)
            ENDIF
            if (LIT==2) RETURN
            PC(1)=X(1)
            PC(2)=X(1)+B*SQRT(X(2)*X(2)+1._long)
            if (P==E(3)) PC(ID)=PC(2)+XC(2)
         CASE ('P')
!=========PUISSANCE
            if (X(2)<=0._long .OR. X(2)>=1._long) IF21=-7
            if (IF21==-7) RETURN
            A=X(1)/real(N,kind=long)
            B=1._long/X(2)
            X(1)=X(1)/(Z(2)-Z(1))**X(2)
            DO I=1,N
               J=I+1
               XC(J)=XC(I)+A
               ZC(J)=(XC(J)/X(1))**B+Z(1)
               if (LIT/=2) THEN
                  C=0.5_long*(XC(J)-XC(I))
                  D=ZC(J)-ZC(I)
                  PC(J)=PC(I)+2._long*SQRT(C*C+D*D)
               ENDIF
            ENDDO
         CASE ('C')
!=========CERCLE
            IF21=-8
            if (ZIP<5) RETURN
            IF21=0
            A=Z(2)-Z(1)
            C=A+ToutPetit
            TETA=Pi
            if (C<D) THEN
               TETA=0.5_long*Pi
               if (.not.is_zero(A-X(1))) THEN
                  B=1._long - A/X(1)
                  B=B*B
                  B=-1._long + 1._long/B
                  B=SQRT(B)
                  TETA=ATAN(B)
                  if (A>X(1)) TETA=Pi-TETA
               ENDIF
            ENDIF
            TETA=TETA/real(N,kind=long)
            B=TETA
            DO I=1,N
               J=I+1
               ZC(J)=X(1)*(1._long-COS(B))+Z(1)
               XC(J)=D*SIN(B)
               if (LIT==1) PC(J)=D*B
               B=B+TETA
            ENDDO
            if (C<D) RETURN
            XC(ID)=0._long
            ZC(ID)=Z(1)+D
            if (Z(2)>ZC(ID)) IF21=3
            U=ZC(ID)
            if (LIT==1) PC(ID)=PC(J)
         CASE ('F')
!=========FER A CHEVAL
            IF21=-8
            if (ZIP<5) RETURN
            IF21=0
            TETA=Pi/6._long
            N=ID/2-1
            TETA=TETA/real(N,kind=long)
            B=TETA
            DO I=1,N
               J=I+1
               ZC(J)=X(1)*(1._long-COS(B))+Z(1)
               XC(J)=2._long*X(1)*SIN(B)
               if (LIT==1) PC(J)=2._long*X(1)*B
               B=B+TETA
            ENDDO
            I1=J+1
            XC(I1)=XC(J)
            ZC(I1)=ZC(J)+X(1)/4._long
            if (LIT==1) PC(I1)=PC(J)+X(1)/2._long
            N=ID-I1
            TETA=0.5_long*Pi/real(N,kind=long)
            B=TETA
            J=I1
            DO I=1,N
               J=J+1
               XC(J)=X(1)*COS(B)
               ZC(J)=0.5_long*X(1)*SIN(B)+ZC(I1)
               if (LIT==1) PC(J)=X(1)*B+PC(I1)
               B=B+TETA
            ENDDO
            XC(ID)=0._long
            U=ZC(ID)
         CASE ('O')
            IF21=-5
         CASE DEFAULT
            CALL TA1
            !WRITE(lu(12),'(a)') 'TAL3 : cas non defini'
            !write(lu(12),'(a)') ' Merci d''envoyer un rapport de bug'
            write(error_unit,'(a)') 'TAL3 : cas non defini'
            write(error_unit,'(a)') ' Merci d''envoyer un rapport de bug'
            STOP 219
      END SELECT
!
      END SUBROUTINE TAL3
!
      SUBROUTINE TAL4
!
      USE PARAMETRES,ONLY: Long,ToutPetit
      USE PARAMETRES_TAL,ONLY : ZIP
      USE TR01,ONLY : X,Z,U,PP,XC,ZC,PC
      USE TE1,ONLY : LIT,ID,IF2
      USE TE5,ONLY : N0,NC
      use mage_utilitaires, only: is_zero
!
      IMPLICIT NONE ; SAVE

      INTEGER PrePT,DerPT
      INTEGER I,I0,I1,I2,I3,I4,I5,K,J1,J2,J3,J4
      real(kind=Long) :: P3,P4
!=========RECHERCHE DES CHENEAUX
!---------I=POINT SOMMET D'UN CHENAL N0(I)=1
!---------I=POINT CREUX D'UN CHENAL  N0(I)=-1
!---------I=POINT QUELCONQUE D'UN CHENAL N0(I)=0
      I=1
      I1=0
      DO I2=2,ID
         I3=0
         if (Z(I2)<Z(I)) I3=1
         N0(I)=I3-I1
         I=I2
         I1=I3
      ENDDO
      N0(ID)=1-I3
      I2=ID
      DO I=2,ID
         I1=I2-1
         if (.not.is_zero(Z(I1)-Z(I2))) EXIT
         N0(I2)=0
         if (N0(I1)==0) N0(I1)=1
         I2=I1
      ENDDO
!=========ELIMINATION DES POINTS SUPERFLUS
!---------PrePT=PREMIER POINT DE LA SECTION APRES ELIMINATION
!---------DerPT=DERNIER POINT DE LA SECTION APRES ELIMINATION
      PrePT=0
      DerPT=0
      I4=ID
      DO I=1,ID
         if (PrePT==0.AND.N0(I)>0) PrePT=I
         if (DerPT==0.AND.N0(I4)>0) DerPT=I4
         if (PrePT/=0.AND.DerPT/=0) EXIT
         I4=I4-1
      ENDDO
      if (PrePT/=1) IF2=IF2+1
      if (DerPT/=ID) IF2=IF2+2
      if (DerPT<PrePT+2) IF2=-IF2
      if (IF2<0) RETURN
!=========(NC)=CLASSEMENT DES INDICES DES POINTS CORRESPONDANT
!=========AU CLASSEMENT DES COTES DANS L'ORDRE CROISSANT
      ID=0
      DO I=PrePT,DerPT
         ID=ID+1
         I2=ID
         if (I/=PrePT) THEN
            DO I3=1,I4
               K=NC(I3)
               if (Z(K)>Z(I)) THEN
                  DO K=I3,I4
                     I1=I2-1
                     NC(I2)=NC(I1)
                     I2=I1
                  ENDDO
                  EXIT
               ENDIF
            ENDDO
         ENDIF
         NC(I2)=I
         I4=ID
      ENDDO
!---------ZC CLASSEMENT DES COTES DANS L'ORDRE CROISSANT ET DECALAGE
!---------DES COTES IDENTIQUES
      DO I2=1,ID
         I=NC(I2)
         ZC(I2)=Z(I)
         if (I2/=1.AND.ZC(I2)<=ZC(I1)) ZC(I2)=ZC(I1)+ToutPetit
         Z(I)=ZC(I2)
         I1=I2
         XC(I2)=0._long
         PC(I2)=0._long
      ENDDO
!=========CALCUL XC=LARGEUR,PC=PERIMETRE MOUILLE CORRESPONDANT A ZC
      I1=PrePT
      I2=0
      I5=PrePT+1
      DO K=I5,DerPT
         if (N0(K)<0) I0=K
         if (N0(K)>0) I2=K
         if (I2/=0) THEN
!---------I1 PREMIER POINT DU CHENAL A GAUCHE,I0 POINT DU CREUX
!---------I2 DERNIER POINT DU CHENAL A DROITE
!---------J1 INDICE DU POINT COURANT SUPERIEUR GAUCHE
!---------J2 INDICE DU POINT COURANT SUPERIEUR DROIT
            I3=0
            I4=0
            J1=I0
            J2=I0
            DO I=1,ID
               if (ZC(I)>Z(I0)) THEN
                  if (ZC(I)>Z(J1).AND.J1>I1) J1=J1-1
                     J3=J1+1
                  U=0._long
                  if (ZC(I)<Z(J1)) THEN
                     U=(X(J1)-X(J3))*(Z(J1)-ZC(I))/(Z(J1)-Z(J3))
                  ENDIF
                  U=U-X(J1)
                  XC(I)=XC(I)+U
                  if (LIT/=2) THEN
                     if (I3/=J1) P3=0._long
                     if (ZC(I)<=Z(I1)) THEN
                        U=SQRT((X(J3)+U)**2+(ZC(I)-Z(J3))**2)
                        PC(I)=PC(I)+U-P3
                     ENDIF
                     I3=J1
                     P3=U
                     IF (ZC(I)>Z(PrePT) .AND. I1==PrePT) THEN
                        PC(I)=PC(I)+ZC(I)-ZC(I-1)
                     ENDIF
                  ENDIF
                  if (ZC(I)>Z(J2).AND.J2<I2) J2=J2+1
                  J4=J2-1
                  U=0._long
                  IF (ZC(I)<Z(J2)) THEN
                     U=(X(J2)-X(J4))*(ZC(I)-Z(J2))/(Z(J2)-Z(J4))
                  ENDIF
                  U=U+X(J2)
                  XC(I)=XC(I)+U
                  if (LIT/=2) THEN
                     if (I4/=J2) P4=0._long
                     if (ZC(I)<=Z(I2)) THEN
                        PP=SQRT((U-X(J4))**2+(ZC(I)-Z(J4))**2)
                        PC(I)=PC(I)+PP-P4
                     ENDIF
                     I4=J2
                     P4=PP
                     IF (ZC(I)>Z(DerPT) .AND. I2==DerPT) THEN
                        PC(I)=PC(I)+ZC(I)-ZC(I-1)
                     ENDIF
                  ENDIF
               ENDIF
            ENDDO
            I1=I2
            I2=0
         ENDIF
      ENDDO
      DO I=1,ID
         XC(I)=ABS(XC(I))
      ENDDO
      if (LIT==2) RETURN
      K=1
      DO I=2,ID
         PC(I)=PC(K)+PC(I)
         K=I
      ENDDO
!yG------ à voir ci dessous
!---------U,PP=COTES DES BERGES GAUCHE ET DROITE
      U=Z(PrePT)
      PP=Z(DerPT)
!
      END SUBROUTINE TAL4
!=======================================================================
!=======================================================================
      SUBROUTINE TAL5(IF81)
!
      USE PARAMETRES,ONLY : Long
      USE PARAMETRES_TAL,ONLY : ZIP,ZIB,ZIDMAX,ZIB1
      USE CAR,ONLY : P
!!!      USE TR01,ONLY : U,PP,DX,AX,XC,ZC,PC,AXD       !-JBF 02/04/2008
      USE TR01,ONLY : DX,AX,XC,ZC,PC,AXD,B1=>U,B2=>PP  !+JBF 02/04/2008
      USE TR1,ONLY : CO
      USE FICHIERTAL,ONLY : LU2,LU3,LU4,LU0
      USE TE1,ONLY : LIT,ID,IB,IF1,IF2,IF21,IF3,IF4,IB0,IF0
      USE TE2_TE3,ONLY : LS
      use mage_utilitaires, only: is_zero
      IMPLICIT NONE ; SAVE

!=======================================================================
      INTEGER,INTENT(inout) :: IF81
      real(kind=Long) :: XTEST,UTEST
      CHARACTER,DIMENSION(2) :: A*4
      INTEGER :: I,J,K,L,N,II
      real(kind=Long) :: B
      real(kind=long) :: U   !+JBF 02/04/2008
!
      A(1) = 'MINE'
      A(2) = 'MAJE'
      XTEST = 1000._Long
      UTEST = 10._Long
!========= IMPRESSION SUR LU2
!--------- IF81 controle la validite des noeuds
!========= IF1:CONTROLE LA PROGRESSION DES SECTIONS DANS LE BIEF
      if (IF81<0) WRITE(LU2,794)
      if (IF1<=-7) WRITE (LU2,795)
      if (IF1<=-10) WRITE(LU2,796) ZIDMAX,ZIP
      if (IF1==-7.OR.IF1==-9) WRITE(LU2,798) ZIP
      if (IF1==-11.OR.IF1==-13) WRITE(LU2,798) ZIP
      if (IF1==-6) WRITE(LU2,799) ZIB
      if (IF1==-3) WRITE(LU2,802) IB
      if (IF1==1 .OR. IF1==2) WRITE(LU2,803) IB,A(LIT),LS(IB,LIT),AX(LIT)
      if (IF1==2 .AND. LU0==0) WRITE(LU2,804)
      if (IF1==3) THEN
         if (LS(IB,2)==0) WRITE(LU2,805) IB
         if (LS(IB,1)==0) WRITE(LU2,806) IB
         if (LS(IB,1)==0) IF1=-2
         DO I=1,2
            if (is_zero(CO(IB,I)) .AND. LS(IB,I)/=0) THEN
               WRITE(LU2,807) A(I),IB
               IF1=-2
            ENDIF
         ENDDO
         if (.not.is_zero(CO(IB,1)) .AND. .not.is_zero(CO(IB,2))) THEN
            if (.not.is_zero(CO(IB,1)-CO(IB,2))) THEN
               WRITE(LU2,808) IB
               IF1=-2
            ENDIF
         ENDIF
         if (IF1/=-2 .AND. LS(IB,2)/=0) THEN
            DO I=1,2
               AXD(I)=AXD(I)*CO(IB,I)
               AX(I)=AX(I)*CO(IB,I)
            ENDDO
            I=1
            if (AXD(2)<AX(1) .OR. AXD(2)>AXD(1)) I=0
            if (AX(2)<AX(1) .OR. AX(2)>AXD(1)) I=0
            if (I==0) THEN
               WRITE(LU2,809) IB
               IF4=-1
               if (IF0>0) IF0=-IF0
            ENDIF
         ENDIF
         if (IABS(IF0)==LS(IB,2)) THEN
            if (LS(IB,2)/=1) THEN
               WRITE(LU2,810) IB
               IF4=-1
               IF0=-1
            ENDIF
         ENDIF
         if (IF1/=-2 .AND. IF0>=0) THEN
            WRITE(LU2,811) IB
            if (IB0/=0) THEN
               LS(IB,1)=LS(IB,1)+LS(IB0,1)
               LS(IB,2)=LS(IB,2)+LS(IB0,2)
            ENDIF
         ENDIF
      ENDIF
      if (IF1<0) IF4=-1
      if (IF1>3.AND.IF4==1) WRITE(LU2,812)
      if (IF1/=1.AND.IF1/=2) RETURN
!=========IF2 ET IF21 :CONTROLENT LA VALIDITE DES COORDONNEES DES POINTS
!=========DE LA SECTION
      if (IF2==-1) WRITE(LU2,814)
      if (IF2==0) WRITE(LU2,815)
      if (IF2==1) WRITE(LU2,816) P
!!!      if (IF2==1) PP=U  !-JBF 02/04/2008
      if (IF2==1) B2=B1    !+JBF 02/04/2008
      if (IF2>=2) WRITE(LU2,817)
      if (IF21>=0) THEN
         if (IF2>=-1) THEN
!YG -- largeur cote
!---------IMPRESSION SUR LU2 DES SECTIONS DE DONNEES
!---------TRANSFORMEES EN LARGEURS-COTES
            WRITE(LU2,818) (XC(I),I=1,ID)
            WRITE(LU2,819) (ZC(I),I=1,ID)
            if (LIT==1) WRITE(LU2,820) (PC(I),I=1,ID)
!!!            WRITE(LU2,8205) MIN(U,PP)  !-JBF 02/04/2008
            WRITE(LU2,8205) MIN(B1,B2)    !+JBF 02/04/2008
         ENDIF
         !write(l9,*) 'cotes de berges 1 : ',B1,B2
         if (IF2==3) WRITE(LU2,821)
         if (IF2==4) WRITE(LU2,822)
         if (IF2==5) WRITE(LU2,823)
         if (IF2==-2) WRITE(LU2,824)
         if (IF2<-2) WRITE(LU2,825)
         if (IF2<-1) IF4=-1
         if (IF2<-1.AND.IF0>0) IF0=-IF0
         if (IF21==3) WRITE(LU2,8255)
         if (IF21==2) WRITE(LU2,826)
         if (IF21==1) WRITE(LU2,827)
      ENDIF
      if (IF21==-1.OR.IF21==-3) WRITE(LU2,828)
      if (IF21==-2) WRITE(LU2,829)
      if (IF21==-3.OR.IF21==-6) WRITE(LU2,829)
      if (IF21==-4.OR.IF21==-6) WRITE(LU2,830)
      if (IF21==-5) WRITE(LU2,831)
      if (IF21==-7) WRITE(LU2,832)
      if (IF21==-8) WRITE (LU2,833)
      if (IF21<0) IF4=-1
      if (IF21<0.AND.IF0>0) IF0=-IF0
!=========IF3:CONTROLE LA VALIDITE DES ABSCISSES DE POSITION
!=========DES SECTIONS
      if (IF3==-7) WRITE (LU2,834)
      if (IF3==-1.OR.IF3==-2) WRITE(LU2,834)
      if (IF3<-2) WRITE(LU2,835)
      if (IF3<0) IF4=-1
      if (IF3<0.AND.IF0>0) IF0=-IF0
      if (IF3==1.OR.IF3==3) WRITE (LU2,836)
      if (IF3==2.OR.IABS(IF3)==3) WRITE (LU2,837)
      if (IF21<0) RETURN
!=========VERIFICATION DES LARGEURS APRES TRANSFORMATION
!=========DE LA SECTION EN LARGEURS-COTES
!=========VERIFICATION DE LA COTE DE DEBORDEMENT MINEUR-MOYEN
!========= N=-1 DETECTION D'ERREURS GROSSIERES SUR LARGEURS OU COTES
      if (LIT==1) THEN
!!!         B=U  !-JBF 02/04/2008
         B=B1    !+JBF 02/04/2008
!!!         if (PP>U) B=PP  !-JBF 02/04/2008
         if (B2 > B1) B=B2  !+JBF 02/04/2008
         if (is_zero(DX(1))) DX(1)=B
         if (DX(1)>B) WRITE(LU2,838)
         if (DX(1)>B) DX(1)=B
         if (DX(1)<ZC(1)) WRITE(LU2,839)
         if (DX(1)<ZC(1)) DX(1)=B
      ENDIF
      !write(l9,*) 'cotes de berges 2 : ',U,PP
      K=1
      L=1
      J=1
      N=0
      U=ZC(1)
      DO I=1,ID
         U=ZC(I)-U
         if (XC(I)>XTEST.OR.U>UTEST) N=-1
         U=ZC(I)
         if (XC(I)<0._long)K=-1
         if (is_zero(XC(I)) .AND. I/=1 .AND. I/=ID) K=-1
         if (LIT==1) THEN
            if (XC(I)<XC(J).AND.DX(1)<ZC(I)) L=-1
            J=I
         ENDIF
      ENDDO

      !write(l9,*) 'cotes de berges 3 : ',U,PP

      if (N<0) WRITE(LU2,8395)
      if (LIT==1) THEN
         if (K/=-1 .AND. L/=1) THEN
            WRITE(LU2,840)
            DX(1)=B
         ENDIF
         WRITE(LU2,841) DX(1)
      ENDIF
      if (K/=1) THEN
         WRITE(LU2,842)
         IF4=-1
         if (IF0>0) IF0=-IF0
      ENDIF
      if (LIT==2) THEN
         WRITE(LU2,843) DX(2)
         K=1
         if (IF0<0) K=-1
         if (is_zero(DX(2)) .AND. LS(IB,2)/=1) IF0=IF0+K
         if (.not.is_zero(DX(2)) .AND. LS(IB,2)==1) THEN
            WRITE(LU2,844) IB
            IF4=-1
            if (IF0>0) IF0=-IF0
         ENDIF
      ENDIF


!=========ENREGISTREMENT DES SECTIONS DE DONNEES SUR LU3 ET LU4
!=========EN VU DE L'INTERPOLLATION DES SECTIONS DE CALCUL
!=========SI IF4=-1 ARRET DE L' ENREGISTREMENT
      if (IF4/=1) RETURN
      I=LS(IB,LIT)
      if (IB0/=0) I=LS(IB0,LIT)+I
      !write(l9,*) 'Tal5 : ',ID,AX(1),DX(1),U,PP
!!!      if (LIT==1) WRITE(LU3,REC=I) ID,AX(1),DX(1),U,PP,  !-JBF 02/04/2008
      if (LIT==1) WRITE(LU3,REC=I) ID,AX(1),DX(1),B1,B2,(XC(II),ZC(II),PC(II),II=1,ID)
      if (LIT==2) WRITE(LU4,REC=I) ID,AX(2),DX(2),(XC(II),ZC(II),II=1,ID)
!
794   FORMAT(//' *** ERREUR DE DONNEE---> NOEUD AMONT ou AVAL NON VALIDE'/)
795   FORMAT(//' *** ERREUR DE DIMENSIONNEMENT DES MODULES :')
796   FORMAT(5X,'L''ESPACE MEMOIRE RESERVE POUR LES SECTIONS DE CALCUL' &
               ,' N''EST PAS SUFFISANT'/5X,'(',I2,' INFERIEUR A 2*',I2,'+2)')
798   FORMAT(5X,'PLACES RESERVEES PAR SECTION DE DONNEE = ',I4,' (AUTORISEES = 2 A 24)')
799   FORMAT(//' *** ERREUR DE DONNEE---> LE NOMBRE DE BIEFS EST LIMITE A ',I3/)
802   FORMAT(//' *** ERREUR DE DONNEE---> IL MANQUE DES LIGNES POUR DEFINIR LE BIEF ',I3/)
803   FORMAT(//' BIEF ',I3,'  LIT ',A4,'UR  SECTION NUMERO ',I4,/' ABSCISSE DE POSITION: ',F8.1)
804   FORMAT(' /// CETTE SECTION EST SINGULIERE')
805   FORMAT(/' LE BIEF ',I3,' N''A PAS DE LIT MAJEUR')
806   FORMAT(//' *** ERREUR DE DONNEE---> LE BIEF ',I3, ' N''A PAS DE LIT MINEUR'/)
807   FORMAT(//' *** ERREUR DE DONNEE---> IL Y A MOINS DE 2 SECTIONS A' &
               ,'YANT DES ABSCISSES',/'     DE POSITION DIFFERENTES POUR DEFINIR' &
               ,' LE LIT ',A4,'UR DU BIEF ',I3/)
808   FORMAT(//' *** ERREUR DE DONNEE---> BIEF ',I3,'  L''ORIENTATION' &
             ,' DES ABSCISSES DE POSITION'/'     DU LIT MAJEUR EST DIFFERENTE',' DE CELLE DU LIT MINEUR'/)
809   FORMAT(//' *** ERREUR DU LIT MAJEUR DANS LE BIEF ',I3,' --->LES ABSCISSES DE POSITION'/5X, &
               'SONT EN DEHORS DU LIT MINEUR'/)
810   FORMAT(//' *** ERREUR DU LIT MAJEUR DANS LE BIEF ',I3,'---> LES DISTANCES'/5X, &
               '(AUTRES QUE LA PREMIERE) SONT TOUTES NULLES'/)
811   FORMAT(/' ---> FIN DU BIEF ',I3/)
812   FORMAT(/' ---> FIN D''INTRODUCTION DES DONNEES'/)
814   FORMAT(' CETTE SECTION A ETE INTRODUITE EN LARGEURS-COTES DECROISSANTES')
815   FORMAT(' CETTE SECTION A ETE INTRODUITE EN LARGEURS-COTES CROISSANTES')
816   FORMAT(' CETTE SECTION A ETE INTRODUITE SOUS FORME PARAMETREE : ',A1)
817   FORMAT(' CETTE SECTION A ETE INTRODUITE EN ABSCISSES-COTES')
818   FORMAT(' LARGEURS  :',8F8.2)
819   FORMAT(' COTES     :',8F8.2)
820   FORMAT(' PERIMETRES:',8F8.2)
8205  FORMAT(' COTE DE LA BERGE LA PLUS BASSE:',F8.2)
821   FORMAT(' >>> POINTS SUPPRIMES A GAUCHE DE LA SECTION ORIGINALE')
822   FORMAT(' >>> POINTS SUPPRIMES A DROITE DE LA SECTION ORIGINALE')
823   FORMAT(' >>> POINTS SUPPRIMES A GAUCHE ET A DROITE DE LA SECTION ORIGINALE')
824   FORMAT(//' *** ERREUR DE DONNEE---> IL Y A MOINS DE 2 POINTS POUR DEFINIR CETTE SECTION'/)
825   FORMAT(//' *** ERREUR DE DONNEE---> CETTE SECTION EST BIZARRE'/)
8255  FORMAT(' >>> LA COTE DE BERGE EST SUPERIEURE A LA COTE DU FOND+DIAMETRE')
826   FORMAT(' >>> TOUS LES POINTS DE LA SECTION ORIGINALE DE DONNEE' &
             /5X,'N''ONT PAS ETE PRIS EN COMPTE CAR LES PLACES' &
             /5X,'RETENUES DANS LES MODULES NE LE PERMETTENT PAS')
827   FORMAT(' >>> MODIFICATION DE CETTE SECTION (FRUIT NEGATIF)')
828   FORMAT(//' *** ERREUR DE DONNEE---> LE PARAMETRE DE LONGUEUR EST' &
              ,' NEGATIF'/  '                         ou NUL si Rectangle'/)
829   FORMAT(//' *** ERREUR DE DONNEE---> COTE DU FOND SUPERIEURE OU EGALE A LA COTE DE BERGE'/)
830   FORMAT(//' *** ERREUR DE DONNEE---> RAYON OU EQUIVALENT NUL'/)
831   FORMAT(//' *** ERREUR DE DONNEE---> SECTION N''EXISTANT PAS DANS LE CATALOGUE'/)
832   FORMAT(//' *** ERREUR DE DONNEE---> EXPOSANT HORS LIMITES'/5X, &
               '(INTERVALLE AUTORISE:L''OUVERT COMPRIS ENTRE 0 ET 1)'/)
833   FORMAT(//' *** ERREUR DE DONNEE---> IL N''Y A PAS ASSEZ DE PLACES' &
              ,' DANS LES MODULES'/5X,'POUR INTRODUIRE CE TYPE DE SECTIONS')
834   FORMAT(//' *** ERREUR DE DONNEE---> L''ABSCISSE DE POSITION DE CE' &
              ,'TTE SECTION'/'     N''EST PAS MONOTONE'/)
835   FORMAT(//' *** ERREUR DE DONNEE---> CETTE SECTION EST A UNE DIST' &
              ,'ANCE'/'     NEGATIVE DE LA PRECEDENTE'/)
836   FORMAT(' >>> 2 DISTANCES NULLES CONSECUTIVES')
837   FORMAT(' >>> MEME ABSCISSE DE POSITION QUE POUR LA SECTION PRECEDENTE')
838   FORMAT(' >>> LA COTE DE DEBORDEMENT MINEUR-MOYEN EST DANS CETTE' &
            ,' SECTION'/'     SUPERIEURE A LA COTE DE LA BERGE MAXI')
839   FORMAT(' >>> LA COTE DE DEBORDEMENT MINEUR-MOYEN EST DANS CETTE' &
            ,' SECTION'/'     INFERIEURE A LA COTE DU FOND')
8395  FORMAT(' >>> VARIATION RAPIDE DE COTE OU LARGEUR TRES GRANDE DANS CETTE SECTION')
840   FORMAT(' >>> LA COTE DE DEBORDEMENT MINEUR-MOYEN EST DANS CETTE' &
            ,' SECTION'/'     INFERIEURE A UNE COTE CORRESPONDANT A UNE LARGEUR DECROISSANTE')
841   FORMAT(' COTE EFFECTIVE DE DEBORDEMENT MINEUR-MOYEN:',F8.2)
842   FORMAT(//' *** ERREUR DE DONNEE---> LARGEURS NEGATIVES DANS CETTE SECTION'/25X, &
               'OU NULLES AILLEURS QU''AU PREMIER ET DERNIER POINT'/)
843   FORMAT(' DISTANCE PAR RAPPORT A LA SECTION PRECEDENTE DU LIT MAJEUR:',F8.1)
844   FORMAT(//' *** ERREUR DU LIT MAJEUR DANS LE BIEF ',I3 &
              ,'---> LA PREMIERE DISTANCE'/5X,'N''EST PAS NULLE'/)
!
      END SUBROUTINE TAL5
!=======================================================================
!=======================================================================
      SUBROUTINE TAL6
!
      USE PARAMETRES_TAL,ONLY : ZIP,ZIB,ZIB1
      USE CAR,ONLY : TC
      USE FICHIERTAL,ONLY : LU2,LU7
      USE TE1,ONLY : LI,IBMAX,LIT,ID,IB,IPA,IF4
      USE TE2_TE3,ONLY : N3,N4,N5,N6,N9,IUB
      USE TE4,ONLY : IC
      IMPLICIT NONE ; SAVE

!=======================================================================
      CHARACTER(len=3), allocatable ::  U8(:),U9(:)
      character :: U88*3,U89*3,UBB*3
      !DIMENSION U8(ZIB),U9(ZIB),IU9(ZIB)
      INTEGER IFIC0,NEU
      INTEGER,DIMENSION(16) :: FIC1
      INTEGER :: I,N1,N2,LB1,LB2,LB3,L,K,K1,K2
      integer,allocatable :: IU9(:)

      allocate (u8(zib),u9(zib),iu9(zib))
!---------ENREGISTREMENT BIDON SERVANT A LA RELECTURE
      IFIC0=0
      FIC1(1:16)=0
      !NEU=ZIP                                                     !loic
      NEU=ZIB                                                      !loic
      U88='^^^'
      U89='  M'
      UBB='   '
      WRITE(LU7) IFIC0,IFIC0,IFIC0,(FIC1(I),I=1,16)
!=========RECHERCHE DES NOEUDS AMONT ET AVAL DU MODELE
!---------IC(N1)=2 SI N1 EST NOEUD QUELCONQUE
!---------IC(N1)=1 SI N1 EST NOEUD AMONT
!---------IC(I)=0 SI I N'EST PAS UN NOEUD DU MODELE
!---------IC(N2)=-1 SI N2 EST NOEUD AVAL
!---------IB=NOMBRE DE NOEUDS AVAL
      NEU=NEU+1
      LIT=10000
      ID=0
      IPA=0
      IB=0
      DO I=1,IBMAX
         N1=N3(I)
         N2=N4(I)
         if (N1<LIT) LIT=N1
         if (N2<LIT) LIT=N2
         if (N1>ID) ID=N1
         if (N2>ID) ID=N2
         if (N5(N1)/=0.AND.N6(N1)==0) IC(N1)=1
         if (N5(N1)/=0.AND.N6(N1)/=0) IC(N1)=2
         if (N5(N2)==0 .AND. N6(N2)/=0) THEN
            if (IC(N2)==0) IB=IB+1
            IC(N2)=-1
         ENDIF
      ENDDO
!=========verification de la connexite du graphe
      CALL TAL60
      if (IF4==-1) RETURN
!=========CLASSEMENT DES BIEFS
      CALL TAL61
      if (IPA==-1) WRITE(LU2,804)
      if (IPA==0) WRITE(LU2,805)
      if (IPA>0) WRITE(LU2,806)
      if (IPA<0) IF4=-1
      if (IPA<0) RETURN
      LB1=0
      LB2=0
      LB3=0
      DO I=1,IBMAX
         IU9(I)=IABS(IUB(I))
         U8(I)=UBB
         if (IUB(I)<0) U8(I)=U89
         if (IUB(I)<0) LB2=I
         if (IUB(I)>0.AND.LB2==0) LB1=I
         if (IUB(I)>0.AND.LB2>0) LB3=I
      ENDDO
      WRITE(LU2,807)
      L=(IBMAX-1)/20+1
      K2=0
      DO K=1,L
         K1=K2+1
         K2=K2+20
         if (K==L) K2=IBMAX
         WRITE(LU2,808) (IU9(I),I=K1,K2)
         WRITE(LU2,811) (U8(I),I=K1,K2)
      ENDDO
      WRITE (LU7) NEU,(IC(I),I=1,NEU)
      WRITE (LU7) (IUB(I),I=1,IBMAX),LI,IPA,LB1,LB2,LB3
!=========IMPRESSION DES NOEUDS AMONT DU MODELE
      N2=0
      DO N1=1,NEU
         if (IC(N1)==1) THEN
            N2=N2+1
            U9(N2)=TC(N1)
            U8(N2)=U88
         ENDIF
      ENDDO
      WRITE(LU2,809)
      L=(N2-1)/20+1
      K2=0
      DO K=1,L
         K1=K2+1
         K2=K2+20
         if (K==L) K2=N2
         WRITE(LU2,811) (U9(I),I=K1,K2)
         WRITE(LU2,811) (U8(I),I=K1,K2)
      ENDDO
!=========IMPRESSION DES NOEUDS AVAL DU MODELE
!=========RECUPERATION DES NOEUDS AVAL DANS(N9)
!---------N9(1)=NOMBRE D'ELEMENTS DE(N9)
      N1=1
      DO N2=1,NEU
         if (IC(N2)<0) THEN
            L=N1
            N1=N1+1
            U9(L)=TC(N2)
            U8(L)=U88
            N9(N1)=N2
         ENDIF
      ENDDO
      N9(1)=N1
      WRITE(LU2,810)
      N2=(L-1)/20+1
      K2=0
      DO K=1,N2
         K1=K2+1
         K2=K2+20
         if (K==N2) K2=L
         WRITE(LU2,811) (U9(I),I=K1,K2)
         WRITE(LU2,811) (U8(I),I=K1,K2)
      ENDDO
      if (IPA==0) RETURN
      if (N1<=2) THEN
         DO L=1,IBMAX
            I=IABS(IUB(L))
            N9(2)=N4(I)
            if (IUB(L)<0) EXIT
         ENDDO
      ENDIF
!=========ORDRE DE REMPLISSAGE DES MATRICES
      CALL TAL62
!      print*,' TAL62 ok'
      CALL TAL63
!      print*,' TAL63 ok'
804   FORMAT(//' *** ERREUR DE DONNEE---> MAUVAIS REPERAGE DES NOEUDS'/)
805   FORMAT(' MODELE RAMIFIE')
806   FORMAT(' MODELE MAILLE (LES BIEFS DE MAILLE SONT SOULIGNES PAR LA LETTRE M)')
807   FORMAT(//' CLASSEMENT DES BIEFS DE L''AVAL VERS L''AMONT =')
808   FORMAT(20(1X,I3))
809   FORMAT(//' NOEUDS AMONT DU MODELE =')
810   FORMAT(//' NOEUDS AVAL DU MODELE =')
811   FORMAT(20(1X,A3))
!
      END SUBROUTINE TAL6
!=======================================================================
!=======================================================================
      SUBROUTINE TAL60
!
      USE PARAMETRES_TAL,ONLY : ZIB
      USE FICHIERTAL,ONLY : LU2
      USE TE1,ONLY : IBMAX,LIT,ID,IF4
      USE TE2_TE3,ONLY : N3,N4
      USE TE4,ONLY : IC
!
      use, intrinsic :: iso_fortran_env, only: output_unit
      IMPLICIT NONE ; SAVE

!=======================================================================
!===== verification de la connexite du graphe
!=====       si le graphe est connexe il existe une chaine qui relie un
!=====       noeud a tous les autres
!=======================================================================
      LOGICAL, allocatable :: BOOL(:)
      logical :: BOOL1
      INTEGER :: I,J,II
!
      allocate (bool(zib+1))
!-----selection d'un noeud de depart
      BOOL(LIT)=.TRUE.
      DO I=LIT+1,ID
         BOOL(I)=.FALSE.
      ENDDO
!-----on boucle jusqu'a obtenir un point fixe
   22 CONTINUE
         BOOL1=.FALSE.
!-----boucle sur tous les noeuds I non encore atteints
         DO I=LIT,ID
            if (IC(I)/=0.AND..NOT.BOOL(I)) THEN
!-----boucle sur tous les noeuds J deja atteints
               DO J=LIT,ID
                  if (IC(J)/=0.AND.BOOL(J)) THEN
!-----s'il existe une chaine entre I et J alors on peut atteindre I
                     DO II=1,IBMAX
                        if ((N3(II)==I.AND.N4(II)==J).OR.(N3(II)==J.AND.N4(II)==I)) THEN
                           BOOL(I)=.TRUE.
                           BOOL1=.TRUE.
                        ENDIF
                     ENDDO
                  ENDIF
               ENDDO
            ENDIF
         ENDDO
      if (BOOL1) GOTO 22
!-----s'il existe un noeud qu'on ne peut pas atteindre alors le graphe
!-----n'est pas connexe
      DO I=LIT,ID
         if (IC(I)/=0.AND..NOT.BOOL(I)) THEN
            WRITE(output_unit,'(//A)') ' *** ERREUR---> le graphe n''est pas connexe'
            WRITE(LU2,'(//A)') ' *** ERREUR---> le graphe n''est pas connexe'
            IF4=-1
            EXIT
         ENDIF
      ENDDO
!
      END SUBROUTINE TAL60
!=======================================================================
!=======================================================================
      SUBROUTINE TAL61
!
      USE PARAMETRES_TAL,ONLY : ZIB
      USE TE1,ONLY :IBMAX,LIT,ID,IB,IPA,IF1
      USE TE2_TE3,ONLY : N3,N4,N5,N6,N7,N8,IUB
      IMPLICIT NONE ; SAVE

!=======================================================================
!==========CLASSEMENT GLOBAL DES BIEFS
      INTEGER :: I,J,K,L,L3,L4,L5,L6,M,N1,N2
!
      K=IBMAX
      L3=1
      M=2
   20 CONTINUE
         M=3-M
!---------M=2 RECHERCHE DES NOEUDS DEFLUENTS
!---------M=1 RECHERCHE DES AUTRES NOEUDS
   25    CONTINUE
            L5=L3
            L3=0
            L4=0
! NOTE: LIT (module TE1) = premier noeud défluent = début de la maille
! NOTE: ID (module TE1) = nombre total de nœuds = numéro du dernier nœud aval
            DO N1=LIT,ID
!---------ON ELIMINE N1 CAR N1 EST BRANCHE A L'AVAL D'UN BIEF
               if (N6(N1)==0) THEN
                  J=N5(N1)
!---------N1 A DEJA ETE TRAITE
                  if (J/=0) THEN
                     L4=1
                     L=J
                     if (L>=2) L=2
                     if (L==M) THEN
                        L3=1
                        DO L=1,J
                           I=N7(L,N1)
!---------(IUB)=CLASSEMENT DES BIEFS DE L'AVAL VERS L'AMONT
                           IUB(K)=I
                           N2=N4(I)
                           N6(N2)=N6(N2)-1
                           K=K-1
                        ENDDO
                        N5(N1)=0
                     ENDIF
                  ENDIF
               ENDIF
            ENDDO
         if (L3==1) GOTO 25
      if (L4/=0.AND.L5/=0) GOTO 20
! NOTE: à la sortie de la boucle 20, N5 et N6 sont nuls

!---------ERREUR DANS LA NUMEROTATION DES NOEUDS
      IPA=-1
      if (K/=0) RETURN
!=========DETERMINATION DES BIEFS DE MAILLE
      DO L=LIT,ID
         N6(L)=0
      ENDDO
      LIT=0
      L3=0
      L4=0
      K=IBMAX
      DO L=1,IBMAX
         I=IUB(K)
!---------LIT=PREMIER NOEUD DEFLUENT=DEBUT DE MAILLE
!---------L3=DERNIER NOEUD CONFLUENT DE MAILLE
         if (L4==N3(I) .AND. LIT==0) THEN
            N6(L6)=N6(L6)+1
            LIT=N3(I)
         ENDIF
         L4=N3(I)
         N2=N4(I)
         if (LIT/=0) N6(N2)=N6(N2)+1
         if (N6(N2)>1) L3=N2
         K=K-1
         L6=N2
      ENDDO
!---------MODELE RAMIFIE
      IPA=0
      if (LIT==0) RETURN
!---------MODELE MAILLE
!---------IPA=NOMBRE DE BIEFS DE MAILLE
!---------L3=-1 MAILLE A PLUSIEURS NOEUDS AVAL
      if (IB>1) L3=-1
      J=0
      L4=0
      M=1
      IF1=0
      L6=0
      K=IBMAX
      DO L=1,IBMAX
         I=IUB(K)
         N1=N3(I)
         N2=N4(I)
         if (N1==LIT) L4=1
         if (L3>=0) THEN
            if (N6(L3)==0) M=0
         ENDIF
         if (L4*M==1) THEN
!---------IUB(L)<0 SI IUB(L) EST BIEF DE MAILLE
            IUB(K)=-I
            if (N2==L3) N6(L3)=N6(L3)-1
            IPA=IPA+1
            I=N8(N2)+1
!---------ON RAJOUTE DANS LE TABLEAU N7 LES BIEFS DE MAILLE
!---------ARRIVANT SUR LES NOEUDS DE MAILLE
!---------N8(N2)=NOMBRE DE BIEFS DE MAILLE RELIES AU NOEUD  N2
            N7(I,N2)=IUB(K)
            N8(N2)=I
            if (L6/=N1) THEN
!---------(N5)=NOEUDS AMONT DES BIEFS DE MAILLE
!---------IF1=NOMBRE DE CES NOEUDS
               IF1=IF1+1
               N5(IF1)=N1
               L6=N1
            ENDIF
         ENDIF
         K=K-1
      ENDDO
!
      END SUBROUTINE TAL61

      SUBROUTINE TAL62
!
      USE PARAMETRES_TAL,ONLY : ZIB
      USE FICHIERTAL,ONLY : LU7
      USE TE1,ONLY : IBMAX,IPA,IF1
      USE TE2_TE3,ONLY : N4,N5,N7,N8,N9,IUB
      USE TE4,ONLY : IC,I1,J1
      IMPLICIT NONE ; SAVE

!=======================================================================
      DIMENSION IUR(ZIB),IR0(ZIB),I0(ZIB),J0(ZIB)
      INTEGER :: IUR
      INTEGER :: IR0
      INTEGER :: I,I0,J,J0,L,L1,L2,L3,L4,L5,L7,N1,N91,M0,M1
!======CLASSEMENT DES BIEFS DE MAILLE POUR OBTENIR
!====== (I)=MATRICE IDENTITE
!====== (H)=   "    TRIANGULAIRE SUPERIEURE
!====== (C)=   "    NULLE
!======ET REMPLISSAGE DE (H)
!------IUR(I)=RANG DU BIEF I DANS (IUB)
      DO L=1,IBMAX
         I=IABS(IUB(L))
         IUR(I)=L
      ENDDO
      N91=N9(1)
      L7=IPA+1
      M0=0
      J=1
!======PREMIERE ETAPE
      DO L=1,IF1
         L1=N5(L)
         L3=N8(L1)
         N1=0
         DO L2=1,L3
            I=N7(L2,L1)
            L5=L2
            if (I<0) EXIT
            DO L4=2,N91
               if (N4(I)==N9(L4)) THEN
!------ON RETIENT LE RANG DES BIEFS AVAL DE MAILLE DE LA LIGNE L
!------DANS LA LISTE (IR0)
!------IR0(L7)=RANG DU BIEF CORRESPONDANT A LA COLONNE L7
!------IR(I)=NUMERO DE LA COLONNE OCCUPEE PAR LE BIEF I
                  N1=1
                  L7=L7-1
                  IR0(L7)=IUR(I)
                  IC(I)=L7
                  EXIT
               ENDIF
            ENDDO
         ENDDO
         if (N1/=0) THEN
            if (I<=0) THEN
!------ON RAJOUTE A LA LISTE (IR0) LES RANGS DES BIEFS <0 DE LA LIGNE L
               N1=L7
               DO L2=L5,L3
                  I=-N7(L2,L1)
                  L7=L7-1
                  IR0(L7)=IUR(I)
                  IC(I)=L7
!------M0=NOMBRE D'ELEMENTS NON NUL DE (H)
!------(I0)=INDICES DES LIGNES DE (H)
!------(J0)=INDICES DES COLONNES DE (H)
                  M0=M0+1
                  I0(M0)=L7
                  J0(M0)=N1
               ENDDO
            ENDIF
!------ON PERMUTE LES LIGNES J ET L
            N5(L)=N5(J)
            N5(J)=L1
            J=J+1
         ENDIF
      ENDDO
!======DEUXIEME ETAPE
47    CONTINUE
      if (L7==1) GOTO 64
      DO L=J,IF1
         L1=N5(L)
         L3=N8(L1)
         N1=0
         DO L2=1,L3
            I=N7(L2,L1)
            L5=L2
            if (I<0) EXIT
            if (N1==0) THEN
               DO L4=L7,IPA
                  if (IUR(I)==IR0(L4)) THEN
!------ON A TROUVE DANS LA LIGNE L UN BIEF >0 QUI APPARTIENT A LA LISTE (IR0)
                     N1=L4
                     EXIT
                  ENDIF
               ENDDO
            ENDIF
         ENDDO
         if (N1/=0) EXIT
      ENDDO
      if (I<=0) THEN
!------ON RAJOUTE A LA LISTE (IR0) LES RANGS DES BIEFS <0 DE LA LIGNE L
         DO L2=L5,L3
            I=-N7(L2,L1)
            L7=L7-1
            IR0(L7)=IUR(I)
            IC(I)=L7
            M0=M0+1
            I0(M0)=L7
            J0(M0)=N1
         ENDDO
      ENDIF
!------ON PERMUTE LES LIGNES J ET L
      N5(L)=N5(J)
      N5(J)=L1
      J=J+1
      GOTO 47
64    CONTINUE
!======M1=NOMBRE D'ELEMENTS DE (I1) ET (J1)
!======(I1)=INDICES DES LIGNES DE (B) ET (C1)
!======(J1)=INDICES DES COLONNES DE (B)
      M1=0
      L7=IPA+1
      DO L=1,IF1
         L1=N5(L)
         L3=N8(L1)
         J=N7(1,L1)
         J=IC(J)
         DO L2=2,L3
            I=N7(L2,L1)
            if (I<0) EXIT
            I=IC(I)
            L7=L7-1
            M1=M1+1
            I1(M1)=L7
            J1(M1)=J
            M1=M1+1
            I1(M1)=L7
            J1(M1)=I
         ENDDO
      ENDDO
!======ECRITURE SUR FICHIER LU7
      WRITE(LU7) (IR0(I),I=1,IPA)
      WRITE(LU7) M0,M1,(I1(I),J1(I),I=1,M1)
      if (M0/=0) WRITE(LU7) (I0(I),J0(I),I=1,M0)
!
      END SUBROUTINE TAL62
!=======================================================================
!=======================================================================
      SUBROUTINE TAL63
!
      USE PARAMETRES_TAL,ONLY : ZIB
      USE FICHIERTAL,ONLY : LU7
      USE TE1,ONLY : IBMAX,IF1
      USE TE2_TE3,ONLY : N3,N4,N5,N7,N8,IUB
      USE TE4,ONLY : IC,I1,J1
      IMPLICIT NONE ; SAVE

      INTEGER :: I,J,L,L1,L2,L3,L6,M1
!
!=======================================================================
!=========(I1)=INDICE DES LIGNES DE (A)
!=========(J1)=INDICE DES COLONNES DE (A)
!---------J1>0 ON MET 1 DANS (A)
!---------J1<0 ON MET -1 DANS (A)
!---------I1(1)=IF1=NOMBRE DE LIGNES DE (A)
!---------M1=NOMBRE D'ELEMENTS DE (I1) ET (J1)
      M1=0
      L2=IF1
      DO L=1,IF1
         L1=N5(L)
         L3=N8(L1)
         DO L6=1,L3
            I=IABS(N7(L6,L1))
            J=IC(I)
            M1=M1+1
            I1(M1)=L2
            if (N7(L6,L1)<0) J=-J
            J1(M1)=J
         ENDDO
         L2=L2-1
      ENDDO
      WRITE(LU7) M1,(I1(I),J1(I),I=1,M1)
!=========== (N8)= NOEUDS AMONT TETE DE MAILLE
!-----------STOCKAGE DANS (N8) DES L2 NOEUDS AMONT
      L2=0
      DO 103 I=1,IBMAX
         L3=IUB(I)
         if (L3<=0) THEN
            L3=-L3
            L3=N3(L3)
            if (L2/=0) THEN
               DO L=1,L2
                  if (N8(L)==L3) GOTO 103
               ENDDO
            ENDIF
            L2=L2+1
            N8(L2)=L3
         ENDIF
103   CONTINUE
!-----------ELIMINATION DES NOEUDS AVAL DE (N8)
      DO I=1,IBMAX
         L3=IUB(I)
         if (L3<=0) THEN
            L3=-L3
            L3=N4(L3)
            DO L=1,L2
               if (N8(L)==L3) THEN
                  L2=L2-1
                  DO L3=L,L2
                     N8(L3)=N8(L3+1)
                  ENDDO
                  EXIT
               ENDIF
            ENDDO
         ENDIF
      ENDDO
      WRITE(LU7) L2,(N8(I),I=1,L2)
!
      END SUBROUTINE TAL63
!=======================================================================
!=======================================================================
      SUBROUTINE TAL7
!
      USE PARAMETRES,ONLY: ToutPetit,Un,Long
      USE PARAMETRES_TAL,ONLY : ZIP,ZIB
      USE TR02,ONLY : XC,YC,PC,B1,B2,AC,DC,ZC,AX,SI
      USE TR1,ONLY : PAS,SU,CO
      USE TR2,ONLY : S
      USE FICHIERTAL,ONLY : LU2,LU3,LU4,LU5,LU7,LU8,LU9,LUA,LU
      USE TE1,ONLY : LI,IBMAX,LIT,ID,IPA,IF1,IF2,IF3,IF4,IB0,IF0
      USE TE2_TE3,ONLY : LS,IPAS,IUB,NS0
      USE JBF,ONLY: NPK,IKB,PK,DXX
      use mage_utilitaires, only: is_zero
      IMPLICIT NONE ; SAVE

!=======================================================================
      real(kind=Long),allocatable :: SO(:)
      real(kind=Long),DIMENSION(20) :: AAX
      INTEGER,DIMENSION(2,20) :: IS
      INTEGER :: IBini,IndNPK
      INTEGER :: I,I0,I1,II,IS1,IS2,J,K,L,LSI,LS0,LS1,LS3,N,NS,NSD,M,M1
      real(kind=Long) :: EPSA,EPSU
      real(kind=Long) :: AXI,AXJ,UX,S0,UP,PA,U
      INTEGER :: ICA,ICA0
!
      allocate (SO(1:ZIB))
      SO(1:ZIB)=0._Long
      EPSA=ToutPetit
      EPSU=Un+ToutPetit
      LSI=0
      IPA=0
      IF3=4
      LU=LU5
      if (LI==2) LU=LU8
      DO LIT=1,LI
         LS1=1
         DO I0=1,IBMAX
!---------(IUB) CONTIENT LES BIEFS DANS L'ORDRE DE CALCUL AVAL VERS AMONT
            II=IABS(IUB(I0))
            L=IPAS(II)
            K=2
            NSD=LS(II,LIT)
            IBini=II-1
            if (IBini/=0) THEN
                     K=K+IPAS(IBini)
               NSD=NSD-LS(IBini,LIT)
            ENDIF
!---------NSD=NOMBRE DE SECTIONS DE DONNEE DU BIEF II
            LS0=0
            LS3=0
            if (NSD==0) GOTO 51
            J=L
            IF0=0
            NS=2
            I=L
            READ(LUA,REC=I) AXI,ICA
            I=K-1
            READ(LUA,REC=I) AXJ,ICA
            UX=AXJ-AXI
            UX=ABS(UX)
            UX=UX+UX
            S0=0._long
            ICA0=0
            DO M1=K,L
!========= ABSCISSE DE POSITIONNEMENT DE LA SECTION DE CALCUL
               if (ICA0==0) UP=0._long
               J=J-1
               I=J
               READ(LUA,REC=I) AXJ,ICA
               AX=AXJ-AXI
!---supplement JBF : recherche du pas de calcul
               DO IndNPK=1,NPK
                  if (II==IKB(IndNPK) .AND. ABS(PK(IndNPK)-AXJ)<0.011_long) THEN
                     PA=AX/DXX(IndNPK)
                     GOTO 252
                  ENDIF
               ENDDO
!---finJBF
               PA=AX/PAS(II)
!---supplement JBF
252            CONTINUE
!---finJBF
               PA=ABS(PA)+0.5_long
               N=INT(PA)
               if (N==0) N=1
!---------PA=PAS DE CALCUL DANS LE SOUS-BIEF
               PA=AX/real(N,kind=long)
               if (.not.is_zero(PA)) N=N+1
!---------TEST POUR NE PAS DOUBLER LA SECTION AVAL SI ELLE EST SINGULIERE
               if (N==1.AND.M1==K) GOTO 47
!---------AX=ABSCISSE DE CALCUL
!---------IB0 NUMERO ABSOLU DE LA SECTION DE CALCUL
               AX=AXI
               if (N/=1) N=N-ICA
               DO I1=1,N
                  IB0=LS1+LS3
                  if (I1==N.AND.ICA==0) AX=AXJ
30                CONTINUE
!========= LECTURE DES SECTIONS
                  if (NS/=0) THEN
                     if (NSD/=0 .OR. LIT/=1) THEN
                        if (NSD==0) GOTO 51
                        DO M=1,NS
                           if (M==NS) THEN
!========= TRANSVASEMENT DE LA SECTION DE DONNEE 2 DANS 1
                              IF1=IF2
                              ZC(1)=ZC(2)
                              AC(1)=AC(2)
                              DC(1)=DC(2)
                              B1(1)=B1(2)
                              B2(1)=B2(2)
                              DO I=1,IF1
                                 XC(1,I)=XC(2,I)
                                 YC(1,I)=YC(2,I)
                                 if (LIT==1) PC(1,I)=PC(2,I)
                              ENDDO
                           ENDIF
!========= LECTURE SUR FICHIER PROVISOIRE D'UNE SECTION DE DONNEE
                           I=NSD
                           if (IBini/=0) I=I+LS(IBini,LIT)
                           if (LIT==1) READ (LU3,REC=I) IF2,AC(2),DC(2),B1(2),B2(2),(XC(2,I),YC(2,I),PC(2,I),I=1,IF2)
                           if (LIT==2) READ (LU4,REC=I) IF2,AC(2),DC(2),(XC(2,II),YC(2,II),II=1,IF2)
                           NSD=NSD-1
!---------(ZC)=COTE DU FOND
                           ZC(2)=YC(2,1)
!---------(YC)=TIRANTS D'EAU
                           DO I=1,IF2
                              YC(2,I)=YC(2,I)-ZC(2)
                           ENDDO
                        ENDDO
                        I=NS
                        NS=1
                        if (is_zero(AC(2)-AC(1)) .AND. I/=2) GOTO 30
                        U=AX-AC(2)
                        if (ABS(U)<=EPSA) AX=AC(2)
                     ENDIF
                  ENDIF
                  if (LIT==1) THEN
                     if (M1/=K .AND. I1==1) THEN
                        if (ICA0/=1) THEN
!---------LSI=COMPTEUR DE SECTIONS SINGULIERES
!---------IS1= RANG DU BIEF CONTENANT LA LSI IEME SINGULARITE
!---------IS2=NUMERO DE LA SECTION DE CALCUL CORRESPONDANTE
                           LSI=LSI+1
                           IS1=I0
                           IS2=LS0+1
                           I=IPAS(IBMAX)+LSI
                           WRITE(LUA,REC=I) AX,IS1,IS2
                        ENDIF
                     ENDIF
                  ENDIF
                  NS=0
                  U=AX*CO(II,1)
                  if (U>=AC(1)*CO(II,1)) THEN
                     NS=1
                     if (LIT==2 .AND. is_zero(DC(1))) IF0=0
                     if (LIT==2 .AND. is_zero(DC(1))) GOTO 30
                     if (U>AC(2)*CO(II,1)) GOTO 30
                     NS=0
                     if (is_zero(AX-AC(2))) NS=1
!=========SINUOSITE
                     SI=SU(II)
                     if (LIT/=1) THEN
                        SI=DC(1)/(AC(2)-AC(1))
                        SI=ABS(SI)
                        if (SI>1._long .AND.SI<=EPSU) SI=1._long
                     ENDIF
!=========AMALGAME DE 2 SECTIONS CONSECUTIVES
                     CALL TAL71
!                     print*,' TAL71 ok'
!=========INTERPOLLATION DES SECTIONS DE CALCUL DE L'AVAL VERS L'AMONT
                     IF4=1
                     if (M1==L.AND.I1==N) IF4=0
                     CALL TAL72
!---------SO(I0)=SECTION MOYENNE DU BIEF DE RANG I0
                     if (LIT==1) THEN
!                     print*,iabs(iub(i0)),so(i0),s(id-1),s0,up
                        SO(I0)=SO(I0)+(S(ID-1)+S0)*UP
                        if (I1==1) UP=ABS(PA)/UX
                        S0=S(ID-1)
                     ENDIF
!=========STOCKAGE DES POINTS EN MEMOIRE CENTRALE
!=========ET VIDANGE DU BUFFER SUR FICHIER
                     IF4=1
                     CALL TAL73
                     LS0=LS0+1
                  ENDIF
                  LS3=LS3+1
                  AX=AX+PA
               ENDDO
47             CONTINUE
               AXI=AXJ
               ICA0=ICA
            ENDDO
51          CONTINUE
!---------RECUPERATION DANS (NS0) DE LS0=NOMBRE DE SECTIONS DE CALCUL
!---------DU BIEF II DE RANG I0
            NS0(LIT,I0)=LS0
            LS1=LS1+NS0(1,I0)
         ENDDO
!---------VIDANGE DU DERNIER BUFFER
         IF4=0
         if (IPA/=0) CALL TAL73
         IF3=3
         LU=LU9
      ENDDO
!=========SOUSTRACTION LIT MAJEUR-LIT MINEUR
      IF0=0
      if (LI==2) CALL TAL74
      if (IF0/=0) REWIND LU9
      WRITE(LU2,801)
      LI=LI-IF0
      DO LIT=1,LI
         WRITE(LU7) (NS0(LIT,I0),I0=1,IBMAX),LSI
         if (LIT==2) WRITE(LU2,802)
         DO I0=1,IBMAX
            II=IABS(IUB(I0))
            if (NS0(LIT,I0)>0) WRITE(LU2,803) II,NS0(LIT,I0)
         ENDDO
      ENDDO
      if (IF0/=0) WRITE(LU2,804)
      if (LSI==0) WRITE(LU2,805)
      if (LSI/=0) THEN
!---------ENREGISTREMENT SUR LU7 DE IS1 ET IS2 PAR BLOC DE 20 (SENS AVAL AMONT)
         WRITE(LU2,806)
         K=IPAS(IBMAX)
         N=0
         DO L=1,LSI
            I=K+L
            READ(LUA,REC=I) AX,IS1,IS2
            N=N+1
            IS(1,N)=IS1
            IS(2,N)=IS2
            AAX(N)=AX
            if (N==20 .OR. L==LSI) THEN
               WRITE (LU7) ((IS(I,J),I=1,2),J=1,N)
!---------IMPRESSION SUR LU2 DES NUMEROS (SENS AMONT VERS AVAL)
!---------DES SECTIONS SINGULIERES
               DO J=1,N
                  I0=IS(1,J)
                  II=IABS(IUB(I0))
                  I=NS0(1,I0)+1-IS(2,J)
                  WRITE(LU2,803) II,I,AAX(J)
               ENDDO
               N=0
            ENDIF
         ENDDO
      ENDIF
      WRITE(LU7) (SO(I),I=1,IBMAX)
      CALL TAL75
      if (IF0==0) WRITE(LU2,807)

801   FORMAT(//' LIT MINEUR:     BIEF=     NOMBRE DE SECTIONS DE CALCUL=')
802   FORMAT(//'( LIT MAJEUR:     BIEF=     NOMBRE DE SECTIONS DE CALCUL=')
803   FORMAT(19X,I3,30X,I4,16X,F8.1)
804   FORMAT(//' SUPPRESSION DU LIT MAJEUR DU MODELE')
805   FORMAT(//' CE MODELE N''A PAS DE SINGULARITE')
806   FORMAT(//' SINGULARITES:   BIEF=   NUMERO DE LA SECTION DE CALCUL=   ABSCISSE DE POSITION=')
807   FORMAT(//' ---> FIN NORMALE DU PROGRAMME TALWEG')

      END SUBROUTINE TAL7

      SUBROUTINE TAL71
!
      USE PARAMETRES,ONLY : Long,ToutPetit
      USE PARAMETRES_TAL,ONLY : ZIP
      USE TR02,ONLY : XC,YC,PC,YE,XE,PE
      USE TE1,ONLY : LIT,ID,IB,IF1,IF2,IF21
!
      IMPLICIT NONE ; SAVE
      real(kind=Long) :: YCM,YCM2,C
      INTEGER :: I,I0,J,J0,K,L,L1,L2,M,N,NM
!
      YCM=1._long/ToutPetit
      YCM2=2*YCM
!=======================================================================
!=========AMALGAME DES TIRANTS D'EAU DE 2 SECTIONS CONSECUTIVES
!=========ET INTERPOLLATION DES POINTS CORRESPONDANTS
!---------POINT SUPPLEMENTAIRE POUR L'INTERPOLLATION
      I0=IF1+1
      J0=IF2+1
      YC(1,I0)=YCM
      YC(2,J0)=YCM
      XC(1,I0)=XC(1,IF1)
      XC(2,J0)=XC(2,IF2)
      if (LIT/=2) THEN
         PC(1,I0)=PC(1,IF1)+YCM2
         PC(2,J0)=PC(2,IF2)+YCM2
      ENDIF
!---------SECTION1: I=INDICE DU POINT COURANT,ID=NOMBRE DE COTES CUMULEES
!---------SECTION2: J=INDICE DU POINT COURANT,IB=NOMBRE DE COTES CUMULEES
      ID=0
      IB=0
      I=1
      J=1
      K=0
35    CONTINUE
         K=K+1
         M=I
         N=J
         NM=IF2
         L1=1
         L2=2
         if (YC(1,I)>YC(2,J)) then
            M=J
            N=I
            NM=IF1
            L1=2
            L2=1
         ENDIF
!---------(YE) ENSEMBLE DES TIRANTS D'EAU DES SECTIONS 1 ET 2
!---------(XE),(PE) LARGEURS ET PERIMETRES CORRESPONDANTS
         YE(K)=YC(L1,M)
         XE(L1,K)=XC(L1,M)
         if (LIT==1) PE(L1,K)=PC(L1,M)
         if (YE(K)<=YC(L2,NM)) THEN
            L=N-1
            C=1._long
            if (L/=0) C=(YE(K)-YC(L2,L))/(YC(L2,N)-YC(L2,L))
            if (L==0) L=1
            XE(L2,K)=XC(L2,L)+(XC(L2,N)-XC(L2,L))*C
            if (LIT==1) PE(L2,K)=PC(L2,L)+(PC(L2,N)-PC(L2,L))*C
         ENDIF
         L=I
         if (YC(1,L)<=YC(2,J)) I=I+1
         if (YC(1,L)>=YC(2,J)) J=J+1
         if (I==I0.AND.ID==0) ID=K
         if (J==J0.AND.IB==0) IB=K
      if (ID==0.OR.IB==0) GOTO 35
      IF21=0
!
      END SUBROUTINE TAL71

!=======================================================================
!=======================================================================
      SUBROUTINE TAL72
!
      USE PARAMETRES,ONLY: Long
      USE PARAMETRES_TAL,ONLY : ZIP
      USE TR02,ONLY : B1,B2,AC,DC,ZC,AX,SI,YE,XE,PE
      USE TR2,ONLY : ZDM,X,Z,S,P
      USE TE1,ONLY : LIT,ID,IB,IF4
      USE TE5,ONLY : N0
      use mage_utilitaires, only: is_zero
      IMPLICIT NONE ; SAVE

      real(kind=Long) :: EPS,EPSI,EPS1,EPS2,EPS3
      real(kind=Long) :: A,A1,C,DS,DS0,DS1,DZ1,DZ2,S1,X1,ZD,ZF
      INTEGER :: I,I1,J,K,L,L1,M
!=======================================================================
      EPSI=1.E-3_long
      EPS1=0.01_long
      EPS2=0.01_long
      EPS3=5.E-4_long
      EPS=1.E-2_long
      DZ1=1000._long
      DZ2=2000._long
!=========INTERPOLLATION
!---------INTERPOLLATION COTE DU FOND ZF,MOYEN-MINEUR ZD,MOYEN-MAJEUR ZDM
      A=AC(2)-AC(1)
      C=0._long
      if (is_zero(A) .AND. IF4==0) C=1._long
      if (.not.is_zero(A)) C=(AX-AC(1))/A
      ZF=(ZC(2)-ZC(1))*C+ZC(1)
      if (LIT/=2) THEN
         ZD=(DC(2)-DC(1))*C+DC(1)
         A1=(B1(2)-B1(1))*C+B1(1)
         ZDM=(B2(2)-B2(1))*C+B2(1)
         if (ZDM>A1) ZDM=A1
      ENDIF
      S(1)=0._long
      I1=0
25    CONTINUE
         I1=I1+1
         J=I1
         I=I1
         if (J>IB) J=IB
         if (I>ID) I=ID
         Z(I1)=(YE(J)-YE(I))*C+YE(I)+ZF
         X(I1)=(XE(2,J)-XE(1,I))*C+XE(1,I)
         if (LIT==1) P(I1)=(PE(2,J)-PE(1,I))*C+PE(1,I)
         if (I1>1) S(I1)=S(M)+0.5_long*(Z(I1)-Z(M))*(X(I1)+X(M))
         M=I1
         N0(I1)=0
      if (I<ID.OR.J<IB) GOTO 25
      ID=I1
!---------ID=NOMBRE DE POINT DE LA SECTION DE CALCUL
!=========RECHERCHE DES PREMIERS POINTS A ELIMINER
      DS1=0._long
      I1=1
      DO J =2,ID
         DS1=DS1+X(J)*EPSI
         if (is_zero(DS1)) EXIT
         A1=S(J)/DS1
         if (A1>1._long) EXIT
         N0(I1)=1
         I1=J
      ENDDO
!=========RECHERCHE DES POINTS SUIVANTS A ELIMINER
      DS1=0._long
30    CONTINUE
      I=I1
      J=I1+1
      K=I1+2
      I1=J
      if (I1/=ID) THEN
         DS=DS1
         DO L1=K,ID
            DO L=J,L1
               DS0=S(L)-S(I)+DS
               A1=Z(L)-Z(I)
               A=A1/(Z(L1)-Z(I))
               DS0=DS0-A1*(A*(X(L1)-X(I))*0.5_long+X(I))
               A1=DS0/S(L)
               if (ABS(A1)>EPS1) GOTO 30
               if (LIT/=2) THEN
                  if (L/=L1) THEN
                     A1=A*(P(L1)-P(I))+P(I)
                     A1=1._long-A1/P(L)
                     if (ABS(A1)>EPS2) GOTO 30
                  ENDIF
               ENDIF
            ENDDO
!---------SI ON ELIMINE LE POINT I1  N0(I1)=1
            N0(I1)=1
            DS1=DS0
            I1=L1
         ENDDO
      ENDIF
!---------ELIMINATION DES POINTS
      L=0
      IB=L
      DO I=1,ID
         if (N0(I)/=1) THEN
            L=L+1
            Z(L)=Z(I)
            X(L)=X(I)
            if (LIT/=2) THEN
               P(L)=P(I)
               if (Z(L)<=(ZD+EPS3)) IB=L
            ENDIF
         ENDIF
      ENDDO
      ID=L
!---------CALCUL DES SURFACES
      L=1
      DO I=2,ID
         S(I)=S(L)+0.5_long*(X(I)+X(L))*(Z(I)-Z(L))
         L=I
      ENDDO
      if (LIT/=2) THEN
         C=Z(IB)-ZD
         if (ABS(C)>EPS3) THEN
!=========ON AJOUTE LE POINT DE DEBORDEMENT S'IL N'EST PAS DANS LE TABLEAU
!---------IB=INDICE DE DEBORDEMENT MINEUR MOYEN
            IB=IB+1
            I1=IB+1
            ID=ID+1
            I=ID+I1
            DO L=I1,ID
               K=I-L
               J=K-1
               Z(K)=Z(J)
               X(K)=X(J)
               P(K)=P(J)
               S(K)=S(J)
            ENDDO
            J=J-1
            C=(ZD-Z(J))/(Z(IB)-Z(J))
            X(IB)=(X(IB)-X(J))*C+X(J)
            P(IB)=(P(IB)-P(J))*C+P(J)
            S(IB)=S(J)+0.5_long*(X(IB)+X(J))*(ZD-Z(J))
         ENDIF
         Z(IB)=ZD
      ENDIF
!=========REDUCTION ET COMPLEMENT DE LA SECTION DE CALCUL
      L=IB+1
      if (L<=ID) THEN
         S1=0._long
         X1=0._long
         if (LIT==1) X1=X(IB)
         DO I=L,ID
            X(I)=SI*(X(I)-X1)+X1
            if (LIT/=2) THEN
               S1=S(IB)+X1*(Z(I)-Z(IB))
               P(I)=SI*(P(I)-P(IB))+P(IB)
            ENDIF
            S(I)=SI*(S(I)-S1)+S1
         ENDDO
      ENDIF
      J=ID
      ID=ID+1
      Z(ID)=Z(J)+DZ1
      P(ID)=P(J)
      if (LIT==1 .AND. .not.is_zero(X(J))) P(ID)=P(J)+DZ2
      S(ID)=S(J)+DZ1*X(J)
      if (is_zero(X(J))) X(J)=EPS
      X(ID)=X(J)
!
      END SUBROUTINE TAL72
!=======================================================================
!=======================================================================
      SUBROUTINE TAL73
!
      USE PARAMETRES,ONLY : Long, sp
      USE PARAMETRES_TAL,ONLY : ZIP,ZIDMAX
      USE TR02,ONLY : AX,SI
      USE TR2,ONLY : ZDM,X,Z,S,P
      USE TR3,ONLY : XX
      USE FICHIERTAL,ONLY : LU
      USE TE1,ONLY : LIT,ID,IB,IPA,IF3,IF4,IB0,IF0
      IMPLICIT NONE ; SAVE

      INTEGER :: J,K,L,M,N
      real(kind=Long) :: Z1
      real(kind=sp) :: xx0(zidmax)
!=======================================================================
!---------ZIDMAX=NOMBRE DE POINTS DE SECTIONS DE CALCUL PRESENTS
!---------EN MEMOIRE CENTRALE EN MEME TEMPS
      N=(IPA+ID+1)*IF4
      L=1
      if (N>ZIDMAX) L=2
      DO K=1,L
         if (K*IF4/=L) THEN
!=========VIDAGE DU BUFFER SUR FICHIER
!---------IF3=4,LU=LU5 SI LIT MINEUR MODELE A 1 LIT
!---------IF3=4,LU=LU8 SI LIT MINEUR MODELE A 2 LITS
!---------IF3=3,LU=LU9 SI LIT MAJEUR
            DO M=1,IF3
              !WRITE (LU) IPA,(XX(M,J),J=1,IPA)  !jbf-
               xx0=real(xx(m,:),sp)               !jbf+
               WRITE (LU) IPA,(XX0(J),J=1,IPA)   !jbf+
            ENDDO
            IPA=0
         ELSE
            IPA=IPA+1
            N=ID+IPA
            IB=IB+IPA
            IF0=IF0+1
!=========STOCKAGE EN MEMOIRE CENTRALE DES SECTIONS (SENS AVAL AMONT)
!---------XX(1,I)=INDICE DU DERNIER POINT DE LA SECTION EN COURS DANS (XX)
!---------XX(2,I)=COTE DE DEBORDEMENT MOYEN MAJEUR SI LIT = 1
!---------XX(2,I)=NUMERO ABSOLU DE LA SECTION DE CALCUL SI LIT=2
!---------        <0 SI SECTION FIN DE MAJEUR
!---------XX(3,I)=ABSCISSE DE POSITIONNEMENT
!---------XX(4,I)=INDICE DU POINT DE DEBORDEMENT MINEUR MOYEN
!---------XX(1,I+1......XX(1,I))=LARGEURS
!---------         XX(1,XX(1,I))=SINUOSITE DU LIT MAJEUR SI LIT=2
!---------XX(2,I+1)=COTE DU FOND
!---------XX(2,I+2......XX(1,I))=TIRANTS D'EAU
!---------XX(3,I+1......XX(1,I))=SURFACES
!---------XX(4,I+1......XX(1,I))=PERIMETRES
            XX(1,IPA)=real(N,kind=long)+0.1_long
            XX(2,IPA)=real(IB0,kind=long)+0.1_long
            if (IF0==1) XX(2,IPA)=-XX(2,IPA)
            XX(3,IPA)=AX
            if (LIT/=2) THEN
               XX(2,IPA)=ZDM
               XX(4,IPA)=real(IB,kind=long)+0.1_long
            ENDIF
            Z1=0._long
            DO M=1,ID
               IPA=IPA+1
               XX(1,IPA)=X(M)
               XX(2,IPA)=Z(M)-Z1
               XX(3,IPA)=S(M)
               if (LIT==1) XX(4,IPA)=P(M)
               if (M==1) Z1=Z(M)
            ENDDO
            if (LIT==2) XX(1,IPA)=SI
            if (K/=L) THEN
!---------FIN DU MODELE
!
!=========VIDAGE DU BUFFER SUR FICHIER
!---------IF3=4,LU=LU5 SI LIT MINEUR MODELE A 1 LIT
!---------IF3=4,LU=LU8 SI LIT MINEUR MODELE A 2 LITS
!---------IF3=3,LU=LU9 SI LIT MAJEUR
               DO M=1,IF3
                  !WRITE (LU) IPA,(XX(M,J),J=1,IPA)  !jbf-
                  xx0=real(xx(m,:),sp)                !jbf+
                  WRITE (LU) IPA,(XX0(J),J=1,IPA)    !jbf+
               ENDDO
               IPA=0
            ENDIF
         ENDIF
      ENDDO
!
      END SUBROUTINE TAL73
!=======================================================================
!=======================================================================
      SUBROUTINE TAL74
!
      USE PARAMETRES,ONLY : Long
      USE PARAMETRES_TAL,ONLY : ZIB,ZIDMAX
      USE TR3,ONLY : XX
      USE FICHIERTAL,ONLY : LU2,LU5,LU6,LU8,LU9
      USE TE1,ONLY : IBMAX,IF0
      USE TE2_TE3,ONLY : IUB,NS0

      IMPLICIT NONE ; SAVE
      INTEGER :: I,I0,I1,I2,I3,I4,I5,J,K,L,L1,L2,L3,LS1,LS2,M,N,N1,N2,N3
      real(kind=Long) :: SI
!
!=======================================================================
!=========SOUSTRACTION DU LIT MINEUR
!=========REDEFINITION DE LA COTE DE DEBORDEMENT

      I3=0
      I4=0
      L1=0
      L2=0
      N=0
      M=0
      LS1=0
      L3=1
      REWIND LU8
      REWIND LU9
      !DO 60 I0=1,IBMAX                      !loic
      DO I0=1,IBMAX                          !loic
         N1=NS0(1,I0)
         N2=NS0(2,I0)
         N3=N1+1
         if (I0==IBMAX) N1=N3
         !DO 60 K=1,N1                       !loic
         DO K=1,N1                           !loic
            I1=I3+1
            if (I1>L1) THEN
!=========LIT MINEUR
               DO L=1,L3
                  if (N/=0) THEN
!---------RECUPERATION SUR LU5 DE 2 ENREGISTREMENTS DU GROUPE N
                     DO I=1,2
                        WRITE(LU5) L1,(XX(I,J),J=1,L1)
                     ENDDO
                     if (K==N3.AND.L==L3) GOTO 38
                  ENDIF
!---------MISE EN MEMOIRE CENTRALE
!---------SI L/=L3  SURFACES ET PERIMETRES DU GROUPE N
!---------SI L=L3  LARGEURS ET COTES DU GROUPE N+1
                  DO I=1,2
                     READ(LU8) L1,(XX(I,J),J=1,L1)
                  ENDDO
               ENDDO
               I1=1
               L3=2
               N=N+1
            ENDIF
            LS1=LS1+1
            I3=INT(XX(1,I1))
            if (N2<=0) GOTO 60
            I2=I4+1
            !if (I2>L2) THEN               !loic
            if (I2<=L2) GOTO 52             !loic
!=========LIT MAJEUR
               !if (M/=0) THEN              !loic
               if (M==0) GOTO 48            !loic
38                CONTINUE
!---------RECUPERATION SUR LU6 DE 3 ENREGISTREMENTS DU GROUPE M
                  DO I=3,5
                     WRITE(LU6) L2,(XX(I,J),J=1,L2)
                  ENDDO
                  if (K==N3) GOTO 60
               !ENDIF                        !loic
48             CONTINUE                      !loic
!---------MISE EN MEMOIRE CENTRALE DES LARGEURS,COTES,SURFACES
!---------DU GROUPE M+1
               DO I=3,5
                  READ(LU9) L2,(XX(I,J),J=1,L2)
               ENDDO
               I2=1
               M=M+1
            !ENDIF                           !loic
52          CONTINUE                         !loic
            LS2=INT(XX(4,I2))
            !if (LS1/=IABS(LS2)) GOTO 60    !loic
            if (LS1==IABS(LS2)) THEN        !loic
               I4=INT(XX(3,I2))
               I5=I2+1
!---------REDUCTION DU LIT MAJEUR LARGEURS ET SURFACES
               SI=XX(3,I4)
               if (SI>1._long) THEN
                  IF0=1
                  I=IABS(IUB(I0))
                  WRITE(LU2,800) I,XX(5,I2)
                  GOTO 60
               ENDIF
               SI=SI*XX(1,I3)
               XX(3,I4)=XX(3,I4-1)
               J=0
               DO I=I5,I4
                  XX(3,I)=XX(3,I)-SI
                  if (XX(3,I)<0._long .AND. XX(4,I)<XX(2,I1)) XX(3,I)=0._long
                  if (XX(3,I)<0._long) J=1
                  XX(5,I)=XX(5,I)-SI*(XX(4,I)-XX(4,I5))
                  if (XX(5,I)<0._long) XX(5,I)=0._long
               ENDDO
               N2=N2-1
               if (J/=0) THEN
                   IF0=1
                  I=IABS(IUB(I0))
                  WRITE(LU2,801) I,XX(5,I2)
               ENDIF
!---------REDEFINITION DE LA COTE DE DEBORDEMENT
               if (XX(2,I1)<XX(4,I5)) XX(2,I1)=XX(4,I5)
            ENDIF                            !loic
          ENDDO                              !loic
      ENDDO                                  !loic
60    CONTINUE
!
800   FORMAT(/' *** ERREUR DU LIT MAJEUR---> SINUOSITE SUPERIEURE A 1', &
             /5X,'BIEF ',I3,3X,'ABSCISSE DE POSITION ',F8.1 &
             /5X,'LA DISTANCE ENTRE LES 2 SECTIONS DE DONNEE DU LIT MAJEUR' &
             /5X,'EST INCOMPATIBLE AVEC LE LIT MINEUR'/)
801   FORMAT(/' *** ERREUR DU LIT MAJEUR---> LARGEURS NEGATIVES' &
             /5X,'BIEF ',I3,3X,'ABSCISSE DE POSITION ',F8.1/)
!
      END SUBROUTINE TAL74
!=======================================================================
!=======================================================================
      SUBROUTINE TAL75
!
      USE PARAMETRES,ONLY : Long, sp
      USE PARAMETRES_TAL,ONLY : ZIB,ZIDMAX
      USE TR3,ONLY : XX
      USE FICHIERTAL,ONLY : LU2,LU5
      USE TE1,ONLY : IBMAX
      USE TE2_TE3,ONLY : IUB,NS0
      IMPLICIT NONE ; SAVE

      INTEGER :: I,I0,J,K,LSC,M,N,N1,NS
      real(kind=Long),DIMENSION(5) :: AX
      real(kind=sp)::xx0(zidmax)
      INTEGER,DIMENSION(5) :: NX
!----------(NX)=NUMEROS DES SECTIONS DE CALCUL PAR BIEF SENS AMONT AVAL
!----------(AX)=ABSCISSES DE POSITION CORRESPONDANTES
      REWIND LU5
      I=1
      LSC=0
      DO I0=1,IBMAX
         K=IABS(IUB(I0))
         WRITE(LU2,801) K
         NS=NS0(1,I0)
         N1=NS+1
         K=0
         DO N=1,NS
            if (I>=LSC) THEN
               I=1
               DO M=1,4
                  READ(LU5) LSC,(XX0(J),J=1,LSC)
                  xx(m,:)=real(xx0,kind=long)
               ENDDO
            ENDIF
            if (K==5) K=0
            K=K+1
            NX(K)=N1-N
            AX(K)=XX(3,I)
            I=int(XX(1,I)+1)
            if (K==5.OR.N==NS) WRITE(LU2,802) (NX(J),AX(J),J=1,K)
         ENDDO
      ENDDO

801   FORMAT(//' BIEF',I3,5X,'CORRESPONDANCE   NUMERO DE SECTION : ABSCISSE DE POSITION')
802   FORMAT(5(1X,I4,':',F8.1))

      END SUBROUTINE TAL75

subroutine IniVarMod_TAL
!==============================================================================
!             Routine d'initialisation générale des modules TALWEG
!==============================================================================
   ! NOTE: pas de clause ONLY dans ces USE puisque ce sont des initialisations
   use te1 ; use te2_te3 ; use te4 ; use te5
   use tr01 ; use tr02 ; use tr1 ; use tr2 ; use tr3
   use JBF
   use CAR
   implicit none

   call inite1 ; call inite2_te3 ; call inite4 ; call inite5
   call initr01 ; call initr02 ; call initr1 ; call initr2
   call initr3
   call iniJBF
   call iniCAR
end subroutine IniVarMod_TAL


subroutine Lire_TIT
!==============================================================================
!                      Lecture du fichier .TIT
!==============================================================================
      use parametres, only: long, lTra
      use fichiertal, only: lu7
      use geometrie_noeud, only: tc

      use TopoGeometrie, only: la_topo, is_bief_aval_maille
      use, intrinsic :: iso_fortran_env, only: error_unit
      implicit none
! -- Variables --
      integer :: i,ib,kb
      integer :: l,lb1,lb3,lu3
      integer :: n,naa,nai,neu,nzi,nzz
      integer n1,n2,n3,n4
      character :: cfic
      integer :: ibmax, nomax, nclm, nclv
      integer, allocatable :: iub(:), ibu(:), na(:), nz(:), ma(:), mz(:), nc(:)
      integer, pointer :: lbamv(:), lbz(:,:), iba, ibz
      integer :: jbmax

! FIXME: ici on peut faire l'allocation des tableaux à la_topo%net%nb au lieu de ibsup
! NOTE: on fait le pari que le ibmax du fichier TIT est bien le même que celui qu'on a obtenu en lisant le fichier NET
! NOTE: si ça plante c'est qu'il y a un bug
! NOTE: cela permet de ne pas allouer des tableaux si cette boîte noire n'est pas utilisée (réseau ramifié)

      lbamv => la_topo%net%lbamv
      lbz => la_topo%net%lbz
      iba => la_topo%net%iba
      ibz => la_topo%net%ibz

      jbmax = la_topo%net%nb

      allocate (ma(jbmax),mz(jbmax),nc(jbmax+1))
      allocate (iub(jbmax),ibu(jbmax),na(jbmax),nz(jbmax))

      lu3 = lTra
      open(newunit=lu7,file='_dummy.TIT',form='unformatted',status='old')
!---lecture du fichier .tit
      read(lu7,err=999,end=999) n1,n2,n3,n4
!      if (n1/=la_topo%net%nb) then
      if (n1 < la_topo%net%nb) then
         write(error_unit,'(a,2(i3,a))') ' >>>> Nombre de biefs maximum ',la_topo%net%nb, &
                                ' incompatible avec TALWEG (',n1,') <<<<'
         write(error_unit,*) n1,n2,n3,n4
         stop 129
      endif
      read(lu7) cfic

!   lecture des noeuds amont et aval des biefs (selon numero dans .TAL)
      read(lu7) nomax,(tc(n),n=1,nomax)
!   comptage des biefs
      ibmax = 0
      do ib = 1, la_topo%net%nb+1
         read(lu7) i, nai, nzi, cfic
!        stockage temporaire dans MA et MZ avant reclassement (rang de calcul)
         if (i/=0) then
            ma(i) = nai  ;  mz(i) = nzi  ;  ibmax = ibmax+1
         else
            exit
         end if
      enddo
!   lecture du type des noeuds
      read(lu7) neu,(nc(n),n=1,nomax)

!   comptages des c.l. amont et aval
      nclm=0  ;  nclv=0
      do n = 1, nomax
         if (nc(n)==1) then  !--->noeud amont
            nclm = nclm+1
         else if (nc(n)==-1) then  !--->noeud aval
            nclv = nclv+1
         else
            nc(n) = 0
         end if
      end do

!   inversion du classement des biefs (sens amont-->aval pour le transitoire)
!   création de la table ibu inverse de iub
      read(lu7) (iub(ib),ib=ibmax,1,-1),l
      do ib = 1, ibmax
         do kb = 1, ibmax
            if (abs(iub(kb))/=ib) then
               continue
            else
               ibu(ib) = kb
            endif
         enddo
      enddo
      !on prend le classement des biefs produit par TALWEG
      do ib = 1, ibmax
         la_topo%net%numero(ib) = iabs(iub(ib))
         la_topo%net%rang(ib) = iabs(ibu(ib))
      enddo

!   éléments pour remplir les matrices : Poirson février 89
      call Ini_MatriceMaille(lb1,lb3,lu7,iub,ibmax)
      if (lb1==0) lb1 = -1
      iba = lb1
      ibz = lb3
      if (iba<0) then
!     il n'y a pas de bief à l'amont de la maille : arrêt du programme
         call err035
      endif
      !recherche du premier bief aval de maille (dans la maille)
      do ib = iba+1, ibz-1
         if (is_bief_aval_maille(ib)) then
            la_topo%net%iby = ib
            exit
         endif
      enddo

!   quelque écritures sur TRA
      if (ibz>ibmax) then
         if(iba==ibmax) then
            write(lu3,'(a)') ' Réseau ramifié'
         else
            write(lu3,'(a)') ' Réseau maillé ouvert (plusieurs CL aval)'
            write(lu3,'(1x,a,i3,a,i3,a)') 'Numéro du dernier bief amont de maille : ',iub(iba),' (',iba,')'
            write(lu3,'(a,i4)') ' Nombre de biefs dans la maille :',ibmax-iba
         endif
      else
         write(lu3,'(a)') ' Réseau maillé fermé'
         write(lu3,'(1x,a,i3,a,i3,a)') 'Numéro du dernier bief amont de maille : ',iub(iba),' (',iba,')'
         write(lu3,'(1x,a,i3,a,i3,a)') 'Numéro du premier bief aval de maille : ',iub(ibz),' (',ibz,')'
         write(lu3,'(a,i4)') ' Nombre de biefs dans la maille :',ibz-1-iba
      endif

!   Vérification de la cohérence de la maille et des c.l. aval
      if (iba/=ibmax .and. ibz>ibmax .and. nclv<2) then
         write(error_unit,1500)
         stop 133
      endif

!   remplissage de na et nz indexés par le rang de calcul
      do kb = 1, ibmax
         ib = ibu(kb)
         na(ib) = ma(kb)
         nz(ib) = mz(kb)
      enddo

!   repérage des biefs amont et des biefs aval du modèle
!   remplissage de lbamv et lbz
      do ib = 1, ibmax
         naa = na(ib)  !noeud amont du bief de rang ib dans l'ordre de calcul
         nzz = nz(ib)  !noeud aval du bief de rang ib dans l'ordre de calcul
         if (nc(naa)>0) lbamv(naa) = ib  !--->noeud amont du modèle
         if (nc(nzz)<0) lbamv(nzz) = ib  !--->noeud aval du modèle
      enddo
      do n = 1, nomax
         do ib = 1, ibmax
            if (n==na(ib)) then
               lbz(n,1) = ib  ;  exit
            endif
         enddo
         do ib = ibmax,1,-1
            if (n==na(ib)) then
               lbz(n,2) = ib  ;  exit
            endif
         enddo
      enddo

      close (lu7,status='delete')
      return
 999  write(lu3,'(a)') '>>>> Erreur de lecture de .TIT <<<<'
      write(error_unit,'(a)') '>>>> Erreur de lecture de .TIT <<<<'
      stop 134

!--->fin de lecture du fichier .tit

 1500 format(//,' >>>> ERREUR à la CL aval : quand la maille est fermée'      &
             ,/,' il faut obligatoirement un bief à l''aval de la maille'  &
             ,/,' La C.L. aval ne peut être liée à un noeud qui est '      &
             ,/,' un confluent : si nécessaire ajouter un bief fictif')
end subroutine Lire_TIT


subroutine Ini_MatriceMaille(lb1,lb3,lu7,iub,ibmax)
!==============================================================================
!     éléments pour remplir les blocs :   M   A   B
!                                                 H
!==============================================================================
      use parametres, only: long
      use maille, only: nr,na,nb,nh,ir,ia,ja,ib,jb,ih,jh,bool,ir1,ir2,ir3
      implicit none
! -- Prototype --
      integer,intent(out) :: lb1,lb3
      integer,intent(in) :: lu7,iub(*),ibmax
! -- Variables --
      integer :: i, j

      ir1 = 0  ;  ir2 = 0  ;  ir3 = 0
      do i = 1, ibmax
!        iub(i) numéro du bief de rang i
         if (iub(i)<0) then
            ir2 = i  !ir2 rang du dernier bief de maille (sens amont-aval) ou sinon 0
         else if (ir2==0) then
            ir1 = i  !ir1 rang du dernier bief amont de maille (sens amont-aval) ou sinon 0
         else
            ir3 = i  !ir3 rang du dernier bief aval de maille (sens amont-aval) ou sinon 0
         endif
      enddo

      if (ir2/=0) then  !modèle maillé
         nr = ir2-ir1  !nr nombre de biefs de maille = nombre de colonnes par matrice
         read(lu7) (ir(i),i=1,nr)  !ir(i) rang, dans TIT (Talweg-Fluvia),du bief situé à la colonne i des matrices
         j = ibmax+1
         do i = 1, nr
           ir(i) = j-ir(i)  !ir(i) rang, dans MAGE,du bief situé à la colonne i des matrices
         enddo

!        éléments pour remplir la matrice (b)
         read(lu7) nh,nb,(ib(i),jb(i),i=1,nb)

!        éléments pour remplir la matrice (h)
         if (nh/=0) read(lu7) (ih(i),jh(i),i=1,nh)

!        éléments pour remplir les matrices (a) si ja(i)<0 ou (m) si ja(i)>0
         read(lu7) na,(ia(i),ja(i),i=1,na)

         read(lu7)
         do j = 1, nr
            i = ir(j)
            bool(i) = .true.
         enddo
         do j = 1, nh
!        la ligne i de (h) a un élément non nul
!        donc le bief situé à la colonne i n'est pas bief aval de maille
            i = ih(j)
!        i rang du bief
            i = ir(i)
!        le bief de rang i n'est pas bief aval de maille
            bool(i) = .false.
         enddo
      endif
      lb1 = ir1
      if (ir2/=0) then
         lb3 = ir2+1  !---> modèle maillé
      else
         lb3 = ibmax+1  !---> modèle ramifié
      endif
end subroutine Ini_MatriceMaille
