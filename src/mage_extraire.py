import numpy as np
import sys
import matplotlib.animation as animation
import matplotlib.pyplot as plt

class real_result:
    def __init__(self, filename = ''):
        self.time = []
        self.values = []

    def append(self,t,v):
        self.time.append(t)
        self.values.append(v)

class data:
    def __init__(self, filename = ''):
        self.filename = filename
        if self.filename[-4:] == '.bin' or self.filename[-4:] == '.BIN':
            self.name = self.filename[:-4]
        else:
            self.name = self.filename
        self.ibmax = 0
        self.ismax = 0
        self.mage_version = 0
        self.is1 = []
        self.is2 = []
        self.xgeo = []
        self.zfd = []
        self.ygeo = []
        self.ybas = []
        self.zmin = 0.0
        self.ibu=[]
        self.ibs=[]
        self.values = {}
        self.values['Z']=real_result()
        self.values['Q']=real_result()
        self.values['S']=real_result()
        self.values['L']=real_result()
        self.values['M']=real_result()
        self.values['R']=real_result()
        self.values['U']=real_result()
        self.values['V']=real_result()
        self.values['W']=real_result()
        self.values['G']=real_result()
        self.values['D']=real_result()
        self.raw_data = []
        if filename == '': return
        print("lecture de ",self.filename)
        with open(self.filename,'rb') as f:
            # first line
            data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
            data = np.fromfile(f, dtype=np.int32, count=3)
            self.ibmax = data[0]
            print("nombre de biefs : ",self.ibmax)
            self.ismax = data[1]
            print("nombre de sections : ",self.ismax)
            self.mage_version = data[2]
            data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
            if self.mage_version < 0:
                print("Version de Mage : 7")
                kbl = self.mage_version * -1
                # second line
                data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
                data = np.fromfile(f, dtype=np.int32, count=self.ibmax)
                for i in range(self.ibmax):
                    self.ibu.append(data[i])
                data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
                # third line
                data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
                data = np.fromfile(f, dtype=np.int32, count=2*self.ibmax)
                for i in range(self.ibmax):
                    self.is1.append(data[2*i])
                    self.is2.append(data[2*i+1])
                data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
                # fourth line
                for i in range(0, self.ismax, kbl):
                    data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
                    data = np.fromfile(f, dtype=np.float32, count=min(kbl,self.ismax-i))
                    for j in range(min(kbl,self.ismax-i)):
                        self.xgeo.append(data[j])
                    data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
                # fifth line
                data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
                self.zmin = np.fromfile(f, dtype=np.float32, count=1)
                data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
                # sixth line
                for i in range(0, self.ismax, kbl):
                    data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
                    data = np.fromfile(f, dtype=np.float32, count=3*min(kbl,self.ismax-i))
                    for j in range(min(kbl,self.ismax-i)):
                        self.zfd.append(data[3*j])
                        self.ygeo.append(data[3*j+1])
                        self.ybas.append(data[3*j+2])
                    data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
                # seventh line
                for i in range(1, self.ismax+1, kbl):
                    data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
                    data = np.fromfile(f, dtype=np.int32, count=min(kbl,self.ismax-i+1))
                    for j in range(min(kbl,self.ismax-i+1)):
                        self.ibs.append(data[j])
                    data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)

            elif self.mage_version < 80:
                print("ERROR: the file ",self.filename," was created by an unknown version of Mage." )
                exit()
            else:
                print("Version de Mage : ",self.mage_version)
                for i in range(self.ibmax):
                    self.ibu.append(1)
                for i in range(self.ismax):
                    self.ibs.append(1)
                # second line
                data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
                data = np.fromfile(f, dtype=np.int32, count=2*self.ibmax)
                for i in range(self.ibmax):
                    self.is1.append(data[2*i])
                    self.is2.append(data[2*i+1])
                data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
                # third line
                data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
                data = np.fromfile(f, dtype=np.float32, count=self.ismax)
                for i in range(self.ismax):
                    self.xgeo.append(data[i])
                #self.xgeo.append(data[self.ismax-1])
                data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
                # fourth line
                data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
                data = np.fromfile(f, dtype=np.float32, count=3*self.ismax)
                for i in range(self.ismax):
                    self.zfd.append(data[3*i])
                    self.ygeo.append(data[3*i+1])
                    self.ybas.append(data[3*i+2])
                data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)

            # real data
            data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
            while data.size > 0:
                self.raw_data.append(data)
                npts = np.fromfile(f, dtype=np.int32, count=1)
                #print("npts = "+str(npts[0]))
                self.raw_data.append(npts)
                t1 = np.fromfile(f, dtype=np.float64, count=1)
                #print("t = "+str(t1[0]))
                self.raw_data.append(t1)
                a = np.fromfile(f, dtype=np.byte, count=1)
                #print("a = "+bytearray(a).decode())
                self.raw_data.append(a)
                data = np.fromfile(f, dtype=np.float32, count=npts[0])
                self.raw_data.append(data)
                self.values[bytearray(a).decode()].append(t1[0], data)
                data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
                self.raw_data.append(data)
                data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)

            print("nombre de pas de temps : ",len(self.values['Z'].time))

    def write_bin_8(self, name):
        if name[-4:] != '.bin' and name[-4:] != '.BIN':
            name = name+'.BIN'
        count = np.array([1],dtype=np.int32)
        with open(name,'wb') as f:
            # first line
            count[0] = 4*3
            count.tofile(f)
            np.array([self.ibmax, self.ismax, 82],dtype=np.int32).tofile(f)
            count.tofile(f)
            # second line
            count[0] = 4*2*self.ibmax
            count.tofile(f)
            ltmp = []
            for i in range(self.ibmax):
                ltmp.append(self.is1[i])
                ltmp.append(self.is2[i])
            np.array(ltmp,dtype=np.int32).tofile(f)
            count.tofile(f)
            # third line
            count[0] = 4*self.ismax
            count.tofile(f)
            np.array(self.xgeo,dtype=np.float32).tofile(f)
            count.tofile(f)
            # fourth line
            count[0] = 4*3*self.ismax
            count.tofile(f)
            ltmp = []
            for i in range(self.ismax):
                ltmp.append(self.zfd[i])
                ltmp.append(self.ygeo[i])
                ltmp.append(self.ybas[i])
            np.array(ltmp,dtype=np.float32).tofile(f)
            count.tofile(f)
            # real data
            for item in self.raw_data:
                item.tofile(f)

    def write_bin_7(self, name):
        if name[-4:] != '.bin' and name[-4:] != '.BIN':
            name = name+'.BIN'
        count = np.array([1],dtype=np.int32)
        with open(name,'wb') as f:
            # first line
            count[0] = 4*3
            count.tofile(f)
            np.array([self.ibmax, self.ismax, -1000],dtype=np.int32).tofile(f)
            count.tofile(f)
            kbl = 1000
            # second line
            count[0] = 4*self.ibmax
            count.tofile(f)
            ltmp = []
            for i in range(self.ibmax):
                ltmp.append(self.ibu[i])
            np.array(ltmp,dtype=np.int32).tofile(f)
            count.tofile(f)
            # third line
            count[0] = 4*2*self.ibmax
            count.tofile(f)
            ltmp = []
            for i in range(self.ibmax):
                ltmp.append(self.is1[i])
                ltmp.append(self.is2[i])
            np.array(ltmp,dtype=np.int32).tofile(f)
            count.tofile(f)
            # fourth line
            for i in range(0, self.ismax, kbl):
                count[0] = 4*min(kbl,self.ismax-i)
                count.tofile(f)
                ltmp = []
                for j in range(min(kbl,self.ismax-i)):
                    ltmp.append(self.xgeo[j])
                np.array(ltmp,dtype=np.float32).tofile(f)
                count.tofile(f)
            # fifth line
            count[0] = 4
            count.tofile(f)
            np.array([self.zmin],dtype=np.float32).tofile(f)
            count.tofile(f)
            # sixth line
            for i in range(0, self.ismax, kbl):
                count[0] = 4*3*min(kbl,self.ismax-i)
                count.tofile(f)
                ltmp = []
                for j in range(min(kbl,self.ismax-i)):
                    ltmp.append(self.zfd[j])
                    ltmp.append(self.ygeo[j])
                    ltmp.append(self.ybas[j])
                np.array(ltmp,dtype=np.int32).tofile(f)
                count.tofile(f)
            # seventh line
            for i in range(0, self.ismax, kbl):
                count[0] = 4*min(kbl,self.ismax-i)
                count.tofile(f)
                ltmp = []
                for j in range(min(kbl,self.ismax-i)):
                    ltmp.append(self.ibs[j])
                np.array(ltmp,dtype=np.int32).tofile(f)
                count.tofile(f)
            # real data
            for item in self.raw_data:
                item.tofile(f)

def animate(i):
    l = []
    for j in range(len(lines)):
        lines[j][0].set_data(res[j].xgeo, res[j].values['Z'].values[i])
        l.append(lines[j][0])
    return l

if __name__ == "__main__":
    ifilename = []
    if len(sys.argv) < 1:
        print("ERROR: no input file name given")
        exit()
    else:
        for name in sys.argv[1:]:
            ifilename.append(str(name))

    res = []
    for name in ifilename:
        res.append(data(name))

    nt = min([len(r.values['Z'].time) for r in res])

    fig = plt.figure()
    mm = 0
    jj = 0
    for j in range(nt):
        #m = max(max([r.values['Z'].values[j] for r in res]))
        m = max(res[0].values['Z'].values[j])
        if m > mm:
            jj = j
            mm = m

    lines = []
    line, = plt.plot(res[0].xgeo, res[0].values['Z'].values[jj])
    line.set_alpha(0.0)
    for r in res:
        lines.append(plt.plot([], []))
    if res[0].mage_version > 80:
        line3 = plt.plot(res[0].xgeo, res[0].zfd)
    else:
        line3 = plt.plot(res[0].xgeo, res[0].zfd + res[0].zmin)
    fig.tight_layout()

    ani = animation.FuncAnimation(fig, animate, frames=nt,
                              interval=10, blit=True, repeat=True)
    plt.show()
