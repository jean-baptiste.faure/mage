import numpy as np
import sys

class Biefsed:
    def __init__(self, ns=0, t=0.0, sectionsed=[]):
        self.ns = ns
        self.t = t
        self.sectionsed = sectionsed

class Sectionsed:
    def __init__(self, nz=0, isect=0, layers=[]):
        self.nz = nz
        self.isect = isect
        self.layers = layers

class Layers:
    def __init__(self, ncs=0, h=[], d50=[], sigma=[]):
        self.ncs = ncs
        self.h = h
        self.d50 = d50
        self.sigma = sigma

def read_bin(ifilename):
    print("lecture de ",ifilename)
    with open(ifilename,'rb') as f:
        # first line
        data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
        data = np.fromfile(f, dtype=np.int32, count=3)
        ibmax = data[0]
        print("nombre de biefs : ",ibmax)
        ismax = data[1]
        print("nombre de sections : ",ismax)
        mage_version = data[2]
        print("Version de Mage : ",mage_version)
        data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
        if mage_version < 80:
            print("ERROR: the file ",ifilename," was created by an older version of Mage." )
            exit()
        elif mage_version <= 82:
            # second line
            data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
            data = np.fromfile(f, dtype=np.int32, count=2*ibmax)
            is1 = []
            is2 = []
            for i in range(ibmax):
                is1.append(data[2*i])
                is2.append(data[2*i+1])
            data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
            # third line
            data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
            data = np.fromfile(f, dtype=np.float32, count=ismax)
            xl = []
            for i in range(ismax):
                xl.append(data[i])
            xl.append(data[ismax-1])
            data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
            # fourth line
            data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
            data = np.fromfile(f, dtype=np.int32, count=ismax)
            npts = []
            for i in range(ismax):
                npts.append(data[i])
            data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)

            # real data
            datat=[]
            t=[]
            data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
            while data.size > 0:
                ns = np.fromfile(f, dtype=np.int32, count=1)[0]
                t1 = np.fromfile(f, dtype=np.float64, count=1)[0]
                print("Reading timestep t : ",t1)
                data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
                newbiefsed = Biefsed(ns=ns, t=t1)
                for i in range(ns):
                    data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
                    nzone = np.fromfile(f, dtype=np.int32, count=1)[0]
                    data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
                    newsectionsed = Sectionsed(nz=nzone, isect=i+1)
                    for j in range(nzone):
                        data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
                        ncs = np.fromfile(f, dtype=np.int32, count=1)[0]
                        data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
                        newlayer = Layers(ncs=ncs)
                        for k in range(ncs):
                            data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
                            data = np.fromfile(f, dtype=np.float64, count=3)
                            data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
                            newlayer.h.append(data[0])
                            newlayer.d50.append(data[1])
                            newlayer.sigma.append(data[2])
                        newsectionsed.layers.append(newlayer)
                    newbiefsed.sectionsed.append(newsectionsed)
                datat.append(newbiefsed)
                t.append(t1)

                data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
        else:
            # second line
            data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
            data = np.fromfile(f, dtype=np.int32, count=2*ibmax)
            is1 = []
            is2 = []
            for i in range(ibmax):
                is1.append(data[2*i])
                is2.append(data[2*i+1])
            data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
            # third line
            data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
            data = np.fromfile(f, dtype=np.float32, count=ismax)
            xl = []
            for i in range(ismax):
                xl.append(data[i])
            xl.append(data[ismax-1])
            data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
            # fourth line
            data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
            data = np.fromfile(f, dtype=np.int32, count=ismax)
            npts = []
            for i in range(ismax):
                npts.append(data[i])
            data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)

            # real data
            datat=[]
            t=[]
            data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
            print('l = '+str(data[0]))
            while data.size > 0:
                ns = np.fromfile(f, dtype=np.int32, count=1)[0]
                t1 = np.fromfile(f, dtype=np.float64, count=1)[0]
                with_charriage = np.fromfile(f, dtype=np.int32, count=1)[0]
                data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
                print("Reading timestep t : ",t1)
                newbiefsed = Biefsed(ns=ns, t=t1)

                if with_charriage == 1:
                    data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
                    npts = np.fromfile(f, dtype=np.int32, count=ns)
                    data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
                else:
                    npts = np.ones(ns, dtype=np.int32)
                sum_pts = npts.sum()

                data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
                ncs  = np.fromfile(f, dtype=np.int32, count=sum_pts)
                data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)

                data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
                hds  = np.fromfile(f, dtype=np.float64, count=ncs.sum()*3)
                data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)
                ipt = 0
                ics = 0
                ihds = 0
                for i in range(ns):
                    newsectionsed = Sectionsed(nz=npts[i], isect=i+1)
                    for j in range(npts[i]):
                        newlayer = Layers(ncs=ncs[ipt])
                        for k in range(ncs[ipt]):
                            newlayer.h.append(hds[ihds])
                            newlayer.d50.append(hds[ihds+1])
                            newlayer.sigma.append(hds[ihds+2])
                            ihds += 3
                        ipt += 1
                        newsectionsed.layers.append(newlayer)
                    newbiefsed.sectionsed.append(newsectionsed)
                datat.append(newbiefsed)
                t.append(t1)

                data = np.fromfile(f, dtype=np.int32, count=1) # line length (bytes)

    print("nombre de pas de temps : ",len(t))


if __name__ == "__main__":
    if len(sys.argv) > 1:
        ifilename = str(sys.argv[1])
    else :
        print("ERROR: no input file name given")
        exit()

    read_bin(ifilename)
