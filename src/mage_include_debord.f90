!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - INRAE - 1989-2020                           #
!# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################

   if (y < sec_is%ymoy) then  ! pas de débordement
      sdyn = sec_is%section_mouillee(z)
      pdyn = sec_is%perimetre(z)
      debord_ok = .false.
   else                                       ! débordement en lit moyen et/ou majeur
      debord_ok = .true.
      ! lit mineur
      lmin = sec_is%almy
      smin = sec_is%smy + lmin*(y-sec_is%ymoy)
      pmin = sec_is%pmy + 2._long*(y-sec_is%ymoy)  ! on tient compte de l'interface mineur / moyen
      ! section dynamique et lit moyen
      ldyn = sec_is%largeur(z)       ! largeur totale puisque pas de lit majeur
      sdyn = max(sec_is%section_mouillee(z),smin)       ! idem
      pdyn = max(sec_is%perimetre(z),pmin)     ! idem
      lmoy = ldyn-lmin
      smoy = sdyn-smin
      pmoy = pdyn - sec_is%pmy + 2._long*(y-sec_is%ymoy)  ! on tient compte de l'interface mineur / moyen

      if (smoy < zero) then ! vérification sur la section mouillée
         block
            type(section_values) :: sec_val
            sec_val%smoy = smoy
            call err014(is,1,sec_val)
         end block
      endif
      if (pmoy < zero) then ! vérification sur le perimètre mouillé
         block
            type(section_values) :: sec_val
            sec_val%pmoy = pmoy ; sec_val%pmin = pmin
            call err014(is,2,sec_val)
         end block
      endif
      ! cas d'un tout petit lit moyen : section dynamique = lit mineur
      if ((smoy >= zero) .and. (smoy < 0.01_long*sdyn)) then ! le lit moyen est traité comme un lit majeur (stockage seul)
         ldyn = lmin  ;  sdyn = smin  ;  pdyn = pmin
         lmoy = zero  ;  smoy = zero  ;  pmoy = zero
         debord_ok = .false.
      end if
   endif
