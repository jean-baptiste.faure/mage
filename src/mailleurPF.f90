!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteurs : Pierre FARISSIER & Jean-Baptiste FAURE - INRAE - 1989-2020       #
!# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################
program mailleurPF
!  program principal pour SECMA qui a éte transformé en sous-programme

   implicit none
   character :: ficin*35, ficout*35, ficlog*35, ficopt*35
   double precision :: tmoy
   integer :: n, ios, iarg
   character :: ctmoy*10
   character(len=40) :: argmnt
   interface
      character*10 function read_option(lu,key)
         integer,intent(in) :: lu
         character(len=*),intent(in) :: key
      end function read_option
   end interface

   iarg = 1 ; call get_command_argument(iarg,argmnt)
   if (len_trim(argmnt) > 0) then  !il y a des arguments sur la ligne de commande
      ficin = trim(argmnt)
      n = scan(ficin,'.',.true.) !on cherche le . le plus à droite
      if (n == 0) ficin = trim(ficin)//'.ST'
      iarg = iarg+1 ; call get_command_argument(iarg,argmnt)
      if (len_trim(argmnt) > 0) then
         ficout = trim(argmnt)
      else
         n = scan(ficin,'.',.true.)
         if (n == 0) then
            ficout = trim(ficin)//'.M'
         else
            ficout = ficin(1:n-1)//'.M'
         endif
      endif
   else
      write(*,'('' NOM DU FICHIER DE SECTIONS .ST: '',$)')  !ne pas modifier la chaîne, elle est utilisée telle quelle par PamHyr
      read(*,'(a15)') ficin

      write(*,'('' NOM DU FICHIER DE MAILLES EN SORTIE : '',$)')  !ne pas modifier la chaîne, elle est utilisée telle quelle par PamHyr
      read(*,'(a15)') ficout
   endif

   n = scan(ficin,'.',.true.)
   !fichier de log
   ficlog = ficin(1:n-1)//'_mailleur.log'
   open(unit=9,file = trim(ficlog),status='unknown',form='formatted')
   !fichier pour lire et enregistrer les options utilisées
   ficopt = ficin(1:n-1)//'_options_mailleur'
   open(unit=7,file = trim(ficopt),status='unknown',form='formatted')

   ctmoy = read_option(7,'tmoy')
   if (ctmoy == '') then
      write(*,'(''TAILLE DE LA MAILLE MOYENNE: '',$)')  !ne pas modifier la chaîne, elle est utilisée telle qu'elle par PamHyr
      read(*,*) tmoy
      write(7,'(a)') '*Taille de la maille moyenne'
      write(7,'(a,f8.3)') '*tmoy = ',tmoy
   else
      read(ctmoy,*,iostat=ios) tmoy
      if (ios > 0) then
         write(0,*) ' Erreur dans fichier d''options : ',ctmoy
         stop 2
      endif
   endif

   write(9,*)
   write(9,*) ' Lancement de la version automatique de SECMA'
   write(9,*) ' --------------------------------------------'
   write(9,*)
   write(9,*) ' Paramètres définis par défaut :'
   write(9,*) ' 1/ prise en compte de toutes les lignes directrices'
   write(9,*) ' 2/ 12 mailles par zone (entre 2 lignes directrices)'
   write(9,*) ' 3/ pas d''optimisation'
   write(9,*) ' 4/ interpolation linéaire des altitudes'
   write(9,*) ' 5/ mesure des pk le long de la ligne directrice centrale ou suivante'
   write(9,*)
   write(9,*) ' L''option 2 peut être modifiée avec le mot clé nb_mailles_par_zone dans le fichier d''options'
   write(9,*) ' si un des mots clé est absent du fichier d''options, il est ajouté automatiquement avec la valeur par défaut '
   write(9,*) ' NB : pour réduire le pas d''espace transversal, il faut ajouter des lignes directrices ou modifier la valeur'
   write(9,*) '      de la clé nb_mailles_par_zone dans le fichier d''options'
   write(9,*)
   write(9,*)
   write(9,*)


   call secma(ficin,ficout,tmoy)
   close (9)
end program mailleurPF

character*10 function read_option(lu,key)
   !recherche dans le fichier lié à l'unité logique LU une option définie par la clé KEY
   !en sortie read_option vaut '' (null) si la clé n'est pas trouvée, sinon la valeur de la clé
   implicit none
   !--prototype
   integer,intent(in) :: lu
   character(len=*),intent(in) :: key
   !--variables
   character(len=80) :: ligne
   integer :: ios, n, k

   read_option = ''
   rewind(lu)
   do
      read(lu,'(a)',iostat=ios) ligne
      if (ios < 0)  then
         backspace(lu)
         exit ! fin du fichier
      elseif (ios > 0) then
         write(0,*) ' >>>> Erreur dans le fichier d''options à la ligne : '
         write(0,'(a)') ligne
         stop 777
      elseif (ligne(1:1) == '*' .or. trim(ligne) == '') then
         cycle
      else
         k = scan(ligne,'=')
         if (k > 0) then
            n = len_trim(key)
            if (ligne(1:n) == trim(key)) then
               read_option = trim(adjustl(ligne(k+1:)))
               exit
            else
               cycle
            endif
         endif
      endif
   enddo
end function read_option
