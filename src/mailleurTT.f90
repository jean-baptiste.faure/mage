!##############################################################################
!#                                                                            #
!#                           PROGRAM MailleurTT                               #
!#                                                                            #
!# Auteurs : Théophile Terraz - INRAE - 2024                                  #
!# Licence : lGPL                                                             #
!#                                                                            #
!# Copyright (C) 2024 INRAE                                                   #
!#                                                                            #
!# This program  is free software; you can redistribute it and/or             #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# This program  is distributed in the hope that it will be useful, but       #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# This program . If not, see <http://www.gnu.org/licenses/>.                 #
!##############################################################################

program mailleurTT

  use objet_bief

  implicit none

  type(bief), target :: mon_bief
  integer :: i, nb_lines, nb_sections
  integer :: lplan, lineaire, origine
  character(len=250) :: arg
  character(len=250) :: st_file, m_file, command
  character(len=3) :: tags(2)
  real*8 :: step, origin_value
  integer, dimension(2) :: limites
  integer, dimension(1) :: flm
  character(len=3), dimension(1) :: directrice1, directrice2

  i = 1
  ! st_name
  call get_command_argument(i, arg)
  if (len_trim(arg) == 0) stop i
  st_file = trim(arg)
  i = i+1
  ! m_name
  call get_command_argument(i, arg)
  if (len_trim(arg) == 0) stop i
  m_file = trim(arg)
  i = i+1
  ! command
  call get_command_argument(i, arg)
  if (len_trim(arg) == 0) then
    command = "mesh"
  else
    command = trim(arg)
  endif
  i = i+1
  ! step
  call get_command_argument(i, arg)
  if (len_trim(arg) == 0) stop i
  read(arg,*) step
  i = i+1
  ! limite1
  call get_command_argument(i, arg)
  if (len_trim(arg) == 0) stop i
  read(arg,*) limites(1)
  i = i+1
  ! limite2
  call get_command_argument(i, arg)
  if (len_trim(arg) == 0) stop i
  read(arg,*) limites(2)
  i = i+1
  ! directrice1
  call get_command_argument(i, arg)
  if (len_trim(arg) == 0) stop i
  directrice1 = trim(arg)
  i = i+1
  ! directrice2
  call get_command_argument(i, arg)
  if (len_trim(arg) == 0) stop i
  directrice2 = trim(arg)
  i = i+1
  ! lplan
  call get_command_argument(i, arg)
  if (len_trim(arg) == 0) stop i
  read(arg,*) lplan
  i = i+1
  ! lm
  call get_command_argument(i, arg)
  if (len_trim(arg) == 0) stop i
  read(arg,*) flm(1)
  i = i+1
  ! lineaire
  call get_command_argument(i, arg)
  if (len_trim(arg) == 0) stop i
  read(arg,*) lineaire
  i = i+1
  ! origin
  call get_command_argument(i, arg)
  if (len_trim(arg) == 0) then
    origine = 1
  else
    read(arg,*) origine
  endif
  i = i+1
  ! origin value
  call get_command_argument(i, arg)
  if (len_trim(arg) == 0) then
    origin_value = 0.0
  else
    read(arg,*) origin_value
  endif
  i = i+1

  call mon_bief%init(st_file, 0, 0)
  call mon_bief%set_name("tmp", 3)

  if (command == "mesh") then
    tags(2) = "un"
    call mon_bief%get_nb_lines(nb_lines)
    nb_sections = mon_bief%nprof
    do i=1,nb_lines
        tags(1) = tags(2)
        call mon_bief%get_line_tag(i, tags(2))
        call st_to_m(mon_bief, 0, tags(1), tags(2))
    enddo
    tags(1) = tags(2)
    tags(2) = "np"
    call st_to_m(mon_bief, 0, tags(1), tags(2))

    if (limites(1) == -1) limites(1) = 1
    if (limites(2) == -1) limites(2) = nb_sections
    if (flm(1) == -1) flm(1) = 3
    ! directrice1(1) = "un"
    ! directrice2(1) = "np"

    call mon_bief%interpolate_profils_pas_transversal(limites, directrice1, directrice2, step, lplan == 1, flm, lineaire == 1)

    call mon_bief%purge()

  else if (command == "update_kp") then

    ! lplan is the orintation of the bief:
    ! 1: up -> downstream
    ! 2: down -> upstream
    ! else: keep current orientation
    call mon_bief%update_pk(directrice1(1), directrice2(1), origine, origin_value, lplan)
  else
    stop i
  endif

  call mon_bief%output_bief(m_file)

  contains

  subroutine st_to_m(mon_bief, npoints, tag1, tag2)
    ! prototype
    type(bief), target, intent(inout) :: mon_bief
    integer, intent(in) :: npoints
    character(len=3) :: tag1, tag2
    ! variables locales
    type(bief) :: bief_tmp
    integer :: i

    call mon_bief%extract(bief_tmp, tag1, tag2)
    call bief_tmp%st_to_m_compl(npoints)
    do i=1,bief_tmp%nprof
        if (tag1 .ne. "un") then
            bief_tmp%sections(i)%xyz(1)%tag = tag1
        else
            bief_tmp%sections(i)%xyz(1)%tag = '   '
        endif
        if (tag2 .ne. "np") then
            bief_tmp%sections(i)%xyz(bief_tmp%sections(i)%np)%tag = tag2
        else
            bief_tmp%sections(i)%xyz(bief_tmp%sections(i)%np)%tag = '   '
        endif
    enddo
    call mon_bief%patch(bief_tmp, tag1, tag2)
  end subroutine st_to_m

end program
