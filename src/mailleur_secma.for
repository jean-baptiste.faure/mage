! pas de modif faite pour ratio negatif points insuffisants
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!                                                                             C
! Nom du fichier: SECMA5.FOR                  Date: 05/08/91      Auteur:PF   C
!                                                                             C
! But:  CREER DES SECTION EN TRAVERS INTERMEDIAIRES                           C
!       D'UN FICHIER DE SECTIONS EN TRAVERS                                   C
!       INTERPOL ENTRE CN : BEZIER ETIMATEUR SIMPLE                           C
!                                                                             C
!       NUMERISATION DES SECTIONS: ABCISSE CURVILIGNE                         C
!       NB DE SECTION INTER AU CHOIX                                          C
!       DECOUPE EN SECTIONS PLANES                                            C
!                                                                             C
!       avec UTILISATION DES ZONES ET NEW FORMAT F12.4                        C
! AUTOMATIQUE: TAILLE DE LA MAILLE EN DONN�E                                 C
!                                                                             C
! IDEM SECMA2 MAIS DECOUPAGES EN SOUSROUTINES...                              C
! 17 12 91 autorise zone de taille nulle                                      C
! V3.3 13 01 92 CHOIX PL/BORD POUR DECOUPAGE DES COURBES                      C
! V3.4 10 02 92 calcul des pk pour sections interpolees                       C
! V3.4 11 02 92 CHOIX DES LIGNES PRISES EN COMPTE                             C
! V3.5 22 06 92 MARQUAGE DES LIGNES DIRECTRICES DANS LE .M + DECOUPAGE        C
!               REPARTITION DES PLANS DE COUPES SELON EQUIDISTANCE/COURBES    C
!               GESTION DES SECTIONS NON PLANES                               C
!               2 TYPE DE DERIVES: OCS OU PERPENDICULAIRE AU ST               C
!      29 06 92 TRANSFERT DU CODE ASSOCIE AUX POINTS DIRECTEURS               C
! V4.0 08 07 92 optimisation du nombre de maille (bof...)                     C
!               VERIF ESTIM DERIVEE: MEME POINT=MEME DERIVEE                  C
!               SECURISATION MINI
! V4.1 17 09 92 OPTION LINZ INTERPOLATION LINEAIRE SUR Z
! V4.2 15 10 92 CORRECTION PROVISOIRE BAVURE DERIVEE POUR LIGNES CONFONDUES   C
!               SOL DEFINITIVE : ECRIRE BASE DE DISTANCE (COMME ANGLE)        C
!               + LIGNE + DIRECTRICE QUE D'AUTRE.... BORDEL TOTAL
!                 -> A ECRIRE
! V5.0 10 09 93 REECRITURE DE L INTERPOLATEUR DE TRAJECTOIRE: SPLINE T B C
!               + MODULARISATION
!              A REGLER: Z LIN, MISE A PERPENDICULAIRE, CONTROLE PT
!               CONFONDUS, T VARIABLE, CB IDEM...
!V5.01 28 10 94 NOM SUR LES SECTIONS
!               FIN DE LA NOTION DE LIT MINEUR: DEMANDE LD POUR DECOUPE ET PK
!V5.02 2  11 94 PAS ENTRE MAX SECTION LD ET BORDS
!V5.1     11 96 LECTURE d'un .ST en entree
! secma 54g avec choix des lignes directrices
! modification du 24/2/2000 passage en double precision
! secma54 du 31/8/00 avec nombre de zones porte de 30 � 220 puis 1000 plus tard
! passage a 20 comme coeffcient multiplicatuer pour optimiser et a 30000 points le 4 juin 2007
! le 31 aout 2007, correction du nombre de mailles et introduction d'un decoupage lineaire au lieu de splin en longitudinal
! le 23 janvier 2008 : option de conserver tous les points des sections pour transformer st en m
! le 4 f�vrier 2008 : ajout d'uen distance minimale sur l'axe longitudinal (correction 07/04/08)
! le 18fevrier 2008 : verification pour ne pas ajouter des points differents dans deux sections brutes confondues = version 6.3
! le 1 octobre 2008 : ajout de pas longitudinal variable
! le 19 aout 2009 v6.5 : ajout option pour eliminer points doubles hors lignes directrices
! le 11 mars 2011 : coorection pour le recalcul des PK
! le 9 mai 2011: version 7.0 : rotation, translation et homothetie 
! des sections interpolees pour tenir compte de lignes limites
! le 12 mars 2014 : version 8.0 on tient compte de limites par ligne
! et on applique distance minimale apres
! le 5 mai 2014 : correction de l'appel a apartseg pour eviter points nouveaux dans sections confondues
! le 18 septembre 2014  modif por application dismin, aussi si indice  +1 ou -1
! le 13 novembre 2014 : correction de la modif du 18 septembre 2014; 
! en outre si un point chang�, tous confondus suivants chang�s
! 25 novembre 2014: pour appliquer dismin on compare a tous les points de la section -1
! 1er decembre : on applique dismin par zone pour ne pas changer les points sur directrices (et bords)
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine secma(ficin,ficout,tmoy)  !program SECMA
      implicit none
      INTEGER NOST,NOPT,NBPTSTI,NBST,NBSTF,n
     :,NBLGN,NBPTZN,MZOP,NBPTZOP,NBPTST,LINZ
     :,NPTFIN,NBLGNO,LPC,NZONE,NSTPER,KSTPLAN,NBPER,NBHISTO
     :,NBSTI,NOL,NOZN,NPTDEP,NOLO,NOPTIDEP
     :,NLDAXE,NBPTZ,NO
     :,KS1,KP1,KZO,KS2,KP2,KP3,PTLGN,N2ZN,I
      DOUBLE PRECISION Z,X,Y,XB,YB,ZB,VERSION
     :,PK,PKF,T0,H31,H32,H2
     :, T1,DY1,DZ1,B0,B1,C0,C1,DZ0,DX1,DX0,DY0
     :,PASM,DPLCTOT
      CHARACTER CLIGN*4,CPLAN*1
      INTEGER IPLAN
! variable pour choisir maillage en plan ou en 3D     
      LOGICAL LPLAN
! variable pour interpolation lineaire ou par spline
      LOGICAL LINEAIRE 
! variable pour corriger sections intermediaires selon bords
      LOGICAL AJUST
!
      PARAMETER (VERSION=8.0)
! NB ST MAX EN ENTREE
      PARAMETER (KS1=1500)
! NB DE POINT PAR ST MAX EN ENTREE
      PARAMETER (KP1=1000)
! NB ZONE MAX
      PARAMETER (KZO=1000)
! NB ST MAX DANS BD INTERMEDIAIRE
      PARAMETER (KS2=1500)
! NB PT PAR ST MAX DANS BD INTERMEDIAIRE
      PARAMETER (KP2=10500)
! NB PT PAR ST MAX DANS BD FINALE (POST OPTIMIZ)
      PARAMETER (KP3=1000)
      DOUBLE PRECISION  STX(KP1,KS1),STY(KP1,KS1),STZ(KP1,KS1),
     +  STIX(KP2,KS1),STIY(KP2,KS1),STIZ(KP2,KS1),
     + STFX(KP2,KS2),STFY(KP2,KS2),
     + STFZ(KP2,KS2),STFOX(KP3,KS2),STFOY(KP3,KS2),
     + STFOZ(KP3,KS2)
      CHARACTER FICIN*35, FICOUT*35
      CHARACTER  REP*1,CHAR2*2,CHAR10*10
! pour ne donner que 1 pas transversal
      Logical UNPAS
! variable logique pour completer nombre de points zone a nombre de points maximal
      LOGICAL COMPL(KZO)       
!
!
! ENTETE DE FICHIER: IE HISTORIQUE DU TRAITEMENT:
      CHARACTER HISTO*80
      COMMON/BDHISTO/HISTO(20)/NBHISTO/NBHISTO
      COMMON/HISTO/H2,H31,H32,LINZ,NLDAXE   
!
! BD DIM DES STRUCTURES:
! nbst   : nombre de st du fichier de depart  (<KS1)
! nbptsti: nombre de point des st du maillage (<KP2 PUIS KP3)
! nbstf  : nombre de st du maillage           (<KS2)
!
      COMMON/BDDIM/NBST,NBPTSTI,NBSTF
!
! BD ST  (ORIGINE FICHIER de depart)
!  NBPTST()*NBST -> COTE X Y Z DU POINT 
      COMMON/BDST/NBPTST(KS1),STX,STY,STZ

! BD ENTETE DES SECTIONS
! PK: FICHIER ORIGINE, PKF: PK MAILLAGE
      COMMON/BDENTETE/PK(KS1),PKF(KS2)
! BD NOM DES SECTIONS ORIGINE
      CHARACTER*12 NOMST
      COMMON /BDNOMST/NOMST(KS1)

! BD ZONES (SECTION X NOZONE)->NO PT D'APPUI SUR LA SECTION
! NBLGNO=NB LIGNE .SZ, LPC:LIGNE PRISE EN COMPTE, 
! NBLGN:NB LGN PRISE EN COMPTE 
      CHARACTER CODLGN*3
	   CHARACTER*1 Charajust
      COMMON /BDZO/NZONE(KS1,KZO),NBLGNO,LPC(KZO),CODLGN(KZO)
!
! INDICE DES LIGNES DIRECTRICES DANS LE MAILLAGE
! + NB POINT /ZONE, PAS MOYEN /ST/ZONE, 
      COMMON/BDLGN/PTLGN(0:KZO+1),PASM(KZO)
      COMMON/NBDLGN/NBLGN,NBPTZN(KZO)
!
! BD POUR MODULE OPTIMISATION 
! MZOP ZONE OPTIMISER=1, 
      COMMON/BDOPT/MZOP(KZO),NBPTZOP(KZO)
!
! BD FLAG POUR ST MESUREES PLANES:
      COMMON /BDSTPLAN/ KSTPLAN(KS1)
!                          
! BD ST INTERMEDIAIRE (ST de points equidistants SELON ABCISSE CURVILIGNE )
!                      INTERPOL LIN ENTRE PTS D'UNE MEME SECTION
! NBPT-DE-LA-ZONE * NB-SECTIONS
!   NBPTSTI*NBST  
      COMMON/STI/STIX,STIY,STIZ
!
! BD ST OU LES TRAJ SONT PERPENDICULAIRE AU ST (PLAN X,Y)
      COMMON/BDSTPER/NBPER,NSTPER(KS1)
!
! BD DES TENSION CONTINUITE BIAIS A DROITE (0) ET GAUCHE (1) DE CHAQUE PT
      COMMON/TCB/T0(KP2),T1(KP2),C0(KP2),C1(KP2),B0(KP2),B1(KP2)
! BD DES TGTES AUX PTS DES STI A DROITE (0) ET GAUCHE (1) 
      COMMON/DERIV/DX0(KP2),DY0(KP2),DZ0(KP2),DX1(KP2),DY1(KP2),DZ1(KP2)
!
! BD ST  FINALE  AVANT OPTIMISATION NBPTSTI*NBSTF          
! NBSTI nb des section inter entre section de depart
      COMMON/BDSTF/ NBSTI(KS2),STFX,STFY,
     + STFZ
!
! BD STF APRES OPTIMISATION....
      COMMON/BDSTFO/ STFOX,STFOY,
     + STFOZ
!
! CALCULS LOCAUX: BD LONGUEUR POUR ST EN ENTREE
      DOUBLE PRECISION LSEG(KP1,KS1),LCUM(KP1,KS1),LTOT(KS1)
      DOUBLE PRECISION LTOTMIN,LTOTMAX,LTOTMOY
! PROFIL EN LONG:
      INTEGER NOPTLM(KS2)
      COMMON/numerodir/NOPTLM
      double precision xlg(0:kzo,kp2),ylg(0:kzo,kp2),zlg(0:kzo,kp2)
      INTEGER NLG(0:KZO)
      double precision tmoy
      common/xyzlg/xlg,ylg,zlg
	   common/nlg/nlg
       
      UNPAS=.FALSE. 
!       
      WRITE(9,*) ' But:  CREER UN MAILLAGE DE LA RIVIERE BASEE '
      WRITE(9,*) '       SUR  DES SECTIONS EN TRAVERS'
      WRITE(9,*) '      -NUMERISATION DES SECTIONS: ABCISSE CURVILIGNE'
      WRITE(9,*) '      -INTERPOL ENTRE CN : SPLINE T C B '
!    WRITE(9,*) '     (PAR DEFAUT INTERPOLATION LINEAIRE 2D)'
      WRITE(9,*) '      -PRISE EN COMPTE DE ZONES'
      WRITE(9,*) '      -OPTIMISATION DES MAILLES '
!    WRITE(9,*) '     (PAR DEFAUT NOMBRE DE POINTS PAR ZONE CONSTANT)'
      WRITE(9,*) ' '
      WRITE(9,'(A13,F3.1)')'   VERSION: ',VERSION
      WRITE(9,*) ' '
!
! DEMANDE ET LECTURE DU FICHIER .ST
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
 21   WRITE(9,20)
 20   FORMAT(//,' NOM DU FICHIER DE SECTIONS .ST: ',$)
      !READ(5,'(A35)') FICIN  !-JBF 2015/06/17 : r�ponse en argument
      OPEN(11,FILE=FICIN,STATUS='OLD',ERR=21)
 23   WRITE(9,22)
 22   FORMAT(//,' NOM DU FICHIER DE SECTIONS DE MAILLAGE EN SORTIE : '
     :,$)
      !READ(5,'(A35)') FICOUT  !-JBF 2015/06/17 : r�ponse en argument
      OPEN(10,FILE=FICOUT,STATUS='unknown',ERR=23)
      CALL LECT_FIC(11)
      CLOSE(11)
      WRITE(9,*)'si vous voulez un maillage '
      WRITE(9,*)'calculees sur des distances horizontales'
      WRITE(9,*)'entrez 2(-2 pour interpolation longitudinale spline)'
      WRITE(9,*)'sinon les distances seront en 3D'
      WRITE(9,*)'entrez 3(-3 pour interpolation longitudinale lineaire)'
      WRITE(9,*)'(defaut = 2)'
      !READ(*,'(a2)')char2
      !Read(char2,'(I2)')IPLAN
      iplan = -3 !WRITE(9,*)iplan 
      IF(IPLAN.EQ.-2)THEN
        LPLAN=.TRUE.
        LINEAIRE=.FALSE.
      ELSEIF(IPLAN.EQ.3)THEN
        LPLAN=.FALSE.
        LINEAIRE=.FALSE.
      ELSEIF(IPLAN.EQ.-3)THEN
        LPLAN=.FALSE.
        LINEAIRE=.TRUE.
      ELSE
        LPLAN=.TRUE.
        LINEAIRE=.TRUE.
      ENDIF    
!
! TEST PLANEITE DES SECTIONS
      CALL ST_PLAN
!
! DEMANDE DE PRISE EN COMPTE DES LIGNES:
      NBLGN=0
      LPC(2)=0
!      WRITE(9,*) 'NBLGNO',NBLGNO
      DO 50 NOL=2,NBLGNO-1
      WRITE(9,*) 'PRISE EN COMPTE DE LA LIGNE',NOL-1,' (0/1)'
      If(NOL.EQ.2)then
        WRITE(9,*)'repondez 2 pour prise en compte de toutes les lignes'
        WRITE(9,*)'repondez 3 pour prise en compte de toutes les lignes'
        WRITE(9,*) 'et un seul pas espace transversal'
        WRITE(9,*) 'repondez 4 pour prise en compte de aucune ligne'
        WRITE(9,*) '(defaut = 3)'
      ELSE
        WRITE(9,*) 
     &         'repondez 2 pour prise en compte toutes lignes suivantes'
        WRITE(9,*) '(defaut = 1)'
      endif
      cplan = ' ' !READ (*,'(A)')CPLAN  !-JBF 2015-06-17 : r�ponse par d�faut
! 51     READ (*,'(A)')CPLAN
      IF(CPLAN.NE.' ')THEN
        READ(CPLAN,'(I1)')LPC(NOL)
      ELSEIF(NOL.EQ.2)THEN
        LPC(NOL)=3
      ELSE
        LPC(NOL)=1
      ENDIF    
      IF (LPC(NOL).EQ.1)then
        NBLGN=NBLGN+1
      elseif(lpc(nol).eq.2)then
        do n=nol,nblgno-1
          lpc(n)=1
        enddo
        nblgn=nblgn+nblgno-nol
        go to 550
      elseif(lpc(nol).eq.3)then
        UNPAS=.TRUE.
        do n=2,nblgno-1
          lpc(n)=1
        enddo
        nblgn=nblgno-2
        goto 550
!     elseif(lpc(nol).ne.0)then  
!          GOTO 51
      elseif(lpc(nol).eq.4)then
        do n=nol,nblgno-1
          lpc(n)=0
        enddo
        goto 550
      endif   
 50   CONTINUE
 550  LPC(1)=1
      LPC(NBLGNO)=1
      WRITE(9,*) 'PRISE EN COMPTE DE ',NBLGN,' LIGNES'
      WRITE(9,*) ' SOIT ',NBLGN+1,' ZONES'
! mise a jour du nom des directrices retenues
      NOL=1
      DO NOLO=1,NBLGNO
      IF(LPC(NOLO).EQ.1) THEN
         CODLGN(NOL)=CODLGN(NOLO)
         NOL=NOL+1
      ENDIF
      ENDDO
! MISE A JOUR DE NZONE(ST,NOL)
      DO 53 NOST=1,NBST
      NOL=1
      DO 52 NOLO=1,NBLGNO
      IF(LPC(NOLO).EQ.1) THEN
         NZONE(NOST,NOL)=NZONE(NOST,NOLO)
         NOL=NOL+1
      ENDIF
 52     CONTINUE
 53   CONTINUE

!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
! DECOUPAGE DES SECTIONS EN NBPTSTI POINTS-> STI      
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
! INITIALISATIONS
      NOPTIDEP=1
      PTLGN(0)=1
!
! POUR TOUTES LES ZONES
!       WRITE(9,*) 'PAS MOYEN SUR LES ZONES :'
!    READ*,PASM0
      DO 105 NOZN=1,NBLGN+1
!
! PRELIMINAIRES: CALCULS DES LONGEURS DES SECTIONS SUR LA ZONE:
! PRELIMINAIRES: STATISTIQUE SUR LES LONGUEURS (-> CHOIX DU PAS)
      LTOTMIN= 1.E10
      LTOTMAX=-1.E10
      LTOTMOY= 0.
      DO 107 NOST=1,NBST
!
      NPTDEP=NZONE(NOST,NOZN)
      NPTFIN=NZONE(NOST,NOZN+1)
      NBPTZ=NPTFIN-NPTDEP
!
      LCUM(1,NOST)=0.
      DO 110 NO=1,NBPTZ
        NOPT=NPTDEP-1+NO
        XB=STX(NOPT,NOST)
        YB=STY(NOPT,NOST)
        ZB=STZ(NOPT,NOST)
        X=STX(NOPT+1,NOST)
        Y=STY(NOPT+1,NOST)
        Z=STZ(NOPT+1,NOST)
        IF(LPLAN)THEN
        LSEG(NO,NOST)=SQRT((X-XB)*(X-XB)+(Y-YB)*(Y-YB))
        ELSE
        LSEG(NO,NOST)=SQRT((X-XB)*(X-XB)+(Y-YB)*(Y-YB)+(Z-ZB)*(Z-ZB))
        ENDIF
        LCUM(NO+1,NOST)=LCUM(NO,NOST)+LSEG(NO,NOST)
 110  CONTINUE
!
      LTOT(NOST)= LCUM(NBPTZ+1,NOST)
      IF (LTOT(NOST).GT.LTOTMAX) LTOTMAX=LTOT(NOST)
      IF (LTOT(NOST).LT.LTOTMIN) LTOTMIN=LTOT(NOST)
      LTOTMOY=LTOTMOY+LTOT(NOST)

!     SECTION SUIVANTE
 107  CONTINUE

! CHOIX DU PAS ...
      LTOTMOY=LTOTMOY/NBST
      If(.NOT.UNPAS)then
      WRITE(9,*) ' '
      WRITE(9,*) '      TRAITEMENT  ZONE   ',NOZN
      WRITE(9,*) 'LONGUEUR MOYENNE DE LA ZONE SUR LES SECTIONS:',LTOTMOY
      WRITE(9,*) 'LONGUEUR MIN     DE LA ZONE SUR LES SECTIONS:',LTOTMIN
      WRITE(9,*) 'LONGUEUR MAX     DE LA ZONE SUR LES SECTIONS:',LTOTMAX
!
      WRITE(9,*) 'PAS MOYEN SUR LA ZONE ?'
      WRITE(9,*) '(ENTREZ 0 pour donner le nombre de mailles)'
      WRITE(9,*) '(ENTREZ -dist pour completer'
      WRITE(9,*) ' au nombre maximal de points de la zone avec une '
      WRITE(9,*) 'distance minimale entre points ajoutes egale a dist)'
      WRITE(9,*) '(defaut = 1000000 m)'
      char10 = '0.' !READ(*,'(A10)')char10
      IF(CHAR10(1:2).NE.'  ')THEN
        READ(CHAR10,'(F10.0)')PASM(NOZN)
!        READ*,PASM(NOZN)
      ELSE
        PASM(NOZN)=999999.9
      ENDIF
      If(PASM(NOZN).LT.-0.00000000001)THEN
        COMPL(NOZN)=.TRUE.
        PASM(NOZN)=-PASM(NOZN)
      ELSE
        COMPL(NOZN)=.FALSE.  
      ENDIF  
!
! OPTIMISATION ???
      IF(.NOT.COMPL(NOZN))THEN
      WRITE(9,*) 'OPTIMISATION SIGNIFIE CONSERVER PAS TRANSVERSAL'
      WRITE(9,*) 'NE PAS OPTIMISER = CONSERVER NOMBRE DE POINTS'
      WRITE(9,*) 'ZONE A OPTIMISER (0,1):'
      WRITE(9,*)'(defaut = 0 = nombre points conserve)'
!    WRITE(9,*) '   PERSONNELLEMENT JE DECONSEILLE ....'
      cplan = '0' !READ(*,'(A)')CPLAN
      IF(CPLAN.NE.' ')THEN
        READ(CPLAN,'(I1)')MZOP(NOZN)
      ELSE
        MZOP(NOZN)=0
      ENDIF
      ELSE
        MZOP(NOZN)=0
! fin du if sur complement section        
      ENDIF  
!        MZOP(NOZN)=0
! si on ne veut qu'un pas : on ne rentre le pas qu'une fois
      elseIF(NOZN.eq.1)then
      WRITE(9,*) 'PAS TRANSVERSAL :'
      WRITE(9,*) '(ENTREZ -dist pour completer'
      WRITE(9,*) ' au nombre maximal de points de la zone avec une '
      WRITE(9,*) 'distance minimale entre points ajoutes egale a dist)'
      WRITE(9,*)'(defaut = 1000000 m)'
      char10 = '  ' !READ(*,'(A10)')char10
      IF(CHAR10(1:2).NE.'  ')THEN
      READ(CHAR10,'(F10.0)')PASM(NOZN)
!      READ*,PASM(NOZN)
      ELSE
         PASM(NOZN)=999999.9
      ENDIF
      If(PASM(NOZN).LT.-0.00000000001)THEN
        DO N2ZN=1,NBLGN+1
           compl(N2ZN)=.TRUE.
           MZOP(N2ZN)=0
           PASM(N2ZN)=-PASM(NOZN)
        ENDDO     
        COMPL(NOZN)=.TRUE.
      ELSE
        DO N2ZN=1,NBLGN+1
           compl(N2ZN)=.FALSE.
        ENDDO     
      ENDIF  
!        PASM(NOZN)=pasm0
!
! OPTIMISATION ???
      IF(.NOT.COMPL(NOZN))THEN
      WRITE(9,*) 'OPTIMISATION SIGNIFIE CONSERVER PAS TRANSVERSAL'
      WRITE(9,*) 'NE PAS OPTIMISER = CONSERVER NOMBRE DE POINTS'
      WRITE(9,*) 'TOUTES ZONES A OPTIMISER (0,1):'
      WRITE(9,*)'(defaut = 0 = nombre points conserve)'
!    WRITE(9,*) '   PERSONNELLEMENT JE DECONSEILLE ....'
      cplan = '0' !READ(*,'(A)')CPLAN
      IF(CPLAN.NE.' ')THEN
        READ(CPLAN,'(I1)')MZOP(NOZN)
      ELSE
        MZOP(NOZN)=0
      ENDIF    
      ELSE
        MZOP(NOZN)=0
! fin du if sur complement section        
      ENDIF  
      DO N2ZN=1,NBLGN+1
         pasm(N2ZN)=pasm(1)
         MZOP(N2ZN)=MZOP(1)
      ENDDO     
! fin du if sur unpas         
      endif  
         
!
      CALL CALCPTSECT(NOZN,LTOTMAX,LTOT,LCUM,LSEG,NOPTIDEP
     :,COMPL,LTOTMOY)
! ZONE SUIVANTE
 105    CONTINUE
!
! 
! ETAPE FINALE: RAJOUTER LA DERNIER LIGNE ...
      DO 190 NOST=1,NBST
      STIX(NOPTIDEP,NOST)=STX(NBPTST(NOST),NOST)
      STIY(NOPTIDEP,NOST)=STY(NBPTST(NOST),NOST)
      STIZ(NOPTIDEP,NOST)=STZ(NBPTST(NOST),NOST)
 190    CONTINUE
!
! NOMBRE DE COURBES DANS LE MAILLAGE PROVISOIRE
      NBPTSTI=NOPTIDEP
! suppression de cette ecriture car erronne en cas d'optimisation
!    WRITE(9,*) 'NOMBRE FINAL DE MAILLES ',NBPTSTI-1
      IF(NBPTSTI.GT.KP2) THEN 
      WRITE(9,*) ' NOMBRE DE POINTS  PAR SECTIONS '//
     +  'INTERMEDIAIRES TROP GRAND >',KP2
      STOP
      ENDIF
!
! FIN DE DECOUPAGE DES SECTIONS
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC 
! AFFICHAGE DES SECTIONS INTERMEDIAIRES
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

!    WRITE(9,*) 'AFFICHAGE DES SECTIONS INTERMEDIAIRE O/N?'
!    READ(5,'(A)') REP
      REP='N'
      IF ((REP.EQ.'O').OR.(REP.EQ.'o')) THEN
      DO 200 NOST=1,NBST
        WRITE(9,*) ' '
        WRITE(9,*) 'SECTION NO',NOST
        DO 220 NOPT=1,NBPTSTI
          WRITE(9,*) STIX(NOPT,NOST),STIY(NOPT,NOST),STIZ(NOPT,NOST)
 220    CONTINUE
 200  CONTINUE
      ENDIF
!
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
! CREATION DES ST INTERMEDIAIRE 
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
      If(.NOT.LINEAIRE)THEN
        WRITE(9,*) ' '
        WRITE(9,*) ' INTERPOLATION LINEAIRES DES ALTITUDES (0,1)?'
        WRITE(9,*)'(defaut = 1)'
        cplan = '1' !READ(*,'(A)')CPLAN
        IF(CPLAN.NE.' ')THEN
          read(CPLAN,'(I1)')linz
        ELSE
          linz=1
        ENDIF
! fin du if sur lineaire
      ENDIF
!
! CALCUL DU NOMBRE DE SECTIONS INTERMEDIARES...
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
! Demande de la ligne directrice servant d'axe:
      WRITE(9,*) ' CODE DES LIGNES'
      WRITE(9,'(20(1X,I3))') (I,I=1,NBLGN)
      WRITE(9,'(20(1X,A3))') (CODLGN(I),I=2,NBLGN+1)

      WRITE(9,*) ' DONNEZ LE NUMERO DE LA LIGNE DIRECTRICE SERVANT '
      WRITE(9,*) ' D AXE POUR AJOUTER DES SECTIONS '
      WRITE(9,*)  'et le calcul des PK'
      WRITE(9,*)'(defaut = 0 = bord 1)'
      WRITE(9,*)'( ',nblgn+1,' = bord 2)'
!      clign = '    ' !READ(*,'(A)')Clign
!      IF(Clign.NE.'    ')THEN
!        read(Clign,'(I4)')nldaxe
!        if(nldaxe.GT.NBLGN+1)then
!          WRITE(9,*) 
!     &             'cette ligne directrice inexistante: on prend bord 1'
!            nldaxe=0
!        endif
!      ELSE
!        nldaxe=0
!      ENDIF
!        READ*,NLDAXE
      if (nblgn > 2) then
        nldaxe = NBLGN/2+1   !+JBF 2015/06/18 : r�ponse par d�faut
      elseif (nblgn >= 1) then
        nldaxe = 1           !+JBF 2015/06/18 : r�ponse par d�faut
      else
        nldaxe = 0           !+JBF 2015/06/18 : cas o� il n'y a pas de ligne directrice
      endif
      write(9,*) 'Ligne directrice choisie : ',nldaxe,codlgn(nldaxe+1)
!
! CopiE des numeros des points de l'axe sur les sections dans NOPTLM:
      DO 401 NOST=1,NBST
! modif du 22 01 07 : on utilise plus loin stx et pas stix
        NOPTLM(NOST)=NZONE(NOST,NLDAXE+1)
 401  CONTINUE
! RECHERCHE DU LIT MINEUR:
!    WRITE(9,*) 'RECHERCHE DU PROFIL EN LONG...'
!    DO 401 NOST=1,NBST
!      ZMIN=1.E10
!      DO 402 NOPT=1,NBPTSTI
!        IF(STIZ(NOPT,NOST).LT.ZMIN) THEN
!          ZMIN=STIZ(NOPT,NOST)
!          NOPTLM(NOST)=NOPT
!        ENDIF
! 402    CONTINUE
!
! 401    CONTINUE
! calcul des sections intermediaires
      CALL CALSECTL(DPLCTOT,LPLAN,LINEAIRE,AJUST,tmoy)
!
! RECOPI DE LA DERNIERE STI 
      NBSTF=NBSTF+1
!      IF(NBSTF.GT.KS2) THEN 
!        WRITE(9,*) ' NOMBRE DE SECTIONS FINALES TROP GRAND >',KS2
!        STOP
!      ENDIF
      WRITE(9,*) 'TRAITEMENT SECTION ',NBST,' ->',NBSTF
!    DO 590 NOPTI=1, NBPTSTI
!        STFX(NOPTI,NBSTF)=STIX(NOPTI,NBST)
!        STFY(NOPTI,NBSTF)=STIY(NOPTI,NBST)
!        STFZ(NOPTI,NBSTF)=STIZ(NOPTI,NBST)
! 590    CONTINUE
!
!
      WRITE(9,*)  ' '
      WRITE(9,*) ' NOMBRE DE SECTIONS FINALES: ',NBSTF
      WRITE(9,*)  ' '
!
! OPTIMISATION DU MAILLAGE: suppression points en trop dans chaque section 
!CCCCCCCCCCCCCCCCCCCCCCCCC
      CALL OPTIMISATION(LPLAN)
!
!
! ajustement des sections sur deux lignes de bord ou lignes directrices
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC 
      IF(NBSTF.GT.NBST)then
         WRITE(9,*)'correction sections interpolees par bords (0/1/2)'
         WRITE(9,*)'defaut =0 : pas de correction'
         WRITE(9,*)'1 signifie limites sections interpolees replacees'
         WRITE(9,*)' sur les lignes directrices/bords (fichiers lus)'
         WRITE(9,*)'2 signifie limites toutes sections  replacees'
         WRITE(9,*)' sur les bords (fichiers lus)'
         CHARAJUST = '0' !read(*,'(A1)')CHARAJUST
         if(charajust.eq.'1')then    
            ajust=.true.
         elseif(charajust.eq.'2')then    
            ajust=.true.
         else
            ajust=.false.
         endif            
      ELSE
! si pas de sections intermediaires      
        ajust=.false.
      ENDIF
      if(ajust)then
        call ajust_bords(CHARajust)
!        close(28)
      endif

! applicationde la distance minimale entre sections
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      !CALL applicdismin
!
! CALCUL DU PK DES SECTIONS DU MAILLAGE
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      CALL CALC_PK(NLDAXE,DPLCTOT)

!     ECRITURE DU FICHIER DES SECTIONS EN TRAVERS FINAL
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      CALL ECR_FIC(10,VERSION,FICIN,FICOUT,LPLAN,LINEAIRE)
      CLOSE(10)
      END
!
! FIN DU PROGRAMME PRINCIPAL ....
!
!
!##############################################################################
      SUBROUTINE APPLICDISMIN
!##############################################################################
! application d'une distance minimale entre sections
! application par zone au 1/12/2014
!
      implicit none
      INTEGER NBPTSTI,NBST,NBSTF,NOLGN
     :,nblgn,nbptzn,ptlgn,KZO
! NB ZONE MAX
      PARAMETER (KZO=1000)
      DOUBLE PRECISION  DISMIN,pasm
      character*1 rep,applicapres,applicextremites
      character*10 char10
      logical lapplicapres,lapplicextremites
!
      COMMON/BDDIM/NBST,NBPTSTI,NBSTF
! INDICE DES LIGNES DIRECTRICES DANS LE MAILLAGE
! + NB POINT /ZONE, PAS MOYEN /ST/ZONE, 
      COMMON/BDLGN/PTLGN(0:KZO+1),PASM(KZO)
      COMMON/NBDLGN/NBLGN,NBPTZN(KZO)
!
!       
      WRITE(9,*) 'ENTREZ DISTANCE MINIMALE ENTRE 2 POINTS SUR UNE LIGNE'
      WRITE(9,*) 'CETTE DISTANCE EST TOUJOURS UNE DISTANCE 2D'
      WRITE(9,*)'(defaut = 0.00001 m)'
      READ(*,'(A10)')char10
      READ(CHAR10,'(F10.0)')DISMIN
      rep='0'
      IF(DISMIN.le.0.0001)then
!	      DISMIN=0.00001
	      return
! cas ou pas de sections interpolees	      
      elseIF(NBSTF.GT.NBST)then
           rep='1'
      else
        WRITE(9,*)'distance appliquee a toutes sections ---> 1'
        WRITE(9,*)'distance appliquee a sections interpolees ---> 0'
	     WRITE(9,*)'(defaut = 0)'
	     read(*,'(A1)')rep
      endif
      if(rep.eq.'1')then
        lapplicapres=.FALSE.
        WRITE(9,*)'points sur bords et directrices changes ---> 1'
        WRITE(9,*)'points sur bords et directrices inchanges ---> 0'
	     WRITE(9,*)'(defaut = 0)'
	     read(*,'(A1)')applicextremites
	     if(applicextremites.eq.'1')then
			 lapplicextremites=.TRUE.
	     else
			 lapplicextremites=.FALSE.
	     endif
      else
        WRITE(9,*)'distance appliquee avant et apres ---> 1'
        WRITE(9,*)'distance appliquee apres seulement ---> 0'
	     WRITE(9,*)'(defaut = 0)'
	     read(*,'(A1)')applicapres
	     if(applicapres.eq.'1')then
			 lapplicapres=.TRUE.
	     else
			 lapplicapres=.FALSE.
   	  endif
	     lapplicextremites=.TRUE.
! fin du if sur sectins interpolees ou toutes
      endif
       
       DO NOLGN=0,NBLGN
         CALL applicdisminz(PTLGN(NOLGN),PTLGN(NOLGN+1)
     :,dismin,rep,lapplicapres,lapplicextremites)     
       enddo
       
       return
       end
!##############################################################################
      SUBROUTINE APPLICDISMINz(ndeb,nfin,dismin,rep,lapplicapres
     :,lapplicextremites)
!##############################################################################
! CALCUL DES ST FINALES ENTRE NOST ET NOST+1
! CAS ENTRE DEUX ST MESUREES NON PLANES => STF NON PLANE
!(IL Y EN A NBSTI(NOST) ENTRE CES DEUX ST)
!
      implicit none
      INTEGER NOST,NBPTSTI,NBST,NBSTF,ndeb,nfin
     :,NT,NOSTI,NBT,NOCB,NBSTI,nsti,nsto,nbpmt,nbpms
     :,KS2,KP2,KP3,ip,IDEBUT,IFIN
      DOUBLE PRECISION X,Y,Z
      PARAMETER (KS2=1500)
      PARAMETER (KP2=10500,KP3=1000)
      DOUBLE PRECISION  STFX(KP2,KS2),STFY(KP2,KS2),
     + STFZ(KP2,KS2),DIS,DISMIN,coefm,coefp,disp
      character*1 rep,applicapres
      double precision stfox(kp3,ks2),stfoy(kp3,ks2),stfoz(kp3,ks2)
      logical encore,lapplicapres,lapplicextremites,lmodif(kp2)
      COMMON/BDSTFO/ STFOX,STFOY,
     + STFOZ
!
      COMMON/BDDIM/NBST,NBPTSTI,NBSTF
!
! BD ST  FINALE  NBPTSTI*NBSTF          
! NBSTI nb des section inter entre section de depart
      COMMON/BDSTF/ NBSTI(KS2),STFX,STFY,
     + STFZ
!       
! ondistingue sections de base inchangees et sections interpolees
! ou pas
! si repositionnement g�n�ral on part de un couple de valeurs 
! nsti, nsto impossible
       if(rep.eq.'1')then
          nsti=-20000
		  nsto=1
           if(lapplicextremites)then
		  idebut=ndeb
		  ifin=nfin
	   else
! on garde tout de meme les points extremes		  
		  idebut=ndeb+1
		  ifin=nfin-1
           endif
       else
          nsti=0
		  nsto=1
		  idebut=ndeb
		  ifin=nfin
! fin du if sur sectins interpolees ou toutes
       endif
       do nost=2,nbstf-1
	     IF(NSTI.EQ.NBSTI(NSTO))THEN
! si fin de zone entre sections donc section de base		 
             NSTO=NSTO+1
             NSTI=0
         ELSE
! si section entre sections de base		 
	   NSTI=NSTI+1
	   coefm=1.
	   coefp=1.
           do nocb=idebut,ifin
	     lmodif(nocb)=.FALSE.
	   enddo
           do nocb=idebut,ifin
	     if(.NOT.lmodif(nocb))then
	         x=stfox(nocb,nost)
	         y=stfoy(nocb,nost)
	         z=stfoz(nocb,nost)
! on compare a tous les points de la section nost-1		 
!        DIS=(X-STFoX(nocb,Nost-1))**2+
!     :(Y-STFoY(nocb,Nost-1))**2+(Z-STFoZ(nocb,Nost-1))**2
        DIS=(X-STFoX(nocb,Nost-1))**2+
     :(Y-STFoY(nocb,Nost-1))**2
        nbpmt=nocb
! on modifie pour chercher que de nocb-1 a nocb+1 ,
! points pouvant etre mis en relation	   
      do nbpms=max(nocb-1,ndeb),min(nocb+1,nfin)
!        if(SQRT((X-STFoX(nbpms,Nost-1))**2+
!     :(Y-STFoY(nbpms,Nost-1))**2+(Z-STFoZ(nbpms,Nost-1))**2).LT.DIS)
         if((X-STFoX(nbpms,Nost-1))**2+
     :(Y-STFoY(nbpms,Nost-1))**2.LT.DIS)
     :then
!          dis=SQRT((X-STFoX(nbpms,Nost-1))**2+
!     :(Y-STFoY(nbpms,Nost-1))**2+(Z-STFoZ(nbpms,Nost-1))**2)
          dis=(X-STFoX(nbpms,Nost-1))**2+
     :(Y-STFoY(nbpms,Nost-1))**2
           nbpmt=nbpms
	 endif  
      enddo
      IF(SQRT(DIS).LT.coefm*DISMIN)THEN
!	     coefm=1.02
!		 coefp=0.98
	     coefm=1.0
		 coefp=1.0
        X=STFoX(nbpmt,Nost-1)
        Y=STFoY(nbpmt,Nost-1)
        Z=STFoZ(nbpmt,Nost-1)
! points identiques mis aux memes valeurs		
		encore=.TRUE.
		do ip=nocb,ifin
		  if(encore)then
		       DISp=(STFoX(NOCB,Nost)-STFoX(ip,Nost))**2+
     :(STFoY(NOCB,Nost)-STFoY(ip,Nost))**2
	           if(disp.gt.0.0000000001)THEN
                     encore=.FALSE.
                   else
      STFoX(ip,Nost)= X
      STFoY(ip,Nost)= Y
      STFoZ(ip,Nost)= Z
      lmodif(ip)=.TRUE.
                   endif
! fin du if sur encore
                  endif
! fin du do sur ip			   
		enddo
! points identiques mis aux memes valeurs
! a priori inutile
!		encore=.TRUE.
!		do ip=nocb-1,idebut,-1
!		  if(encore)then
!		       DISp=SQRT((STFoX(NOCB,Nost)-STFoX(ip,Nost))**2+
!     :(STFoY(NOCB,Nost)-STFoY(ip,Nost))**2+
!     :(STFoZ(NOCB,Nost)-STFoz(ip,Nost))**2)
!	           if(disp.gt.0.00001)THEN
!				   encore=.FALSE.
!			   else
!      STFoX(ip,Nost)= X
!      STFoY(ip,Nost)= Y
!      STFoZ(ip,Nost)= Z
!               endif
! fin du if sur encore
!          endif
! fin du do sur ip			   
!		enddo
! comparaison avec nost+1	nocb	
      ELSE
		if(lapplicapres)then  
! on compare a tous les points de la section nost+1		 
        DIS=(X-STFoX(nocb,Nost+1))**2+
     :(Y-STFoY(nocb,Nost+1))**2
        nbpmt=nocb
! on modifie pour chercher que de nocb-1 a nocb+1 ,
! points pouvant etre mis en relation	   
      do nbpms=max(nocb-1,ndeb),min(nocb+1,nfin)
         if((X-STFoX(nbpms,Nost+1))**2+
     :(Y-STFoY(nbpms,Nost+1))**2.LT.DIS)
     :then
          dis=(X-STFoX(nbpms,Nost+1))**2+
     :(Y-STFoY(nbpms,Nost+1))**2
           nbpmt=nbpms
	 endif  
      enddo
      IF(SQRT(DIS).LT.coefm*DISMIN)THEN
!	     coefp=1.02
!		 coefm=0.98
	     coefp=1.0
		 coefm=1.0
        X=STFoX(nbpmt,Nost+1)
        Y=STFoY(nbpmt,Nost+1)
        Z=STFoZ(nbpmt,Nost+1)
        DIS=SQRT((STFoX(NOCB,Nost-1)-X)**2+
     :(STFoY(NOCB,Nost-1)-Y)**2)
! si on a plus que 2*dismin, on remplace nost par 1/2 nost-1,nost+1
           IF(DIS.GT.2.*DISMIN)THEN
             X=0.5*(STFoX(NOCB,Nost-1)+X)   
             Y=0.5*(STFoy(NOCB,Nost-1)+Y)   
             Z=0.5*(STFoz(NOCB,Nost-1)+Z) 
           ENDIF
! points identiques mis aux memes valeurs		
		encore=.TRUE.
		do ip=nocb,ifin
		  if(encore)then
		       DISp=(STFoX(NOCB,Nost)-STFoX(ip,Nost))**2+
     :(STFoY(NOCB,Nost)-STFoY(ip,Nost))**2
	           if(disp.gt.0.0000000001)THEN
				   encore=.FALSE.
			   else
      STFoX(ip,Nost)= X
      STFoY(ip,Nost)= Y
      STFoZ(ip,Nost)= Z
       lmodif(ip)=.TRUE.
               endif
! fin du if sur encore
          endif
! fin du do sur ip			   
		enddo
! points identiques mis aux memes valeurs		
! a priori inutile
!		encore=.TRUE.
!		do ip=nocb-1,idebut,-1
!		  if(encore)then
!		       DISp=SQRT((STFoX(NOCB,Nost)-STFoX(ip,Nost))**2+
!     :(STFoY(NOCB,Nost)-STFoY(ip,Nost))**2+
!     :(STFoZ(NOCB,Nost)-STFoz(ip,Nost))**2)
!	           if(disp.gt.0.00001)THEN
!				   encore=.FALSE.
!			   else
!      STFoX(ip,Nost)= X
!      STFoY(ip,Nost)= Y
!      STFoZ(ip,Nost)= Z
!               endif
! fin du if sur encore
!          endif
! fin du do sur ip			   
!		enddo
! fin du if sur DEUXIeme dis<dismin nost+1 TOUS POINTS
        ENDIF        
! fin du if sur lapplicapres (comparaison avec nost+1)
        endif
! fin du if sur premier dis<dismin nost-1 tous points
      ENDIF
! a priori inutile car fait avec points identiques
!      STFoX(NOCB,Nost)= X
!      STFoY(NOCB,Nost)= Y
!      STFoZ(NOCB,Nost)= Z
! fin du if sur lmodif
                  endif
! FIN BOUCLE SUR point
      ENDDO
! fin if sur section
      ENDIF
! FIN BOUCLE SUR SECTION
      ENDDO
      END
!
!##############################################################################
      SUBROUTINE ESTIM_TCB(NOST)
!##############################################################################
! ESTIMATION DE LA TENTION DU BAIS ET DE CONTINUITE SUR UNE TRANCHE
!
      implicit none
      INTEGER NOST,NBPTSTI,NBST,NBSTF,NOCO
     :,KS1,KP1,KZO,KS2,KP2,KP3 
      DOUBLE PRECISION 
     :Y1,Z1,X1,X2,Y2,Z2,X0,Y0,Z0,D01,D02,D_11,C1,c0,B0,T0 
     :,T1,b1,Z_1,y_1,x_1
      PARAMETER (KS1=1500)
      PARAMETER (KP1=1000)
      PARAMETER (KZO=1000)
      PARAMETER (KS2=1500)
      PARAMETER (KP2=10500)
      PARAMETER (KP3=1000)
      DOUBLE PRECISION  STIX(KP2,KS1),STIY(KP2,KS1),STIZ(KP2,KS1)
!
      COMMON/BDDIM/NBST,NBPTSTI,NBSTF
!   
!   NBPTSTI*NBST  
       COMMON/STI/STIX,STIY,STIZ
!
! BD DES TENSION CONTINUITE BIAIS A DROITE (0) ET GAUCHE (1) DE CHAQUE PT
      COMMON/TCB/ T0(KP2),T1(KP2),C0(KP2),C1(KP2),B0(KP2),B1(KP2)
!
! BD ST OU LES TRAJ SONT PERPENDICULAIRE AU ST (PLAN X,Y)
!      COMMON/BDSTPER/NBPER,NSTPER(KS1)
!
! POUR TOUTES LES COURBES
      DO 100 NOCO=1,NBPTSTI
! INIT:
      IF(NOST.NE.1) THEN
        X_1=STIX(NOCO,NOST-1)
        Y_1=STIY(NOCO,NOST-1)
        Z_1=STIZ(NOCO,NOST-1)
      ELSE
        X_1=123.456
        Y_1=123.456
        Z_1=123.456
      ENDIF
!
        X0=STIX(NOCO,NOST)
        Y0=STIY(NOCO,NOST)
        Z0=STIZ(NOCO,NOST)
!
        X1=STIX(NOCO,NOST+1)
        Y1=STIY(NOCO,NOST+1)
        Z1=STIZ(NOCO,NOST+1)
!
      IF (NOST.NE.NBST-1) THEN
        X2=STIX(NOCO,NOST+2)
        Y2=STIY(NOCO,NOST+2)
        Z2=STIZ(NOCO,NOST+2)
      ELSE
        X2=123.456
        Y2=123.456
        Z2=123.456
      ENDIF
! IL NE FAUT PAS QUE Z INTERVIENNE DANS LA TENTION 
! POURQUOI ??????
      Z_1=0.
      Z0=0.
      Z1=0.
      Z2=0.
!
      D01=SQRT((X1-X0)**2+(Y1-Y0)**2+(Z1-Z0)**2)
      D02=SQRT((X2-X0)**2+(Y2-Y0)**2+(Z2-Z0)**2)
      D_11=SQRT((X1-X_1)**2+(Y1-Y_1)**2+(Z1-Z_1)**2)
!
! TENTION:
      IF(NOST.EQ.1) THEN
        T0(NOCO)=1.
      ELSEIF(D_11.LT.0.00001)THEN
        T0(NOCO)=1.
      ELSE
!        T0(NOCO)=1.-2.*(D01/D_11)
        T0(NOCO)=1.
      ENDIF
!
      IF(NOST.NE.NBST-1) THEN
        T1(NOCO)=1.
      ELSEIF(D02.LT.0.00001)THEN
        T1(NOCO)=1.
      ELSE
        T1(NOCO)=1.
!        T1(NOCO)=1.-2.*(D01/D02)
      ENDIF
!     WRITE(9,*) NOST,NOCO,T0(NOCO),T1(NOCO)
! CONTINUITE
      C0(NOCO)=0.
      C1(NOCO)=0.
! BAIS:
      B0(NOCO)=0.
      B1(NOCO)=0.
!
 100    CONTINUE
!
 999    END
!##############################################################################
      SUBROUTINE CALC_DERIV(NOST,LINZ)
!##############################################################################
! CALCULS DES VECTEURS DERIVEES SUR UNE TRANCHE
!
      implicit none
      INTEGER NOST,NBPTSTI,NBST,NBSTF,
     :LINZ,NOCO
     :,KS1,KP1,KZO,KS2,KP2,KP3 
      DOUBLE PRECISION B0,C0,DX0,DY0,DZ0,T0,Z2,Z1,Z0
     :,B1,C1,DX1,DY1,DZ1,T1,X_1
     :,X0,Y0,X1,Y1,y_1,Z_1,X2,Y2
     :,PA0,PB0,PA1,PB1
      PARAMETER (KS1=1500)
      PARAMETER (KP1=1000)
      PARAMETER (KZO=1000)
      PARAMETER (KS2=1500)
      PARAMETER (KP2=10500)
      PARAMETER (KP3=1000)
      DOUBLE PRECISION  STIX(KP2,KS1),STIY(KP2,KS1),STIZ(KP2,KS1)
    
!
      COMMON/BDDIM/NBST,NBPTSTI,NBSTF
!   
!   NBPTSTI*NBST  
       COMMON/STI/STIX,STIY,STIZ
!
! BD DES TENSION CONTINUITE BIAIS A DROITE (0) ET GAUCHE (1) DE CHAQUE PT
      COMMON/TCB/ T0(KP2),T1(KP2),C0(KP2),C1(KP2),B0(KP2),B1(KP2)
!
! BD DES TGTES AUX PTS DES STI A DROITE (0) ET GAUCHE (1) 
      COMMON/DERIV/DX0(KP2),DY0(KP2),DZ0(KP2),DX1(KP2),DY1(KP2),DZ1(KP2)
!
! POUR TOUTES LES COURBES
      DO 100 NOCO=1,NBPTSTI
! INIT:
      IF(NOST.NE.1) THEN
        X_1=STIX(NOCO,NOST-1)
        Y_1=STIY(NOCO,NOST-1)
        Z_1=STIZ(NOCO,NOST-1)
      ELSE
        X_1=123.456
        Y_1=123.456
        Z_1=123.456
      ENDIF
!
        X0=STIX(NOCO,NOST)
        Y0=STIY(NOCO,NOST)
        Z0=STIZ(NOCO,NOST)
!
        X1=STIX(NOCO,NOST+1)
        Y1=STIY(NOCO,NOST+1)
        Z1=STIZ(NOCO,NOST+1)
!
      IF (NOST.NE.NBST-1) THEN
        X2=STIX(NOCO,NOST+2)
        Y2=STIY(NOCO,NOST+2)
        Z2=STIZ(NOCO,NOST+2)
      ELSE
        X2=123.456
        Y2=123.456
        Z2=123.456
      ENDIF
!
! CALCUL DES PARAMETRES
      PA0=(1.-T0(NOCO))*(1.+C0(NOCO))*(1.+B0(NOCO))/2.
      PB0=(1.-T0(NOCO))*(1.-C0(NOCO))*(1.-B0(NOCO))/2.
!
      PA1=(1.-T1(NOCO))*(1.-C1(NOCO))*(1.+B1(NOCO))/2.
      PB1=(1.-T1(NOCO))*(1.+C1(NOCO))*(1.-B1(NOCO))/2.
!
! VERIF PA0=PB0 ET IDEM 1 (C ET B NULS)
!         IF(PA0.NE.PB0) 
!     +      WRITE(9,*) ' =====> BIZARRE, PA0 .NE. PB0',NOCO,NOST
!         IF(PA1.NE.PB1) 
!     +      WRITE(9,*) ' =====> BIZARRE, PA0 .NE. PB0',NOCO,NOST
!
! CALCULS DES DERIVEES
      DX0(NOCO)=PA0*(X0-X_1) + PB0*(X1-X0)
      DY0(NOCO)=PA0*(Y0-Y_1) + PB0*(Y1-Y0)
      DZ0(NOCO)=PA0*(Z0-Z_1) + PB0*(Z1-Z0)
!
      DX1(NOCO)=PA1*(X1-X0) + PB1*(X2-X1)
      DY1(NOCO)=PA1*(Y1-Y0) + PB1*(Y2-Y1)
      DZ1(NOCO)=PA1*(Z1-Z0) + PB1*(Z2-Z1)
!
! VERIF SI D0 ET D1 MEME MODULE
!         D0=SQRT(DX0(NOCO)**2+DY0(NOCO)**2+DZ0(NOCO)**2)
!         D1=SQRT(DX1(NOCO)**2+DY1(NOCO)**2+DZ1(NOCO)**2)
!         IF(D0.NE.D1) 
!     +      WRITE(9,*) '==> BIZARRE D0.NE.D1',NOCO,NOST,D0,D1
!
! POUR LE CAS OU L ON INTERPOLE Z LINEAIRE, ON PEUR DIRE QUE LA CONTINUITE SUR
! Z EST EGALE A -1, CAD PA0 SUZ Z = 0 ET PB1 SUR Z = 0. DONC:
      IF(LINZ.EQ.1.) THEN
         DZ0(NOCO)=PB0*(Z1-Z0)
         DZ1(NOCO)=PA1*(Z1-Z0)
      ENDIF
!
! VERIF   -> OK
!         IF(DX0(NOCO).EQ.0..AND.DY0(NOCO).EQ.0.) 
!     +      WRITE(9,*) '======> BIZARRE, VECT DERIV 0 NUL',NOCO,NOST
!         IF(DX1(NOCO).EQ.0..AND.DY1(NOCO).EQ.0.) 
!     +      WRITE(9,*) '======> BIZARRE, VECT DERIV 1 NUL',NOCO,NOST
 

! REMARQUES: AUX EXTREMITE, LES PARAMETRES T=1 C=0  B=0 
! DONNE P 0 OU P 1 NUL LA DERIVE = VECTEUR NUL:
!         IF(NOST.EQ.1) THEN
!            DX0(NOCO)=0.
!            DY0(NOCO)=0.
!            DZ0(NOCO)=0.
!         ENDIF
!
!         IF(NOST.EQ.NBST-1) THEN
!            DX1(NOCO)=0.
!            DY1(NOCO)=0.
!            DZ1(NOCO)=0.
!         ENDIF
!
 100    CONTINUE

!
 999    END
!
!##############################################################################
      SUBROUTINE PT_TRAJ(NST,NCB,T,X,Y,Z)
!##############################################################################
! SUR UNE TRANCHE NST DONNEE, SUR UNE TRAJECTOIRE NCB DONNEE, A UN T DONNE
! CALCULE X,Y,Z EN UTILISANT LA BASE DES TANGEANTES 
!
      implicit none
      INTEGER NBPTSTI,NBST,NBSTF
     :,NCB,NST
     :,KS1,KP1,KZO,KS2,KP2,KP3 
      DOUBLE PRECISION Z,X,Y
     :,DX0,DY0,DZ0,DX1,DY1,DZ1,T,H2,H3,H4,H1,T2,T3
      PARAMETER (KS1=1500)
      PARAMETER (KP1=1000)
      PARAMETER (KZO=1000)
      PARAMETER (KS2=1500)
      PARAMETER (KP2=10500)
      PARAMETER (KP3=1000)
      DOUBLE PRECISION STIX(KP2,KS1),STIY(KP2,KS1),STIZ(KP2,KS1)
!
      COMMON/BDDIM/NBST,NBPTSTI,NBSTF
!   
!   NBPTSTI*NBST  
       COMMON/STI/STIX,STIY,STIZ
!
! BD DES TGTES AUX PTS DES STI A DROITE (0) ET GAUCHE (1) 
      COMMON/DERIV/DX0(KP2),DY0(KP2),DZ0(KP2),DX1(KP2),DY1(KP2),DZ1(KP2)
!
! PUISSANCE DE T
      T2=T*T
      T3=T2*T
! VALEURS DES POLYNOMES DE HERMITE
      H1=2.*T3-3.*T2+1.
      H2=-2.*T3+3.*T2
      H3=T3-2.*T2+T
      H4=T3-T2
!
! CALCUL DU POINT DE LA TRAJECTOIRE
      X=STIX(NCB,NST)*H1+STIX(NCB,NST+1)*H2+DX0(NCB)*H3+DX1(NCB)*H4
      Y=STIY(NCB,NST)*H1+STIY(NCB,NST+1)*H2+DY0(NCB)*H3+DY1(NCB)*H4
      Z=STIZ(NCB,NST)*H1+STIZ(NCB,NST+1)*H2+DZ0(NCB)*H3+DZ1(NCB)*H4
!
 999  END
!##############################################################################
      SUBROUTINE ST_PLAN
!##############################################################################
! TESTE TOUTES LES ST MESUREE ET VERIFIE SI ELLES SONT PLANES....
      implicit none
      INTEGER NOST,NOPT,NBPTSTI,NBST,NBSTF
     :,KSTPLAN,NBPTST
     :,KS1,KP1,KZO,KS2,KP2,KP3 
      DOUBLE PRECISION X,Y,XB,YB,DELTA,XA,YA,D,DY,DX
      PARAMETER (KS1=1500)
      PARAMETER (KP1=1000)
      PARAMETER (KZO=1000)
      PARAMETER (KS2=1500)
      PARAMETER (KP2=10500)
      PARAMETER (KP3=1000)
      DOUBLE PRECISION  STX(KP1,KS1),STY(KP1,KS1),STZ(KP1,KS1)

!
      COMMON/BDDIM/NBST,NBPTSTI,NBSTF
! BD ST  (ORIGINE FICHIER de depart)
!  NBPTST()*NBST -> COTE X Y Z DU POINT 
      COMMON/BDST/NBPTST(KS1),STX,STY,STZ
! BD FLAG POUR ST MESUREES PLANES:
      COMMON /BDSTPLAN/ KSTPLAN(KS1)
!
      DELTA=.5
      DO 100 NOST=1,NBST
!        WRITE(9,*) ' SECTION',NOST
      NOPT=1
      XA=STX(NOPT,NOST)
      YA=STY(NOPT,NOST)
      NOPT=NBPTST(NOST)
      XB=STX(NOPT,NOST)
      YB=STY(NOPT,NOST)
      DX=XB-XA
      DY=YB-YA
!
      KSTPLAN(NOST)=1
      DO 110 NOPT=2,NBPTST(NOST)-1
      X=STX(NOPT,NOST)
      Y=STY(NOPT,NOST)
      D=(X-XA)*DY-(Y-YA)*DX
!          WRITE(9,*) NOPT,D
      IF(D.GT.DELTA) KSTPLAN(NOST)=0
! DANGER : PROVISOIRE:
        KSTPLAN(NOST)=0
 110    CONTINUE
!     IF(KSTPLAN(NOST).EQ.0) WRITE(9,*) '#### SECTION ',NOST,' PAS PLANE.'
 100  CONTINUE

      END
!##############################################################################
      SUBROUTINE LECT_FIC(NCA)
! NCA= NO CHANNEL
!##############################################################################
!
      implicit none
      INTEGER NBPTSTI,NBST,NBSTF
     :,N1,N2,N3
     :,KS1,KP1,KZO,KS2,KP2,KP3 
     :,NBLGNO,NBHISTO,NCA,NBPTST,M,I,N4,NOLGN
     :,NBPTSEC,LPC,NZONE
     :,J,M0
      DOUBLE PRECISION XMIN,YMIN,ZMIN,XMAX,YMAX,ZMAX,PK,PKF,X1,Y1,Z1
      PARAMETER (KS1=1500)
      PARAMETER (KP1=1000)
      PARAMETER (KZO=1000)
      PARAMETER (KS2=1500)
      PARAMETER (KP2=10500)
      PARAMETER (KP3=1000)

      DOUBLE PRECISION STX(KP1,KS1),STY(KP1,KS1),STZ(KP1,KS1)
! ENTETE DE FICHIER: IE HISTORIQUE DU TRAITEMENT:
      CHARACTER HISTO*80,REP*1
      LOGICAL TTDIR,TTNOM,TTCONF
      COMMON/BDHISTO/HISTO(20)/NBHISTO/NBHISTO

! BD DIM DES STRUCTURES:
      COMMON/BDDIM/NBST,NBPTSTI,NBSTF
! BD ST  (ORIGINE FICHIER)
!  NBPTST()*NBST -> COTE X Y Z DU POINT 
      COMMON/BDST/NBPTST(KS1),STX,STY,STZ
      COMMON/BDENTETE/PK(KS1),PKF(KS2)
! BD NOM DES SECTIONS ORIGINE
       CHARACTER*12 NOMST
       COMMON /BDNOMST/NOMST(KS1)
!
      CHARACTER CODLGN*3
      COMMON /BDZO/NZONE(KS1,KZO),NBLGNO,LPC(KZO),CODLGN(KZO)
!
      CHARACTER CODL*3,COD(1000)*3
      CHARACTER*80 LIGNE
!
      XMAX=-1.E10
      YMAX=-1.E10
      ZMAX=-1.E10
      XMIN= 1.E10
      YMIN= 1.E10
      ZMIN= 1.E10       
      NBST=1
      NBPTSEC=1
      NOLGN=2
      DO I=1,100
        COD(I)(1:1)='A'
        IF(I-1.LT.10)THEN
          COD(I)(2:2)='0'
          write(COD(I)(3:3),'(I1)')I-1
        ELSE  
          write(COD(I)(2:3),'(I2)')I-1
        ENDIF         
      ENDDO  
      DO I=101,200
        COD(I)(1:1)='B'
        IF(I-101.LT.10)THEN
          COD(I)(2:2)='0'
          write(COD(I)(3:3),'(I1)')I-101
        ELSE  
          write(COD(I)(2:3),'(I2)')I-101
        ENDIF         
      ENDDO  
      DO I=201,300
        COD(I)(1:1)='C'
        IF(I-201.LT.10)THEN
          COD(I)(2:2)='0'
          write(COD(I)(3:3),'(I1)')I-201
        ELSE  
          write(COD(I)(2:3),'(I2)')I-201
        ENDIF         
      ENDDO  
      DO I=301,400
        COD(I)(1:1)='D'
        IF(I-301.LT.10)THEN
          COD(I)(2:2)='0'
          write(COD(I)(3:3),'(I1)')I-301
        ELSE  
          write(COD(I)(2:3),'(I2)')I-301
        ENDIF         
      ENDDO  
      DO I=401,500
        COD(I)(1:1)='E'
        IF(I-401.LT.10)THEN
          COD(I)(2:2)='0'
          write(COD(I)(3:3),'(I1)')I-401
        ELSE  
          write(COD(I)(2:3),'(I2)')I-401
        ENDIF         
      ENDDO  
      DO I=501,600
        COD(I)(1:1)='F'
        IF(I-501.LT.10)THEN
          COD(I)(2:2)='0'
          write(COD(I)(3:3),'(I1)')I-501
        ELSE  
          write(COD(I)(2:3),'(I2)')I-501
        ENDIF         
      ENDDO  
      DO I=601,700
        COD(I)(1:1)='G'
        IF(I-601.LT.10)THEN
          COD(I)(2:2)='0'
          write(COD(I)(3:3),'(I1)')I-601
        ELSE  
          write(COD(I)(2:3),'(I2)')I-601
        ENDIF         
      ENDDO  
      DO I=701,800
        COD(I)(1:1)='H'
        IF(I-701.LT.10)THEN
          COD(I)(2:2)='0'
          write(COD(I)(3:3),'(I1)')I-701
        ELSE  
          write(COD(I)(2:3),'(I2)')I-701
        ENDIF         
      ENDDO  
      DO I=801,900
        COD(I)(1:1)='I'
        IF(I-801.LT.10)THEN
          COD(I)(2:2)='0'
          write(COD(I)(3:3),'(I1)')I-801
        ELSE  
          write(COD(I)(2:3),'(I2)')I-801
        ENDIF         
      ENDDO  
      DO I=901,1000
        COD(I)(1:1)='J'
        IF(I-901.LT.10)THEN
          COD(I)(2:2)='0'
          write(COD(I)(3:3),'(I1)')I-901
        ELSE  
          write(COD(I)(2:3),'(I2)')I-901
        ENDIF         
      ENDDO  
!
! FORMAT DE L HISTORIQUE
 99   FORMAT (A80)
! FORMAT DES ENTETES DES SECTIONS:
 100  format(4I6,F13.4,1X,A12)
! FORMAT DES POINTS DES SECTIONS+LIGNE DE CODAGE
 101  FORMAT (3F13.4,1X,A3)
      WRITE(9,*)'si le fichier a des sections ayant'
      WRITE(9,*)'toutes le meme nombre de points'
      WRITE(9,*)'(fichier de sections de maillage *.m)'
      WRITE(9,*)'tous les points pourront etre'
      WRITE(9,*)'consideres comme directrices'
      WRITE(9,*)'si vous entrez 1'
      WRITE(9,*)'ces directrices seront nommees'
      WRITE(9,*)'A00 A01 etc'
      WRITE(9,*)'si vous entrez 2'
      WRITE(9,*)'sinon, si vous voulez supprimer'
      WRITE(9,*)'des points confondus dans un section'
      WRITE(9,*)'entrez 3'
      WRITE(9,*)'sinon entrez 0'
      WRITE(9,*)'(defaut = 0)'
      
      rep = '0' !read(*,'(a)')rep
      If(rep.eq.'2')then
       TTDIR=.TRUE.
       TTNOM=.TRUE.
       TTCONF=.FALSE.
      elseif(rep.eq.'1')then
       TTDIR=.TRUE.
       TTNOM=.FALSE.
       TTCONF=.FALSE.
      elseif(rep.eq.'3')then
       TTDIR=.FALSE.
       TTNOM=.FALSE.
       TTCONF=.TRUE.
      else
       TTDIR=.FALSE.
       TTNOM=.FALSE.
       TTCONF=.FALSE.
      ENDIF   
      
 
!
! LECTURE DE L HISTORIQUE DU FICHIER:
      NBHISTO=0
      WRITE(9,*) ' '
      WRITE(9,*) ' HISTORIQUE DU FICHIER :'
 120  READ (NCA,99,END=15) LIGNE
      IF (LIGNE(1:1).EQ.'#') THEN
      NBHISTO=NBHISTO+1
      HISTO(NBHISTO)=LIGNE
      WRITE(9,*) HISTO(NBHISTO)
      GOTO 120
      ENDIF
!
      read(LIGNE,100) N1,N2,N3,N4,pk(nbst),NOMST(NBST) 
!      IF(N3.NE.0) WRITE(9,*) '### FICHIER DE SECTIONS INCORRECT'
      N3=0
      J=1
      M0=0
      DO 10 M=1,5000000
      READ(NCA,101,END=15) X1,Y1,Z1,CODL
      
! cas FIN DE SEGMENT
      IF (ABS(X1-999.999).LT.0.0001) THEN
!         CREE DEUX LIGNES BIDONS: LIGNE 1 SUR 1ER POINT SECTION
!                                  DERNIERE LIGNE SUR DERNIER POINT DE LA SEC.
      NZONE(NBST,1)=1
      NZONE(NBST,NOLGN)=NBPTSEC-1
      NOLGN=NOLGN+1
      IF(NBLGNO.GT.KZO+1) THEN 
      WRITE(9,*) ' NOMBRE DE LIGNES TROP GRAND >',KZO
      STOP
      ENDIF
      IF(M-M0.NE.N4+1)THEN
        WRITE(9,*)'attention section ',nbst
        WRITE(9,*)'nombre de points lu ',M-M0-1,' different de '
        WRITE(9,*)'nombre de points ',N4,' dans en tete'
      ENDIF 
!
! TEST DE LA CONSTANCE DU NOMBRE DE LIGNE (ZONES):
        IF (NBST.EQ.1) THEN
          NBLGNO=NOLGN-1
        ELSE
      IF(.NOT.TTDIR)THEN
      IF (NBLGNO.NE.NOLGN-1) THEN
        WRITE(9,*) 
     +       'ERREUR NOMBRE DE ZONES PAS CONSTANT SUR LES SECTIONS',NBST
!           STOP
      ENDIF
! fin du if sur ttdir    
         ENDIF
         ENDIF
! RAZ DES COMPTEURS :
      NOLGN=2
!        1 RESERVE A LA BORDURE
      NBPTST(NBST) = NBPTSEC -1
      NBPTSEC=1
      NBST=NBST+1
      IF(NBST.GT.KS1) THEN 
      WRITE(9,*) ' NOMBRE DE SECTIONS TROP GRAND >',KS1
      STOP
      ENDIF
! LECTURE ENTETE SECTION SUIVANTE
      read(NCA,100,end=15) N1,N2,N3,N4,pk(nbst),NOMST(NBST) 
        N3=0
        j=1
        M0=M
!    IF(N3.NE.0) WRITE(9,*) '### FICHIER DE SECTIONS INCORRECT'
      GOTO 10
    
      ENDIF
! end cas fin de segment
!
! DECODAGE PT COURANT 
      IF (ABS(X1-999.999).GT.0.0001) THEN 
      IF (X1.GT.XMAX) XMAX=X1
      IF (Y1.GT.YMAX) YMAX=Y1
      IF (Z1.GT.ZMAX) ZMAX=Z1
      IF (X1.LT.XMIN) XMIN=X1
      IF (Y1.LT.YMIN) YMIN=Y1
      IF (Z1.LT.ZMIN) ZMIN=Z1
      If(TTCONF)THEN
       IF(NBPTSEC.GT.1)THEN
! 0.00006 car x, y et z en 4 decimales
        if(abs(x1-stx(nbptsec-1,nbst)).lt.0.00006)then
        if(abs(y1-sty(nbptsec-1,nbst)).lt.0.00006)then
        if(abs(z1-stz(nbptsec-1,nbst)).lt.0.00006)then
        IF(CODL.EQ.'   ')then
! point confondu donc on revient en arriere
          nbptsec=nbptsec-1
! fin du if sur codl
      ENDIF
! fin du if sur z1
      ENDIF
! fin du if sur y1
      ENDIF
! fin du if sur x1
      ENDIF
! fin du if sur nbptsec=1
      ENDIF
! fin du if sur ttconf
      ENDIF
 
      STX(NBPTSEC,NBST) = X1
      STY(NBPTSEC,NBST) = Y1
      STZ(NBPTSEC,NBST) = Z1
      NBPTSEC=NBPTSEC+1
      IF(NBPTSEC.GT.KP1) THEN 
      WRITE(9,*) ' NOMBRE DE POINTS PAR  SECTION TROP GRAND >',KP1
      STOP
! fin du if sur nbptsec=kp1
      ENDIF
! fin du if sur x1=999.999
      ENDIF
!
! PASSAGE DE ZONE
!c      IF (X1.EQ.-1.) THEN
! si tout point  = directrice
! sauf premier point et dernier
      IF (TTDIR) THEN
        IF(M-M0.NE.1)THEN
         IF(M-M0.NE.N4)THEN
          IF(ABS(X1-999.999).GT.0.0001)THEN
!c pas de test...
!c    IF (NOLGN.NE.INT(Y1)+1) WRITE(9,*) 'BAVURE .... LIGNES NON ORDONNEES'
!c     +    ,NBST
      NZONE(NBST,NOLGN)=NBPTSEC-1
      IF(TTNOM)THEN
        IF(CODL.EQ.'   ')then
          CODL=COD(J)
          j=J+1
        ENDIF
! fin du if sur TTNOM        
      ENDIF    
      CODLGN(NOLGN)=CODL
      NOLGN=NOLGN+1
! fin des if sur m=1,N4 et fin de section    
      ENDIF
      ENDIF
      ENDIF
!c    GOTO 10
! si directrice dans le cas pas TTDIR      
      ELSEIF (CODL.NE.'   ') THEN
!c pas de test...
!c    IF (NOLGN.NE.INT(Y1)+1) WRITE(9,*) 'BAVURE .... LIGNES NON ORDONNEES'
!c     +    ,NBST
      NZONE(NBST,NOLGN)=NBPTSEC-1
      CODLGN(NOLGN)=CODL
      NOLGN=NOLGN+1
!c    GOTO 10
      ENDIF 
  10  CONTINUE
!
 15   NBST=NBST-1
!
! FIN LECTURE FICHIER
!CCCCCCCCCCCCCCCCCCCC
      WRITE(9,*) ' '
      WRITE(9,*) 'NOMBRE DE SECTIONS: ',NBST
      WRITE(9,*) 'NOMBRE DE LIGNES  : ',NBLGNO-2,'(+2 BORDURES)'
      WRITE(9,*) 'XMIN , XMAX :',XMIN,XMAX
      WRITE(9,*) 'YMIN , YMAX :',YMIN,YMAX
      WRITE(9,*) ' '
      WRITE(9,*) ' CODE DES LIGNES'
      WRITE(9,'(20(1X,I3))') (I,I=1,NBLGNO-2)
      WRITE(9,'(20(1X,A3))') (CODLGN(I),I=2,NBLGNO-1)
      WRITE(9,*) ' '
!
      END
!
!##############################################################################
! SOUROUTINE: ECR_FIC (NCA,VERSION,FICIN,FICOUT,LPLAN,LINEAIRE)
! NCA= NO CHANNEL
!##############################################################################
      SUBROUTINE ECR_FIC(NCA,VERSION,FICIN,FICOUT,LPLAN,LINEAIRE)
!
      implicit none
      INTEGER NOST,NOPT,NBPTSTI,NBST,NBSTF,K,I
     :,NBLGN,NBPTZN,MZOP,NBPTZOP
     :,LPC,NZONE,NLDAXE
     :,KS1,KP1,KZO,KS2,KP2,KP3,NBSTI,LINZ,NSTO
     :,NBLGNO,NBHISTO,NCA,NOPER,NOLGN,NOHISTO,NSTPER,NBPER,NSTI 
      DOUBLE PRECISION X1,Y1,Z1,H2,H31,H32,VERSION
     :,PK,PKF,CODE
      PARAMETER (KS1=1500)
      PARAMETER (KP1=1000)
      PARAMETER (KZO=1000)
      PARAMETER (KS2=1500)
      PARAMETER (KP2=10500)
      PARAMETER (KP3=1000)
      DOUBLE PRECISION  STFX(KP2,KS2),STFY(KP2,KS2),
     + STFZ(KP2,KS2),STFOX(KP3,KS2),STFOY(KP3,KS2),
     + STFOZ(KP3,KS2)
!
      CHARACTER FICIN*35, FICOUT*35
      Logical change1,LPLAN,LINEAIRE
      character *1 tab(52)
!
      CHARACTER VER*5
      CHARACTER LIGNE*10,LIGNE2*80
      CHARACTER TXT1*15,TXT2*15
! ENTETE DE FICHIER: IE HISTORIQUE DU TRAITEMENT:
      CHARACTER HISTO*80
      COMMON/BDHISTO/HISTO(20)/NBHISTO/NBHISTO
      COMMON/HISTO/H2,H31,H32,LINZ,NLDAXE   
      COMMON/BDDIM/NBST,NBPTSTI,NBSTF
      COMMON/BDSTF/ NBSTI(KS2),STFX,STFY,
     + STFZ
      COMMON/BDENTETE/PK(KS1),PKF(KS2)
! BD NOM DES SECTIONS ORIGINE
       CHARACTER*12 NOMST,NOMST1
       COMMON /BDNOMST/NOMST(KS1)
! NBLGN:NB LGN PRISE EN COMPTE 
      CHARACTER CODLGN*3
      COMMON /BDZO/NZONE(KS1,KZO),NBLGNO,LPC(KZO),CODLGN(KZO)
!
!BD STF APRES OPTIMISATION....
      COMMON/BDSTFO/ STFOX,STFOY,
     + STFOZ
!
! INDICE DES LIGNES DIRECTRICES DANS LE MAILLAGE
      INTEGER PTLGN
      DOUBLE PRECISION PASM 
      COMMON/BDLGN/PTLGN(0:KZO+1),PASM(KZO)
      COMMON/NBDLGN/NBLGN,NBPTZN(KZO)
!
! MZOP ZONE OPTIMISER=1, 
      COMMON/BDOPT/MZOP(KZO),NBPTZOP(KZO)
!
! BD ST OU LES TRAJ SONT PERPENDICULAIRE AU ST (PLAN X,Y)
      COMMON/BDSTPER/NBPER,NSTPER(KS1)
!
      CHARACTER CODL*3,REP*1,codpre*3
      LOGICAL ecrm,ecrdir
      double precision xpre,ypre,zpre
!
! FORMAT DE L HISTORIQUE 
 99   format(A80)
! FORMAT DES ENTETES DE SECTIONS 
 100  format(4(1X,I5),1X,F12.4,1X,A12)
! FORMAT DES POINTS DES SECTIONS
 101  format (3(1X,F12.4),1X,A3)
        X1=0.
        Y1=0.
        Z1=0.
 
! pour tester le nom des sections alhpabet mis dans tab
       Tab(1)='a' 
       Tab(2)='b' 
       Tab(3)='c' 
       Tab(4)='d' 
       Tab(5)='e' 
       Tab(6)='f' 
       Tab(7)='g' 
       Tab(8)='h' 
       Tab(9)='i' 
       Tab(10)='j' 
       Tab(11)='k' 
       Tab(12)='l' 
       Tab(13)='m' 
       Tab(14)='n' 
       Tab(15)='o' 
       Tab(16)='p' 
       Tab(17)='q' 
       Tab(18)='r' 
       Tab(19)='s' 
       Tab(20)='t' 
       Tab(21)='u' 
       Tab(22)='v' 
       Tab(23)='w' 
       Tab(24)='x' 
       Tab(25)='y' 
       Tab(26)='z' 
       Tab(27)='A' 
       Tab(28)='B' 
       Tab(29)='C' 
       Tab(30)='D' 
       Tab(31)='E' 
       Tab(32)='F' 
       Tab(33)='G' 
       Tab(34)='H' 
       Tab(35)='I' 
       Tab(36)='J' 
       Tab(37)='K' 
       Tab(38)='L' 
       Tab(39)='M' 
       Tab(40)='N' 
       Tab(41)='O' 
       Tab(42)='P' 
       Tab(43)='Q' 
       Tab(44)='R' 
       Tab(45)='S' 
       Tab(46)='T' 
       Tab(47)='U' 
       Tab(48)='V' 
       Tab(49)='W' 
       Tab(50)='X' 
       Tab(51)='Y' 
       Tab(52)='Z' 
       
        WRITE(9,*)'abandon points doubles (2/1/0)'
        WRITE(9,*)'(defaut = 0)'
        WRITE(9,*)'si abandon, le fichier ne sera pas un .m'
        WRITE(9,*)'et le nombre de points ecrit sera faux'
        WRITE(9,*)'si reponse 2, on abandonne point double directrice'
        WRITE(9,*)'si reponse 1, on garde directrice'
        WRITE(9,*)'si reponse 0, on garde tout'
        rep = '0' !read(*,'(A)')rep
        IF(REP.EQ.'1')then
            ecrm=.FALSE.
            ecrdir=.TRUE.
        ELSEIF(REP.EQ.'2')then
            ecrm=.FALSE.
            ecrdir=.FALSE.
        ELSE
            ECRM=.TRUE.
        ENDIF
! ECRITURE DE L HISTORIQUE:
      DO 200 NOHISTO=1,NBHISTO
      WRITE(NCA,99)HISTO (NOHISTO)
 200    CONTINUE
! MISE A JOUR DE L'HISTORIQUE:
!        WRITE(9,*)'LIGNE2'
      WRITE(VER,'(F5.2)')VERSION
      WRITE(NCA,'(A)') '# SECMA V'//VER//' ENTREE: '//FICIN//
     + ' SORTIE: '//FICOUT
! on ecrit que si pas trop de zones
          IF ((NBLGN+1).LT.10) THEN
      WRITE(LIGNE2,'(12I4)') (LPC(I),I=2,NBLGN-1)
      WRITE(NCA,'(A)')'#  DIRECTRICES UTiLISEES : '//LIGNE2
!
      WRITE(LIGNE2,'(12I4)') (NBPTZN(I),I=1,NBLGN+1)
      WRITE(NCA,'(A)')'#  POINTS PAR ZONES: '//LIGNE2
!
      WRITE(LIGNE2,'(12(1X,I1))') (MZOP(I),I=1,NBLGN+1)
      WRITE(NCA,'(A)')'#  ZONES OPTIMISEES :'//LIGNE2
       WRITE(LIGNE2,'(12(1X,F5.1))') (MIN(PASM(I),999.9),I=1,NBLGN+1)
       WRITE(NCA,'(A)')'#  PAS TRANSVERSAL: '//LIGNE2
          ENDIF
!
!      WRITE(LIGNE2,'(2X,30(2X,I1))') (LPC(I),I=2,NBLGNO-1)
!      WRITE(NCA,'(A)')'#  LIGNES UTLISEES :'//LIGNE2
!
!      WRITE(LIGNE2,'(30(1X,I2))') (NBPTZN(I),I=1,NBLGN+1)
!      WRITE(NCA,'(A)')'#  COURBES PAR ZONES:'//LIGNE2
!
!      WRITE(LIGNE2,'(30(2X,I1))') (MZOP(I),I=1,NBLGN+1)
!      WRITE(NCA,'(A)')'#  ZONES OPTIMIS�ES :'//LIGNE2
!
!        WRITE(9,*)'LIGNE3'

!          IF ((NBLGN+1).GT.15) THEN
!       WRITE(LIGNE2,'(15(1X,F5.1))') (PASM(I),I=1,15)
!       WRITE(NCA,'(A)')'#  PAS PAR ZONES: '//LIGNE2
!       WRITE(LIGNE2,'(15(1X,F5.1))') (PASM(I),I=16,NBLGN+1)
!       WRITE(NCA,'(A)')'#                   '//LIGNE2
!          ELSE
!       WRITE(LIGNE2,'(15(1X,F5.1))') (PASM(I),I=1,NBLGN+1)
!       WRITE(NCA,'(A)')'#  PAS PAR ZONES: '//LIGNE2
!          ENDIF
!        WRITE(9,*)'LIGNE4'
!
      IF(LINEAIRE)THEN
        IF(LPLAN)THEN
           WRITE(NCA,'(A)')'#  INTERPOLATION LINEAIRE 2D'
        ELSE
           WRITE(NCA,'(A)')'#  INTERPOLATION LINEAIRE 3D'
        ENDIF
      else
        IF(LPLAN)THEN
           IF (LINZ.EQ.1)THEN
      WRITE(NCA,'(A)')'# INTERPOL SPLIN 2D ET Z: INTERPOL LINEAIRE'
           ELSE
              WRITE(NCA,'(A)')'# INTERPOL SPLIN 2D'
           ENDIF
        ELSE
           IF (LINZ.EQ.1)THEN
      WRITE(NCA,'(A)')'# INTERPOL SPLIN 3D ET Z: INTERPOL LINEAIRE'
           ELSE
              WRITE(NCA,'(A)')'# INTERPOL SPLIN 3D'
           ENDIF
        ENDIF
      endif
!
! TYPE DE CALCUL DES PAS D ESPACE ENTRE SECTION:
! PAS D ESPACE
          WRITE(LIGNE,'(F10.3)') H2
! TYPE DE MAX
        WRITE(VER,'(1X,I3,1X)') NLDAXE
            IF(H31.EQ.0) TXT1='DIRECT. '//VER
            IF(H31.EQ.1) TXT1='BORD 1 '
            IF(H31.EQ.2) TXT1='BORD 2 '
            IF(H32.EQ.0) TXT2='-DIRECT. '//VER
            IF(H32.EQ.1) TXT2='-BORD 1 '
            IF(H32.EQ.2) TXT2='-BORD 2 '
!        WRITE(9,*)'LIGNE5'

        WRITE(NCA,'(A)')'#  D LONGITUDINAL SUR '//TXT1//TXT2
     +           //LIGNE
!
!      WRITE(LIGNE2,'(13(1X,I2))') (NSTPER(I),I=1,NBPER)
!      WRITE(NCA,'(A)')'#    COURBES PERPENDI AUX ST:'//LIGNE2

!
! ECRITURE DU MAILLAGE:
!
!        WRITE(9,*)'LIGNE6'

      NSTO=1 
      NSTI= -1
      DO 630 NOST=1,NBSTF
       IF(NSTI.EQ.NBSTI(NSTO)) THEN
        NSTO=NSTO+1
        NSTI=0
       ELSE
        NSTI=NSTI+1
       ENDIF
         IF(NSTI.EQ.0) THEN
           NOMST1=NOMST(NSTO)
         ELSE
! NOM DE LA SECTION = MELANGE NOM ST -AVANT-APRES
           NOMST1=NOMST(NSTO)(1:5)//'-'//NOMST(NSTO+1)(1:6)
         ENDIF
! on commence � 11 parce que le nom de section peut avoir 12 caract�res
           DO K=11,2,-1
           IF(NOMST1(K:K).EQ.' ')THEN
             NOMST1=NOMST1(1:K-1)//NOMST1(K+1:12)//' '
           ENDIF
           ENDDO 
           CHANGE1=.TRUE.
           Do I=1,12
! on teste que le premier caractere soit une lettre minuscule ou majuscule
           Do K=1,52
             IF(NOMST1(1:1).EQ.Tab(K))CHANGE1=.FALSE.
           ENDDO 
           IF(Change1)THEN
             NOMST1=NOMST1(2:12)//' '
           ENDIF
           EnDdo
!
! ENTETE DE SECTION
!        WRITE(9,*)'LIGNE7',NOST

!     write(NCA,100) NOST,NSTO,NSTI,NBPTSTI,PKF(NOST)
       write(NCA,100) NOST,NSTO,NSTI,NBPTSTI,PKF(NOST),NOMST1 
       DO 640 NOPT=1,NBPTSTI
         xpre=x1
         ypre=y1
         zpre=z1
         codpre=codl
       X1=STFOX(NOPT,NOST)
       Y1=STFOY(NOPT,NOST)
       Z1=STFOZ(NOPT,NOST)
       CODL='   '
!        WRITE(9,*)'LIGNE8',NOPT
       DO 220 NOLGN=1,NBLGN
          IF(NOPT.EQ.PTLGN(NOLGN)) CODL=CODLGN(NOLGN+1)
 220       CONTINUE
       IF(ECRM)THEN
         WRITE (NCA,101) X1,Y1,Z1,CODL
       ELSEIF(ECRDIR)THEN
        if(NOPT.NE.1)THEN
         IF((abs(x1-xpre).GT.0.00001).OR.
     :(abs(y1-ypre).GT.0.00001).OR.
     :(abs(z1-zpre).GT.0.00001))then
         WRITE (NCA,101) Xpre,Ypre,Zpre,CODpre
! si distance faible
         elseif(codpre.ne.'   ')then
             if(codl.ne.'   ')then
         WRITE (NCA,101) Xpre,Ypre,Zpre,CODpre
             else
               codl=codpre
! on ecrit pas le point car on ecrira apres
             endif
! fin du if sur distance
           endif
           if(nopt.eq.NBPTSTI)then
         WRITE (NCA,101) X1,Y1,Z1,CODL
           endif
! fin du if sur nopt=1
       endif
! else sur ecrdir/ecrm
! cas ou on abandonne lignes directrices
       ELSE 
        if(NOPT.NE.1)THEN
         IF((abs(x1-xpre).GT.0.00001).OR.
     :(abs(y1-ypre).GT.0.00001).OR.
     :(abs(z1-zpre).GT.0.00001))then
         WRITE (NCA,101) Xpre,Ypre,Zpre,CODpre
! si distance faible
         elseif(codpre.ne.'   ')then
             if(codl.eq.'   ')then
!         WRITE (NCA,101) Xpre,Ypre,Zpre,CODpre
!             else
               codl=codpre
! on ecrit pas le point car on ecrira apres
             endif
! fin du if sur distance
           endif
           if(nopt.eq.NBPTSTI)then
         WRITE (NCA,101) X1,Y1,Z1,CODL
           endif
! fin du if sur nopt=1
       endif
! fin du if sur ecrm
       ENDIF
  640   CONTINUE
!
! CODE=0 => ESTIMATEUR DE DERIVEE OSC GENERALISE
! CODE=1 => ESTIMATEUR DE DERIVEE PERPENDICULAIRE ST PLAN X Y
!
      CODE=0.
      DO 650 NOPER=1,NBPER
      IF(NOST.EQ.NSTPER(NOPER)) CODE=1.
650     CONTINUE 
! SEPARATEUR +CODE DE TRANSITION ENTRE SECTION
       WRITE (NCA,101) 999.999, 999.999, CODE,' '
! fin boucle sur nost (section)
630   CONTINUE
!
      END
!
!##############################################################################
      SUBROUTINE CALC_ST_PLANE(NOST)
!##############################################################################
! CALCUL DES ST FINALES ENTRE NOST ET NOST+1
! CAS ENTRE DEUX ST MESUREES PLANES => STF PLANE
!(IL Y EN A NBSTI(NOST) ENTRE CES DEUX ST)
!
! PT A ET B = POINT SUR COURBES EXTREMES DEFINISSANT LE PLAN DE COUPE
! 
      implicit none
      INTEGER NOST,NBPTSTI,NBST,NBSTF
     :,NOSTI,NT,NOCB,NBSTI
     :,KS1,KP1,KZO,KS2,KP2,KP3,NBT,NBSTFF
      DOUBLE PRECISION Z,X,Y,X2,Y2,Z2,X1,Y1,Z1,XA,YA,D,T,T1
     :,T0,Z0,DELTA2,YB,XB,ZB,D0,X0,Y0,DX,DY,ZA

      PARAMETER (KS1=1500)
      PARAMETER (KP1=1000)
      PARAMETER (KZO=1000)
      PARAMETER (KS2=1500)
      PARAMETER (KP2=10500)
      PARAMETER (KP3=1000)
      DOUBLE PRECISION  STFX(KP2,KS2),STFY(KP2,KS2),
     + STFZ(KP2,KS2)
!
! NB DE POINTS DE DISCRETISATION DES COURBES
      PARAMETER (NBT=100000)
!
      COMMON/BDDIM/NBST,NBPTSTI,NBSTF
!
! BD ST  FINALE  NBPTSTI*NBSTF          
! NBSTI nb des section inter entre section de depart
      COMMON/BDSTF/ NBSTI(KS2),STFX,STFY,STFZ
!
      DOUBLE PRECISION LCT1(0:NBT),LCT2(0:NBT)
!       
! PRELIMINAIRES
!CCCCCCCCCCCCCCC
!
! CALCUL ABCISSE CURVILIGNE SUR LA COURBE  NO 1 => LCT1(...)
      NOCB=1
      LCT1(0)=0.
      T=0.
      CALL PT_TRAJ(NOST,NOCB,T,X1,Y1,Z1)
!
      DO 510 NT=1,NBT
      T=(NT*1.)/(NBT*1.)
      CALL PT_TRAJ(NOST,NOCB,T,X2,Y2,Z2)
!
      D=SQRT((X2-X1)*(X2-X1)+(Y2-Y1)*(Y2-Y1)+(Z2-Z1)*(Z2-Z1))
      LCT1(NT)=LCT1(NT-1)+D
!
      X1=X2
      Y1=Y2
      Z1=Z2
 510  CONTINUE
!
! CALCUL ABCISSE CURVILIGNE SUR LA COURBE  NO NBPTSTI => LCT2(...)
      NOCB=NBPTSTI
      LCT2(0)=0.
      T=0.
      CALL PT_TRAJ(NOST,NOCB,T,X1,Y1,Z1)
!
      DO 512 NT=1,NBT
      T=(NT*1.)/(NBT*1.)
      CALL PT_TRAJ(NOST,NOCB,T,X2,Y2,Z2)
!
      D=SQRT((X2-X1)*(X2-X1)+(Y2-Y1)*(Y2-Y1)+(Z2-Z1)*(Z2-Z1))
      LCT2(NT)=LCT2(NT-1)+D
!
      X1=X2
      Y1=Y2
      Z1=Z2
 512  CONTINUE
!
!
! CEST PARTI: POUR CHAQUE SECTION INTERMEDIAIRE A TROUVER ...
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
! NBSTFF est l'indice dans STF de la prochaine Section de base
      NBSTFF=NBSTF+NBSTI(NOST)+1
      DO 520 NOSTI=1,NBSTI(NOST)
      NBSTF=NBSTF+1
      IF(NBSTF.GT.KS2) THEN 
        WRITE(9,*) ' NOMBRE DE SECTIONS FINALES TROP GRAND >',KS2
        STOP
      ENDIF
!    WRITE(9,*) 'CALCULS SECTION (PLANE)           ',NBSTF
!
!
! RECHERCHE0 DES POINTS A ET B DEFINISSANT LE PLAN DE COUPE...
!CCCCCCCCCCCCCCCCC
! POINT A RECHERCHE PARAMETRE T CORRESPONDANT AU RATION DE D
      D=(NOSTI*1.)/((NBSTI(NOST)+1)*1.)*LCT1(NBT)
      DO 515 NT=1,NBT
      IF(LCT1(NT).GE.D) GOTO 516
 515    CONTINUE
!  CALCUL DE POINT A
 516    T=(NT*1.)/(NBT*1.)
      NOCB=1
      CALL PT_TRAJ(NOST,NOCB,T,XA,YA,ZA)
!
! POINT B RECHERCHE PARAMETRE T CORRESPONDANT AU RATION DE D
      D=(NOSTI*1.)/((NBSTI(NOST)+1)*1.)*LCT2(NBT)
      DO 518 NT=1,NBT
      IF(LCT2(NT).GE.D) GOTO 519
 518    CONTINUE
! CALCUL DU POUN B
 519    T=(NT*1.)/(NBT*1.)
      NOCB=NBPTSTI
      CALL PT_TRAJ(NOST,NOCB,T,XB,YB,ZB)
!
! MISE EN BOITE DES PTS EXTREMES A ET B
      STFX(1,NBSTF)= XA
      STFY(1,NBSTF)= YA
      STFZ(1,NBSTF)= ZA
      STFX(NBPTSTI,NBSTF)= XB
      STFY(NBPTSTI,NBSTF)= YB
      STFZ(NBPTSTI,NBSTF)= ZB
!
      DX=XB-XA
      DY=YB-YA
!
! RECHERCHE PAR DICHOTOMIE DES PTS SUR LES AUTRES COURBES:
!CCCCCCCCCCCCCCCCC
! EQUATION : (X-XA)DY-(Y-YA)DX=0
! TO ET T1 INDICE DE CHAQUE COTE SUR LA COURBE
      DO 560 NOCB=2,NBPTSTI-1
         T0=0.
         CALL PT_TRAJ(NOST,NOCB,T0,X0,Y0,Z0)
         D0=(X0-XA)*DY-(Y0-YA)*DX
!
         T1=1.
         CALL PT_TRAJ(NOST,NOCB,T1,X1,Y1,Z1)
!         D1=(X1-XA)*DY-(Y1-YA)*DX
!
         DELTA2=1.E-6
!            WRITE(9,*) 'INIT:',T0,D0,T1,D1
!
! TEST DE FIN DE RECHERCHE(T0 ET T1 TRES PROCHE):
 570         IF(ABS(T0-T1).LE.DELTA2) GOTO 580
         T=(T0+T1)/2.
         CALL PT_TRAJ(NOST,NOCB,T,X,Y,Z)
         D=(X-XA)*DY-(Y-YA)*DX
!
!            WRITE(9,*) T0,T1,T,D
!         NEW T0 OU T1
         IF (SIGN(1.D0,D).EQ.SIGN(1.D0,D0)) THEN
        T0=T
        D0=D
         ELSE
        T1=T
!        D1=D
         ENDIF
         GOTO 570           
!
! MISE EN BOITE
 580     STFX(NOCB,NBSTF)= X
         STFY(NOCB,NBSTF)= Y
         STFZ(NOCB,NBSTF)= Z
!               
! RECHERCHE SUR COURBE SUIVANTE 
 560    CONTINUE

!     PROCHAINE ST INTERMEDAIRE
 520  CONTINUE
      END
!##############################################################################
      SUBROUTINE CALC_ST_TORDU(NOST)
!##############################################################################
! CALCUL DES ST FINALES ENTRE NOST ET NOST+1
! CAS ENTRE DEUX ST MESUREES NON PLANES => STF NON PLANE
!(IL Y EN A NBSTI(NOST) ENTRE CES DEUX ST)
!
      implicit none
      INTEGER NOST,NBPTSTI,NBST,NBSTF,NBSTFF
     :,NT,NOSTI,NBSTF2,NBT,NOCB,NBSTI
     :,KS1,KP1,KZO,KS2,KP2,KP3 
      DOUBLE PRECISION Z1,X1,Y1,X,Y,Z,X2,Y2,Z2,D,T,PAS
      PARAMETER (KS1=100)
      PARAMETER (KP1=1000)
      PARAMETER (KZO=1000)
      PARAMETER (KS2=1500)
      PARAMETER (KP2=10500)
      PARAMETER (KP3=1000)
      DOUBLE PRECISION  STFX(KP2,KS2),STFY(KP2,KS2),
     + STFZ(KP2,KS2)
!
! NB DE POINTS DE DISCRETISATION DES COURBES
      PARAMETER (NBT=100000)
!
      COMMON/BDDIM/NBST,NBPTSTI,NBSTF
!
! BD ST  FINALE  NBPTSTI*NBSTF          
! NBSTI nb des section inter entre section de depart
      COMMON/BDSTF/ NBSTI(KS2),STFX,STFY,
     + STFZ
!
      DOUBLE PRECISION LCT(0:NBT)
!       
! CEST PARTI: POUR CHAQUE SECTION INTERMEDIAIRE A TROUVER ...
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!      WRITE(9,*) 'CALCULS COURBE (ST NON PLANE)     '
      DO 520 NOCB=1,NBPTSTI
!
!
! CALCUL ABCISSE CURVILIGNE SUR LA COURBE  NO NOCB => LC(...)
      LCT(0)=0.
      CALL PT_TRAJ(NOST,NOCB,0.D0,X1,Y1,Z1)
!
      DO 510 NT=1,NBT
      T=(NT*1.)/(NBT*1.)
      CALL PT_TRAJ(NOST,NOCB,T,X2,Y2,Z2)
      D=SQRT((X2-X1)*(X2-X1)+(Y2-Y1)*(Y2-Y1)+(Z2-Z1)*(Z2-Z1))
!
      LCT(NT)=LCT(NT-1)+D
      X1=X2
      Y1=Y2
      Z1=Z2
 510  CONTINUE
!
!
! RECHERCHE DES POINTS DES STF SUR COURBE NOCB
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      NBSTF2=NBSTF
      NBSTFF=NBSTF+NBSTI(NOST)+1
      DO 560 NOSTI=1,NBSTI(NOST)
          PAS=LCT(NBT)/((NBSTI(NOST)+1)*1.)
!          IF (PAS.LT.8.) WRITE(9,*) 
!     +    ' ATTENTION=====> DISTANCE ENTRE POINT<8 COURBE:',NOCB

! RECHERCHE DU POINT A LA DISTANCE D 
      D=(NOSTI*1.)*PAS

      DO 540 NT=1,NBT
      IF(LCT(NT).GE.D) GOTO 550
 540    CONTINUE
 550    T=(NT*1.)/(NBT*1.)
      CALL PT_TRAJ(NOST,NOCB,T,X,Y,Z)
!
! MISE EN BOITE
      NBSTF2=NBSTF2+1
      STFX(NOCB,NBSTF2)= X
      STFY(NOCB,NBSTF2)= Y
      STFZ(NOCB,NBSTF2)= Z
!
! RECHERCHE PT SUR PROCHAINE ST MEME COURBE
 560    CONTINUE

!     PROCHAINE COURBE ....
 520  CONTINUE
      NBSTF=NBSTF+NBSTI(NOST)
!      IF(NBSTF.GT.KS2) THEN 
!        WRITE(9,*) ' NOMBRE DE SECTIONS FINALES TROP GRAND >',KS2
!        STOP
!      ENDIF
      END
!
!##############################################################################
      SUBROUTINE CALC_ST_LINEAIRE(NOST)
!##############################################################################
! CALCUL DES ST FINALES ENTRE NOST ET NOST+1
! CAS interpolation lineaire
!(IL Y EN A NBSTI(NOST) ENTRE CES DEUX ST)
!
      implicit none
      INTEGER NOST,NBPTSTI,NBST,NBSTF,NBSTFF
     :,NOSTI,NBSTI,NOPTI
     :,KS1,KP1,KZO,KS2,KP2,KP3 
      DOUBLE PRECISION XA,YA,ZA,YB,XB,ZB,DX,DY,DZ,D
     :,X,Y,Z
      PARAMETER (KS1=1500)
      PARAMETER (KP1=1000)
      PARAMETER (KZO=1000)
      PARAMETER (KS2=1500)
      PARAMETER (KP2=10500)
      PARAMETER (KP3=1000)
      DOUBLE PRECISION  STFX(KP2,KS2),STFY(KP2,KS2),
     + STFZ(KP2,KS2)
      DOUBLE PRECISION  STIX(KP2,KS1),STIY(KP2,KS1),
     + STIZ(KP2,KS1)
!
      COMMON/BDDIM/NBST,NBPTSTI,NBSTF
      COMMON/STI/STIX,STIY,STIZ
!
! BD ST  FINALE  NBPTSTI*NBSTF          
! NBSTI nb des section inter entre section de depart
      COMMON/BDSTF/ NBSTI(KS2),STFX,STFY,STFZ
!
! CEST PARTI: POUR CHAQUE SECTION INTERMEDIAIRE A TROUVER ...
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      NBSTFF=NBSTF+NBSTI(NOST)+1
      DO 520 NOSTI=1,NBSTI(NOST)
        NBSTF=NBSTF+1
        IF(NBSTF.GT.KS2) THEN 
          WRITE(9,*) ' NOMBRE DE SECTIONS FINALES TROP GRAND >',KS2
!          PAUSE
          STOP
        ENDIF
! RATIO entre les deux sections initiales
        D=FLOAT(NOSTI)/FLOAT(NBSTI(NOST)+1)
!
      DO 510 NOPTI=1,NBPTSTI
        XB=STIX(NOPTI,NOST+1)
        YB=STIY(NOPTI,NOST+1)
        ZB=STIZ(NOPTI,NOST+1)
        XA=STIX(NOPTI,NOST)
        YA=STIY(NOPTI,NOST)
        ZA=STIZ(NOPTI,NOST)
        DX=XB-XA
        DY=YB-YA
        DZ=ZB-ZA
        X=XA+D*DX
        Y=YA+D*DY
        Z=ZA+D*DZ
        STFX(NOPTI,NBSTF)=X
        STFY(NOPTI,NBSTF)=Y
        STFZ(NOPTI,NBSTF)=Z
 510  CONTINUE
!               
!     PROCHAINE ST INTERMEDAIRE
 520  CONTINUE
      END
!##############################################################################
      SUBROUTINE CALC_PK(NLDPK,DPLC)
!##############################################################################
!
! CALCUL DES PK DES SECTIONS DU MAILLAGE
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      implicit none
      INTEGER NOST,NBPTSTI,NBST,NBSTF
     :,NBLGN
     :,NSTDEP,NSTFIN,NLDPK,NSTO,NBSTI,NBPTZN
     :,KS1,KP1,KZO,KS2,KP2,KP3,PTLGN 
      DOUBLE PRECISION X0,Y0,X1,Y1,D,DPK,RATIO,PK,PKF,PASM
      PARAMETER (KS1=1500)
      PARAMETER (KP1=1000)
      PARAMETER (KZO=1000)
      PARAMETER (KS2=1500)
      PARAMETER (KP2=10500)
      PARAMETER (KP3=1000)
      DOUBLE PRECISION  STFX(KP2,KS2),STFY(KP2,KS2),
     + STFZ(KP2,KS2),STFOX(KP3,KS2),STFOY(KP3,KS2),
     + STFOZ(KP3,KS2),DPLC
       character*1 char1
       logical recalcul
!
      COMMON/BDDIM/NBST,NBPTSTI,NBSTF
!
! INDICE DES LIGNES DIRECTRICES DANS LE MAILLAGE
! + NB POINT /ZONE, PAS MOYEN /ST/ZONE, 
      COMMON/BDLGN/PTLGN(0:KZO+1),PASM(KZO)
      COMMON/NBDLGN/NBLGN,NBPTZN(KZO)
!
! BD ST  FINALE  NBPTSTI*NBSTF          
! NBSTI nb des section inter entre section de depart
      COMMON/BDSTF/ NBSTI(KS2),STFX,STFY,
     + STFZ
!
! BD STF APRES OPTIMISATION....
      COMMON/BDSTFO/ STFOX,STFOY,
     + STFOZ
!
! BD ENTETE DES SECTIONS
! PK: FICHIER ORIGINE, PKF: PK MAILLAGE
      COMMON/BDENTETE/PK(KS1),PKF(KS2)
!
! PROFIL EN LONG:
       INTEGER NOPTLM(KS2)
! DISTANCE POUR PKF
       DOUBLE PRECISION    DM(KS2)
!
      WRITE(9,*) ' CALCULS DES PK DES SECTIONS'
      WRITE(9,*)'entrez 1 pour recalcul des PK'
      WRITE(9,*)'defaut = pas de recalcul'
      char1 = '0' !read(*,'(A)')char1
      if(char1.NE.'1')then
         recalcul=.FALSE.
      else
         recalcul=.TRUE.
      endif
      if(recalcul)THEN
        WRITE(9,*)'la longueur totale des PK est ',DPLC
        WRITE(9,*)'donnez le PK de la section amont'
        read (*,*)PK(1) 
      ENDIF
! EXTRACTION DU LIT MINEUR POUR CALCUL DES PK...        
!
! on prend la meme pour la decoupe des sections et les pk
!      WRITE(9,*) ' DONNEZ LE NUMERO DE LA LIGNE DIRECTRICE SERVANT '
!      WRITE(9,*) ' D AXE POUR LE CALCUL DES PK: '
!      READ*,NLDPK
!
! CopiE des numeros des points de l'axe sur les sections dans NOPTLM:
      DO 600 NOST=1,NBSTF
          NOPTLM(NOST)=PTLGN(NLDPK)
 600    CONTINUE
!
      NSTDEP=1
      PKF(1)=PK(1)
      DO 630 NSTO=1,NBST-1
!
! CALCUL DISTANCE SUR MAILLAGE
      NSTFIN=NSTDEP+NBSTI(NSTO)
      If(RECALCUL)THEN
!    NSTFIN=NSTDEP+NBSTI(NSTO)+1
        PK(NSTO)=PKF(NSTDEP)
      ELSE 
! inutile si calul bien fait
        PKF(NSTDEP)=PK(NSTO)
!    NSTFIN=NSTDEP+NBSTI(NSTO)
      ENDIF
      D=0.
      DO 620 NOST=NSTDEP,NSTFIN
        X0=STFOX(NOPTLM(NOST),NOST)
        Y0=STFOY(NOPTLM(NOST),NOST)
        X1=STFOX(NOPTLM(NOST+1),NOST+1)
        Y1=STFOY(NOPTLM(NOST+1),NOST+1)
        DM(NOST) = SQRT((X1-X0)*(X1-X0)+(Y1-Y0)*(Y1-Y0))
        D=D+DM(NOST)
 620    CONTINUE
!
       IF(RECALCUL)THEN
          RATIO=1.
       ELSE
      DPK=PK(NSTO+1)-PK(NSTO)
        IF(D.GT.0.0001)THEN
          RATIO=DPK/D
        ELSE
          RATIO=0.
        ENDIF
!    RATIO=DPK/D
! fin du if sur recalcul
      ENDIF
! MAJ DES PK
      D=0
      DO 640 NOST=NSTDEP,NSTFIN
!    DO 640 NOST=NSTDEP,NSTFIN+1
      PKF(NOST)=PK(NSTO)+D*RATIO
      D=D+DM(NOST)
!      WRITE(9,*) 'ST PK',NOST,PKF(NOST),'RATIO',RATIO
 640    CONTINUE
!
      NSTDEP=NSTFIN+1
      if(recalcul)then
          pkf(nstdep)=pk(nsto)+D
      endif
 630  CONTINUE
! LAST SECTION
      IF(.NOT.RECALCUL)THEN 
      PKF(NBSTF)=PK(NBST)
      ENDIF 
!      WRITE(9,*) 'ST PK',NBSTF,PKF(NBSTF)
      END
!
!##############################################################################
      SUBROUTINE OPTIMISATION(LPLAN)
!##############################################################################
! CREE STFO A PARTIR DES STF EN CHOISSISANT LES POINTS DANS LES ZONES
! A OPTIMISER
!
      implicit none
      INTEGER NOST,NOZ,NOPT,NPT,NBPZ,NBPTSTI,NBST,NBSTF,NBM
     :,NPTO,NBIDON
     :,NBLGN,NBPTZN,MZOP,NBPTZOP,NBSTI
     :,KS1,KP1,KZO,KS2,KP2,KP3,PTLGN,PTDEBF,I
     :, nbidon2,ncand,nprec,nplus
!      DOUBLE PRECISION X,Y,XB,YB
      DOUBLE PRECISION Z,X,Y,XB,YB,ZB
     :,PASVRAI,PASM 
      LOGICAL LPLAN,IDEMPREC,ENCORE
     
      PARAMETER (KS1=1500)
      PARAMETER (KP1=1000)
      PARAMETER (KZO=1000)
      PARAMETER (KS2=1500)
      PARAMETER (KP2=10500)
      PARAMETER (KP3=1000)
      
      DOUBLE PRECISION STFX(KP2,KS2),STFY(KP2,KS2),
     + STFZ(KP2,KS2),STFOX(KP3,KS2),STFOY(KP3,KS2),
     + STFOZ(KP3,KS2)
      COMMON/BDDIM/NBST,NBPTSTI,NBSTF
      COMMON/BDSTF/ NBSTI(KS2),STFX,STFY,
     + STFZ
!BD STF APRES OPTIMISATION....
      COMMON/BDSTFO/ STFOX,STFOY,
     + STFOZ

! INDICE DES LIGNES DIRECTRICES DANS LE MAILLAGE
! + NB POINT /ZONE, PAS MOYEN /ST/ZONE, 
      COMMON/BDLGN/PTLGN(0:KZO+1),PASM(KZO)
      COMMON/NBDLGN/NBLGN,NBPTZN(KZO)
!
! BD POUR MODULE OPTIMISATION (???)
      COMMON/BDOPT/MZOP(KZO),NBPTZOP(KZO)
!
! BD LONGUEUR
      DOUBLE PRECISION LCUM(KP2),LTOT
!      DOUBLE PRECISION LSEG(KP2),LCUM(KP2),LTOT
      DOUBLE PRECISION LCOUR
!
!      WRITE(9,*)  ' DEBUT OPTIMISATION'
!
      
      IF(PTLGN(NBLGN+1).GT.KP3) THEN 
      WRITE(9,*) ' NOMBRE DE POINTS  PAR SECTIONS '//
     +  'FINALES TROP GRAND >',KP3
      STOP
      ENDIF
!
      PTDEBF=1
      DO 100 NOZ=1,NBLGN+1
!
!        WRITE(9,*) ' '
      IF (MZOP(NOZ).NE.1) THEN
! ZONE NON OPTIMISEE => RECOPIE 
!     WRITE(9,*) ' ZONE ',NOZ,' PAS D OPTIMISATION'
!       WRITE(9,*) 'NB PT RECOPIE: ', NBPTZN(NOZ)
!       WRITE(9,*) ' DEPUIS', PTDEBF
!       WRITE(9,*) ' SUR', PTLGN(NOZ-1)
      DO 200 NOST=1,NBSTF
       DO 210 NOPT=1,NBPTZN(NOZ)
         STFOX(PTLGN(NOZ-1)+NOPT-1,NOST)=STFX(PTDEBF+NOPT-1,NOST)
         STFOY(PTLGN(NOZ-1)+NOPT-1,NOST)=STFY(PTDEBF+NOPT-1,NOST)
         STFOZ(PTLGN(NOZ-1)+NOPT-1,NOST)=STFZ(PTDEBF+NOPT-1,NOST)
 210       CONTINUE
 200     CONTINUE
      PTDEBF=PTDEBF+NBPTZN(NOZ)
      GOTO 100
      ENDIF
!
! ZONE A OPTIMISER 
      WRITE(9,*) ' ZONE ',NOZ,' OPTIMISATION EN COURS '
!     WRITE(9,*) 'NB DE POINTS CALCULES:',NBPTZOP(NOZ)
!     WRITE(9,*) 'NB DE POINTS DEMANDES:',NBPTZN(NOZ)
      WRITE(9,*) 'PAS D ESPACE DEMANDE :',PASM(NOZ)
!
      NBPZ=NBPTZOP(NOZ)
      DO 300 NOST=1,NBSTF
! si dans une section et une zone, on a un point en commun avec section precedente
! on choisit les points de meme numero ordre que section precedente
        IDEMPREC=.FALSE.
        IF(NOST.NE.1)THEN
            DO NOPT=1,NBPZ
          IF(.NOT.IDEMPREC)THEN
          NPT=PTDEBF+NOPT
          XB=STFX(NPT,NOST-1)
          YB=STFY(NPT,NOST-1)
          X=STFX(NPT,NOST)
          Y=STFY(NPT,NOST)
          IF(ABS(X-XB).LT.0.00001)THEN
          IF(ABS(Y-YB).LT.0.00001)THEN
            IDEMPREC=.TRUE.
          ENDIF
          ENDIF
! fin du if sur not.idemprec
          ENDIF
! fin boucle sur nopt
            ENDDO
! fin du if sur nost=1
        ENDIF           
         IF(IDEMPREC)THEN
! on choisit les points de meme numero ordre que section precedente
           do npto=PTLGN(NOZ-1),NBPTZN(NOZ)-1+PTLGN(NOZ-1)
            ENCORE=.TRUE.
            DO NPT=PTDEBF,PTDEBF+NBPZ
              IF(ENCORE)THEN
          XB=STFX(NPT,NOST-1)
          YB=STFY(NPT,NOST-1)
          X=STFOX(NPTO,NOST-1)
          Y=STFOY(NPTO,NOST-1)
          IF(ABS(X-XB).LT.0.00001)THEN
          IF(ABS(Y-YB).LT.0.00001)THEN
         STFOX(NPTO,NOST)=STFX(NPT,NOST)
         STFOY(NPTO,NOST)=STFY(NPT,NOST)
         STFOZ(NPTO,NOST)=STFZ(NPT,NOST)
           ENCORE=.FALSE.
          ENDIF
          ENDIF
! fin du if sur encore
              ENDIF
! fin boucle sur npt
            ENDDO
            IF(ENCORE)THEN
               WRITE(9,*)'probleme cas idemprec de optimisation'
               WRITE(9,*)'section ',nost,' zone',noz
            ENDIF  
! fin boucle sur npto
            ENDDO
!
! cas normal: section sans point commun avec section precedente
         ELSE     
!
! CALCUL DE LA LONGUEUR DE LA SECTION
        LCUM(1)=0
!        DO 310 NOPT= 1,NBPZ
!Cc
!          NPT=PTDEBF-1+NOPT
!          XB=STFX(NPT,NOST)
!          YB=STFY(NPT,NOST)
!c          ZB=STFZ(NPT,NOST)
!          X=STFX(NPT+1,NOST)
!          Y=STFY(NPT+1,NOST)
!c          Z=STFZ(NPT+1,NOST)
!          LSEG(NOPT)=SQRT((X-XB)*(X-XB)+(Y-YB)*(Y-YB))
!c          LSEG(NOPT)=SQRT((X-XB)*(X-XB)+(Y-YB)*(Y-YB)+(Z-ZB)*(Z-ZB))
!        LCUM(NOPT+1)=LCUM(NOPT)+LSEG(NOPT)
! 310        CONTINUE
!        LTOT=LCUM(NBPZ+1)
        DO 310 NOPT= 1,NBPZ
!
          NPT=PTDEBF-1+NOPT
          XB=STFX(PTDEBF,NOST)
          YB=STFY(PTDEBF,NOST)
          X=STFX(NPT+1,NOST)
          Y=STFY(NPT+1,NOST)
          IF(LPLAN)THEN
          LCUM(NOPT+1)=SQRT((X-XB)*(X-XB)+(Y-YB)*(Y-YB))
          ELSE
          ZB=STFZ(NPT,NOST)
          Z=STFZ(NPT+1,NOST)
       LCUM(NOPT+1)=SQRT((X-XB)*(X-XB)+(Y-YB)*(Y-YB)+(Z-ZB)*(Z-ZB))
          ENDIF
!          LSEG(NOPT)=SQRT((X-XB)*(X-XB)+(Y-YB)*(Y-YB)+(Z-ZB)*(Z-ZB))
!        LCUM(NOPT+1)=LCUM(NOPT)+LSEG(NOPT)
 310        CONTINUE
        LTOT=LCUM(NBPZ+1)
!            WRITE(9,*) ' DL',LSEG(NBPZ)-LSEG(1)
!
        IF (LTOT.NE.0) THEN 
          NBM=INT(LTOT/PASM(NOZ))
        ELSE
          NBM=0
          PASVRAI=0.
        ENDIF
        IF (NBM.NE.0) THEN
          PASVRAI=LTOT/FLOAT(NBM)
!          PASVRAI=LTOT/(NBM*1.)
        ENDIF
!
!        WRITE(9,*) NOST, ' LONGUEUR DE SECTION:',LTOTc
!        WRITE(9,*) '              NOMBRE DE VRAIES MAILLES',NBM
!        WRITE(9,*) '              PAS REEL                 ',PASVRAI

!
!  LES POINTS BIDON
       IF(NBM.NE.0)THEN
! NBIDON CONTIENT le nombre de dedoublements des points
              NBIDON=NBPTZN(NOZ)-NBM
       NBIDON=NBIDON/NBM
! nbidon 2 continet le nombre de points en plus � dedoubler       
       nBIDON2=NBPTZN(NOZ)-NBM*(NBIDON+1)
!       DO 330 NOPT=1,NBIDONc
!         NPT=PTDEBF
!         NPTO=PTLGN(NOZ-1)+NOPT-1
!         STFOX(NPTO,NOST)=STFX(NPT,NOST)
!         STFOY(NPTO,NOST)=STFY(NPT,NOST)
!         STFOZ(NPTO,NOST)=STFZ(NPT,NOST)
! 330       CONTINUE
! SELECTION DES VRAIS POINTS:
       nplus=0
       DO 320 NOPT=0,NBM-2
         LCOUR=Float(NOPT)*PASVRAI
         DO 340 NPT=1,NBPZ
           IF(LCUM(NPT).GE.LCOUR) GOTO 350
 340         CONTINUE
 350         NPT=PTDEBF+NPT-1
!C             NPT=PTDEBF+(NOPT-1)*NDELTA
         NPTO=PTLGN(NOZ-1)+(Nbidon+1)*NOPT+nplus
! si on a redouble une fois de plus nplus a augmente de 1
         STFOX(NPTO,NOST)=STFX(NPT,NOST)
         STFOY(NPTO,NOST)=STFY(NPT,NOST)
         STFOZ(NPTO,NOST)=STFZ(NPT,NOST)
          do I=1,NBIDON
         STFOX(NPTO+I,NOST)=STFX(NPT,NOST)
         STFOY(NPTO+I,NOST)=STFY(NPT,NOST)
         STFOZ(NPTO+I,NOST)=STFZ(NPT,NOST)
          enddo
! de temps en temps, on double un point          
          if(nbidon2.gt.0)then
            if(nopt.eq.0)then
!              nplus=nplus+1
              nprec=0
!              i=nbidon+1
!         STFOX(NPTO+I,NOST)=STFX(NPT,NOST)
!         STFOY(NPTO+I,NOST)=STFY(NPT,NOST)
!         STFOZ(NPTO+I,NOST)=STFZ(NPT,NOST)
            else
              ncand=int(float(nopt)*float(nbidon2)/float(nbm))
              if(ncand.GT.nprec)then
                nprec=ncand
                nplus=nplus+1
              i=nbidon+1
         STFOX(NPTO+I,NOST)=STFX(NPT,NOST)
         STFOY(NPTO+I,NOST)=STFY(NPT,NOST)
         STFOZ(NPTO+I,NOST)=STFZ(NPT,NOST)
              endif
! fin du if sur nopt=0
            endif              
! fin du if sur nbidon2=0
            endif              
                
 320       CONTINUE
! complment par le dernier point avant la fin de la zone
         LCOUR=Float(NBM-1)*PASVRAI
         DO 1340 NPT=1,NBPZ
           IF(LCUM(NPT).GE.LCOUR) GOTO 1350
 1340         CONTINUE
 1350         NPT=PTDEBF+NPT-1
           do npto=(nbidon+1)*(nbm-1)+nplus,NBPTZN(NOZ)-1
         STFOX(NPTO+PTLGN(NOZ-1),NOST)=STFX(NPT,NOST)
         STFOY(NPTO+PTLGN(NOZ-1),NOST)=STFY(NPT,NOST)
         STFOZ(NPTO+PTLGN(NOZ-1),NOST)=STFZ(NPT,NOST)
          enddo
! si nbm=0
         else
          npt=ptdebf
           do npto=0,NBPTZN(NOZ)-1
         STFOX(NPTO+PTLGN(NOZ-1),NOST)=STFX(NPT,NOST)
         STFOY(NPTO+PTLGN(NOZ-1),NOST)=STFY(NPT,NOST)
         STFOZ(NPTO+PTLGN(NOZ-1),NOST)=STFZ(NPT,NOST)
          enddo
        endif
! fin du if sur section normale/section avec point commun section precedente (idemprec)
        ENDIF
! fin boucle sur section
 300     CONTINUE
      PTDEBF=PTDEBF+NBPTZOP(NOZ)

 100  CONTINUE
!
! NB POINT PAR STFO :
      NBPTSTI=PTLGN(NBLGN+1) 
      WRITE(9,*) 'NOMBRE FINAL DE MAILLES ',NBPTSTI-1
!
! RECOPIE DE LA DERNIERE COURBE...
      DO 800 NOST=1,NBSTF
      STFOX(NBPTSTI,NOST)=STFX(PTDEBF,NOST)
      STFOY(NBPTSTI,NOST)=STFY(PTDEBF,NOST)
      STFOZ(NBPTSTI,NOST)=STFZ(PTDEBF,NOST)
 800  CONTINUE
      END

!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      SUBROUTINE CALCPTSECT(NOZN,LTOTMAX,LTOT,LCUM,LSEG,NOPTIDEP
     :,COMPL,LTOTMOY)
! permet de rajouter des points par zone avec un pas d'esapce     
! permet aussi de completer chauqe section pour que dans chaque zone
! elle ait le nombre de points maximal par zone
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      implicit none
      INTEGER NOST,NOPT,NBPTSTI,NBST,NBSTF
     :,NBPTZN,MZOP,NBPTZOP,NBPTST
     :,NPTFIN
     :,NOZN,NPTDEP,NOPTIDEP
     :,NPTSTI,NOPTI,NBPTZ,NBLGN
     :,KS1,KP1,KZO,KS2,KP2,KP3,PTLGN
     :,NBLGNO,LPC,NZONE
     
      DOUBLE PRECISION XT,YT,ZT,X1,Y1,Z1,X2,Y2,Z2,T
     :,PAS,PASM,X3,Y3
      LOGICAL APPART 
! NB ST MAX EN ENTREE
      PARAMETER (KS1=1500)
! NB DE POINT PAR ST MAX EN ENTREE
      PARAMETER (KP1=1000)
! NB ZONE MAX
      PARAMETER (KZO=1000)
! NB ST MAX DANS BD INTERMEDIAIRE
      PARAMETER (KS2=1500)
! NB PT PAR ST MAX DANS BD INTERMEDIAIRE
      PARAMETER (KP2=10500)
! NB PT PAR ST MAX DANS BD FINALE (POST OPTIMIZ)
      PARAMETER (KP3=1000)
      DOUBLE PRECISION  STX(KP1,KS1),STY(KP1,KS1),STZ(KP1,KS1),
     +  STIX(KP2,KS1),STIY(KP2,KS1),STIZ(KP2,KS1)
     :,ALPHA(KP1),BETA(KP1*KS1),DMOINS1,D,RATIO,COEF
      INTEGER I,j,k,L,J0,NMAX,M2,J1,NTOT,NTOTNMAX,IMAX
     :,NPOINTPLUS,NBETA(KP1),NBETA2(KP1),NPLUS(KP1) 
     :,NBETAMAX
      LOGICAL TROUVE,UNDEPLUS
!,Jprec(kp1)
! variable logique pour completer nombre de points zone a nombre de points maximal
      LOGICAL COMPL(KZO),complsmax       
! lecture de reponse en caractere
      Character*80 LIGNE    
!
! BD DIM DES STRUCTURES:
! nbst   : nombre de st du fichier de depart  (<KS1)
! nbptsti: nombre de point des st du maillage (<KP2 PUIS KP3)
! nbstf  : nombre de st du maillage           (<KS2)
!
      COMMON/BDDIM/NBST,NBPTSTI,NBSTF
!
! BD ST  (ORIGINE FICHIER de depart)
!  NBPTST()*NBST -> COTE X Y Z DU POINT 
      COMMON/BDST/NBPTST(KS1),STX,STY,STZ


! BD ZONES (SECTION X NOZONE)->NO PT D'APPUI SUR LA SECTION
! NBLGNO=NB LIGNE .SZ, LPC:LIGNE PRISE EN COMPTE, 
!
! INDICE DES LIGNES DIRECTRICES DANS LE MAILLAGE
! + NB POINT /ZONE, PAS MOYEN /ST/ZONE, 
      COMMON/BDLGN/PTLGN(0:KZO+1),PASM(KZO)
      COMMON/NBDLGN/NBLGN,NBPTZN(KZO)
!
! BD POUR MODULE OPTIMISATION 
! MZOP ZONE OPTIMISER=1, 
      COMMON/BDOPT/MZOP(KZO),NBPTZOP(KZO)
!
! BD ST INTERMEDIAIRE (ST de points equidistants SELON ABCISSE CURVILIGNE )
!                      INTERPOL LIN ENTRE PTS D'UNE MEME SECTION
! NBPT-DE-LA-ZONE * NB-SECTIONS
!   NBPTSTI*NBST  
       COMMON/STI/STIX,STIY,STIZ
! BD ZONES (SECTION X NOZONE)->NO PT D'APPUI SUR LA SECTION
! NBLGNO=NB LIGNE .SZ, LPC:LIGNE PRISE EN COMPTE, 
! NBLGN:NB LGN PRISE EN COMPTE 
      CHARACTER CODLGN*3
      COMMON /BDZO/NZONE(KS1,KZO),NBLGNO,LPC(KZO),CODLGN(KZO)
!
!
! CALCULS LOCAUX: BD LONGUEUR POUR ST EN ENTREE
      DOUBLE PRECISION LSEG(KP1,KS1),LCUM(KP1,KS1),LTOT(KS1)
      DOUBLE PRECISION LCOUR,LTOTMAX,LTOTMOY,LCUM2(KP1,KS1)
      
      IF(.NOT.COMPL(NOZN))THEN
       IF(MZOP(NOZN).EQ.1) THEN
! TANT PIS, J AVAIS PREVENU...
! NBPTSTI VAUT LE MAXIMUM DE COURBES DEMAND� * 10 POUR POUVOIR CHOISIR:
        If(PASM(NOZN).EQ.0.)THEN
          WRITE(9,*)'donnez le nombre maximal de mailles zone ',NOZN
          read(*,*)NPTSTI
          PASM(NOZN)=LTOTMAX/FLOAT(NPTSTI+1)
        ELSE  
          NPTSTI = INT(LTOTMAX/PASM(NOZN))
        ENDIF  
         IF(NPTSTI.EQ.0)THEN
           MZOP(NOZN)=0
           NPTSTI=1 
           NBPTZN(NOZN)=NPTSTI 
         ELSE  
           NBPTZN(NOZN)=NPTSTI 
!           NPTSTI =NPTSTI *10
           NPTSTI =NPTSTI *20
           NBPTZOP(NOZN)=NPTSTI 
         ENDIF  
      ELSE
! CAS NON OPTIMISE
        If(PASM(NOZN).EQ.0.)THEN
          WRITE(9,*)'donnez le nombre de mailles zone ',NOZN
          nptsti = 12 !read(*,*)NPTSTI
        ELSE  
          NPTSTI = INT(LTOTMOY/PASM(NOZN))
        ENDIF  
! SI LE PAS EST PLUS GRAND QUE LA LONG MOYENNE, PAS DE LIGNE INTERMEDIAIRE
! MAIS JUSTE UNE LIGNE= LA LIGNE DIRECTRICE:
        IF(NPTSTI.EQ.0) NPTSTI=1
        NBPTZN(NOZN)=NPTSTI 
! ecriture deplacee le 11/09/06 car faux en cas d'optimisation    
         WRITE(9,*) 'NOMBRE DE Mailles  POUR la ZONE',NOZN ,': ',NPTSTI 
!       WRITE(9,*) 'PAS MOYEN SUR CETTE ZONE: ',LTOTMOY/(NPTSTI *1.)
! fin du if sur optimise    
      ENDIF
!
! POSITION DES LIGNES DIRECTRICE DANS LE MAILLAGE:
      PTLGN(NOZN)=PTLGN(NOZN-1)+NBPTZN(NOZN) 
!
! FIN DES PRELIMINAIRES
! DEBUT DU DECOUPAGE DES SECTIONS DE LA ZONES
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
! POUR TOUTES LES SECTIONS DE DONNEES:
      DO 120 NOST=1,NBST
!       WRITE(9,*)'nost',nost,LTOT(NOST),NPTSTI
!
!      PAS = LTOT(NOST)/(NPTSTI *1.)
      PAS = LTOT(NOST)/FLOAT(NPTSTI)
!       WRITE(9,*) 'SECTION LONGUEUR PAS :',NOST,LTOT(NOST),PAS
!
! POINTS DE DEPART ET DE FIN DE LA ZONE SUR LA SECTION NOST
      NPTDEP=NZONE(NOST,NOZN)
      NPTFIN=NZONE(NOST,NOZN+1)
      NBPTZ=NPTFIN-NPTDEP
!
! RECOPIE DU POINT DE LA LIGNE DIRECTRICE (NOZN)
      STIX(NOPTIDEP,NOST)=STX(NPTDEP,NOST)
      STIY(NOPTIDEP,NOST)=STY(NPTDEP,NOST)
      STIZ(NOPTIDEP,NOST)=STZ(NPTDEP,NOST)

      IF(NOST.NE.1)THEN
! si les deux extremites de la zone  sont identiques a ceux de la section d'avant
! on met tout identique
         IF((ABS(STX(NPTDEP,NOST)-STX(NZONE(NOST-1,NOZN),NOST-1))
     :.LT.0.0001).AND.(ABS(STY(NPTDEP,NOST)-
     :STY(NZONE(NOST-1,NOZN),NOST-1)).LT.0.0001))THEN
         IF((ABS(STX(NPTFIN,NOST)-STX(NZONE(NOST-1,NOZN+1),NOST-1))
     :.LT.0.0001).AND.(ABS(STY(NPTFIN,NOST)-
     :STY(NZONE(NOST-1,NOZN+1),NOST-1)).LT.0.0001))THEN
                DO NOPTI=1,NPTSTI-1
        STIX(NOPTIDEP+NOPTI,NOST)=STIX(NOPTIDEP+NOPTI,NOST-1)
        STIY(NOPTIDEP+NOPTI,NOST)=STIY(NOPTIDEP+NOPTI,NOST-1)
        STIZ(NOPTIDEP+NOPTI,NOST)=STIZ(NOPTIDEP+NOPTI,NOST-1)
              ENDDO
! LE PROCHAIN POINT DES ST INTER EST NPTSTI PLUS LOIN:
!      NOPTIDEP=NOPTIDEP+NPTSTI 
!      RETURN
! on va a la section suivante
           GO TO 120               
          ENDIF
          ENDIF
! fin du if sur nost=1
         ENDIF       


!
! RECHERCHE DES (NBPTSTI-1) POINTS SUIVANTS:
      DO 140 NOPTI=1,NPTSTI -1
! RECHERCHE LE POINT A LCOUR DU DEBUT DE LA ZONE
        LCOUR=NOPTI*PAS         
!
! CAS PARTICULIER: ZONE DE TAILLE NULLE: TOUTES LES LIGNES PASSENT PAR 1 PT
        IF (PAS.EQ.0.) THEN
!              WRITE(9,*) 'WARNING: ZONE DE TAILLE NULLE ....'
          XT=STX(NPTDEP,NOST)       
          YT=STY(NPTDEP,NOST)       
          ZT=STZ(NPTDEP,NOST)       
          GOTO 150
        ENDIF
!
!   RECHERCHE DU SEGMENT A LCOUR
        DO 160 NOPT=2,NBPTZ+1
          IF(LCUM(NOPT,NOST).GE.LCOUR) GOTO 180
 160        CONTINUE
        WRITE(9,*) 'WARNING...LCOUR>LTOT   SECTION',NOST 
!
! CALCUL DU Z INTERPOLATION LINEAIRE
180         X1=STX((NPTDEP-1)+NOPT-1,NOST)
        X2=STX((NPTDEP-1)+NOPT,NOST)
        Y1=STY((NPTDEP-1)+NOPT-1,NOST)
        Y2=STY((NPTDEP-1)+NOPT,NOST)
        Z1=STZ((NPTDEP-1)+NOPT-1,NOST)
        Z2=STZ((NPTDEP-1)+NOPT,NOST)
!
        T = (LCOUR-LCUM(NOPT-1,NOST))/LSEG(NOPT-1,NOST)
        XT=X1+T*(X2-X1)
        YT=Y1+T*(Y2-Y1)
        ZT=Z1+T*(Z2-Z1)
!
 150        STIX(NOPTIDEP+NOPTI,NOST)=XT
        STIY(NOPTIDEP+NOPTI,NOST)=YT
        STIZ(NOPTIDEP+NOPTI,NOST)=ZT
! 
!            WRITE(9,*) NOPTIDEP+NOPTI,NOST,XT
! PT SUIVANT SUR LA SECTION
 140      CONTINUE
! une fois tous les points definis, on teste si ils se trouvent sur les segments
! de la section precedente
! si le cas se presente, on confond avec le point de meme numero 
! de la section precedente
      IF(NOST.NE.1)THEN
      DO NOPTI=1,NPTSTI-1
        X3=STIX(NOPTIDEP+NOPTI,NOST)
        Y3=STIY(NOPTIDEP+NOPTI,NOST)
        APPART=.FALSE.
        DO NOPT=NOPTIDEP,NOPTIDEP+NPTSTI-2 
         IF(.NOT.APPART)THEN
          X1=STIX(NOPT,NOST-1)
          Y1=STIY(NOPT,NOST-1)
          X2=STIX(NOPT+1,NOST-1)
          Y2=STIY(NOPT+1,NOST-1)
          CALL APPARTSEG(X1,Y1,X2,Y2,X3,Y3,APPART)
          IF(APPART)THEN
            IF(NOPT.LT.NOPTIDEP+NOPTI)THEN
              DO k=NOPT+1,NOPTIDEP+NOPTI
                STIX(K,NOST)=STIX(K,NOST-1)
                STIY(K,NOST)=STIY(K,NOST-1)
                STIZ(K,NOST)=STIZ(K,NOST-1)
              ENDDO
            ELSE
              DO k=NOPTIDEP+NOPTI,NOPT
                STIX(K,NOST)=STIX(K,NOST-1)
                STIY(K,NOST)=STIY(K,NOST-1)
                STIZ(K,NOST)=STIZ(K,NOST-1)
              ENDDO
            ENDIF 
! fin du if sur APPART
          ENDIF
! fin du if sur NOT.APPART
         ENDIF
! fin de la boucle sur nopt
        ENDDO
! fin de la boucle sur nopti de nost
      ENDDO
! fin du if sur nost=1
      ENDIF 

!  section suivante
 120    CONTINUE
! else : maintenant on complete juste le fichier st en fichier m 
      ELSE
      NPTSTI=0
      NMAX=1
! POUR TOUTES LES SECTIONS DE DONNEES:
      DO NOST=1,NBST
! POINTS DE DEPART ET DE FIN DE LA ZONE SUR LA SECTION NOST
      NPTDEP=NZONE(NOST,NOZN)
      NPTFIN=NZONE(NOST,NOZN+1)
      NBPTZ=NPTFIN-NPTDEP
      IF(NBPTZ.GT.NPTSTI)THEN
! NPTSTI nombre de points maximal dans la zone
          NPTSTI=NBPTZ
        NMAX=NOST
      ENDIF
! fin boucle sur nost        
      ENDDO
! on propose de prendre un nombre de points superieur
      WRITE(9,*)'le nombre de points maximal est de '
      WRITE(9,*)NPTSTI+1,' dans la zone ',NOZN
      WRITE(9,*)'y compris le point limite droit'
      WRITE(9,*)'ET le point limite gauche'
      WRITE(9,*)'voulez vous augmenter ce nombre'
      WRITE(9,*)'si oui, entrez le nombre de points voulu'
      WRITE(9,*)'sinon touche entree'
      ligne = '  ' !read(*,'(A80)')LIGNE
      if(LIGNE(1:2).EQ.'  ')then
         WRITE(9,*)'nombre de points retenu ',NPTSTI+1
         complsmax=.false.
      else
         read(LIGNE(1:5),'(I5)')NbPTZ
         if(nbptz.GT.NPTSTI+1)THEN
         NPTSTI=NBPTZ-1
         complsmax=.true.
         WRITE(9,*)'nombre de points choisi ',NPTSTI+1
         else
         complsmax=.false.
         WRITE(9,*)'nombre de points retenu ',NPTSTI+1
         endif
      endif
! selon complsmax, on doit completer la section de depart(nmax) ou pas     
      IF(COMPLSMAX)THEN
! on recherche les longueurs relatives ou mettre des points en plus
        J=0 
        DO NOST=1,NBST
          NPTDEP=NZONE(NOST,NOZN)
          NPTFIN=NZONE(NOST,NOZN+1)
          NBPTZ=NPTFIN-NPTDEP
          DO i=1,NBPTZ
            BETA(J+I)=LCUM(I+1,NOST)/LTOT(NOST)
          ENDDO
          J=J+NBPTZ
        ENDDO
        NTOT=J
        NOST=NMAX
          NPTDEP=NZONE(NOST,NOZN)
          NPTFIN=NZONE(NOST,NOZN+1)
          NBPTZ=NPTFIN-NPTDEP
          DO i=1,NBPTZ
            ALPHA(I)=LCUM(I+1,NOST)/LTOT(NOST)
          ENDDO
          NTOTNMAX=NBPTZ
! nbeta nombre depoints en longuer relative dans chaque segement de nmax
          DO I=1,NTOTNMAX
            NBETA(I)=0
            NPLUS(I)=0
          ENDDO 
          DO J=1,NTOT
            DO I=1,NTOTNMAX
              IF(ALPHA(I).GT.BETA(J))THEN
                NBETA(I)=NBETA(I)+1
              ENDIF
            ENDDO
          ENDDO
! nombre de valeurs dans chaque intervalle
          DO I=NTOTNMAX,2,-1
            NBETA(I)=NBETA(I)-NBETA(I-1)
            NBETA2(I)=NBETA(I)
          ENDDO 
          NBETA2(1)=NBETA(1)
! boucle sur le nombre de points a rajouter
          NBETAMAX=0
          DO K=1,NPTSTI-NTOTNMAX
           DO I=1,NTOTNMAX
            IF(NBETA2(I).GT.NBETAMAX)THEN
              NBETAMAX=NBETA2(I)
              IMAX=I
            ENDIF
           ENDDO
           NPLUS(IMAX)=NPLUS(IMAX)+1
           NBETA2(IMAX)=NBETA2(IMAX)*NPLUS(IMAX)/(NPLUS(IMAX)+1)
! fin boucle sur K
          ENDDO
! rajouter les points dans STI
        STIX(NOPTIDEP,NOST)=STX(NPTDEP,NOST)
        STIY(NOPTIDEP,NOST)=STY(NPTDEP,NOST)
        STIZ(NOPTIDEP,NOST)=STZ(NPTDEP,NOST)
        lcum2(1,NOST)=LCUM(1,NOST)
            NPOINTPLUS=0
            DO J=1,NTOTNMAX
              DO I=1,NPLUS(J)
!        COEF=(BETA(NBETA2(J-1)+(NBETA(J)*I)/(NPLUS(J)+1))
!     :-BETA(NBETA2(J-1)))
!     :/(BETA(NBETA2(j))-BETA(NBETA2(J-1)))
        COEF=Float(I)/Float(NPLUS(J)+1)
        STIX(NOPTIDEP+J-1+NPOINTPLUS+I,NOST)=(1.-COEF)
     :*STX(NPTDEP+J-1,NOST)+COEF*STX(NPTDEP+J,NOST)
        STIY(NOPTIDEP+J-1+NPOINTPLUS+I,NOST)=(1.-COEF)
     :*STY(NPTDEP+J-1,NOST)+COEF*STY(NPTDEP+J,NOST)
        STIZ(NOPTIDEP+J-1+NPOINTPLUS+I,NOST)=(1.-COEF)
     :*STZ(NPTDEP+J-1,NOST)+COEF*STZ(NPTDEP+J,NOST)
       lcum2(J+NPOINTPLUS+I,nost)=
     :(1.-COEF)*lcum(j,nost)+COEF*lcum(j+1,nost)
              ENDDO
!             WRITE(9,*)j,nplus(j),NPOINTPLUS
              NPOINTPLUS=NPOINTPLUS+NPLUS(J)
        STIX(NOPTIDEP+J+NPOINTPLUS,NOST)=STX(NPTDEP+J,NOST)
        STIY(NOPTIDEP+J+NPOINTPLUS,NOST)=STY(NPTDEP+J,NOST)
        STIZ(NOPTIDEP+J+NPOINTPLUS,NOST)=STZ(NPTDEP+J,NOST)
       lcum2(J+NPOINTPLUS+1,nost)=lcum(j+1,nost)
            ENDDO
        DO J=1,NPTSTI
          LCUM(J+1,NOST)=LCUM2(J+1,NOST)
        ENDDO
! si complsmax est faux on n'a pas a completer
      ELSE
! on remplit stix
      NOST=NMAX
! POINTS DE DEPART ET DE FIN DE LA ZONE SUR LA SECTION NOST
      NPTDEP=NZONE(NOST,NOZN)
!      NPTFIN=NZONE(NOST,NOZN+1)
!      NBPTZ=NPTFIN-NPTDEP
      DO J=0,NPTSTI
        STIX(NOPTIDEP+J,NOST)=STX(NPTDEP+J,NOST)
        STIY(NOPTIDEP+J,NOST)=STY(NPTDEP+J,NOST)
        STIZ(NOPTIDEP+J,NOST)=STZ(NPTDEP+J,NOST)
      ENDDO
! fin du if sur complsmax
      ENDIF     

! POUR TOUTES LES SECTIONS DE DONNEES:
! on commence par nmax et on av dabord vers plus et ensuite vers moins
      DO NOST=NMAX+1,NBST
!      IF(LTOT(NOST-1).LT.0.0001)THEN
!        ALPHA=0. 
!      ELSE
! rapport des longueurs
!        ALPHA=LTOT(nost)/LTOT(Nost-1)
!      ENDIF      
! POINTS DE DEPART ET DE FIN DE LA ZONE SUR LA SECTION NOST
      NPTDEP=NZONE(NOST,NOZN)
      NPTFIN=NZONE(NOST,NOZN+1)
      NBPTZ=NPTFIN-NPTDEP
! on remplit stix
      DO J=0,NBPTZ
        STIX(NOPTIDEP+J,NOST)=STX(NPTDEP+J,NOST)
        STIY(NOPTIDEP+J,NOST)=STY(NPTDEP+J,NOST)
        STIZ(NOPTIDEP+J,NOST)=STZ(NPTDEP+J,NOST)
      ENDDO

      IF(NBPTZ.EQ.0.OR.LTOT(NOST).LT.0.0001)THEN
! on rajoute nptsti fois le point unique
         DO k=1,NPTSTI
        STIX(NOPTIDEP+K,NOST)=STX(NPTDEP,NOST)
        STIY(NOPTIDEP+K,NOST)=STY(NPTDEP,NOST)
        STIZ(NOPTIDEP+K,NOST)=STZ(NPTDEP,NOST)
         LCUM(K+1,NOST)=0.
         ENDDO 
        nbptz=nptsti        
! si nbptz differente de 0 (cas normal)
       ELSEIF(LTOT(NOST-1).LT.0.0001)THEN 
! on ajoute des points pour qu'ils soient equirepartis
! Boucle sur le nombre de points a rajouter      
      DO K=1,NPTSTI-NBPTZ
        D=0.
        J0=1
! boucle pour determiner ou rajouter le point      
        DO J=1,NBPTZ
          DO I=1,k-1
!         IF(J.NE.JPREC(I))THEN
! longueur du segment j
          Dmoins1=lcum(J+1,nost)-lcum(j,nost)
!     :-alpha*(lcum(J+1,nost-1)-lcum(j,nost-1))
          IF(dmoins1.gt.D)THEN
            d=dmoins1
            j0=j
          endif  
!         ENDIF 
! fin boucle sur i          
        ENDDO
! fin boucle sur j          
        ENDDO
! on rajoute un point en commencant par decaler ceux qui sont d'indice plus haut        
        DO j=NBPTZ+1,J0+1,-1
          STIX(NOPTIDEP+j,NOST)=STIX(NOPTIDEP+j-1,NOST)
          STIY(NOPTIDEP+j,NOST)=STIY(NOPTIDEP+j-1,NOST)
          STIZ(NOPTIDEP+j,NOST)=STIZ(NOPTIDEP+j-1,NOST)
          lcum(j+1,nost)=lcum(j,nost)
        ENDDO  
!
      IF(lcum(J0+1,nost)-lcum(j0,nost).GT.2.*PASM(NOZN))THEN
        STIX(NOPTIDEP+j0,NOST)=0.5*(STIX(NOPTIDEP+j0-1,NOST)
     :+STIX(NOPTIDEP+j0+1,NOST))
        STIY(NOPTIDEP+j0,NOST)=0.5*(STIY(NOPTIDEP+j0-1,NOST)
     :+STIY(NOPTIDEP+j0+1,NOST))
        STIZ(NOPTIDEP+j0,NOST)=0.5*(STIZ(NOPTIDEP+j0-1,NOST)
     :+STIZ(NOPTIDEP+j0+1,NOST))
       lcum(j0+1,nost)=0.5*(lcum(j0,nost)+lcum(j0+2,nost))
! on permet de revenir voir ce segment     
!            jprec(k)=0
! si distance trop petite on double un point
      ELSE
        STIX(NOPTIDEP+j0,NOST)=STIX(NOPTIDEP+j0-1,NOST)
        STIY(NOPTIDEP+j0,NOST)=STIY(NOPTIDEP+j0-1,NOST)
        STIZ(NOPTIDEP+j0,NOST)=STIZ(NOPTIDEP+j0-1,NOST)
       lcum(j0+1,nost)=lcum(j0,nost)
! on ne permet pas de revenir voir ce segment qui est trop court    
!            jprec(k)=j0+1
      ENDIF
! il ya un point de plus
      nbptz=nbptz+1
! Fin boucle sur K      
      ENDDO
! si nbptz differente de 0 (cas normal) et ltot(nost-1) non nulle
       ELSE
! on calcule les ratios sur NOST-1
! ratios cibles pour nost
         DO i=1,NPTSTI
           ALPHA(I)=LCUM(I+1,NOST-1)/LTOT(NOST-1)
         ENDDO
! on calcule les ratios sur nost
         DO i=1,NBPTZ
           BETA(I)=LCUM(I+1,NOST)/LTOT(NOST)
         ENDDO
         DO i=NBPTZ+1,NPTSTI
! on met provisoirement tous ces points en bout de zone
           BETA(i)=1.
          STIX(NOPTIDEP+i,NOST)=STIX(NOPTIDEP+i-1,NOST)
          STIY(NOPTIDEP+i,NOST)=STIY(NOPTIDEP+i-1,NOST)
          STIZ(NOPTIDEP+i,NOST)=STIZ(NOPTIDEP+i-1,NOST)
          lcum(i+1,nost)=lcum(i,nost)
         ENDDO
! M nombre de points ajoutes
!         M=0      
!        M=NPTSTI-NBPTZ
! boucle pour determiner ou rajouter le point      
         DO M2=1,NPTSTI-NBPTZ
           TROUVE=.FALSE.
! on commence � 1 parce que 0 correspond a extremite de section           
        DO J0=1,NBPTZ
          IF(.NOT.TROUVE)THEN
            undeplus=.TRUE.
!         IF(M.LT.NPTSTI-NBPTZ)THEN
!          IF(BETA(J0).GT.ALPHA(J0))THEN
            do J1=J0,NBPTZ
              IF(Undeplus)then
               IF(BETA(J1).LE.ALPHA(J1))THEN
                 undeplus=.FALSE.
               ENDIF
              ENDIF  
! fin boucle sur J1               
            enddo
! deuxieme chance pour rajouter un point            
!            if(.not.undeplus)then
!             if(j0.ne.nbptz)then
!                j1=j0+1
!                if(beta(j0).ne.beta(j1))then
!                if(beta(j0).GT.alpha(j0))then
!                if(beta(j1).GT.alpha(j1))then
!               if(0.5*(beta(j0)+beta(j1)).GT.alpha(j1))then
!                  UNDEPLUS=.TRUE.
!                endif  
!                endif  
!                endif  
!                endif 
! fin if sur j0                
!              endif
! fin if sur not undeplus               
!            endif
            IF(Undeplus)then
             if(j0.eq.nbptz)then
               TROUVE=.TRUE.
             else
              j1=j0+1
              IF(beta(j0)-alpha(j0).GT.alpha(j1)-beta(J0))then
                TROUVE=.TRUE.
              endif
! fin du if sur j0=nbptz              
             endif
! fin if sur undeplus             
            endif
            if(trouve)then 
! on rajoute un point en commencant par decaler ceux qui sont d'indice plus haut        
        DO j=NBPTZ+1,J0+1,-1
          STIX(NOPTIDEP+j,NOST)=STIX(NOPTIDEP+j-1,NOST)
          STIY(NOPTIDEP+j,NOST)=STIY(NOPTIDEP+j-1,NOST)
          STIZ(NOPTIDEP+j,NOST)=STIZ(NOPTIDEP+j-1,NOST)
          lcum(j+1,nost)=lcum(j,nost)
          beta(j)=beta(j-1)
        ENDDO  
! on teste si le point a creer est entre deux points de la section precedente
! 0.00006 car 4 d�cimales dans un fichier st normalement
        do k=0,NPTSTI
          if(abs(stix(noptidep+J0-1,nost)-stix(noptidep+k,nost+1))
     :.lt.0.00006)THEN
          if(abs(stiy(noptidep+J0-1,nost)-stiy(noptidep+k,nost+1))
     :.lt.0.00006)THEN
             do L=0,NPTSTI
          if(abs(stix(noptidep+J0+1,nost)-stix(noptidep+l,nost+1))
     :.lt.0.00006)THEN
          if(abs(stiy(noptidep+J0+1,nost)-stiy(noptidep+l,nost+1))
     :.lt.0.00006)THEN
            if(L.EQ.K)THEN
! on double le point a gauche
        STIX(NOPTIDEP+j0,NOST)=STIX(NOPTIDEP+j0-1,NOST)
        STIY(NOPTIDEP+j0,NOST)=STIY(NOPTIDEP+j0-1,NOST)
        STIZ(NOPTIDEP+j0,NOST)=STIZ(NOPTIDEP+j0-1,NOST)
        lcum(j0+1,nost)=lcum(j0,nost)
        beta(j0)=beta(j0-1)
        trouve=.FALSE.
             else
! dans ce cas on prend le point k+1
        STIX(NOPTIDEP+j0,NOST)=STIX(NOPTIDEP+K+1,NOST+1)
        STIY(NOPTIDEP+j0,NOST)=STIY(NOPTIDEP+k+1,NOST+1)
        STIZ(NOPTIDEP+j0,NOST)=STIZ(NOPTIDEP+k+1,NOST+1)
        if(abs(stix(noptidep+J0+1,nost)
     :-stix(noptidep+J0-1,nost)).LT.0.00006)THEN
        ratio=(stix(noptidep+J0,nost)-stix(noptidep+J0-1,nost))/
     :(stix(noptidep+J0+1,nost)-stix(noptidep+J0-1,nost))
        elseif(abs(stiy(noptidep+J0+1,nost)
     :-stiy(noptidep+J0-l,nost)).LT.0.00006)THEN
        ratio=(stiy(noptidep+J0,nost)-stiy(noptidep+J0-1,nost))/
     :(stiy(noptidep+J0+1,nost)-stiy(noptidep+J0-1,nost))
        endif
       lcum(j0+1,nost)=(1.-ratio)*lcum(j0,nost)+ratio*lcum(j0+2,nost)
       beta(j0)=(1.-ratio)*beta(j0-1)+ratio*beta(J0+1) 
        trouve=.FALSE.
             endif
          endif
          endif
! fin du do sur l
          enddo  
          endif
          endif
! fin du do sur k
          enddo  
           if(TROUVE)then
        if(beta(j0).eq.beta(j0-1))then
! point deja double, on le triple
! en pratique rien a faire car le doublement a droite a deja ete fait
        else        
        ratio=(alpha(j0)-beta(j0-1))/(beta(j0)-beta(j0-1))
        if(ratio.lt.0.)then
            WRITE(9,*)'ratio negatif',nost,noptidep+J0
!            ratio=0.
! on double le point a gauche
        STIX(NOPTIDEP+j0,NOST)=STIX(NOPTIDEP+j0-1,NOST)
        STIY(NOPTIDEP+j0,NOST)=STIY(NOPTIDEP+j0-1,NOST)
        STIZ(NOPTIDEP+j0,NOST)=STIZ(NOPTIDEP+j0-1,NOST)
        lcum(j0+1,nost)=lcum(j0,nost)
        beta(j0)=beta(j0-1)
!         endif   
        elseif(ratio.lt.1.-ratio)then
        IF(ratio*(lcum(J0+1,nost)-lcum(j0,nost)).lT.PASM(NOZN))THEN
! si distance trop petite on double le point a gauche
        STIX(NOPTIDEP+j0,NOST)=STIX(NOPTIDEP+j0-1,NOST)
        STIY(NOPTIDEP+j0,NOST)=STIY(NOPTIDEP+j0-1,NOST)
        STIZ(NOPTIDEP+j0,NOST)=STIZ(NOPTIDEP+j0-1,NOST)
        lcum(j0+1,nost)=lcum(j0,nost)
        beta(j0)=beta(j0-1)
        else
        STIX(NOPTIDEP+j0,NOST)=(1.-ratio)*STIX(NOPTIDEP+j0-1,NOST)
     :+ratio*STIX(NOPTIDEP+j0+1,NOST)
        STIY(NOPTIDEP+j0,NOST)=(1.-ratio)*STIY(NOPTIDEP+j0-1,NOST)
     :+ratio*STIY(NOPTIDEP+j0+1,NOST)
        STIZ(NOPTIDEP+j0,NOST)=(1.-ratio)*STIZ(NOPTIDEP+j0-1,NOST)
     :+ratio*STIZ(NOPTIDEP+j0+1,NOST)
       lcum(j0+1,nost)=(1.-ratio)*lcum(j0,nost)+ratio*lcum(j0+2,nost)
       beta(j0)=(1.-ratio)*beta(j0-1)+ratio*beta(J0+1) 
        endif
        else
!        if(ratio.ge.1.-ratio)then
      IF((1.-ratio)*(lcum(J0+1,nost)-lcum(j0,nost)).LT.PASM(NOZN))THEN
! si distance trop petite on double le point a droite
!        STIX(NOPTIDEP+j0,NOST)=STIX(NOPTIDEP+j0,NOST)
!        STIY(NOPTIDEP+j0,NOST)=STIY(NOPTIDEP+j0,NOST)
!        STIZ(NOPTIDEP+j0,NOST)=STIZ(NOPTIDEP+j0,NOST)
!        lcum(j0+1,nost)=lcum(j0+1,nost)
!        beta(j0)=beta(j0)
        else
        STIX(NOPTIDEP+j0,NOST)=(1.-ratio)*STIX(NOPTIDEP+j0-1,NOST)
     :+ratio*STIX(NOPTIDEP+j0+1,NOST)
        STIY(NOPTIDEP+j0,NOST)=(1.-ratio)*STIY(NOPTIDEP+j0-1,NOST)
     :+ratio*STIY(NOPTIDEP+j0+1,NOST)
        STIZ(NOPTIDEP+j0,NOST)=(1.-ratio)*STIZ(NOPTIDEP+j0-1,NOST)
     :+ratio*STIZ(NOPTIDEP+j0+1,NOST)
       lcum(j0+1,nost)=(1.-ratio)*lcum(j0,nost)+ratio*lcum(j0+2,nost)
       beta(j0)=(1.-ratio)*beta(j0-1)+ratio*beta(J0+1) 
       endif
! fin du if sur ratio       
       endif
! fin du if sur betaj0=betaj0-1
       endif 
! fin du if sur trouve apres controle section precedente
       endif
!
! il ya un point de plus
      nbptz=nbptz+1
! fin du if trouve 
      ENDIF
! fin du if not trouve
      ENDIF
! fin du if sur m
!      ENDIF
! Fin boucle sur J0
      ENDDO
! Fin boucle sur M2
      ENDDO
      IF(NBPTZ.NE.NPTSTI)THEN
         WRITE(9,*)'section ',nost,' zone ',nozn
         WRITE(9,*)NPTSTI-nbptz,' points doubles en fin'
         NBPTZ=NPTSTI
      ENDIF
! fin du if sur nbptz nul
      ENDIF
! fin boucle sur nost        
      ENDDO
! on commence par nmax et on av dabord vers plus et ensuite vers moins
      DO NOST=NMAX-1,1,-1
!      IF(LTOT(NOST+1).LT.0.0001)THEN
!        ALPHA=0. 
!      ELSE
! rapport des longueurs
!        ALPHA=LTOT(nost)/LTOT(Nost+1)
!      ENDIF      
! POINTS DE DEPART ET DE FIN DE LA ZONE SUR LA SECTION NOST
      NPTDEP=NZONE(NOST,NOZN)
      NPTFIN=NZONE(NOST,NOZN+1)
      NBPTZ=NPTFIN-NPTDEP
! on remplit stix
      DO J=0,NBPTZ
        STIX(NOPTIDEP+J,NOST)=STX(NPTDEP+J,NOST)
        STIY(NOPTIDEP+J,NOST)=STY(NPTDEP+J,NOST)
        STIZ(NOPTIDEP+J,NOST)=STZ(NPTDEP+J,NOST)
      ENDDO

      IF(NBPTZ.EQ.0.OR.LTOT(NOST).LT.0.0001)THEN
! on rajoute nptsti fois le point unique
         DO k=1,NPTSTI
        STIX(NOPTIDEP+K,NOST)=STX(NPTDEP,NOST)
        STIY(NOPTIDEP+K,NOST)=STY(NPTDEP,NOST)
        STIZ(NOPTIDEP+K,NOST)=STZ(NPTDEP,NOST)
         LCUM(K+1,NOST)=0.
         ENDDO 
        nbptz=nptsti        
! si nbptz differente de 0 (cas normal)
       ELSEIF(LTOT(NOST+1).LT.0.00006)THEN 
! on ajoute des points pour qu'ils soient equirepartis
! Boucle sur le nombre de points a rajouter      
      DO K=1,NPTSTI-NBPTZ
        D=0.
        J0=1
! boucle pour determiner ou rajouter le point      
        DO J=1,NBPTZ
          DO I=1,k-1
!         IF(J.NE.JPREC(I))THEN
! longueur du segment j
          Dmoins1=lcum(J+1,nost)-lcum(j,nost)
!     :-alpha*(lcum(J+1,nost-1)-lcum(j,nost-1))
          IF(dmoins1.gt.D)THEN
            d=dmoins1
            j0=j
          endif  
!         ENDIF 
! fin boucle sur i          
        ENDDO
! fin boucle sur j          
        ENDDO
! on rajoute un point en commencant par decaler ceux qui sont d'indice plus haut        
        DO j=NBPTZ+1,J0+1,-1
          STIX(NOPTIDEP+j,NOST)=STIX(NOPTIDEP+j-1,NOST)
          STIY(NOPTIDEP+j,NOST)=STIY(NOPTIDEP+j-1,NOST)
          STIZ(NOPTIDEP+j,NOST)=STIZ(NOPTIDEP+j-1,NOST)
          lcum(j+1,nost)=lcum(j,nost)
        ENDDO  
!
      IF(lcum(J0+1,nost)-lcum(j0,nost).GT.2.*PASM(NOZN))THEN
        STIX(NOPTIDEP+j0,NOST)=0.5*(STIX(NOPTIDEP+j0-1,NOST)
     :+STIX(NOPTIDEP+j0+1,NOST))
        STIY(NOPTIDEP+j0,NOST)=0.5*(STIY(NOPTIDEP+j0-1,NOST)
     :+STIY(NOPTIDEP+j0+1,NOST))
        STIZ(NOPTIDEP+j0,NOST)=0.5*(STIZ(NOPTIDEP+j0-1,NOST)
     :+STIZ(NOPTIDEP+j0+1,NOST))
       lcum(j0+1,nost)=0.5*(lcum(j0,nost)+lcum(j0+2,nost))
! on permet de revenir voir ce segment     
!            jprec(k)=0
! si distance trop petite on double un point
      ELSE
        STIX(NOPTIDEP+j0,NOST)=STIX(NOPTIDEP+j0-1,NOST)
        STIY(NOPTIDEP+j0,NOST)=STIY(NOPTIDEP+j0-1,NOST)
        STIZ(NOPTIDEP+j0,NOST)=STIZ(NOPTIDEP+j0-1,NOST)
       lcum(j0+1,nost)=lcum(j0,nost)
! on ne permet pas de revenir voir ce segment qui est trop court    
!            jprec(k)=j0+1
      ENDIF
! il ya un point de plus
      nbptz=nbptz+1
! Fin boucle sur K      
      ENDDO
! si nbptz differente de 0 (cas normal) et ltot(nost-1) non nulle
       ELSE
! on calcule les ratios sur NOST+1
! ratios cibles pour nost
         DO i=1,NPTSTI
           ALPHA(I)=LCUM(I+1,NOST+1)/LTOT(NOST+1)
         ENDDO
! on calcule les ratios sur nost
         DO i=1,NBPTZ
           BETA(I)=LCUM(I+1,NOST)/LTOT(NOST)
         ENDDO
         DO i=NBPTZ+1,NPTSTI
! on met provisoirement tous ces points en bout de zone
           BETA(i)=1.
          STIX(NOPTIDEP+i,NOST)=STIX(NOPTIDEP+i-1,NOST)
          STIY(NOPTIDEP+i,NOST)=STIY(NOPTIDEP+i-1,NOST)
          STIZ(NOPTIDEP+i,NOST)=STIZ(NOPTIDEP+i-1,NOST)
          lcum(i+1,nost)=lcum(i,nost)
         ENDDO
! M nombre de points ajoutes
!         M=0      
!        M=NPTSTI-NBPTZ
! boucle pour determiner ou rajouter le point      
         DO M2=1,NPTSTI-NBPTZ
           TROUVE=.FALSE.
! on commence � 1 parce que 0 correspond a extremite de section           
        DO J0=1,NBPTZ
          IF(.NOT.TROUVE)THEN
            undeplus=.TRUE.
!         IF(M.LT.NPTSTI-NBPTZ)THEN
!          IF(BETA(J0).GT.ALPHA(J0))THEN
            do J1=J0,NBPTZ
              IF(Undeplus)then
               IF(BETA(J1).LE.ALPHA(J1))THEN
                 undeplus=.FALSE.
               ENDIF
              ENDIF  
! fin boucle sur J1               
            enddo
! deuxieme chance pour rajouter un point            
!            if(.not.undeplus)then
!              if(j0.ne.nbptz)then
!                j1=j0+1
!                if(beta(j0).ne.beta(j1))then
!                if(beta(j0).GT.alpha(j0))then
!                if(beta(j1).GT.alpha(j1))then
!                if(0.5*(beta(j0)+beta(j1)).GT.alpha(j1))then
!                  UNDEPLUS=.TRUE.
!                endif  
!                endif  
!                endif 
!                endif
! fin if sur j0                
!              endif
! fin if sur not undeplus               
!            endif
            IF(Undeplus)then
             if(j0.eq.nbptz)then
               TROUVE=.TRUE.
             else
              j1=j0+1
              IF(beta(j0)-alpha(j0).GT.alpha(j1)-beta(J0))then
                TROUVE=.TRUE.
              endif
! fin du if sur j0=nbptz              
             endif
! fin if sur undeplus             
            endif
            if(trouve)then 
! on rajoute un point en commencant par decaler ceux qui sont d'indice plus haut        
        DO j=NBPTZ+1,J0+1,-1
          STIX(NOPTIDEP+j,NOST)=STIX(NOPTIDEP+j-1,NOST)
          STIY(NOPTIDEP+j,NOST)=STIY(NOPTIDEP+j-1,NOST)
          STIZ(NOPTIDEP+j,NOST)=STIZ(NOPTIDEP+j-1,NOST)
          lcum(j+1,nost)=lcum(j,nost)
          beta(j)=beta(j-1)
        ENDDO 
! on teste si le point a creer est entre deux points de la section precedente
! 0.00006 car 4 d�cimales dans un fichier st normalement
        do k=0,NPTSTI
          if(abs(stix(noptidep+J0-1,nost)-stix(noptidep+k,nost-1))
     :.lt.0.00006)THEN
          if(abs(stiy(noptidep+J0-1,nost)-stiy(noptidep+k,nost-1))
     :.lt.0.00006)THEN
             do l=0,NPTSTI
          if(abs(stix(noptidep+J0+1,nost)-stix(noptidep+l,nost-1))
     :.lt.0.00006)THEN
          if(abs(stiy(noptidep+J0+1,nost)-stiy(noptidep+l,nost-1))
     :.lt.0.00006)THEN
            if(L.EQ.K)THEN
! on double le point a gauche
        STIX(NOPTIDEP+j0,NOST)=STIX(NOPTIDEP+j0-1,NOST)
        STIY(NOPTIDEP+j0,NOST)=STIY(NOPTIDEP+j0-1,NOST)
        STIZ(NOPTIDEP+j0,NOST)=STIZ(NOPTIDEP+j0-1,NOST)
        lcum(j0+1,nost)=lcum(j0,nost)
        beta(j0)=beta(j0-1)
        trouve=.FALSE.
             else
! dans ce cas on prend le point k+1
        STIX(NOPTIDEP+j0,NOST)=STIX(NOPTIDEP+K+1,NOST-1)
        STIY(NOPTIDEP+j0,NOST)=STIY(NOPTIDEP+k+1,NOST-1)
        STIZ(NOPTIDEP+j0,NOST)=STIZ(NOPTIDEP+k+1,NOST-1)
        if(abs(stix(noptidep+J0+1,nost)
     :-stix(noptidep+J0-l,nost)).LT.0.00006)THEN
        ratio=(stix(noptidep+J0,nost)-stix(noptidep+J0-1,nost))/
     :(stix(noptidep+J0+1,nost)-stix(noptidep+J0-1,nost))
        elseif(abs(stiy(noptidep+J0+1,nost)
     :-stiy(noptidep+J0-l,nost)).LT.0.00006)THEN
        ratio=(stiy(noptidep+J0,nost)-stiy(noptidep+J0-1,nost))/
     :(stiy(noptidep+J0+1,nost)-stiy(noptidep+J0-1,nost))
        endif
       lcum(j0+1,nost)=(1.-ratio)*lcum(j0,nost)+ratio*lcum(j0+2,nost)
       beta(j0)=(1.-ratio)*beta(j0-1)+ratio*beta(J0+1) 
        trouve=.FALSE.
             endif
          endif
          endif
! fin du do sur l
          enddo  
          endif
          endif
! fin du do sur k
          enddo  
           if(TROUVE)then
          if(beta(j0).eq.beta(j0-1))then
! point deja double, on le triple
! en pratique rien a faire car le doublement a droite a deja ete fait
        else        
        ratio=(alpha(j0)-beta(j0-1))/(beta(j0)-beta(j0-1))
        if(ratio.lt.0.)then
!            ratio=0.
            WRITE(9,*)'ratio negatif',nost,noptidep+J0
!         endif   
! on double le point a gauche
        STIX(NOPTIDEP+j0,NOST)=STIX(NOPTIDEP+j0-1,NOST)
        STIY(NOPTIDEP+j0,NOST)=STIY(NOPTIDEP+j0-1,NOST)
        STIZ(NOPTIDEP+j0,NOST)=STIZ(NOPTIDEP+j0-1,NOST)
        lcum(j0+1,nost)=lcum(j0,nost)
        beta(j0)=beta(j0-1)
        elseif(ratio.lt.1.-ratio)then
        IF(ratio*(lcum(J0+1,nost)-lcum(j0,nost)).lT.PASM(NOZN))THEN
! si distance trop petite on double le point a gauche
        STIX(NOPTIDEP+j0,NOST)=STIX(NOPTIDEP+j0-1,NOST)
        STIY(NOPTIDEP+j0,NOST)=STIY(NOPTIDEP+j0-1,NOST)
        STIZ(NOPTIDEP+j0,NOST)=STIZ(NOPTIDEP+j0-1,NOST)
        lcum(j0+1,nost)=lcum(j0,nost)
        beta(j0)=beta(j0-1)
        else
        STIX(NOPTIDEP+j0,NOST)=(1.-ratio)*STIX(NOPTIDEP+j0-1,NOST)
     :+ratio*STIX(NOPTIDEP+j0+1,NOST)
        STIY(NOPTIDEP+j0,NOST)=(1.-ratio)*STIY(NOPTIDEP+j0-1,NOST)
     :+ratio*STIY(NOPTIDEP+j0+1,NOST)
        STIZ(NOPTIDEP+j0,NOST)=(1.-ratio)*STIZ(NOPTIDEP+j0-1,NOST)
     :+ratio*STIZ(NOPTIDEP+j0+1,NOST)
       lcum(j0+1,nost)=(1.-ratio)*lcum(j0,nost)+ratio*lcum(j0+2,nost)
       beta(j0)=(1.-ratio)*beta(j0-1)+ratio*beta(J0+1) 
        endif
        else
!        if(ratio.ge.1.-ratio)then
      IF((1.-ratio)*(lcum(J0+1,nost)-lcum(j0,nost)).LT.PASM(NOZN))THEN
! si distance trop petite on double le point a droite
!        STIX(NOPTIDEP+j0,NOST)=STIX(NOPTIDEP+j0,NOST)
!        STIY(NOPTIDEP+j0,NOST)=STIY(NOPTIDEP+j0,NOST)
!        STIZ(NOPTIDEP+j0,NOST)=STIZ(NOPTIDEP+j0,NOST)
!        lcum(j0+1,nost)=lcum(j0+1,nost)
!        beta(j0)=beta(j0)
        else
        STIX(NOPTIDEP+j0,NOST)=(1.-ratio)*STIX(NOPTIDEP+j0-1,NOST)
     :+ratio*STIX(NOPTIDEP+j0+1,NOST)
        STIY(NOPTIDEP+j0,NOST)=(1.-ratio)*STIY(NOPTIDEP+j0-1,NOST)
     :+ratio*STIY(NOPTIDEP+j0+1,NOST)
        STIZ(NOPTIDEP+j0,NOST)=(1.-ratio)*STIZ(NOPTIDEP+j0-1,NOST)
     :+ratio*STIZ(NOPTIDEP+j0+1,NOST)
       lcum(j0+1,nost)=(1.-ratio)*lcum(j0,nost)+ratio*lcum(j0+2,nost)
       beta(j0)=(1.-ratio)*beta(j0-1)+ratio*beta(J0+1) 
       endif
! fin du if sur ratio       
       endif
! fin du if sur betaj0=betaj0-1
       endif 
! fin du if sur trouve apres test sur section precedente
       ENDIF  
!
! il ya un point de plus
      nbptz=nbptz+1
! fin du if trouve 
      ENDIF
! fin du if not trouve
      ENDIF
! fin du if sur m
!      ENDIF
! Fin boucle sur J0
      ENDDO
! Fin boucle sur M2
      ENDDO
      IF(NBPTZ.NE.NPTSTI)THEN
         WRITE(9,*)'section ',nost,' zone ',nozn
         WRITE(9,*)NPTSTI-nbptz,' points doubles en fin'
         NBPTZ=NPTSTI
      ENDIF
! fin du if sur nbptz nul
      ENDIF
! fin boucle sur nost        
      ENDDO
      NBPTZN(NOZN)=NPTSTI 
! POSITION DES LIGNES DIRECTRICE DANS LE MAILLAGE:
      PTLGN(NOZN)=PTLGN(NOZN-1)+NBPTZN(NOZN) 
!
! fin du if sur compl 
      ENDIF
! 
! LE PROCHAIN POINT DES ST INTER EST NPTSTI PLUS LOIN:
      NOPTIDEP=NOPTIDEP+NPTSTI 
      RETURN
      END     
! *******************************************************************************
         SUBROUTINE APPARTSEG(X1,Y1,X2,Y2,X3,Y3,APPART)
! regarde si le point 3 est sur le segment de 1 � 2
! *******************************************************************************
          DOUBLE PRECISION X1,Y1,X2,Y2,X3,Y3,CAR,DIST,ALP
          LOGICAL APPART

          car=(X1-X2)**2+(Y1-Y2)**2
          if(car.gt.0.0000001)then
            ALP=(X1-X2)*(X3-X1)+(Y1-Y2)*(Y3-Y1)
            ALP=-ALP/CAR
            IF(ALP.GT.0.00001.AND.ALP.LT.0.99999)THEN
              DIST=(X3-X1-ALP*(X2-X1))**2+(Y3-Y1-ALP*(Y2-Y1))**2
!          DIST=SQRT(DIST)
              IF(DIST.LT.0.00000001)THEN
                APPART=.TRUE. 
              ENDIF
            ENDIF
! si les deux points sont confondus
          ELSE
            car=(X1-X3)**2+(Y1-Y3)**2
            if(car.lt.0.00000001)then
              APPART=.TRUE. 
            ENDIF
          ENDIF
          RETURN
          END
! *******************************************************************************
      SUBROUTINE CALSECTL(DPLCTOT,LPLAN,LINEAIRE,AJUST,tmoy)
! en fonction du pas d'espace longitudinal calcule les sections intermediaires
! *******************************************************************************
      implicit none
      INTEGER NOST,NBPTSTI,NBST,NBSTF
     :,NBSTI,NBPTST
     :,NOPTI,LGN1,LGN2,LINZ,KSTPLAN
     :,KS1,KP1,KS2,KP2,KP3
      DOUBLE PRECISION ZPL,YPL,Y1,Y2,XPL,X1,X2,Z1,Z2,TMOY
     :,DT2,D2C,D1,D2,D,D1C,DPL,DT1,DPLC,DPLCTOT,DT0
      CHARACTER CLIGN*4,CPLAN*1
! variable pour choisir maillage en plan ou en 3D     
      LOGICAL LPLAN
! variable pour interpolation lineaire ou par spline
      LOGICAL LINEAIRE      
!
! NB ST MAX EN ENTREE
      PARAMETER (KS1=1500)
! NB DE POINT PAR ST MAX EN ENTREE
      PARAMETER (KP1=1000)
! NB ST MAX DANS BD INTERMEDIAIRE
      PARAMETER (KS2=1500)
! NB PT PAR ST MAX DANS BD INTERMEDIAIRE
      PARAMETER (KP2=10500)
! NB PT PAR ST MAX DANS BD FINALE (POST OPTIMIZ)
      PARAMETER (KP3=1000)
      DOUBLE PRECISION  STX(KP1,KS1),STY(KP1,KS1),STZ(KP1,KS1),
     +  STIX(KP2,KS1),STIY(KP2,KS1),STIZ(KP2,KS1),
     + STFX(KP2,KS2),STFY(KP2,KS2),
     + STFZ(KP2,KS2),PK,PKF
      CHARACTER  CHAR10*10,CHAR4*4,charajust*1
! pour choisir maximum de section ou minimum
      integer ILONGMI  
! variable pour corriger section sur bords      
      logical ajust
!
! BD DIM DES STRUCTURES:
! nbst   : nombre de st du fichier de depart  (<KS1)
! nbptsti: nombre de point des st du maillage (<KP2 PUIS KP3)
! nbstf  : nombre de st du maillage           (<KS2)
!
      COMMON/BDDIM/NBST,NBPTSTI,NBSTF
!
! BD ST  (ORIGINE FICHIER de depart)
!  NBPTST()*NBST -> COTE X Y Z DU POINT 
      COMMON/BDST/NBPTST(KS1),STX,STY,STZ

! BD ENTETE DES SECTIONS
! BD NOM DES SECTIONS ORIGINE
       CHARACTER*12 NOMST
       COMMON /BDNOMST/NOMST(KS1)
! PK: FICHIER ORIGINE, PKF: PK MAILLAGE
      COMMON/BDENTETE/PK(KS1),PKF(KS2)
!
! BD FLAG POUR ST MESUREES PLANES:
      COMMON /BDSTPLAN/ KSTPLAN(KS1)
!                          
! BD ST INTERMEDIAIRE (ST de points equidistants SELON ABCISSE CURVILIGNE )
!                      INTERPOL LIN ENTRE PTS D'UNE MEME SECTION
! NBPT-DE-LA-ZONE * NB-SECTIONS
!   NBPTSTI*NBST  
       COMMON/STI/STIX,STIY,STIZ
!
! BD ST  FINALE  AVANT OPTIMISATION NBPTSTI*NBSTF          
! NBSTI nb des section inter entre section de depart
      COMMON/BDSTF/ NBSTI(KS2),STFX,STFY,
     + STFZ
!
      INTEGER NLDAXE
      DOUBLE PRECISION H2,H31,H32
      COMMON/HISTO/H2,H31,H32,LINZ,NLDAXE   
! PROFIL EN LONG:
      INTEGER NOPTLM(KS2)
      INTEGER N1(KS2),N2(KS2),j,jmax
! 
      COMMON/numerodir/NOPTLM

      WRITE(9,*)'donnez le nombre de troncons de pas longitudinaux'
      WRITE(9,*)'(defaut =1)'
      char4 = '    ' !read(*,'(A)')char4
      if(char4.NE.'    ')then
        read(char4,'(I4)')jmax
      else
        jmax=1
      endif
      n1(1)=1
      n2(JMAX)=NBST-1
      if(jmax.NE.1)then
        do nost=1,nbst
           WRITE(9,*)'section ',nost,' pk=',pk(nost),' ',nomst(nost)
        enddo
        WRITE(9,*)'donnez successivement dans l''ordre le numero '
        WRITE(9,*)'des ',jmax-1,' sections delimitant les troncons' 
        do j=2,jmax 
           read(*,*)n1(j)
           if(n1(2).EQ.1)THEN
             read(*,*)n1(2)
           endif 
           n2(j-1)=n1(j)-1
        enddo 
! fin du if sur jmax=1
      ENDIF
      NBSTF=1
      DPLCTOT=0.
      DO J=1,JMAX 
! STATISTIQUE SUR LA LONGUEUR
      IF(JMAX.GT.1)THEN
         WRITE(9,*)'TRONCON ',J
      ENDIF
      WRITE(9,*) 'CALCULS DES LONGUEURS'
      D1C=0.
      D2C=0.
      DPLC=0.
!    DC=0
      DO 400 NOST=n1(J),n2(j)
    
        X1=STIX(1,NOST)-STIX(1,NOST+1)
        X2=STIX(NBPTSTI,NOST)-STIX(NBPTSTI,NOST+1)
! modif du 22 01 07 : on utilise stx et pas stix
        XPL=STX(NOPTLM(NOST),NOST)-STX(NOPTLM(NOST+1),NOST+1)
        Y1=STIY(1,NOST)-STIY(1,NOST+1)
        Y2=STIY(NBPTSTI,NOST)-STIY(NBPTSTI,NOST+1)
        YPL=STY(NOPTLM(NOST),NOST)-STY(NOPTLM(NOST+1),NOST+1)
        IF(LPLAN)THEN
          D1=SQRT(X1*X1+Y1*Y1)
          D2=SQRT(X2*X2+Y2*Y2)
          DPL=SQRT(XPL*XPL+YPL*YPL)
        ELSE
          Z1=STIZ(1,NOST)-STIZ(1,NOST+1)
          Z2=STIZ(NBPTSTI,NOST)-STIZ(NBPTSTI,NOST+1)
          ZPL=STZ(NOPTLM(NOST),NOST)-STZ(NOPTLM(NOST+1),NOST+1)
          D1=SQRT(X1*X1+Y1*Y1+Z1*Z1)
          D2=SQRT(X2*X2+Y2*Y2+Z2*Z2)
          DPL=SQRT(XPL*XPL+YPL*YPL+ZPL*ZPL)
        ENDIF
!
        D1C=D1C+D1
        D2C=D2C+D2
        DPLC=DPLC+DPL
 400  CONTINUE
      DPLCTOT=DPLCTOT+DPLC
!
      WRITE(9,*) 'LONGUEURS: '
      WRITE(9,*) 'BORD DE MAILLAGE 1:', D1C
      WRITE(9,*) 'BORD DE MAILLAGE 2:', D2C
      WRITE(9,*) 'MOYENNE:', (D1C+D2C)/2.
      WRITE(9,*) 'PROFIL EN LONG SUR LIGNE DIRECTRICE:', DPLC
!
      WRITE(9,*) 'ENTREZ PAS ESPACE LONGITUDINAL MOYEN:'
      WRITE(9,*)'(defaut = 1000000 m)'
      !READ(*,'(A10)')char10
      !READ(CHAR10,'(F10.0)')TMOY
      IF(TMOY.EQ.0.)TMOY=999999.9
      IF(TMOY.LT.999999.9)THEN
        WRITE(9,*) 
     &         'le nombre de points sera calcule a partir des longueurs'
        WRITE(9,*)
     &         'des segments entre sections sur 2 DES LIGNES SUIVANTES:'
        WRITE(9,*)
     &       'DIRECTRICE choisie precedemmment(0), BORD 1(1), BORD 2(2)'
        WRITE(9,*) ' '
        WRITE(9,*) 'pour choisir une ligne seulement'
        WRITE(9,*) 'vous pouvez entrer deux fois le meme chiffre '
        WRITE(9,*) ' '
        WRITE(9,*) 'entrez vos deux chiffres successivement '
        WRITE(9,*)'(defaut = (0,0) )'
        clign = '1' !READ(*,'(A)')Clign
        IF(Clign.NE.'    ')THEN
          read(Clign,'(I4)')lgn1
        ELSE
          lgn1=0
        ENDIF
        clign = '2' !READ(*,'(A)')Clign
        IF(Clign.NE.'    ')THEN
          read(Clign,'(I4)')lgn2
        ELSE
          lgn2=0
        ENDIF
!       READ*,LGN1,LGN2
        If(lgn1.ne.lgn2)then
          WRITE(9,*) 'pour plus de sections  entrez 1'
          WRITE(9,*)
     &             'le calcul se fera sur la ligne de longueur maximale'
          WRITE(9,*) 'pour moins de sections  entrez 2'
          WRITE(9,*) 
     &             'le calcul se fera sur la ligne de longueur minimale'
          WRITE(9,*) 'sinon entrez 0 pour prendre la moyenne'
          cplan = '2' !READ(*,'(A)')CPLAN
          IF(CPLAN.NE.' ')THEN
            read(CPLAN,'(I1)')ilongmi
          ELSE
            ilongmi=0
          ENDIF
        else
          ilongmi=1
        endif 
      ELSE
! tmoy trop grand
! cas o� on n'interpole pas de sections       
!         DISMIN=0.00001
         ilongmi=1
         lgn1=0
         lgn2=0 
       ENDIF  
!    IF (TYPE.NE.0.AND.TYPE.NE.1.AND.TYPE.NE.2) GOTO 403
! 
! MISE EN BOITE POUR HISTORIQUE
      H2=TMOY
      H31=LGN1
      H32=LGN2
!
!       WRITE(9,*) 'DISTANCES ENTRE LES SECTIONS:'
      DO 410 NOST=N1(J),N2(J)
! DISTANCE SUR LA  DIRECTRICE
! modif du 22 01 07 : on utilise stx et pas stix
      XPL=STX(NOPTLM(NOST),NOST)-STX(NOPTLM(NOST+1),NOST+1)
      YPL=STY(NOPTLM(NOST),NOST)-STY(NOPTLM(NOST+1),NOST+1)
      IF(LPLAN)THEN
      DT0=SQRT(XPL*XPL+YPL*YPL)
      ELSE
      ZPL=STZ(NOPTLM(NOST),NOST)-STZ(NOPTLM(NOST+1),NOST+1)
      DT0=SQRT(XPL*XPL+YPL*YPL+ZPL*ZPL)
      ENDIF
! DISTANCES SUR LES BORDS
      X1=STIX(1,NOST)-STIX(1,NOST+1)
      X2=STIX(NBPTSTI,NOST)-STIX(NBPTSTI,NOST+1)
      Y1=STIY(1,NOST)-STIY(1,NOST+1)
      Y2=STIY(NBPTSTI,NOST)-STIY(NBPTSTI,NOST+1)
      IF(LPLAN)THEN
      DT1=SQRT(X1*X1+Y1*Y1)
      DT2=SQRT(X2*X2+Y2*Y2)
      ELSE
      Z1=STIZ(1,NOST)-STIZ(1,NOST+1)
      Z2=STIZ(NBPTSTI,NOST)-STIZ(NBPTSTI,NOST+1)
      DT1=SQRT(X1*X1+Y1*Y1+Z1*Z1)
      DT2=SQRT(X2*X2+Y2*Y2+Z2*Z2)
      ENDIF
!
          IF(LGN1.EQ.0) D1=DT0
          IF(LGN1.EQ.1) D1=DT1
          IF(LGN1.EQ.2) D1=DT2

          IF(LGN2.EQ.0) D2=DT0
          IF(LGN2.EQ.1) D2=DT1
          IF(LGN2.EQ.2) D2=DT2
!
         If (ilongmi.eq.1)then
            D=MAX(D1,D2)
         elseIf(ilongmi.eq.2)then
            D=MIN(D1,D2)
         else   
            D=0.5*(D1+D2)
         endif
!
!
! CALCUL DU NB DE SECTIONS INTERMEDIARES : 
! coorrection du 31 08 07 car nombre de sections et pas d'intervalles
           NBSTI(NOST)=NINT(D/TMOY)-1
!           NBSTI(NOST)=INT(D/TMOY)-1
           IF(NBSTI(NOST).LT.0)NBSTI(NOST)=0
! AMELIORATION A FAIRE: CALCULER TOUTES DES DISTANCES SUR TOUS LES 
! POINTS ET VERIFIER QU AVEC LE NOMBRE DE POINTS, ON EST DESSOUS UN MAX
!      WRITE(9,*) 'SECTIONS ',NOST,'-',NOST+1
!      WRITE(9,*) 'NB SECTIONS:',NBSTI(NOST),' D',D,' T MOY',
!     +   D/(NBSTI(NOST)+1)
!         WRITE(9,*) 'D1 ',D1,' D2',D2,' D MOY ',D
!
 410    CONTINUE
!
! CALCULS DES ST INTERMEDIARES:
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
      DO NOST=N1(J), N2(J)
!
! RECOPI DE TOUTES lES STI COUR DANS STF
          DO NOPTI=1, NBPTSTI
        STFX(NOPTI,NBSTF)=STIX(NOPTI,NOST)
        STFY(NOPTI,NBSTF)=STIY(NOPTI,NOST)
        STFZ(NOPTI,NBSTF)=STIZ(NOPTI,NOST)
! fin du do sur nopti        
         ENDDO
         NBSTF=NBSTF+NBSTI(NOST)+1   
      IF(NBSTF.GT.KS2) THEN 
        WRITE(9,*) ' NOMBRE DE SECTIONS FINALES TROP GRAND >',KS2
        STOP
      ENDIF
! fin du do sur nost
      ENDDO
! fin boucle sur J nombre de troncons
      ENDDO
! derniere section    
        NOST=NBST   
          DO NOPTI=1, NBPTSTI
        STFX(NOPTI,NBSTF)=STIX(NOPTI,NOST)
        STFY(NOPTI,NBSTF)=STIY(NOPTI,NOST)
        STFZ(NOPTI,NBSTF)=STIZ(NOPTI,NOST)
! fin du do sur nopti        
         ENDDO
!      IF(NBSTF.GT.KS2) THEN 
!        WRITE(9,*) ' NOMBRE DE SECTIONS FINALES TROP GRAND >',KS2
!        STOP
!      ENDIF
 
! pour toutes les st de depart:
      NBSTF=0
      DO 500 NOST=1, NBST-1
!
! RECOPI DE LA STI COUR DANS STF
      NBSTF=NBSTF+1
      WRITE(9,*) 'TRAITEMENT SECTION ',NOST,' ->',NBSTF
! si on demande on fait une interpolation longitudinale lineaire et pas splin
      IF(LINEAIRE)THEN 
         CALL CALC_ST_LINEAIRE(NOST)
      ELSE    
! ESTIMATION DES PARAMETRES DE LINTERPOLATEUR T C B
        CALL ESTIM_TCB(NOST)
!
! CALCULS DE VECTEUR DES DERIVEES
        CALL CALC_DERIV(NOST,LINZ)
!
! CALCULS DES ST INTERMEDIAIRES SELON PLANITUDE DES SECTIONS...
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
        IF (KSTPLAN(NOST).EQ.1.AND.KSTPLAN(NOST+1).EQ.1) THEN 
          CALL CALC_ST_PLANE(NOST)
        ELSE
          CALL CALC_ST_TORDU(NOST)
        ENDIF
! fin du if sur lplan
      ENDIF        
!
! ST SUIVANTE
 500    CONTINUE
      RETURN
      END 
      
      
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC      
      SUBROUTINE AJUST_bords(charajust)
! modifie sections pour que les extremites correspondent aux traces fournies
! homothetie, translation et rotation
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC  
      implicit none
      logical trace
      character*35 ficin4,ficin5
      character*1 charajust
      integer nbst,nbptsti,nbstf,nf4,nf5,kp,kzo,KS2,KP2,ks1,kp3
      PARAMETER (KS2=1500,KS1=1500)
      PARAMETER (KP2=10500,KP3=1000)
      parameter (kp=10500,KZO=1000)
	   logical opt(0:kzo)
      double precision xnlg(0:kzo,kp),ynlg(0:kzo,kp),znlg(0:kzo,kp)
      double precision xld(kp),yld(kp),zld(kp)
      double precision xnld(kp),ynld(kp),znld(kp)
      double precision xlG2(kp),ylg2(kp),zlg2(kp)
!      double precision xnlg2(kp),ynlg2(kp),znlg2(kp)
      double precision xaxe(kp),yaxe(kp),zaxe(kp)
      double precision xperpen(kp),yperpen(kp),zperpen(kp)
	   double precision x,y,z
      INTEGER I,NL,M,NOST,NBLGN,NBPTZN,N1
     :,LPC,nzone,nblgno,nsto,nsti,nbsti,nlg2,nld
      DOUBLE PRECISION  STFX(KP2,KS2),STFY(KP2,KS2),
     + STFZ(KP2,KS2)
      double precision stfox(kp3,ks2),stfoy(kp3,ks2),stfoz(kp3,ks2)
      COMMON/BDSTFO/ STFOX,STFOY,
     + STFOZ
      
      CHARACTER CODLGN*3
      COMMON /BDZO/NZONE(KS1,KZO),NBLGNO,LPC(KZO),CODLGN(KZO)
      COMMON/BDDIM/NBST,NBPTSTI,NBSTF
      COMMON/NBDLGN/NBLGN,NBPTZN(KZO)
      COMMON/BDSTF/ NBSTI(KS2),STFX,STFY,
     + STFZ
      double precision xlg(0:kzo,kp),ylg(0:kzo,kp),zlg(0:kzo,kp)
      INTEGER NLG(0:KZO)
      common/xyzlg/xlg,ylg,zlg
	   common/nlg/nlg
      INTEGER PTLGN
      DOUBLE PRECISION PASM 
      COMMON/BDLGN/PTLGN(0:KZO+1),PASM(KZO)
      
      nf4=14
      nf5=15
!      OPEN(28,FILE='sortie',STATUS='UNKNOWN')
!      write(28,*)'nblgn',nblgn
! lecture des lignes directrices a suivre      
 225  WRITE(9,226)
 226  FORMAT(//,' NOM DU FICHIER DE limite gauche: ',$)
      WRITE(9,*)'attention: au format sem'
      WRITE(9,*)'entrez un blanc si pas de limite gauche'
      READ(5,'(A35)') FICIN4
      IF(FICIN4(1:1).ne.' ')then
!       ficin4='limiteg.sem'
      
      OPEN(NF4,FILE=FICIN4,STATUS='OLD',ERR=225)
      opt(0)=.TRUE.
      do i=1,KP
        read(nf4,'(3f13.4)',end=700)xlg(0,i),ylg(0,i),zlg(0,i)
      enddo  
 700  nlg(0)=i-1
! if sur fichier existe
      else
      opt(0)=.false.
! pour le cas des deux bords (charajust=2) on cree un bord
!	 pas de ligne utilisateur donc on prend les points existants
			 NL=0
             N1=PTLGN(NL)
			 nlg(nl)=nbstf
		     DO M=1,NBSTF
               XLG(NL,M)=STFOX(N1,M)
			   YLG(NL,M)=STFOY(N1,M)
			 ENDDO
! fin du if sur fichier existe			 
      endif
 325   WRITE(9,326)
 326   FORMAT(//,' NOM DU FICHIER DE limite droite: ',$)
      WRITE(9,*)'attention: au format sem'
       WRITE(9,*)'entrez un blanc si pas de limite droite'
      READ(5,'(A35)') FICIN5
      IF(FICIN5(1:1).ne.' ')then
      OPEN(NF5,FILE=FICIN5,STATUS='OLD',ERR=325)
      opt(nblgn+1)=.TRUE.
      do i=1,KP
        read(nf5,'(3f13.4)',end=800)
     :xlg(nblgn+1,i),ylg(nblgn+1,i),zlg(nblgn+1,i)
      enddo  
 800  nlg(nblgn+1)=i-1
      else
! si fichier n'existe pas	  
      opt(nblgn+1)=.false.
! pour le cas des deux bords  (charajust=2) on cree un bord
!	 pas de ligne utilisateur donc on prend les points existants
			 NL=NBLGN+1
             N1=PTLGN(NL)
			 nlg(nl)=nbstf
		     DO M=1,NBSTF
               XLG(NL,M)=STFOX(N1,M)
			   YLG(NL,M)=STFOY(N1,M)
			 ENDDO
! FIN DU IF SUR OPT
           ENDIF
      close(nf4)
      close(nf5)
	  if(charajust.eq.'1')then
	  DO NL=1,NBLGN
 525   WRITE(9,526)
 526   FORMAT(//,' NOM DU FICHIER DE LIGNE ',$)
      WRITE(9,*)'NUMERO ',NL,' NOM: ',CODLGN(NL+1) 
      WRITE(9,*)'ATTENTION: AU FORMAT SEM'
       WRITE(9,*)'ENTREZ UN BLANC SI PAS DE LIGNE'
      READ(5,'(A35)') FICIN5
      IF(FICIN5(1:1).NE.' ')THEN
      OPEN(NF5,FILE=FICIN5,STATUS='OLD',ERR=525)
      OPT(NL)=.TRUE.
      DO I=1,KP
        READ(NF5,'(3F13.4)',END=900)
     :XLG(NL,I),YLG(NL,I),ZLG(NL,I)
      ENDDO  
 900  NLG(NL)=I-1
      ELSE
      OPT(NL)=.FALSE.
      ENDIF
      CLOSE(NF5)
! FIN BOUCLE SUR NL
      ENDDO
        
! SOIT ON A LA LIGNE SOIT ELLE EST FORMEE PAR LES POINTS DES SECTIONS
      DO NL=0, NBLGN+1
		 IF(OPT(NL))THEN
! PREMIERE ET DERNIERE SECTIONS EXCLUES AINSI QUE SECTIONS BASE 
             M=1
             N1=PTLGN(NL)
!	   WRITE(9,*)'N1NL',N1,NL,NBPTZN(NL)
             XNLG(NL,M)=STFOX(N1,M)
			 YNLG(NL,M)=STFOY(N1,M)
             M=NBSTF
             N1=PTLGN(NL)
             XNLG(NL,M)=STFOX(N1,M)
			 YNLG(NL,M)=STFOY(N1,M)
             NSTO=1
	         NSTI=0
      DO M=2,NBSTF-1
	     IF(NSTI.EQ.NBSTI(NSTO))THEN
! si fin de zone entre sections donc section de base		 
             NSTO=NSTO+1
             NSTI=0
             N1=PTLGN(NL)
             XNLG(NL,M)=STFOX(N1,M)
			 YNLG(NL,M)=STFOY(N1,M)
         ELSE
! si point entre sections de base		 
!		  WRITE(28,*)'M',M
		   NSTI=NSTI+1
! COUPURE DE CHAQUE LIGNE EN NBSTI(nsto) MORCEAUX DE LONGUEUR EGALE
           CALL DISTGV2(X,Y,Z,M,NSTI,NL,NBSTI(NSTO))
           XNLG(NL,M)=X
           YNLG(NL,M)=Y
           ZNLG(NL,M)=Z
		 ENDIF
! FIN BOUCLE SUR SECTION
      ENDDO
! ELSE DU IF SUR OPT
           ELSE
!	 pas de ligne utilisateur donc on prend les points existants
             N1=PTLGN(NL)
		     DO M=1,NBSTF
               XNLG(NL,M)=STFOX(N1,M)
			   YNLG(NL,M)=STFOY(N1,M)
			 ENDDO
! FIN DU IF SUR OPT
           ENDIF
! FIN BOUCLE SUR NL
         ENDDO
! CORRECTION ZONE PAR ZONE SECTION PAR SECTION
      DO NL=1,NBLGN+1
! zone 1 entre ligne 0 et 1	  
! PREMIERE ET DERNIERE SECTIONS EXCLUES AINSI QUE SECTIONS BASE     NSTO=1
	  NSTI=0
      NSTO=1
      DO NOST=2,NBSTF-1
	     IF(NSTI.EQ.NBSTI(NSTO))THEN
            NSTO=NSTO+1
            NSTI=0
         ELSE
		    NSTI=NSTI+1
!			write(28,*)'nost corrige',nost
            CALL CORRECT2(NOST,NL,XNLG,ynlg)
		 ENDIF
! FIN BOUCLE SUR SECTION
      ENDDO
! FIN BOUCLE SUR ZONE
      ENDDO
! if sur charajust : cas de correction globale uniquement par bords  (charajust=2)
      else
! on renomme les variables
       do I=1,nlg(0)
        xlg2(i)=xlg(0,i)
        ylg2(i)=ylg(0,i)
        zlg2(i)=zlg(0,i)
	   enddo
	   nlg2=nlg(0)
	   do i=1,nlg(nblgn+1)
        xld(i)=xlg(nblgn+1,i)
        yld(i)=ylg(nblgn+1,i)
        zld(i)=zlg(nblgn+1,i)
	   enddo
	   nld=nlg(nblgn+1)
! soit on a les traces des sections soit on les cree
      trace=.false.
      if(nlg2.eq.nld)then
         if(nlg2.eq.nbstf)then
! on suppose que les points sont les traces des sections
            trace=.true.
        endif
      endif
      if(.not.trace)then
! coupure de chaque ligne en nbstf-1 morceaux de longueur egale
      do m=1,nbstf
        call  distgv(x,y,z,m,xlg2,ylg2,zlg2,nlg2,nbstf)
        xnlg(0,m)=x
        ynlg(0,m)=y
!        znlg(0,m)=z
        call  distgv(x,y,z,m,xld,yld,zld,nld,nbstf)
        xnld(m)=x
        ynld(m)=y
!        znld(m)=z
      enddo
! etape supplementaire du 30/04/12
! on definit un axe et on suppose les sections perpendiculaires a cet axe
! axe est forme du milieu des points de meme numero
      do m=1,nbstf
        xaxe(m)=0.5*(xnlg(0,m)+xnld(m))
        yaxe(m)=0.5*(ynlg(0,m)+ynld(m))
!        zaxe(m)=0.5*(znlg(0,m)+znld(m))
      enddo
! on definit en chaque point de l'axe, la perpendiculiare en ce point
      do m=2,nbstf-1
        xperpen(m)=yaxe(m+1)-yaxe(m-1)
        yperpen(m)=-(xaxe(m+1)-xaxe(m-1))
!        zaxe(m)=0.5*(znlg(m)+znld(m))
      enddo
	  m=1
        xperpen(m)=yaxe(m+1)-yaxe(m)
        yperpen(m)=-(xaxe(m+1)-xaxe(m))
		m=nbstf
        xperpen(m)=yaxe(m)-yaxe(m-1)
        yperpen(m)=-(xaxe(m)-xaxe(m-1))
! on cree un deuxieme point pour definir l'axe de la section
      do m=1,nbstf
        xperpen(m)=xperpen(m)+xaxe(m)
        yperpen(m)=yperpen(m)+yaxe(m)
      enddo

! on retient les points ou cette perpendiculaire coupe les limites
      do m=1,nbstf
        call croistgv(xnlg(0,m),ynlg(0,m),znlg(0,m),xaxe(m),yaxe(m)
     :,xperpen(m),yperpen(m),xlg2,ylg2,zlg2,nlg2)   
        call croistgv(xnld(m),ynld(m),znld(m),xaxe(m),yaxe(m)
     :,xperpen(m),yperpen(m),xld,yld,zld,nld)   
      enddo

! ecriture des fichiers resultats au format sem
      ficin4='limitegnew.sem'
      OPEN(NF4,FILE=FICIN4,STATUS='UNKNOWN')
      ficin5='limitednew.sem'
      OPEN(NF5,FILE=FICIN5,STATUS='UNKNOWN')
      do i=1,nbstf
        xlg2(i)=xnlg(0,i)
        ylg2(i)=ynlg(0,i)
        zlg2(i)=znlg(0,i)
        write(nf4,'(3F13.4)')xlg2(i),ylg2(i),zlg2(i)
      enddo  
      do i=1,nbstf
        xld(i)=xnld(i)
        yld(i)=ynld(i)
        zld(i)=znld(i)
        write(nf5,'(3F13.4)')xld(i),yld(i),zld(i)
      enddo  
      close(nf4)
      close(nf5)
! fin du if sur les traces definies ou pas
      endif
! correction section par section
! version b: premiere et derniere sections exclues car mauvais resultats
      do nost=2,nbstf-1
         call correct(nost,xlg2(nost),ylg2(nost),zlg2(nost)
     :,xld(nost),yld(nost),zld(nost))
! fin boucle sur section
      enddo
! fin du if sur charajust
      endif
      RETURN
      END
      
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC      
      Subroutine correct(nost,x1,y1,z1,x2,y2,z2)
! rotation, translation et homothetie sur une section      
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      integer nost,i,nbst,nbstf,nbptsti,kp3,ks2
      double precision x1,y1,x2,y2,z1,z2,sinangle,cosangle
      double precision xli,xlf,raphom,delx,dely,delx2,dely2
! NB ST MAX DANS BD INTERMEDIAIRE
      PARAMETER (KS2=1500)
! NB PT PAR ST MAX DANS BD FINALE (POST OPTIMIZ)
      PARAMETER (KP3=1000)
      double precision stfox(kp3,ks2),stfoy(kp3,ks2),stfoz(kp3,ks2)
      COMMON/BDSTFO/ STFOX,STFOY,
     + STFOZ
      COMMON/BDDIM/NBST,NBPTSTI,NBSTF
      
! calcul rapport homothetie
! longueur initiale
       xli=sqrt((stfox(1,nost)-stfox(nbptsti,nost))**2+
     :(stfoy(1,nost)-stfoy(nbptsti,nost))**2)
       xlf=sqrt((x1-X2)**2+
     :(y1-Y2)**2)
      if(xli.gt.0.0001)then
        raphom=xlf/xli
      else
        WRITE(9,*)'section ',nost,' extremites confondues'
        stop
      endif
! on decale x et y pour que extremite gauche coincide      
      delx=stfox(1,nost)-x1
      dely=stfoy(1,nost)-y1
      do I=1,nbptsti
        stfox(I,nost)=stfox(i,nost)-delx
        stfoy(I,nost)=stfoy(i,nost)-dely
      enddo
! on calcule rotation pour que extremite droite coincide
      delx=-stfox(1,nost)+stfox(nbptsti,nost)
      dely=-stfoy(1,nost)+stfoy(nbptsti,nost)
      delx2=-x1+X2
      dely2=-y1+y2
      sinangle=(-delx2*dely+dely2*delx)/(raphom*(delx**2+dely**2))
      cosangle=(delx2*delx+dely2*dely)/(raphom*(delx**2+dely**2))
! on applique rotation et homothetie
      do I=2,nbptsti
        delx=-stfox(1,nost)+stfox(i,nost)
        dely=-stfoy(1,nost)+stfoy(i,nost)
        delx2=raphom*(delx*cosangle-dely*sinangle)
        dely2=raphom*(delx*sinangle+dely*cosangle)
        stfox(I,nost)=stfox(1,nost)+delx2
        stfoy(I,nost)=stfoy(1,nost)+dely2
      enddo

      return
      end

!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC      
      Subroutine correct2(nost,nozn,xnlg,ynlg)
! rotation, translation et homothetie sur une section      
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      integer nost,i,nbst,nbstf,nbptsti,kp3,ks2,nozn,kp,kzo
     :,n1,n2,nblgn,nbptzn
      double precision x1,y1,x2,y2,z1,z2,sinangle,cosangle
      double precision xli,xlf,raphom,delx,dely,delx2,dely2
! NB ST MAX DANS BD INTERMEDIAIRE
      parameter (kp=10500,KZO=1000)
      PARAMETER (KS2=1500)
! NB PT PAR ST MAX DANS BD FINALE (POST OPTIMIZ)
      PARAMETER (KP3=1000)
      double precision xnlg(0:kzo,kp),ynlg(0:kzo,kp)
      double precision stfox(kp3,ks2),stfoy(kp3,ks2),stfoz(kp3,ks2)
      COMMON/BDSTFO/ STFOX,STFOY,
     + STFOZ
      COMMON/BDDIM/NBST,NBPTSTI,NBSTF
      COMMON/NBDLGN/NBLGN,NBPTZN(KZO)
      INTEGER PTLGN
      DOUBLE PRECISION PASM 
      COMMON/BDLGN/PTLGN(0:KZO+1),PASM(KZO)
	  
! zone nozn entre ligne nozn-1 et nozn	  
      n1=ptlgn(nozn-1)
	  n2=ptlgn(nozn)
	  x1=xnlg(nozn-1,nost)
	  x2=xnlg(nozn,nost)
	  y1=ynlg(nozn-1,nost)
	  y2=ynlg(nozn,nost)
      
! calcul rapport homothetie
! longueur initiale
       xli=sqrt((stfox(n1,nost)-stfox(n2,nost))**2+
     :(stfoy(n1,nost)-stfoy(n2,nost))**2)
       xlf=sqrt((x1-X2)**2+
     :(y1-Y2)**2)
      if(xli.gt.0.0001)then
        raphom=xlf/xli
! on decale x et y pour que extremite gauche coincide      
      delx=stfox(n1,nost)-x1
      dely=stfoy(n1,nost)-y1
! on fait l'op�ration sur toute la partie droite de la section	  
      do I=n1,Nbptsti
        stfox(I,nost)=stfox(i,nost)-delx
        stfoy(I,nost)=stfoy(i,nost)-dely
      enddo
! on calcule rotation pour que extremite droite coincide
      delx=-stfox(n1,nost)+stfox(n2,nost)
      dely=-stfoy(n1,nost)+stfoy(n2,nost)
      delx2=-x1+X2
      dely2=-y1+y2
      sinangle=(-delx2*dely+dely2*delx)/(raphom*(delx**2+dely**2))
      cosangle=(delx2*delx+dely2*dely)/(raphom*(delx**2+dely**2))
! on applique rotation et homothetie
      do I=n1+1,nbptsti
        delx=-stfox(n1,nost)+stfox(i,nost)
        dely=-stfoy(n1,nost)+stfoy(i,nost)
        delx2=raphom*(delx*cosangle-dely*sinangle)
        dely2=raphom*(delx*sinangle+dely*cosangle)
        stfox(I,nost)=stfox(n1,nost)+delx2
        stfoy(I,nost)=stfoy(n1,nost)+dely2
      enddo
      else
! la longueur est nulle et doit le rester sauf erreur
         raphom=1.
!        WRITE(9,*)'section ',nost,' extremites confondues'
!        stop
      endif

      return
      end

!***************************************
! calcul de r�partition des points sur la limite
       subroutine distgv(x,y,z,m,xtgv,ytgv,ztgv,n,nbp)   
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC       
        implicit none
        integer kp
        parameter(kp=10500)
        double precision x,y,z,x3,y3,z3,x4,y4,z4
     :,xtgv(KP),ytgv(KP),ztgv(KP),dist0,x0,y0,z0
     :,dist(kp),dist2(kp)
        logical encore
        integer i,n,m,nbp

        x=999999.999
        y=999999.999
        z=999999.999
        dist0=0.             
        encore=.true.
        do i=1,n-1
          x3=xtgv(i)
          x4=xtgv(i+1)
          y3=ytgv(i)
          y4=ytgv(i+1)
          Z3=ZTGV(i)
          z4=ztgv(i+1)
          dist(i)=sqrt((x3-x4)**2+(y3-y4)**2)
          dist2(i)=dist0+dist(i)
          dist0=dist2(i)
        enddo  
          dist0=dist0/float(nbp-1)
          dist0=float(m-1)*dist0
!          WRITE(9,*)dist0,dist2(n-1)
!           dist0=coef*dist0
           do i=1,n-1
            if(encore)then
            if (dist2(i).gE.dist0)then
              x4=xtgv(i)
              y4=ytgv(i)
              z4=ztgv(i)
              x3=xtgv(i+1)
              y3=ytgv(i+1)
              z3=ztgv(i+1)
              if (dist(i).gt.0.0001)then
              x=x3+((dist2(i)-dist0)/dist(i))*(x4-x3)
              y=y3+((dist2(i)-dist0)/dist(i))*(y4-y3)
              z=z3+((dist2(i)-dist0)/dist(i))*(z4-z3)
              else
              x=x4
              y=y4
              z=z4
              endif
!			  WRITE(9,*)'m=',m,x,y,z
			  return
!              encore=.false.
            endif
            endif
          enddo
              x=xtgv(n)
              y=ytgv(n)
              z=ztgv(n)
         return    
         end      
!***************************************
! calcul de r�partition des points sur la limite
       SUBROUTINE DISTGV2(X,Y,Z,M,NSTI,NL,nbp)   
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC       
      IMPLICIT NONE
        INTEGER KP,KZO,kp2,kp3,kS2,ks1,ndebut,nfin
      PARAMETER (KS2=1500,KS1=1500)
        PARAMETER(KP=10500,KZO=1000,kp2=10500,kp3=1000)
        DOUBLE PRECISION X,Y,Z,X3,Y3,Z3,X4,Y4,Z4
     :,XTGV(0:KZO,KP),YTGV(0:KZO,KP),ZTGV(0:KZO,KP)
     :,DIST0,X0,Y0,Z0
     :,DIST(KP),DIST2(KP),XDEBUT,XFIN,YDEBUT,YFIN
     :,xtgv2(kp),ytgv2(kp)
        LOGICAL ENCORE
      INTEGER I,N,M,ntgv(0:KZO)
     :,nsti,NL,ntgv2,nb1,nblgn,nbptzn,nbp
      double precision stfox(kp3,ks2),stfoy(kp3,ks2),stfoz(kp3,ks2)
      COMMON/NBDLGN/NBLGN,NBPTZN(KZO)
      COMMON/BDSTFO/ STFOX,STFOY,
     + STFOZ
      common/xyzlg/xtgv,ytgv,ztgv
      common/nlg/ntgv
      INTEGER PTLGN
      DOUBLE PRECISION PASM
      logical debut,fin
      COMMON/BDLGN/PTLGN(0:KZO+1),PASM(KZO)
	  SAVE NTGV2,XTGV2,YTGV2

! if sur nsti=1 qui donne la ligne entre deux sections de base
      if(nsti.eq.1)then
        NB1=PTLGN(NL)
!		write(28,*)'ptlgn0',NL,ptlgn(NL)
        XDEBUT=STFOX(NB1,M-NSTI)
		YDEBUT=STFOY(NB1,M-NSTI)
		XFIN=STFOX(NB1,M-NSTI+NBP+1)
		YFIN=STFOY(NB1,M-NSTI+NBP+1)
        debut=.TRUE.
        fin=.TRUE.
! selection des points entre debut et fin		
        DO I=1,NTGV(NL)
          X3=XTGV(NL,I)
          Y3=YTGV(NL,I)
          if(abs(x3-xdebut).lt.0.0002)then
          if(abs(y3-ydebut).lt.0.0002)then
            ndebut=i+1
            debut=.false.
		  endif
          endif
		enddo
        DO I=1,NTGV(NL)
          X3=XTGV(NL,I)
          Y3=YTGV(NL,I)
          if(abs(x3-xfin).lt.0.0002)then
          if(abs(y3-yfin).lt.0.0002)then
            nfin=i-1
            fin=.false.
		  endif
          endif
		enddo
! determination ndebut
        if(debut)then
        NDEBUT=1 
        DO I=1,NTGV(NL)-1
          X3=XTGV(NL,I)
          X4=XTGV(NL,I+1)
          Y3=YTGV(NL,I)
          Y4=YTGV(NL,I+1)
        if(debut)then
		  if(x3.gt.xdebut)then
		    if(x4.lt.xdebut)then
			  if(Y3.Gt.xdebut)then
			    if(y4.lt.ydebut)then
				 ndebut=i+1
            debut=.false.
                endif 
                endif 
                endif 
                endif 
! fin du if sur debut		
		endif
        if(debut)then
		  if(x3.lt.xdebut)then
		    if(x4.gt.xdebut)then
			  if(Y3.Gt.xdebut)then
			    if(y4.lt.ydebut)then
				 ndebut=i+1
            debut=.false.
                endif 
                endif 
                endif 
                endif 
! fin du if sur debut		
		endif
        if(debut)then
		  if(x3.gt.xdebut)then
		    if(x4.lt.xdebut)then
			  if(Y3.lt.xdebut)then
			    if(y4.gt.ydebut)then
				 ndebut=i+1
            debut=.false.
                endif 
                endif 
                endif 
                endif 
! fin du if sur debut		
		endif
        if(debut)then
		  if(x3.lt.xdebut)then
		    if(x4.gt.xdebut)then
			  if(Y3.lt.xdebut)then
			    if(y4.gt.ydebut)then
				 ndebut=i+1
            debut=.false.
                endif 
                endif 
                endif 
                endif 
! fin du if sur debut		
		endif
        ENDDO 
! fin du if sur debut		
		endif
! DETERMINATION NFIN
        if(fin)then
        nfin=ndebut-1
        DO I=1,NTGV(nl)-1
          X3=XTGV(NL,I)
          X4=XTGV(NL,I+1)
          Y3=YTGV(NL,I)
          Y4=YTGV(NL,I+1)
        if(fin)then
		  IF(X3.Gt.XFIN)THEN
		    IF(X4.Lt.XFIN)THEN
			  IF(Y3.Gt.XFIN)THEN
			    IF(Y4.LT.YFIN)THEN
				 NFIN=I
                 fin=.false.
                ENDIF 
                ENDIF 
                ENDIF 
                ENDIF 
! fin du if sur fin
        endif
        if(fin)then
		  IF(X3.Lt.XFIN)THEN
		    IF(X4.GT.XFIN)THEN
			  IF(Y3.Gt.XFIN)THEN
			    IF(Y4.LT.YFIN)THEN
				 NFIN=I
                 fin=.false.
                ENDIF 
                ENDIF 
                ENDIF 
                ENDIF 
! fin du if sur fin
        endif
        if(fin)then
		  IF(X3.Gt.XFIN)THEN
		    IF(X4.LT.XFIN)THEN
			  IF(Y3.Lt.XFIN)THEN
			    IF(Y4.GT.YFIN)THEN
				 NFIN=I
                 fin=.false.
                ENDIF 
                ENDIF 
                ENDIF 
                ENDIF 
! fin du if sur fin
        endif
        if(fin)then
		  IF(X3.Lt.XFIN)THEN
		    IF(X4.GT.XFIN)THEN
			  IF(Y3.Lt.XFIN)THEN
			    IF(Y4.GT.YFIN)THEN
				 NFIN=I
                 fin=.false.
                ENDIF 
                ENDIF 
                ENDIF 
                ENDIF 
! fin du if sur fin
        endif
        ENDDO
! fin du if sur fin
        endif
! si un pas trouve on tire droit		
		if(debut.or.fin)then
		   nfin=ndebut-1
!		   write(28,*)'xfin',xfin,yfin
		endif
!        write(28,*)'ndebfin',ndebut,nfin		
		DO I=NDEBUT,NFIN
		  XTGV2(I-NDEBUT+2)=XTGV(NL,I)
		  YTGV2(I-NDEBUT+2)=YTGV(NL,I)
		ENDDO
		XTGV2(1)=XDEBUT
		YTGV2(1)=YDEBUT
		NTGV2=NFIN-NDEBUT+3
		XTGV2(NTGV2)=XFIN
		YTGV2(NTGV2)=YFIN
! fin du if sur nsti=1 qui donne laligne entre deux sections de base
      endif
! on calcule la position du nsti point sur la ligne
        X=999999.999
        Y=999999.999
        Z=999999.999
        DIST0=0.             
        ENCORE=.TRUE.
        DO I=1,Ntgv2-1
          X3=XTGV2(I)
          X4=XTGV2(I+1)
          Y3=YTGV2(I)
          Y4=YTGV2(I+1)
          DIST(I)=SQRT((X3-X4)**2+(Y3-Y4)**2)
          DIST2(I)=DIST0+DIST(I)
          DIST0=DIST2(I)
        ENDDO 
! nbp est le nombre de sections intermediaires
! nbp+1 le nombre de segments
          DIST0=DIST0/FLOAT(Nbp+1)
          DIST0=FLOAT(nsti)*DIST0
           DO I=1,Ntgv2-1
            IF(ENCORE)THEN
            IF (DIST2(I).GE.DIST0)THEN
              X4=XTGV2(I)
              Y4=YTGV2(I)
              X3=XTGV2(I+1)
              Y3=YTGV2(I+1)
              IF (DIST(I).GT.0.0001)THEN
              X=X3+((DIST2(I)-DIST0)/DIST(I))*(X4-X3)
              Y=Y3+((DIST2(I)-DIST0)/DIST(I))*(Y4-Y3)
              ELSE
              X=X4
              Y=Y4
              ENDIF
			  RETURN
            ENDIF
            ENDIF
          ENDDO
         X=XTGV2(NTGV2)
         Y=YTGV2(NTGV2)
         RETURN    
         END      
!***************************************
! calcul de l'intersection de deux droites
        Subroutine intdro (x,y,z,x1,y1,x2,y2,x3,y3,z3,x4,y4,z4)
     
        double precision x,y,z,x1,y1,x2,y2,x3,y3,z3,x4,y4,z4
        double precision a1,a2,az
   
        if(abs(x2-x1).lt.0.001)then
       if(abs(x4-x3).lt.0.001)then
          x=9999999.999
          y=9999999.999
          z=9999999.999
       elseif(abs(y4-y3).lt.0.001)then
          x=9999999.999
          y=9999999.999
          z=9999999.999
        else  
          x=x1
          a2=(y4-y3)/(x4-x3)
          y=y3+a2*(x-x3)
          az=(Z4-Z3)/(x4-x3)
          z=z3+az*(x-x3)
        endif  
       elseif(abs(x4-x3).lt.0.001)then
         if(abs(y4-y3).lt.0.001)then
          x=9999999.999
          y=9999999.999
          z=9999999.999
         else
          x=x3 
          a1=(y2-y1)/(x2-x1)
          y=y1+a1*(x-x1)   
          az=(Z4-Z3)/(y4-y3)
          z=z3+az*(y-y3)
         endif 
       else
        a1=(y2-y1)/(x2-x1)
        a2=(y4-y3)/(x4-x3)
        az=(Z4-Z3)/(x4-x3)
        if(abs(a1-A2).gt.0.0001)then       
          x=(y3-y1+a1*x1-a2*x3)/(a1-a2)
          y=y3+a2*(x-x3)  
          z=z3+az*(x-x3)
        else 
          x=9999999.999
          y=9999999.999
          z=9999999.999
        endif
       endif
       return
       end   

!***************************************
! calcul de l'intersection de une droite et de plusieurs droites
       subroutine croistgv(x0,y0,z0,x1,y1,x2,y2,xtgv,ytgv,ztgv,n)   
        integer kp
        parameter(kp=10500)
        double precision x,y,z,x1,y1,x2,y2,x3,y3,z3,x4,y4,z4
     :,xtgv(KP),ytgv(KP),ztgv(KP),dist,dist0,x0,y0,z0
        logical encore
        integer i,n

        x0=999999.999
        y0=999999.999
        z0=999999.999
        dist0=10000.             
        do i=1,n-1
          x3=xtgv(i)
          x4=xtgv(i+1)
          y3=ytgv(i)
          y4=ytgv(i+1)
          Z3=ZTGV(i)
          z4=ztgv(i+1)
          
          call intdro (x,y,z,x1,y1,x2,y2,x3,y3,z3,x4,y4,Z4)
          if(x.le.max(x3,x4))then
          if(x.ge.min(x3,x4))then
          if(y.le.max(y3,y4))then
          if(y.ge.min(y3,y4))then
          dist=sqrt((x-x1)**2+(y-y1)**2)
          if (dist.lt.dist0)then
             x0=x
             y0=y
             z0=z
             dist0=dist                       
          endif
          endif
          endif
          endif
          endif
         enddo
         return    
         end
      
 
 
