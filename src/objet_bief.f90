!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - INRAE - 1989-2020                           #
!# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################

module objet_bief

  use, intrinsic :: iso_fortran_env, only: error_unit, output_unit
  use Parametres, only: long, lnode, lname, l9, un, toutpetit
  use objet_point, only: point3D, pointLC, removeDoublons, distanceH, distance3D, interpol3D
  use objet_section, only: profil_brut, profil, interpolate_linear, couche_sed_section, profilAC
  use Mage_Utilitaires, only: between, next_real, next_string, next_int, zegal

  implicit none

  integer, parameter :: lbief = 15  !nombre de caractères autorisés pour les noms des biefs


  type segment_sedimentaire
    character(len=lbief) :: nom_bief !nom du bief qui contient le tronçon
    real(kind=long) :: pk_deb        !abscisse de début du tronçon
    real(kind=long) :: pk_fin        !abscisse de fin du tronçon
    integer :: nbcs                  !nombre de couches sédimentaires
    type(couche_sed_section), allocatable :: cs(:)
  end type segment_sedimentaire

  type(segment_sedimentaire), dimension(:), allocatable :: seg_sed

  !============!
  !=== bief ===!
  !============!

  ! Bief : les biefs du réseau ; un bief relie un nœud amont
  !        (1er nœud) à un nœud aval (2e nœud) contient le nom
  !        du fichier de données topo associées et des index sur
  !        la liste des profils en travers qui en constituent la géométrie
  type bief
    character(len=lbief) :: name     !nom du bief
    character(len=lnode) :: amont    !nom du nœud amont
    character(len=lnode) :: aval     !nom du nœud aval
    character(len=lname) :: fichier_geo  !fichier de géométrie au format ST2
    integer :: nam, nav !numéro des noeuds amont et aval (la cohérence
                        !avec les noms des noeuds amont et aval est
                        !automatique lors de la création des noeuds)
    integer :: gcd !position par rapport aux autres biefs du réseau : gauche, centre ou droite
                   !gauche -> -1 ; centre -> 0 ; droit -> +1
                   !ce paramètre est lu dans le fichier .NET, la valeur par défaut est 0
    integer :: ibg, ibd, ibc !bief amont gauche, droit, centré
    integer :: is1, is2    !index de début et fin dans la collection des profils
    ! NOTE: il n'est pas absolument nécessaire d'avoir à la fois is1 et is2 car en fait bief(i)%is1 = bief(i-1)%is2+1
    ! NOTE: mais c'est plus sûr car indépendant de l'ordre de lecture des données
    ! NOTE: et plus pratique quand on parcourt les biefs selon le rang de calcul.
    ! NOTE: contrairement à la version 7 de MAGE, is1 et is2 ne sont pas indexés selon le rang de calcul mais selon l'ordre des données (numéro dans NET)
    integer :: nprof ! nombre de profils dans le tableau sections
    integer :: sections_mem_size ! actual size of the section array in memory
                                 ! 0 if it is a pointer to a slice of the rezo sections
    class(profil_brut), dimension(:), allocatable :: sections
    class(profil_brut), dimension(:), pointer :: sections_ptr

    integer nb_lines ! nombre de lignes directrices
    character(len=3), dimension(:), allocatable :: lines_tags ! noms des lignes directrices
    contains
      procedure :: init => init_bief_from_geo_file
      procedure :: set_name => set_bief_name
      procedure :: get_name => get_bief_name
      procedure :: find_lines
      procedure :: get_line_tag
      procedure :: get_nb_lines
      procedure :: insert => insert_profil
      procedure :: split => split_bief
      procedure :: extract => extract_bief
      procedure :: patch => patch_bief
      procedure :: interpolate_profils_pas_transversal
      procedure :: interpolate_profils_npoints
      procedure :: st_to_m_pasm
      procedure :: st_to_m_nmailles
      procedure :: st_to_m_compl
      procedure :: get_mean_2D_length
      procedure :: get_mean_3D_length
      procedure :: get_min_2D_length
      procedure :: get_min_3D_length
      procedure :: get_max_2D_length
      procedure :: get_max_3D_length
      procedure :: output_bief
      procedure :: output_bief_mascaret
      procedure :: update_pk
      procedure :: deallocate_bief
      procedure :: purge
      procedure :: adjust_banks
  end type bief

  !======!
  contains
  !======!

  subroutine set_bief_name(self, name, len_name)
    implicit none
    ! prototype
    class(bief), target, intent(inout) :: self
    character(len=lbief), intent(in) :: name
    integer, optional :: len_name
    if(present(len_name))then
      if(len_name <= lbief)then
        self%name(1:len_name) = name(1:len_name)
        if(len_name <= lbief) self%name(len_name+1:)=''
      else
        self%name(1:lbief)=name(1:lbief)
      endif
    else
      self%name = trim(name)
    endif
  end subroutine set_bief_name


  subroutine get_bief_name(self, name)
    implicit none
    ! prototype
    class(bief), target, intent(in) :: self
    character(len=lbief), intent(out) :: name
    name = trim(self%name)
  end subroutine get_bief_name


  subroutine init_bief_from_geo_file(self, fichier_geo, with_charriage, with_water, historique)
    implicit none
    ! prototype
    class(bief), target, intent(inout) :: self
    character(len=lname), intent(in) :: fichier_geo
    integer, intent(in), optional :: with_charriage
    integer, intent(in), optional :: with_water
    character(len=80),dimension(:),allocatable,optional,intent(out) :: historique
    ! variables locales
    integer :: ierr, nligne, nf, luu, charriage, water
    class(profil_brut), dimension(:), pointer :: les_profils
    character(len=80) :: ligne=''
    real(kind=long) :: x, y, z

    !initialisation
    self%nprof = 0
    nligne = 0
    self%fichier_geo = trim(fichier_geo)
    if (present(with_water)) then
      if (present(with_charriage)) then
        charriage = with_charriage
      else
        charriage = 0
      endif
      water = with_water
    else
      water = 0; charriage = 0
    endif
    ! on compte le nombre de profils
    inquire(file=trim(self%fichier_geo), number=luu)
    if (luu == -1) then
      open(newunit=luu,file=trim(self%fichier_geo),status='old',form='formatted',iostat=ierr)
      if (ierr > 0) then
        write(output_unit,*) '>>>> Ouverture du fichier ST ',trim(self%fichier_geo),' impossible'
        stop ' Ouverture du fichier ST impossible'
      endif
    else
      rewind(luu)
    endif
    do !1ère lecture du fichier : comptage des profils
      read(luu,'(a)',iostat=ierr) ligne
      if (ierr > 0) then
        write(error_unit,'(3a,i0,a)') '>>>> Erreur de lecture de ',trim(self%fichier_geo),' a la ligne ',nligne, &
                                        ' dans compte_profils()'
        stop 6
      elseif (ierr < 0) then
        exit
      else
        nligne = nligne+1
        if (ligne(1:1) /= '#' .and. ligne(1:1) /= '*') then
          nf = 1  ;  x = next_real(ligne,'',nf)
                     y = next_real(ligne,'',nf)
          if (abs(x-999.999_long)+abs(y-999.999_long) < 0.001_long) self%nprof = self%nprof+1
        endif
      endif
    enddo

    if(allocated(self%sections)) call self%deallocate_bief()
    if (water == 0) then
      allocate (profil_brut::self%sections(self%nprof))
    else
      allocate (profil::self%sections(self%nprof))
    endif
    self%sections_mem_size = self%nprof
    self%is1 = 1
    self%is2 = self%nprof
    self%sections_ptr(1:) => self%sections(1:)

    if(present(historique))then
      call newBief(self%fichier_geo, luu, self%sections, self%name, charriage, historique)
    else
      call newBief(self%fichier_geo, luu, self%sections, self%name, charriage)
    endif

    call find_lines(self)

    close(luu)
  end subroutine init_bief_from_geo_file


  subroutine get_line_tag (self, i, tag)
    ! prototype
    class(bief), target :: self
    integer, intent(in) :: i
    character(len=3), intent(inout) :: tag

    tag = self%lines_tags(i)
  end subroutine get_line_tag


  subroutine get_nb_lines (self, nb_lines)
    ! prototype
    class(bief), target :: self
    integer, intent(out) :: nb_lines
    nb_lines = self%nb_lines
  end subroutine get_nb_lines


  subroutine find_lines (self)
    implicit none
    ! prototype
    class(bief), target :: self
    ! variables locales
    integer :: nb_lines
    integer :: i, j, k, nb_trouve
    character(len=3), dimension(:), allocatable :: lines_tmp

    nb_lines = 0
    do j=1, self%sections_ptr(1)%np
      if(trim(self%sections_ptr(1)%xyz(j)%tag) .ne. '')then
        nb_lines = nb_lines+1
      endif
    enddo
    allocate(lines_tmp(nb_lines))

    nb_lines = 0
    do j=1, self%sections_ptr(1)%np
      if(trim(self%sections_ptr(1)%xyz(j)%tag) .ne. '')then
        nb_trouve = 1
        do i=2, self%nprof
          do k=1, self%sections_ptr(i)%np
            if(trim(self%sections_ptr(i)%xyz(k)%tag) .eq. trim(self%sections_ptr(1)%xyz(j)%tag))then
              nb_trouve = nb_trouve+1
              exit
            endif
          enddo
        enddo
        if (nb_trouve .eq. self%nprof)then
          nb_lines = nb_lines+1
          lines_tmp(nb_lines) = self%sections_ptr(1)%xyz(j)%tag
        endif
      endif
    enddo

    allocate(self%lines_tags(nb_lines))
    self%lines_tags(1:nb_lines) = lines_tmp(1:nb_lines)
    self%nb_lines = nb_lines
    deallocate(lines_tmp)

  end subroutine find_lines


  subroutine insert_profil(self, new_profil, position)
    implicit none
    ! prototype
    class(bief) :: self
    class(profil_brut), intent(in) :: new_profil
    integer, intent(in), optional :: position
    ! variables locales
    type(profil_brut), dimension(:),allocatable :: profil_brut_temp
    type(profil), dimension(:),allocatable :: profil_temp
    integer :: old_size, old_np, pos, new_size

    if (position < 1 .or. position > self%nprof) then
      print*,"Error: position ",position," does not exist in bief ",self%name
      stop
    endif

    if (self%sections_mem_size < 1) then
      print*,"Error: can not insert a section in this bief object"
      print*,"The section array is only a pointer to an upper level"
      stop
    endif

    old_size = self%nprof
    new_size = self%nprof
    old_np = self%nprof
    if(present(position)) then
      if (position > old_np) then ! on ne va pas au delà du dernier point+1
        pos = old_np + 1
      elseif (position < 1) then
        pos = 1
      else
        pos = position
      endif
    else
      pos = old_np+1
    endif

    if (old_size < old_np + 1) then
      if (old_size <= 0) then
        new_size=1
      else
        new_size = old_size*2
      endif
      ! on double la taille a allouer :
      self%sections_mem_size = new_size
    end if

    select type(prfl_ptr => self%sections)
      type is (profil_brut)
        ! on alloue le nouvel espace memoire
        allocate(profil_brut_temp(new_size))
        !on copie les anciennes valeurs de sections :
        if (pos > 1) profil_brut_temp(:pos-1) = prfl_ptr(:pos-1)
        if (pos <= old_np) profil_brut_temp(pos+1:old_np+1) = prfl_ptr(pos:old_np)
        ! on place profil a sa place dans le tableau
        profil_brut_temp(pos) = new_profil
        ! prfl_ptr pointe sur l'espace memoire de profil_brut_temp, profil_brut_temp est désalloue
        call move_alloc(profil_brut_temp, self%sections)
      type is (profil)
        ! on alloue le nouvel espace memoire
        allocate(profil_temp(new_size))
        !on copie les anciennes valeurs de sections :
        if (pos > 1) profil_temp(:pos-1) = prfl_ptr(:pos-1)
        if (pos <= old_np) profil_temp(pos+1:old_np+1) = prfl_ptr(pos:old_np)
        ! on place profil a sa place dans le tableau
        profil_temp(pos) = new_profil
        ! prfl_ptr pointe sur l'espace memoire de profil_temp, profil_temp est désalloue
        call move_alloc(profil_temp, self%sections)
    end select

    self%nprof = old_np + 1

  end subroutine insert_profil


  subroutine interpolate_profils_pas_transversal(self, limites, directrices1, directrices2, pas, lplan, longmi, lineaire)
    implicit none
    ! prototype
    class(bief), target :: self
    integer, dimension(:), intent(in) :: limites, longmi
    real(kind=long), intent(in) :: pas
    character(len=3), dimension(:), intent(in) :: directrices1, directrices2
    logical, intent(in) :: lplan
    logical, intent(in) :: lineaire
    ! variables locales
    real(kind=long) :: d1,d2,d
    integer :: itronc, isect, nb_troncons, nbstf
    integer, dimension(:), allocatable :: nbsti

    ! check if limites is sorted
    do isect=1,size(limites)-1
      if(limites(isect)>limites(isect+1)) stop '>>> Erreur : limites non triées'
    enddo
    nb_troncons = size(limites)-1
    if(limites(nb_troncons+1)>self%nprof) stop '>>> Erreur : valeurs trop élevées dans limites'
    if(limites(1)<1) stop '>>> Erreur : valeurs négatives ou nulles dans limites'
    if(size(directrices1).ne.nb_troncons) stop '>>> Erreur : directrices1 doit avoir une taille de nb_troncons'
    if(size(directrices2).ne.nb_troncons) stop '>>> Erreur : directrices2 doit avoir une taille de nb_troncons'
    if(size(longmi).ne.nb_troncons) stop '>>> Erreur : longmi doit avoir une taille de nb_troncons'

    allocate(nbsti(self%nprof-1))
    nbsti(:) = 0
    ! calcul des longueurs
    do itronc =1,nb_troncons
      do isect=limites(itronc),limites(itronc+1)-1
        if(lplan)then
          d1=distanceH(self%sections_ptr(isect)%get(directrices1(itronc)),self%sections_ptr(isect+1)%get(directrices1(itronc)))
          d2=distanceH(self%sections_ptr(isect)%get(directrices2(itronc)),self%sections_ptr(isect+1)%get(directrices2(itronc)))
        else
          d1=distance3D(self%sections_ptr(isect)%get(directrices1(itronc)),self%sections_ptr(isect+1)%get(directrices1(itronc)))
          d2=distance3D(self%sections_ptr(isect)%get(directrices2(itronc)),self%sections_ptr(isect+1)%get(directrices2(itronc)))
        endif

        if (longmi(itronc).eq.1) then
            d=max(d1,d2)
        else if (longmi(itronc).eq.2) then
            d=min(d1,d2)
        else
            d=0.5*(d1+d2)
        endif

!       calcul du nb de sections intermediares :
!       correction du 31 08 07 car nombre de sections et pas d'intervalles
        nbsti(isect)=nint(d/pas)-1
        if(nbsti(isect).lt.0) nbsti(isect)=0
!       amelioration a faire: calculer toutes des distances sur tous les
!       points et verifier qu avec le nombre de points, on est dessous un max
      enddo
    enddo

    call self%interpolate_profils_npoints(nbsti, lplan, lineaire)
  end subroutine interpolate_profils_pas_transversal


  subroutine interpolate_profils_npoints(self, nbsti, lplan, lineaire)
    implicit none
    ! prototype
    class(bief), target :: self
    integer, dimension(:), intent(in) :: nbsti
    logical, intent(in) :: lplan
    logical, intent(in) :: lineaire
    ! variables locales
    integer :: nbstf, i
    class(profil_brut), dimension(:), allocatable :: prfl

    nbstf = self%nprof
    do i=1, size(nbsti)
        nbstf = nbstf + nbsti(i)
    enddo

    select type(st=>self%sections)
      type is(profil_brut)
        allocate(profil_brut::prfl(nbstf))
      type is(profil)
        allocate(profil::prfl(nbstf))
    end select

    if(lineaire)then
        call interpolate_linear(self%sections, nbsti, prfl)
    else
        call calc_st_tordu(self%sections, nbsti, prfl)
    endif
    call move_alloc(prfl, self%sections)
    self%nprof = nbstf
    self%sections_mem_size = nbstf
    self%sections_ptr(1:) => self%sections(1:)
  end subroutine interpolate_profils_npoints


  subroutine calc_deriv(xyz0,xyz1,xyz2,xyz3,dx1,dy1,dz1,dx2,dy2,dz2,linz)
  ! calculs des vecteurs derivees sur les points
    implicit none
    ! prototype
    type(point3D), intent(in) :: xyz0,xyz1,xyz2,xyz3
    real(kind=long), intent(out) :: dx1,dy1,dz1,dx2,dy2,dz2
    integer, intent(in) :: linz
    real(kind=long) :: t1,t2,c1,c2,b1,b2, pa0,pa1,pb0,pb1
    real(kind=long) :: d12, d13, d02

! !     estimation des parametres de linterpolateur t c b
!     call estim_tcb(isect)
    d12 = distance3D(xyz1, xyz2)
    d13 = distance3D(xyz1, xyz3)
    d02 = distance3D(xyz0, xyz2)

    t1 = 1.0 - 2.0 * (d12/d02)
    t2 = 1.0 - 2.0 * (d12/d13)
    c1 = 0.0
    c2 = 0.0
    b1 = 0.0
    b2 = 0.0

!   calcul des parametres
    pa0=(1.-t1)*(1.+c1)*(1.+b1)/2.
    pb0=(1.-t1)*(1.-c1)*(1.-b1)/2.

    pa1=(1.-t2)*(1.-c2)*(1.+b2)/2.
    pb1=(1.-t2)*(1.+c2)*(1.-b2)/2.

!   verif pa0=pb0 et idem 1 (c et b nuls)
!   if(pa0.ne.pb0) print*,' =====> bizarre, pa0 .ne. pb0',noco,nost
!   if(pa1.ne.pb1) print*,' =====> bizarre, pa0 .ne. pb0',noco,nost

!   calculs des derivees
    dx1=pa0*(xyz1%x-xyz0%x) + pb0*(xyz2%x-xyz1%x)
    dy1=pa0*(xyz1%y-xyz0%y) + pb0*(xyz2%y-xyz1%y)
    dz1=pa0*(xyz1%z-xyz0%z) + pb0*(xyz2%z-xyz1%z)

    dx2=pa1*(xyz2%x-xyz1%x) + pb1*(xyz3%x-xyz2%x)
    dy2=pa1*(xyz2%y-xyz1%y) + pb1*(xyz3%y-xyz2%y)
    dz2=pa1*(xyz2%z-xyz1%z) + pb1*(xyz3%z-xyz2%z)

!   verif si d0 et d1 meme module
!   d0=sqrt(dx0(noco)**2+dy0(noco)**2+dz0(noco)**2)
!   d1=sqrt(dx1(noco)**2+dy1(noco)**2+dz1(noco)**2)
!   if(d0.ne.d1) print*,'==> bizarre d0.ne.d1',noco,nost,d0,d1

!   pour le cas ou on interpole z lineaire, on peur dire que la continuite sur
!   z est egale a -1, cad pa0 suz z = 0 et pb1 sur z = 0. donc:
    if(linz.eq.1.) then
         dz1=pb0*(xyz2%z-xyz1%z)
         dz2=pa1*(xyz2%z-xyz1%z)
    endif
  end subroutine calc_deriv


  subroutine calc_st_tordu(sections_in, nbsti, sections_out)
  ! calcul des st finales entre sections_in(i) et sections_in(i+1)
  ! cas entre deux st mesurees non planes => stf non plane
  ! (il y en a nbsti(i) entre ces deux st)
    implicit none
    ! prototype
    class(profil_brut), dimension(:), intent(in) :: sections_in
    integer, dimension(:), intent(in) :: nbsti
    class(profil_brut), dimension(:), intent(inout) :: sections_out
    ! variables locales
    integer :: nsect_out, nsect_in, isect_out, isect_in, isect_loc, ipt
    real(kind=long) :: t,dx2,dy2,dz2,dx1,dy1,dz1,dpk
    type(point3D) :: xyz_bout, xyz_bout2

    nsect_in = size(sections_in)
    nsect_out = size(sections_out)
    isect_out = 1

    do isect_in=1,nsect_in-1
      sections_out(isect_out) = sections_in(isect_in)
      isect_out = isect_out+1
      dpk = sections_in(isect_in+1)%pk - sections_in(isect_in)%pk
      do isect_loc=1,nbsti(isect_in)
        sections_out(isect_out) = sections_in(isect_in)
        ! RATIO entre les deux sections initiales
        t=float(isect_loc)/float(nbsti(isect_in)+1)
        do ipt=1,sections_in(isect_in)%np
          if (nsect_in == 2) then
            xyz_bout%x = sections_in(isect_in)%xyz(ipt)%x - &
                         (sections_in(isect_in+1)%xyz(ipt)%x - &
                          sections_in(isect_in)%xyz(ipt)%x)
            xyz_bout%y = sections_in(isect_in)%xyz(ipt)%y - &
                         (sections_in(isect_in+1)%xyz(ipt)%y - &
                          sections_in(isect_in)%xyz(ipt)%y)
            xyz_bout%z = sections_in(isect_in)%xyz(ipt)%z - &
                         (sections_in(isect_in+1)%xyz(ipt)%z - &
                          sections_in(isect_in)%xyz(ipt)%z)
            xyz_bout2%x = sections_in(isect_in+1)%xyz(ipt)%x - &
                          (sections_in(isect_in)%xyz(ipt)%x - &
                           sections_in(isect_in+1)%xyz(ipt)%x)
            xyz_bout2%y = sections_in(isect_in+1)%xyz(ipt)%y - &
                          (sections_in(isect_in)%xyz(ipt)%y - &
                           sections_in(isect_in+1)%xyz(ipt)%y)
            xyz_bout2%z = sections_in(isect_in+1)%xyz(ipt)%z - &
                          (sections_in(isect_in)%xyz(ipt)%z - &
                           sections_in(isect_in+1)%xyz(ipt)%z)
            call calc_deriv(xyz_bout, &
                            sections_in(isect_in)%xyz(ipt), &
                            sections_in(isect_in+1)%xyz(ipt), &
                            xyz_bout, &
                            dx1,dy1,dz1,dx2,dy2,dz2,0)
          elseif (isect_in == 1) then
            xyz_bout%x = sections_in(isect_in)%xyz(ipt)%x - &
                         (sections_in(isect_in+1)%xyz(ipt)%x - &
                          sections_in(isect_in)%xyz(ipt)%x)
            xyz_bout%y = sections_in(isect_in)%xyz(ipt)%y - &
                         (sections_in(isect_in+1)%xyz(ipt)%y - &
                          sections_in(isect_in)%xyz(ipt)%y)
            xyz_bout%z = sections_in(isect_in)%xyz(ipt)%z - &
                         (sections_in(isect_in+1)%xyz(ipt)%z - &
                          sections_in(isect_in)%xyz(ipt)%z)
            call calc_deriv(xyz_bout, &
                            sections_in(isect_in)%xyz(ipt), &
                            sections_in(isect_in+1)%xyz(ipt), &
                            sections_in(isect_in+2)%xyz(ipt), &
                            dx1,dy1,dz1,dx2,dy2,dz2,0)
          elseif (isect_in == nsect_in-1) then
            xyz_bout2%x = sections_in(isect_in+1)%xyz(ipt)%x - &
                          (sections_in(isect_in)%xyz(ipt)%x - &
                           sections_in(isect_in+1)%xyz(ipt)%x)
            xyz_bout2%y = sections_in(isect_in+1)%xyz(ipt)%y - &
                          (sections_in(isect_in)%xyz(ipt)%y - &
                           sections_in(isect_in+1)%xyz(ipt)%y)
            xyz_bout2%z = sections_in(isect_in+1)%xyz(ipt)%z - &
                          (sections_in(isect_in)%xyz(ipt)%z - &
                           sections_in(isect_in+1)%xyz(ipt)%z)
            call calc_deriv(sections_in(isect_in-1)%xyz(ipt), &
                            sections_in(isect_in)%xyz(ipt), &
                            sections_in(isect_in+1)%xyz(ipt), &
                            xyz_bout2, &
                            dx1,dy1,dz1,dx2,dy2,dz2,0)
          else
            call calc_deriv(sections_in(isect_in-1)%xyz(ipt), &
                            sections_in(isect_in)%xyz(ipt), &
                            sections_in(isect_in+1)%xyz(ipt), &
                            sections_in(isect_in+2)%xyz(ipt), &
                            dx1,dy1,dz1,dx2,dy2,dz2,0)
          endif
          call pt_traj(sections_in(isect_in),sections_in(isect_in+1),ipt,t,&
                       sections_out(isect_out)%xyz(ipt),dx1,dy1,dz1,dx2,dy2,dz2)
          sections_out(isect_out)%xyz(ipt)%tag = sections_in(isect_in)%xyz(ipt)%tag
        enddo
        sections_out(isect_out)%pk = sections_in(isect_in)%pk + t*dpk
        write(sections_out(isect_out)%name,'(a,i0)') 'interpol',int(sections_out(isect_out)%pk)
        isect_out = isect_out+1
      enddo
    enddo
    sections_out(nsect_out) = sections_in(nsect_in)
  end subroutine calc_st_tordu


  subroutine pt_traj(sect1,sect2,ncb,t,xyz,dx0,dy0,dz0,dx1,dy1,dz1)
  ! sur une tranche sect1 sect2 donnee, sur une trajectoire ncb donnee, a un t donne
  ! calcule x,y,z en utilisant la base des tangeantes
    implicit none
    ! prototype
    class(profil_brut), intent(in) :: sect1,sect2
    integer, intent(in) :: ncb
    real(kind=long), intent(in) :: dx0,dy0,dz0,dx1,dy1,dz1,t
    type(point3D), intent(inout) :: xyz
    real(kind=long) :: h2,h3,h4,h1,t2,t3

    ! puissance de t
    t2=t*t
    t3=t2*t

    ! valeurs des polynomes de hermite
    h1=2.*t3-3.*t2+1.
    h2=-2.*t3+3.*t2
    h3=t3-2.*t2+t
    h4=t3-t2

    ! calcul du point de la trajectoire
    xyz%x = sect1%xyz(ncb)%x * h1 + sect2%xyz(ncb)%x * h2 + dx0 * h3 + dx1 * h4
    xyz%y = sect1%xyz(ncb)%y * h1 + sect2%xyz(ncb)%y * h2 + dy0 * h3 + dy1 * h4
    xyz%z = sect1%xyz(ncb)%z * h1 + sect2%xyz(ncb)%z * h2 + dz0 * h3 + dz1 * h4
  end subroutine pt_traj

  subroutine newBief(STfile, luu, les_profils, name, charriage, historique)
  !==============================================================================
  !    construction d'un jeu de profils par lecture d'un fichier de
  !    profils XYZ au format ST
  !
  !==============================================================================
    ! prototype
    character(len=lname), intent(in) :: STfile
    character(len=lbief), intent(in) :: name
    integer, intent(in) :: luu
    class(profil_brut), intent(inout), dimension(:) :: les_profils
    integer, intent(inout) :: charriage
    character(len=80),dimension(:),allocatable,optional,intent(out) :: historique
    ! variables locales
    integer :: ierr

    if (present(historique)) historique = read_st_history(STfile, luu)
    select type(les_profils)
      type is(profil_brut)
        charriage = 0
    end select
    select case (charriage)
      case (0) ; call newBief_3D(STfile,luu,les_profils)
      case (1)
        select type(les_profils)
          class is(profil)
            call newBief_TS(STfile,luu,les_profils)
        end select
      case (2)
        select type(les_profils)
          class is(profil)
            call newBief_SED(STfile,luu,name,les_profils)
        end select
      case (3)
        select type(les_profils)
          class is(profil)
            call newBief_TS2(STfile,luu,les_profils)
        end select
      case default ; stop '>>> Erreur : valeur incorrecte pour charriage'
    end select
  end subroutine newBief


  function read_st_history(STfile, luu)
  !==============================================================================
  !    lecture de l'en-tête d'un fichier de profils XYZ au format ST
  !
  !==============================================================================
    ! prototype
    character(len=80),dimension(:),allocatable :: read_st_history
    character(len=*), intent(in) :: STfile
    integer, intent(in) :: luu
    ! variables locales
    character(len=80),dimension(:),allocatable :: historique
    character(len=250) :: ligne=''
    integer :: ierr, nligne

    allocate(read_st_history(0))
    rewind (luu)
    do !recherche de la ligne d'entête du 1er profil
        read(luu,'(a)',iostat=ierr) ligne
        if (ierr > 0) then
          write(error_unit,*) '>>>> Erreur de lecture de ',trim(STfile),' a la ligne ',nligne
          stop 6
        elseif (ierr < 0) then
          exit
        else
          if (ligne(1:1) == '#') then
              allocate(historique(size(read_st_history)+1))
              historique(1:size(read_st_history)) = read_st_history
              write(historique(size(read_st_history)+1),'(A80)') ligne(1:80)
              call move_alloc(historique,read_st_history)
          elseif (ligne(1:1) == '*') then
              cycle
          else
              exit
          endif
        endif
    enddo
    if (ierr < 0) then  !fin de fichier !!!
        write(error_unit,*) '>>>> Erreur de lecture entete de ',trim(STfile),' : fin de fichier !!!'
        stop 6
    endif
  end function read_st_history


  subroutine newBief_3D(STfile, luu, les_profils)
  !==============================================================================
  !    construction d'un jeu de profils TS par lecture d'un fichier de
  !    profils XYZ au format ST (SANS couches sédimentaires)
  !
  !==============================================================================
    ! prototype
    character(len=*), intent(in) :: STfile
    integer, intent(in) :: luu
    class(profil_brut), intent(inout), dimension(:) :: les_profils
    ! variables locales
    character(len=250) :: ligne=''
    character(len=3) :: tag=''
    character(len=20) :: nomProfil=''
    integer :: ierr, nligne, nprof, np, nc, inull, nf, n, i, j
    real(kind=long) :: x, y, z, pk
    type(point3D), allocatable :: xyz(:), xyz_buf(:)
    logical :: realloc

    rewind (luu)
    do !recherche de la ligne d'entête du 1er profil
      read(luu,'(a)',iostat=ierr) ligne
      if (ierr > 0) then
        write(error_unit,*) '>>>> Erreur de lecture de ',trim(STfile),' a la ligne ',nligne
        stop 6
      elseif (ierr < 0) then
        exit
      else
        if (ligne(1:1) == '#' .or. ligne(1:1) == '*') then
          cycle
        else
          exit
        endif
      endif
    enddo
    ! ici on a la ligne d'entête du 1er profil
    nprof = 0
    do while (ierr == 0) !décodage du fichier
      !décodage de la ligne d'entête de section
      read(ligne,'(4i6,f13.0)') inull,inull,inull,np,pk
      nc = inull  !ne sert à rien sauf à faire disparaître un avertissement sur inull (set but not used)
      nf = 39
      nprof = nprof + 1  ! nouveau profil
!       nomProfil = ligne(39:58)
      nomProfil = next_string(ligne,'',nf)
      if (len_trim(nomProfil) == 0) then
        i = scan(STfile,'.',back=.true.)-1
        if (i < 1) i = len_trim(STfile)
        !on utilise '/' et '\' pour pouvoir tester un exé Windows sous Linux, donc avec un / comme séparateur de répertoires
        j = max(scan(STfile,'/',back=.true.),scan(STfile,'\',back=.true.))+1
        write(nomProfil,'(2a,i0)') STfile(j:i),'&', int(pk)
!         nomProfil=''
      endif
      ! NOTE: il peut arriver que la valeur de np lue dans l'entête du profil soit différente du nombre réel de points du profil
      allocate(xyz(1:np))

      x = 0._long  ;  y = 0._long  ;  np = 0  ;  realloc = .false.
      do   !lecture des coordonnées
        read(luu,'(a)') ligne
        if (ligne(1:1) /= '#' .and. ligne(1:1) /= '*') then
          !lecture de la ligne en 2 fois pour vérifier la valeur de nc
          nf = 1  ;  x = next_real(ligne,'',nf)
                     y = next_real(ligne,'',nf)
                     z = next_real(ligne,'',nf)
                    tag = next_string(ligne,'',nf)
          if (abs(x-999.999_long)+abs(y-999.999_long) > 0.001_long) then
            np = np+1
            if (np > size(xyz)) then
              realloc = .true.
              allocate (xyz_buf(np+10))
              xyz_buf(1:np) = xyz(1:np)
              call move_alloc(xyz_buf,xyz)
            endif
            xyz(np) = point3D(x, y, z, tag)
          else
            if (realloc) write(error_unit,'(a,f0.3,2a,/,17x,a)') ' >>>> ATTENTION : le nombre de points du profil pk ',pk, &
                              ' du fichier ',trim(STfile),' indiqué par l''entête du profil est inférieur au nombre réel'
            exit
          endif
        endif
      enddo
      ! il faut corriger la taille de xyz(:) s'il y a erreur sur le nombre de points indiqué dans l'entête du profil
      ! NOTE: ce n'est pas vraiment nécessaire pour newBief_3D() parce que le problème est corrigé par le clonage dans xyz_TS(:)
      ! NOTE: on le fait quand même par précaution et par homogénéité avec newBief_TS() et newBief_SED() qui ne clonent pas.
      if (np < size(xyz) .and. .not.realloc) then
        write(error_unit,'(a,f0.3,2a,/,17x,a)') ' >>>> ATTENTION : le nombre de points du profil pk ',pk, &
                          ' du fichier ',trim(STfile),' indiqué par l''entête du profil est supérieur au nombre réel'
        allocate (xyz_buf(np))
        xyz_buf(1:np) = xyz(1:np)
        call move_alloc(xyz_buf,xyz)
      endif

      call removeDoublons(xyz)
      call les_profils(nprof)%init(pk, nomProfil, xyz)
      deallocate (xyz)
      !if (verbose) call affiche(les_profils(nprof))

      read(luu,'(a)',iostat=ierr) ligne  !lecture de la ligne d'entête de section suivante
    enddo
  end subroutine newBief_3D


  subroutine newBief_TS(STfile, luu, les_profils)
  !==============================================================================
  !    construction d'un jeu de profils TS par lecture d'un fichier de
  !    profils XYZ au format ST étendu avec couches sédimentaires aux points
  !
  !==============================================================================
    ! prototype
    character(len=*), intent(in) :: STfile
    integer, intent(in) :: luu
    class(profil), intent(inout), dimension(:) :: les_profils
    ! variables locales
    character(len=1000) :: ligne=''
    character(len=3) :: tag=''
    character(len=20) :: nomProfil=''
    integer :: ierr, nligne, nprof, np, i, j, nc, inull, nf
    real(kind=long) :: x, y, z, pk
    type(point3D), allocatable :: xyz(:), xyz_buf(:)
    logical :: bFail, realloc
    real(kind=long), allocatable, dimension(:) :: zc, d50, sigma,tau

    rewind (luu)
    do !recherche de la ligne d'entête du 1er profil
      read(luu,'(a)',iostat=ierr) ligne
      if (ierr > 0) then
        write(error_unit,*) '>>>> Erreur de lecture de ',trim(STfile),' a la ligne ',nligne
        stop 6
      elseif (ierr < 0) then
        exit
      else
        if (ligne(1:1) == '#' .or. ligne(1:1) == '*') then
          cycle
        else
          exit
        endif
      endif
    enddo
    if (ierr < 0) then  !fin de fichier !!!
      write(error_unit,*) '>>>> Erreur de lecture de ',trim(STfile),' : fin de fichier !!!'
      stop 6
    endif
    ! ici on a la ligne d'entête du 1er profil
    nprof = 0
    do while (ierr == 0) !décodage du fichier
      !décodage de la ligne d'entête de section
      read(ligne,'(4i6,f13.0)') inull,inull,inull,np,pk
      nc = inull  !ne sert à rien sauf à faire disparaître un avertissement sur inull (set but not used)
      nf = 39
      nprof = nprof + 1  ! nouveau profil
!       nomProfil = ligne(39:58)
      nomProfil = next_string(ligne,'',nf)
      if (len_trim(nomProfil) == 0) then
        i = len_trim(STfile)
        !on utilise '/' et '\' pour pouvoir tester un exé Windows sous Linux, donc avec un / comme séparateur de répertoires
        j = max(scan(STfile,'/',back=.true.),scan(STfile,'\',back=.true.))+1
        write(nomProfil,'(2a,i6.6)') STfile(j:i),'&', int(pk)
      endif
      ! NOTE: il peut arriver que la valeur de np lue dans l'entête du profil soit différente du nombre réel de points du profil
      allocate(xyz(1:np))

      x = 0._long  ;  y = 0._long  ;  np = 0  ;  realloc = .false.
      do   !lecture des coordonnées
        read(luu,'(a)') ligne
        if (ligne(1:1) /= '#' .and. ligne(1:1) /= '*') then
          !lecture de la ligne en 2 fois pour vérifier la valeur de nc
          !ici on ne peut pas utiliser next_real() et next_string() car on ne sait pas alors détecter les tags vides
          read(ligne,'(3f13.0,a3)') x, y, z, tag
          nf = 44  ;  nc = next_int(ligne,'',nf)
          if (nc > 0) then  ! rien à lire si nc = 0
            allocate(zc(0:nc))
            allocate(d50(nc))
            allocate(sigma(nc))
            allocate(tau(nc))
            do i = 1, nc
              zc(i)    = next_real(ligne,'',nf)
              d50(i)   = next_real(ligne,'',nf)
              sigma(i) = next_real(ligne,'',nf)
              tau(i)   = next_real(ligne,'',nf)
            enddo
          elseif (nc == 0) then
            ! valeurs par défaut pour pouvoir utiliser les fichiers ST tels quels -> une seule couche sédimentaire
            nc = 1
            allocate(zc(0:nc))
            allocate(d50(nc))
            allocate(sigma(nc))
            allocate(tau(nc))
            zc(1)    = z
            d50(1)   = 0.001_long
            sigma(1) = 3._long
            tau(1)   = -1._long
          else
            write(error_unit,*) 'ERREUR de lecture des couches sédimentaires : nombre de couches négatif !'
            stop 6
          endif
          ! affectation
          if (abs(x-999.999_long)+abs(y-999.999_long) > 0.001_long) then
            np = np+1
            if (np > size(xyz)) then
              realloc = .true.
              allocate (xyz_buf(np+10))
              xyz_buf(1:np) = xyz(1:np)
              call move_alloc(xyz_buf,xyz)
            endif
            xyz(np)%x = x  ;  xyz(np)%y = y  ;  xyz(np)%z = z
            xyz(np)%tag = tag
            xyz(np)%nbcs = nc
            allocate(xyz(np)%cs(nc))
            bFail = .false.
            zc(0) = xyz(np)%z
            do i = 1, nc
              xyz(np)%cs(i)%zc    = zc(i)
              xyz(np)%cs(i)%d50   = d50(i)
              xyz(np)%cs(i)%sigma = sigma(i)
              xyz(np)%cs(i)%tau   = tau(i)
              !vérification de l'empilement des couches sédimentaires
              if (zc(i) > zc(i-1)) then
                if (i == 1) then
                  write(error_unit,*) &
                                    '>>>> Erreur : le plancher de la 1ère couche est au-dessus de la cote du fond'
                else
                  write(error_unit,*) &
                                    '>>>> Erreur : le plancher de la couche ',i,' est au-dessus de celui de la couche ',i-1
                endif
                write(error_unit,*) trim(ligne)
                bFail = .true.
              endif
            enddo
            if (bFail) then
              write(error_unit,*) '>>>> Erreur dans la définition des couches sédimentaires'
              stop 6
            endif
          else
            if (realloc) write(error_unit,'(a,f0.3,2a,/,17x,a)') ' >>>> ATTENTION : le nombre de points du profil pk ',pk, &
                              ' du fichier ',trim(STfile),' indiqué par l''entête du profil est inférieur au nombre réel'
            deallocate(zc)
            deallocate(d50)
            deallocate(sigma)
            deallocate(tau)
            exit
          endif
          deallocate(zc)
          deallocate(d50)
          deallocate(sigma)
          deallocate(tau)
        endif
      enddo
      ! il faut corriger la taille de xyz(:) s'il y a erreur sur le nombre de points indiqué dans l'entête du profil
      if (np < size(xyz) .and. .not.realloc) then
        write(error_unit,'(a,f0.3,2a,/,17x,a)') ' >>>> ATTENTION : le nombre de points du profil pk ',pk, &
                          ' du fichier ',trim(STfile),' indiqué par l''entête du profil est supérieur au nombre réel'
        allocate (xyz_buf(np))
        xyz_buf(1:np) = xyz(1:np)
        call move_alloc(xyz_buf,xyz)
      endif
      call removeDoublons(xyz)
      call les_profils(nprof)%init(pk, nomProfil, xyz)
      deallocate (xyz)
      !if (verbose) call affiche(les_profils(nprof))

      read(luu,'(a)',iostat=ierr) ligne  !lecture de la ligne d'entête de section suivante
    enddo
  end subroutine newBief_TS


  subroutine newBief_SED(STfile, luu, nom_bief, les_profils)
  !==============================================================================
  !    construction d'un jeu de profils TS par lecture d'un fichier de
  !    profils XYZ au format ST fusionné avec les données du fichier SED
  !    lu auparavant
  !
  !==============================================================================
    ! prototype
    character(len=*), intent(in) :: STfile, nom_bief
    integer, intent(in) :: luu
    class(profil), intent(inout), dimension(:) :: les_profils
    ! variables locales
    integer :: np, i, nc, num_seg, ic
    real(kind=long) :: pk, s

    call newBief_3D(STfile, luu, les_profils)

    do i = 1, size(les_profils)
      !recherche d'un tronçon SED contenant ce profil
      pk = les_profils(i)%pk
      num_seg = 0  !couche sédimentaire par défaut
      do ic = 1, ubound(seg_sed,dim=1)
        if (trim(seg_sed(ic)%nom_bief) == trim(nom_bief)) then
          if (between(pk,seg_sed(ic)%pk_deb,seg_sed(ic)%pk_fin)) then
            num_seg = ic
            exit
          endif
        endif
      enddo

      nc = seg_sed(num_seg)%nbcs
      les_profils(i)%nbcs = nc

      allocate(les_profils(i)%cs(nc))
      les_profils(i)%cs(1)%hc    = seg_sed(num_seg)%cs(1)%hc
      les_profils(i)%cs(1)%d50   = seg_sed(num_seg)%cs(1)%d50
      les_profils(i)%cs(1)%sigma = seg_sed(num_seg)%cs(1)%sigma
      les_profils(i)%cs(1)%tau   = seg_sed(num_seg)%cs(1)%tau
      if (nc > 1) then
        do ic = 2, nc
          les_profils(i)%cs(ic)%hc    = seg_sed(num_seg)%cs(ic)%hc
          les_profils(i)%cs(ic)%d50   = seg_sed(num_seg)%cs(ic)%d50
          les_profils(i)%cs(ic)%sigma = seg_sed(num_seg)%cs(ic)%sigma
          les_profils(i)%cs(ic)%tau   = seg_sed(num_seg)%cs(ic)%tau
        enddo
      end if
    enddo
  end subroutine newBief_SED


  subroutine newBief_TS2(STfile, luu, les_profils)
  !==============================================================================
  !    construction d'un jeu de profils TS par lecture d'un fichier de
  !    profils XYZ au format ST etendu avec couches aux profils
  !
  !==============================================================================
    ! prototype
    character(len=*), intent(in) :: STfile
    integer, intent(in) :: luu
    class(profil), intent(inout), dimension(:) :: les_profils
    ! variables locales
    character(len=1000) :: ligne=''
    integer :: i, num_seg, ic, itmp, ierr, nc, nf, nligne, np, j
    real(kind=long) :: pk, dy, s
    real(kind=long) :: hc, d50, sigma, tau, dtmp
    character(len=20) :: name

    call newBief_3D(STfile, luu, les_profils)

    rewind (luu)
    i = 0
    do
      read(luu,'(a)',iostat=ierr) ligne
      if (ierr > 0) then
        write(error_unit,*) '>>>> Erreur de lecture de ',trim(STfile),' a la ligne ',nligne
        stop 6
      elseif (ierr < 0) then
        exit
      elseif (ligne(1:1) == '#' .or. ligne(1:1) == '*') then
          cycle
      else
          nf = 1
          itmp = next_int(ligne,'',nf)
          itmp = next_int(ligne,'',nf)
          itmp = next_int(ligne,'',nf)
          np   = next_int(ligne,'',nf)
          dtmp = next_real(ligne,'',nf)
          name = next_string(ligne,'',nf)
          nc = next_int(ligne,'',nf)
          i = i + 1  ! nouveau profil
          if (nc > 0) then  ! rien à lire si nc = 0
            allocate(les_profils(i)%cs(nc))
            do ic = 1, nc
              les_profils(i)%cs(ic)%hc    = next_real(ligne,'',nf)
              les_profils(i)%cs(ic)%d50   = next_real(ligne,'',nf)
              les_profils(i)%cs(ic)%sigma = next_real(ligne,'',nf)
              les_profils(i)%cs(ic)%tau   = next_real(ligne,'',nf)
            enddo
          elseif (nc == 0) then
            ! valeurs par défaut pour pouvoir utiliser les fichiers ST tels quels -> une seule couche sédimentaire
            nc = 1
            allocate(les_profils(i)%cs(nc))
            les_profils(i)%cs(1)%hc    = 0.0
            les_profils(i)%cs(1)%d50   = 0.001_long
            les_profils(i)%cs(1)%sigma = 3._long
            les_profils(i)%cs(1)%tau   = -1._long
          else
            write(error_unit,*) 'ERREUR de lecture des couches sédimentaires : nombre de couches négatif !'
            stop 6
          endif
          les_profils(i)%nbcs = nc
          do j=1, np+1
             read(luu,'(a)',iostat=ierr) ligne
          enddo
      endif
    enddo
  end subroutine newBief_TS2


  subroutine split_bief(self, bief1, bief2, tag)
  ! split a bief in two biefs along a headline. Headline point is duplicated.
    ! prototype
    class(bief), intent(in) :: self
    class(bief), intent(out), target :: bief1, bief2
    character(len=3), intent(in) :: tag
    ! variables locales
    integer :: i

    write(bief1%name,*) self%name//'1'
    bief1%amont = self%amont
    bief1%aval = self%aval
    bief1%fichier_geo = self%fichier_geo
    bief1%nam = self%nam
    bief1%nav = self%nav
    bief1%gcd = self%gcd
    bief1%ibg = self%ibg
    bief1%ibd = self%ibd
    bief1%ibc = self%ibc
    bief1%is1 = self%is1
    bief1%is2 = self%is2
    bief1%nprof = self%nprof
    bief1%sections_mem_size = bief1%nprof
    ! on alloue sections meme si self%sections_mem_size == 0
    allocate(bief1%sections(bief1%sections_mem_size))

    write(bief2%name,*) self%name//'2'
    bief2%amont = self%amont
    bief2%aval = self%aval
    bief2%fichier_geo = self%fichier_geo
    bief2%nam = self%nam
    bief2%nav = self%nav
    bief2%gcd = self%gcd
    bief2%ibg = self%ibg
    bief2%ibd = self%ibd
    bief2%ibc = self%ibc
    bief2%is1 = self%is1
    bief2%is2 = self%is2
    bief2%nprof = self%nprof
    bief2%sections_mem_size = bief2%nprof
    ! on alloue sections meme si self%sections_mem_size == 0
    allocate(bief2%sections(bief2%sections_mem_size))

    do i=1, self%nprof
      call self%sections_ptr(i)%split(bief1%sections(i), bief2%sections(i), tag)
    enddo
    bief1%sections_ptr(1:) => bief1%sections(1:)
    bief2%sections_ptr(1:) => bief2%sections(1:)

  end subroutine split_bief


  subroutine extract_bief(self, bief_out, tag1, tag2)
  ! extract a bief zone between 2 headlines. Headline points are kept.
    ! prototype
    class(bief), intent(in) :: self
    type(bief), intent(out), target :: bief_out
    character(len=3), intent(in) :: tag1, tag2
    ! variables locales
    integer :: i

    if(tag1 == tag2 .and. trim(tag1).ne.'')then
      write(l9,'(a)')  ' >>>> ERREUR dans extract_bief() : tags identiques'
      stop ' >>>> ERREUR dans extract_bief()'
    endif

!     allocate(bief::bief_out)
    write(bief_out%name,*) trim(self%name)//'1'
    bief_out%amont = self%amont
    bief_out%aval = self%aval
    bief_out%fichier_geo = self%fichier_geo
    bief_out%nam = self%nam
    bief_out%nav = self%nav
    bief_out%gcd = self%gcd
    bief_out%ibg = self%ibg
    bief_out%ibd = self%ibd
    bief_out%ibc = self%ibc
    bief_out%is1 = self%is1
    bief_out%is2 = self%is2
    bief_out%nprof = self%nprof
    bief_out%sections_mem_size = bief_out%nprof
    ! on alloue sections meme si self%sections_mem_size == 0
    allocate(bief_out%sections(bief_out%sections_mem_size))

    do i=1, self%nprof
      call self%sections_ptr(i)%extract(bief_out%sections(i), tag1, tag2)
    enddo
    bief_out%sections_ptr(1:) => bief_out%sections(1:)
  end subroutine extract_bief


  subroutine patch_bief(self, bief_in, tag1, tag2)
  ! patch a bief zone between 2 headlines.
    ! prototype
    class(bief), intent(inout) :: self
    type(bief), intent(in), target :: bief_in
    character(len=3), intent(in) :: tag1, tag2
    ! variables locales
    integer :: i

    if(bief_in%nprof .ne. self%nprof)then
      write(l9,'(a)')  ' >>>> ERREUR dans patch_bief() : nombres de profils différents'
      stop ' >>>> ERREUR dans patch_bief()'
    endif

    do i=1, self%nprof
      call self%sections_ptr(i)%patch(bief_in%sections(i), tag1, tag2)
    enddo
  end subroutine patch_bief


  function get_max_3D_length(self)
    implicit none
    ! prototype
    class(bief), intent(in) :: self
    real(kind=long) :: get_max_3D_length
    ! variables locales
    integer :: i
    real(kind=long) :: length

    get_max_3D_length = self%sections_ptr(1)%length3D()
    do i=2, self%nprof
      length = self%sections_ptr(i)%length3D()
      if (length > get_max_3D_length) get_max_3D_length = length
    enddo
  end function get_max_3D_length


  function get_min_3D_length(self)
    implicit none
    ! prototype
    class(bief), intent(in) :: self
    real(kind=long) :: get_min_3D_length
    ! variables locales
    integer :: i
    real(kind=long) :: length

    get_min_3D_length = self%sections_ptr(1)%length3D()
    do i=2, self%nprof
      length = self%sections_ptr(i)%length3D()
      if (length < get_min_3D_length) get_min_3D_length = length
    enddo
  end function get_min_3D_length


  function get_mean_3D_length(self)
    implicit none
    ! prototype
    class(bief), intent(in) :: self
    real(kind=long) :: get_mean_3D_length
    ! variables locales
    integer :: i

    get_mean_3D_length = self%sections_ptr(1)%length3D()
    do i=2, self%nprof
      get_mean_3D_length = get_mean_3D_length + self%sections_ptr(i)%length3D()
    enddo
    get_mean_3D_length = get_mean_3D_length/self%nprof
  end function get_mean_3D_length


  function get_max_2D_length(self)
    implicit none
    ! prototype
    class(bief), intent(in) :: self
    real(kind=long) :: get_max_2D_length
    ! variables locales
    integer :: i
    real(kind=long) :: length

    get_max_2D_length = self%sections_ptr(1)%length2D()
    do i=2, self%nprof
      length = self%sections_ptr(i)%length2D()
      if (length > get_max_2D_length) get_max_2D_length = length
    enddo
  end function get_max_2D_length


  function get_min_2D_length(self)
    implicit none
    ! prototype
    class(bief), intent(in) :: self
    real(kind=long) :: get_min_2D_length
    ! variables locales
    integer :: i
    real(kind=long) :: length

    get_min_2D_length = self%sections_ptr(1)%length2D()
    do i=2, self%nprof
      length = self%sections_ptr(i)%length2D()
      if (length < get_min_2D_length) get_min_2D_length = length
    enddo
  end function get_min_2D_length


  function get_mean_2D_length(self)
    implicit none
    ! prototype
    class(bief), intent(in) :: self
    real(kind=long) :: get_mean_2D_length
    ! variables locales
    integer :: i

    get_mean_2D_length = self%sections_ptr(1)%length2D()
    do i=2, self%nprof
      get_mean_2D_length = get_mean_2D_length + self%sections_ptr(i)%length2D()
    enddo
    get_mean_2D_length = get_mean_2D_length/self%nprof
  end function get_mean_2D_length


  subroutine st_to_m_compl(self, npoints)
  ! find the section with the maximum number of points
  ! or use the given number of points if present
  ! modif all other sections in the bief to match this number
    ! prototype
    class(bief), intent(inout), target :: self
    integer, intent(in) :: npoints
    ! variables locales
    integer :: nbptst, isectmax, npoint_max, isect, i, j, k, nbetamax, imax, j1, j0
    real(kind=long) :: lcum, ltot, beta1, coef, ratio
    real(kind=long), dimension(:), allocatable :: alpha, beta
    integer, dimension(:), allocatable ::nplus, nbeta
    type(point3D), dimension(:), allocatable :: xyz
    logical :: trouve, undeplus

    ! on cherche ta section avec le plus de points
    isectmax = 1
    npoint_max = 0
    do isect=1, self%nprof
      if(npoint_max < self%sections_ptr(isect)%np) then
        npoint_max = self%sections_ptr(isect)%np
        isectmax = isect
      endif
    enddo

    if (npoint_max < npoints)then
      ! on complète la section max
      nbptst = npoints
      allocate(alpha(nbptst-1))
      allocate(nbeta(nbptst-1))
      nbeta = 0
      allocate(nplus(nbptst-1))

      allocate(point3D::xyz(nbptst))

      nplus(:) = 0
      ! on recherche les longueurs relatives ou mettre des points en plus
      isect=isectmax
      lcum = 0.0
      ltot = self%sections_ptr(isect)%length3D()
      do i=1,self%sections_ptr(isect)%np-1
        lcum = lcum + distance3D(self%sections_ptr(isect)%xyz(i), &
                                 self%sections_ptr(isect)%xyz(i+1))
        alpha(i)=lcum/ltot
      end do

      do isect=1, self%nprof
        lcum = 0.0
        ltot = self%sections_ptr(isect)%length3d()
        do i=1,self%sections_ptr(isect)%np-1
          lcum = lcum + distance3D(self%sections_ptr(isect)%xyz(i), &
                                   self%sections_ptr(isect)%xyz(i+1))
          beta1=lcum/ltot
          do k=1,npoint_max-1
            if(alpha(k) .ge. beta1) nbeta(k) = nbeta(k)+1
          end do
!           print*, "j=", j, "size(nbeta)=", size(nbeta)
!           nbeta(j) = nbeta(j)+1
        end do
      end do
!     nombre de valeurs dans chaque intervalle
      do k=npoint_max-1,2,-1
        nbeta(k) = nbeta(k)-nbeta(k-1)
      enddo

!     boucle sur le nombre de points a rajouter
      do j=1,npoints-npoint_max
          nbetamax=0
          do i=1,nbptst-1
              if(nbeta(i).gt.nbetamax)then
                  nbetamax=nbeta(i)
                  imax=i
              end if
          end do
          nplus(imax)=nplus(imax)+1
          nbeta(imax)=nbeta(imax)*nplus(imax)/(nplus(imax)+1)
      end do ! fin boucle sur j

!     rajouter les points dans STI
      k=1
      isect=isectmax
      do j=1,npoint_max-1
        xyz(k) = self%sections_ptr(isect)%xyz(j)
        k = k+1
        do i=1,nplus(j)
          coef=float(i)/float(nplus(j)+1)
          xyz(k)%x = (1.0-coef)*self%sections_ptr(isect)%xyz(j)%x + coef*self%sections_ptr(isect)%xyz(j+1)%x
          xyz(k)%y = (1.0-coef)*self%sections_ptr(isect)%xyz(j)%y + coef*self%sections_ptr(isect)%xyz(j+1)%y
          xyz(k)%z = (1.0-coef)*self%sections_ptr(isect)%xyz(j)%z + coef*self%sections_ptr(isect)%xyz(j+1)%z
          xyz(k)%tag='   '
          k = k+1
        end do
      end do
      xyz(nbptst) = self%sections_ptr(isect)%xyz(npoint_max)
      call move_alloc(xyz, self%sections_ptr(isect)%xyz)
      self%sections_ptr(isect)%xyz_size = size(self%sections_ptr(isect)%xyz)
      self%sections_ptr(isect)%np = nbptst
      deallocate(alpha)
      deallocate(nbeta)
      deallocate(nplus)
    else ! si complsmax est faux on n'a pas a completer
      nbptst = npoint_max
!       allocate(xyz, source=self%sections_ptr(isectmax)%xyz)
    endif ! fin du if sur complsmax

    allocate(xyz(1))
    xyz(1)%tag='   '
    allocate(alpha(nbptst-1))
    allocate(beta(0:nbptst-1))
    do isect=1,self%nprof
      self%sections_ptr(isect)%xyz = [self%sections_ptr(isect)%xyz,[(xyz,i=self%sections_ptr(isect)%np+1,nbptst)]]
      self%sections_ptr(isect)%xyz_size = size(self%sections_ptr(isect)%xyz)
    enddo

!   POUR TOUTES LES SECTIONS DE DONNEES:
!   on commence par nmax et on av dabord vers plus et ensuite vers moins
    do isect=isectmax+1,self%nprof

      ltot = self%sections_ptr(isect)%length3D()
      if(self%sections_ptr(isect)%np .le. 1 .or. ltot .lt. 0.0001)then
        ! point unique (nbptst fois)
        do i=2,nbptst
          self%sections_ptr(isect)%xyz(i) = self%sections_ptr(isect)%xyz(1)
          self%sections_ptr(isect)%xyz(i)%tag = '   '
        enddo
        self%sections_ptr(isect)%np=nbptst
      elseif(self%sections_ptr(isect-1)%length3d().lt. 0.0001)then
        call self%sections_ptr(isect)%interpolate_points_npoints(nbptst, lplan=.false., keep=.true.)
      else ! cas normal
!           on calcule les ratios sur nost-1
!           ratios cibles pour nost
        lcum = 0.0
        ltot = self%sections_ptr(isect-1)%length3D()
        do i=1,self%sections_ptr(isect-1)%np-1
          lcum = lcum + distance3D(self%sections_ptr(isect-1)%xyz(i), &
                                   self%sections_ptr(isect-1)%xyz(i+1))
          alpha(i)=lcum/ltot
        end do
        lcum = 0.0
        ltot = self%sections_ptr(isect)%length3D()
        beta(0)=0.0
        do i=1,self%sections_ptr(isect)%np-1
          lcum = lcum + distance3D(self%sections_ptr(isect)%xyz(i), &
                                   self%sections_ptr(isect)%xyz(i+1))
          beta(i)=lcum/ltot
        end do
        do i=self%sections_ptr(isect)%np, nbptst-1
          beta(i)=1.0
        enddo

        do k=1, nbptst-self%sections_ptr(isect)%np ! boucle sur les points à rajouter
          trouve = .false.
          do j0=1,self%sections_ptr(isect)%np-1
            if(trouve)exit
            undeplus=.true.
            do j1=j0,self%sections_ptr(isect)%np-1
              if(undeplus)then
                if(beta(j1).le.alpha(j1))then
                  undeplus=.false.
                  exit
                endif
              endif
            enddo

            if (undeplus) then
              if (j0.eq.self%sections_ptr(isect)%np-1) then
                trouve=.true.
              else
                j1=j0+1
                if (beta(j0)-alpha(j0).gt.alpha(j1)-beta(j0)) then ! beta(j0) .gt. (alpha(j1)+alpha(j2))/2
                  trouve=.true.
                end if
              end if ! fin du if sur j0=self%sections_ptr(isect)%np
            end if ! fin if sur undeplus


            if (trouve) then
              ! on rajoute un point en commencant par decaler ceux qui sont d'indice plus haut
              do j=self%sections_ptr(isect)%np,j0+1,-1
                self%sections_ptr(isect)%xyz(j+1) = self%sections_ptr(isect)%xyz(j)
                self%sections_ptr(isect)%xyz(j)%tag='   '
                beta(j)=beta(j-1)
              end do
              self%sections_ptr(isect)%np = self%sections_ptr(isect)%np+1

              if (beta(j0).eq.beta(j0-1)) then
!               point deja double, on le triple
!               en pratique rien a faire car le doublement a droite a deja ete fait
              else
                ratio=(alpha(j0)-beta(j0-1))/(beta(j0)-beta(j0-1))
                if (ratio.lt.0.) then
                  write(*,*)'ratio negatif',isect,j0
!                 ratio=0.
!                 on double le point a gauche
                  self%sections_ptr(isect)%xyz(j0) = self%sections_ptr(isect)%xyz(j0-1)
                  beta(j0)=beta(j0-1)
                  self%sections_ptr(isect)%xyz(j0)%tag='   '
                else if (ratio.lt.1.0-ratio) then
!                 if (ratio*(lcum(j0+1,nost)-lcum(j0,nost)).lt.pasm(nozn)) then
! !                 si distance trop petite on double le point a gauche
!
!                   xyz(1) = self%sections_ptr(isect)%xyz(j0-1)
!                   call self%sections_ptr(isect)%insert(xyz(1),position=j0)
!                   beta(j0)=beta(j0-1)
!                 else
                    self%sections_ptr(isect)%xyz(j0+1)%x &
                         = (1.0-ratio) * self%sections_ptr(isect)%xyz(j0)%x &
                         +       ratio * self%sections_ptr(isect)%xyz(j0+1)%x
                    self%sections_ptr(isect)%xyz(j0+1)%y &
                         = (1.0-ratio) * self%sections_ptr(isect)%xyz(j0)%y &
                         +       ratio * self%sections_ptr(isect)%xyz(j0+1)%y
                    self%sections_ptr(isect)%xyz(j0+1)%z &
                         = (1.0-ratio) * self%sections_ptr(isect)%xyz(j0)%z &
                         +       ratio * self%sections_ptr(isect)%xyz(j0+1)%z
                    self%sections_ptr(isect)%xyz(j0+1)%tag='   '
                    beta(j0)=(1.-ratio)*beta(j0-1)+ratio*beta(j0+1)
!                 end if
                else ! if(ratio.ge.1.-ratio)then
!                 if ((1.-ratio)*(lcum(j0+1,nost)-lcum(j0,nost)).lt.pasm(nozn)) then
! !                 si distance trop petite on double le point a droite
! !                 stix(noptidep+j0,nost)=stix(noptidep+j0,nost)
! !                 stiy(noptidep+j0,nost)=stiy(noptidep+j0,nost)
! !                 stiz(noptidep+j0,nost)=stiz(noptidep+j0,nost)
! !                 lcum(j0+1,nost)=lcum(j0+1,nost)
! !                 beta(j0)=beta(j0)
!                 else
                    self%sections_ptr(isect)%xyz(j0+1)%x &
                         = (1.0-ratio) * self%sections_ptr(isect)%xyz(j0)%x &
                         +       ratio * self%sections_ptr(isect)%xyz(j0+1)%x
                    self%sections_ptr(isect)%xyz(j0+1)%y &
                         = (1.0-ratio) * self%sections_ptr(isect)%xyz(j0)%y &
                         +       ratio * self%sections_ptr(isect)%xyz(j0+1)%y
                    self%sections_ptr(isect)%xyz(j0+1)%z &
                         = (1.0-ratio) * self%sections_ptr(isect)%xyz(j0)%z &
                         +       ratio * self%sections_ptr(isect)%xyz(j0+1)%z
                    beta(j0)=(1.-ratio)*beta(j0-1)+ratio*beta(j0+1)
                    self%sections_ptr(isect)%xyz(j0+1)%tag='   '
!                 end if
                end if ! fin du if sur ratio
              end if ! fin du if sur betaj0=betaj0-1
            end if ! fin du if sur trouve apres controle section precedente
          enddo
        enddo
        if(self%sections_ptr(isect)%np.ne.nbptst)then
#ifdef eng
!         à traduire
#else
          write(*,*)'section ',isect
          write(*,*)nbptst - self%sections_ptr(isect)%np,' points doubles en fin'
#endif
          do i=self%sections_ptr(isect)%np+1, nbptst
            self%sections_ptr(isect)%xyz(i)=self%sections_ptr(isect)%xyz(self%sections_ptr(isect)%np)
!             call self%sections_ptr(isect)%insert(xyz(1))
          enddo
        end if
      endif
    enddo

!   on commence par nmax et on av vers moins
    do isect=isectmax-1,1,-1
      ltot = self%sections_ptr(isect)%length3D()
      if(self%sections_ptr(isect)%np .le. 1 .or. ltot .lt. 0.0001)then
        ! point unique (nbptst fois)
        do i=2,nbptst
          self%sections_ptr(isect)%xyz(i) = self%sections_ptr(isect)%xyz(1)
          self%sections_ptr(isect)%xyz(i)%tag='   '
        enddo
        self%sections_ptr(isect)%np=nbptst
      elseif(self%sections_ptr(isect+1)%length3d().lt. 0.0001)then
        call self%sections_ptr(isect)%interpolate_points_npoints(nbptst, lplan=.false., keep=.true.)
      else ! cas normal
!           on calcule les ratios sur nost-1
!           ratios cibles pour nost
        lcum = 0.0
        ltot = self%sections_ptr(isect+1)%length3D()
        do i=1,self%sections_ptr(isect+1)%np-1
          lcum = lcum + distance3D(self%sections_ptr(isect+1)%xyz(i), &
                                   self%sections_ptr(isect+1)%xyz(i+1))
          alpha(i)=lcum/ltot
        end do
        lcum = 0.0
        ltot = self%sections_ptr(isect)%length3D()
        do i=1,self%sections_ptr(isect)%np-1
          lcum = lcum + distance3D(self%sections_ptr(isect)%xyz(i), &
                                   self%sections_ptr(isect)%xyz(i+1))
          beta(i)=lcum/ltot
        end do
        do i=self%sections_ptr(isect)%np, nbptst-1
          beta(i)=1.0
        enddo

        do k=1, nbptst-self%sections_ptr(isect)%np ! boucle sur les points à rajouter
          trouve = .false.
          do j0=1,self%sections_ptr(isect)%np-1
            if(trouve)exit
            undeplus=.true.
            do j1=j0,self%sections_ptr(isect)%np-1
              if(undeplus)then
                if(beta(j1).le.alpha(j1))then
                  undeplus=.false.
                  exit
                endif
              endif
            enddo

            if (undeplus) then
              if (j0.eq.self%sections_ptr(isect)%np-1) then
                trouve=.true.
              else
                j1=j0+1
                if (beta(j0)-alpha(j0).gt.alpha(j1)-beta(j0)) then ! beta(j0) .gt. (alpha(j1)+alpha(j2))/2
                  trouve=.true.
                end if
              end if ! fin du if sur j0=self%sections_ptr(isect)%np
            end if ! fin if sur undeplus


            if (trouve) then
              ! on rajoute un point en commencant par decaler ceux qui sont d'indice plus haut
              do j=self%sections_ptr(isect)%np,j0+1,-1
                self%sections_ptr(isect)%xyz(j+1) = self%sections_ptr(isect)%xyz(j)
                self%sections_ptr(isect)%xyz(j)%tag='   '
                beta(j)=beta(j-1)
              end do
              self%sections_ptr(isect)%np = self%sections_ptr(isect)%np+1

              if (beta(j0).eq.beta(j0-1)) then
!               point deja double, on le triple
!               en pratique rien a faire car le doublement a droite a deja ete fait
              else
                ratio=(alpha(j0)-beta(j0-1))/(beta(j0)-beta(j0-1))
                if (ratio.lt.0.) then
                  write(*,*)'ratio negatif',isect,j0
!                 ratio=0.
!                 on double le point a gauche
                  self%sections_ptr(isect)%xyz(j0) = self%sections_ptr(isect)%xyz(j0-1)
                  self%sections_ptr(isect)%xyz(j0)%tag='   '
                  beta(j0)=beta(j0-1)
                else if (ratio.lt.1.0-ratio) then
!                 if (ratio*(lcum(j0+1,nost)-lcum(j0,nost)).lt.pasm(nozn)) then
! !                 si distance trop petite on double le point a gauche
!
!                   xyz(1) = self%sections_ptr(isect)%xyz(j0-1)
!                   call self%sections_ptr(isect)%insert(xyz(1),position=j0)
!                   beta(j0)=beta(j0-1)
!                 else
                    self%sections_ptr(isect)%xyz(j0+1)%x &
                         = (1.0-ratio) * self%sections_ptr(isect)%xyz(j0)%x &
                         +       ratio * self%sections_ptr(isect)%xyz(j0+1)%x
                    self%sections_ptr(isect)%xyz(j0+1)%y &
                         = (1.0-ratio) * self%sections_ptr(isect)%xyz(j0)%y &
                         +       ratio * self%sections_ptr(isect)%xyz(j0+1)%y
                    self%sections_ptr(isect)%xyz(j0+1)%z &
                         = (1.0-ratio) * self%sections_ptr(isect)%xyz(j0)%z &
                         +       ratio * self%sections_ptr(isect)%xyz(j0+1)%z
                    self%sections_ptr(isect)%xyz(j0+1)%tag='   '
                    beta(j0)=(1.-ratio)*beta(j0-1)+ratio*beta(j0+1)
!                 end if
                else ! if(ratio.ge.1.-ratio)then
!                 if ((1.-ratio)*(lcum(j0+1,nost)-lcum(j0,nost)).lt.pasm(nozn)) then
! !                 si distance trop petite on double le point a droite
! !                 stix(noptidep+j0,nost)=stix(noptidep+j0,nost)
! !                 stiy(noptidep+j0,nost)=stiy(noptidep+j0,nost)
! !                 stiz(noptidep+j0,nost)=stiz(noptidep+j0,nost)
! !                 lcum(j0+1,nost)=lcum(j0+1,nost)
! !                 beta(j0)=beta(j0)
!                 else
                    self%sections_ptr(isect)%xyz(j0+1)%x &
                         = (1.0-ratio) * self%sections_ptr(isect)%xyz(j0)%x &
                         +       ratio * self%sections_ptr(isect)%xyz(j0+1)%x
                    self%sections_ptr(isect)%xyz(j0+1)%y &
                         = (1.0-ratio) * self%sections_ptr(isect)%xyz(j0)%y &
                         +       ratio * self%sections_ptr(isect)%xyz(j0+1)%y
                    self%sections_ptr(isect)%xyz(j0+1)%z &
                         = (1.0-ratio) * self%sections_ptr(isect)%xyz(j0)%z &
                         +       ratio * self%sections_ptr(isect)%xyz(j0+1)%z
                    self%sections_ptr(isect)%xyz(j0+1)%tag='   '
                    beta(j0)=(1.-ratio)*beta(j0-1)+ratio*beta(j0+1)
!                 end if
                end if ! fin du if sur ratio
              end if ! fin du if sur betaj0=betaj0-1
            end if ! fin du if sur trouve apres controle section precedente
          enddo
        enddo
        if(self%sections_ptr(isect)%np.ne.nbptst)then
#ifdef eng
!         à traduire
#else
          write(*,*)'section ',isect
          write(*,*)nbptst - self%sections_ptr(isect)%np,' points doubles en fin'
#endif
          do i=self%sections_ptr(isect)%np+1, nbptst
            xyz(1)=self%sections_ptr(isect)%xyz(self%sections_ptr(isect)%np)
            call self%sections_ptr(isect)%insert(xyz(1))
          enddo
        end if
      endif
    enddo
    deallocate(alpha)
    deallocate(beta)
    deallocate(xyz)

  end subroutine st_to_m_compl


  subroutine st_to_m_pasm(self, pasm, l, lplan)
  ! modif all sections in the bief to match a mean step on a reference legth.
  ! l : longueur de référence (min, max, moy, 3D ou 2D)
    ! prototype
    class(bief), intent(inout) :: self
    real(kind=long) :: pasm
    real(kind=long) :: l
    logical :: lplan
    ! variables locales
    integer :: nmailles

    nmailles = int(l/pasm)
    if(nmailles.eq.0) nmailles=1
    call self%st_to_m_nmailles(nmailles, lplan)
  end subroutine st_to_m_pasm


  subroutine st_to_m_nmailles(self, nmailles, lplan)
  ! modif all sections in the bief to match a number of cells.
    ! prototype
    class(bief), intent(inout) :: self
    logical, intent(in) :: lplan
    integer, intent(in) :: nmailles
    ! variables locales
    ! nptz : nombre de segments avant modif
    ! isectmax : indice de la section qui contient le maximum de points
    integer :: nbptst, i, j, k, l, nptz, np
    real(kind=long) :: pas
    type(point3D), dimension(:), allocatable :: xyz
    logical :: appart

    nbptst = nmailles+1

    do i=1,self%nprof
      if(lplan) then
        pas = self%sections_ptr(i)%length2D()
      else
        pas = self%sections_ptr(i)%length3D()
      endif
      np = self%sections_ptr(i)%np
      if (i.ne.1) then
        ! si les deux extremites de la zone  sont identiques a ceux de la section d'avant on met tout identique
        if (abs(self%sections_ptr(i)%xyz(1)%x  - self%sections_ptr(i-1)%xyz(1)%x) < 0.0001 .and. &
            abs(self%sections_ptr(i)%xyz(1)%y  - self%sections_ptr(i-1)%xyz(1)%y) < 0.0001 .and. &
            abs(self%sections_ptr(i)%xyz(1)%z  - self%sections_ptr(i-1)%xyz(1)%z) < 0.0001 .and. &
            abs(self%sections_ptr(i)%xyz(np)%x - self%sections_ptr(i-1)%xyz(1)%x) < 0.0001 .and. &
            abs(self%sections_ptr(i)%xyz(np)%y - self%sections_ptr(i-1)%xyz(1)%y) < 0.0001 .and. &
            abs(self%sections_ptr(i)%xyz(np)%z - self%sections_ptr(i-1)%xyz(1)%z) < 0.0001) then
          allocate(xyz, source=self%sections_ptr(i-1)%xyz)
          call move_alloc(xyz, self%sections_ptr(i)%xyz) ! xyz désalloué ici
          self%sections_ptr(i)%np = nbptst
          self%sections_ptr(i)%xyz_size = nbptst
          cycle
        endif
      endif

      if (pas.eq.0.) then ! cas particulier: zone de taille nulle: toutes les lignes passent par 1 pt
        allocate(point3D::xyz(nbptst))
        do j=1, nbptst
          xyz(j)=self%sections_ptr(i)%xyz(1)
        enddo
        call move_alloc(xyz, self%sections_ptr(i)%xyz) ! xyz désalloué ici
        self%sections_ptr(i)%np = nbptst
        self%sections_ptr(i)%xyz_size = nbptst
      else
        call self%sections_ptr(i)%interpolate_points_npoints(nbptst, lplan, .false.)
      endif

      if (i.ne.1) then
        do j=2,nmailles ! nouveau point
          appart=.false.
          do k=1,nmailles
            if (.not. appart) then
              appart =self%sections_ptr(i)%xyz(j)%appartseg(self%sections_ptr(i-1)%xyz(k),self%sections_ptr(i-1)%xyz(k+1))
              if (appart) then
                if (k.lt.j) then
                  do l = k+1, j
                    self%sections_ptr(i)%xyz(l) = self%sections_ptr(i-1)%xyz(l)
                  enddo
                else
                  do l = j, k
                    self%sections_ptr(i)%xyz(l) = self%sections_ptr(i-1)%xyz(l)
                  enddo
                endif
              endif
            endif
          enddo
        enddo
      endif
    enddo
  end subroutine st_to_m_nmailles

  subroutine purge(self)
    ! prototype
    class(bief), target :: self
    ! variables locales
    integer :: i
    do i=1, self%nprof
      call self%sections_ptr(i)%purge_points_alignes()
    enddo
  end subroutine purge

  subroutine update_pk(self, directrice1, directrice2, origine, origin_value, s)
  ! calcul des pk des sections du maillage
    implicit none
    ! prototype
    class(bief), target :: self
    character(len=3), intent(in) :: directrice1, directrice2
    integer, intent(in) :: origine
    real(kind=long), intent(in), optional :: origin_value
    integer, intent(in), optional :: s
    ! variables locales
    real(kind=long) :: dpk, sgn
    integer :: i

    if (origine > self%nprof .or. origine < 1) then
        write(error_unit,*) '>>>> Erreur dans update_pk : origine en dehors du bief'
        stop
    endif

    if (present(s)) then
        if (s == 1) then
            sgn = 1.0D0
        elseif (s == 2) then
            sgn = -1.0D0
        else
            sgn = dsign(un, self%sections_ptr(self%nprof)%pk - self%sections_ptr(1)%pk)
        endif
    else
        if (abs(self%sections_ptr(self%nprof)%pk - self%sections_ptr(1)%pk) < toutpetit) then
            sgn = 1.0D0
        else
            sgn = dsign(un, self%sections_ptr(self%nprof)%pk - self%sections_ptr(1)%pk)
        endif
    endif

    if (present(origin_value)) then
        self%sections_ptr(origine)%pk = origin_value
    endif

    if (origine < self%nprof) then
      do i=origine+1, self%nprof
        ! toujours distance 2D
        dpk = distanceH(self%sections_ptr(i-1)%get(directrice1),self%sections_ptr(i)%get(directrice1))&
            + distanceH(self%sections_ptr(i-1)%get(directrice2),self%sections_ptr(i)%get(directrice2))
        self%sections_ptr(i)%pk = self%sections_ptr(i-1)%pk + sgn * dpk /2
      enddo
    endif
    if (origine > 1) then
      do i=origine-1, 1, -1
        ! toujours distance 2D
        dpk = distanceH(self%sections_ptr(i+1)%get(directrice1),self%sections_ptr(i)%get(directrice1))&
            + distanceH(self%sections_ptr(i+1)%get(directrice2),self%sections_ptr(i)%get(directrice2))
        self%sections_ptr(i)%pk = self%sections_ptr(i+1)%pk - sgn * dpk /2
      enddo
    endif

  end subroutine update_pk

  subroutine output_bief(self, file_name)
    ! prototype
    class(bief), intent(in) :: self
    character(len=lname), intent(in) :: file_name
    ! variables locales
    integer :: i, j, u
    real(kind=long) :: pas
    type(point3D), pointer :: xyz

    open(newunit=u,file=trim(file_name),status='unknown')
    do i=1,self%nprof
      write(u,'(4(1x,i5),1x,f12.4,1x,a12)') i, 0, 0, self%sections_ptr(i)%np, self%sections_ptr(i)%pk, self%sections_ptr(i)%name
      do j=1,self%sections_ptr(i)%np
        xyz => self%sections_ptr(i)%xyz(j)
        write (u,'(3(1x,f12.4),1x,a3)') xyz%x,xyz%y,xyz%z,xyz%tag
      enddo
      write (u,'(3(1x,f12.4),1x,a3)') 999.999, 999.999, 0.0,' '
    enddo
    close(u)

  end subroutine output_bief

  subroutine output_bief_mascaret(self, file_name)
    ! prototype
    class(bief), intent(in) :: self
    character(len=lname), intent(in) :: file_name
    ! variables locales
    integer :: i, j, u
    real(kind=long) :: pas
    type(profilAC) :: pAC
    character(len=1) :: a

    open(newunit=u,file=trim(file_name),status='unknown')
    do i=1,self%nprof
      write(u,'(a6,1x,a,1x,a,1x,f0.4)') "PROFIL",trim(self%name),trim(self%sections_ptr(i)%name),self%sections_ptr(i)%pk
      pAC = self%sections_ptr(i)%projectionPlan()
      do j=1,self%sections_ptr(i)%np
        if (j < self%sections_ptr(i)%li(1)) then
          a = 'T'
        elseif (j > self%sections_ptr(i)%li(2)) then
          a = 'T'
        else
          a = 'B'
        endif
        write (u,'(2(f0.4,1x),1x,a1)') pAC%yz(j)%y,pAC%yz(j)%z,a
      enddo
    enddo
    close(u)

  end subroutine output_bief_mascaret

  subroutine adjust_banks(self, bord_gauche, bord_droit)
  ! modifie sections pour que les extremites correspondent aux traces fournies
  ! homothetie, translation et rotation
    use objet_section, only: init_profil, interpolate_points_npoints, correct
    implicit none
    ! prototype
    class(bief), intent(inout) :: self
    character(len=lname), intent(in) :: bord_droit, bord_gauche
    ! variables locales
    integer :: nfd,nfg, iostat1, iostat2, i, j
    integer, dimension(0:self%nb_lines+1) :: nlg
    logical, dimension(0:self%nb_lines+1) :: opt
    type(point3D), dimension(:), allocatable :: xyzlg, xyzld, xyz_tmp
    type(point3D), dimension(self%nprof,0:self%nb_lines+1) :: xyz
    logical :: trace
    type(profil_brut) :: bordd, bordg, borddn, bordgn
    character(len=14) :: ficin4,ficin5
    integer nf4,nf5
    real(kind=long), dimension(:), allocatable :: xaxe, yaxe, xperpen, yperpen
    real(kind=long) :: x, y, z

    iostat1 = 1
    iostat2 = 0
    if(trim(bord_gauche).ne.'')then
      open(newunit=nfg,file=trim(bord_gauche),status='old', iostat=iostat1)
      opt(0)=.true.
      i = 0
      do
        read(nfg,*,iostat=iostat2)
        if (iostat2 /= 0) exit
        i = i + 1
      end do
      rewind(nfg)
      allocate(xyzlg(i))
      do i=1,size(xyzlg)
        read(nfg,*,iostat=iostat2) x, y, z
        call xyzlg(i)%init(x, y, z)
      end do
      close(nfg)
    else
      opt(0)=.false.
      allocate(xyzlg(self%nprof))
      do i=1, self%nprof
        xyzlg(i) = self%sections_ptr(i)%xyz(1)
      enddo
    endif
!     call init_profil(bordg, 0.0d0, 'bordgauche', xyzlg)

    iostat1 = 1
    iostat2 = 0
    if(trim(bord_droit).ne.'')then
      open(newunit=nfd,file=trim(bord_droit),status='old', iostat=iostat1)
      opt(self%nb_lines+1)=.true.
      i = 0
      do
        read(nfd,*,iostat=iostat2)
        if (iostat2 /= 0) exit
        i = i + 1
      end do
      rewind(nfd)
      allocate(xyzld(i))
      do i=1,size(xyzld)
        read(nfd,*,iostat=iostat2) x, y, z
        call xyzld(i)%init(x, y, z)
      end do
      close(nfd)
    else
      opt(self%nb_lines+1)=.false.
      allocate(xyzld(self%nprof))
      do i=1, self%nprof
        xyzld(i) = self%sections_ptr(i)%get('np ')
      enddo
    endif
!     call init_profil(bordd, 0.0d0, 'borddroit', xyzld)

!   soit on a les traces des sections soit on les cree
    trace=.false.
    if(size(xyzld).eq.size(xyzlg))then
        if(size(xyzld).eq.self%nprof)then
!           on suppose que les points sont les traces des sections
            trace=.true.
        endif
    endif

    if(.not.trace)then
      call init_profil(bordgn, 0.0d0, 'bordgauche', xyzlg)
!       bordgn = bordg
      call init_profil(borddn, 0.0d0, 'borddroit', xyzld)
!       borddn = bordd
      call interpolate_points_npoints(bordgn,self%nprof,.true.,.false.)
      call interpolate_points_npoints(borddn,self%nprof,.true.,.false.)
      if (1 == 0)then
    !     etape supplementaire du 30/04/12
    !     on definit un axe et on suppose les sections perpendiculaires a cet axe
    !     axe est forme du milieu des points de meme numero
        allocate(xaxe(self%nprof), yaxe(self%nprof))
        allocate(xperpen(self%nprof), yperpen(self%nprof))
        do i=1,self%nprof
            xaxe(i)=0.5*(bordgn%xyz(i)%x+borddn%xyz(i)%x)
            yaxe(i)=0.5*(bordgn%xyz(i)%y+borddn%xyz(i)%y)
        end do
    !     on definit en chaque point de l'axe, la perpendiculiare en ce point
        xperpen(1)=yaxe(2)-yaxe(1)
        yperpen(1)=-(xaxe(2)-xaxe(1))
        do i=2,self%nprof-1
            xperpen(i)=yaxe(i+1)-yaxe(i-1)
            yperpen(i)=-(xaxe(i+1)-xaxe(i-1))
        end do
        xperpen(self%nprof)=yaxe(self%nprof)-yaxe(self%nprof-1)
        yperpen(self%nprof)=-(xaxe(self%nprof)-xaxe(self%nprof-1))
    !     on cree un deuxieme point pour definir l'axe de la section
        do i=1,self%nprof
            xperpen(i)=xperpen(i)+xaxe(i)
            yperpen(i)=yperpen(i)+yaxe(i)
        end do
    !     on retient les points ou cette perpendiculaire coupe les limites
        do i=2,self%nprof-1
            call croistgv(bordgn%xyz(i),xaxe(i),yaxe(i),xperpen(i),yperpen(i),xyzlg)
            call croistgv(borddn%xyz(i),xaxe(i),yaxe(i),xperpen(i),yperpen(i),xyzld)
        end do
        deallocate(xaxe, yaxe)
        deallocate(xperpen, yperpen)

    !     ecriture des fichiers resultats au format sem
        ficin4='limitegnew.sem'
        open(newunit=nf4,file=ficin4,status='unknown')
        do i=1,self%nprof
            write(nf4,'(3f13.4)')bordgn%xyz(i)%x,bordgn%xyz(i)%y,bordgn%xyz(i)%z
        end do
        close(nf4)

        ficin5='limitednew.sem'
        open(newunit=nf5,file=ficin5,status='unknown')
        do i=1,self%nprof
            write(nf5,'(3F13.4)')borddn%xyz(i)%x,borddn%xyz(i)%y,borddn%xyz(i)%z
        end do
        close(nf5)
      endif ! 1==0
    end if ! trace

!     do i=1, self%nprof
!         print*,"bordgn", bordgn%xyz(i)%x, bordgn%xyz(i)%y
!     enddo
!     do i=1, self%nprof
!         print*,"borddn", borddn%xyz(i)%x, borddn%xyz(i)%y
!     enddo

!   correction section par section
!   version b: premiere et derniere sections exclues car mauvais resultats
    do i=1,self%nprof
      call correct(self%sections_ptr(i),bordgn%xyz(i), borddn%xyz(i))
    end do

  end subroutine adjust_banks

  ! #####################################################
  subroutine intdro (x,y,z,x1,y1,x2,y2,p3,p4)
  ! #####################################################
  ! calcul de l'intersection de deux droites
    implicit none

    real(kind=long), intent(out) :: x,y,z
    real(kind=long), intent(in) :: x1,y1,x2,y2
    type(point3D), intent(in) :: p3, p4
    real(kind=long) :: a1,a2,az,x3,x4,y3,y4,z3,z4

    x3=p3%x
    y3=p3%y
    z3=p3%z
    x4=p4%x
    y4=p4%y
    z4=p4%z

    if(abs(x2-x1).lt.0.001)then
        if(abs(x4-x3).lt.0.001)then
            x=9999999.999
            y=9999999.999
            z=9999999.999
        elseif(abs(y4-y3).lt.0.001)then
            x=9999999.999
            y=9999999.999
            z=9999999.999
        else
            x=x1
            a2=(y4-y3)/(x4-x3)
            y=y3+a2*(x-x3)
            az=(Z4-Z3)/(x4-x3)
            z=z3+az*(x-x3)
        endif

    elseif(abs(x4-x3).lt.0.001)then
        if(abs(y4-y3).lt.0.001)then
            x=9999999.999
            y=9999999.999
            z=9999999.999
        else
            x=x3
            a1=(y2-y1)/(x2-x1)
            y=y1+a1*(x-x1)
            az=(Z4-Z3)/(y4-y3)
            z=z3+az*(y-y3)
        endif

    else
        a1=(y2-y1)/(x2-x1)
        a2=(y4-y3)/(x4-x3)
        az=(Z4-Z3)/(x4-x3)
        if(abs(a1-A2).gt.0.0001)then
            x=(y3-y1+a1*x1-a2*x3)/(a1-a2)
            y=y3+a2*(x-x3)
            z=z3+az*(x-x3)
        else
            x=9999999.999
            y=9999999.999
            z=9999999.999
        endif
    endif
  return

  end ! SUBROUTINE INTDRO

  ! ########################################################
  subroutine croistgv(point_out,xaxe,yaxe,xperpen,yperpen,bord)
  ! ########################################################
  ! calcul de l'intersection de une droite et de plusieurs droites
    implicit none

    real(kind=long) :: xaxe,yaxe,xperpen,yperpen,dist,dist0,x,y,z
    logical :: encore
    integer :: i,n
    type(point3D),intent(out) :: point_out
    type(point3D) :: p3, p4
    type(point3D), dimension(:), intent(in) :: bord

    point_out%x=999999.999
    point_out%y=999999.999
    point_out%z=999999.999
    dist0=10000.
    n = size(bord)
    do i=1,n-1
      p3=bord(i)
      p4=bord(i+1)

      call intdro (x,y,z,xaxe,yaxe,xperpen,yperpen,p3,p4)
      if(x.le.max(p3%x,p4%x))then
        if(x.ge.min(p3%x,p4%x))then
          if(y.le.max(p3%y,p4%y))then
            if(y.ge.min(p3%y,p4%y))then
              dist=sqrt((x-xaxe)**2+(y-yaxe)**2)
              if (dist.lt.dist0)then
                point_out%x=x
                point_out%y=y
                point_out%z=z
                dist0=dist
              endif
            endif
          endif
        endif
      endif
    enddo

  end ! SUBROUTINE CROISTGV

  subroutine deallocate_bief(self)
     class(bief), target, intent(inout) :: self
     integer :: i
     self%sections_ptr => null()
     if (allocated(self%sections)) then
        do i=1, self%nprof
            call self%sections(i)%remove_profil()
        enddo
        deallocate(self%sections)
     endif
     self%nprof = 0
     self%nb_lines = 0
     if (allocated(self%lines_tags))deallocate(self%lines_tags)
  end subroutine deallocate_bief

end module objet_bief
