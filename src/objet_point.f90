!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - INRAE - 1989-2020                           #
!# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################

module objet_point

  use, intrinsic :: iso_fortran_env, only: error_unit, output_unit
  use Parametres, only: long, precision_alti
  use Mage_Utilitaires, only: between, do_crash

  implicit none

  !===========================!
  !=== couche_sedimentaire ===!
  !===========================!

  type couche_sedimentaire
    real(kind=long) :: zc    !cote du plancher de la couche ; le toit de la couche (donc son épaisseur)
                             !est défini parle plancher de la couche précédente (supérieure).
    real(kind=long) :: d50   !valeur locale du d50
    real(kind=long) :: sigma !valeur locale de l'étendue granulométrique
    real(kind=long) :: tau   !valeur locale de la contrainte de mise en mouvement
    contains
      procedure, private :: egal => clone_cs
      procedure, private :: init => init_cs
      generic :: assignment (=) => egal
      procedure :: write_cs_unformated => write_cs_unformated
      procedure :: read_cs_unformated => read_cs_unformated
      procedure :: write_cs_formated => write_cs_formated
      procedure :: read_cs_formated => read_cs_formated
      generic   :: write(unformatted) => write_cs_unformated
      generic   :: read(unformatted) => read_cs_unformated
      generic   :: write(formatted) => write_cs_formated
      generic   :: read(formatted) => read_cs_formated
  end type couche_sedimentaire

  interface couche_sedimentaire
    module procedure constructor_cs
  end interface

  !===============!
  !=== pointLC ===!
  !===============!

  !type pour la tabulation largeur-cote
  type pointLC
    real(kind=long) :: h  !tirant d'eau (profondeur)
    real(kind=long) :: l  !largeur au miroir ; tient compte de la présence d'iles : l(n) <= abs(yd(n)-yg(n))
    real(kind=long) :: s  !section mouillée ; tient compte de la présence d'iles
    real(kind=long) :: p  !périmètre mouillé ; tient compte de la présence d'iles
    contains
      procedure, private :: egal => clone_pointLC
      procedure, private :: init => init_pointLC
      generic :: assignment (=) => egal
  end type pointLC

  interface pointLC
    module procedure constructor_pointLC
  end interface

  !===============!
  !=== pointAC ===!
  !===============!

  !type de point pour la projection abscisses-cotes d'un profil XYZ
  type pointAC
    real(kind=long) :: y  !abscisse en travers
    real(kind=long) :: z  !altitude
    contains
      procedure, private :: egal => clone_pointAC
      procedure, private :: init => init_pointAC
      generic :: assignment (=) => egal
  end type pointAC

  interface pointAC
    module procedure constructor_pointAC
  end interface

  !===============!
  !=== point3D ===!
  !===============!

  !type générique pour les points en XYZ bruts
  ! NOTE: si on fait de point3D une extension de point2D, on obtient des résultats légèrement différent sur le cas-test Ardèche complet
  ! NOTE: on garde point3D dans ce module à cause de l'attribut tag qui en fait un objet moins générique que point2D
  type point3D
    character(len=3) :: tag      !étiquette du point ; permet de définir globalement les lits (mêmes tags)
    real(kind=long) :: x, y      !coordonnées géographiques du point
    real(kind=long) :: z         !altitude du point
    integer :: nbcs ! nombre de couches sédimentaires
    type(couche_sedimentaire), allocatable, dimension(:) :: cs
    contains
      procedure, private :: egal => clone_point3D
      procedure :: init => init_point3D
      generic :: assignment (=) => egal
      procedure :: write_point3D_unformated => write_point3D_unformated
      procedure :: read_point3D_unformated => read_point3D_unformated
      procedure :: write_point3D_formated => write_point3D_formated
      procedure :: read_point3D_formated => read_point3D_formated
      generic   :: write(unformatted) => write_point3D_unformated
      generic   :: read(unformatted) => read_point3D_unformated
      generic   :: write(formatted) => write_point3D_formated
      generic   :: read(formatted) => read_point3D_formated
      procedure :: projectionOrtho
      procedure :: appartseg
      procedure :: dist_from_seg
  end type point3D

  interface point3D
    module procedure constructor_point3D
  end interface

  interface removeDoublons
    module procedure removeDoublons3D
  end interface

  !===========================================================================!
  !                                                                           !
  contains                                                                    !
  !                                                                           !
  !===========================================================================!

  !===========================!
  !=== couche_sedimentaire ===!
  !===========================!

  subroutine init_cs(self, zc, d50, sigma, tau)
    implicit none
    ! prototype
    class(couche_sedimentaire) :: self
    real(kind=long) :: zc, d50, sigma, tau

    self%zc = zc
    self%d50 = d50
    self%sigma = sigma
    self%tau = tau
  end subroutine init_cs


  function constructor_cs(zc, d50, sigma, tau)
    implicit none
    ! prototype
    type(couche_sedimentaire) :: constructor_cs
    real(kind=long) :: zc, d50, sigma, tau

    call init_cs(constructor_cs, zc, d50, sigma, tau)
  end function constructor_cs


  ! renvoie une copie de ct
  subroutine clone_cs(self,ct)
    implicit none
    ! prototype
    class(couche_sedimentaire), intent(out) :: self
    class(couche_sedimentaire), intent(in) :: ct

    self%zc = ct%zc
    self%d50 = ct%d50
    self%sigma = ct%sigma
    self%tau = ct%tau
  end subroutine clone_cs


  subroutine write_cs_unformated(self, unit, iostat, iomsg)
    implicit none
    class(couche_sedimentaire), intent(in)    :: self
    integer, intent(in)         :: unit
    integer, intent(out)        :: iostat
    character(*), intent(inout) :: iomsg

    write(unit, iostat=iostat, iomsg=iomsg) self%zc,self%d50,self%sigma,self%tau
  end subroutine write_cs_unformated


  subroutine read_cs_unformated(self, unit, iostat, iomsg)
    implicit none
    class(couche_sedimentaire), intent(inout)    :: self
    integer, intent(in)         :: unit
    integer, intent(out)        :: iostat
    character(*), intent(inout) :: iomsg

    read(unit, iostat=iostat, iomsg=iomsg) self%zc,self%d50,self%sigma,self%tau
  end subroutine read_cs_unformated


  subroutine write_cs_formated (self, unit, iotype, v_list, iostat, iomsg)
    implicit none
    class(couche_sedimentaire), intent(in) :: self
    integer, intent(in) :: unit
    character(*), intent(in) :: iotype
    integer, intent(in)  :: v_list(:)
    integer, intent(out) :: iostat
    character(*), intent(inout) :: iomsg

    write (unit, *, iostat=iostat) self%zc,self%d50,self%sigma,self%tau
  end subroutine write_cs_formated


  subroutine read_cs_formated (self, unit, iotype, v_list, iostat, iomsg)
    implicit none
    class(couche_sedimentaire), intent(inout) :: self
    integer, intent(in) :: unit
    character(*), intent(in) :: iotype
    integer, intent(in)  :: v_list(:)
    integer, intent(out) :: iostat
    character(*), intent(inout) :: iomsg

    read (unit, *, iostat=iostat) self%zc,self%d50,self%sigma,self%tau
  end subroutine read_cs_formated

  !===============!
  !=== pointLC ===!
  !===============!

  subroutine init_pointLC(self, h, l, s, p)
    implicit none
    ! prototype
    class(pointLC) :: self
    real(kind=long) :: h, l, s, p

    self%h = h
    self%l = l
    self%s = s
    self%p = p
  end subroutine init_pointLC


  function constructor_pointLC(h, l, s, p)
    implicit none
    ! prototype
    type(pointLC) :: constructor_pointLC
    real(kind=long) :: h, l, s, p

    call init_pointLC(constructor_pointLC, h, l, s, p)
  end function constructor_pointLC


  ! renvoie une copie de pt
  subroutine clone_pointLC(self,pt)
    implicit none
    ! prototype
    class(pointLC), intent(out) :: self
    class(pointLC), intent(in) :: pt

    self%h = pt%h
    self%l = pt%l
    self%s = pt%s
    self%p = pt%p
  end subroutine clone_pointLC

  !===============!
  !=== pointAC ===!
  !===============!

  subroutine init_pointAC(self, y, z)
    implicit none
    ! prototype
    class(pointAC) :: self
    real(kind=long) :: y, z

    self%y = y
    self%z = z
  end subroutine init_pointAC


  function constructor_pointAC(y, z)
    implicit none
    ! prototype
    type(pointAC) :: constructor_pointAC
    real(kind=long) :: y, z

    call init_pointAC(constructor_pointAC, y, z)
  end function constructor_pointAC


  ! renvoie une copie de pt
  subroutine clone_pointAC(self,pt)
    implicit none
    ! prototype
    class(pointAC), intent(out) :: self
    class(pointAC), intent(in) :: pt

    self%y = pt%y
    self%Z = pt%Z
  end subroutine clone_pointAC

  !=========================!
  !=== point 3D generique===!
  !=========================!

  subroutine init_point3D(self, x, y, z, tag, nbcs, cs)
    implicit none
    ! prototype
    class(point3D) :: self
    real(kind=long) :: x, y, z
    character(len=3), optional :: tag
    integer, optional :: nbcs
    integer :: i
    type(couche_sedimentaire), allocatable, dimension(:), optional :: cs

    self%x = x
    self%y = y
    self%z = z
    if (present(tag)) then
      self%tag = trim(tag)
    else
      self%tag = ''
    endif

    if(present(cs)) then
      ! size(cs) prend priorité sur nbcs (choix au hasard)
      self%nbcs = size(cs)
      allocate(self%cs, source=cs)
    else
      if(present(nbcs)) then
        self%nbcs = nbcs
        allocate(self%cs(nbcs))
      else
        self%nbcs = 1
        allocate(self%cs(1))
      endif
      do i=1,size(self%cs)
        self%cs(i)%zc = z
        self%cs(i)%d50 = 0.001_long
        self%cs(i)%sigma = 3._long
        self%cs(i)%tau = -1._long
      enddo
    endif
  end subroutine init_point3D


  ! renvoie une copie de pt
  subroutine clone_point3D(self,pt)
    implicit none
    ! prototype
    class(point3D), intent(out) :: self
    class(point3D), intent(in) :: pt

    self%x = pt%x
    self%y = pt%y
    self%z = pt%z
    self%tag = pt%tag
    self%nbcs = pt%nbcs
    if(allocated(self%cs)) deallocate(self%cs)
    if (allocated(pt%cs)) then
        allocate (self%cs, source = pt%cs)
    else
        allocate (self%cs(pt%nbcs))
    endif
  end subroutine clone_point3D


  subroutine write_point3D_unformated(self, unit, iostat, iomsg)
    implicit none
    class(point3D), intent(in)    :: self
    integer, intent(in)         :: unit
    integer, intent(out)        :: iostat
    character(*), intent(inout) :: iomsg
    write(unit, iostat=iostat, iomsg=iomsg) self%x,&
                                            self%y,&
                                            self%z,&
                                            self%tag,&
                                            self%nbcs,&
                                            self%cs(:)
  end subroutine write_point3D_unformated


  subroutine read_point3D_unformated(self, unit, iostat, iomsg)
    implicit none
    class(point3D), intent(inout)    :: self
    integer, intent(in)         :: unit
    integer, intent(out)        :: iostat
    character(*), intent(inout) :: iomsg

    read(unit, iostat=iostat, iomsg=iomsg) self%x,&
                                           self%y,&
                                           self%z,&
                                           self%tag,&
                                           self%nbcs,&
                                           self%cs(:)
  end subroutine read_point3D_unformated


  subroutine write_point3D_formated (self, unit, iotype, v_list, iostat, iomsg)
    implicit none
    class(point3D), intent(in) :: self
    integer, intent(in) :: unit
    character(*), intent(in) :: iotype
    integer, intent(in)  :: v_list(:)
    integer, intent(out) :: iostat
    character(*), intent(inout) :: iomsg

    write (unit, *, iostat=iostat) self%x,&
                                   self%y,&
                                   self%z,&
                                   self%tag,&
                                   self%nbcs,&
                                   self%cs(:)
  end subroutine write_point3D_formated


  subroutine read_point3D_formated (self, unit, iotype, v_list, iostat, iomsg)
    implicit none
    class(point3D), intent(inout) :: self
    integer, intent(in) :: unit
    character(*), intent(in) :: iotype
    integer, intent(in)  :: v_list(:)
    integer, intent(out) :: iostat
    character(*), intent(inout) :: iomsg

    write (unit, *, iostat=iostat) self%x,&
                                   self%y,&
                                   self%z,&
                                   self%tag,&
                                   self%nbcs,&
                                   self%cs(:)
  end subroutine read_point3D_formated


  function projectionOrtho(self, xyz1, xyz2)
  !==============================================================================
  !     projection orthogonale du point self sur la droite [xyz1,xyz2]
  !
  ! NB : dans cette version on ne tient pas compte de la pente du profil en long

  !entrée : xyz1, xyz2 et self, 3 point3D
  !sortie : la projection orthogonale, de type pointAC
  !
  !==============================================================================
    ! prototype
    class(point3D), intent(in) :: xyz1, xyz2, self
    type(pointAC) :: projectionOrtho
    ! variables locales
    real(kind=long) :: x21, y21, a

    ! calcul
    x21 = xyz2%x - xyz1%x
    y21 = xyz2%y - xyz1%y
    a   = x21*x21 + y21*y21
    if (a > 0.000001_long) then
      projectionOrtho%y = ((self%x - xyz1%x)*x21 + (self%y - xyz1%y)*y21) / sqrt(a)
    else
      projectionOrtho%y = 0._long
    endif
    projectionOrtho%z = self%z
  end function projectionOrtho


  function appartseg(self, xyz1, xyz2)
  ! regarde si le point self est sur le segment [xyz1 ; xyz2]
    ! prototype
    class(point3D), intent(in) :: self, xyz1, xyz2
    logical :: appartseg
    ! variables localse
    real(kind=long) :: car, dist, alp

    appartseg=.false.
    car=(xyz2%x-xyz1%x)**2+(xyz2%y-xyz1%y)**2+(xyz2%z-xyz1%z)**2
    if(car.gt.0.0000001)then
      alp=(xyz1%x-xyz2%x)*(self%x-xyz1%x)+(xyz1%y-xyz2%y)*(self%y-xyz1%y)+(xyz1%z-xyz2%z)*(self%z-xyz1%z)
      alp=-alp/car
      if(alp.gt.0.00001.and.alp.lt.0.99999)then
        dist = (self%x-xyz1%x-alp*(xyz2%x-xyz1%x))**2+ &
               (self%y-xyz1%y-alp*(xyz2%y-xyz1%y))**2+ &
               (self%z-xyz1%z-alp*(xyz2%z-xyz1%z))**2
        if(dist.lt.0.00000001)then
          appartseg=.true.
        endif
      endif
    ! si les deux points sont confondus
    else
      car=(self%x-xyz1%x)**2+(self%y-xyz1%y)**2+(self%z-xyz1%z)**2
      if(car.lt.0.00000001)then
        appartseg=.true.
      endif
    endif
    return
  end function appartseg

  function dist_from_seg(self, xyz1, xyz2)
  ! calcule la distance minimum entre self et le segment [xyz1 ; xyz2]
    ! prototype
    class(point3D), intent(in) :: self, xyz1, xyz2
    real(kind=long) :: dist_from_seg
    ! variables localse
    real(kind=long) :: a2, b2, c2
    a2 = distance3Dcarre(self, xyz1)
    b2 = distance3Dcarre(self, xyz2)
    c2 = distance3Dcarre(xyz1, xyz2)
    if(c2.lt.0.00000001)then
        ! si les deux points sont confondus
        dist_from_seg = 0.0
    else
        dist_from_seg = sqrt(abs(a2 - ((c2 - b2 + a2) / (2*sqrt(c2)))**2))
    endif
  end function dist_from_seg

  !===============!
  !=== point3D ===!
  !===============!

  function constructor_point3D(x, y, z, tag, nbcs, cs)
    implicit none
    ! prototype
    type(point3D) :: constructor_point3D
    real(kind=long) :: x, y, z
    character(len=3), optional :: tag
    character(len=3) :: tag2
    integer, optional :: nbcs
    integer :: nbcs2
    type(couche_sedimentaire), allocatable, dimension(:), optional :: cs

    if (present(tag)) then
      tag2 = tag
    else
      tag2 = ''
    endif
    if (present(nbcs)) then
      nbcs2 = nbcs
    else
      nbcs2 = 1
    endif
    if (present(cs)) then
      call constructor_point3D%init(x, y, z, tag2, nbcs2, cs)
    else
      call constructor_point3D%init(x, y, z, tag2, nbcs2)
    endif
  end function constructor_point3D


  function constructor_point3D_from_string(ligne)
    implicit none
    ! prototype
    type(point3D) :: constructor_point3D_from_string
    character(*) :: ligne
    ! variables locales
    character(len=3) :: tag
    real(kind=long) :: x, y, z
    integer :: nbcs, i

    !lecture de la ligne en 2 fois pour vérifier la valeur de nbcs
    read(ligne,'(3f13.0,a3)') x, y, z, tag
    read(ligne(42:),*) nbcs
    call constructor_point3D_from_string%init(x, y, z, tag, nbcs)
    read(ligne(46:),'(i2,*(f10.3,f10.6,2f10.3))') nbcs, &
                                   (constructor_point3D_from_string%cs(i)%zc, &
                                    constructor_point3D_from_string%cs(i)%d50, &
                                    constructor_point3D_from_string%cs(i)%sigma, &
                                    constructor_point3D_from_string%cs(i)%tau, &
                                    i=1,nbcs)
  end function constructor_point3D_from_string

  !=============!
  !=== other ===!
  !=============!

  function interpol3D(ptX,ptY,z) result(ptZ)
  !  détermine le point ptZ situé à l'altitude z sur la droite [ptX, ptY]
  !  on est dans le cas où z est compris entre ptX%z et ptY%z
    type(point3D), intent(in) :: ptX, ptY
    real(kind=long), intent(in) :: z
    type(point3D) :: ptZ
    ! -- variables locales --
    real(kind=long) :: tt

    call ptZ%init(x=0.0D0,y=0.0D0,z=0.0D0,tag='   ',nbcs=1)
    if (abs(ptY%z-ptX%z) < precision_alti) then
        if (abs(ptX%z-z) < precision_alti) then
          !on veut un point à la même cote que ptX et ptY
          tt = 0.5_long
        else
          write(error_unit,'(a)') ' >>>> ERREUR : interpolation impossible dans interpol3D()'
          write(error_unit,'(a,3(1x,f0.3))') 'ptX = ',ptX%x,ptX%y,ptX%z
          write(error_unit,'(a,3(1x,f0.3))') 'ptY = ',ptY%x,ptY%y,ptY%z
          write(error_unit,'(a,f0.3)')  '  z = ',z
          call do_crash('interpol3D()')
          !stop 6
          !tt = 0.5_long
        endif
    else
        tt = (z-ptX%z) / (ptY%z-ptX%z)
    endif
    ptZ%x = ptX%x + tt*(ptY%x-ptX%x)
    ptZ%y = ptX%y + tt*(ptY%y-ptX%y)
    ptZ%z = z
  end function interpol3D


  function p_scalaire_H(ptX,ptY,ptA,ptB)
  ! calcul le produit scalaire "horizontal" des vecteurs ptXptY et ptAptB
    type(point3D), intent(in) :: ptX, ptY, ptA, ptB
    real(kind=long) :: p_scalaire_H

    p_scalaire_H = (ptY%x-ptX%x)*(ptB%x-ptA%x)+(ptY%y-ptX%y)*(ptB%y-ptA%y)

  end function p_scalaire_H


  function distance3Dcarre(xyz1, xyz2)
  !==============================================================================
  !           carré de la distance euclidienne entre 2 point3D

  !entrée : xyz1 et xyz2, 2 point3D
  !sortie : carré de la distance euclidienne entre ces deux points
  !
  !==============================================================================
    ! prototype
    type(point3D), intent(in) :: xyz1, xyz2
    real(kind=long) :: distance3Dcarre

    distance3Dcarre = (xyz2%x - xyz1%x)**2 + (xyz2%y - xyz1%y)**2 + (xyz2%z - xyz1%z)**2
  end function distance3Dcarre


  function distance3D(xyz1, xyz2)
  !==============================================================================
  !           distance euclidienne entre 2 point3D

  !entrée : xyz1 et xyz2, 2 point3D
  !sortie : la distance euclidienne entre ces deux points
  !
  !==============================================================================
    ! prototype
    type(point3D), intent(in) :: xyz1, xyz2
    real(kind=long) :: distance3D

    distance3D = sqrt(distance3Dcarre(xyz1, xyz2))
  end function distance3D


  function distanceHcarre(xyz1, xyz2)
  !==============================================================================
  !           carré de la distance euclidienne horizontale entre 2 point3D

  !entrée : xyz1 et xyz2, 2 point3D
  !sortie : carré de la distance euclidienne horizontale entre ces deux points
  !
  !==============================================================================
    ! prototype
    type(point3D), intent(in) :: xyz1, xyz2
    real(kind=long) :: distanceHcarre

    distanceHcarre = (xyz2%x - xyz1%x)**2 + (xyz2%y - xyz1%y)**2
  end function distanceHcarre


  function distanceH(xyz1, xyz2)
  !==============================================================================
  !           distance euclidienne horizontale entre 2 point3D

  !entrée : xyz1 et xyz2, 2 point3D
  !sortie : la distance euclidienne horizontale entre ces deux points
  !
  !==============================================================================
    ! prototype
    type(point3D), intent(in) :: xyz1, xyz2
    real(kind=long) :: distanceH

    distanceH = sqrt(distanceHcarre(xyz1, xyz2))
  end function distanceH


  function distance2Dcarre(yz1, yz2)
  !==============================================================================
  !                   carré de la distance euclidienne entre 2 pointAC

  !entrée : yz1 et yz2, 2 pointAC
  !sortie : carré de la distance euclidienne entre ces deux points
  !
  !==============================================================================
    ! prototype
    type(pointAC), intent(in) :: yz1, yz2
    real(kind=long) :: distance2Dcarre

    distance2Dcarre = (yz2%y - yz1%y)**2 + (yz2%z - yz1%z)**2
  end function distance2Dcarre


  function distance2D(yz1, yz2)
  !==============================================================================
  !                   distance euclidienne entre 2 pointAC

  !entrée : yz1 et yz2, 2 pointAC
  !sortie : la distance euclidienne entre ces deux points
  !
  !==============================================================================
    ! prototype
    type(pointAC), intent(in) :: yz1, yz2
    real(kind=long) :: distance2D

    distance2D = sqrt(distance2Dcarre(yz1, yz2))
  end function distance2D


  subroutine removeDoublons3D (pXYZ)
  !==============================================================================
  !       suppression des doublons, ie les points identiques et successifs
  !         si l'un des deux points est nommé c'est celui-ci qu'on garde

  ! NB: le tableau résultat est alloué par la fonction

  !entrée : un tableau de point3D
  !sortie : copie du tableau donné en entrée, expurgé des points en double
  !         de type point3D.
  !
  !==============================================================================
    ! prototype
    type(point3D), intent(inout), allocatable :: pXYZ(:)
    ! variables locales
    type(point3D), allocatable :: qXYZ(:)
    integer :: np, n, i
    integer, allocatable :: liste(:)

    ! calcul
    np = size(pXYZ)

    allocate(liste(np))

    i = 1  ;  liste(i) = 1  ;  n = 2  !on garde le premier point
    do while (n < np)
      if (distance3D(pXYZ(n),pXYZ(n-1)) < 0.001_long) then
        if (pXYZ(n)%tag == '' .OR. pXYZ(n)%tag == pXYZ(n-1)%tag) then
          continue  !on oublie ce point
        else
          i = i+1  ;  liste(i) = n  !on garde ce point
        endif
      else
        i = i+1  ;  liste(i) = n  ! on garde ce point
      endif
      n = n+1
    enddo
    i = i+1 ; liste(i) = np  !on garde le dernier point

    !création du profil nettoyé
    allocate(qXYZ(i))
    do n = 1, i
       qXYZ(n) = pXYZ(liste(n))
    enddo
    call move_alloc(qXYZ, pXYZ)
    deallocate(liste)
  end subroutine removeDoublons3D

end module objet_point
