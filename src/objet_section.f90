!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - INRAE - 1989-2020                           #
!# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################

module objet_section


  use, intrinsic :: iso_fortran_env, only: error_unit, output_unit
  use Parametres, only: long, nzsup, precision_alti, l9, zero, un
  use Mage_Utilitaires, only: between, do_crash
  use objet_point, only: point3D, pointLC, pointAC, &
  distance3D, interpol3D, distanceH, p_scalaire_H, distance2D, couche_sedimentaire

  implicit none

  ! NOTE: la définition de verbose est nécessaire pour éviter une dépendance circulaire avec la module Data_Num_Fixes
  logical :: verbose
  logical :: is_LC   ! indicateur pour savoir s'il faut utiliser la variante largeur-cote des routines largeur(), perimetre() et surface()
  integer :: LC_type ! type de construction de la représentation largeurs-cotes des profils.
                    ! 0 -> méthode d'origine mise en œuvre par largeursCotes()
                    ! 1 -> calcul en utilisant largeur_XYZ(), perimetre_XYZ() et section_XYZ() sur le profil 3D
                    ! 2 -> calcul en utilisant largeur_XYZ(), perimetre_XYZ() et section_XYZ() sur la projection
                    !      abscisses-cotes du profil original (dans un monde parfait 2 == 0)

  type couche_sed_section
    real(kind=long) :: hc    !épaisseur de la couche
    real(kind=long) :: d50   !valeur locale du d50
    real(kind=long) :: sigma !valeur locale de l'étendue granulométrique
    real(kind=long) :: tau   !valeur locale de la contrainte de mise en mouvement
  end type couche_sed_section

  interface assignment (=)
    module procedure clone_CS_section
  end interface

  !===================!
  !=== profil_brut ===!
  !===================!

  type profil_brut
    character(len=20) :: name
    real(kind=long) :: pk        !abscisse en long (position)
    real(kind=long) :: zf        !cote du fond
    integer :: np                !nombre de points du profil != taille effective du tableau xyz
    integer :: nzone        !nombre de sous-sections dans le profil, a priori 3 sous-sections
    integer :: li(0:nzsup)  !limites (indices) droites des sous-sections dans un profil XYZ (li(nz) < li(nz+1), nz ≤ nzone-1
                            !a priori les zones sont définies de la rive gauche à la rive droite
                            !li(nzone)=np et li(0)=1
    integer :: main !numéro de la zone qui correspond au lit mineur. Le plus souvent ce sera 2 (pour 3 zones)
                    !c'est la zone comprise entre li(main-1) et li(main)
    integer :: xyz_size          !taille effective de xyz en memoire
    type(point3D), dimension(:), allocatable :: xyz !liste ordonnée des points du profil
    contains
      procedure, private :: egal => clone_profil
      procedure :: init => init_profil
      generic   :: assignment (=) => egal
      procedure :: fusion_profils => fusion_profils
      generic   :: operator (+) => fusion_profils
      procedure :: insert => insert_point3D
      procedure :: add => add_point
!       procedure :: get => get_point_from_pos, get_point_from_tag
      procedure :: get => get_point_from_tag
      procedure :: remove_doublons => remove_doublons
      procedure :: remove_point => remove_point
      procedure :: update_zf => update_zf
      procedure :: ajuste_z => ajuste_z
      procedure :: affiche => affiche
      procedure :: remove_profil => removeProfil
      procedure :: split => split_profil
      procedure :: length3D => get_3D_length
      procedure :: length2D => get_2D_length
      procedure :: extract => extract_profil
      procedure :: patch => patch_profil
      procedure :: interpolate_points_npoints
      procedure :: projectionPlan => projectionPlan
      procedure :: purge_points_alignes
  end type profil_brut

  !==============!
  !=== profil ===!
  !==============!

  !type profil TS complet
  type, extends(profil_brut) :: profil

    !données tabulées largeurs-cotes pour la couche supérieure
    logical :: wh_aJour  !faux si xy a été modifié sans refaire la tabulation wh
                          !NB : à gérer par le programmeur
    integer :: nh        !nombre de niveaux du profil
                          !taille effective du tableau wh
    integer :: ip        !niveau d'interpolation courant (dernier utilisé)
                          !la dernière hauteur d'eau utilisée est entre wh(ip)%h et wh(ip+1)%h
    type(pointLC), allocatable, dimension(:) :: wh  ! ordonnés par ordre wh(n)%h croissant

  !   real (kind=long) :: l_g, l_m, l_d  ! largeurs utiles des lits moyens gauche et droits et mineur
  !                                      ! pour le stockage des sédiments
    real(kind=long) :: Ks(nzsup)  !Ks(nz) = coefficient de Strickler pour la sous-section nz
                                  !contrainte : un seul coefficient de frottement par sous-section ;
                                  !on ne peut pas redécouper une sous-section en plusieurs zones de frottement

    real(kind=long) :: drag_force(nzsup) !produit N*D*CD définissant la force de trainée dans chaque sous-section :
                                          !N  = densité d'arbres au m2
                                          !D  = diamètre moyen des arbres
                                          !CD = coefficient de trainée
    real(kind=long) :: psi_t !coefficient de réglage pour le cisaillement à l'interface MC <-> FP

    integer :: iss ! indicateur de singularité de la section
                    ! on appelle section singulière la section aval d'un couple singulier
                    ! c'est à dire de 2 sections entre lesquelles on a placé une loi d'ouvrage
                    ! ISS = 0      ==> la section est régulière (pas d'ouvrage)
                    ! ISS = NS > 0 ==> la section est singulière et NS est le numéro de l'ouvrage
                    !                  composé tel que all_OuvCmp(NS)%js = numéro de la section


  !   données ajoutées pour MAGE
    real(kind=long) :: ymoy !tirant d'eau de débordement en lit moyen
    real(kind=long) :: almy !largeur au miroir à la cote de débordement mineur-moyen
    real(kind=long) :: pmy  !périmètre mouillé à la cote de débordement mineur-moyen
    real(kind=long) :: smy  !section mouillée à la cote de débordement mineur-moyen
    !les variables suivantes sont utiles si on veut éliminer des points bas aux extrémités des profils
    real(kind=long) :: ybgau !tirant d'eau de la berge gauche ; a priori le 1er point du profil
    real(kind=long) :: ybdro !tirant d'eau de la berge droite ; a priori le dernier point du profil
    real(kind=long) :: ybmin !tirant d'eau de berge minimum
    real(kind=long) :: ybmax !tirant d'eau de berge maximum

  ! sections mouillées des lits majeurs à la cote de débordement
    real(kind=long) :: smaj_left, smaj_right

  ! Limites eau
  !   irg et ird sont les premiers indices en partant des rives gauche et
  !   droite qui correspondent à des points sous la surface de l'eau
  !   ptX et ptY sont les points interpolés où le plan d'eau intersecte le profil
  !   known_level est le niveau d'eau pour lequel on a obtenu irg, ird, ptX et ptY
    real(kind=long) :: known_level
    integer :: irg, ird
    type(point3D) :: ptX, ptY

  ! Limites de la zone utile pour le charriage
  ! Elles sont définies par l'utilisateur à l'aide des lignes directrices FG et FD
    integer :: kfg, kfd
    real(kind=long) :: largeur_active

  ! compartiment sédimentaire
    integer :: nbcs ! nombre de couches sédimentaires
    type(couche_sed_section), allocatable, dimension(:) :: cs
    contains
      procedure :: get_data_geom => get_data_geom
      procedure :: section_mouillee => section_mouillee
      procedure :: section_xyz => section_xyz
      procedure :: section_lc => section_lc
      procedure :: largeur => largeur
      procedure :: largeur_xyz => largeur_xyz
      procedure :: largeur_lc => largeur_lc
      procedure :: perimetre => perimetre
      procedure :: perimetre_xyz => perimetre_xyz
      procedure :: perimetre_lc => perimetre_lc
      procedure :: surface_mouillee_tirant => surface_mouillee_tirant ! surfy
      procedure :: surface_mouillee_cote => section_mouillee ! surf
      procedure :: limite_eau => limite_eau
      procedure :: largeursCotes => largeursCotes
      procedure :: largeursCotes_alt => largeursCotes_alt
      procedure :: strickler_lit_moyen => strickler_lit_moyen
      procedure :: triZ => triZ
      procedure :: init_autres_data_geom => init_autres_data_geom
  end type profil

  !==================!
  !=== profil_ptr ===!
  !==================!

  type profil_ptr
    type(profil),pointer :: ptr
  end type profil_ptr

  !================!
  !=== profilAC ===!
  !================!

  !type pour la projection abscisses-cotes d'un profil XYZ
  type profilAC
    real(kind=long) :: pk  !pk du profil (abscisse en long)
    integer :: np          !nb de points réel du profil en travers
                            !nb+2 : taille effective du tableau yz
    type(pointAC), allocatable :: yz(:)  !à dimensionner de 0 à np+1 pour permettre
                                          !d'ajouter un point à gauche et à droite décalés de 100 m en altitude
  end type profilAC

  !======!
  contains
  !======!


  subroutine clone_CS_section(c2,c1)
    !opération de clonage d'un compartiment sédimentaire -> affectation
    implicit none
    ! -- prototype --
    type(couche_sed_section), intent(in)  :: c1
    type(couche_sed_section), intent(out) :: c2
    ! -- variables locales --

    c2%hc    = c1%hc
    c2%d50   = c1%d50
    c2%sigma = c1%sigma
    c2%tau = c1%tau

  end subroutine clone_CS_section

  !=======================!
  !=== profil generique===!
  !=======================!

  subroutine init_profil(self, pk, name, xyz)
    implicit none
    ! prototype
    class(profil_brut) :: self
    real(kind=long) :: pk
    character(len=20) :: name
    type(point3D), dimension(:), intent(in):: xyz
    integer :: np, n, nl, irg, ird, ibg, ibd
    type(point3D) :: p3d

    np = size(xyz)
    if (np > 0) then
      self%pk = pk
  !     self%zf = zf
      self%np = np
      self%xyz_size = np
      self%name = name
      allocate(self%xyz, source=xyz)
      call self%update_zf()
      !initialisation profil
      select type(self)
        class is (profil)
          self%known_level = -9999._long
          self%irg = -1
          self%ird = -1
          call self%ptX%init(x=0.0D0,y=0.0D0,z=0.0D0,tag='   ',nbcs=1)
          call self%ptY%init(x=0.0D0,y=0.0D0,z=0.0D0,tag='   ',nbcs=1)
          call ajuste_Z(self)
          self%nbcs = 0

          if (LC_type > 0) then
              call self%largeursCotes_alt()
          else
              call self%largeursCotes()
          endif

          !vérification que la tabulation largeurs-cotes a bien été faite
          if (.not.self%wh_aJour) then
          write(error_unit,'(a)') ' >>>> BUG dans init_profil() : la tabulation largeurs-cotes n''a pas été faite'
          call do_crash('init_profil()')
          endif

      end select

      !recherche des points RG et RD (par défaut on suppose le profil donné de RG vers RD)
      irg = 1  ;  ird = np
      ibg = 1  ;  ibd = np
      do n = 1, np
        select case (trim(self%xyz(n)%tag))
          case ('RG','rg','Rg','rG')  ! limite gauche du lit mineur et droite du lit majeur gauche
            irg = n
          case ('RD','rd','Rd','rD')  ! limite droite du lit mineur et gauche du lit majeur droit
            ird = n
          case ('BG','bg','Bg','bG')  ! berge gauche == limite gauche du lit mineur
            ibg = n
          case ('BD','bd','Bd','bD')  ! berge droite == limite droite du lit mineur
            ibd = n
          case default
        end select
      enddo
      self%nzone = 3
      self%li(0) = 1
      self%li(1) = max(irg,ibg)
      self%li(2) = min(ird,ibd)
      self%li(3) = np
      self%main = 2

      if (irg > ird) then
        ! le profil est donné de RD vers RG, il faut le retourner
        !if(verbose) write(output_unit,*) '>>>> le profil du pk ', pk, ' est donné de RD vers RG, on le retourne'
        write(l9,*) '>>>> le profil du pk ', pk, ' est donné de RD vers RG, on le retourne'
!         if(verbose) call war010(pk)
        do n = 1, np/2
          p3d = self%xyz(n)
          nl = np-n+1
          self%xyz(n) = self%xyz(nl)
          self%xyz(nl) = p3d
        enddo
        irg = np-irg+1  ;  ird = np-ird+1
        if (ibg > ibd) then
          ibg = np-ibg+1  ;  ibd = np-ibd+1
        endif
          self%li(1) = max(irg,ibg)
          self%li(2) = min(ird,ibd)
      endif
   else
     write(l9,'(a)')  ' >>>> ERREUR dans init_profil() : Le profil d''entrée est vide'
     stop ' >>>> ERREUR dans init_profil()'
   endif ! (np > 0)

  end subroutine init_profil

  ! renvoie une copie de prfl
  subroutine clone_profil(self,prfl)
    implicit none
    ! prototype
    class(profil_brut), intent(out) :: self
    class(profil_brut), intent(in) :: prfl

    self%pk = prfl%pk
    self%zf = prfl%zf
    self%np = prfl%np
    self%xyz_size = prfl%xyz_size
    self%name = prfl%name
    if (allocated(prfl%xyz)) then
      allocate(self%xyz, source=prfl%xyz)
    else
      allocate(self%xyz(self%np))
    endif
    select type(self)
      type is (profil)
        select type(prfl)
          type is (profil)
            self%wh_aJour       = prfl%wh_aJour
            self%nh             = prfl%nh
            self%ip             = prfl%ip
            self%nzone          = prfl%nzone
            self%li             = prfl%li
            self%main           = prfl%main
            self%Ks             = prfl%Ks
            self%drag_force     = prfl%drag_force
            self%psi_t          = prfl%psi_t
            self%iss            = prfl%iss
            self%ymoy           = prfl%ymoy
            self%almy           = prfl%almy
            self%pmy            = prfl%pmy
            self%smy            = prfl%smy
            self%ybgau          = prfl%ybgau
            self%ybdro          = prfl%ybdro
            self%ybmin          = prfl%ybmin
            self%ybmax          = prfl%ybmax
            self%smaj_left      = prfl%smaj_left
            self%smaj_right     = prfl%smaj_right
            self%known_level    = prfl%known_level
            self%irg            = prfl%irg
            self%ird            = prfl%ird
            self%ptX            = prfl%ptX
            self%ptY            = prfl%ptY
            self%kfg            = prfl%kfg
            self%kfd            = prfl%kfd
            self%largeur_active = prfl%largeur_active
            self%nbcs           = prfl%nbcs
            if (allocated(prfl%wh)) then
              allocate(self%wh, source=prfl%wh)
            else
              allocate(self%wh(self%nh))
            endif
            if (allocated(prfl%cs)) then
              allocate(self%cs, source=prfl%cs)
            else
              allocate(self%cs(self%nbcs))
            endif
          type is (profil_brut)
            self%wh_aJour       = .false.
            self%nh             = 0
            self%ip             = 0
            self%nzone          = 0
            self%li             = 0
            self%main           = 0
            self%Ks             = 0
            self%drag_force     = 0
            self%psi_t          = 0
            self%iss            = 0
            self%ymoy           = 0
            self%almy           = 0
            self%pmy            = 0
            self%smy            = 0
            self%ybgau          = 0
            self%ybdro          = 0
            self%ybmin          = 0
            self%ybmax          = 0
            self%smaj_left      = 0
            self%smaj_right     = 0
            self%known_level    = 0
            self%irg            = 0
            self%ird            = 0
            call self%ptX%init(x=0.0D0,y=0.0D0,z=0.0D0,tag='   ',nbcs=1)
            call self%ptY%init(x=0.0D0,y=0.0D0,z=0.0D0,tag='   ',nbcs=1)
            self%kfg            = 0
            self%kfd            = 0
            self%largeur_active = 0
            self%nbcs           = 0
            allocate(self%wh(0))
            allocate(self%cs(0))
        end select
    end select
  end subroutine clone_profil

  subroutine insert_point3D(self, xyz, position)
    implicit none
    ! prototype
    class(profil_brut) :: self
    type(point3D), intent(in) :: xyz
    integer, intent(in), optional :: position
    ! variables locales
    type(point3D), dimension(:), allocatable :: xyz_temp
    integer :: old_size, old_np, pos, new_size, i

    select type(self)
      type is (profil_brut)
        continue
      class default
        print*,"Warning: insert point only available for profil_brut"
    end select

    old_size = self%xyz_size
    new_size = self%xyz_size
    old_np = self%np
    if(present(position)) then
      if (position > old_np) then ! on ne va pas au delà du dernier point+1
        pos = old_np + 1
      elseif (position < 1) then
        pos = 1
      else
        pos = position
      endif
    else
      pos = old_np+1
    endif


    if (old_size < old_np + 1) then
      if (old_size <= 0) then
        new_size=1
      else
        new_size = old_size*2
      endif
      ! on double la taille a allouer :
      self%xyz_size = new_size
    end if

    ! on alloue le nouvel espace memoire
!     print*,"allocated(xyz_temp)=", allocated(xyz3D_temp)
!     allocate(point3D::xyz3D_temp(old_size))
!     !on copie les anciennes valeurs de xyz :
!     print*, " titi"
!     if (pos > 1) xyz3D_temp(:pos-1) = self%xyz(:pos-1)
!     print*, " tata"
!     if (pos <= old_np) xyz3D_temp(pos+1:old_np+1) = self%xyz(pos:old_np)
!     ! on place xyz a sa place dans le tableau
!     xyz3D_temp(pos) = xyz
!     print*, " tutu"
!     !  self%xyz pointe sur l'espace memoire de xyz3D_temp, xyz3D_temp est désalloue
!     call move_alloc(xyz3D_temp, self%xyz)
!     print*, " tete"
      if (pos == 1) then
        self%xyz=[xyz,self%xyz(:)]
      elseif (pos==old_np+1) then
        self%xyz=[self%xyz(:pos-1),xyz]
      else
        self%xyz=[self%xyz(:pos-1),xyz,self%xyz(pos:old_np)]
      endif
!     select type(xyz_ptr => self%xyz)
!       type is (point3D)
!         ! on alloue le nouvel espace memoire
!         allocate(point3D::xyz_temp(new_size))
!         select type(xyz_tmp_ptr => self%xyz)
!           type is (point3D)
! !             !on copie les anciennes valeurs de xyz :
! !             if (pos > 1) xyz_tmp_ptr(:pos-1) = xyz_ptr(:pos-1)
! !             if (pos <= old_np) xyz_tmp_ptr(pos+1:old_np+1) = xyz_ptr(pos:old_np)
! !             ! on place xyz a sa place dans le tableau
! !             xyz_tmp_ptr(pos) = xyz
! !             ! xyz_ptr pointe sur l'espace memoire de xyz_temp, xyz_temp est désalloue
! !             call move_alloc(xyz_temp, self%xyz)
!         end select
!       type is (pointTS)
!         ! on alloue le nouvel espace memoire
!         allocate(pointTS::xyz_temp(new_size))
!         select type(xyz_tmp_ptr => self%xyz)
!           type is (pointTS)
!             !on copie les anciennes valeurs de xyz :
!             if (pos > 1) xyz_tmp_ptr(:pos-1) = xyz_ptr(:pos-1)
!             if (pos <= old_np) xyz_tmp_ptr(pos+1:old_np+1) = xyz_ptr(pos:old_np)
!             ! on place xyz a sa place dans le tableau
!             xyz_tmp_ptr(pos) = xyz
!             ! xyz_ptr pointe sur l'espace memoire de xyz_temp, xyz_temp est désalloue
!             call move_alloc(xyz_temp, self%xyz)
!         end select
!     end select
!     if(allocated(xyz3D_temp))then
!         ! xyz_ptr pointe sur l'espace memoire de xyz3D_temp, xyz3D_temp est désalloue
!         call move_alloc(xyz3D_temp, self%xyz)
!     elseif(allocated(xyzTS_temp))then
!         ! xyz_ptr pointe sur l'espace memoire de xyzTS_temp, xyzTS_temp est désalloue
!         call move_alloc(xyzTS_temp, self%xyz)
!     else
!       print*,"PROBLEM!!"
!     endif

    self%np = old_np + 1
    self%xyz_size = size(self%xyz)

    call self%update_zf()
  end subroutine insert_point3D

  subroutine add_point(self, xyz, position)
    implicit none
    ! prototype
    class(profil_brut) :: self
    type(point3D), intent(in) :: xyz
    integer, intent(in) :: position

    select type(self)
      type is (profil_brut)
        continue
      class default
        print*,"Warning: add point only available for profil_brut"
!         return
    end select

    if (position < 1 .or. position > self%np) then
      print*,"Error: position ",position," does not exist in profil ",self%name
      stop
    endif

    self%xyz(position) = xyz

    call self%update_zf()
  end subroutine add_point

  function get_point_from_tag(self, tag)
    implicit none
    ! prototype
    type(point3D),pointer :: get_point_from_tag
    class(profil_brut),intent(in),target :: self
    character(len=3), intent(in) :: tag
    ! variables locales
    integer :: i

    get_point_from_tag => null()
!     if (trim(tag) == '') stop 'must use a valid tag'
    if (trim(tag) == '' .or. trim(tag) == 'un')then
      get_point_from_tag => self%xyz(1)
      return
    endif
    if (trim(tag) == 'np')then
      get_point_from_tag => self%xyz(self%np)
      return
    endif

    do i=1,self%np
      if (trim(self%xyz(i)%tag) == trim(tag)) then
        get_point_from_tag => self%xyz(i)
        return
      endif
    enddo

    print*,"Error: tag ",tag," does not exist in profil ",self%name
    stop
  end function get_point_from_tag

  subroutine remove_point(self, position)
    implicit none
    ! prototype
    class(profil_brut) :: self
    integer, intent(in) :: position
    ! variables locales
    type(point3D), dimension(:),allocatable :: xyz_temp
    integer :: old_size, old_np, pos, new_size

    select type(self)
      type is (profil_brut)
        continue
      class default
        print*,"remove point only available for profil_brut"
        return
    end select

    old_size = self%xyz_size
    new_size = self%xyz_size
    old_np = self%np

    if (old_np < 1) return
    if (position > old_np) then ! on ne va pas au delà du dernier point
      pos = old_np
    elseif (position < 1) then
      pos = 1
    else
      pos = position
    endif

    if (old_size >= (old_np - 1)*2) then ! trop de place allouee
      ! on desalloue la memoire excedentaire
      new_size = old_np - 1
    end if

    ! on alloue le nouvel espace memoire
    allocate(xyz_temp(new_size))
    !on copie les anciennes valeurs de xyz :
    if (pos > 1) xyz_temp(:pos-1) = self%xyz(:pos-1)
    if (pos < old_np) xyz_temp(pos:old_np-1) = self%xyz(pos+1:old_np)
    ! self%xyz pointe sur l'espace memoire de xyz_temp, xyz_temp est désalloue
    call move_alloc(xyz_temp, self%xyz)

    self%xyz_size = new_size
    self%np = old_np - 1
    call self%update_zf()
  end subroutine remove_point

  subroutine update_zf(self)
    implicit none
    ! prototype
    class(profil_brut) :: self
    integer :: i

    self%zf = 9999999.99
    do i = 1, self%np
      self%zf = min(self%zf, self%xyz(i)%z)
    enddo
  end subroutine update_zf

  subroutine remove_doublons(self)
  !==============================================================================
  !       suppression des doublons, ie les points identiques et successifs
  !         si l'un des deux points est nommé c'est celui-ci qu'on garde

  ! NB: le tableau résultat est alloué par la fonction

  !entrée : un tableau de point3D
  !sortie : copie du tableau donné en entrée, expurgé des points en double
  !         de type point3D.
  !
  !==============================================================================
    ! prototype
    class(profil_brut), intent(inout) :: self
    ! variables locales
    integer :: n

    n = self%np-1 ! on garde le dernier point
    do while (n > 1) ! on garde le premier point
      if (distance3D(self%xyz(n),self%xyz(n+1)) < 0.001_long) then
        if (self%xyz(n)%tag == '' .OR. self%xyz(n)%tag == self%xyz(n+1)%tag) then
          !on oublie ce point
          call self%remove_point(n)
        endif
      endif
      n = n-1
    enddo
  end subroutine remove_doublons

  ! ajustement des cotes de profils trop voisines
  subroutine ajuste_Z(self)
    implicit none
    ! -- prototype --
    class(profil_brut), intent(inout) :: self
    ! -- variables locales --
    integer :: i, j
    real(kind=long) :: z0

    do i = 1, self%np - 1
        z0 = self%xyz(i)%z
        do j = i+1, self%np
          if (abs(self%xyz(j)%z - z0) < precision_alti) then
              self%xyz(j)%z = z0
          elseif (abs(self%xyz(j)%z - z0) < 2._long * precision_alti) then
              if (self%xyz(j)%z < z0) then
                self%xyz(j)%z = z0 - precision_alti - precision_alti
              else
                self%xyz(j)%z = z0 + precision_alti + precision_alti
              endif
          endif
        enddo
    enddo

  end subroutine ajuste_Z

  subroutine affiche(self)
  ! affiche la géométrie du profil passé en argument
    ! -- prototype --
    class(profil_brut), intent(in) :: self
    ! -- variables locales --
    integer :: i, j

    write(l9,*)
    write(l9,'(3a,f10.3)') 'Profil ',self%name,' Pk = ',self%pk
    do i = 1, self%np
        if (self%xyz(i)%nbcs == 0)then
              write(l9,*) 'Point3D : ',self%xyz(i)%x,self%xyz(i)%y,self%xyz(i)%z,self%xyz(i)%tag
        else
              write(l9,*) 'PointTS : ',self%xyz(i)%x,self%xyz(i)%y,self%xyz(i)%z,self%xyz(i)%tag
              write(l9,*) '          ',self%xyz(i)%nbcs
              do j = 1, self%xyz(i)%nbcs
                write(l9,*) '          ',self%xyz(i)%cs(j)%zc,self%xyz(i)%cs(j)%d50,self%xyz(i)%cs(j)%sigma,self%xyz(i)%cs(j)%tau
              enddo
        endif
    enddo
  end subroutine affiche

  subroutine removeProfil(self)
  !==============================================================================
  !           désalloue les tableaux avec allocation dynamique d'un profil

  !entrée : self, un profil (type profil)
  !sortie : self
  !
  !==============================================================================
    !prototype
    class(profil_brut), intent(inout) :: self

    select type(self)
      type is(profil_brut)
        if(allocated(self%xyz)) deallocate(self%xyz)
      type is(profil)
        if(allocated(self%xyz)) deallocate(self%xyz)
        if(allocated(self%wh)) deallocate(self%wh)
    end select
  end subroutine removeProfil


  subroutine interpolate_linear(sections_in, nbsti, sections_out)
    implicit none
    ! prototype
    class(profil_brut), dimension(:), intent(in) :: sections_in
    integer, dimension(:), intent(in) :: nbsti
    class(profil_brut), dimension(:), intent(inout) :: sections_out
    ! variables locales
    integer :: nsect_out, nsect_in, isect_out, isect_in, isect_loc, ipt
    real(kind=long) :: d, x1,y1,z1, x2,y2,z2, dpk

    nsect_in = size(nbsti)+1
    nsect_out = size(sections_out)
    isect_out = 1
    do isect_in =1,nsect_in-1
      sections_out(isect_out) = sections_in(isect_in)
      ! TODO implémenter - et * pour les points
      isect_out = isect_out+1
      dpk = sections_in(isect_in+1)%pk - sections_in(isect_in)%pk
      do isect_loc=1,nbsti(isect_in)
        sections_out(isect_out) = sections_in(isect_in)
        ! RATIO entre les deux sections initiales
        d=float(isect_loc)/float(nbsti(isect_in)+1)
        do ipt=1,sections_in(isect_in)%np
          x1 = sections_in(isect_in)%xyz(ipt)%x
          y1 = sections_in(isect_in)%xyz(ipt)%y
          z1 = sections_in(isect_in)%xyz(ipt)%z
          x2 = sections_in(isect_in+1)%xyz(ipt)%x
          y2 = sections_in(isect_in+1)%xyz(ipt)%y
          z2 = sections_in(isect_in+1)%xyz(ipt)%z
          sections_out(isect_out)%xyz(ipt)%x = x1+d*(x2-x1)
          sections_out(isect_out)%xyz(ipt)%y = y1+d*(y2-y1)
          sections_out(isect_out)%xyz(ipt)%z = z1+d*(z2-z1)
          sections_out(isect_out)%xyz(ipt)%tag = sections_in(isect_in)%xyz(ipt)%tag
        enddo
        sections_out(isect_out)%pk = sections_in(isect_in)%pk + d*dpk
        write(sections_out(isect_out)%name,'(a,i0)') 'interpol',int(sections_out(isect_out)%pk)
        isect_out = isect_out+1
      enddo
    enddo
    sections_out(nsect_out) = sections_in(nsect_in)
  end subroutine interpolate_linear

  !==============!
  !=== profil ===!
  !==============!

  subroutine get_data_geom(self,z,lg,pr,sf)
  !==============================================================================
  ! interpolation de la largeur au miroir, du périmètre et de la surface
  !           mouillés dans un profil pour une cote donnée
  !
  ! NB : cette fonction modifie le profil en fixant son attribut ip

  !entrée : un_profil, de type profil
  !         z, la cote à laquelle il faut calculer la largeur
  !sortie : la valeur de la largeur, du périmètre et de la surface
  !         à la cote z
  !
  !==============================================================================
    ! prototype
    class(profil),intent(inout), target :: self  !cette fonction modifie le profil
                                                 !en fixant son attribut ip
    real(kind=long),intent(in) :: z
    real(kind=long),intent(out) :: lg, pr, sf
    ! variables locales
    !integer, pointer :: ip0
    integer :: i, ip0
    real(kind=long), pointer :: lg1, lg2, pr1, pr2
    real(kind=long) :: dh, hh, dz
    !
    !calcul
    hh = z-self%zf

    if (.NOT.self%wh_aJour) stop 905
    ip0 = self%ip
    if (hh >= self%wh(ip0)%h .AND. hh < self%wh(ip0+1)%h) then  !niveau courant
        lg1 => self%wh(ip0)%l ; lg2 => self%wh(ip0+1)%l
        pr1 => self%wh(ip0)%p ; pr2 => self%wh(ip0+1)%p
        dh = self%wh(ip0+1)%h - self%wh(ip0)%h
        dz = hh - self%wh(ip0)%h
        lg = lg1 + dz*(lg2-lg1)/dh  ! interpolation linéaire
        pr = pr1 + dz*(pr2-pr1)/dh  ! interpolation linéaire
        sf = self%wh(ip0)%s + dz*0.5_long*(lg1+lg) !méthode des trapèzes
        return
    elseif (hh < self%wh(ip0)%h) then                            ! recherche vers le bas
        do i = ip0, 1, -1
          if (hh >= self%wh(i)%h) then
              lg1 => self%wh(i)%l ; lg2 => self%wh(i+1)%l
              pr1 => self%wh(i)%p ; pr2 => self%wh(i+1)%p
              dh = self%wh(i+1)%h - self%wh(i)%h
              dz = hh - self%wh(i)%h
              lg = lg1 + dz*(lg2-lg1)/dh  ! interpolation linéaire
              pr = pr1 + dz*(pr2-pr1)/dh  ! interpolation linéaire
              sf = self%wh(i)%s + dz*0.5_long*(lg1+lg) !méthode des trapèzes
              self%ip = i
              return
          endif
        enddo
        write(error_unit,*) 'PB au profil : ',self%name, self%pk, self%zf, hh
        stop 901 ! on arrive au fond du profil
    elseif (hh >= self%wh(ip0+1)%h) then                         !recherche vers le haut
        do i = ip0, self%nh-1
          if (hh < self%wh(i+1)%h) then
              lg1 => self%wh(i)%l ; lg2 => self%wh(i+1)%l
              pr1 => self%wh(i)%p ; pr2 => self%wh(i+1)%p
              dh = self%wh(i+1)%h - self%wh(i)%h
              dz = hh - self%wh(i)%h
              lg = lg1 + dz*(lg2-lg1)/dh  ! interpolation linéaire
              pr = pr1 + dz*(pr2-pr1)/dh  ! interpolation linéaire
              sf = self%wh(i)%s + dz*0.5_long*(lg1+lg) !méthode des trapèzes
              self%ip = i
              return
          endif
        enddo
        write(error_unit,*) 'Erreur au profil ',self%name,z, self%nh, self%np
        do i = 1, self%nh
          write(error_unit,*) i,self%wh(i)%h,self%wh(i)%l,&
                        self%wh(i)%p,self%wh(i)%s
        enddo
        write(error_unit,*) ' >>>> Appel par get_data_geom()'
        stop 902 ! on arrive au sommet du profil
    else
        lg = -1._long  ;  pr = -1._long  ;  sf = -1._long
        write(error_unit,*) 'Erreur pas normale dans get_data_geom()'
        stop 6
    endif
  end subroutine get_data_geom

  function section_mouillee(self,z,k)
  !==============================================================================
  ! interpolation de la section mouillée dans un profil pour une cote donnée
  !
  !entrée : un_profil, de type profil
  !         z, la cote à laquelle il faut calculer la section
  !         k, le numéro de la sous-section, 0 pour la section totale
  !            calcul sur la représentation largeurs-cotes si absent
  !sortie : la valeur de la section à la cote z
  !
  !==============================================================================
    ! prototype
    class(profil), intent(inout) :: self
    real(kind=long),intent(in) :: z
    integer, intent(in), optional :: k
    real(kind=long) :: section_mouillee

    if (.not.present(k) .or. k==0) then
      if (is_LC) then
        section_mouillee = self%section_LC(z)
      else
        section_mouillee = self%section_XYZ(z,0)
      endif
    else
      section_mouillee = self%section_XYZ(z,k)
    endif
    if (section_mouillee <= zero) then
      write(l9,*) '>>>> ERREUR : section_mouillee mouillée négative au pk ',self%pk,' : ',section_mouillee,z,k,self%zf, &
            minval(self%xyz(:)%z)
    endif
  end function section_mouillee

  function surface_mouillee_tirant(self,y)
  !==============================================================================
  !     calcul de la surface mouillée en fonction du tirant d'eau y
  !==============================================================================
    implicit none
    ! -- prototype --
    real(kind=long) :: surface_mouillee_tirant
    real(kind=long),intent(in) :: y
    class(profil), intent(inout) :: self
    ! -- variables --
!     real(kind=long) :: ymax
    !---------------------------------------------------------------------------
    surface_mouillee_tirant = huge(zero)
    !y = tirant d'eau dans la section courante
    if (y > zero) then
!       ymax = self%ybmax+100._long  !ybmax(is)=  tirant d'eau maximal de la section is
!       if (y > ymax) then  !dépassement du y maximal ---> interpolation impossible
!         call war001(is,y,ymax,+1)
!       endif
      surface_mouillee_tirant = self%section_mouillee(y+self%zf)
    else
!       call err025(is,y,3)
    endif
    if (surface_mouillee_tirant > huge(zero)/2) stop '>>>> BUG dans surface_mouillee_tirant()'
  end function surface_mouillee_tirant


  function section_LC(self,z)
  !==============================================================================
  ! interpolation de la section mouillée dans un profil pour une cote donnée
  !
  ! NB : cette fonction utilise la représentation largeurs-cotes du profil et
  !      modifie le profil en fixant son attribut ip
  !
  !entrée : self, de classe profil
  !         z, la cote à laquelle il faut calculer la section
  !sortie : la valeur de la section à la cote z
  !
  !==============================================================================
    ! prototype
    class(profil),intent(inout), target :: self
    real(kind=long),intent(in) :: z
    real(kind=long) :: section_LC
    ! variables locales
    integer :: ip0
    real(kind=long) :: lg2
    type(pointLC), pointer :: wh

    !calcul
    if (.NOT.self%wh_aJour) then
        write(error_unit,*) 'Erreur dans Section : ', self%name, self%pk
        stop 907
    else
        lg2 = largeur_LC(self,z)  !met à jour ip0 => à faire en premier
        if (lg2 < zero) then
          section_LC = -1._long
        else
          ip0 = self%ip
          wh => self%wh(ip0)
          section_LC = wh%s + 0.5_long*(wh%l+lg2)*(z-self%zf-wh%h)
        endif
    endif
  end function section_LC


  function section_XYZ(self,z,k)
  !==============================================================================
  ! interpolation de la section mouillée dans un profil pour une cote donnée
  ! directement à partir du profil 3D
  !
  !entrée : self, de classe profil
  !         z, la cote à laquelle il faut calculer la section
  !         k, le numéro de la sous-section, 0 pour la section totale
  !sortie : la valeur de la section à la cote z
  !
  !==============================================================================
    ! prototype
    class(profil),intent(inout), target :: self
    real(kind=long),intent(in) :: z
    integer, intent(in) :: k
    real(kind=long) :: section_XYZ
    ! variables locales
    integer :: irg, ird, i
    real(kind=long) :: h1,h2
    type(point3D) :: ptX, ptY, ptW
    type(point3D), dimension(:), pointer :: p3D
    integer, dimension(:), pointer :: lit

    p3D(1:) => self%xyz(:)
    lit(0:) => self%li(:)

    if (z < self%zf+precision_alti) then
        section_XYZ = zero
        return
    endif


    !calcul
    call self%limite_Eau(z)
    irg = self%irg  ;  ird = self%ird
    ptX = self%ptX  ;  ptY = self%ptY
    if (irg == -1 .and. ird == -1) then
        !il y a eu un problème dans limite_Eau()
        section_XYZ = -1._long  ;  return
    endif
    if (k >= 1 .and. k <= self%nzone) then
        if (irg > lit(k) .OR. ird < lit(k-1))  then
          section_XYZ = zero  ;  return
        else
          !on ramène irg, ird, ptX et ptY aux bordures de la sous-section k s'ils sont à l'extérieur
          if (irg <= lit(k-1)) then
              ptX = p3D(lit(k-1))
              irg = lit(k-1)
          endif
          if (ird >= lit(k)) then
              ptY = p3D(lit(k))
              ird = lit(k)
          endif
        endif
    elseif (k > 0) then
        write(error_unit,'(a,i0)') '>>>> ERREUR dans section_XYZ : numéro de sous-section erroné : ',k
        stop 6
    endif
    !ici on a irg, ird, ptX et ptY pour n'importe quelle valeur de k
    h1 = z - max(p3D(irg)%z,ptX%z)
    h2 = abs(p3D(irg)%z-ptX%z)
    section_XYZ = sign(distanceH(ptX,p3D(irg)),p_scalaire_H(ptX,ptY,ptX,p3D(irg))) * (h1+0.5_long*h2)
    h1 = z - max(p3D(ird)%z,ptY%z)
    h2 = abs(p3D(ird)%z-ptY%z)
    section_XYZ = section_XYZ + sign(distanceH(p3D(ird),ptY),p_scalaire_H(ptX,ptY,p3D(ird),ptY)) * (h1+0.5_long*h2)
    if (maxval(p3D(irg:ird)%z) < z) then
        do i = irg, ird-1
          h1 = z - max(p3D(i)%z,p3D(i+1)%z)
          h2 = abs(p3D(i)%z-p3D(i+1)%z)
          section_XYZ = section_XYZ + sign(distanceH(p3D(i),p3D(i+1)),p_scalaire_H(ptX,ptY,p3D(i),p3D(i+1))) * (h1+0.5_long*h2)
        enddo
    else  !il y a des points hauts, il faut ignorer les parties émergées
        do i = irg, ird-1
          if  (p3D(i)%z < z .and. p3D(i+1)%z < z) then
              h1 = z - max(p3D(i)%z,p3D(i+1)%z)
              h2 = abs(p3D(i)%z-p3D(i+1)%z)
              section_XYZ = section_XYZ + sign(distanceH(p3D(i),p3D(i+1)),p_scalaire_H(ptX,ptY,p3D(i),p3D(i+1))) * (h1+0.5_long*h2)
          elseif (p3D(i)%z >= z .and. p3D(i+1)%z >= z) then
              continue
          elseif (p3D(i)%z < z .and. p3D(i+1)%z >= z) then
              ptW = interpol3D(p3D(i),p3D(i+1),z)
              h2 = ptW%z - p3D(i)%z
              section_XYZ = section_XYZ + sign(distanceH(p3D(i),ptW),p_scalaire_H(ptX,ptY,p3D(i),ptW)) * 0.5_long*h2
          elseif  (p3D(i)%z >= z .and. p3D(i+1)%z < z) then
              ptW = interpol3D(p3D(i),p3D(i+1),z)
              h2 = ptW%z - p3D(i+1)%z
              section_XYZ = section_XYZ + sign(distanceH(ptW,p3D(i+1)),p_scalaire_H(ptX,ptY,ptW,p3D(i+1))) * 0.5_long*h2
          endif
        enddo
    endif
  end function section_XYZ


  subroutine limite_Eau(self, z)
  !==============================================================================
  !   détermination des points limites RG et RD pour un niveau d'eau donné
  !
  !   irg et ird sont les premiers indices en partant des rives gauche et
  !   droite qui correspondent à des points sous la surface de l'eau
  !   ptX et ptY sont les points interpolés où le plan d'eau intersecte le profil
  !   known_level est le niveau d'eau pour lequel on a obtenu irg, ird, ptX et ptY
  !==============================================================================
    ! prototype
    class(profil),intent(inout) :: self
    real(kind=long),intent(in) :: z
    ! variables locales
    integer :: i, np, irg, ird
    type(point3D) :: ptX, ptY

    !print*,'>>>> Appel limite_Eau() pour ', self%pk, self%known_level, z

    !Si le test suivant est vrai, alors limite_Eau() a déjà été appelé pour ce niveau d'eau et on y va en confiance
    if (abs(z-self%known_level) < 0.0001_long) then
      !irg, ird, ptX et ptY sont déjà à jour
      self%known_level = z
      return
    elseif (z < self%zf) then
      write(l9,*) '>>>> limite_Eau() impossible : niveau < cote du fond'
      write(l9,*) ' pk = ',self%pk,' Z = ',z,' Zf = ',self%zf
      ! on sort avec irg = ird = -1 qui signale un problème
      self%irg = -1  ;  self%ird = -1
      self%ptX = self%xyz(1)  ;  self%ptY = self%xyz(self%np)
      return
    else
      !initialisation
      irg = -1  ;  ird = -1
      !recherche des index irg et ird sur la section totale
      np = self%np
      i = 0
      do while (irg < 0 .and. i < np)
        i = i + 1
        if (self%xyz(i)%z <= z) irg = i
      enddo
      i = np + 1
      do while (ird < 0 .and. i > 1)
        i = i - 1
        if (self%xyz(i)%z <= z) ird = i
      enddo
      if (irg < 0 .or. ird < 0) then
        write(error_unit,*) '>>>> Erreur dans limite_Eau() : profil sec'
        write(error_unit,*) self%name,self%pk,self%zf,z,irg,ird
        call do_crash('limite_Eau()')
        stop 6
      endif

      !interpolation des points ptX et ptY
      if (irg > 1) then
        ptX = interpol3D(self%xyz(irg-1),self%xyz(irg),z)
      else
        ptX = self%xyz(1)
      endif
      if (ird < self%np) then
        ptY = interpol3D(self%xyz(ird),self%xyz(ird+1),z)
      else
        ptY = self%xyz(self%np)
      endif
      !mise à jour de %known_level
      self%known_level = z
      self%irg = irg
      self%ird = ird
      self%ptX = ptX
      self%ptY = ptY
    endif

  end subroutine limite_Eau

  function largeur_LC(self,z)
  !==============================================================================
  ! interpolation de la largeur au miroir dans un profil pour une cote donnée
  !
  ! NB : cette fonction utilise la représentation largeurs-cotes du profil et
  !      modifie le profil en fixant son attribut ip

  !entrée : self, de type profil
  !         z, la cote à laquelle il faut calculer la largeur
  !sortie : la valeur de la largeur à la cote z
  !
  !==============================================================================
    ! prototype
    class(profil),intent(inout), target :: self  !cette fonction modifie le profil
                                                 !en fixant son attribut ip
    real(kind=long),intent(in) :: z
    real(kind=long) :: largeur_LC
    ! variables locales
    integer :: ip0, i
    real(kind=long) :: hh
    class(pointLC),dimension(:), pointer :: wh

    !calcul de la largeur totale à partir de la projection largeur-cote
    hh = z-self%zf
    if (hh < zero) then
        largeur_LC = -1._long
        return
    endif
    wh(1:) => self%wh(:)

    if (.NOT.self%wh_aJour) stop 905
    ip0 = self%ip
    if (hh >= wh(ip0)%h .AND. hh < wh(ip0+1)%h) then  !niveau courant
        largeur_LC = wh(ip0)%l + (hh-wh(ip0)%h)*(wh(ip0+1)%l-wh(ip0)%l)/(wh(ip0+1)%h-wh(ip0)%h)  ! interpolation linéaire
        return
    elseif (hh < wh(ip0)%h) then                            ! recherche vers le bas
        do i = ip0, 1, -1
          if (hh >= wh(i)%h) then
              largeur_LC = wh(i)%l + (hh-wh(i)%h)*(wh(i+1)%l-wh(i)%l)/(wh(i+1)%h-wh(i)%h)  ! interpolation linéaire
              self%ip = i
              return
          endif
        enddo
        write(error_unit,*) 'Erreur inf dans Largeur() au profil : ',self%name, self%pk, self%zf, hh
        stop 901 ! on arrive au fond du profil
    elseif (hh >= wh(ip0+1)%h) then                         !recherche vers le haut
        do i = ip0, self%nh-1
          if (hh < wh(i+1)%h) then
              largeur_LC = wh(i)%l + (hh-wh(i)%h)*(wh(i+1)%l-wh(i)%l)/(wh(i+1)%h-wh(i)%h)  ! interpolation linéaire
              self%ip = i
              return
          endif
        enddo
        write(error_unit,*) 'Erreur sup dans Largeur() au profil ',self%name, hh, self%nh, self%np, self%zf
        do i = 1, self%nh
          write(error_unit,*) self%wh(i)%h, self%wh(i)%l
        enddo
        call do_crash('largeur_LC()')
        stop 902 ! on arrive au sommet du profil
    else
        largeur_LC = -1._long
        write(error_unit,*) 'Erreur pas normale dans GeometrieSection_largeur() au profil : ', &
                            self%name, self%pk, self%zf, z
        stop 6
    endif
  end function largeur_LC


  function largeur_XYZ(un_profil,z,k)
  !==============================================================================
  ! interpolation de la largeur au miroir dans un profil pour une cote donnée
  ! directement à partir du profil 3D
  !
  !entrée : un_profil, de type profil
  !         z, la cote à laquelle il faut calculer la largeur
  !         k, le numéro de la sous-section, 0 pour la section totale
  !sortie : la valeur de la largeur à la cote z
  !
  !==============================================================================
    ! prototype
    class(profil),intent(inout), target :: un_profil  !cette fonction modifie le profil
                                              !en fixant son attribut ip
    real(kind=long),intent(in) :: z
    integer, intent(in) :: k
    real(kind=long) :: largeur_XYZ
    ! variables locales
    integer :: i, irg, ird
    type(point3D) :: ptX, ptY, ptW
    type(point3D), dimension(:), pointer :: p3D
    integer, dimension(:), pointer :: lit

    p3D(1:) => un_profil%xyz(:)
    lit(0:) => un_profil%li(:)

    call limite_Eau(un_profil,z)
    irg = un_profil%irg  ;  ird = un_profil%ird
    ptX = un_profil%ptX  ;  ptY = un_profil%ptY
    if (irg == -1 .and. ird == -1) then
      !il y a eu un problème dans limite_Eau()
      largeur_XYZ = -1._long  ;  return
    endif
    if (k >= 1 .and. k <= un_profil%nzone) then
      if (irg > lit(k)) then
        largeur_XYZ = zero  ;  return
      elseif (ird < lit(k-1)) then
        largeur_XYZ = zero  ;  return
      else
        if (irg <= lit(k-1)) then
          ptX = p3D(lit(k-1))
          irg = lit(k-1)
        endif
        if (ird >= lit(k)) then
          ptY = p3D(lit(k))
          ird = lit(k)
        endif
      endif
    elseif (k > 0) then
      write(error_unit,'(a,i0)') '>>>> ERREUR dans largeur_XYZ : numéro de sous-section erroné : ',k
      stop 6
    endif
    if (maxval(p3D(irg:ird)%z) < z) then
      !largeur_XYZ = distanceH(ptX,ptY) !NON! le profil n'est pas obligatoirement plan
      !l'utilisation de SIGN() et de p_scalaire_H() permet de tenir compte des profils non convexes
      !qui se referment (arche de pont par exemple)
      largeur_XYZ = sign(distanceH(ptX,p3D(irg)),p_scalaire_H(ptX,ptY,ptX,p3D(irg)))
      largeur_XYZ = largeur_XYZ + sign(distanceH(p3D(ird),ptY),p_scalaire_H(ptX,ptY,p3D(ird),ptY))
      do i = irg, ird-1
        largeur_XYZ = largeur_XYZ + sign(distanceH(p3D(i),p3D(i+1)),p_scalaire_H(ptX,ptY,p3D(i),p3D(i+1)))
      enddo
    else  !il y a des points hauts, il faut ignorer les parties émergées
      largeur_XYZ = sign(distanceH(ptX,p3D(irg)),p_scalaire_H(ptX,ptY,ptX,p3D(irg)))
      largeur_XYZ = largeur_XYZ + sign(distanceH(p3D(ird),ptY),p_scalaire_H(ptX,ptY,p3D(ird),ptY))
      do i = irg, ird-1
        if  (p3d(i)%z < z .and. p3d(i+1)%z < z) then
          largeur_XYZ = largeur_XYZ + sign(distanceH(p3D(i),p3D(i+1)),p_scalaire_H(ptX,ptY,p3D(i),p3D(i+1)))
        elseif (p3d(i)%z >= z .and. p3d(i+1)%z >= z) then
          continue
        elseif (p3d(i)%z < z .and. p3d(i+1)%z >= z) then
          ptW = interpol3D(p3D(i),p3D(i+1),z)
          largeur_XYZ = largeur_XYZ + sign(distanceH(p3D(i),ptW),p_scalaire_H(ptX,ptY,p3D(i),ptW))
        elseif  (p3d(i)%z >= z .and. p3d(i+1)%z < z) then
          ptW = interpol3D(p3D(i),p3D(i+1),z)
          largeur_XYZ = largeur_XYZ + sign(distanceH(ptW,p3D(i+1)),p_scalaire_H(ptX,ptY,ptW,p3D(i+1)))
        endif
      enddo
    endif
  end function largeur_XYZ


  function largeur(self,z,k)
  !==============================================================================
  !  interpolation de la largeur au miroir dans un profil pour une cote donnée
  !
  !entrée : un_profil, de type profil
  !         z, la cote à laquelle il faut calculer le périmètre
  !         k, le numéro de la sous-section, 0 pour la section totale
  !            calcul sur la représentation largeurs-cotes si absent
  !sortie : la valeur du périmètre à la cote z
  !
  !==============================================================================
    ! prototype
    class(profil),intent(inout), target :: self
    real(kind=long),intent(in) :: z
    integer, intent(in), optional :: k
    real(kind=long) :: largeur

    if (.not.present(k) .or. k==0) then
      if (is_LC) then
        largeur = self%largeur_LC(z)
      else
        largeur = self%largeur_XYZ(z,0)
      endif
    else
      largeur = self%largeur_XYZ(z,k)
    endif
    if (largeur < zero) then
      write(l9,*) '>>>> ERREUR : largeur mouillée négative au pk ',self%pk,' : ',largeur,z,k
    endif
  end function largeur


  function perimetre_LC(self,z)
  !==============================================================================
  !    interpolation du périmètre mouillé dans un profil pour une cote donnée
  !
  ! NB : cette fonction utilise la représentation largeurs-cotes du profil et
  !      modifie le profil en fixant son attribut ip
  !
  !entrée : self, de type profil
  !         z, la cote à laquelle il faut calculer le périmètre
  !sortie : la valeur du périmètre à la cote z
  !
  !==============================================================================
    ! prototype
    class(profil),intent(inout), target :: self
    real(kind=long),intent(in) :: z
    real(kind=long) :: perimetre_LC
    ! variables locales
    integer :: i, ip0
    real(kind=long) ::h0, p0, h1, p1, hip0, pip0, hip1, pip1
    real(kind=long), dimension(:), pointer :: pr, h
    real(kind=long) :: hh

    hh = z-self%zf
    if (hh < zero) then
      perimetre_LC = -1._long
      return
    endif

    if (.NOT.self%wh_aJour) stop 906
    ip0 = self%ip
    hip0 = self%wh(ip0)%h
    pip0 = self%wh(ip0)%p
    hip1 = self%wh(ip0+1)%h
    pip1 = self%wh(ip0+1)%p
    if (hh >= hip0 .AND. hh < hip1) then  !niveau courant
      perimetre_LC = pip0 + (hh-hip0)*(pip1-pip0)/(hip1-hip0)  ! interpolation linéaire
      return
    elseif (hh < hip0) then                            ! recherche vers le bas
      do i = ip0, 1, -1
        h0 = self%wh(i)%h
        p0 = self%wh(i)%p
        h1 = self%wh(i+1)%h
        p1 = self%wh(i+1)%p
        if (hh >= h0) then
          perimetre_LC = p0 + (hh-h0)*(p1-p0)/(h1-h0)  ! interpolation linéaire
          self%ip = i
          return
        endif
      enddo
      stop 903 ! on arrive au fond du profil
    elseif (hh >= hip1) then                         !recherche vers le haut
      do i = ip0, self%nh-1
        h0 = self%wh(i)%h
        p0 = self%wh(i)%p
        h1 = self%wh(i+1)%h
        p1 = self%wh(i+1)%p
        if (hh < h1) then
          perimetre_LC = p0 + (hh-h0)*(p1-p0)/(h1-h0)  ! interpolation linéaire
          self%ip = i
          return
        endif
      enddo
        stop 904 ! on arrive au sommet du profil
    else
      perimetre_LC = -1._long
      write(error_unit,*) 'Erreur pas normale dans TopoGeometrie_perimetre()'
      stop 6
    endif
  end function perimetre_LC


  function perimetre_XYZ(self,z,k)
  !==============================================================================
  ! interpolation du périmètre mouillé dans un profil pour une cote donnée
  ! directement à partir du profil 3D
  !
  !entrée : self, de type profil
  !         z, la cote à laquelle il faut calculer le périmètre
  !         k, le numéro de la sous-section, 0 pour la section totale
  !sortie : la valeur du périmètre à la cote z
  !
  !==============================================================================
    ! prototype
    class(profil),intent(inout), target :: self
    real(kind=long),intent(in) :: z
    integer, intent(in) :: k
    real(kind=long) :: perimetre_XYZ
    ! variables locales
    integer :: i, irg, ird
    type(point3D) :: ptX, ptY, ptW
    type(point3D), dimension(:), pointer :: p3D
    integer, dimension(:), pointer :: lit

    p3D(1:) => self%xyz(:)
    lit(0:) => self%li(:)

    if (z < self%zf+precision_alti) then
      perimetre_XYZ = zero
      return
    endif

    ! recherche des 2 points extrêmes du profil qui intersectent le plan d'eau
    call limite_Eau(self,z)
    irg = self%irg  ;  ird = self%ird
    ptX = self%ptX  ;  ptY = self%ptY
    if (irg == -1 .or. ird == -1) then
      !il y a eu un problème dans limite_Eau()
      perimetre_XYZ = -1._long  ;  return
    endif
    if (k >= 1 .and. k <= self%nzone) then
    !calcul du périmètre mouillé du lit n° k
      if (irg > lit(k) .OR. ird < lit(k-1)) then
        perimetre_XYZ = zero  ;  return
      else
        if (irg <= lit(k-1)) then
          ptX = p3D(lit(k-1))
          irg = lit(k-1)
        endif
        if (ird >= lit(k)) then
          ptY = p3D(lit(k))
          ird = lit(k)
        endif
      endif
    elseif (abs(k) > 0) then
        write(error_unit,'(a,i0)') '>>>> ERREUR dans perimetre_XYZ : numéro de sous-section erroné : ',k
        stop 6
    endif
    !ici on a irg, ird, ptX et ptY pour n'importe quelle valeur de k
    !les points sont dans l'ordre suivant de rive gauche vers rive droite : ptX -> irg -> ird -> ptY
    if (maxval(p3D(irg:ird)%z) < z) then
      !il n'y a pas de point haut qui dépasse la cote de l'eau
      perimetre_XYZ = distance3D(ptX,p3D(irg)) + distance3D(p3D(ird),ptY)
      do i = irg, ird-1
        perimetre_XYZ = perimetre_XYZ + distance3D(p3D(i),p3D(i+1))
      enddo
    else
      !il y a des points hauts, il faut ignorer les parties émergées
      perimetre_XYZ = distance3D(ptX,p3D(irg)) + distance3D(p3D(ird),ptY)
      do i = irg, ird-1
        if  (p3D(i)%z < z .and. p3D(i+1)%z < z) then
          perimetre_XYZ = perimetre_XYZ + distance3D(p3D(i),p3D(i+1))
        elseif (p3D(i)%z >= z .and. p3D(i+1)%z >= z) then
          continue
        elseif (p3D(i)%z < z .and. p3D(i+1)%z >= z) then
          ptW = interpol3D(p3D(i),p3D(i+1),z)
          perimetre_XYZ = perimetre_XYZ + distance3D(p3D(i),ptW)
        elseif  (p3D(i)%z >= z .and. p3D(i+1)%z < z) then
          ptW = interpol3D(p3D(i),p3D(i+1),z)
          perimetre_XYZ = perimetre_XYZ + distance3D(ptW,p3D(i+1))
        endif
      enddo
    endif
    !correction pour les frottements sur les murs extérieurs
    i = self%np
    if ((k == 0 .OR. k == 1) .AND. z > p3D(1)%z) perimetre_XYZ = perimetre_XYZ + z-p3D(1)%z
    if ((k == 0 .OR. k == self%nzone) .AND. z > p3D(i)%z) perimetre_XYZ = perimetre_XYZ + z-p3D(i)%z

  end function perimetre_XYZ


  function perimetre(self,z,k)
  !==============================================================================
  !    interpolation du périmètre mouillé dans un profil pour une cote donnée
  !
  !entrée : un_profil, de type profil
  !         z, la cote à laquelle il faut calculer le périmètre
  !         k, le numéro de la sous-section, 0 pour la section totale
  !            calcul sur la représentation largeurs-cotes si absent
  !sortie : la valeur du périmètre à la cote z
  !
  !==============================================================================
    ! prototype
    class(profil),intent(inout), target :: self
    real(kind=long),intent(in) :: z
    integer, intent(in), optional :: k
    real(kind=long) :: perimetre

    if (.not.present(k) .or. k==0) then
      if (is_LC) then
        perimetre = self%perimetre_LC(z)
      else
        perimetre = self%perimetre_XYZ(z,0)
      endif
    else
      perimetre = self%perimetre_XYZ(z,k)
    endif
    if (perimetre < zero) then
      write(l9,*) '>>>> ERREUR : périmètre mouillé négatif au pk ',self%pk,' : ',perimetre,z,k
    endif
  end function perimetre

  subroutine largeursCotes(self)
  !==============================================================================
  !        tabulation d'un profil en travers à partir de ses données brutes

  !entrée : self%xyz, les données brutes du profil (type profil)
  !sortie : self%wh, les données tabulées en largeurs-cotes du profil
  !
  ! DONE: vérifier la prise en compte du cas des zones plates : deux abscisses successives pour la même cote.
  !==============================================================================
    ! prototype
    class(profil),intent(inout) :: self
    type(profilAC) :: pAC
    ! variables locales
    real(kind=long) :: z0, ll, pp, dy, dz, l1, l2, dh, hh
    integer :: i, j, np, point1
    type(pointAC) :: pc1, pc2 !limites gauche et droite du lit courant
    logical :: trace_ON=.false., trace_WH = .false.

    !initialisation du profil AC
    pAC = self%projectionPlan()
    !arrondi à la précision precision_alti
    do i = 0, pAC%np+1
        pAC%yz(i)%z = aint(pAC%yz(i)%z / precision_alti , long) *  precision_alti
    enddo
    !initialisation du profil LC
    np = pAC%np
    self%nh = np+1
    if(.not.allocated(self%wh)) then
      allocate(self%wh(1:np+1))
    elseif(size(self%wh).lt.np+1) then
      deallocate(self%wh)
      allocate(self%wh(1:np+1))
    endif
    !décalage des cotes pour les zones plates
    !on suppose que l'utilisateur ne peut pas définir des cotes avec une précision
    !inférieure à precision_alti
    do i = 1, pAC%np-1
        if (abs(pAC%yz(i)%z - pAC%yz(i+1)%z) < precision_alti) then
          pAC%yz(i+1)%z = pAC%yz(i+1)%z - precision_alti*2._long
          !il faut enlever 2*precision_alti et non ajouter sinon le calcul du
          !périmètre mouillé ne fonctionne pas !!! (JBF 2010-12-17)
        endif
    enddo
    !recherche des largeurs
    do i = 1, pAC%np  ! boucle sur toutes les altitudes du profil
        z0 = pAC%yz(i)%z
        ll = 0._long      !initialisation de la largeur au miroir cherchée
        pp = 0._long      !initialisation du périmètre mouillé
        point1 = -1
        if (trace_ON) write(l9,*) 'LC1 : ',z0,ll,pp,point1
        do j = 1, pAC%np   !on cherche les points du profil qui encadrent cette altitude
          if (abs(z0-pAC%yz(j)%z) < precision_alti) then
              if (point1 < 0) then !nouveau lit
                if (pAC%yz(j+1)%z > z0) then
                    if (trace_ON) write(l9,*) 'LC2a : ',j, pAC%yz(j)%z, pAC%yz(j+1)%z, point1
                    point1 = -1  ! branche montante : on est dans un minimum local du profil
                else
                    if (trace_ON) write(l9,*) 'LC2b : ',j, pAC%yz(j)%z, pAC%yz(j+1)%z, point1
                    point1 = j  ;  pc1 = pAC%yz(j)
                endif
              else                 ! fin du lit en cours
                pc2 = pAC%yz(j)
                pp = pp + distance2D(pAC%yz(j-1),pc2)
                ll = ll + (pc2%y - pc1%y)
                if (trace_ON) write(l9,*) 'LC3 : ',j, pc2, pp, ll
                if (z0 >= pAC%yz(j-1)%z .and. z0 > pAC%yz(j+1)%z) then
                    point1 = j  !nouveau lit : z0 est un sommet local
                    pc1 = pAC%yz(j)
                    if (trace_ON) write(l9,*) 'LC4a : ',j, point1
                else
                    point1 = -1
                    if (trace_ON) write(l9,*) 'LC4b : ',j, point1
                endif
              endif
          else if ( between(z0,pAC%yz(j-1)%z,pAC%yz(j)%z) ) then
              if (trace_ON) write(l9,*) 'LC5 : ',j, z0, pAC%yz(j-1)%z, pAC%yz(j)%z, point1
              if (point1 < 0) then  !nouveau lit
                point1 = j-1
                dy = pAC%yz(j)%y - pAC%yz(j-1)%y
                dz = pAC%yz(j)%z - pAC%yz(j-1)%z
                pc1%y = pAC%yz(j-1)%y + (z0-pAC%yz(j-1)%z)*dy/dz
                pc1%z = z0
                pp = pp + distance2D(pc1,pAC%yz(j))
                if (trace_ON) write(l9,*) 'LC6a : ',j, pc1%y, pc1%z, pp, point1
              else                  !fin du lit courant
                dy = pAC%yz(j)%y - pAC%yz(j-1)%y
                dz = pAC%yz(j)%z - pAC%yz(j-1)%z
                pc2%y = pAC%yz(j-1)%y + (z0-pAC%yz(j-1)%z)*dy/dz
                pc2%z = z0
                ll = ll + (pc2%y-pc1%y)
                pp = pp + distance2D(pc2,pAC%yz(j-1))
                point1 = -1
                if (trace_ON) write(l9,*) 'LC6b : ',j, pc1%y, pc2%y, ll, pp, point1
              endif
          else if (z0 >= pAC%yz(j-1)%z .and. z0 >= pAC%yz(j)%z) then
              if (point1 >= 0) pp = pp + distance2D(pAC%yz(j-1),pAC%yz(j))  !NB: point1 peut être nul
              if (trace_ON) write(l9,*) 'LC7a : ',j, ll, pp, point1
          else if (z0 <= pAC%yz(j-1)%z .and. z0 <= pAC%yz(j)%z) then
              if (trace_ON) write(l9,*) 'LC7b : ',j, ll, pp, point1
              continue
          else if (abs(z0-pAC%yz(j-1)%z) < precision_alti) then
              if (trace_ON) write(l9,*) 'LC7c : ',j, ll, pp, point1
              continue
          else
              if(verbose) write(l9,*) ' >>>> ERREUR : On ne devrait pas arriver ici !!!'
              if(verbose) write(l9,*) self%pk, z0, pAC%yz(j-1)%z, pAC%yz(j)%z
              stop 666
          endif
        enddo
        if (point1 > 0) then
          if (trace_ON) write(l9,*) 'LC8a : ',ll, pp, point1
          ll = ll + pAC%yz(np+1)%y - pc1%y
          pp = pp + z0 - pAC%yz(np)%z
          if (trace_ON) write(l9,*) 'LC8b : ',ll, pp, point1
        endif
        self%wh(i)%h = z0 - self%zf
        self%wh(i)%l = ll
        self%wh(i)%p = pp
    enddo
    self%wh(np+1)%h = pAC%yz(0)%z-self%zf
    self%wh(np+1)%l = pAC%yz(np+1)%y
  !   self%wh(np+1)%p = self%wh(np)%p +200._long


    call triZ(self)  !classement du profil LC par Z croissants
                                !np est modifié par triZ() !!!

    np = self%nh
    self%wh(np)%p = self%wh(np-1)%p +200._long


    !élimination du premier point s'il est à la même cote que le 2ème (fond plat)
    !on suppose que l'utilisateur ne peut pas définir des cotes avec une précision
    !inférieure à precision_alti
    if (abs(self%wh(1)%h-self%wh(2)%h) < precision_alti) then
        hh = self%wh(1)%h
        do i = 1, self%nh-1
          self%wh(i) = self%wh(i+1)
        enddo
        self%wh(1)%h = hh
        self%nh = self%nh - 1
    endif

    !calcul des sections mouillées par la méthode des trapèzes
    do i = 1, self%nh
        self%wh(i)%s = 0._long
    enddo
    do i = 2, self%nh
        l1 = self%wh(i-1)%l  ;  l2 = self%wh(i)%l
        dh = self%wh(i)%h - self%wh(i-1)%h
        self%wh(i)%s = self%wh(i-1)%s + 0.5_long*(l1+l2)*dh
    enddo
    !vérification que le périmètre mouillé est bien croissant
    do i = 2, self%nh
        if (self%wh(i)%p < self%wh(i-1)%p) then
          if(verbose) write(l9,*) ' >>>> erreur dans la conversion en largeurs-cotes : périmètre mouillé décroissant'
          if(verbose) write(l9,*) ' Profil : ',self%pk
          if(verbose) write(l9,*) self%zf+self%wh(i-1)%h, self%wh(i-1)
          if(verbose) write(l9,*) self%zf+self%wh(i)%h, self%wh(i)
          if(verbose) write(l9,*) ' >>>> profil largeurs-cotes complet :'
          do j = 1, self%nh
              if(verbose) write(l9,*) self%zf+self%wh(j)%h, self%wh(j)
          enddo
          if(verbose) write(l9,*) ' >>>> profil abscisses-cotes complet :'
          do j = 0, pAC%np+1
              if(verbose) write(l9,*)  pAC%yz(j), pAC%yz(j)%z-self%zf
          enddo
          call do_crash('largeursCotes()')
          stop 666
        endif
    enddo

    if (trace_WH) then
        write(l9,*) ' Pk = ',self%pk
        do i = 1, self%nh
          write(l9,*) self%wh(i)
        enddo
    endif

    self%wh_aJour = .true.
    self%ip = 1
    deallocate(pAC%yz)
  end subroutine largeursCotes



  subroutine largeursCotes_alt(self)
  !==============================================================================
  !        tabulation d'un profil en travers à partir de ses données brutes

  !entrée : self%xyz, les données brutes du profil (type profil)
  !sortie : self%wh, les données tabulées en largeurs-cotes du profil
  !
  ! DONE: vérifier la prise en compte du cas des zones plates : deux abscisses successives pour la même cote.
  !==============================================================================
    implicit none
    ! prototype
    class(profil),intent(inout) :: self
    ! variables locales
    real(kind=long) :: z0, hh, l1, l2, dh
    integer :: i, j, np
    type(profil) :: prfl
    type(profilAC) :: pAC

    np = self%np
    if(.not.allocated(self%wh)) then
      allocate(self%wh(1:np+1))
    elseif(size(self%wh).lt.np+1) then
      deallocate(self%wh)
      allocate(self%wh(1:np+1))
    endif
    self%nh = np+1
    self%ip = 1
    select case (LC_type)
        case (1)
          ! on fait le calcul sur le profil 3D
          do i = 1, self%np  ! boucle sur toutes les altitudes du profil
              z0 = self%xyz(i)%z
              self%wh(i)%h = z0 - self%zf
              self%wh(i)%l = self%largeur_XYZ(z0,0)
              self%wh(i)%p = self%perimetre_XYZ(z0,0)
!               self%wh(i)%s = self%section_XYZ(z0,0)
          enddo
        case (2)
          ! on fait le calcul sur la projection abscisses-cotes du profil 3D
          pAC = projectionPlan(self)
          prfl = self
          prfl%xyz(1:np)%x = pAC%pk
          prfl%xyz(1:np)%y = pAC%yz(1:np)%y
          prfl%xyz(1:np)%z = pAC%yz(1:np)%z
          do i = 1, self%np  ! boucle sur toutes les altitudes du profil
              z0 = self%xyz(i)%z
              self%wh(i)%h = z0 - self%zf
              self%wh(i)%l = prfl%largeur_XYZ(z0,0)
              self%wh(i)%p = prfl%perimetre_XYZ(z0,0)
!               self%wh(i)%s = prfl%section_XYZ(z0,0)
          enddo
        case default
          write(error_unit,'(a,i0)') '>>>> BUG dans largeursCotes_alt() : valeur erronnée de LC_type : ',LC_type
          call do_crash('largeursCotes_alt()')
    end select

    ! Prolongement de 100 m
    self%wh(np+1)%h = self%wh(np)%h + 100._long
    self%wh(np+1)%l = self%wh(np)%l

    call self%triZ()  !classement du profil LC par Z croissants
                                !nh est modifié par triZ() !!!
    np = self%nh
    self%wh(np)%p = self%wh(np-1)%p +200._long


    !élimination du premier point s'il est à la même cote que le 2ème (fond plat)
    !on suppose que l'utilisateur ne peut pas définir des cotes avec une précision
    !inférieure à precision_alti
    if (abs(self%wh(1)%h-self%wh(2)%h) < precision_alti) then
        hh = self%wh(1)%h
        do i = 1, self%nh-1
          self%wh(i) = self%wh(i+1)
        enddo
        self%wh(1)%h = hh
        self%nh = self%nh - 1
    endif

    !vérification que le périmètre mouillé est bien croissant
    do i = 2, self%nh
        if (self%wh(i)%p < self%wh(i-1)%p) then
          if(verbose) write(l9,*) ' >>>> erreur dans la conversion en largeurs-cotes : périmètre mouillé décroissant'
          if(verbose) write(l9,*) ' Profil : ',self%pk
          if(verbose) write(l9,*) self%zf+self%wh(i-1)%h, self%wh(i-1)
          if(verbose) write(l9,*) self%zf+self%wh(i)%h, self%wh(i)
          if(verbose) write(l9,*) ' >>>> profil largeurs-cotes complet :'
          do j = 1, self%nh
              if(verbose) write(l9,*) self%zf+self%wh(j)%h, self%wh(j)
          enddo
          call do_crash('largeursCotes_alt()')
          stop 666
        endif
    enddo

    !calcul des sections mouillées par la méthode des trapèzes
    do i = 1, self%nh
        self%wh(i)%s = 0._long
    enddo
    do i = 2, self%nh
        l1 = self%wh(i-1)%l  ;  l2 = self%wh(i)%l
        dh = self%wh(i)%h - self%wh(i-1)%h
        self%wh(i)%s = self%wh(i-1)%s + 0.5_long*(l1+l2)*dh
    enddo

    self%wh_aJour = .true.
    self%ip = 1
  end subroutine largeursCotes_alt

  function projectionPlan(self)
  !==============================================================================
  !        conversion d'un profil XYZ en profil AC par projection
  !              sur le plan joignant les 2 points extrêmes
  !
  ! NB : dans cette version on ne tient pas compte de la pente du profil en long

  !entrée : self, un profil
  !sortie : le même profil converti en abscisses-cotes, de type profilAC
  !
  !==============================================================================
    !prototype
    class(profil_brut), intent(in), target :: self
    type(profilAC) :: projectionPlan
    !variables locales
    type(point3D), pointer :: xyz1, xyz2, xyz
    integer :: ip, np

    !les points extrêmes du profil
    np = self%np
    xyz1 => self%xyz(1)
    xyz2 => self%xyz(np)

    !initialisation du profil AC
    projectionPlan%np = np
    projectionPlan%pk = self%pk
    allocate(projectionPlan%yz(0:np+1))  !on ajoute 2 points à 100 m au dessus des extrémités
    projectionPlan%yz(0)%y = 0._long  ;  projectionPlan%yz(0)%z = max(xyz1%z+100._long , xyz2%z+100._long)
    projectionPlan%yz(1)%y = 0._long  ;  projectionPlan%yz(1)%z = xyz1%z
    do ip = 2, np
    ! NOTE: pas de problème pour aller jusqu'au dernier point, on trouve la distance entre 1 et np
      xyz => self%xyz(ip)
      projectionPlan%yz(ip) = xyz%projectionOrtho(xyz1,xyz2)
    enddo
    projectionPlan%yz(np+1)%y = projectionPlan%yz(np)%y  ;  projectionPlan%yz(np+1)%z = projectionPlan%yz(0)%z
  end function projectionPlan

  function strickler_lit_moyen(self,z)
  !==============================================================================
  !  Calcul d'un coefficient de Strickler unique pour le lit moyen
  !==============================================================================
    ! -- prototype --
    class(profil), intent(inout) :: self
    real(kind=long), intent(in) :: z
    real(kind=long) :: strickler_lit_moyen
    ! -- variables --
    real(kind=long) :: p_l, p_r

    ! /!\ modifie self /!\ !

    p_l = self%perimetre(z,1)
    p_r = self%perimetre(z,3)

    if (p_l+p_r > zero) then
        strickler_lit_moyen =  (p_l+p_r) / (p_l/self%ks(1) + p_r/self%ks(3))
    else
        strickler_lit_moyen = 2._long / (un/self%ks(1) + un/self%ks(3))
    endif
    strickler_lit_moyen = 0.0001_long * anint(strickler_lit_moyen * 10000._long)
  end function strickler_lit_moyen


  subroutine triZ(self)
  !==============================================================================
  !   classement d'un profil LC par Z croissants avec élimination des doublons
  !   les doublons sont déportés au sommet de la liste et "oubliés" en réduisant nh

  !entrée : wh, tableau de pointLC (largeurs-cotes)
  !sortie : le tableau wh trié par cotes croissantes
  !         nh, entier, la taille utile du tableau wh
  !
  ! DONE: vérifier la prise en compte du cas des zones plates : deux abscisses successives pour la même cote.
  !==============================================================================
    !prototype
    class(profil), intent(inout), target :: self
    !variables locales
    type(pointLC), dimension(:), pointer :: wh
    integer :: i, nbperm
    real(kind=long) :: h0, l0, p0, s0
    !tri
    wh => self%wh
    self%nh = size(self%wh)
    nbperm = 1
    do while (nbperm > 0)
        nbperm = 0
  ! DONE: décider quelle est la bonne borne ci-dessous.
  !      do i = 1, np-1  !-JBF 13/06/2008
        do i = 1, self%nh-1   !+JBF 13/06/2008
          if (abs(wh(i)%h - wh(i+1)%h) < 0.00001_long) then
              wh(i+1)%h = wh(i+1)%h + 9999._long
              self%nh = self%nh -1
          else if (wh(i)%h > wh(i+1)%h) then  !permutation
              nbperm = nbperm + 1
              h0 = wh(i)%h ; l0 = wh(i)%l ; p0 = wh(i)%p ; s0 = wh(i)%s
              wh(i)%h = wh(i+1)%h  ;  wh(i+1)%h = h0
              wh(i)%l = wh(i+1)%l  ;  wh(i+1)%l = l0
              wh(i)%p = wh(i+1)%p  ;  wh(i+1)%p = p0
              wh(i)%s = wh(i+1)%s  ;  wh(i+1)%s = s0
          endif
        enddo
    enddo
  end subroutine triZ


  subroutine init_autres_data_geom(self)
  !==============================================================================
  !   définition des autres données géométriques de section
  !==============================================================================
    ! -- variables --
    integer :: li1,li2
    real(kind=long) :: z
    class(profil) :: self

    ! NOTE: ici on fait l'hypothèse qu'il y a 3 lits : lit majeur gauche, lit mineur, lit majeur droit
    li1 = self%li(1) ; li2 = self%li(2)  !limites du lit mineur
    z = min(self%xyz(li1)%z,self%xyz(li2)%z)
    self%ymoy = z - self%zf
    self%almy = self%largeur(z,self%main)
    self%pmy  = self%perimetre(z,self%main)
    self%smy  = self%section_mouillee(z,self%main)
    self%ybgau = self%xyz(1)%z-self%zf
    self%ybdro = self%xyz(self%np)%z-self%zf
    self%ybmin = min(self%ybgau,self%ybdro)
    self%ybmax = max(self%ybgau,self%ybdro)
    self%smaj_left  = self%section_XYZ(self%xyz(li1)%z,1)
    self%smaj_right = self%section_XYZ(self%xyz(li2)%z,3)
    self%iss = 0
    self%psi_t = zero
    self%drag_force(:) = zero
    self%Ks(:) = -1._long
  end subroutine init_autres_data_geom


  function fusion_profils(prfl1, prfl2) result(prfl3)
    ! concatenation de deux profils
    implicit none
    class(profil_brut), intent(in) :: prfl1, prfl2
    class(profil_brut), allocatable :: prfl3
    ! variable locale
    integer :: i

    select type(prfl1)
      type is(profil_brut)
        allocate(profil_brut::prfl3)
      type is(profil)
        select type(prfl2)
          type is(profil_brut)
            allocate(profil_brut::prfl3)
          type is(profil)
            allocate(profil::prfl3)
      end select
    end select

    prfl3%name = prfl1%name ! choix par defaut, questionable
    prfl3%pk = (prfl1%pk+prfl2%pk)/2 ! moyenne des deux pk, questionable
    prfl3%zf = min(prfl1%zf, prfl2%zf)
    prfl3%np = prfl1%np + prfl2%np
    prfl3%xyz_size = prfl3%np
    allocate(prfl3%xyz(prfl3%xyz_size))
    do i=1, prfl1%np
      prfl3%xyz(i) = prfl1%xyz(i)
    enddo
    do i=1, prfl2%np
      prfl3%xyz(prfl1%np+i) = prfl2%xyz(i)
    enddo
  end function fusion_profils


  subroutine split_profil(self, prfl1, prfl2, tag)
  ! split a profile in two at a named point. This point is duplicated.
    ! prototype
    class(profil_brut), intent(in) :: self
    class(profil_brut), intent(out) :: prfl1, prfl2
    character(len=3), intent(in) :: tag
    ! variables locales
    integer :: i, np
    logical :: tag_found

    prfl1 = self
    prfl2 = self

    tag_found = .false.
    np = self%np
    do i=1,np
      if(.not. tag_found) then
        if (self%xyz(i)%tag == tag) then
          tag_found = .true.
        else
          call prfl1%remove_point(1) ! np and zf already taken into account in remove_point
        endif
      else ! tag found
        call prfl2%remove_point(i) ! np and zf already taken into account in remove_poin
      endif
    enddo
  end subroutine split_profil


  subroutine extract_profil(self, prfl_out, tag1, tag2)
  ! extract a profile between two named point.
    ! prototype
    class(profil_brut), intent(in) :: self
    class(profil_brut), intent(out) :: prfl_out
    character(len=3), intent(in) :: tag1, tag2
    ! variables locales
    integer :: i, np, i1, i2
    logical :: first_tag_found, second_tag_found

    prfl_out = self
    deallocate(prfl_out%xyz)
    first_tag_found = .false.
    second_tag_found = .false.
    np = self%np

    if(tag1 == tag2 .and. trim(tag1).ne.'')then
      write(l9,'(a)')  ' >>>> ERREUR dans extract_profil() : tags identiques'
      stop ' >>>> ERREUR dans extract_profil() : tags identiques'
    endif

    i1 = 1
    i2 = np
    if(trim(tag1) == '' .or. trim(tag1) == 'un') first_tag_found = .true.
    if(trim(tag2) == '' .or. trim(tag2) == 'np') second_tag_found = .true.

    do i=1,np
      if(.not. first_tag_found) then
        if (self%xyz(i)%tag == tag1 .or. self%xyz(i)%tag == tag2) then
          first_tag_found = .true.
          i1=i
        endif
      else if(.not. second_tag_found) then
        if (self%xyz(i)%tag == tag1 .or. self%xyz(i)%tag == tag2) then
          second_tag_found = .true.
          i2=i
        endif
      endif
    enddo

    if(.not. first_tag_found .or. .not. second_tag_found)then
      write(l9,'(a)')  ' >>>> ERREUR dans extract_profil() : tag non trouvé',tag1, tag2
      stop ' >>>> ERREUR dans extract_profil() : tag non trouvé'
    endif

    prfl_out%xyz = [self%xyz(i1:i2)]
    prfl_out%np=i2-i1+1
    prfl_out%xyz_size = size(prfl_out%xyz)

  end subroutine extract_profil


  subroutine patch_profil(self, prfl_in, tag1, tag2)
  ! patch a profile between two named point.
    ! prototype
    class(profil_brut), intent(inout) :: self
    class(profil_brut), intent(in) :: prfl_in
    character(len=3), intent(in) :: tag1, tag2
    ! variables locales
    integer :: i, np, i1, i2
    logical :: first_tag_found, second_tag_found

    first_tag_found = .false.
    second_tag_found = .false.
    np = self%np

    if(tag1 == tag2 .and. trim(tag1).ne.'')then
      write(l9,'(a)')  ' >>>> ERREUR dans patch_profil() : tags identiques'
      stop ' >>>> ERREUR dans patch_profil()'
    endif
    i1 = 1
    i2 = np
    if(trim(tag1) == '' .or. trim(tag1) == 'un') first_tag_found = .true.
    if(trim(tag2) == '' .or. trim(tag2) == 'np') second_tag_found = .true.

    do i=1,np
      if(.not. first_tag_found) then
        if (self%xyz(i)%tag == tag1 .or. self%xyz(i)%tag == tag2) then
          first_tag_found = .true.
          i1=i
        endif
      else if(.not. second_tag_found) then
        if (self%xyz(i)%tag == tag1 .or. self%xyz(i)%tag == tag2) then
          second_tag_found = .true.
          i2=i
        endif
      endif
    enddo

    if(.not. first_tag_found .or. .not. second_tag_found)then
      write(l9,'(a)')  ' >>>> ERREUR dans patch_profil() : tag non trouvé'
      stop ' >>>> ERREUR dans patch_profil()'
    endif

    if(i1 .ne. 1)then
      if (i2 .ne. np)then
        self%xyz = [self%xyz(:i1-1),prfl_in%xyz(:),self%xyz(i2+1:)]
      else
        self%xyz = [self%xyz(:i1-1),prfl_in%xyz(:)]
      endif
    else
      if(i2 .ne. np)then
        self%xyz = [self%xyz(:i1-1),prfl_in%xyz(:),self%xyz(i2+1:)]
      else
        self%xyz = [prfl_in%xyz(:)]
      endif
    endif
    self%np=np-(i2-i1+1)+prfl_in%np
    self%xyz_size = size(self%xyz)

  end subroutine patch_profil


  function get_3D_length(self)
  ! renvoie la longueur du profil en 3D
    ! prototype
    class(profil_brut), intent(in) :: self
    real(kind=long) :: get_3D_length
    ! variables locales
    integer :: i

    get_3D_length = 0
    do i=1, self%np-1
      get_3D_length = get_3D_length + distance3D(self%xyz(i),self%xyz(i+1))
    enddo
  end function get_3D_length


  function get_2D_length(self)
  ! renvoie la longueur du profil en 2D
    ! prototype
    class(profil_brut), intent(in) :: self
    real(kind=long) :: get_2D_length
    ! variables locales
    integer :: i

    get_2D_length = 0
    do i=1, self%np-1
      get_2D_length = get_2D_length + distanceH(self%xyz(i),self%xyz(i+1))
    enddo
  end function get_2D_length


  subroutine interpolate_points_npoints(self, npoints, lplan, keep)
  ! remplace les points du profil par npoints points interpolés sur les segments d'origine.
  ! On ne garde pas les points d'origine sauf les extremites (comptees dans npoints).
  ! Si lplan : distances horizontales
    ! prototype
    class(profil_brut), intent(inout) :: self
    integer :: npoints
    logical :: lplan, keep
    ! variables locales
    real(kind=long), dimension(:), allocatable :: segment_length
    real(kind=long) :: profil_length, pas, disti, distj, d
    integer :: np_in, i, j, k
    class(point3D), dimension(:), allocatable :: xyz

    np_in = self%np
    if (keep) then
      ! on n'enlève pas de points
      if (npoints .le. np_in) return
      ! on garde les points d'origine
      ! on ajoute des points pour qu'ils soient equirepartis
      allocate(point3D::xyz(1))
      xyz(1)%tag='   '
      do k=1,npoints-np_in ! boucle sur le nombre de points a rajouter
        disti=0.0
        i=1
        do j=1, self%np-1 ! self%np augmente a chaque boucle sur k
          if(lplan) then
            distj = distanceH(self%xyz(j),self%xyz(j+1))
          else
            distj = distance3D(self%xyz(j),self%xyz(j+1))
          endif
          if (distj .gt. disti) then
            disti = distj
            i=j
          endif
        enddo
        xyz(1)%x=0.5*self%xyz(i)%x+0.5*self%xyz(i+1)%x
        xyz(1)%y=0.5*self%xyz(i)%y+0.5*self%xyz(i+1)%y
        xyz(1)%z=0.5*self%xyz(i)%z+0.5*self%xyz(i+1)%z
        call self%insert(xyz(1),i+1)
      enddo
      deallocate(xyz)
    else
      allocate(segment_length(self%np-1))
      allocate(point3D::xyz(npoints))

      profil_length = 0.0
      do j=1,self%np-1
        if(lplan) then
          segment_length(j) = distanceH(self%xyz(j),self%xyz(j+1))
        else
          segment_length(j) = distance3D(self%xyz(j),self%xyz(j+1))
        endif
        profil_length = profil_length + segment_length(j)
      enddo

      pas = profil_length / (npoints - 1)

      xyz(1)%x = self%xyz(1)%x ! on garde le premier point
      xyz(1)%y = self%xyz(1)%y
      xyz(1)%z = self%xyz(1)%z
      xyz(1)%tag = self%xyz(1)%tag
      j = 1 ! indice segment d'origine
      distj = 0.0  ! somme longueur segments d'origine
      do i=2,npoints-1 ! nouveaux points
        disti = pas*(i-1)
        do while (disti > distj + segment_length(j))
          distj = distj + segment_length(j)
          j = j+1
        enddo
        ! nouveau point i est entre ancien point j et j+1
        ! distance depuis j : d = disti - distj
        ! ratio :
        d = (disti - distj) / segment_length(j)
        xyz(i)%x = self%xyz(j)%x + d*(self%xyz(j+1)%x - self%xyz(j)%x)
        xyz(i)%y = self%xyz(j)%y + d*(self%xyz(j+1)%y - self%xyz(j)%y)
        xyz(i)%z = self%xyz(j)%z + d*(self%xyz(j+1)%z - self%xyz(j)%z)
      enddo
      xyz(npoints)%x = self%xyz(self%np)%x ! on garde le dernier point
      xyz(npoints)%y = self%xyz(self%np)%y
      xyz(npoints)%z = self%xyz(self%np)%z
      xyz(npoints)%tag = self%xyz(self%np)%tag
      do i=1,self%np
         call self%remove_point(1)
      enddo
      do i=1,npoints
         call self%insert(xyz(i), i)
      enddo
      deallocate(xyz)
    endif ! fin du if sur keep

  end subroutine interpolate_points_npoints


  subroutine correct(self,xyz1, xyz2)
  ! rotation, translation et homothetie sur une section
    ! prototype
    class(profil_brut), intent(inout) :: self
    type(point3D), intent(in) :: xyz1, xyz2
    ! variables locales
    real(kind=long) :: li, lf, rh, dx, dy, dx2, dy2, cosangle, sinangle
    integer :: i

    ! calcul rapport homothetie
    ! longueur initiale
    li = distanceH(self%xyz(1), self%xyz(self%np))
    lf = distanceH(xyz1, xyz2)
    if(li.gt.0.0001)then
      rh = lf/li
      ! on decale x et y pour que extremite gauche coincide
      dx = self%xyz(1)%x - xyz1%x
      dy = self%xyz(1)%y - xyz1%y
      ! on fait l'opération sur toute la partie droite de la section
      do i=1, self%np
        self%xyz(i)%x = self%xyz(i)%x - dx
        self%xyz(i)%y = self%xyz(i)%y - dy
      enddo
      ! on calcule rotation pour que extremite droite coincide
      dx = self%xyz(self%np)%x - self%xyz(1)%x
      dy = self%xyz(self%np)%y - self%xyz(1)%y
      dx2 = xyz2%x - xyz1%x
      dy2 = xyz2%y - xyz1%y
      sinangle=(-dx2*dy+dy2*dx)/(rh*(dx**2+dy**2))
      cosangle=(dx2*dx+dy2*dy)/(rh*(dx**2+dy**2))
      ! on applique rotation et homothetie
      do i=2, self%np
        dx = self%xyz(i)%x - self%xyz(1)%x
        dy = self%xyz(i)%y - self%xyz(1)%y
        dx2= rh * (dx*cosangle - dy*sinangle)
        dy2= rh * (dx*sinangle + dy*cosangle)
        self%xyz(i)%x = self%xyz(1)%x + dx2
        self%xyz(i)%y = self%xyz(1)%y + dy2
      enddo
    else
        write(*,*)'extremites confondues'
        stop
    endif
  end subroutine correct

  subroutine purge_points_alignes(self)
  ! supression des points doubles et alignés sur une section
    ! prototype
    class(profil_brut), intent(inout) :: self
    ! variables locales
    integer :: i, new_np
    class(point3D), dimension(:), allocatable :: xyz
    logical :: align
    real(kind=long) :: d

    align = .true.
    do while (align)
      align = .false.
      do i=2, self%np-1
        d = self%xyz(i)%dist_from_seg(self%xyz(i-1), self%xyz(i+1))
        if (d .lt. 0.00001 .and. trim(self%xyz(i)%tag) == "")then
          align = .true.
          call self%remove_point(i)
          exit
        endif
      enddo
    enddo

  end subroutine purge_points_alignes

end module objet_section
