##############################################################################
#                                                                            #
#                           PROGRAMME MAGE                                   #
#                                                                            #
# Auteur : Jean-Baptiste FAURE - INRAE - 1989-2020                           #
# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
# Licence : lGPL                                                             #
#                                                                            #
#                                                                            #
# This file is part of Mage, a fortran solver for 1D shallow water equations #
# in compex networks.                                                        #
#                                                                            #
# Copyright (C) 2023 INRAE                                                   #
#                                                                            #
# Mage is free software; you can redistribute it and/or                      #
# modify it under the terms of the GNU Lesser General Public                 #
# License as published by the Free Software Foundation; either               #
# version 3 of the License, or (at your option) any later version.           #
#                                                                            #
# Alternatively, you can redistribute it and/or                              #
# modify it under the terms of the GNU General Public License as             #
# published by the Free Software Foundation; either version 2 of             #
# the License, or (at your option) any later version.                        #
#                                                                            #
# Mage is distributed in the hope that it will be useful, but                #
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
# License or the GNU General Public License for more details.                #
#                                                                            #
# You should have received a copy of the GNU Lesser General Public           #
# License and a copy of the GNU General Public License along with            #
# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
##############################################################################
#                                                                            #
#               Makefile running automated tests                             #
#                                                                            #
##############################################################################

# Data Sets and names of test-cases
data1		=$(DATA)/Saar
etude1  	=Saar
data2 	=$(DATA)/Marche_Ressaut
etude2 	=marche
data3 	=$(DATA)/Marche_AR
etude3 	=marche
data4		=$(DATA)/HGtest1
etude4	=HGmaster
data5 	=$(DATA)/Torrentiel
etude5	=test2
data6 	=$(DATA)/Casier
etude6	=Casier
data7 	=$(DATA)/Demo_AM0
etude7	=DemoAM
data8 	=$(DATA)/Clairmarais
etude8	=clairmao
data9 	=$(DATA)/FleuveRouge
etude9	=bhh
data10 	=$(DATA)/Ardeche
etude10	=ardeche1
data11 	=$(DATA)/Ardeche5
etude11	=ardeche5
data12	=$(DATA)/ISM_DemoAM0/ktest2
etude12	=DemoAM
data13	=$(DATA)/ISM_1bief/ism1bief_4
etude13	=ism1bief
data14	=$(DATA)/ISM_manips/CMW
etude14	=canal3m
data15	=$(DATA)/ISM_manips/CWM
etude15	=canal3m
data16	=$(DATA)/ISM_manips/UCM
etude16	=canal3m
data17	=$(DATA)/ISM_manips/UCW
etude17	=canal3m
data18	=$(DATA)/drag_force
etude18	=canal3m_MW
data19	=$(DATA)/Regulation
etude19	=Test12d
data20	=$(DATA)/Regulation_Rhone-Isere
etude20	=modele1
data21	=$(DATA)/Charriage_1
etude21	=litmaj

ifeq ($(findstring win,$(config)), win)
#version MS-Windows
define need_recode
	recode -v ISO-8859-15..UTF-8
endef

else
# Version Linux
define need_recode
	@:
endef

endif

run1: build
	@echo cd $(data1) > $(TMP)/run_$(PROG_NAME)
	@echo $(VG) $(EXECUTABLE) -r -LC=0 -eps=9 -fp=1 $(etude1).REP >> $(TMP)/run_$(PROG_NAME)
	@chmod u+x $(TMP)/run_$(PROG_NAME)
	$(TMP)/run_$(PROG_NAME) 2>&1|tee -a $(PROG_NAME).log

run1b: build
	@echo cd $(data1) > $(TMP)/run_$(PROG_NAME)
	@echo $(VG) $(EXECUTABLE) -r -eps=9 -fp=1 $(etude1).REP >> $(TMP)/run_$(PROG_NAME)
	$(TMP)/run_$(PROG_NAME) 2>&1|tee -a $(PROG_NAME).log

run2: build
	@echo cd $(data2) > $(TMP)/run_$(PROG_NAME)
	@echo $(VG) $(EXECUTABLE) -r -eps=9 $(etude2).REP >> $(TMP)/run_$(PROG_NAME)
	$(TMP)/run_$(PROG_NAME) 2>&1|tee -a $(PROG_NAME).log

run3: build
	@echo cd $(data3) > $(TMP)/run_$(PROG_NAME)
	@echo $(VG) $(EXECUTABLE) -r -eps=9 $(etude3).REP >> $(TMP)/run_$(PROG_NAME)
	$(TMP)/run_$(PROG_NAME) 2>&1|tee -a $(PROG_NAME).log

run4: build
	@echo cd $(data4) > $(TMP)/run_$(PROG_NAME)
	@echo $(VG) $(EXECUTABLE) -r -LC=0 -eps=6 $(etude4).REP >> $(TMP)/run_$(PROG_NAME)
	$(TMP)/run_$(PROG_NAME) 2>&1|tee -a $(PROG_NAME).log

run4b: build
	@echo cd $(data4) > $(TMP)/run_$(PROG_NAME)
	@echo $(VG) $(EXECUTABLE) -r -eps=6 $(etude4).REP >> $(TMP)/run_$(PROG_NAME)
	$(TMP)/run_$(PROG_NAME) 2>&1|tee -a $(PROG_NAME).log

run5: build
	@echo cd $(data5) > $(TMP)/run_$(PROG_NAME)
	@echo $(VG) $(EXECUTABLE) -r -LC=0 -eps=9 $(etude5).REP >> $(TMP)/run_$(PROG_NAME)
	$(TMP)/run_$(PROG_NAME) 2>&1|tee -a $(PROG_NAME).log

run5b: build
	@echo cd $(data5) > $(TMP)/run_$(PROG_NAME)
	@echo $(VG) $(EXECUTABLE) -r -eps=5 $(etude5).REP >> $(TMP)/run_$(PROG_NAME)
	$(TMP)/run_$(PROG_NAME) 2>&1|tee -a $(PROG_NAME).log

run6: build
	@echo cd $(data6) > $(TMP)/run_$(PROG_NAME)
	@echo $(VG) $(EXECUTABLE) -r -LC=0 -eps=9 $(etude6).REP >> $(TMP)/run_$(PROG_NAME)
	$(TMP)/run_$(PROG_NAME) 2>&1|tee -a $(PROG_NAME).log

run6b: build
	@echo cd $(data6) > $(TMP)/run_$(PROG_NAME)
	@echo $(VG) $(EXECUTABLE) -r -eps=9 $(etude6).REP >> $(TMP)/run_$(PROG_NAME)
	$(TMP)/run_$(PROG_NAME) 2>&1|tee -a $(PROG_NAME).log

run7: build
	@echo cd $(data7) > $(TMP)/run_$(PROG_NAME)
	@echo $(VG) $(EXECUTABLE) -r -LC=0 -eps=9 $(etude7).REP >> $(TMP)/run_$(PROG_NAME)
	$(TMP)/run_$(PROG_NAME) 2>&1|tee -a $(PROG_NAME).log

run7b: build
	@echo cd $(data7) > $(TMP)/run_$(PROG_NAME)
	@echo $(VG) $(EXECUTABLE) -r -eps=6 $(etude7).REP >> $(TMP)/run_$(PROG_NAME)
	$(TMP)/run_$(PROG_NAME) 2>&1|tee -a $(PROG_NAME).log

run8: build
	@echo cd $(data8) > $(TMP)/run_$(PROG_NAME)
	@echo $(VG) $(EXECUTABLE) -r -LC=0 -eps=9 $(etude8).REP >> $(TMP)/run_$(PROG_NAME)
	$(TMP)/run_$(PROG_NAME) 2>&1|tee -a $(PROG_NAME).log

run8b: build
	@echo cd $(data8) > $(TMP)/run_$(PROG_NAME)
	@echo $(VG) $(EXECUTABLE) -r -eps=9 $(etude8).REP >> $(TMP)/run_$(PROG_NAME)
	$(TMP)/run_$(PROG_NAME) 2>&1|tee -a $(PROG_NAME).log

run9: build
	@echo cd $(data9) > $(TMP)/run_$(PROG_NAME)
	@echo $(VG) $(EXECUTABLE) -r -LC=0 -eps=9 $(etude9).REP >> $(TMP)/run_$(PROG_NAME)
	$(TMP)/run_$(PROG_NAME) 2>&1|tee -a $(PROG_NAME).log

run10: build
	@echo cd $(data10) > $(TMP)/run_$(PROG_NAME)
	@echo $(VG) $(EXECUTABLE) -r -LC=0 -eps=5 -fp=1 $(etude10).REP >> $(TMP)/run_$(PROG_NAME)
	$(TMP)/run_$(PROG_NAME) 2>&1|tee -a $(PROG_NAME).log

run10b: build
	@echo cd $(data10) > $(TMP)/run_$(PROG_NAME)
	@echo $(VG) $(EXECUTABLE) -r -eps=5 -fp=1 $(etude10).REP >> $(TMP)/run_$(PROG_NAME)
	$(TMP)/run_$(PROG_NAME) 2>&1|tee -a $(PROG_NAME).log

run11: build
	@echo cd $(data11) > $(TMP)/run_$(PROG_NAME)
	@echo $(VG) $(EXECUTABLE) -r -LC=0 -eps=5 -fp=1 $(etude11).REP >> $(TMP)/run_$(PROG_NAME)
	$(TMP)/run_$(PROG_NAME) 2>&1|tee -a $(PROG_NAME).log

run11b: build
	@echo cd $(data11) > $(TMP)/run_$(PROG_NAME)
	@echo $(VG) $(EXECUTABLE) -r -eps=7 -fp=1 $(etude11).REP >> $(TMP)/run_$(PROG_NAME)
	$(TMP)/run_$(PROG_NAME) 2>&1|tee -a $(PROG_NAME).log

run12: build
	@echo cd $(data12) > $(TMP)/run_$(PROG_NAME)
	@echo $(VG) $(EXECUTABLE) -r -LC=0 -i=-2 -w=0.3 $(etude12).REP >> $(TMP)/run_$(PROG_NAME)
	$(TMP)/run_$(PROG_NAME) 2>&1|tee -a $(PROG_NAME).log

run12b: build
	@echo cd $(data12) > $(TMP)/run_$(PROG_NAME)
	@echo $(VG) $(EXECUTABLE) -r -i=-2 -w=0.3 $(etude12).REP >> $(TMP)/run_$(PROG_NAME)
	$(TMP)/run_$(PROG_NAME) 2>&1|tee -a $(PROG_NAME).log

run13: build
	@echo cd $(data13) > $(TMP)/run_$(PROG_NAME)
	@echo $(VG) $(EXECUTABLE) -r -i=1 -w=0.3 $(etude13).REP >> $(TMP)/run_$(PROG_NAME)
	$(TMP)/run_$(PROG_NAME) 2>&1|tee -a $(PROG_NAME).log

run14: build
	@echo cd $(data14) > $(TMP)/run_$(PROG_NAME)
	@echo $(VG) $(EXECUTABLE) -r -i=1 -w=0.3 $(etude14).REP >> $(TMP)/run_$(PROG_NAME)
	$(TMP)/run_$(PROG_NAME) 2>&1|tee -a $(PROG_NAME).log

run15: build
	@echo cd $(data15) > $(TMP)/run_$(PROG_NAME)
	@echo $(VG) $(EXECUTABLE) -r -i=1 -w=0.3 $(etude15).REP >> $(TMP)/run_$(PROG_NAME)
	$(TMP)/run_$(PROG_NAME) 2>&1|tee -a $(PROG_NAME).log

run16: build
	@echo cd $(data16) > $(TMP)/run_$(PROG_NAME)
	@echo $(VG) $(EXECUTABLE) -r -i=1 -w=0.3 $(etude16).REP >> $(TMP)/run_$(PROG_NAME)
	$(TMP)/run_$(PROG_NAME) 2>&1|tee -a $(PROG_NAME).log

run17: build
	@echo cd $(data17) > $(TMP)/run_$(PROG_NAME)
	@echo $(VG) $(EXECUTABLE) -r -i=1 -w=0.3 $(etude17).REP >> $(TMP)/run_$(PROG_NAME)
	$(TMP)/run_$(PROG_NAME) 2>&1|tee -a $(PROG_NAME).log

run18: build
	@echo cd $(data18) > $(TMP)/run_$(PROG_NAME)
	@echo $(VG) $(EXECUTABLE) -r -i=1 -w=0.3 $(etude18).REP >> $(TMP)/run_$(PROG_NAME)
	$(TMP)/run_$(PROG_NAME) 2>&1|tee -a $(PROG_NAME).log

run19: build
	@echo cd $(data19) > $(TMP)/run_$(PROG_NAME)
	@echo $(VG) $(EXECUTABLE) -r -i=0 $(etude19).REP >> $(TMP)/run_$(PROG_NAME)
	$(TMP)/run_$(PROG_NAME) 2>&1|tee -a $(PROG_NAME).log

run20: build
	@echo cd $(data20) > $(TMP)/run_$(PROG_NAME)
	@echo $(VG) $(EXECUTABLE) -r -i=0 -LC=0 -eps=7 -fp=2 $(etude20)_LC.REP >> $(TMP)/run_$(PROG_NAME)
	$(TMP)/run_$(PROG_NAME) 2>&1|tee -a $(PROG_NAME).log

run20b: build
	@echo cd $(data20) > $(TMP)/run_$(PROG_NAME)
	@echo $(VG) $(EXECUTABLE) -r -i=0 -fp=2 $(etude20).REP >> $(TMP)/run_$(PROG_NAME)
	$(TMP)/run_$(PROG_NAME) 2>&1|tee -a $(PROG_NAME).log

run21: build
	@echo cd $(data21) > $(TMP)/run_$(PROG_NAME)
	@echo $(VG) $(EXECUTABLE) -c=2 $(etude21).REP >> $(TMP)/run_$(PROG_NAME)
	$(TMP)/run_$(PROG_NAME) 2>&1|tee -a $(PROG_NAME).log

num	=1
run: run$(num)

#---------------------------------------------------------------------#
# test
#---------------------------------------------------------------------#
diff = ./my_diff
difopt = '-b -d -E -t --unified=0'
grepopt = 'Version\|Ligne\|Copie\|correctif\|compilation\|Nombre total\|Date simulation\|DE MAGE ****\|---\|+++\|ATTENTION\|: Erreur rel. maxi. =\|: Volume total ='
compare_BIN = ./mage_Compare_BIN

test0:
	-@rm test.diff
	-@rm $(PROG_NAME).log
	-@chmod u+x my_diff
	@if [ ! -d $(TMP) ] ; then mkdir $(TMP) ; fi
	@echo "*" >test.diff

update_REF0:
	@echo
	@echo "## Mise à jour des fichiers de résultats de référence ##"
	@echo

test1: run1
	@echo "*" >>test.diff
	@echo "********************" >>test.diff
	@echo "Test numéro 1 : Saar" >>test.diff
	@echo "********************" >>test.diff
	@echo "Diff filtré entre $(data1)/Saar2_REF2.TRA et $(data1)/Saar2.TRA" >>test.diff
	$(need_recode) $(data1)/Saar2.TRA
	$(diff) $(difopt) $(data1)/Saar2_REF2.TRA $(data1)/Saar2.TRA | grep $(grepopt) >>test.diff
	-$(compare_BIN) $(data1)/Saar2_REF2.BIN $(data1)/Saar2.BIN >>test.diff
#	$(need_recode) $(data1)/Mage_fin.ini
#	$(diff) $(difopt) $(data1)/Mage_fin_REF2.ini $(data1)/Mage_fin.ini >>test.diff

update_REF1: run1
	@echo
	@echo "## Mise à jour des fichiers de résultats de référence : Saar ##"
	cp $(data1)/Saar2.TRA $(data1)/Saar2_REF2.TRA
	cp $(data1)/Saar2.BIN $(data1)/Saar2_REF2.BIN

test2: run2
	@echo "*" >>test.diff
	@echo "************************************" >>test.diff
	@echo "Test numéro 2 : Ressaut stationnaire" >>test.diff
	@echo "************************************" >>test.diff
	@echo "Diff filtré entre $(data2)/marche_REF.TRA et $(data2)/marche.TRA" >>test.diff
	$(need_recode) $(data2)/marche.TRA
	$(diff) $(difopt) $(data2)/marche_REF.TRA $(data2)/marche.TRA | grep $(grepopt) >>test.diff
	-$(compare_BIN) $(data2)/marche_REF.BIN $(data2)/marche.BIN >>test.diff
#	$(need_recode) $(data2)/Mage_fin.ini
#	$(diff) $(difopt) $(data2)/Mage_fin_REF.ini $(data2)/Mage_fin.ini >>test.diff

update_REF2: run2
	@echo
	@echo "## Mise à jour des fichiers de résultats de référence : Ressaut stationnaire ##"
	cp $(data2)/marche.TRA $(data2)/marche_REF.TRA
	cp $(data2)/marche.BIN $(data2)/marche_REF.BIN

test3: run3
	@echo "*" >>test.diff
	@echo "********************************************" >>test.diff
	@echo "Test numéro 3 : Marche / retour au permanent" >>test.diff
	@echo "********************************************" >>test.diff
	@echo "Diff filtré entre $(data3)/marche_REF.TRA et $(data3)/marche.TRA" >>test.diff
	$(need_recode) $(data3)/marche.TRA
	$(diff) $(difopt) $(data3)/marche_REF.TRA $(data3)/marche.TRA | grep $(grepopt) >>test.diff
	-$(compare_BIN) $(data3)/marche_REF.BIN $(data3)/marche.BIN >>test.diff
#	$(need_recode) $(data3)/Mage_fin.ini
#	$(diff) $(difopt) $(data3)/Mage_fin_REF.ini $(data3)/Mage_fin.ini >>test.diff

update_REF3: run3
	@echo
	@echo "## Mise à jour des fichiers de résultats de référence : Marche / retour au permanent ##"
	cp $(data3)/marche.TRA $(data3)/marche_REF.TRA
	cp $(data3)/marche.BIN $(data3)/marche_REF.BIN

test4: run4
	@echo "*" >>test.diff
	@echo "***********************" >>test.diff
	@echo "Test numéro 4 : Hogneau" >>test.diff
	@echo "***********************" >>test.diff
	@echo "Diff filtré entre $(data4)/HGmaster_REF2.TRA et $(data4)/HGmaster.TRA" >>test.diff
	$(need_recode) $(data4)/HGmaster.TRA
	$(diff) $(difopt) $(data4)/HGmaster_REF2.TRA $(data4)/HGmaster.TRA | grep $(grepopt) >>test.diff
	-$(compare_BIN) $(data4)/HGmaster_REF2.BIN $(data4)/HGmaster.BIN >>test.diff
#	$(need_recode) $(data4)/Mage_fin.ini
#	$(diff) $(difopt) $(data4)/Mage_fin_REF2.ini $(data4)/Mage_fin.ini >>test.diff
	$(need_recode) $(data4)/Mage_fin.sin
	$(diff) $(difopt) $(data4)/Mage_fin_REF2.sin $(data4)/Mage_fin.sin >>test.diff

update_REF4: run4
	@echo
	@echo "## Mise à jour des fichiers de résultats de référence : Hogneau ##"
	cp $(data4)/HGmaster.TRA $(data4)/HGmaster_REF2.TRA
	cp $(data4)/HGmaster.BIN $(data4)/HGmaster_REF2.BIN
	cp $(data4)/Mage_fin.sin $(data4)/Mage_fin_REF2.sin

test5: run5
	@echo "*" >>test.diff
	@echo "**************************" >>test.diff
	@echo "Test numéro 5 : Torrentiel" >>test.diff
	@echo "**************************" >>test.diff
	@echo "Diff filtré entre $(data5)/test2_REF2.TRA et $(data5)/test2.TRA" >>test.diff
	$(need_recode) $(data5)/test2.TRA
	$(diff) $(difopt) $(data5)/test2_REF2.TRA $(data5)/test2.TRA | grep $(grepopt) >>test.diff
	-$(compare_BIN) $(data5)/test2_REF2.BIN $(data5)/test2.BIN >>test.diff
#	$(need_recode) $(data5)/Mage_fin.ini
#	$(diff) $(difopt) $(data5)/Mage_fin_REF2.ini $(data5)/Mage_fin.ini >>test.diff

update_REF5: run5
	@echo
	@echo "## Mise à jour des fichiers de résultats de référence : Torrentiel ##"
	cp $(data5)/test2.TRA $(data5)/test2_REF2.TRA
	cp $(data5)/test2.BIN $(data5)/test2_REF2.BIN

test6: run6
	@echo "*" >>test.diff
	@echo "**************************" >>test.diff
	@echo "Test numéro 6 : Casier    " >>test.diff
	@echo "**************************" >>test.diff
	@echo "Diff filtré entre $(data6)/Casier_REF2.TRA et $(data6)/Casier.TRA" >>test.diff
	$(need_recode) $(data6)/Casier.TRA
	$(diff) $(difopt) $(data6)/Casier_REF2.TRA $(data6)/Casier.TRA | grep $(grepopt) >>test.diff
	-$(compare_BIN) $(data6)/Casier_REF2.BIN $(data6)/Casier.BIN >>test.diff
#	$(need_recode) $(data6)/Mage_fin.ini
#	$(diff) $(difopt) $(data6)/Mage_fin_REF2.ini $(data6)/Mage_fin.ini >>test.diff

update_REF6: run6
	@echo
	@echo "## Mise à jour des fichiers de résultats de référence : Casier ##"
	cp $(data6)/Casier.TRA $(data6)/Casier_REF2.TRA
	cp $(data6)/Casier.BIN $(data6)/Casier_REF2.BIN

test7: run7
	@echo "*" >>test.diff
	@echo "**************************" >>test.diff
	@echo "Test numéro 7 : Demo_AM0  " >>test.diff
	@echo "**************************" >>test.diff
	@echo "Diff filtré entre $(data7)/DemoAM_REF2.TRA et $(data7)/DemoAM.TRA" >>test.diff
	$(need_recode) $(data7)/DemoAM.TRA
	$(diff) $(difopt) $(data7)/DemoAM_REF2.TRA $(data7)/DemoAM.TRA | grep $(grepopt) >>test.diff
	-$(compare_BIN) $(data7)/DemoAM_REF2.BIN $(data7)/DemoAM.BIN >>test.diff
#	$(need_recode) $(data7)/Mage_fin.ini
#	$(diff) $(difopt) $(data7)/Mage_fin_REF2.ini $(data7)/Mage_fin.ini >>test.diff

update_REF7: run7
	@echo
	@echo "## Mise à jour des fichiers de résultats de référence : Demo_AM0 ##"
	cp $(data7)/DemoAM.TRA $(data7)/DemoAM_REF2.TRA
	cp $(data7)/DemoAM.BIN $(data7)/DemoAM_REF2.BIN

test8: run8
	@echo "*" >>test.diff
	@echo "*****************************" >>test.diff
	@echo "Test numéro 8 : Clairmarais  " >>test.diff
	@echo "*****************************" >>test.diff
	@echo "Diff filtré entre $(data8)/clairmao_REF3.TRA et $(data8)/clairmao.TRA" >>test.diff
	$(need_recode) $(data8)/clairmao.TRA
	$(diff) $(difopt) $(data8)/clairmao_REF3.TRA $(data8)/clairmao.TRA | grep $(grepopt) >>test.diff
	-$(compare_BIN)  $(data8)/clairmao_REF3.BIN $(data8)/clairmao.BIN >>test.diff
#	$(need_recode) $(data8)/Mage_fin.ini
#	$(diff) $(difopt) $(data8)/Mage_fin_REF3.ini $(data8)/Mage_fin.ini >>test.diff

update_REF8: run8
	@echo
	@echo "## Mise à jour des fichiers de résultats de référence : Clairmarais ##"
	cp $(data8)/clairmao.TRA $(data8)/clairmao_REF3.TRA
	cp $(data8)/clairmao.BIN $(data8)/clairmao_REF3.BIN

test_9: run_9
	@echo "*" >>test.diff
	@echo "*****************************" >>test.diff
	@echo "Test numéro 9 : Fleuve-Rouge " >>test.diff
	@echo "*****************************" >>test.diff
	@echo "Diff filtré entre $(data9)/bhh_03.TRA et $(data9)/bhh.TRA" >>test.diff
	$(need_recode) $(data9)/bhh.TRA
	$(diff) $(difopt) $(data9)/bhh_03.TRA $(data9)/bhh.TRA | grep $(grepopt) >>test.diff

update_REF_9: run_9
	@echo
	@echo "## Mise à jour des fichiers de résultats de référence : Fleuve-Rouge ##"
	cp $(data9)/bhh.TRA $(data9)/bhh_03.TRA
	cp $(data9)/bhh.BIN $(data9)/bhh_03.BIN

test10: run10
	@echo "*" >>test.diff
	@echo "*******************************" >>test.diff
	@echo "Test numéro 10 : Ardèche amont " >>test.diff
	@echo "*******************************" >>test.diff
	@echo "Diff filtré entre $(data10)/ardeche1_REF2.TRA et $(data10)/ardeche1.TRA" >>test.diff
	$(need_recode) $(data10)/ardeche1.TRA
	$(diff) $(difopt) $(data10)/ardeche1_REF2.TRA $(data10)/ardeche1.TRA | grep $(grepopt) >>test.diff
	-$(compare_BIN) $(data10)/ardeche1_REF2.BIN $(data10)/ardeche1.BIN >>test.diff
#	$(need_recode) $(data10)/Mage_fin.ini
#	$(diff) $(difopt) $(data10)/Mage_fin_REF2.ini $(data10)/Mage_fin.ini >>test.diff

update_REF10: run10
	@echo
	@echo "## Mise à jour des fichiers de résultats de référence : Ardèche amont ##"
	cp $(data10)/ardeche1.TRA $(data10)/ardeche1_REF2.TRA
	cp $(data10)/ardeche1.BIN $(data10)/ardeche1_REF2.BIN

test_11: run_11
	@echo "*" >>test.diff
	@echo "*********************************" >>test.diff
	@echo "Test numéro 11 : Ardèche complet " >>test.diff
	@echo "*********************************" >>test.diff
	@echo "Diff filtré entre $(data11)/ardeche5_REF4.TRA et $(data11)/ardeche5.TRA" >>test.diff
	$(need_recode) $(data11)/ardeche5.TRA
	$(diff) $(difopt) $(data11)/ardeche5_REF4.TRA $(data11)/ardeche5.TRA | grep $(grepopt) >>test.diff
	-$(compare_BIN) $(data11)/ardeche5_REF4.BIN $(data11)/ardeche5.BIN >>test.diff
#	$(need_recode) $(data11)/Mage_fin.ini
#	$(diff) $(difopt) $(data11)/Mage_fin_REF4.ini $(data11)/Mage_fin.ini >>test.diff

update_REF_11: run_11
	@echo
	@echo "## Mise à jour des fichiers de résultats de référence : Ardèche complet ##"
	cp $(data11)/ardeche5.TRA $(data11)/ardeche5_REF4.TRA
	cp $(data11)/ardeche5.BIN $(data11)/ardeche5_REF4.BIN

test12: run12
	@echo "*" >>test.diff
	@echo "*********************************" >>test.diff
	@echo "Test numéro 12 : ISM_DemoAM0     " >>test.diff
	@echo "*********************************" >>test.diff
	@echo "Diff filtré entre $(data12)/DemoAM_REF6.TRA et $(data12)/DemoAM.TRA" >>test.diff
	$(need_recode) $(data12)/DemoAM.TRA
	$(diff) $(difopt) $(data12)/DemoAM_REF6.TRA $(data12)/DemoAM.TRA | grep $(grepopt) >>test.diff
	-$(compare_BIN) $(data12)/DemoAM_REF6.BIN $(data12)/DemoAM.BIN >>test.diff
#	$(need_recode) $(data12)/Mage_fin.ini
#	$(diff) $(difopt) $(data12)/Mage_fin_REF6.ini $(data12)/Mage_fin.ini >>test.diff

update_REF12: run12
	@echo
	@echo "## Mise à jour des fichiers de résultats de référence : ISM_DemoAM0 ##"
	cp $(data12)/DemoAM.TRA $(data12)/DemoAM_REF6.TRA
	cp $(data12)/DemoAM.BIN $(data12)/DemoAM_REF6.BIN

test13: run13
	@echo "*" >>test.diff
	@echo "*********************************" >>test.diff
	@echo "Test numéro 13 : ISM_1bief 4     " >>test.diff
	@echo "*********************************" >>test.diff
	@echo "Diff filtré entre $(data13)/ism1bief_REF3.TRA et $(data13)/ism1bief.TRA" >>test.diff
	$(need_recode) $(data13)/ism1bief.TRA
	$(diff) $(difopt) $(data13)/ism1bief_REF3.TRA $(data13)/ism1bief.TRA | grep $(grepopt) >>test.diff
	-$(compare_BIN) $(data13)/ism1bief_REF3.BIN $(data13)/ism1bief.BIN >>test.diff
#	$(need_recode) $(data13)/Mage_fin.ini
#	$(diff) $(difopt) $(data13)/Mage_fin_REF3.ini $(data13)/Mage_fin.ini >>test.diff

update_REF13: run13
	@echo
	@echo "## Mise à jour des fichiers de résultats de référence : ISM_1bief 4  ##"
	cp $(data13)/ism1bief.TRA $(data13)/ism1bief_REF3.TRA
	cp $(data13)/ism1bief.BIN $(data13)/ism1bief_REF3.BIN

test14: run14
	@echo "*" >>test.diff
	@echo "*********************************" >>test.diff
	@echo "Test numéro 14 : ISM_manips CMW  " >>test.diff
	@echo "*********************************" >>test.diff
	@echo "Diff filtré entre $(data14)/canal3m_REF2.TRA et $(data14)/canal3m.TRA" >>test.diff
	$(need_recode) $(data14)/canal3m.TRA
	$(diff) $(difopt) $(data14)/canal3m_REF2.TRA $(data14)/canal3m.TRA | grep $(grepopt) >>test.diff
	-$(compare_BIN) $(data14)/canal3m_REF2.BIN $(data14)/canal3m.BIN >>test.diff
#	$(need_recode) $(data14)/Mage_fin.ini
#	$(diff) $(difopt) $(data14)/Mage_fin_REF2.ini $(data14)/Mage_fin.ini >>test.diff

update_REF14: run14
	@echo
	@echo "## Mise à jour des fichiers de résultats de référence : ISM_manips CMW  ##"
	cp $(data14)/canal3m.TRA $(data14)/canal3m_REF2.TRA
	cp $(data14)/canal3m.BIN $(data14)/canal3m_REF2.BIN

test15: run15
	@echo "*" >>test.diff
	@echo "*********************************" >>test.diff
	@echo "Test numéro 15 : ISM_manips CWM  " >>test.diff
	@echo "*********************************" >>test.diff
	@echo "Diff filtré entre $(data15)/canal3m_REF2.TRA et $(data15)/canal3m.TRA" >>test.diff
	$(need_recode) $(data15)/canal3m.TRA
	$(diff) $(difopt) $(data15)/canal3m_REF2.TRA $(data15)/canal3m.TRA | grep $(grepopt) >>test.diff
	-$(compare_BIN) $(data15)/canal3m_REF2.BIN $(data15)/canal3m.BIN >>test.diff
#	$(need_recode) $(data15)/Mage_fin.ini
#	$(diff) $(difopt) $(data15)/Mage_fin_REF2.ini $(data15)/Mage_fin.ini >>test.diff

update_REF15: run15
	@echo
	@echo "## Mise à jour des fichiers de résultats de référence : ISM_manips CWM  ##"
	cp $(data15)/canal3m.TRA $(data15)/canal3m_REF2.TRA
	cp $(data15)/canal3m.BIN $(data15)/canal3m_REF2.BIN

test16: run16
	@echo "*" >>test.diff
	@echo "*********************************" >>test.diff
	@echo "Test numéro 16 : ISM_manips UCM  " >>test.diff
	@echo "*********************************" >>test.diff
	@echo "Diff filtré entre $(data16)/canal3m_REF2.TRA et $(data16)/canal3m.TRA" >>test.diff
	$(need_recode) $(data16)/canal3m.TRA
	$(diff) $(difopt) $(data16)/canal3m_REF2.TRA $(data16)/canal3m.TRA | grep $(grepopt) >>test.diff
	-$(compare_BIN) $(data16)/canal3m_REF2.BIN $(data16)/canal3m.BIN >>test.diff
#	$(need_recode) $(data16)/Mage_fin.ini
#	$(diff) $(difopt) $(data16)/Mage_fin_REF2.ini $(data16)/Mage_fin.ini >>test.diff

update_REF16: run16
	@echo
	@echo "## Mise à jour des fichiers de résultats de référence : ISM_manips UCM  ##"
	cp $(data16)/canal3m.TRA $(data16)/canal3m_REF2.TRA
	cp $(data16)/canal3m.BIN $(data16)/canal3m_REF2.BIN

test17: run17
	@echo "*" >>test.diff
	@echo "*********************************" >>test.diff
	@echo "Test numéro 17 : ISM_manips UCW  " >>test.diff
	@echo "*********************************" >>test.diff
	@echo "Diff filtré entre $(data17)/canal3m_REF2.TRA et $(data17)/canal3m.TRA" >>test.diff
	$(need_recode) $(data17)/canal3m.TRA
	$(diff) $(difopt) $(data17)/canal3m_REF2.TRA $(data17)/canal3m.TRA | grep $(grepopt) >>test.diff
	-$(compare_BIN) $(data17)/canal3m_REF2.BIN $(data17)/canal3m.BIN >>test.diff
#	$(need_recode) $(data17)/Mage_fin.ini
#	$(diff) $(difopt) $(data17)/Mage_fin_REF2.ini $(data17)/Mage_fin.ini >>test.diff

update_REF17: run17
	@echo
	@echo "## Mise à jour des fichiers de résultats de référence : ISM_manips UCW  ##"
	cp $(data17)/canal3m.TRA $(data17)/canal3m_REF2.TRA
	cp $(data17)/canal3m.BIN $(data17)/canal3m_REF2.BIN

test18: run18
	@echo "*" >>test.diff
	@echo "*********************************" >>test.diff
	@echo "Test numéro 18 : drag force      " >>test.diff
	@echo "*********************************" >>test.diff
	@echo "Diff filtré entre $(data18)/canal3m_MW_REF.TRA et $(data18)/canal3m_MW.TRA" >>test.diff
	$(need_recode) $(data18)/canal3m_MW.TRA
	$(diff) $(difopt) $(data18)/canal3m_MW_REF.TRA $(data18)/canal3m_MW.TRA | grep $(grepopt) >>test.diff
	-$(compare_BIN) $(data18)/canal3m_MW_REF.BIN $(data18)/canal3m_MW.BIN >>test.diff
#	$(need_recode) $(data18)/Mage_fin.ini
#	$(diff) $(difopt) $(data18)/Mage_fin_MW_REF.ini $(data18)/Mage_fin.ini >>test.diff

update_REF18: run18
	@echo
	@echo "## Mise à jour des fichiers de résultats de référence : drag force  ##"
	cp $(data18)/canal3m_MW.TRA $(data18)/canal3m_MW_REF.TRA
	cp $(data18)/canal3m_MW.BIN $(data18)/canal3m_MW_REF.BIN

test19: run19
	@echo "*" >>test.diff
	@echo "*********************************" >>test.diff
	@echo "Test numéro 19 : Régulation      " >>test.diff
	@echo "*********************************" >>test.diff
	@echo "Diff filtré entre $(data19)/Test12_REF3.TRA et $(data19)/Test12.TRA" >>test.diff
	$(need_recode) $(data19)/Test12.TRA
	$(diff) $(difopt) $(data19)/Test12_REF3.TRA $(data19)/Test12.TRA | grep $(grepopt) >>test.diff
	-$(compare_BIN) $(data19)/Test12_REF3.BIN $(data19)/Test12.BIN >>test.diff
#	$(need_recode) $(data19)/Mage_fin.ini
#	$(diff) $(difopt) $(data19)/Mage_fin_REF2.ini $(data19)/Mage_fin.ini >>test.diff
	$(need_recode) $(data19)/Mage_fin.sin
	$(diff) $(difopt) $(data19)/Mage_fin_REF3.sin $(data19)/Mage_fin.sin >>test.diff

update_REF19: run19
	@echo
	@echo "## Mise à jour des fichiers de résultats de référence : Régulation  ##"
	cp $(data19)/Test12.TRA $(data19)/Test12_REF3.TRA
	cp $(data19)/Test12.BIN $(data19)/Test12_REF3.BIN

test20: run1b
	@echo "*" >>test.diff
	@echo "****************************" >>test.diff
	@echo "Test numéro 20 : Saar en XYZ" >>test.diff
	@echo "****************************" >>test.diff
	@echo "Diff filtré entre $(data1)/Saar2_XYZ2.TRA et $(data1)/Saar2.TRA" >>test.diff
	$(need_recode) $(data1)/Saar2.TRA
	$(diff) $(difopt) $(data1)/Saar2_XYZ2.TRA $(data1)/Saar2.TRA | grep $(grepopt) >>test.diff
	-$(compare_BIN) $(data1)/Saar2_XYZ2.BIN $(data1)/Saar2.BIN >>test.diff
#	$(need_recode) $(data1)/Mage_fin.ini
#	$(diff) $(difopt) $(data1)/Mage_fin_XYZ.ini $(data1)/Mage_fin.ini >>test.diff

update_REF20: run1b
	@echo
	@echo "## Mise à jour des fichiers de résultats de référence : Saar en XYZ  ##"
	cp $(data1)/Saar2.TRA $(data1)/Saar2_XYZ2.TRA
	cp $(data1)/Saar2.BIN $(data1)/Saar2_XYZ2.BIN

test21: run4b
	@echo "*" >>test.diff
	@echo "*******************************" >>test.diff
	@echo "Test numéro 21 : Hogneau en XYZ" >>test.diff
	@echo "*******************************" >>test.diff
	@echo "Diff filtré entre $(data4)/HGmaster_XYZ3.TRA et $(data4)/HGmaster.TRA" >>test.diff
	$(need_recode) $(data4)/HGmaster.TRA
	$(diff) $(difopt) $(data4)/HGmaster_XYZ3.TRA $(data4)/HGmaster.TRA | grep $(grepopt) >>test.diff
	-$(compare_BIN) $(data4)/HGmaster_XYZ3.BIN $(data4)/HGmaster.BIN >>test.diff
#	$(need_recode) $(data4)/Mage_fin.ini
#	$(diff) $(difopt) $(data4)/Mage_fin_XYZ3.ini $(data4)/Mage_fin.ini >>test.diff
	$(need_recode) $(data4)/Mage_fin.sin
	$(diff) $(difopt) $(data4)/Mage_fin_XYZ3.sin $(data4)/Mage_fin.sin >>test.diff

update_REF21: run4b
	@echo
	@echo "## Mise à jour des fichiers de résultats de référence : Hogneau en XYZ  ##"
	cp $(data4)/HGmaster.TRA $(data4)/HGmaster_XYZ3.TRA
	cp $(data4)/HGmaster.BIN $(data4)/HGmaster_XYZ3.BIN
	cp $(data4)/Mage_fin.sin $(data4)/Mage_fin_XYZ3.sin

test22: run5b
	@echo "*" >>test.diff
	@echo "**********************************" >>test.diff
	@echo "Test numéro 22 : Torrentiel en XYZ" >>test.diff
	@echo "**********************************" >>test.diff
	@echo "Diff filtré entre $(data5)/test2_XYZ3.TRA et $(data5)/test2.TRA" >>test.diff
	$(need_recode) $(data5)/test2.TRA
	$(diff) $(difopt) $(data5)/test2_XYZ3.TRA $(data5)/test2.TRA | grep $(grepopt) >>test.diff
	-$(compare_BIN) $(data5)/test2_XYZ3.BIN $(data5)/test2.BIN >>test.diff
#	$(need_recode) $(data5)/Mage_fin.ini
#	$(diff) $(difopt) $(data5)/Mage_fin_XYZ3.ini $(data5)/Mage_fin.ini >>test.diff

update_REF22: run5b
	@echo
	@echo "## Mise à jour des fichiers de résultats de référence : Torrentiel en XYZ  ##"
	cp $(data5)/test2.TRA $(data5)/test2_XYZ3.TRA
	cp $(data5)/test2.BIN $(data5)/test2_XYZ3.BIN

test23: run6b
	@echo "*" >>test.diff
	@echo "*******************************" >>test.diff
	@echo "Test numéro 23 : Casier en XYZ " >>test.diff
	@echo "*******************************" >>test.diff
	@echo "Diff filtré entre $(data6)/Casier_XYZ3.TRA et $(data6)/Casier.TRA" >>test.diff
	$(need_recode) $(data6)/Casier.TRA
	$(diff) $(difopt) $(data6)/Casier_XYZ3.TRA $(data6)/Casier.TRA | grep $(grepopt) >>test.diff
	-$(compare_BIN) $(data6)/Casier_XYZ3.BIN $(data6)/Casier.BIN >>test.diff
#	$(need_recode) $(data6)/Mage_fin.ini
#	$(diff) $(difopt) $(data6)/Mage_fin_XYZ3.ini $(data6)/Mage_fin.ini >>test.diff

update_REF23: run6b
	@echo
	@echo "## Mise à jour des fichiers de résultats de référence : Casier en XYZ  ##"
	cp $(data6)/Casier.TRA $(data6)/Casier_XYZ3.TRA
	cp $(data6)/Casier.BIN $(data6)/Casier_XYZ3.BIN

test24: run7b
	@echo "*" >>test.diff
	@echo "**********************************" >>test.diff
	@echo "Test numéro 24 : Demo_AM0 en XYZ  " >>test.diff
	@echo "**********************************" >>test.diff
	@echo "Diff filtré entre $(data7)/DemoAM_XYZ3.TRA et $(data7)/DemoAM.TRA" >>test.diff
	$(need_recode) $(data7)/DemoAM.TRA
	$(diff) $(difopt) $(data7)/DemoAM_XYZ3.TRA $(data7)/DemoAM.TRA | grep $(grepopt) >>test.diff
	-$(compare_BIN) $(data7)/DemoAM_XYZ3.BIN $(data7)/DemoAM.BIN >>test.diff
#	$(need_recode) $(data7)/Mage_fin.ini
#	$(diff) $(difopt) $(data7)/Mage_fin_XYZ3.ini $(data7)/Mage_fin.ini >>test.diff

update_REF24: run7b
	@echo
	@echo "## Mise à jour des fichiers de résultats de référence : Demo_AM0 en XYZ  ##"
	cp $(data7)/DemoAM.TRA $(data7)/DemoAM_XYZ3.TRA
	cp $(data7)/DemoAM.BIN $(data7)/DemoAM_XYZ3.BIN

test25: run8b
	@echo "*" >>test.diff
	@echo "*************************************" >>test.diff
	@echo "Test numéro 25 : Clairmarais en XYZ  " >>test.diff
	@echo "*************************************" >>test.diff
	@echo "Diff filtré entre $(data8)/clairmao_XYZ2.TRA et $(data8)/clairmao.TRA" >>test.diff
	$(need_recode) $(data8)/clairmao.TRA
	$(diff) $(difopt) $(data8)/clairmao_XYZ2.TRA $(data8)/clairmao.TRA | grep $(grepopt) >>test.diff
	-$(compare_BIN) $(data8)/clairmao_XYZ2.BIN $(data8)/clairmao.BIN >>test.diff
#	$(need_recode) $(data8)/Mage_fin.ini
#	$(diff) $(difopt) $(data8)/Mage_fin_XYZ2.ini $(data8)/Mage_fin.ini >>test.diff

update_REF25: run8b
	@echo
	@echo "## Mise à jour des fichiers de résultats de référence : Clairmarais en XYZ  ##"
	cp $(data8)/clairmao.TRA $(data8)/clairmao_XYZ2.TRA
	cp $(data8)/clairmao.BIN $(data8)/clairmao_XYZ2.BIN

test26: run10b
	@echo "*" >>test.diff
	@echo "**************************************" >>test.diff
	@echo "Test numéro 26 : Ardèche amont en XYZ " >>test.diff
	@echo "**************************************" >>test.diff
	@echo "Diff filtré entre $(data10)/ardeche1_XYZ2.TRA et $(data10)/ardeche1.TRA" >>test.diff
	$(need_recode) $(data10)/ardeche1.TRA
	$(diff) $(difopt) $(data10)/ardeche1_XYZ2.TRA $(data10)/ardeche1.TRA | grep $(grepopt) >>test.diff
	-$(compare_BIN) $(data10)/ardeche1_XYZ2.BIN $(data10)/ardeche1.BIN >>test.diff
#	$(need_recode) $(data10)/Mage_fin.ini
#	$(diff) $(difopt) $(data10)/Mage_fin_XYZ2.ini $(data10)/Mage_fin.ini >>test.diff

update_REF26: run10b
	@echo
	@echo "## Mise à jour des fichiers de résultats de référence : Ardèche amont en XYZ  ##"
	cp $(data10)/ardeche1.TRA $(data10)/ardeche1_XYZ2.TRA
	cp $(data10)/ardeche1.BIN $(data10)/ardeche1_XYZ2.BIN

test_27: run_11b
	@echo "*" >>test.diff
	@echo "****************************************" >>test.diff
	@echo "Test numéro 27 : Ardèche complet en XYZ " >>test.diff
	@echo "****************************************" >>test.diff
	@echo "Diff filtré entre $(data11)/ardeche5_XYZ4.TRA et $(data11)/ardeche5.TRA" >>test.diff
	$(need_recode) $(data11)/ardeche5.TRA
	$(diff) $(difopt) $(data11)/ardeche5_XYZ4.TRA $(data11)/ardeche5.TRA | grep $(grepopt) >>test.diff
	-$(compare_BIN) $(data11)/ardeche5_XYZ4.BIN $(data11)/ardeche5.BIN >>test.diff
#	$(need_recode) $(data11)/Mage_fin.ini
#	$(diff) $(difopt) $(data11)/Mage_fin_XYZ4.ini $(data11)/Mage_fin.ini >>test.diff

update_REF_27: run_11b
	@echo
	@echo "## Mise à jour des fichiers de résultats de référence : Ardèche complet en XYZ  ##"
	cp $(data11)/ardeche5.TRA $(data11)/ardeche5_XYZ4.TRA
	cp $(data11)/ardeche5.BIN $(data11)/ardeche5_XYZ4.BIN

test28: run12b
	@echo "*" >>test.diff
	@echo "************************************" >>test.diff
	@echo "Test numéro 28 : ISM_DemoAM0 en XYZ " >>test.diff
	@echo "************************************" >>test.diff
	@echo "Diff filtré entre $(data12)/DemoAM_XYZ4.TRA et $(data12)/DemoAM.TRA" >>test.diff
	$(need_recode) $(data12)/DemoAM.TRA
	$(diff) $(difopt) $(data12)/DemoAM_XYZ4.TRA $(data12)/DemoAM.TRA | grep $(grepopt) >>test.diff
	-$(compare_BIN) $(data12)/DemoAM_XYZ4.BIN $(data12)/DemoAM.BIN >>test.diff
#	$(need_recode) $(data12)/Mage_fin.ini
#	$(diff) $(difopt) $(data12)/Mage_fin_XYZ4.ini $(data12)/Mage_fin.ini >>test.diff

update_REF28: run12b
	@echo
	@echo "## Mise à jour des fichiers de résultats de référence : ISM_DemoAM0 en XYZ  ##"
	cp $(data12)/DemoAM.TRA $(data12)/DemoAM_XYZ4.TRA
	cp $(data12)/DemoAM.BIN $(data12)/DemoAM_XYZ4.BIN

test29: run20
	@echo "*" >>test.diff
	@echo "**********************************************" >>test.diff
	@echo "Test numéro 29 : Régulation Rhône-Isère en LC " >>test.diff
	@echo "**********************************************" >>test.diff
	@echo "Diff filtré entre $(data20)/modele1_LC_REF.TRA et $(data20)/modele1_LC.TRA" >>test.diff
	$(need_recode) $(data20)/modele1_LC.TRA
	$(diff) $(difopt) $(data20)/modele1_LC_REF.TRA $(data20)/modele1_LC.TRA | grep $(grepopt) >>test.diff
	-$(compare_BIN) $(data20)/modele1_LC_REF.BIN $(data20)/modele1_LC.BIN >>test.diff
	$(need_recode) $(data20)/Mage_fin.sin
	$(diff) $(difopt) $(data20)/Mage_fin_REF.sin $(data20)/Mage_fin.sin >>test.diff

update_REF29: run20
	@echo
	@echo "## Mise à jour des fichiers de résultats de référence : Régulation Rhône-Isère en LC  ##"
	cp $(data20)/modele1_LC.TRA $(data20)/modele1_LC_REF.TRA
	cp $(data20)/modele1_LC.BIN $(data20)/modele1_LC_REF.BIN
	cp $(data20)/Mage_fin.sin $(data20)/Mage_fin_REF.sin

test30: run20b
	@echo "*" >>test.diff
	@echo "***********************************************" >>test.diff
	@echo "Test numéro 30 : Régulation Rhône-Isère en XYZ " >>test.diff
	@echo "***********************************************" >>test.diff
	@echo "Diff filtré entre $(data20)/modele1_XYZ.TRA et $(data20)/modele1.TRA" >>test.diff
	$(need_recode) $(data20)/modele1.TRA
	$(diff) $(difopt) $(data20)/modele1_XYZ.TRA $(data20)/modele1.TRA | grep $(grepopt) >>test.diff
	-$(compare_BIN) $(data20)/modele1_XYZ.BIN $(data20)/modele1.BIN >>test.diff
	$(need_recode) $(data20)/Mage_fin.sin
	$(diff) $(difopt) $(data20)/Mage_fin_XYZ.sin $(data20)/Mage_fin.sin >>test.diff

update_REF30: run20b
	@echo
	@echo "## Mise à jour des fichiers de résultats de référence : Régulation Rhône-Isère en XYZ  ##"
	cp $(data20)/modele1.TRA $(data20)/modele1_XYZ.TRA
	cp $(data20)/modele1.BIN $(data20)/modele1_XYZ.BIN
	cp $(data20)/Mage_fin.sin $(data20)/Mage_fin_XYZ.sin

test31: run21
	@echo "*" >>test.diff
	@echo "*********************************************************" >>test.diff
	@echo "Test numéro 31 : Charriage : vérification largeur active " >>test.diff
	@echo "*********************************************************" >>test.diff
	@echo "Diff filtré entre $(data21)/$(etude21)_REF.TRA et $(data21)/$(etude21).TRA" >>test.diff
	$(need_recode) $(data21)/$(etude21).TRA
	$(diff) $(difopt) $(data21)/$(etude21)_REF.TRA $(data21)/$(etude21).TRA | grep $(grepopt) >>test.diff
	-$(compare_BIN) $(data21)/$(etude21)_REF.BIN $(data21)/$(etude21).BIN >>test.diff
	@echo "Diff entre $(data21)//MailleurPF/Bief_1_final_REF.M et $(data21)/MailleurPF/Bief_1_final.M" >>test.diff
	$(need_recode) $(data21)/MailleurPF/Bief_1_final.M
	$(diff) $(difopt) $(data21)/MailleurPF/Bief_1_final_REF.M $(data21)/MailleurPF/Bief_1_final.M >>test.diff

update_REF31: run21
	@echo
	@echo "## Mise à jour des fichiers de résultats de référence : Régulation Rhône-Isère en XYZ  ##"
	cp $(data21)/$(etude21).TRA $(data21)/$(etude21)_REF.TRA
	cp $(data21)/$(etude21).BIN $(data21)/$(etude21)_REF.BIN
	cp $(data21)/MailleurPF/Bief_1_final.M $(data21)/MailleurPF/Bief_1_final_REF.M

mage_Compare_BIN: mage_Compare_BIN.f90
	@echo
	@echo "##  Compilation de $< ..."
	gfortran -o mage_Compare_BIN mage_Compare_BIN.f90
	@echo

rm_mage_Compare_BIN: test.diff
	@echo
	@echo "##  Suppression de l'exécutable mage_Compare_BIN"
	@rm mage_Compare_BIN
	@echo

# TODO: gérer les dépendances du fichier test.diff
# NOTE: le cas Fleuve-Rouge n'a pas de fichiers ST donc ne peut pas être joué avec Mage-8
list:= 0 1 2 3 4 5 6 7 8 10 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 28 29 30 31
#list:= 0 1 2 3 4 5 6 7 8 10 11 12 13 14 15 16 17 18 19
runalltests:= $(foreach num, $(list), test$(num))
updateallREF:= $(foreach num, $(list), update_REF$(num))
alltests: build mage_Compare_BIN $(runalltests) rm_mage_Compare_BIN
test: build mage_Compare_BIN test0 test$(num) rm_mage_Compare_BIN
updateAll: build $(updateallREF)
updateREF: build update_REF0 update_REF$(num)
