!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteurs : Pierre FARISSIER & Jean-Baptiste FAURE - INRAE - 1989-2020       #
!# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################
! secma 54g avec choix des lignes directrices
! modification du 24/2/2000 passage en double precision
! secma54 du 31/8/00 avec nombre de zones porte de 30 à 220
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!                                                                             C
! Nom du fichier: SECMA5.FOR                  Date: 05/08/91      Auteur:PF   C
!                                                                             C
! But:  CREER DES SECTION EN TRAVERS INTERMEDIAIRES                           C
!       D'UN FICHIER DE SECTIONS EN TRAVERS                                   C
!       INTERPOL ENTRE CN : BEZIER ETIMATEUR SIMPLE                           C
!                                                                             C
!       NUMERISATION DES SECTIONS: ABCISSE CURVILIGNE                         C
!       NB DE SECTION INTER AU CHOIX                                          C
!       DECOUPE EN SECTIONS PLANES                                            C
!                                                                             C
!       avec UTILISATION DES ZONES ET NEW FORMAT F12.4                        C
! AUTOMATIQUE: TAILLE DE LA MAILLE EN DONNÉE                                  C
!                                                                             C
! IDEM SECMA2 MAIS DECOUPAGES EN SOUSROUTINES...                              C
! 17 12 91 autorise zone de taille nulle                                      C
! V3.3 13 01 92 CHOIX PL/BORD POUR DECOUPAGE DES COURBES                      C
! V3.4 10 02 92 calcul des pk pour sections interpolees                       C
! V3.4 11 02 92 CHOIX DES LIGNES PRISES EN COMPTE                             C
! V3.5 22 06 92 MARQUAGE DES LIGNES DIRECTRICES DANS LE .M + DECOUPAGE        C
!               REPARTITION DES PLANS DE COUPES SELON EQUIDISTANCE/COURBES    C
!               GESTION DES SECTIONS NON PLANES                               C
!               2 TYPE DE DERIVES: OCS OU PERPENDICULAIRE AU ST               C
!      29 06 92 TRANSFERT DU CODE ASSOCIE AUX POINTS DIRECTEURS               C
! V4.0 08 07 92 optimisation du nombre de maille (bof...)                     C
!               VERIF ESTIM DERIVEE: MEME POINT=MEME DERIVEE                  C
!               SECURISATION MINI
! V4.1 17 09 92 OPTION LINZ INTERPOLATION LINEAIRE SUR Z
! V4.2 15 10 92 CORRECTION PROVISOIRE BAVURE DERIVEE POUR LIGNES CONFONDUES   C
!               SOL DEFINITIVE : ECRIRE BASE DE DISTANCE (COMME ANGLE)        C
!               + LIGNE + DIRECTRICE QUE D'AUTRE.... BORDEL TOTAL
!                 -> A ECRIRE
! V5.0 10 09 93 REECRITURE DE L INTERPOLATEUR DE TRAJECTOIRE: SPLINE T B C
!               + MODULARISATION
!              A REGLER: Z LIN, MISE A PERPENDICULAIRE, CONTROLE PT
!               CONFONDUS, T VARIABLE, CB IDEM...
!V5.01 28 10 94 NOM SUR LES SECTIONS
!               FIN DE LA NOTION DE LIT MINEUR: DEMANDE LD POUR DECOUPE ET PK
!V5.02 2  11 94 PAS ENTRE MAX SECTION LD ET BORDS
!V5.1     11 96 LECTURE d'un .ST en entree
! passage a 20 comme coeffcient multiplicatuer pour optimiser et a 30000 points le 4 juin 2007
! le 31 aout 2007, correction du nombre de mailles et introduction d'un decoupage lineaire au lieu de splin en longitudinal
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      !program SECMA5  !JBF 2009/03/23 : transformation de secma5 en routine
      subroutine secma(ficin,ficout,tmoy)

      implicit none
      INTEGER NOST,NOPT,NBPTSTI,NBST,NBSTF,n
     :,NBLGN,NBPTZN,MZOP,NBPTZOP,NBPTST,LINZ
     :,NPTFIN,NBLGNO,LPC,NZONE,NSTPER,KSTPLAN,NBPER,NBHISTO
     :,NBSTI,NOL,NOZN,NPTDEP,NOLO,NOPTIDEP
     :,LGN1,LGN2,NPTSTI,NOPTI,NLDAXE,NBPTZ,NO
     :,KS1,KP1,KZO,KS2,KP2,KP3,PTLGN,N2ZN
      DOUBLE PRECISION Z,X,Y,XB,YB,ZB,VERSION
     :,DC,XPL,D2C,DPLC,D1,D2,YPL,ZPL,PK,PKF,T0,H31,H32,H2
     :, T1,DY1,DZ1,B0,B1,C0,C1,DZ0,DX1,DX0,DY0
     :,XT,YT,ZT,X1,Y1,Z1,X2,Y2,Z2,DT0,DT1,DT2,D,T,TMOY,DPL,D1C
     :,PAS,PASM
      CHARACTER CLIGN*4,CPLAN*1
      INTEGER IPLAN
! variable pour choisir maillage en plan ou en 3D
      LOGICAL LPLAN
! variable pour interpolation lineaire ou par spline
      LOGICAL LINEAIRE
!
      PARAMETER (VERSION=6.1)
! NB ST MAX EN ENTREE
      PARAMETER (KS1=1000)
! NB DE POINT PAR ST MAX EN ENTREE
      PARAMETER (KP1=1000)
! NB ZONE MAX
      PARAMETER (KZO=1000)
! NB ST MAX DANS BD INTERMEDIAIRE
      PARAMETER (KS2=5000)
! NB PT PAR ST MAX DANS BD INTERMEDIAIRE
      PARAMETER (KP2=3000)
! NB PT PAR ST MAX DANS BD FINALE (POST OPTIMIZ)
      PARAMETER (KP3=1000)
      DOUBLE PRECISION  STX(KP1,KS1),STY(KP1,KS1),STZ(KP1,KS1),
     +  STIX(KP2,KS1),STIY(KP2,KS1),STIZ(KP2,KS1),
     + STFX(KP2,KS2),STFY(KP2,KS2),
     + STFZ(KP2,KS2),STFOX(KP3,KS2),STFOY(KP3,KS2),
     + STFOZ(KP3,KS2)
      CHARACTER FICIN*35, FICOUT*35
      CHARACTER  REP*1
! pour ne donner que 1 pas transversal
      Logical UNPAS
! pour choisir maximum de section ou minimum
      integer ILONGMI

!
!
! ENTETE DE FICHIER: IE HISTORIQUE DU TRAITEMENT:
      CHARACTER HISTO*80
      COMMON/BDHISTO/HISTO(20)/NBHISTO/NBHISTO
      COMMON/HISTO/H2,H31,H32,LINZ,NLDAXE
!
! BD DIM DES STRUCTURES:
! nbst   : nombre de st du fichier de depart  (<KS1)
! nbptsti: nombre de point des st du maillage (<KP2 PUIS KP3)
! nbstf  : nombre de st du maillage           (<KS2)
!
      COMMON/BDDIM/NBST,NBPTSTI,NBSTF
!
! BD ST  (ORIGINE FICHIER de depart)
!  NBPTST()*NBST -> COTE X Y Z DU POINT
      COMMON/BDST/NBPTST(KS1),STX,STY,STZ

! BD ENTETE DES SECTIONS
! PK: FICHIER ORIGINE, PKF: PK MAILLAGE
      COMMON/BDENTETE/PK(KS1),PKF(KS2)
! BD NOM DES SECTIONS ORIGINE
       CHARACTER*12 NOMST
       COMMON /BDNOMST/NOMST(KS1)

! BD ZONES (SECTION X NOZONE)->NO PT D'APPUI SUR LA SECTION
! NBLGNO=NB LIGNE .SZ, LPC:LIGNE PRISE EN COMPTE,
! NBLGN:NB LGN PRISE EN COMPTE
      CHARACTER CODLGN*3
      COMMON /BDZO/NZONE(KS1,KZO),NBLGNO,LPC(KZO),CODLGN(KZO)
!
! INDICE DES LIGNES DIRECTRICES DANS LE MAILLAGE
! + NB POINT /ZONE, PAS MOYEN /ST/ZONE,
      COMMON/BDLGN/PTLGN(0:KZO+1),PASM(KZO)
      COMMON/NBDLGN/NBLGN,NBPTZN(KZO)
!
! BD POUR MODULE OPTIMISATION
! MZOP ZONE OPTIMISER=1,
      COMMON/BDOPT/MZOP(KZO),NBPTZOP(KZO)
!
! BD FLAG POUR ST MESUREES PLANES:
      COMMON /BDSTPLAN/ KSTPLAN(KS1)
!
! BD ST INTERMEDIAIRE (ST de points equidistants SELON ABCISSE CURVILIGNE )
!                      INTERPOL LIN ENTRE PTS D'UNE MEME SECTION
! NBPT-DE-LA-ZONE * NB-SECTIONS
!   NBPTSTI*NBST
       COMMON/STI/STIX,STIY,STIZ
!
! BD ST OU LES TRAJ SONT PERPENDICULAIRE AU ST (PLAN X,Y)
      COMMON/BDSTPER/NBPER,NSTPER(KS1)
!
! BD DES TENSION CONTINUITE BIAIS A DROITE (0) ET GAUCHE (1) DE CHAQUE PT
      COMMON/TCB/T0(KP2),T1(KP2),C0(KP2),C1(KP2),B0(KP2),B1(KP2)
! BD DES TGTES AUX PTS DES STI A DROITE (0) ET GAUCHE (1)
      COMMON/DERIV/DX0(KP2),DY0(KP2),DZ0(KP2),DX1(KP2),DY1(KP2),DZ1(KP2)
!
! BD ST  FINALE  AVANT OPTIMISATION NBPTSTI*NBSTF
! NBSTI nb des section inter entre section de depart
      COMMON/BDSTF/ NBSTI(KS2),STFX,STFY,
     + STFZ
!
! BD STF APRES OPTIMISATION....
      COMMON/BDSTFO/ STFOX,STFOY,
     + STFOZ
!
! CALCULS LOCAUX: BD LONGUEUR POUR ST EN ENTREE
      DOUBLE PRECISION LSEG(KP1,KS1),LCUM(KP1,KS1),LTOT(KS1)
      DOUBLE PRECISION LCOUR,LTOTMIN,LTOTMAX,LTOTMOY
! PROFIL EN LONG:
       INTEGER NOPTLM(KS2)

      interface
         character*10 function read_option(lu,key)
            integer,intent(in) :: lu
            character(len=*),intent(in) :: key
         end function read_option
      end interface

      character :: read__option*10, c_nptsti*2

      UNPAS=.FALSE.
!
      WRITE(9,*) ' But:  CREER UN MAILLAGE DE LA RIVIERE BASEE '
      WRITE(9,*) '       SUR  DES SECTIONS EN TRAVERS'
      WRITE(9,*) '      -NUMERISATION DES SECTIONS: ABCISSE CURVILIGNE'
      WRITE(9,*) '      -INTERPOL ENTRE CN : SPLINE T C B '
      WRITE(9,*) '      -PRISE EN COMPTE DE ZONES'
      WRITE(9,*) '      -OPTIMISATION DES MAILLES '
      WRITE(9,*) ' '
      WRITE(9,'(A,F3.1)') '   VERSION: ',VERSION
      WRITE(9,*) ' '
!
! DEMANDE ET LECTURE DU FICHIER .SZ
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
 21   WRITE(9,20)
 20   FORMAT(//,' NOM DU FICHIER DE SECTIONS .ST: ',$)
      !READ(*,'(A35)') FICIN  !-JBF 2009/03/23 : réponse en argument
      OPEN(11,FILE=FICIN,STATUS='OLD') !il faut que le code plante si le fichier n'existe pas
 23   WRITE(9,22)
 22   FORMAT(//,' NOM DU FICHIER DE MAILLES EN SORTIE : '
     :,$)
      !READ(*,'(A35)') FICOUT  !-JBF 2009/03/23 : réponse en argument
      OPEN(10,FILE=FICOUT,STATUS='unknown')
      CALL LECT_FIC(11)
      CLOSE(11)
      WRITE(9,*)'si vous voulez un maillage '
      WRITE(9,*)'calculé sur des distances horizontales'
      WRITE(9,*)'entrez 2 (-2 pour interpolation longitudinale spline)'
      write(9,*)'sinon les distances seront en 3D'
      WRITE(9,*)'entrez 3 ',
     &          '(-3 pour interpolation longitudinale linéaire)'
      !READ(*,*)Iplan
         iplan = -3

      IF(IPLAN.EQ.-2)THEN
        LPLAN=.TRUE.
        LINEAIRE=.FALSE.
      ELSEIF(IPLAN.EQ.2)THEN
        LPLAN=.TRUE.
        LINEAIRE=.TRUE.
      ELSEIF(IPLAN.EQ.-3)THEN
        LPLAN=.FALSE.
        LINEAIRE=.TRUE.
      ELSE
        LPLAN=.FALSE.
        LINEAIRE=.FALSE.
      ENDIF
!
! TEST PLANEITE DES SECTIONS
      CALL ST_PLAN
!
! DEMANDE DE PRISE EN COMPTE DES LIGNES:
      NBLGN=0
      LPC(2)=0
      WRITE(9,*) 'NBLGNO',NBLGNO
      DO 50 NOL=2,NBLGNO-1
      WRITE(9,*) 'PRISE EN COMPTE DE LA LIGNE',NOL-1,' (0/1)'
      If(NOL.EQ.2)then
      Write(9,*) 'repondez 2 pour prise en compte de toutes les lignes'
      Write(9,*) 'repondez 3 pour prise en compte de toutes les lignes'
      Write(9,*) 'et un seul pas espace transversal'
      endif
   51 continue
      !READ (*,'(A)')CPLAN  !-JBF 2009/03/23 : réponse par défaut
      cplan = '3'           !+JBF 2009/03/23 : réponse par défaut

      IF(CPLAN.NE.' ')THEN
        READ(CPLAN,'(I1)')LPC(NOL)
      ELSE
        LPC(NOL)=3
      ENDIF
        IF (LPC(NOL).EQ.1)then
          NBLGN=NBLGN+1
      elseif(lpc(nol).eq.2)then
         do n=2,nblgno-1
           lpc(n)=1
         enddo
         nblgn=nblgno-2
         go to 550
      elseif(lpc(nol).eq.3)then
         UNPAS=.TRUE.
         do n=2,nblgno-1
           lpc(n)=1
         enddo
         nblgn=nblgno-2
         go to 550
       elseif(lpc(nol).ne.0)then
            GOTO 51
       endif
 50   CONTINUE
 550  LPC(1)=1
      LPC(NBLGNO)=1
      WRITE(9,*) 'PRISE EN COMPTE DE ',NBLGN,' LIGNES'
      WRITE(9,*) ' SOIT ',NBLGN+1,' ZONES'

! MISE A JOUR DE NZONE(ST,NOL)
      DO 53 NOST=1,NBST
      NOL=1
      DO 52 NOLO=1,NBLGNO
        IF(LPC(NOLO).EQ.1) THEN
           NZONE(NOST,NOL)=NZONE(NOST,NOLO)
           NOL=NOL+1
        ENDIF
 52     CONTINUE
 53   CONTINUE
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
! DECOUPAGE DES SECTIONS EN NBPTSTI POINTS-> STI
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
! INITIALISATIONS
      NOPTIDEP=1
      PTLGN(0)=1
!
! POUR TOUTES LES ZONES
!      WRITE(9,*) 'PAS MOYEN SUR LES ZONES :'
!      READ*,PASM0
      !pasmo = 1  !+JBF 2009/03/23 : réponse par défaut
      DO 105 NOZN=1,NBLGN+1
!
! PRELIMINAIRES: CALCULS DES LONGEURS DES SECTIONS SUR LA ZONE:
! PRELIMINAIRES: STATISTIQUE SUR LES LONGUEURS (-> CHOIX DU PAS)
      LTOTMIN= 1.E10
      LTOTMAX=-1.E10
      LTOTMOY= 0.
        DO 107 NOST=1,NBST
!
        NPTDEP=NZONE(NOST,NOZN)
        NPTFIN=NZONE(NOST,NOZN+1)
        NBPTZ=NPTFIN-NPTDEP
!
        LCUM(1,NOST)=0
        DO 110 NO=1,NBPTZ
          NOPT=NPTDEP-1+NO
          XB=STX(NOPT,NOST)
          YB=STY(NOPT,NOST)
          ZB=STZ(NOPT,NOST)
          X=STX(NOPT+1,NOST)
          Y=STY(NOPT+1,NOST)
          Z=STZ(NOPT+1,NOST)
          IF(LPLAN)THEN
          LSEG(NO,NOST)=SQRT((X-XB)*(X-XB)+(Y-YB)*(Y-YB))
          ELSE
          LSEG(NO,NOST)=SQRT((X-XB)*(X-XB)+(Y-YB)*(Y-YB)+(Z-ZB)*(Z-ZB))
          ENDIF
          LCUM(NO+1,NOST)=LCUM(NO,NOST)+LSEG(NO,NOST)
 110      CONTINUE
!
        LTOT(NOST)= LCUM(NBPTZ+1,NOST)
        IF (LTOT(NOST).GT.LTOTMAX) LTOTMAX=LTOT(NOST)
        IF (LTOT(NOST).LT.LTOTMIN) LTOTMIN=LTOT(NOST)
        LTOTMOY=LTOTMOY+LTOT(NOST)

!       SECTION SUIVANTE
 107    CONTINUE

! CHOIX DU PAS ...
      LTOTMOY=LTOTMOY/NBST
      If(.NOT.UNPAS)then
      WRITE(9,*) ' '
      WRITE(9,*) '      TRAITEMENT  ZONE   ',NOZN
      WRITE(9,*) 'LONGUEUR MOYENNE DE LA ZONE SUR LES SECTIONS:',LTOTMOY
      WRITE(9,*) 'LONGUEUR MIN     DE LA ZONE SUR LES SECTIONS:',LTOTMIN
      WRITE(9,*) 'LONGUEUR MAX     DE LA ZONE SUR LES SECTIONS:',LTOTMAX
!
      WRITE(9,*) 'PAS MOYEN SUR LA ZONE ?'
      WRITE(9,*) '(ENTREZ 0 pour donner le nombre de mailles)'
!      READ*,PASM(NOZN) !-JBF 2009/03/23 : réponse par défaut
      pasm(nozn) = 0    !+JBF 2009/03/23 : réponse par défaut
!
! OPTIMISATION ???
      WRITE(9,*) 'OPTIMISATION SIGNIFIE CONSERVER PAS TRANSVERSAL'
      WRITE(9,*) 'NE PAS OPTIMISER = CONSERVER NOMBRE DE POINTS'
      WRITE(9,*) 'ZONE A OPTIMISER (0,1):'
!      WRITE(9,*) '   PERSONNELLEMENT JE DECONSEILLE ....'
      !READ(*,'(A)')CPLAN
      cplan = '0'
      IF(CPLAN.NE.' ')THEN
        READ(CPLAN,'(I1)')MZOP(NOZN)
      ELSE
        MZOP(NOZN)=0
      ENDIF
!        MZOP(NOZN)=0
! si on ne veut qu'un pas : on en rentre le pas qu'une fois
      elseIF(NOZN.eq.1)then
      WRITE(9,*) 'PAS TRANSVERSAL :'
!      READ*,PASM(NOZN) !-JBF 2009/03/23 : réponse par défaut
      pasm(nozn) = 0    !+JBF 2009/03/23 : réponse par défaut
!
! OPTIMISATION ???
      WRITE(9,*) 'OPTIMISATION SIGNIFIE CONSERVER PAS TRANSVERSAL'
      WRITE(9,*) 'NE PAS OPTIMISER = CONSERVER NOMBRE DE POINTS'
      WRITE(9,*) 'TOUTES ZONES A OPTIMISER (0,1):'
!      WRITE(9,*) '   PERSONNELLEMENT JE DECONSEILLE ....'
      !READ(*,'(A)')CPLAN  !-JBF 2009/03/23 : réponse par défaut
      cplan = '0'          !+JBF 2009/03/23 : réponse par défaut

      IF(CPLAN.NE.' ')THEN
        READ(CPLAN,'(I1)') MZOP(NOZN)
      ELSE
        MZOP(NOZN)=0
      ENDIF
      DO N2ZN=1,NBLGN+1
           pasm(N2ZN)=pasm(1)
           MZOP(N2ZN)=MZOP(1)
      ENDDO
! cas de un seul pas deja entre
!      else c
!           pasm(NOZN)=pasm(1)
!           MZOP(NOZN)=MZOP(1)
! fin du if sur unpas
      endif

!
      IF(MZOP(NOZN).EQ.1) THEN
! TANT PIS, J AVAIS PREVENU...
! NBPTSTI VAUT LE MAXIMUM DE COURBES DEMANDÉ * 10 POUR POUVOIR CHOISIR:
        If(PASM(NOZN).EQ.0.)THEN
          write(9,*)'donnez le nombre maximal de mailles zone ',NOZN
          read(*,*)NPTSTI
          PASM(NOZN)=LTOTMAX/FLOAT(NPTSTI+1)
        ELSE
            NPTSTI = INT(LTOTMAX/PASM(NOZN))
          ENDIF
           IF(NPTSTI.EQ.0)THEN
             MZOP(NOZN)=0
             NPTSTI=1
             NBPTZN(NOZN)=NPTSTI
           ELSE
             NBPTZN(NOZN)=NPTSTI
!             NPTSTI =NPTSTI *10
             NPTSTI =NPTSTI *20
             NBPTZOP(NOZN)=NPTSTI
           ENDIF
      ELSE
! CAS NON OPTIMISE
         If(PASM(NOZN).EQ.0.)THEN
            write(9,*)'donnez le nombre de mailles zone ',NOZN
            c_nptsti = trim(read_option(7,'nb_mailles_par_zone'))
            nptsti = 12
            if (c_nptsti == '') then
               write(7,'(a)') '*Nombre de mailles par zone'
               write(7,'(a,i2.2)') 'nb_mailles_par_zone = ',nptsti
            else
               read(c_nptsti,'(i2)') nptsti
            endif
         ELSE
            NPTSTI = INT(LTOTMOY/PASM(NOZN))
         ENDIF
! SI LE PAS EST PLUS GRAND QUE LA LONG MOYENNE, PAS DE LIGNE INTERMEDIAIRE
! MAIS JUSTE UNE LIGNE= LA LIGNE DIRECTRICE:
        IF(NPTSTI.EQ.0) NPTSTI=1
          NBPTZN(NOZN)=NPTSTI
! ecriture deplacee le 11/09/06 car faux en cas d'optimisation
          WRITE(9,*) 'Nombre de mailles  pour la zone',NOZN,' : ',NPTSTI
          WRITE(9,*) 'Pas moyen sur cette zone : ',LTOTMOY/(NPTSTI*1.)
! fin du if sur optimise
      ENDIF
!
! POSITION DES LIGNES DIRECTRICE DANS LE MAILLAGE:
      PTLGN(NOZN)=PTLGN(NOZN-1)+NBPTZN(NOZN)
!
! FIN DES PRELIMINAIRES
! DEBUT DU DECOUPAGE DES SECTIONS DE LA ZONES
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
! POUR TOUTES LES SECTIONS DE DONNEES:
      DO 120 NOST=1,NBST
!
!        PAS = LTOT(NOST)/(NPTSTI *1.)
        PAS = LTOT(NOST)/FLOAT(NPTSTI)
!         WRITE(9,*) 'SECTION LONGEUR PAS :',NOST,LTOT(NOST),PAS
!          IF (PAS.LT.8.) WRITE(9,*)
!     +    ' ATTENTION=====> DISTANCE ENTRE POINT<8 SECTION:',NOST
!
! POINTS DE DEPART ET DE FIN DE LA ZONE SUR LA SECTION NOST
        NPTDEP=NZONE(NOST,NOZN)
        NPTFIN=NZONE(NOST,NOZN+1)
        NBPTZ=NPTFIN-NPTDEP
!
! RECOPIE DU POINT DE LA LIGNE DIRECTRICE (NOZN)
        STIX(NOPTIDEP,NOST)=STX(NPTDEP,NOST)
        STIY(NOPTIDEP,NOST)=STY(NPTDEP,NOST)
        STIZ(NOPTIDEP,NOST)=STZ(NPTDEP,NOST)
!
! RECHERCHE DES (NBPTSTI-1) POINTS SUIVANTS:
        DO 140 NOPTI=1,NPTSTI -1
! RECHERCHE LE POINT A LCOUR DU DEBUT DE LA ZONE
          LCOUR=NOPTI*PAS
!
! CAS PARTICULIER: ZONE DE TAILLE NULLE: TOUTES LES LIGNES PASSENT PAR 1 PT
          IF (PAS.EQ.0.) THEN
!              WRITE(9,*) 'WARNING: ZONE DE TAILLE NULLE ....'
            XT=STX(NPTDEP,NOST)
            YT=STY(NPTDEP,NOST)
            ZT=STZ(NPTDEP,NOST)
            GOTO 150
          ENDIF
!
!   RECHERCHE DU SEGMENT A LCOUR
          DO 160 NOPT=2,NBPTZ+1
            IF(LCUM(NOPT,NOST).GE.LCOUR) GOTO 180
 160        CONTINUE
          WRITE(9,*) 'WARNING...LCOUR>LTOT   SECTION',NOST
!
! CALCUL DU Z INTERPOLATION LINEAIRE
 180      X1=STX((NPTDEP-1)+NOPT-1,NOST)
          X2=STX((NPTDEP-1)+NOPT,NOST)
          Y1=STY((NPTDEP-1)+NOPT-1,NOST)
          Y2=STY((NPTDEP-1)+NOPT,NOST)
          Z1=STZ((NPTDEP-1)+NOPT-1,NOST)
          Z2=STZ((NPTDEP-1)+NOPT,NOST)
!
          T = (LCOUR-LCUM(NOPT-1,NOST))/LSEG(NOPT-1,NOST)
          XT=X1+T*(X2-X1)
          YT=Y1+T*(Y2-Y1)
          ZT=Z1+T*(Z2-Z1)
!
 150      STIX(NOPTIDEP+NOPTI,NOST)=XT
          STIY(NOPTIDEP+NOPTI,NOST)=YT
          STIZ(NOPTIDEP+NOPTI,NOST)=ZT
!
!            WRITE(9,*) NOPTIDEP+NOPTI,NOST,XT
! PT SUIVANT SUR LA SECTION
 140      CONTINUE
!
!  section suivante
 120    CONTINUE
!
! LE PROCHAIN POINT DES ST INTER EST NPTSTI PLUS LOIN:
      NOPTIDEP=NOPTIDEP+NPTSTI
! ZONE SUIVANTE
 105    CONTINUE
!
!
! ETAPE FINALE: RAJOUTER LA DERNIER LIGNE ...
      DO 190 NOST=1,NBST
        STIX(NOPTIDEP,NOST)=STX(NBPTST(NOST),NOST)
        STIY(NOPTIDEP,NOST)=STY(NBPTST(NOST),NOST)
        STIZ(NOPTIDEP,NOST)=STZ(NBPTST(NOST),NOST)
 190  CONTINUE
!
! NOMBRE DE COURBES DANS LE MAILLAGE PROVISOIRE
      NBPTSTI=NOPTIDEP
! suppression de cette ecriture car erronne en cas d'optimisation
!      WRITE(9,*) 'NOMBRE FINAL DE MAILLES ',NBPTSTI-1
      IF(NBPTSTI.GT.KP2) THEN
        WRITE(9,*) ' NOMBRE DE POINTS  PAR SECTIONS '//
     +  'INTERMEDIAIRES TROP GRAND >',KP2
        STOP
      ENDIF
!
! FIN DE DECOUPAGE DES SECTIONS
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
! AFFICHAGE DES SECTIONS INTERMEDIAIRES
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

!      WRITE(9,*) 'AFFICHAGE DES SECTIONS INTERMEDIAIRE O/N?'
!      READ(5,'(A)') REP
      REP='N'
      IF ((REP.EQ.'O').OR.(REP.EQ.'o')) THEN
        DO 200 NOST=1,NBST
          WRITE(9,*) ' '
          WRITE(9,*) 'SECTION NO',NOST
          DO 220 NOPT=1,NBPTSTI
           WRITE(9,*) STIX(NOPT,NOST),STIY(NOPT,NOST),STIZ(NOPT,NOST)
 220        CONTINUE
 200      CONTINUE
      ENDIF
!
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
! CREATION DES ST INTERMEDIAIRE
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
!      WRITE(9,*) ' '
!      WRITE(9,*) ' INTERPOLATION LINEAIRES DES ALTITUDES (0,1)?'
!      READ*,LINZ  !-JBF 2009/03/23 : réponse par défaut
! desactivation le 11 septembre pour avoir une question de moins
      LINZ=1       !+JBF 2009/03/23 : réponse par défaut
!
! CALCUL DU NOMBRE DE SECTIONS INTERMEDIARES...
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
! Demande de la ligne directrice servant d'axe:
!      WRITE(9,*) ' DONNEZ LE NUMERO DE LA LIGNE DIRECTRICE SERVANT '
!        WRITE(9,*) ' D AXE POUR LA DECOUPE DES SECTIONS: '
!        Write(9,*)  'et le calcul des PK'
!        READ(*,'(A)')Clign  !-JBF 2009/03/23 : réponse par défaut
!             IF(Clign.NE.'    ')THEN
!             read(Clign,'(I4)')nldaxe
!             ELSE
!             nldaxe=1
!             ENDIF
!        READ*,NLDAXE
         if (nblgno > 2) then
            nldaxe = NBLGNO/2+1  !+JBF 2009/03/23 : réponse par défaut
         else
            nldaxe = 1           !+JBF 2009/06/18 : cas où il n'y a pas de ligne directrice
         endif
         write(9,*) 'nldaxe = ',nldaxe
!
! CopiE des numeros des points de l'axe sur les sections dans NOPTLM:
      DO 401 NOST=1,NBST
! modif du 22 01 07 : on utilise plus loin stx et pas stix
            NOPTLM(NOST)=NZONE(NOST,NLDAXE+1)
 401    CONTINUE
! RECHERCHE DU LIT MINEUR:
!      WRITE(9,*) 'RECHERCHE DU PROFIL EN LONG...'
!      DO 401 NOST=1,NBST
!        ZMIN=1.E10
!        DO 402 NOPT=1,NBPTSTI
!          IF(STIZ(NOPT,NOST).LT.ZMIN) THEN
!            ZMIN=STIZ(NOPT,NOST)
!            NOPTLM(NOST)=NOPT
!          ENDIF
! 402    CONTINUE
!
! 401    CONTINUE
! STATISTIQUE SUR LA LONGUEUR
      WRITE(9,*) 'CALCULS DES LONGUEURS'
      D1C=0
      D2C=0
      DPLC=0
      DC=0
      DO 400 NOST=1,NBST-1

        X1=STIX(1,NOST)-STIX(1,NOST+1)
        X2=STIX(NBPTSTI,NOST)-STIX(NBPTSTI,NOST+1)
! modif du 22 01 07 : on utilise stx et pas stix
        XPL=STX(NOPTLM(NOST),NOST)-STX(NOPTLM(NOST+1),NOST+1)
        Y1=STIY(1,NOST)-STIY(1,NOST+1)
        Y2=STIY(NBPTSTI,NOST)-STIY(NBPTSTI,NOST+1)
        YPL=STY(NOPTLM(NOST),NOST)-STY(NOPTLM(NOST+1),NOST+1)
        IF(LPLAN)THEN
        D1=SQRT(X1*X1+Y1*Y1)
        D2=SQRT(X2*X2+Y2*Y2)
        DPL=SQRT(XPL*XPL+YPL*YPL)
        ELSE
        Z1=STIZ(1,NOST)-STIZ(1,NOST+1)
        Z2=STIZ(NBPTSTI,NOST)-STIZ(NBPTSTI,NOST+1)
        ZPL=STZ(NOPTLM(NOST),NOST)-STZ(NOPTLM(NOST+1),NOST+1)
        D1=SQRT(X1*X1+Y1*Y1+Z1*Z1)
        D2=SQRT(X2*X2+Y2*Y2+Z2*Z2)
        DPL=SQRT(XPL*XPL+YPL*YPL+ZPL*ZPL)
        ENDIF
!
        D1C=D1C+D1
        D2C=D2C+D2
        DPLC=DPLC+DPL
 400    CONTINUE
!
      WRITE(9,*) 'LONGUEURS: '
      WRITE(9,*) 'BORD DE MAILLAGE 1:', D1C
      WRITE(9,*) 'BORD DE MAILLAGE 2:', D2C
      WRITE(9,*) 'MOYENNE:', (D1C+D2C)/2.
      WRITE(9,*) 'PROFIL EN LONG SUR LIGNE DIRECTRICE:', DPLC
!
 403    write(9,*) 'TAILLE DE LA MAILLE MOYENNE:', tmoy
      !READ*,TMOY  !pas d'espace  !-JBF 2009/03/23 : réponse en argument
      WRITE(9,*)
     &     'le nombre de points sera calcule a partir des longueurs'
      WRITE(9,*)
     &     'des segments entre sections sur 2 DES LIGNES SUIVANTES:'
      WRITE(9,*)
     &     'DIRECTRICE choisie precedemmment(0), BORD 1(1), BORD 2(2)'
      write(9,*) ' '
      write(9,*) 'pour choisir une ligne seulement'
      write(9,*) 'vous pouvez entrer deux fois le meme chiffre '
      write(9,*) ' '
      write(9,*) 'entrez vos deux chiffres successivement '
      !READ(*,'(A)')Clign  !-JBF 2009/03/23 : réponse par défaut
      !clign = '0'          !+JBF 2009/03/23 : réponse par défaut
      clign = '1'          !+JBF 2009/03/23 : réponse par défaut, modif 2010-09-13
          IF(Clign.NE.'    ')THEN
             read(Clign,'(I4)')lgn1
          ELSE
             lgn1=0
          ENDIF
          if (dplc < 0.1) lgn1=1
      !READ(*,'(A)')Clign  !-JBF 2009/03/23 : réponse par défaut
      !clign = '0'          !+JBF 2009/03/23 : réponse par défaut
      clign = '2'          !+JBF 2009/03/23 : réponse par défaut, modif 2010-09-13
          IF(Clign.NE.'    ')THEN
             read(Clign,'(I4)')lgn2
          ELSE
             lgn2=0
          ENDIF
          if (dplc < 0.1) lgn2=1
!       READ*,LGN1,LGN2
        If(lgn1.ne.lgn2)then
            WRITE(9,*) 'pour plus de sections entrez 1'
            Write(9,*)
     &         'le calcul se fera sur la ligne de longueur maximale'
            WRITE(9,*) 'pour moins de sections entrez 2'
            Write(9,*)
     &         'le calcul se fera sur la ligne de longueur minimale'
            write(9,*) 'sinon entrez 0 pour prendre la moyenne'
            !READ(*,'(A)')CPLAN !-JBF 2009/06/18 : réponse par défaut
            cplan = read_option(7,'ilongmi')
            if (cplan == '') then
               cplan = '0'
               write(7,'(a)') ''
               write(7,'(a)') '* pour plus de sections entrez 1'
               write(7,'(a)')
     &         '*   le calcul se fera sur la ligne de longueur maximale'
               write(7,'(a)') '* pour moins de sections entrez 2'
               write(7,'(a)')
     &         '*   le calcul se fera sur la ligne de longueur minimale'
               write(9,*) '* sinon entrez 0 pour prendre la moyenne'
               write(7,'(a)') 'ilongmi = 0'
               write(7,'(a)') ''
            endif
            !cplan = '1'         !+JBF 2009/06/18 : réponse par défaut
            !cplan = '2'         !+JBF 2009/06/18 : réponse par défaut, modif 2010-09-13
            IF (CPLAN.NE.' ') THEN
               read(CPLAN,'(I1)') ilongmi
            ELSE
               ilongmi = 0
            ENDIF
        else
            ilongmi=1
        endif
!      IF (TYPE.NE.0.AND.TYPE.NE.1.AND.TYPE.NE.2) GOTO 403
!
! MISE EN BOITE POUR HISTORIQUE
      H2=TMOY
      H31=LGN1
      H32=LGN2
!
!       WRITE(9,*) 'DISTANCES ENTRE LES SECTIONS:'
      DO 410 NOST=1,NBST-1
! DISTANCE SUR LA  DIRECTRICE
! modif du 22 01 07 : on utilise stx et pas stix
        XPL=STX(NOPTLM(NOST),NOST)-STX(NOPTLM(NOST+1),NOST+1)
        YPL=STY(NOPTLM(NOST),NOST)-STY(NOPTLM(NOST+1),NOST+1)
        IF(LPLAN)THEN
        DT0=SQRT(XPL*XPL+YPL*YPL)
        ELSE
        ZPL=STZ(NOPTLM(NOST),NOST)-STZ(NOPTLM(NOST+1),NOST+1)
        DT0=SQRT(XPL*XPL+YPL*YPL+ZPL*ZPL)
        ENDIF
! DISTANCES SUR LES BORDS
        X1=STIX(1,NOST)-STIX(1,NOST+1)
        X2=STIX(NBPTSTI,NOST)-STIX(NBPTSTI,NOST+1)
        Y1=STIY(1,NOST)-STIY(1,NOST+1)
        Y2=STIY(NBPTSTI,NOST)-STIY(NBPTSTI,NOST+1)
        IF(LPLAN)THEN
        DT1=SQRT(X1*X1+Y1*Y1)
        DT2=SQRT(X2*X2+Y2*Y2)
        ELSE
        Z1=STIZ(1,NOST)-STIZ(1,NOST+1)
        Z2=STIZ(NBPTSTI,NOST)-STIZ(NBPTSTI,NOST+1)
        DT1=SQRT(X1*X1+Y1*Y1+Z1*Z1)
        DT2=SQRT(X2*X2+Y2*Y2+Z2*Z2)
        ENDIF
!
          IF(LGN1.EQ.0) D1=DT0
          IF(LGN1.EQ.1) D1=DT1
          IF(LGN1.EQ.2) D1=DT2

          IF(LGN2.EQ.0) D2=DT0
          IF(LGN2.EQ.1) D2=DT1
          IF(LGN2.EQ.2) D2=DT2
!
        If (ilongmi.eq.1)then
            D=MAX(D1,D2)
        elseIf (ilongmi.eq.2)then
            D=MIN(D1,D2)
        else
            D = 0.5*(D1+D2)
        endif
!
!
! CALCUL DU NB DE SECTIONS INTERMEDIAIRES :
! coorrection du 31 08 07 car nombre de sections et pas d'intervalles
             !NBSTI(NOST)=INT(D/TMOY)-1
             NBSTI(NOST)=NINT(D/TMOY)-1  !JBF 2010-09-13
             IF(NBSTI(NOST).LT.0)NBSTI(NOST)=0
!             write(9,*) 'Nb sections : ',nost,d,tmoy,nbsti(nost),
!     &                  NOPTLM(NOST), xpl, ypl
! AMELIORATION A FAIRE: CALCULER TOUTES DES DISTANCES SUR TOUS LES
! POINTS ET VERIFIER QU AVEC LE NOMBRE DE POINTS, ON EST DESSOUS UN MAX
        WRITE(9,*) 'SECTIONS ',NOST,'-',NOST+1, NOPTLM(NOST)
        WRITE(9,*) 'NB SECTIONS:',NBSTI(NOST),' D',D,' T MOY',
     &                            D/(NBSTI(NOST)+1)
         WRITE(9,*) 'D1 ',D1,' D2',D2,' D MOY ',D, dt0, dt1, dt2
!
 410    CONTINUE
!
! CALCULS DES ST INTERMEDIARES:
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
! pour toutes les st de depart:
 501    NBSTF=0
      DO 500 NOST=1, NBST-1
!
! RECOPI DE LA STI COUR DANS STF
        NBSTF=NBSTF+1
        IF(NBSTF.GT.KS2) THEN
          WRITE(9,*) ' NOMBRE DE SECTIONS FINALES TROP GRAND >',KS2
          STOP
        ENDIF
        WRITE(9,*) 'TRAITEMENT SECTION ',NOST,' ->',NBSTF
        DO 510 NOPTI=1, NBPTSTI
          STFX(NOPTI,NBSTF)=STIX(NOPTI,NOST)
          STFY(NOPTI,NBSTF)=STIY(NOPTI,NOST)
          STFZ(NOPTI,NBSTF)=STIZ(NOPTI,NOST)
 510      CONTINUE
! si on demande on fait une interpolation longitudinale lineaire et pas splin
      IF(LINEAIRE)THEN
           CALL CALC_ST_LINEAIRE(NOST)
        ELSE
!
! ESTIMATION DES PARAMETRES DE LINTERPOLATEUR T C B
        CALL ESTIM_TCB(NOST)
!
! CALCULS DE VECTEUR DES DERIVEES
        CALL CALC_DERIV(NOST,LINZ)
!
! CALCULS DES ST INTERMEDIAIRES SELON PLANITUDE DES SECTIONS...
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
          IF (KSTPLAN(NOST).EQ.1.AND.KSTPLAN(NOST+1).EQ.1) THEN
            CALL CALC_ST_PLANE(NOST)
          ELSE
            CALL CALC_ST_TORDU(NOST)
          ENDIF
! fin du if sur lplan
      ENDIF
!
! ST SUIVANTE
 500    CONTINUE
!
! RECOPI DE LA DERNIERE STI
      NBSTF=NBSTF+1
        IF(NBSTF.GT.KS2) THEN
          WRITE(9,*) ' NOMBRE DE SECTIONS FINALES TROP GRAND >',KS2
          STOP
        ENDIF
        WRITE(9,*) 'TRAITEMENT SECTION ',NOST,' ->',NBSTF
      DO 590 NOPTI=1, NBPTSTI
          STFX(NOPTI,NBSTF)=STIX(NOPTI,NBST)
          STFY(NOPTI,NBSTF)=STIY(NOPTI,NBST)
          STFZ(NOPTI,NBSTF)=STIZ(NOPTI,NBST)
 590    CONTINUE
!
!
      WRITE(9,*)  ' '
      WRITE(9,*) ' NOMBRE DE SECTIONS FINALES: ',NBSTF
      WRITE(9,*)  ' '
!
! OPTIMISATION DU MAILLAGE
!CCCCCCCCCCCCCCCCCCCCCCCCC
      CALL OPTIMISATION(LPLAN)
!
! CALCUL DU PK DES SECTIONS DU MAILLAGE
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      CALL CALC_PK(NLDAXE)
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
! AFFICHAGE DES SECTIONS FINALES
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!      WRITE(9,*) 'AFFICHAGE DU MAILLAGE (SECTIONS FINALES) O/N?'
!      READ(5,'(A)') REP
      REP='N'
      IF ((REP.EQ.'O').OR.(REP.EQ.'o')) THEN
        WRITE(9,*) ' '
        WRITE(9,*) 'SECTIONS FINALES '
        DO 700 NOST=1,NBSTF
          WRITE(9,*) 'SECTION NO',NOST
!
          DO 720 NOPT=1,NBPTSTI
            WRITE(9,*) STFX(NOPT,NOST),STFY(NOPT,NOST),STFZ(NOPT,NOST)
!
  720     CONTINUE
  700     CONTINUE
      ENDIF
!
!
!     ECRITURE DU FICHIER DES SECTIONS EN TRAVERS FINAL
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      CALL ECR_FIC(10,VERSION,FICIN,FICOUT)
      CLOSE(10)
!
      !WRITE(9,*) ' '
      !PAUSE
      END
!
! FIN DU PROGRAMME PRINCIPAL ....
!
!
!##############################################################################
      SUBROUTINE ESTIM_TCB(NOST)
!##############################################################################
! ESTIMATION DE LA TENTION DU BAIS ET DE CONTINUITE SUR UNE TRANCHE
!
      implicit none
      INTEGER NOST,NBPTSTI,NBST,NBSTF,NOCO
     :,KS1,KP1,KZO,KS2,KP2,KP3
      DOUBLE PRECISION
     :Y1,Z1,X1,X2,Y2,Z2,X0,Y0,Z0,D01,D02,D_11,C1,c0,B0,T0
     :,T1,b1,Z_1,y_1,x_1
      PARAMETER (KS1=1000)
      PARAMETER (KP1=1000)
      PARAMETER (KZO=1000)
      PARAMETER (KS2=5000)
      PARAMETER (KP2=3000)
      PARAMETER (KP3=1000)
      DOUBLE PRECISION  STIX(KP2,KS1),STIY(KP2,KS1),STIZ(KP2,KS1)
!
      COMMON/BDDIM/NBST,NBPTSTI,NBSTF
!
!   NBPTSTI*NBST
       COMMON/STI/STIX,STIY,STIZ
!
! BD DES TENSION CONTINUITE BIAIS A DROITE (0) ET GAUCHE (1) DE CHAQUE PT
      COMMON/TCB/ T0(KP2),T1(KP2),C0(KP2),C1(KP2),B0(KP2),B1(KP2)
!
! BD ST OU LES TRAJ SONT PERPENDICULAIRE AU ST (PLAN X,Y)
!      COMMON/BDSTPER/NBPER,NSTPER(KS1)
!
! POUR TOUTES LES COURBES
      DO 100 NOCO=1,NBPTSTI
! INIT:
       IF(NOST.NE.1) THEN
          X_1=STIX(NOCO,NOST-1)
          Y_1=STIY(NOCO,NOST-1)
          Z_1=STIZ(NOCO,NOST-1)
       ELSE
          X_1=123.456
          Y_1=123.456
          Z_1=123.456
       ENDIF
!
          X0=STIX(NOCO,NOST)
          Y0=STIY(NOCO,NOST)
          Z0=STIZ(NOCO,NOST)
!
          X1=STIX(NOCO,NOST+1)
          Y1=STIY(NOCO,NOST+1)
          Z1=STIZ(NOCO,NOST+1)
!
       IF (NOST.NE.NBST-1) THEN
          X2=STIX(NOCO,NOST+2)
          Y2=STIY(NOCO,NOST+2)
          Z2=STIZ(NOCO,NOST+2)
       ELSE
          X2=123.456
          Y2=123.456
          Z2=123.456
       ENDIF
! IL NE FAUT PAS QUE Z INTERVIENNE DANS LA TENTION
! POURQUOI ??????
       Z_1=0.
       Z0=0.
       Z1=0.
       Z2=0.
!
       D01=SQRT((X1-X0)**2+(Y1-Y0)**2+(Z1-Z0)**2)
       D02=SQRT((X2-X0)**2+(Y2-Y0)**2+(Z2-Z0)**2)
       D_11=SQRT((X1-X_1)**2+(Y1-Y_1)**2+(Z1-Z_1)**2)
!
! TENTION:
       IF(NOST.EQ.1) THEN
          T0(NOCO)=1.
       ELSEIF(D_11.LT.0.00001)THEN
          T0(NOCO)=1.
       ELSE
!          T0(NOCO)=1.-2.*(D01/D_11)
          T0(NOCO)=1.
       ENDIF
!
       IF(NOST.NE.NBST-1) THEN
          T1(NOCO)=1.
       ELSEIF(D02.LT.0.00001)THEN
          T1(NOCO)=1.
       ELSE
          T1(NOCO)=1.
!          T1(NOCO)=1.-2.*(D01/D02)
       ENDIF
!       WRITE(9,*) NOST,NOCO,T0(NOCO),T1(NOCO)
! CONTINUITE
        C0(NOCO)=0.
        C1(NOCO)=0.
! BAIS:
        B0(NOCO)=0.
        B1(NOCO)=0.
!
 100    CONTINUE
!
 999    END
!##############################################################################
      SUBROUTINE CALC_DERIV(NOST,LINZ)
!##############################################################################
! CALCULS DES VECTEURS DERIVEES SUR UNE TRANCHE
!
      implicit none
      INTEGER NOST,NBPTSTI,NBST,NBSTF,
     :LINZ,NOCO
     :,KS1,KP1,KZO,KS2,KP2,KP3
      DOUBLE PRECISION B0,C0,DX0,DY0,DZ0,T0,Z2,Z1,Z0
     :,B1,C1,DX1,DY1,DZ1,T1,X_1
     :,X0,Y0,X1,Y1,y_1,Z_1,X2,Y2
     :,PA0,PB0,PA1,PB1
      PARAMETER (KS1=1000)
      PARAMETER (KP1=1000)
      PARAMETER (KZO=1000)
      PARAMETER (KS2=5000)
      PARAMETER (KP2=3000)
      PARAMETER (KP3=1000)
      DOUBLE PRECISION  STIX(KP2,KS1),STIY(KP2,KS1),STIZ(KP2,KS1)

!
      COMMON/BDDIM/NBST,NBPTSTI,NBSTF
!
!   NBPTSTI*NBST
       COMMON/STI/STIX,STIY,STIZ
!
! BD DES TENSION CONTINUITE BIAIS A DROITE (0) ET GAUCHE (1) DE CHAQUE PT
      COMMON/TCB/ T0(KP2),T1(KP2),C0(KP2),C1(KP2),B0(KP2),B1(KP2)
!
! BD DES TGTES AUX PTS DES STI A DROITE (0) ET GAUCHE (1)
      COMMON/DERIV/DX0(KP2),DY0(KP2),DZ0(KP2),DX1(KP2),DY1(KP2),DZ1(KP2)
!
! POUR TOUTES LES COURBES
      DO 100 NOCO=1,NBPTSTI
! INIT:
       IF(NOST.NE.1) THEN
          X_1=STIX(NOCO,NOST-1)
          Y_1=STIY(NOCO,NOST-1)
          Z_1=STIZ(NOCO,NOST-1)
       ELSE
          X_1=123.456
          Y_1=123.456
          Z_1=123.456
       ENDIF
!
          X0=STIX(NOCO,NOST)
          Y0=STIY(NOCO,NOST)
          Z0=STIZ(NOCO,NOST)
!
          X1=STIX(NOCO,NOST+1)
          Y1=STIY(NOCO,NOST+1)
          Z1=STIZ(NOCO,NOST+1)
!
       IF (NOST.NE.NBST-1) THEN
          X2=STIX(NOCO,NOST+2)
          Y2=STIY(NOCO,NOST+2)
          Z2=STIZ(NOCO,NOST+2)
       ELSE
          X2=123.456
          Y2=123.456
          Z2=123.456
       ENDIF
!
! CALCUL DES PARAMETRES
       PA0=(1.-T0(NOCO))*(1.+C0(NOCO))*(1.+B0(NOCO))/2.
       PB0=(1.-T0(NOCO))*(1.-C0(NOCO))*(1.-B0(NOCO))/2.
!
       PA1=(1.-T1(NOCO))*(1.-C1(NOCO))*(1.+B1(NOCO))/2.
       PB1=(1.-T1(NOCO))*(1.+C1(NOCO))*(1.-B1(NOCO))/2.
!
! VERIF PA0=PB0 ET IDEM 1 (C ET B NULS)
!         IF(PA0.NE.PB0)
!     +      WRITE(9,*) ' =====> BIZARRE, PA0 .NE. PB0',NOCO,NOST
!         IF(PA1.NE.PB1)
!     +      WRITE(9,*) ' =====> BIZARRE, PA0 .NE. PB0',NOCO,NOST
!
! CALCULS DES DERIVEES
       DX0(NOCO)=PA0*(X0-X_1) + PB0*(X1-X0)
       DY0(NOCO)=PA0*(Y0-Y_1) + PB0*(Y1-Y0)
       DZ0(NOCO)=PA0*(Z0-Z_1) + PB0*(Z1-Z0)
!
       DX1(NOCO)=PA1*(X1-X0) + PB1*(X2-X1)
       DY1(NOCO)=PA1*(Y1-Y0) + PB1*(Y2-Y1)
       DZ1(NOCO)=PA1*(Z1-Z0) + PB1*(Z2-Z1)
!
! VERIF SI D0 ET D1 MEME MODULE
!         D0=SQRT(DX0(NOCO)**2+DY0(NOCO)**2+DZ0(NOCO)**2)
!         D1=SQRT(DX1(NOCO)**2+DY1(NOCO)**2+DZ1(NOCO)**2)
!         IF(D0.NE.D1)
!     +      WRITE(9,*) '==> BIZARRE D0.NE.D1',NOCO,NOST,D0,D1
!
! POUR LE CAS OU L ON INTERPOLE Z LINEAIRE, ON PEUR DIRE QUE LA CONTINUITE SUR
! Z EST EGALE A -1, CAD PA0 SUZ Z = 0 ET PB1 SUR Z = 0. DONC:
       IF(LINZ.EQ.1.) THEN
           DZ0(NOCO)=PB0*(Z1-Z0)
           DZ1(NOCO)=PA1*(Z1-Z0)
       ENDIF
!
! VERIF   -> OK
!         IF(DX0(NOCO).EQ.0..AND.DY0(NOCO).EQ.0.)
!     +      WRITE(9,*) '======> BIZARRE, VECT DERIV 0 NUL',NOCO,NOST
!         IF(DX1(NOCO).EQ.0..AND.DY1(NOCO).EQ.0.)
!     +      WRITE(9,*) '======> BIZARRE, VECT DERIV 1 NUL',NOCO,NOST


! REMARQUES: AUX EXTREMITE, LES PARAMETRES T=1 C=0  B=0
! DONNE P 0 OU P 1 NUL LA DERIVE = VECTEUR NUL:
!         IF(NOST.EQ.1) THEN
!            DX0(NOCO)=0.
!            DY0(NOCO)=0.
!            DZ0(NOCO)=0.
!         ENDIF
!
!         IF(NOST.EQ.NBST-1) THEN
!            DX1(NOCO)=0.
!            DY1(NOCO)=0.
!            DZ1(NOCO)=0.
!         ENDIF
!
 100    CONTINUE

!
 999    END
!
!##############################################################################
      SUBROUTINE PT_TRAJ(NST,NCB,T,X,Y,Z)
!##############################################################################
! SUR UNE TRANCHE NST DONNEE, SUR UNE TRAJECTOIRE NCB DONNEE, A UN T DONNE
! CALCULE X,Y,Z EN UTILISANT LA BASE DES TANGEANTES
!
      implicit none
      INTEGER NBPTSTI,NBST,NBSTF
     :,NCB,NST
     :,KS1,KP1,KZO,KS2,KP2,KP3
      DOUBLE PRECISION Z,X,Y
     :,DX0,DY0,DZ0,DX1,DY1,DZ1,T,H2,H3,H4,H1,T2,T3
      PARAMETER (KS1=1000)
      PARAMETER (KP1=1000)
      PARAMETER (KZO=1000)
      PARAMETER (KS2=5000)
      PARAMETER (KP2=3000)
      PARAMETER (KP3=1000)
      DOUBLE PRECISION STIX(KP2,KS1),STIY(KP2,KS1),STIZ(KP2,KS1)
!
      COMMON/BDDIM/NBST,NBPTSTI,NBSTF
!
!   NBPTSTI*NBST
       COMMON/STI/STIX,STIY,STIZ
!
! BD DES TGTES AUX PTS DES STI A DROITE (0) ET GAUCHE (1)
      COMMON/DERIV/DX0(KP2),DY0(KP2),DZ0(KP2),DX1(KP2),DY1(KP2),DZ1(KP2)
!
! PUISSANCE DE T
      T2=T*T
      T3=T2*T
! VALEURS DES POLYNOMES DE HERMITE
      H1=2.*T3-3.*T2+1.
      H2=-2.*T3+3.*T2
      H3=T3-2.*T2+T
      H4=T3-T2
!
! CALCUL DU POINT DE LA TRAJECTOIRE
      X=STIX(NCB,NST)*H1+STIX(NCB,NST+1)*H2+DX0(NCB)*H3+DX1(NCB)*H4
      Y=STIY(NCB,NST)*H1+STIY(NCB,NST+1)*H2+DY0(NCB)*H3+DY1(NCB)*H4
      Z=STIZ(NCB,NST)*H1+STIZ(NCB,NST+1)*H2+DZ0(NCB)*H3+DZ1(NCB)*H4
!
 999  END
!##############################################################################
      SUBROUTINE ST_PLAN
!##############################################################################
! TESTE TOUTES LES ST MESUREE ET VERIFIE SI ELLES SONT PLANES....
      implicit none
      INTEGER NOST,NOPT,NBPTSTI,NBST,NBSTF
     :,KSTPLAN,NBPTST
     :,KS1,KP1,KZO,KS2,KP2,KP3
      DOUBLE PRECISION X,Y,XB,YB,DELTA,XA,YA,D,DY,DX
      PARAMETER (KS1=1000)
      PARAMETER (KP1=1000)
      PARAMETER (KZO=1000)
      PARAMETER (KS2=5000)
      PARAMETER (KP2=3000)
      PARAMETER (KP3=1000)
      DOUBLE PRECISION  STX(KP1,KS1),STY(KP1,KS1),STZ(KP1,KS1)

!
      COMMON/BDDIM/NBST,NBPTSTI,NBSTF
! BD ST  (ORIGINE FICHIER de depart)
!  NBPTST()*NBST -> COTE X Y Z DU POINT
      COMMON/BDST/NBPTST(KS1),STX,STY,STZ
! BD FLAG POUR ST MESUREES PLANES:
      COMMON /BDSTPLAN/ KSTPLAN(KS1)
!
      DELTA=.5
      DO 100 NOST=1,NBST
!        WRITE(9,*) ' SECTION',NOST
      NOPT=1
      XA=STX(NOPT,NOST)
      YA=STY(NOPT,NOST)
      NOPT=NBPTST(NOST)
      XB=STX(NOPT,NOST)
      YB=STY(NOPT,NOST)
      DX=XB-XA
      DY=YB-YA
!
      KSTPLAN(NOST)=1
      DO 110 NOPT=2,NBPTST(NOST)-1
        X=STX(NOPT,NOST)
        Y=STY(NOPT,NOST)
        D=(X-XA)*DY-(Y-YA)*DX
!          WRITE(9,*) NOPT,D
        IF(D.GT.DELTA) KSTPLAN(NOST)=0
! DANGER : PROVISOIRE:
        KSTPLAN(NOST)=0
 110    CONTINUE
!      IF(KSTPLAN(NOST).EQ.0) WRITE(9,*) '#### SECTION ',NOST,' PAS PLANE.'
 100  CONTINUE

      END
!##############################################################################
      SUBROUTINE LECT_FIC(NCA)
! NCA= NO CHANNEL
!##############################################################################
!
      implicit none
      INTEGER NBPTSTI,NBST,NBSTF
     :,N1,N2,N3
     :,KS1,KP1,KZO,KS2,KP2,KP3
     :,NBLGNO,NBHISTO,NCA,NBPTST,M,I,N4,NOLGN
     :,NBPTSEC,LPC,NZONE
     :,J,M0
      DOUBLE PRECISION XMIN,YMIN,ZMIN,XMAX,YMAX,ZMAX,PK,PKF,X1,Y1,Z1
      PARAMETER (KS1=1000)
      PARAMETER (KP1=1000)
      PARAMETER (KZO=1000)
      PARAMETER (KS2=5000)
      PARAMETER (KP2=3000)
      PARAMETER (KP3=1000)

      DOUBLE PRECISION STX(KP1,KS1),STY(KP1,KS1),STZ(KP1,KS1)
! ENTETE DE FICHIER: IE HISTORIQUE DU TRAITEMENT:
      CHARACTER HISTO*80,REP*1
      LOGICAL TTDIR,TTNOM
      COMMON/BDHISTO/HISTO(20)/NBHISTO/NBHISTO

! BD DIM DES STRUCTURES:
      COMMON/BDDIM/NBST,NBPTSTI,NBSTF
! BD ST  (ORIGINE FICHIER)
!  NBPTST()*NBST -> COTE X Y Z DU POINT
      COMMON/BDST/NBPTST(KS1),STX,STY,STZ
      COMMON/BDENTETE/PK(KS1),PKF(KS2)
! BD NOM DES SECTIONS ORIGINE
       CHARACTER*12 NOMST
       COMMON /BDNOMST/NOMST(KS1)
!
      CHARACTER CODLGN*3
      COMMON /BDZO/NZONE(KS1,KZO),NBLGNO,LPC(KZO),CODLGN(KZO)
!
      CHARACTER CODL*3,COD(1000)*3
      CHARACTER*80 LIGNE
!
      XMAX=-1.E10
      YMAX=-1.E10
      ZMAX=-1.E10
      XMIN= 1.E10
      YMIN= 1.E10
      ZMIN= 1.E10
      NBST=1
      NBPTSEC=1
      NOLGN=2
      DO I=1,100
        COD(I)(1:1)='A'
        IF(I-1.LT.10)THEN
          COD(I)(2:2)='0'
          write(COD(I)(3:3),'(I1)')I-1
        ELSE
          write(COD(I)(2:3),'(I2)')I-1
        ENDIF
      ENDDO
      DO I=101,200
        COD(I)(1:1)='B'
        IF(I-101.LT.10)THEN
          COD(I)(2:2)='0'
          write(COD(I)(3:3),'(I1)')I-101
        ELSE
          write(COD(I)(2:3),'(I2)')I-101
        ENDIF
      ENDDO
      DO I=201,300
        COD(I)(1:1)='C'
        IF(I-201.LT.10)THEN
          COD(I)(2:2)='0'
          write(COD(I)(3:3),'(I1)')I-201
        ELSE
          write(COD(I)(2:3),'(I2)')I-201
        ENDIF
      ENDDO
      DO I=301,400
        COD(I)(1:1)='D'
        IF(I-301.LT.10)THEN
          COD(I)(2:2)='0'
          write(COD(I)(3:3),'(I1)')I-301
        ELSE
          write(COD(I)(2:3),'(I2)')I-301
        ENDIF
      ENDDO
      DO I=401,500
        COD(I)(1:1)='E'
        IF(I-401.LT.10)THEN
          COD(I)(2:2)='0'
          write(COD(I)(3:3),'(I1)')I-401
        ELSE
          write(COD(I)(2:3),'(I2)')I-401
        ENDIF
      ENDDO
      DO I=501,600
        COD(I)(1:1)='F'
        IF(I-501.LT.10)THEN
          COD(I)(2:2)='0'
          write(COD(I)(3:3),'(I1)')I-501
        ELSE
          write(COD(I)(2:3),'(I2)')I-501
        ENDIF
      ENDDO
      DO I=601,700
        COD(I)(1:1)='G'
        IF(I-601.LT.10)THEN
          COD(I)(2:2)='0'
          write(COD(I)(3:3),'(I1)')I-601
        ELSE
          write(COD(I)(2:3),'(I2)')I-601
        ENDIF
      ENDDO
      DO I=701,800
        COD(I)(1:1)='H'
        IF(I-701.LT.10)THEN
          COD(I)(2:2)='0'
          write(COD(I)(3:3),'(I1)')I-701
        ELSE
          write(COD(I)(2:3),'(I2)')I-701
        ENDIF
      ENDDO
      DO I=801,900
        COD(I)(1:1)='I'
        IF(I-801.LT.10)THEN
          COD(I)(2:2)='0'
          write(COD(I)(3:3),'(I1)')I-801
        ELSE
          write(COD(I)(2:3),'(I2)')I-801
        ENDIF
      ENDDO
      DO I=901,1000
        COD(I)(1:1)='J'
        IF(I-901.LT.10)THEN
          COD(I)(2:2)='0'
          write(COD(I)(3:3),'(I1)')I-901
        ELSE
          write(COD(I)(2:3),'(I2)')I-901
        ENDIF
      ENDDO
!
! FORMAT DE L HISTORIQUE
 99   FORMAT (A80)
! FORMAT DES ENTETES DES SECTIONS:
 100  format(4(1X,I5),1X,F12.4,1X,A12)
! FORMAT DES POINTS DES SECTIONS+LIGNE DE CODAGE
 101  FORMAT (3(1X,F12.4),1X,A3)
      Write(9,*)'si le fichier a des sections ayant'
      write(9,*)'toutes le meme nombre de points'
      write(9,*)'(fichier de sections de maillage *.m)'
      write(9,*)'tous les points pourront etre'
      write(9,*)'consideres comme directrices'
      write(9,*)'si vous entrez 1'
      write(9,*)'ces directrices seront nommees'
      write(9,*)'A00 A01 etc'
      write(9,*)'si vous entrez 2'
      write(9,*)
      write(9,*)'sinon entrez 0'
      rep = '0'
      !read(*,'(a)')rep
      If(rep.eq.'2')then
       TTDIR=.TRUE.
       TTNOM=.TRUE.
      elseif(rep.eq.'1')then
       TTDIR=.TRUE.
       TTNOM=.FALSE.
      else
        TTDIR=.FALSE.
        TTNOM=.FALSE.
      ENDIF


!
! LECTURE DE L HISTORIQUE DU FICHIER:
      NBHISTO=0
      WRITE(9,*) ' '
      WRITE(9,*) ' HISTORIQUE DU FICHIER :'
 120  READ (NCA,99,END=15) LIGNE
      IF (LIGNE(1:1).EQ.'#') THEN
      NBHISTO=NBHISTO+1
      HISTO(NBHISTO)=LIGNE
      WRITE(9,*) HISTO(NBHISTO)
      GOTO 120
      ENDIF
!
      read(LIGNE,100) N1,N2,N3,N4,pk(nbst),NOMST(NBST)
!      IF(N3.NE.0) WRITE(9,*) '### FICHIER DE SECTIONS INCORRECT'
      N3=0
      J=1
      M0=0
      DO 10 M=1,5000000
      READ(NCA,101,END=15) X1,Y1,Z1,CODL

! cas FIN DE SEGMENT
      IF (ABS(X1-999.999).LT.0.0001) THEN
!         CREE DEUX LIGNES BIDONS: LIGNE 1 SUR 1ER POINT SECTION
!                                  DERNIERE LIGNE SUR DERNIER POINT DE LA SEC.
      NZONE(NBST,1)=1
      NZONE(NBST,NOLGN)=NBPTSEC-1
      NOLGN=NOLGN+1
      IF(NBLGNO.GT.KZO+1) THEN
        WRITE(9,*) ' NOMBRE DE LIGNES TROP GRAND >',KZO
        STOP
      ENDIF
      IF(M-M0.NE.N4+1)THEN
        write(9,*)'attention section ',nbst-1
        write(9,*)'nombre de points lu ',M-M0-1,' different de '
        write(9,*)'nombre de points ',N4,' dans en tete'
      ENDIF
!
! TEST DE LA CONSTANCE DU NOMBRE DE LIGNE (ZONES):
          IF (NBST.EQ.1) THEN
            NBLGNO=NOLGN-1
          ELSE
      IF(.NOT.TTDIR)THEN
        IF (NBLGNO.NE.NOLGN-1) THEN
          WRITE(9,*)
     +    'ERREUR NOMBRE DE ZONES PAS CONSTANT SUR LES SECTIONS',NBST
!           STOP
        ENDIF
! fin du if sur ttdir
           ENDIF
           ENDIF
! RAZ DES COMPTEURS :
      NOLGN=2
!        1 RESERVE A LA BORDURE
      NBPTST(NBST) = NBPTSEC -1
      NBPTSEC=1
      NBST=NBST+1
      IF(NBST.GT.KS1) THEN
        WRITE(9,*) ' NOMBRE DE SECTIONS TROP GRAND >',KS1
        STOP
      ENDIF
! LECTURE ENTETE SECTION SUIVANTE
      read(NCA,100,end=15) N1,N2,N3,N4,pk(nbst),NOMST(NBST)
        N3=0
        j=1
        M0=M
!      IF(N3.NE.0) WRITE(9,*) '### FICHIER DE SECTIONS INCORRECT'
      GOTO 10

      ENDIF
! end cas fin de segment
!
! DECODAGE PT COURANT
      IF (ABS(X1-999.999).GT.0.0001) THEN
      IF (X1.GT.XMAX) XMAX=X1
      IF (Y1.GT.YMAX) YMAX=Y1
      IF (Z1.GT.ZMAX) ZMAX=Z1
      IF (X1.LT.XMIN) XMIN=X1
      IF (Y1.LT.YMIN) YMIN=Y1
      IF (Z1.LT.ZMIN) ZMIN=Z1
      STX(NBPTSEC,NBST) = X1
      STY(NBPTSEC,NBST) = Y1
      STZ(NBPTSEC,NBST) = Z1
      NBPTSEC=NBPTSEC+1
      IF(NBPTSEC.GT.KP1) THEN
        WRITE(9,*) ' NOMBRE DE POINTS PAR  SECTION TROP GRAND >',KP1
        STOP
      ENDIF
      ENDIF
!
! PASSAGE DE ZONE
!c      IF (X1.EQ.-1.) THEN
! si tout point  = directrice
! sauf premier point et dernier
      IF (TTDIR) THEN
        IF(M-M0.NE.1)THEN
         IF(M-M0.NE.N4)THEN
          IF(ABS(X1-999.999).GT.0.0001)THEN
!c pas de test...
!c      IF (NOLGN.NE.INT(Y1)+1) WRITE(9,*) 'BAVURE .... LIGNES NON ORDONNEES'
!c     +    ,NBST
      NZONE(NBST,NOLGN)=NBPTSEC-1
        IF(TTNOM)THEN
          IF(CODL.EQ.'   ')then
            CODL=COD(J)
            j=J+1
          ENDIF
! fin du if sur TTNOM
        ENDIF
      CODLGN(NOLGN)=CODL
      NOLGN=NOLGN+1
! fin des if sur m=1,N4 et fin de section
       ENDIF
       ENDIF
       ENDIF
!c      GOTO 10
! si directrice dans le cas pas TTDIR
      ELSEIF (CODL.NE.'   ') THEN
!c pas de test...
!c      IF (NOLGN.NE.INT(Y1)+1) WRITE(9,*) 'BAVURE .... LIGNES NON ORDONNEES'
!c     +    ,NBST
      NZONE(NBST,NOLGN)=NBPTSEC-1
      CODLGN(NOLGN)=CODL
      NOLGN=NOLGN+1
!c      GOTO 10
      ENDIF
  10  CONTINUE
!
 15   NBST=NBST-1
!
! FIN LECTURE FICHIER
!CCCCCCCCCCCCCCCCCCCC
      WRITE(9,*) ' '
      WRITE(9,*) 'NOMBRE DE SECTIONS: ',NBST
      WRITE(9,*) 'NOMBRE DE LIGNES  : ',NBLGNO-2,'(+2 BORDURES)'
      WRITE(9,*) 'XMIN , XMAX :',XMIN,XMAX
      WRITE(9,*) 'YMIN , YMAX :',YMIN,YMAX
      WRITE(9,*) ' '
      WRITE(9,*) ' CODE DES LIGNES'
      WRITE(9,'(20(1X,I3))') (I,I=1,NBLGNO-2)
      WRITE(9,'(20(1X,A3))') (CODLGN(I),I=2,NBLGNO-1)
      WRITE(9,*) ' '
!
      END
!
!##############################################################################
! SOUROUTINE: ECR_FIC (NCA,VERSION,FICIN,FICOUT)
! NCA= NO CHANNEL
!##############################################################################
      SUBROUTINE ECR_FIC(NCA,VERSION,FICIN,FICOUT)
!
      implicit none
      INTEGER NOST,NOPT,NBPTSTI,NBST,NBSTF,K,I
     :,NBLGN,NBPTZN,MZOP,NBPTZOP
     :,LPC,NZONE,NLDAXE,PTLGN
     :,KS1,KP1,KZO,KS2,KP2,KP3,NBSTI,LINZ,NSTO
     :,NBLGNO,NBHISTO,NCA,NOPER,NOLGN,NOHISTO,NSTPER,NBPER,NSTI
      DOUBLE PRECISION X1,Y1,Z1,H2,H31,H32,VERSION
     :,PK,PKF,CODE,PASM
      PARAMETER (KS1=1000)
      PARAMETER (KP1=1000)
      PARAMETER (KZO=1000)
      PARAMETER (KS2=5000)
      PARAMETER (KP2=3000)
      PARAMETER (KP3=1000)
      DOUBLE PRECISION  STFX(KP2,KS2),STFY(KP2,KS2),
     + STFZ(KP2,KS2),STFOX(KP3,KS2),STFOY(KP3,KS2),
     + STFOZ(KP3,KS2)
!
      CHARACTER FICIN*35, FICOUT*35
      Logical change1
      character *1 tab(52)
!
      CHARACTER VER*5
      CHARACTER LIGNE*10
      CHARACTER TXT1*15,TXT2*15
! ENTETE DE FICHIER: IE HISTORIQUE DU TRAITEMENT:
      CHARACTER HISTO*80
      COMMON/BDHISTO/HISTO(20)/NBHISTO/NBHISTO
      COMMON/HISTO/H2,H31,H32,LINZ,NLDAXE
      COMMON/BDDIM/NBST,NBPTSTI,NBSTF
      COMMON/BDSTF/ NBSTI(KS2),STFX,STFY,
     + STFZ
      COMMON/BDENTETE/PK(KS1),PKF(KS2)
! BD NOM DES SECTIONS ORIGINE
       CHARACTER*12 NOMST,NOMST1
       COMMON /BDNOMST/NOMST(KS1)
! NBLGN:NB LGN PRISE EN COMPTE
      CHARACTER CODLGN*3
      COMMON /BDZO/NZONE(KS1,KZO),NBLGNO,LPC(KZO),CODLGN(KZO)
!
!BD STF APRES OPTIMISATION....
      COMMON/BDSTFO/ STFOX,STFOY,
     + STFOZ
!
! INDICE DES LIGNES DIRECTRICES DANS LE MAILLAGE
      COMMON/BDLGN/PTLGN(0:KZO+1),PASM(KZO)
      COMMON/NBDLGN/NBLGN,NBPTZN(KZO)
!
! MZOP ZONE OPTIMISER=1,
      COMMON/BDOPT/MZOP(KZO),NBPTZOP(KZO)
!
! BD ST OU LES TRAJ SONT PERPENDICULAIRE AU ST (PLAN X,Y)
      COMMON/BDSTPER/NBPER,NSTPER(KS1)
!
      CHARACTER CODL*3
!
! FORMAT DE L HISTORIQUE
 99   format(A80)
! FORMAT DES ENTETES DE SECTIONS
 100  format(4(1X,I5),1X,F12.4,1X,A12)
! FORMAT DES POINTS DES SECTIONS
 101  format (3(1X,F14.6),1X,A3)

! pour tester le nom des sections alhpabet mis dans tab
       Tab(1)='a'
       Tab(2)='b'
       Tab(3)='c'
       Tab(4)='d'
       Tab(5)='e'
       Tab(6)='f'
       Tab(7)='g'
       Tab(8)='h'
       Tab(9)='i'
       Tab(10)='j'
       Tab(11)='k'
       Tab(12)='l'
       Tab(13)='m'
       Tab(14)='n'
       Tab(15)='o'
       Tab(16)='p'
       Tab(17)='q'
       Tab(18)='r'
       Tab(19)='s'
       Tab(20)='t'
       Tab(21)='u'
       Tab(22)='v'
       Tab(23)='w'
       Tab(24)='x'
       Tab(25)='y'
       Tab(26)='z'
       Tab(27)='A'
       Tab(28)='B'
       Tab(29)='C'
       Tab(30)='D'
       Tab(31)='E'
       Tab(32)='F'
       Tab(33)='G'
       Tab(34)='H'
       Tab(35)='I'
       Tab(36)='J'
       Tab(37)='K'
       Tab(38)='L'
       Tab(39)='M'
       Tab(40)='N'
       Tab(41)='O'
       Tab(42)='P'
       Tab(43)='Q'
       Tab(44)='R'
       Tab(45)='S'
       Tab(46)='T'
       Tab(47)='U'
       Tab(48)='V'
       Tab(49)='W'
       Tab(50)='X'
       Tab(51)='Y'
       Tab(52)='Z'

!
!        WRITE(9,*)'LIGNE1'
! ECRITURE DE L HISTORIQUE:
      DO 200 NOHISTO=1,NBHISTO
        WRITE(NCA,99)HISTO (NOHISTO)
 200    CONTINUE
! MISE A JOUR DE L'HISTORIQUE:
!        WRITE(9,*)'LIGNE2'
        WRITE(VER,'(F5.2)')VERSION
        WRITE(NCA,'(A)') '# SECMA V'//VER//' ENTREE: '//FICIN//
     + ' SORTIE: '//FICOUT
!
!        WRITE(LIGNE2,'(2X,30(2X,I1))') (LPC(I),I=2,NBLGNO-1)
!        WRITE(NCA,'(A)')'#  LIGNES UTLISEES :'//LIGNE2
!
!        WRITE(LIGNE2,'(30(1X,I2))') (NBPTZN(I),I=1,NBLGN+1)
!        WRITE(NCA,'(A)')'#  COURBES PAR ZONES:'//LIGNE2
!
!        WRITE(LIGNE2,'(30(2X,I1))') (MZOP(I),I=1,NBLGN+1)
!        WRITE(NCA,'(A)')'#  ZONES OPTIMISÉES :'//LIGNE2
!
!        WRITE(9,*)'LIGNE3'

!          IF ((NBLGN+1).GT.15) THEN
!         WRITE(LIGNE2,'(15(1X,F5.1))') (PASM(I),I=1,15)
!         WRITE(NCA,'(A)')'#  PAS PAR ZONES: '//LIGNE2
!         WRITE(LIGNE2,'(15(1X,F5.1))') (PASM(I),I=16,NBLGN+1)
!         WRITE(NCA,'(A)')'#                   '//LIGNE2
!          ELSE
!         WRITE(LIGNE2,'(15(1X,F5.1))') (PASM(I),I=1,NBLGN+1)
!         WRITE(NCA,'(A)')'#  PAS PAR ZONES: '//LIGNE2
!          ENDIF
!        WRITE(9,*)'LIGNE4'
!
        IF (LINZ.EQ.1) WRITE(NCA,'(A)')'#  Z: INTERPOL LINEAIRE'
!
! TYPE DE CALCUL DES PAS D ESPACE ENTRE SECTION:
! PAS D ESPACE
            WRITE(LIGNE,'(1X,F6.2)') H2
! TYPE DE MAX
          WRITE(VER,'(1X,I3,1X)') NLDAXE
            IF(H31.EQ.0) TXT1='DIRECT. '//VER
            IF(H31.EQ.1) TXT1='BORD 1 '
            IF(H31.EQ.2) TXT1='BORD 2 '
            IF(H32.EQ.0) TXT2='-DIRECT. '//VER
            IF(H32.EQ.1) TXT2='-BORD 1 '
            IF(H32.EQ.2) TXT2='-BORD 2 '
!        WRITE(9,*)'LIGNE5'

          WRITE(NCA,'(A)')'#  D MOY SUR MAX DE '//TXT1//TXT2
     +           //LIGNE
!
!        WRITE(LIGNE2,'(13(1X,I2))') (NSTPER(I),I=1,NBPER)
!        WRITE(NCA,'(A)')'#    COURBES PERPENDI AUX ST:'//LIGNE2

!
! ECRITURE DU MAILLAGE:
!
!        WRITE(9,*)'LIGNE6'

      NSTO=1
      NSTI= -1
      DO 630 NOST=1,NBSTF
       IF(NSTI.EQ.NBSTI(NSTO)) THEN
            NSTO=NSTO+1
            NSTI=0
         ELSE
            NSTI=NSTI+1
         ENDIF
         IF(NSTI.EQ.0) THEN
           NOMST1=NOMST(NSTO)
         ELSE
! NOM DE LA SECTION = MELANGE NOM ST -AVANT-APRES
           NOMST1=NOMST(NSTO)(1:5)//'-'//NOMST(NSTO+1)(1:6)
! on commence à 11 parce que le nom de section peut avoir 12 caractères
           DO K=11,2,-1
           IF(NOMST1(K:K).EQ.' ')THEN
             NOMST1=NOMST1(1:K-1)//NOMST1(K+1:12)//' '
           ENDIF
           ENDDO
           CHANGE1=.TRUE.
           Do I=1,12
! on teste que le premier caractere soit une lettre minuscule ou majuscule
           Do K=1,52
             IF(NOMST1(1:1).EQ.Tab(K))CHANGE1=.FALSE.
           ENDDO
           IF(Change1)THEN
             NOMST1=NOMST1(2:12)//' '
           ENDIF
           EnDdo
         ENDIF
!
! ENTETE DE SECTION
!        WRITE(9,*)'LIGNE7',NOST

!       write(NCA,100) NOST,NSTO,NSTI,NBPTSTI,PKF(NOST)
       write(NCA,100) NOST,NSTO,NSTI,NBPTSTI,PKF(NOST),NOMST1
       DO 640 NOPT=1,NBPTSTI
         X1=STFOX(NOPT,NOST)
         Y1=STFOY(NOPT,NOST)
         Z1=STFOZ(NOPT,NOST)
         CODL='   '
!        WRITE(9,*)'LIGNE8',NOPT
         DO 220 NOLGN=1,NBLGN
            IF(NOPT.EQ.PTLGN(NOLGN)) CODL=CODLGN(NOLGN+1)
 220       CONTINUE
         WRITE (NCA,101) X1,Y1,Z1,CODL
  640   CONTINUE
!
! CODE=0 => ESTIMATEUR DE DERIVEE OSC GENERALISE
! CODE=1 => ESTIMATEUR DE DERIVEE PERPENDICULAIRE ST PLAN X Y
!
      CODE=0.
      DO 650 NOPER=1,NBPER
        IF(NOST.EQ.NSTPER(NOPER)) CODE=1.
650     CONTINUE
! SEPARATEUR +CODE DE TRANSITION ENTRE SECTION
         WRITE (NCA,101) 999.999D0, 999.999D0, CODE,' '
  630   CONTINUE
!
      END
!
!##############################################################################
      SUBROUTINE CALC_ST_PLANE(NOST)
!##############################################################################
! CALCUL DES ST FINALES ENTRE NOST ET NOST+1
! CAS ENTRE DEUX ST MESUREES PLANES => STF PLANE
!(IL Y EN A NBSTI(NOST) ENTRE CES DEUX ST)
!
! PT A ET B = POINT SUR COURBES EXTREMES DEFINISSANT LE PLAN DE COUPE
!
      implicit none
      INTEGER NOST,NBPTSTI,NBST,NBSTF
     :,NOSTI,NT,NOCB,NBSTI
     :,KS1,KP1,KZO,KS2,KP2,KP3,NBT
      DOUBLE PRECISION Z,X,Y,X2,Y2,Z2,X1,Y1,Z1,XA,YA,D,T,T1
     :,T0,Z0,DELTA2,YB,XB,ZB,D0,X0,Y0,DX,DY,D1,ZA
      PARAMETER (KS1=1000)
      PARAMETER (KP1=1000)
      PARAMETER (KZO=1000)
      PARAMETER (KS2=5000)
      PARAMETER (KP2=3000)
      PARAMETER (KP3=1000)
      DOUBLE PRECISION  STFX(KP2,KS2),STFY(KP2,KS2),
     + STFZ(KP2,KS2)
!
! NB DE POINTS DE DISCRETISATION DES COURBES
      PARAMETER (NBT=100000)
!
      COMMON/BDDIM/NBST,NBPTSTI,NBSTF
!
! BD ST  FINALE  NBPTSTI*NBSTF
! NBSTI nb des section inter entre section de depart
      COMMON/BDSTF/ NBSTI(KS2),STFX,STFY,STFZ
!
      DOUBLE PRECISION LCT1(0:NBT),LCT2(0:NBT)
!
! PRELIMINAIRES
!CCCCCCCCCCCCCCC
!
! CALCUL ABCISSE CURVILIGNE SUR LA COURBE  NO 1 => LCT1(...)
      NOCB=1
      LCT1(0)=0.
      T=0.
      CALL PT_TRAJ(NOST,NOCB,T,X1,Y1,Z1)
!
      DO 510 NT=1,NBT
        T=(NT*1.)/(NBT*1.)
        CALL PT_TRAJ(NOST,NOCB,T,X2,Y2,Z2)
!
        D=SQRT((X2-X1)*(X2-X1)+(Y2-Y1)*(Y2-Y1)+(Z2-Z1)*(Z2-Z1))
        LCT1(NT)=LCT1(NT-1)+D
!
        X1=X2
        Y1=Y2
        Z1=Z2
 510  CONTINUE
!
! CALCUL ABCISSE CURVILIGNE SUR LA COURBE  NO NBPTSTI => LCT2(...)
      NOCB=NBPTSTI
      LCT2(0)=0.
      T=0.
      CALL PT_TRAJ(NOST,NOCB,T,X1,Y1,Z1)
!
      DO 512 NT=1,NBT
        T=(NT*1.)/(NBT*1.)
        CALL PT_TRAJ(NOST,NOCB,T,X2,Y2,Z2)
!
        D=SQRT((X2-X1)*(X2-X1)+(Y2-Y1)*(Y2-Y1)+(Z2-Z1)*(Z2-Z1))
        LCT2(NT)=LCT2(NT-1)+D
!
        X1=X2
        Y1=Y2
        Z1=Z2
 512  CONTINUE
!
!
! CEST PARTI: POUR CHAQUE SECTION INTERMEDIAIRE A TROUVER ...
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      DO 520 NOSTI=1,NBSTI(NOST)
      NBSTF=NBSTF+1
        IF(NBSTF.GT.KS2) THEN
          WRITE(9,*) ' NOMBRE DE SECTIONS FINALES TROP GRAND >',KS2
          STOP
        ENDIF
!      WRITE(9,*) 'CALCULS SECTION (PLANE)           ',NBSTF
!
!
! RECHERCHE0 DES POINTS A ET B DEFINISSANT LE PLAN DE COUPE...
!CCCCCCCCCCCCCCCCC
! POINT A RECHERCHE PARAMETRE T CORRESPONDANT AU RATION DE D
      D=(NOSTI*1.)/((NBSTI(NOST)+1)*1.)*LCT1(NBT)
      DO 515 NT=1,NBT
        IF(LCT1(NT).GE.D) GOTO 516
 515    CONTINUE
!  CALCUL DE POINT A
 516    T=(NT*1.)/(NBT*1.)
      NOCB=1
      CALL PT_TRAJ(NOST,NOCB,T,XA,YA,ZA)
!
! POINT B RECHERCHE PARAMETRE T CORRESPONDANT AU RATION DE D
      D=(NOSTI*1.)/((NBSTI(NOST)+1)*1.)*LCT2(NBT)
      DO 518 NT=1,NBT
        IF(LCT2(NT).GE.D) GOTO 519
 518    CONTINUE
! CALCUL DU POUN B
 519    T=(NT*1.)/(NBT*1.)
      NOCB=NBPTSTI
      CALL PT_TRAJ(NOST,NOCB,T,XB,YB,ZB)
!
! MISE EN BOITE DES PTS EXTREMES A ET B
      STFX(1,NBSTF)= XA
      STFY(1,NBSTF)= YA
      STFZ(1,NBSTF)= ZA
      STFX(NBPTSTI,NBSTF)= XB
      STFY(NBPTSTI,NBSTF)= YB
      STFZ(NBPTSTI,NBSTF)= ZB
!
      DX=XB-XA
      DY=YB-YA
!
! RECHERCHE PAR DICHOTOMIE DES PTS SUR LES AUTRES COURBES:
!CCCCCCCCCCCCCCCCC
! EQUATION : (X-XA)DY-(Y-YA)DX=0
! TO ET T1 INDICE DE CHAQUE COTE SUR LA COURBE
      DO 560 NOCB=2,NBPTSTI-1
           T0=0.
           CALL PT_TRAJ(NOST,NOCB,T0,X0,Y0,Z0)
           D0=(X0-XA)*DY-(Y0-YA)*DX
!
           T1=1.
           CALL PT_TRAJ(NOST,NOCB,T1,X1,Y1,Z1)
           D1=(X1-XA)*DY-(Y1-YA)*DX
!
           DELTA2=1.E-6
!            WRITE(9,*) 'INIT:',T0,D0,T1,D1
!
! TEST DE FIN DE RECHERCHE(T0 ET T1 TRES PROCHE):
 570         IF(ABS(T0-T1).LE.DELTA2) GOTO 580
           T=(T0+T1)/2.
           CALL PT_TRAJ(NOST,NOCB,T,X,Y,Z)
           D=(X-XA)*DY-(Y-YA)*DX
!
!            WRITE(9,*) T0,T1,T,D
!         NEW T0 OU T1
           IF (SIGN(1.D0,D).EQ.SIGN(1.D0,D0)) THEN
            T0=T
            D0=D
           ELSE
            T1=T
            D1=D
           ENDIF
           GOTO 570
!
! MISE EN BOITE
 580         STFX(NOCB,NBSTF)= X
           STFY(NOCB,NBSTF)= Y
           STFZ(NOCB,NBSTF)= Z
!
! RECHERCHE SUR COURBE SUIVANTE
 560    CONTINUE

!     PROCHAINE ST INTERMEDAIRE
 520  CONTINUE
      END
!##############################################################################
      SUBROUTINE CALC_ST_TORDU(NOST)
!##############################################################################
! CALCUL DES ST FINALES ENTRE NOST ET NOST+1
! CAS ENTRE DEUX ST MESUREES NON PLANES => STF NON PLANE
!(IL Y EN A NBSTI(NOST) ENTRE CES DEUX ST)
!
      implicit none
      INTEGER NOST,NBPTSTI,NBST,NBSTF
     :,NT,NOSTI,NBSTF2,NBT,NOCB,NBSTI
     :,KS1,KP1,KZO,KS2,KP2,KP3
      DOUBLE PRECISION Z1,X1,Y1,X,Y,Z,X2,Y2,Z2,D,T,PAS
      PARAMETER (KS1=100)
      PARAMETER (KP1=1000)
      PARAMETER (KZO=1000)
      PARAMETER (KS2=5000)
      PARAMETER (KP2=3000)
      PARAMETER (KP3=1000)
      DOUBLE PRECISION  STFX(KP2,KS2),STFY(KP2,KS2),
     + STFZ(KP2,KS2)
!
! NB DE POINTS DE DISCRETISATION DES COURBES
      PARAMETER (NBT=100000)
!
      COMMON/BDDIM/NBST,NBPTSTI,NBSTF
!
! BD ST  FINALE  NBPTSTI*NBSTF
! NBSTI nb des section inter entre section de depart
      COMMON/BDSTF/ NBSTI(KS2),STFX,STFY,
     + STFZ
!
      DOUBLE PRECISION LCT(0:NBT)
!
! CEST PARTI: POUR CHAQUE SECTION INTERMEDIAIRE A TROUVER ...
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!      WRITE(9,*) 'CALCULS COURBE (ST NON PLANE)     '
      DO 520 NOCB=1,NBPTSTI
!
!
! CALCUL ABCISSE CURVILIGNE SUR LA COURBE  NO NOCB => LC(...)
      LCT(0)=0.
      CALL PT_TRAJ(NOST,NOCB,0.D0,X1,Y1,Z1)
!
      DO 510 NT=1,NBT
        T=(NT*1.)/(NBT*1.)
        CALL PT_TRAJ(NOST,NOCB,T,X2,Y2,Z2)
        D=SQRT((X2-X1)*(X2-X1)+(Y2-Y1)*(Y2-Y1)+(Z2-Z1)*(Z2-Z1))
!
        LCT(NT)=LCT(NT-1)+D
        X1=X2
        Y1=Y2
        Z1=Z2
 510  CONTINUE
!
!
! RECHERCHE DES POINTS DES STF SUR COURBE NOCB
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      NBSTF2=NBSTF
      DO 560 NOSTI=1,NBSTI(NOST)
          PAS=LCT(NBT)/((NBSTI(NOST)+1)*1.)
!          IF (PAS.LT.8.) WRITE(9,*)
!     +    ' ATTENTION=====> DISTANCE ENTRE POINT<8 COURBE:',NOCB

! RECHERCHE DU POINT A LA DISTANCE D
      D=(NOSTI*1.)*PAS

      DO 540 NT=1,NBT
        IF(LCT(NT).GE.D) GOTO 550
 540    CONTINUE
 550    T=(NT*1.)/(NBT*1.)
      CALL PT_TRAJ(NOST,NOCB,T,X,Y,Z)
!
! MISE EN BOITE
      NBSTF2=NBSTF2+1
      STFX(NOCB,NBSTF2)= X
      STFY(NOCB,NBSTF2)= Y
      STFZ(NOCB,NBSTF2)= Z
!
! RECHERCHE PT SUR PROCHAINE ST MEME COURBE
 560    CONTINUE

!     PROCHAINE COURBE ....
 520  CONTINUE
      NBSTF=NBSTF+NBSTI(NOST)
      IF(NBSTF.GT.KS2) THEN
          WRITE(9,*) ' NOMBRE DE SECTIONS FINALES TROP GRAND >',KS2
          STOP
      ENDIF
      END
!
!##############################################################################
      SUBROUTINE CALC_ST_LINEAIRE(NOST)
!##############################################################################
! CALCUL DES ST FINALES ENTRE NOST ET NOST+1
! CAS interpolation lineaire
!(IL Y EN A NBSTI(NOST) ENTRE CES DEUX ST)
!
      implicit none
      INTEGER NOST,NBPTSTI,NBST,NBSTF
     :,NOSTI,NBSTI,NOPTI
     :,KS1,KP1,KZO,KS2,KP2,KP3
      DOUBLE PRECISION XA,YA,ZA,YB,XB,ZB,DX,DY,DZ,D
      PARAMETER (KS1=1000)
      PARAMETER (KP1=1000)
      PARAMETER (KZO=1000)
      PARAMETER (KS2=5000)
      PARAMETER (KP2=3000)
      PARAMETER (KP3=1000)
      DOUBLE PRECISION  STFX(KP2,KS2),STFY(KP2,KS2),
     + STFZ(KP2,KS2)
      DOUBLE PRECISION  STIX(KP2,KS1),STIY(KP2,KS1),
     + STIZ(KP2,KS1)
!
      COMMON/BDDIM/NBST,NBPTSTI,NBSTF
      COMMON/STI/STIX,STIY,STIZ
!
! BD ST  FINALE  NBPTSTI*NBSTF
! NBSTI nb des section inter entre section de depart
      COMMON/BDSTF/ NBSTI(KS2),STFX,STFY,STFZ
!
! CEST PARTI: POUR CHAQUE SECTION INTERMEDIAIRE A TROUVER ...
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      DO 520 NOSTI=1,NBSTI(NOST)
        NBSTF=NBSTF+1
          IF(NBSTF.GT.KS2) THEN
            WRITE(9,*) ' NOMBRE DE SECTIONS FINALES TROP GRAND >',KS2
            !PAUSE
            STOP
          ENDIF
! RATIO entre les deux sections initiales
        D=FLOAT(NOSTI)/FLOAT(NBSTI(NOST)+1)
!
        DO 510 NOPTI=1,NBPTSTI
          XB=STIX(NOPTI,NOST+1)
          YB=STIY(NOPTI,NOST+1)
          ZB=STIZ(NOPTI,NOST+1)
          XA=STIX(NOPTI,NOST)
          YA=STIY(NOPTI,NOST)
          ZA=STIZ(NOPTI,NOST)
          DX=XB-XA
          DY=YB-YA
          DZ=ZB-ZA
          STFX(NOPTI,NBSTF)=XA+D*DX
          STFY(NOPTI,NBSTF)=YA+D*DY
          STFZ(NOPTI,NBSTF)=ZA+D*DZ
 510  CONTINUE
!
!     PROCHAINE ST INTERMEDAIRE
 520  CONTINUE
      END
!##############################################################################
      SUBROUTINE CALC_PK(NLDPK)
!##############################################################################
!
! CALCUL DES PK DES SECTIONS DU MAILLAGE
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      implicit none
      INTEGER NOST,NBPTSTI,NBST,NBSTF
     :,NBLGN
     :,NSTDEP,NSTFIN,NLDPK,NSTO,NBSTI,NBPTZN
     :,KS1,KP1,KZO,KS2,KP2,KP3,PTLGN
      DOUBLE PRECISION X0,Y0,X1,Y1,D,DPK,RATIO,PK,PKF,PASM
      PARAMETER (KS1=1000)
      PARAMETER (KP1=1000)
      PARAMETER (KZO=1000)
      PARAMETER (KS2=5000)
      PARAMETER (KP2=3000)
      PARAMETER (KP3=1000)
      DOUBLE PRECISION  STFX(KP2,KS2),STFY(KP2,KS2),
     + STFZ(KP2,KS2),STFOX(KP3,KS2),STFOY(KP3,KS2),
     + STFOZ(KP3,KS2)
!
      COMMON/BDDIM/NBST,NBPTSTI,NBSTF
!
! INDICE DES LIGNES DIRECTRICES DANS LE MAILLAGE
! + NB POINT /ZONE, PAS MOYEN /ST/ZONE,
      COMMON/BDLGN/PTLGN(0:KZO+1),PASM(KZO)
      COMMON/NBDLGN/NBLGN,NBPTZN(KZO)
!
! BD ST  FINALE  NBPTSTI*NBSTF
! NBSTI nb des section inter entre section de depart
      COMMON/BDSTF/ NBSTI(KS2),STFX,STFY,
     + STFZ
!
! BD STF APRES OPTIMISATION....
      COMMON/BDSTFO/ STFOX,STFOY,
     + STFOZ
!
! BD ENTETE DES SECTIONS
! PK: FICHIER ORIGINE, PKF: PK MAILLAGE
      COMMON/BDENTETE/PK(KS1),PKF(KS2)
!
! PROFIL EN LONG:
       INTEGER NOPTLM(KS2)
! DISTANCE POUR PKF
       DOUBLE PRECISION    DM(KS2)
!
      WRITE(9,*) ' CALCULS DES PK DES SECTIONS'
! EXTRACTION DU LIT MINEUR POUR CALCUL DES PK...
!
! on prend la meme pour la decoupe des sections et les pk
!      WRITE(9,*) ' DONNEZ LE NUMERO DE LA LIGNE DIRECTRICE SERVANT '
!      WRITE(9,*) ' D AXE POUR LE CALCUL DES PK: '
!      READ*,NLDPK
!
! CopiE des numeros des points de l'axe sur les sections dans NOPTLM:
      DO 600 NOST=1,NBSTF
            NOPTLM(NOST)=PTLGN(NLDPK)
 600    CONTINUE
!
      NSTDEP=1
      DO 630 NSTO=1,NBST-1
!
! CALCUL DISTANCE SUR MAILLAGE
      NSTFIN=NSTDEP+NBSTI(NSTO)
      D=0.
      DO 620 NOST=NSTDEP,NSTFIN
          X0=STFOX(NOPTLM(NOST),NOST)
          Y0=STFOY(NOPTLM(NOST),NOST)
          X1=STFOX(NOPTLM(NOST+1),NOST+1)
          Y1=STFOY(NOPTLM(NOST+1),NOST+1)
          DM(NOST) = SQRT((X1-X0)*(X1-X0)+(Y1-Y0)*(Y1-Y0))
          D=D+DM(NOST)
 620    CONTINUE
!
      DPK=PK(NSTO+1)-PK(NSTO)
        IF(D.GT.0.0001)THEN
          RATIO=DPK/D
        ELSE
          RATIO=0.
        ENDIF
!      RATIO=DPK/D
! MAJ DES PK
      D=0
      DO 640 NOST=NSTDEP,NSTFIN
        PKF(NOST)=PK(NSTO)+D*RATIO
        D=D+DM(NOST)
        WRITE(9,*) 'ST PK',NOST,PKF(NOST),'RATIO',RATIO, NSTO
     &                    ,NSTDEP,NSTFIN
 640    CONTINUE
!
      NSTDEP=NSTFIN+1
 630  CONTINUE
! LAST SECTION
      PKF(NBSTF)=PK(NBST)
      WRITE(9,*) 'ST PK',NBSTF,PKF(NBSTF)
      END
!
!##############################################################################
      SUBROUTINE OPTIMISATION(LPLAN)
!##############################################################################
! CREE STFO A PARTIR DES STF EN CHOISSISANT LES POINTS DANS LES ZONES
! A OPTIMISER
!
      implicit none
      INTEGER NOST,NOZ,NOPT,NPT,NBPZ,NBPTSTI,NBST,NBSTF,NBM
     :,NPTO,NBIDON
     :,NBLGN,NBPTZN,MZOP,NBPTZOP,NBSTI
     :,KS1,KP1,KZO,KS2,KP2,KP3,PTLGN,PTDEBF,I
     :, nbidon2,ncand,nprec,nplus
!      DOUBLE PRECISION X,Y,XB,YB
      DOUBLE PRECISION Z,X,Y,XB,YB,ZB
     :,PASVRAI,PASM
      LOGICAL LPLAN

      PARAMETER (KS1=1000)
      PARAMETER (KP1=1000)
      PARAMETER (KZO=1000)
      PARAMETER (KS2=5000)
      PARAMETER (KP2=3000)
      PARAMETER (KP3=1000)

      DOUBLE PRECISION STFX(KP2,KS2),STFY(KP2,KS2),
     + STFZ(KP2,KS2),STFOX(KP3,KS2),STFOY(KP3,KS2),
     + STFOZ(KP3,KS2)
      COMMON/BDDIM/NBST,NBPTSTI,NBSTF
      COMMON/BDSTF/ NBSTI(KS2),STFX,STFY,
     + STFZ
!BD STF APRES OPTIMISATION....
      COMMON/BDSTFO/ STFOX,STFOY,
     + STFOZ

! INDICE DES LIGNES DIRECTRICES DANS LE MAILLAGE
! + NB POINT /ZONE, PAS MOYEN /ST/ZONE,
      COMMON/BDLGN/PTLGN(0:KZO+1),PASM(KZO)
      COMMON/NBDLGN/NBLGN,NBPTZN(KZO)
!
! BD POUR MODULE OPTIMISATION (???)
      COMMON/BDOPT/MZOP(KZO),NBPTZOP(KZO)
!
! BD LONGUEUR
      DOUBLE PRECISION LCUM(KP2),LTOT
!      DOUBLE PRECISION LSEG(KP2),LCUM(KP2),LTOT
      DOUBLE PRECISION LCOUR
!
!      WRITE(9,*)  ' DEBUT OPTIMISATION'
!

      IF(PTLGN(NBLGN+1).GT.KP3) THEN
        WRITE(9,*) ' NOMBRE DE POINTS  PAR SECTIONS '//
     +  'FINALES TROP GRAND >',KP3
        STOP
      ENDIF
!
      PTDEBF=1
      DO 100 NOZ=1,NBLGN+1
!
!        WRITE(9,*) ' '
      IF (MZOP(NOZ).NE.1) THEN
! ZONE NON OPTIMISEE => RECOPIE
!       WRITE(9,*) ' ZONE ',NOZ,' PAS D OPTIMISATION'
!         WRITE(9,*) 'NB PT RECOPIE: ', NBPTZN(NOZ)
!         WRITE(9,*) ' DEPUIS', PTDEBF
!         WRITE(9,*) ' SUR', PTLGN(NOZ-1)
       DO 200 NOST=1,NBSTF
         DO 210 NOPT=1,NBPTZN(NOZ)
           STFOX(PTLGN(NOZ-1)+NOPT-1,NOST)=STFX(PTDEBF+NOPT-1,NOST)
           STFOY(PTLGN(NOZ-1)+NOPT-1,NOST)=STFY(PTDEBF+NOPT-1,NOST)
           STFOZ(PTLGN(NOZ-1)+NOPT-1,NOST)=STFZ(PTDEBF+NOPT-1,NOST)
 210       CONTINUE
 200     CONTINUE
       PTDEBF=PTDEBF+NBPTZN(NOZ)
       GOTO 100
      ENDIF
!
! ZONE A OPTIMISER
       WRITE(9,*) ' ZONE ',NOZ,' OPTIMISATION EN COURS '
!       WRITE(9,*) 'NB DE POINTS CALCULES:',NBPTZOP(NOZ)
!       WRITE(9,*) 'NB DE POINTS DEMANDES:',NBPTZN(NOZ)
       WRITE(9,*) 'PAS D ESPACE DEMANDE :',PASM(NOZ)
!
       NBPZ=NBPTZOP(NOZ)
       DO 300 NOST=1,NBSTF
!
! CALCUL DE LA LONGUEUR DE LA SECTION
          LCUM(1)=0
!          DO 310 NOPT= 1,NBPZ
!Cc
!            NPT=PTDEBF-1+NOPT
!            XB=STFX(NPT,NOST)
!            YB=STFY(NPT,NOST)
!c            ZB=STFZ(NPT,NOST)
!            X=STFX(NPT+1,NOST)
!            Y=STFY(NPT+1,NOST)
!c            Z=STFZ(NPT+1,NOST)
!            LSEG(NOPT)=SQRT((X-XB)*(X-XB)+(Y-YB)*(Y-YB))
!c            LSEG(NOPT)=SQRT((X-XB)*(X-XB)+(Y-YB)*(Y-YB)+(Z-ZB)*(Z-ZB))
!          LCUM(NOPT+1)=LCUM(NOPT)+LSEG(NOPT)
! 310        CONTINUE
!          LTOT=LCUM(NBPZ+1)
          DO 310 NOPT= 1,NBPZ
!
            NPT=PTDEBF-1+NOPT
            XB=STFX(PTDEBF,NOST)
            YB=STFY(PTDEBF,NOST)
            X=STFX(NPT+1,NOST)
            Y=STFY(NPT+1,NOST)
            IF(LPLAN)THEN
            LCUM(NOPT+1)=SQRT((X-XB)*(X-XB)+(Y-YB)*(Y-YB))
            ELSE
            ZB=STFZ(NPT,NOST)
            Z=STFZ(NPT+1,NOST)
            LCUM(NOPT+1)=SQRT((X-XB)*(X-XB)+(Y-YB)*(Y-YB)+(Z-ZB)*(Z-ZB))
            ENDIF
!            LSEG(NOPT)=SQRT((X-XB)*(X-XB)+(Y-YB)*(Y-YB)+(Z-ZB)*(Z-ZB))
!          LCUM(NOPT+1)=LCUM(NOPT)+LSEG(NOPT)
 310        CONTINUE
          LTOT=LCUM(NBPZ+1)
!            WRITE(9,*) ' DL',LSEG(NBPZ)-LSEG(1)
!
          IF (LTOT.NE.0) THEN
            NBM=INT(LTOT/PASM(NOZ))
          ELSE
            NBM=0
            PASVRAI=0.
          ENDIF
          IF (NBM.NE.0) THEN
            PASVRAI=LTOT/FLOAT(NBM)
!            PASVRAI=LTOT/(NBM*1.)
          ENDIF
!
!        WRITE(9,*) NOST, ' LONGUEUR DE SECTION:',LTOTc
!          WRITE(9,*) '              NOMBRE DE VRAIES MAILLES',NBM
!          WRITE(9,*) '              PAS REEL                 ',PASVRAI

!
!  LES POINTS BIDON
       IF(NBM.NE.0)THEN
! NBIDON CONTIENT le nombre de dedoublements des points
                NBIDON=NBPTZN(NOZ)-NBM
         NBIDON=NBIDON/NBM
! nbidon 2 continet le nombre de points en plus à dedoubler
         nBIDON2=NBPTZN(NOZ)-NBM*(NBIDON+1)
!         DO 330 NOPT=1,NBIDONc
!           NPT=PTDEBF
!           NPTO=PTLGN(NOZ-1)+NOPT-1
!           STFOX(NPTO,NOST)=STFX(NPT,NOST)
!           STFOY(NPTO,NOST)=STFY(NPT,NOST)
!           STFOZ(NPTO,NOST)=STFZ(NPT,NOST)
! 330       CONTINUE
! SELECTION DES VRAIS POINTS:
       nplus=0
         DO 320 NOPT=0,NBM-2
           LCOUR=Float(NOPT)*PASVRAI
           DO 340 NPT=1,NBPZ
             IF(LCUM(NPT).GE.LCOUR) GOTO 350
 340         CONTINUE
 350         NPT=PTDEBF+NPT-1
!C             NPT=PTDEBF+(NOPT-1)*NDELTA
           NPTO=PTLGN(NOZ-1)+(Nbidon+1)*NOPT+nplus
! si on a redouble une fois de plus nplus a augmente de 1
           STFOX(NPTO,NOST)=STFX(NPT,NOST)
           STFOY(NPTO,NOST)=STFY(NPT,NOST)
           STFOZ(NPTO,NOST)=STFZ(NPT,NOST)
            do I=1,NBIDON
           STFOX(NPTO+I,NOST)=STFX(NPT,NOST)
           STFOY(NPTO+I,NOST)=STFY(NPT,NOST)
           STFOZ(NPTO+I,NOST)=STFZ(NPT,NOST)
            enddo
! de temps en temps, on double un point
            if(nbidon2.gt.0)then
              if(nopt.eq.0)then
!                nplus=nplus+1
                nprec=0
!                i=nbidon+1
!           STFOX(NPTO+I,NOST)=STFX(NPT,NOST)
!           STFOY(NPTO+I,NOST)=STFY(NPT,NOST)
!           STFOZ(NPTO+I,NOST)=STFZ(NPT,NOST)
              else
                ncand=int(float(nopt)*float(nbidon2)/float(nbm))
                if(ncand.GT.nprec)then
                  nprec=ncand
                  nplus=nplus+1
                i=nbidon+1
           STFOX(NPTO+I,NOST)=STFX(NPT,NOST)
           STFOY(NPTO+I,NOST)=STFY(NPT,NOST)
           STFOZ(NPTO+I,NOST)=STFZ(NPT,NOST)
                endif
! fin du if sur nopt=0
            endif
! fin du if sur nbidon2=0
            endif

 320       CONTINUE
! complment par le dernier point avant la fin de la zone
           LCOUR=Float(NBM-1)*PASVRAI
           DO 1340 NPT=1,NBPZ
             IF(LCUM(NPT).GE.LCOUR) GOTO 1350
 1340         CONTINUE
 1350         NPT=PTDEBF+NPT-1
             do npto=(nbidon+1)*(nbm-1)+nplus,NBPTZN(NOZ)-1
           STFOX(NPTO+PTLGN(NOZ-1),NOST)=STFX(NPT,NOST)
           STFOY(NPTO+PTLGN(NOZ-1),NOST)=STFY(NPT,NOST)
           STFOZ(NPTO+PTLGN(NOZ-1),NOST)=STFZ(NPT,NOST)
            enddo
! si nbm=0
         else
          npt=ptdebf
             do npto=0,NBPTZN(NOZ)-1
           STFOX(NPTO+PTLGN(NOZ-1),NOST)=STFX(NPT,NOST)
           STFOY(NPTO+PTLGN(NOZ-1),NOST)=STFY(NPT,NOST)
           STFOZ(NPTO+PTLGN(NOZ-1),NOST)=STFZ(NPT,NOST)
            enddo
        endif
!
 300     CONTINUE
       PTDEBF=PTDEBF+NBPTZOP(NOZ)

 100  CONTINUE
!
! NB POINT PAR STFO :
      NBPTSTI=PTLGN(NBLGN+1)
      WRITE(9,*) 'NOMBRE FINAL DE MAILLES ',NBPTSTI-1
!
! RECOPIE DE LA DERNIERE COURBE...
      DO 800 NOST=1,NBSTF
      STFOX(NBPTSTI,NOST)=STFX(PTDEBF,NOST)
      STFOY(NBPTSTI,NOST)=STFY(PTDEBF,NOST)
      STFOZ(NBPTSTI,NOST)=STFZ(PTDEBF,NOST)
 800  CONTINUE
      END



!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!                                                                             C
! Nom du fichier: MTO2.FOR                   Date: 14/03/90      Auteur:PF    C
!                                                                             C
! But: extraction  du profil en long de la riviere                            C
!      + demande des cote de l'eau et interpol sur toute la riviere           C
!                                                                             C
! V2.2 PRISE EN COMPTE DES IRREGULARITEES                                     C
! v2.3 09 03 92 new codage des points sur sections originsa : 3F12.4 + '*'    C
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
c !     program MTO   !JBF 06/06/02 : transformation de MTO en routine
c       subroutine mto(ficin,ficenv,nobief,ficout)
c !
c       DOUBLE PRECISION VERSION
c       PARAMETER (VERSION=2.31)
c !
c !      CHARACTER FICIN*15,FICOUT*15,FICENV*15
c       CHARACTER FICIN*35,FICOUT*35,FICENV*35
c       CHARACTER LIGNE*80
c       CHARACTER  REP,CODE
c !
c ! ENTETE DE FICHIER: IE HISTORIQUE DU TRAITEMENT:
c       CHARACTER TXT*5
c       CHARACTER HISTO*80
c       COMMON/BDHISTO/HISTO(20)/NBHISTO/NBHISTO
c !
c ! BD ST  (ORIGINE FICHIER maillage NBPT * NBSECTION)
c       REAL STX(150,1000),STY(150,1000),STZ(150,1000)
c       integer NSECAL(1000)
c       INTEGER NBPTST(1000)
c !
c !      BD  des entetes de sections
c       real pk(1000)
c       integer norig(1000),nsti(1000)
c ! NSTI=0 => SEC ORIGINE => Z NEGATIF.
c !
c ! BD PL (1 PT PAR SECTION)
c       INTEGER NOPTLM(1000)
c ! BD COTE D 'EAU PAR SECTION
c       REAL COTEAU(1000)
c       REAL COTEAU1(1000)
c       REAL COTEAU2(1000)
c ! distance des sec inter entre 2 sec origines
c       REAL DIST(80)
c !
c ! FORMAT ENTREE MAILLAGE
c  1    FORMAT(3(1X,F12.4))
c ! FORMAT SORTIE COTEAU
c  2    FORMAT(3(1X,F12.4))
c ! FORMAT SORTIE PL
c  4    FORMAT(3(1X,F12.4),1X,A)
c ! FORMAT DES ENTETES DE SECTIONS DANS LE MAILLAGE
c  3    format(4(1X,I5),1X,F12.4)
c
c       !write(9,*) ' '
c       !write(9,*) '  EXTRACTION DU PROFIL EN LONG '
c       !write(9,*) '+ DEMANDE DE LA COTE DE L EAU'
c       !write(9,*) '+ CALCUL LIGNE D EAU'
c
c ! 21   WRITE(6,20)
c ! 20   FORMAT(//,' NOM DU FICHIER DE MAILLES : ',$)
c !      READ(5,'(A15)') FICIN  !JBF 06/06/02 : transformation de MTO en routine
c !      OPEN(10,FILE=FICIN,STATUS='OLD',ERR=21)
c       OPEN(10,FILE=FICIN,STATUS='OLD')
c !
c !     LECTURE DU FICHIER DES MAILLES
c !======================================
c       ZMAX=-99999.
c       ZMIN= 99999.
c       NBST=1
c       NBPTSEC=1
c !
c  99   FORMAT (A80)
c ! LECTURE DE L HISTORIQUE DU FICHIER:
c       NBHISTO=0
c       !write(9,*) ' '
c       !write(9,*) ' HISTORIQUE DU FICHIER :'
c  40   READ (10,99,END=15) LIGNE
c       IF (LIGNE(1:1).EQ.'#') THEN
c       NBHISTO=NBHISTO+1
c       HISTO(NBHISTO)=LIGNE
c       !write(9,*) HISTO(NBHISTO)
c       GOTO 40
c       ENDIF
c       read(LIGNE,3) N1,norig(nbst),nsti(nbst),N2,pk(nbst)
c !
c  10     READ(10,1,END=15) X1,Y1,Z1
c ! FIN DE SEGMENT
c       IF (X1.EQ.999.999) THEN
c       NBPTST(NBST) = NBPTSEC -1
c       NBST=NBST+1
c         NBPTSEC=1
c         read(10,3,end=15) N1,norig(nbst),nsti(nbst),N2,pk(nbst)
c       ENDIF
c ! PT COURANT
c       IF (X1.NE.999.999) THEN
c         IF (Z1.GT.ZMAX) ZMAX=Z1
c         IF (Z1.LT.ZMIN) ZMIN=Z1
c       STX(NBPTSEC,NBST) = X1
c       STY(NBPTSEC,NBST) = Y1
c       STZ(NBPTSEC,NBST) = Z1
c       NBPTSEC=NBPTSEC+1
c       ENDIF
c !
c       GOTO 10
c !
c  15   NBST=NBST-1
c       CLOSE(10)
c !
c ! VERIFICATION MAILLAGE
c       DO 30 NOST=1,NBST-1
c         IF (NBPTST(NOST).NE.NBPTST(NOST+1)) THEN
c             !write(9,*) 'ERREUR BASE DE DONNEE'
c             !write(9,*) 'STRUCTURE NON REGULIERE ST:',NOST
c             STOP
c         ENDIF
c  30   CONTINUE
c       NBPT=NBPTST(1)
c !
c         !write(9,*) ' '
c       !write(9,*) 'NOMBRE DE SECTIONS: ',NBST
c       !write(9,*) 'NOMBRE DE POINT PAR SECTIONS : ',NBPT
c         !write(9,*) ' '
c !
c !
c ! RECHERCHE DU LIt mineur
c !################################################
c ! TYPE MINIMUM DES 6 VOISINS ....
c !
c       !write(9,*)  ' '
c       !write(9,*) 'RECHERCHE DU LIT MINEUR ...'
c !
c       Z0=99999.9
c       DO 100 NOPT=1,NBPT
c          IF(STZ(NOPT,1).LT.Z0) THEN
c             N0=NOPT
c             Z0=STZ(NOPT,1)
c          ENDIF
c  100  CONTINUE
c !
c       NOPTLM(1)=N0
c !
c ! RECHERCHE DES POINTS DU LIT SUIVANT
c !  (pt sur section suivante à i-3, i, i+3)
c       DO 200 NOST=2,NBST
c !
c         Z0=99999.99
c         !ND=N0-3
c         ND=max(1,N0-3) !jbf 07/06/2002
c         NF=N0+3
c         DO 220 NOPT=ND, NF
c            IF(STZ(NOPT,NOST).LT.Z0)  THEN
c                   N0=NOPT
c               Z0=STZ(NOPT,NOST)
c            ENDIF
c  220    CONTINUE
c         NOPTLM(NOST)=N0
c !       WRITE(9,*) 'SECTION',NOST
c !     WRITE(9,*) 'NO',N0,' X',STX(N0,NOST),' Y',STY(N0,NOST),' Z',STZ(N0,NOST)
c !
c ! SECTION SUIVANTE
c  200  CONTINUE
c       !write(9,*)  ' '
c !
c ! DEMANDE DES COTE DE L'EAU SUR LES SECTIONS DE DEPART...
c !########################################################
c       !write(9,*) 'ENTREE DES RESULTATS DU CALCUL HYDRO:'
c !     WRITE(9,*) 'ENTREE MANUELLE OU EXTRACTION
c !     +  AUTOMATIQUE(A OU a) D UN .ENV ?'
c !        READ(5,'(A)') REP
c       rep='a'  !JBF 06/06/02 : reponse par defaut
c       IF ((REP.NE.'A').AND.(REP.NE.'a')) THEN
c ! MODE MANUEL
c !#################
c           H1=0
c         NBSO=0
c         DO 300 NOST=1,NBST
c           IF (NSTI(NOST).NE.0) GOTO 300
c           !write(9,*) NORIG(NOST),' SECTION',NOST,' PK ',PK(NOST)
c  301        FORMAT(' COTE 1 ',$)
c  302        FORMAT(' COTE 2 ',$)
c             WRITE(6,301)
c             READ(5,'(F6.2)') COTEAU1(NOST)
c             WRITE(6,302)
c             READ(5,'(F6.2)') COTEAU2(NOST)
c           NBSO=NBSO+1
c           NSECAL(NBSO)=NOST
c  300    CONTINUE
c         !write(9,*) 'IL Y A ',NBSO,' SECTIONS ORIGINES'
c       ELSE
c ! MODE AUTOMATIQUE
c !#################
c         H1=1
c ! 310      WRITE(6,312)
c ! 312      FORMAT(//,' NOM DU FICHIER ENVELOPPE : ',$)
c !          READ(5,'(A15)') FICENV   !JBF 06/06/02 : transformation de MTO en routine
c !          OPEN(10,FILE=FICENV,STATUS='OLD',ERR=310)
c           OPEN(10,FILE=FICENV,STATUS='OLD')
c           !write(9,*) ' NUMERO DU BIEF DE DEPART'
c !         READ*,NOBIEF  !passe en parametre
c !
c         NBSO=0
c         DO 320 NOST=1,NBST
c            REWIND 10
c
c ! LECTURE DES  LIGNES D ENTETE DU .ENV
c ! DEPART RECHERCHE:
c            DO 315 I=1,7
c               READ (10,'()',END=397)
c  315       CONTINUE
c ! DEPLACEMENT SUR LE BIEF DE DEPART DU .ENV
c ! FORMAT DES RESULTATS DU .ENV
c  331       FORMAT (1X,I3,1X,I3,1X,F8.2,2X,F7.2,2X,F7.2,4X,F7.2,4X,F7.2)
c  332       FORMAT(I4)
c
c            !write(9,*)  'Deplacement'
c  330       READ (10,332,END=397) NBIEF
c            IF (NBIEF.NE.NOBIEF) GOTO 330
c            BACKSPACE(10)
c !
c !   RECHERCHE DES ST ORIGINE
c            IF (NSTI(NOST).NE.0) GOTO 320
c ! RECHERCHE PK CORRESPONDANT:
c            !write(9,*)  'Recherche'
c            pkf=-99999.
c ! 340       IF ( ( int(PKF).NE.int(PK(NOST)) ).AND.
c !     +          ( int(PKF).NE.int(PK(NOST))+1 ) ) THEN
c  340        IF (abs(pkf-pk(nost))>3.) then  ! pk pas encore trouve
c                READ (10,331,END=398) NBIEF,IS,PKF,ZF,ZM,ZB,COTE
c                GOTO 340
c             ENDIF
c ! MISE EN BOITE COTE 1
c             COTEAU1(NOST)=COTE
c             write(9,*) ' PK trouve : ',pkf,nost,cote
c ! RECHERCHE SI COTE 2
c ! COTE 2 SERT SI IL N Y A QU UNE SECTION ET DEUX COTE D EAU DANS .ENV
c !
c             READ (10,331,END=399) NBIEF,IS,PKF,ZF,ZM,ZB,COTE
c             IF (PKF.EQ.PK(NOST).AND.PKF.NE.PK(NOST+1)) THEN
c                COTEAU2(NOST)=COTE
c !              write(9,*) NORIG(NOST),' ST',NOST,' PK',PK(NOST),'>',
c !     +                COTEAU1(NOST),'/',COTEAU2(NOST)
c              Else
c                COTEAU2(NOST)=COTEAU1(NOST)
c !              write(9,*) NORIG(NOST),' ST',NOST,' PK',PK(NOST),'>',
c !     +                COTEAU1(NOST)
c             ENDIF
c             NBSO=NBSO+1
c             NSECAL(NBSO)=NOST
c
c             cycle
c
c ! GROSSE BAVURE CHEF:
c  397      WRITE(0,*) 'FATALE BAVURE:  PROBLEME LECTURE FICHIER .ENV'
c           CLOSE(10)
c           STOP
c !
c ! BAVURE CHEF:
c  398      WRITE(9,*)
c      &         'SORRY: PAS PK ',PK(NOST),'SECTION',NOST,'DANS LE .ENV'
c           WRITE(9,*)  'ENTRÉE MANUELLE: COTE DE L EAU POUR CE PK ?'
c           WRITE(9,*)
c      &         '0= pas d entree (interpolation avec autres données)'
c           READ*,COTE
c           IF (COTE.NE.0)THEN
c              COTEAU1(NOST)=COTE
c              COTEAU2(NOST)=COTEAU1(NOST)
c              WRITE(9,*) NORIG(NOST),' ST',NOST,' PK',PK(NOST),'>',
c      +              COTEAU1(NOST)
c              NBSO=NBSO+1
c              NSECAL(NBSO)=NOST
c           ENDIF
c
c  399      COTEAU2(NOST)=COTEAU1(NOST)
c !          write(9,*) NORIG(NOST),' ST',NOST,' PK',PK(NOST),'>',
c !     +           COTEAU1(NOST), '399'
c
c  320     CONTINUE
c !
c !
c !
c          CLOSE(10)
c       ENDIF
c
c !
c ! INTERPOLATION POUR CREER LA LIGNE D EAU
c !#########################################
c         !write(9,*) ' '
c       !write(9,*) 'CALCUL DE LA LIGNE D EAU'
c !
c ! PREMIER  POINT
c             COTEAU(1)=COTEAU1(1)
c !C    WRITE(9,*) 'ST COTEAU',1,COTEAU(1)
c !
c       DO 400 NOSTI=1,NBSO-1
c !
c ! CALCUL LONGEUR ENTRE PT DU LM
c         NSTDEP=NSECAL(NOSTI)
c         NBSTI=NSECAL(NOSTI+1)-NSTDEP
c !
c         DTOT=0
c         DO 410 NOS=1,NBSTI
c           NST1=NSTDEP+NOS-1
c           NST2=NST1+1
c           NLM1=NOPTLM(NST1)
c           NLM2=NOPTLM(NST2)
c           X1=STX(NLM1,NST1)
c           X2=STX(NLM2,NST2)
c           Y1=STY(NLM1,NST1)
c           Y2=STY(NLM2,NST2)
c           DIST(NOS)=SQRT((X2-X1)*(X2-X1)+(Y2-Y1)*(Y2-Y1))
c             DTOT=DTOT+DIST(NOS)
c  410    CONTINUE
c !
c ! CALCUL LIGNE D'EAU INTERPOLATION PROVISOIREMENT LINEAIRE (  ...)
c !------------------------------------------------------------
c ! ON BOUCHE LES TROUS DANS COTEAU...
c !
c         Z1=COTEAU2(NSECAL(NOSTI))
c         Z2=COTEAU1(NSECAL(NOSTI+1))
c !
c         DC=0
c         DO 420 NOS=1,NBSTI
c           NST1=NSTDEP+NOS
c           DC=DC+DIST(NOS)
c           T=DC/DTOT
c           COTEAU(NST1)=Z1+T*(Z2-Z1)
c !C        WRITE(9,*) 'ST COTEAU',NST1,COTEAU(NST1)
c 420     CONTINUE
c !
c ! MORCEAU SUIVANT
c  400  CONTINUE
c       !write(9,*) 'fin de calculs de la ligne d eau'
c !
c ! SORTIE DANS FICHIER
c !####################
c ! 831   WRITE(6,830)
c ! 830   FORMAT(//,' NOM DU FICHIER DU PROFIL EN LONG,LIGNE D EAU ',$)
c !       READ(5,'(A15)') FICOUT   !JBF 06/06/02 : transformation de MTO en routine
c !       OPEN(10,FILE=FICOUT,STATUS='NEW',ERR=831)
c        OPEN(10,FILE=FICOUT,STATUS='unknown') !JBF 07/06/2002
c !
c ! ECRITURE DE L HISTORIQUE
c       DO 860 NOHISTO=1,NBHISTO
c         WRITE(10,99) HISTO(NOHISTO)
c  860    CONTINUE
c ! MISE A JOUR DE L'HISTORIQUE:
c         WRITE(TXT,'(F5.2)')VERSION
c         WRITE(10,'(A)') '# MTO  V'//TXT//' ENTREE: '//FICIN//
c      + ' SORTIE: '//FICOUT
c         IF(H1.EQ.0)WRITE(10,'(A)')'#    ENTREE MANUELLE DES COTES'
c         IF(H1.EQ.1)WRITE(10,'(A)')'#    ENTREE AUTOMATIQUE '//
c      + 'DES COTES FICHIER '//FICENV
c         WRITE(10,'(A)')'#    INTERPOLATION LINEAIRE '
c
c ! PL
c !     DO 820 NOST=1,NBST
c !      NOPT=NOPTLM(NOST)
c !C CODAGE ST ORIGINE
c !         CODE=' '
c !      IF (NSTI(NOST).EQ.0.) CODE = '*'
c !         WRITE(10,4) STX(NOPT,NOST),STY(NOPT,NOST),STZ(NOPT,NOST),CODE
c ! 820       CONTINUE
c !        WRITE(10,2) 999.999,999.999,999.999
c !C
c !C L'EAU
c !     DO 840 NOST=1,NBST
c !      NOPT=NOPTLM(NOST)
c !         WRITE(10,2) STX(NOPT,NOST),STY(NOPT,NOST),COTEAU(NOST)
c ! 840       CONTINUE
c !        WRITE(10,2) 999.999,999.999,999.999
c !C
c !modif JBF du 08/11/2001 pour avoir le même format de sortie que le MTO de Rubar
c       do nost=1,nbst-1
c          do k=1,nbptst(nost)-1
c             write(10,'(f9.2)')
c      &              max(0.,coteau(nost)-(stz(k,nost)+stz(k+1,nost))*0.5)
c          enddo
c          write(10,'(f9.2)') 999.99 !,pk(nost)
c       enddo
c       close(10)
c       END
