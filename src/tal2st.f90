module i_Parametres
!==============================================================================
!       Definition des dimensions maximales des tableaux, de constantes
!       numeriques et de divers constantes physiques et mathematiques.
!==============================================================================
   implicit none
! version
   character(len=21), parameter :: version_string = '0.0.1 du 18/09:2019'
! precision des reels (kind)
   integer, parameter :: rdp = kind(1.d0)
   integer, parameter :: sp = kind(1.)
! longueur des noms de fichier sans extension
   integer, parameter :: fnsize = 60
end module i_Parametres


module i_Geometrie
   use i_Parametres, only : rdp, fnsize
   character (len=fnsize) :: basename
   type profil
      real(kind=rdp) :: x, zmoy
      integer :: nbval
      real(kind=rdp) :: y(24), z(24)
      character(len=2) :: tag(24)
   end type profil
   type (profil) :: datageo(1000)
   integer :: nbpro
end module i_Geometrie


program tal2st
! conversion d'un fichier TAL en fichiers ST (1 par bief)
   use i_Parametres, only: fnsize, sp
   implicit none
   ! Variables locales
   character (len=80) :: cmdline, ligne
   character (len=fnsize+4) :: argmnt
   character (len=fnsize+4) :: tal_file, net_file, st_bief, tal_bief, m_bief, option_file
   integer :: iarg, n, k, lTal, lNet, tmp_tal, lSt, io_status, nBief, lOpt
   character (len=180) :: commande
   real(kind=sp) :: dx
   character(len=3) :: Nam, Nav

   ! ouverture des fichiers
   call get_command(cmdline)
   iarg = 1 ; call get_command_argument(iarg,argmnt)
   n = len_trim(argmnt)
   if (n > 0) then
      if (argmnt(n-3:n) /= '.TAL' .and. argmnt(n-3:n) /= '.tal') then
         tal_file = trim(argmnt)//'.tal'
      else
         tal_file = trim(argmnt)
      endif
      k = len_trim(tal_file)
      net_file = tal_file(1:k-4)//'.net'
      open(newunit=lTal,file=trim(tal_file),form='formatted',status='old')
      open(newunit=lNet,file=trim(net_file),form='formatted',status='unknown')
   else
      stop ' >>>> Fichier TAL manquant'
   endif

   ! commande du mailleur
   commande = 'mailleur8 '

   ! lecture et découpage du fichier TAL
   nBief = 0
   do
      read(lTal,'(a)', iostat=io_status) ligne
      if (io_status /= 0) exit
      if (ligne(1:1) == '*') then
         ! commentaire : on passe
         cycle
      elseif (ligne(1:1) == '#') then
         ! nouveau bief -> lecture du pas d'espace
         if (nBief > 0) then
            ! on ferme les fichiers du bief précédent
            close (lSt)
            close (tmp_tal)
            call lireTal(tal_bief)
            call ecrire_ST(st_bief)
            if (dx < 9000.) then
               ! on a besoin de lancer le mailleur
               commande = 'mailleur8 '//trim(st_bief)
               call run_mailleur(commande,nBief,dx)
               ! écriture sur fichier NET
               write(lNet,'(a5,i3.3,1x,a3,1x,a3,1x,a)') 'Bief_',nBief,Nam,Nav,trim(m_bief)
            else
               !pas besoin de mailler -> écriture directe sur fichier NET
               write(lNet,'(a5,i3.3,1x,a3,1x,a3,1x,a)') 'Bief_',nBief,Nam,Nav,trim(st_bief)
            endif
         endif
         read(ligne(9:),'(f7.2)') dx
         Nam = ligne(3:5)
         Nav = ligne(6:8)
         nBief = nBief + 1
         write(st_bief,'(a,i3.3,a)') 'net/bief_',nBief,'.st'
         write(m_bief,'(a,i3.3,a)') 'net/bief_',nBief,'.M'
         write(tal_bief,'(a,i3.3,a)') 'net/bief_',nBief,'.tal'
         open(newunit=lSt,file=trim(st_bief),form='formatted',status='unknown')
         open(newunit=tmp_tal,file=trim(tal_bief),form='formatted',status='unknown')
      else
         ! ligne normale : on la recopie sur tal_file
         write(tmp_tal,'(a)') trim(ligne)
      endif
   enddo
   ! on convertit le dernier bief
   close (lSt) ; close (tmp_tal)
   call lireTal(tal_bief)
   call ecrire_ST(st_bief)
   if (dx < 9000.) then
      ! on a besoin de lancer le mailleur
      commande = 'mailleur8 '//trim(st_bief)
      call run_mailleur(commande,nBief,dx)
      ! écriture sur fichier NET
      write(lNet,'(a5,i3.3,1x,a3,1x,a3,1x,a)') 'Bief_',nBief,Nam,Nav,trim(m_bief)
   else
      !pas besoin de mailler -> écriture directe sur fichier NET
      write(lNet,'(a5,i3.3,1x,a3,1x,a3,1x,a)') 'Bief_',nBief,Nam,Nav,trim(st_bief)
   endif
   ! on ferme tous les fichiers ouverts
   close (lTal) ; close (lNet)
end program tal2st

subroutine run_mailleur (commande,nBief,dx)
! lancement du mailleur après avoir généré le fichier d'options
   use i_Parametres, only: fnsize, sp
   implicit none
   !  prototype
   character(len=*), intent(in) :: commande
   integer, intent(in) :: nBief
   real(kind=sp), intent(in) :: dx
   ! variables locales
   character(len=fnsize) :: option_file
   integer :: lOpt

   write(option_file,'(a,i3.3,a)') 'net/bief_',nBief,'_options_mailleur'
   open(newunit=lOpt,file=trim(option_file),form='formatted',status='unknown')
   write(lOpt,'(a)') '* fichier généré par Tal2ST'
   write(lOpt,'(a)') '*Taille de la maille moyenne'
   write(lOpt,'(a,f8.3)') 'tmoy = ',dx
   write(lOpt,'(a)') ' '
   write(lOpt,'(a)') '* pour plus de sections entrez 1'
   write(lOpt,'(a)') '*   le calcul se fera sur la ligne de longueur maximale'
   write(lOpt,'(a)') '* pour moins de sections entrez 2'
   write(lOpt,'(a)') '*   le calcul se fera sur la ligne de longueur minimale'
   write(lOpt,'(a)') 'ilongmi = 2'
   write(lOpt,'(a)') '*'
   close (lOpt)
   call execute_command_line(trim(commande))
end subroutine run_mailleur

subroutine LireTAL(bief_tal)
! lecture d'un fichier TAL monobief
   use i_Parametres, only: rdp, fnsize, sp
   use i_geometrie, only : datageo, nbpro
   implicit none
   ! Prototype
   character(len=fnsize+4),intent(in)  :: bief_tal
   ! Variables locales
   integer :: lu, io_status=0, npro, nl, nlmax = 2, npt, i
   character(len=80) :: ligne
   real(kind=rdp) :: z1, z2, largeur

   write(*,'(2a)') ' Lecture du fichier : ',trim(bief_tal)
   open(newunit=lu, file=trim(bief_tal), form='formatted', status='old')
   npro = 0 ; nl = 0
   do
      read(lu,'(a)', iostat=io_status) ligne
      if (io_status /= 0) exit
      if (ligne(1:1) == '*') then
         !commentaire : on passe
         cycle
      elseif (ligne(1:1) == '#') then
         !ligne de titre du bief : on passe (traitée lors du découpage du fichier TAL)
         nl = 0
         cycle
      elseif (nl == 0) then  !nouveau profil / ligne d'abscisses
         npro =  npro + 1
         datageo(npro)%tag(1:24) = '  '
         npt = len_trim(ligne)/6 - 1
         if (ligne(1:1) == 'R') then  !en fait c'est un rectangle
            read(ligne(2:),'(2f6.0)') datageo(npro)%x, largeur
            read(lu,'(a)', iostat=io_status) ligne
            if (io_status /= 0) then
               stop 001
            else
               read(ligne(2:), '(6x,2f6.0)') z1, z2
            endif
            ! NOTE: il faut des lignes directrices pour un profil rectangulaire sinon le mailleur ne conserve pas les angles droits
            datageo(npro)%nbval = 4
            datageo(npro)%y(1) = -0.5_rdp * largeur
            datageo(npro)%z(1) = z2
            datageo(npro)%tag(1) = 'BG'
            datageo(npro)%y(2) = -0.5_rdp * largeur
            datageo(npro)%z(2) = z1
            datageo(npro)%tag(1) = 'FG'
            datageo(npro)%y(3) = +0.5_rdp * largeur
            datageo(npro)%z(3) = z1
            datageo(npro)%tag(1) = 'FD'
            datageo(npro)%y(4) = +0.5_rdp * largeur
            datageo(npro)%z(4) = z2
            datageo(npro)%tag(1) = 'BD'
            cycle
         else
            if (ligne(1:1) == '+') then
               nlmax = 4
               if (npt < 12) stop 301
            else
               nlmax = 2
            endif
            !write(*,*) '>>>> ',ligne(1:1),nlmax
            read(ligne(2:),'(13f6.0)') datageo(npro)%x,(datageo(npro)%y(i),i=1,npt)
            datageo(npro)%nbval = npt
            nl = 1
         endif
      elseif (nl == 1) then
         npt = len_trim(ligne)/6 -1
         if (nlmax == 2) then  ! ligne de cotes
            read(ligne(2:),'(13f6.0)') datageo(npro)%zmoy,(datageo(npro)%z(i),i=1,npt)
            if (ligne(2:7) == '      ') datageo(npro)%zmoy = 9999.999_sp
            nl = 0
         else                  ! ligne d'abscisse (2ème)
            read(ligne(8:),'(12f6.0)') (datageo(npro)%y(i),i=datageo(npro)%nbval+1,datageo(npro)%nbval+npt)
            nl = 2
         endif
      elseif (nl == 2) then
         npt = len_trim(ligne)/6 -1
         if (nlmax == 2) then
            stop 302
         else                  ! ligne de cotes (1ère)
            read(ligne(2:),'(13f6.0)') datageo(npro)%zmoy,(datageo(npro)%z(i),i=1,npt)
            if (ligne(2:7) == '      ') datageo(npro)%zmoy = 9999.999_sp
         endif
         nl = 3
      elseif (nl == 3) then
         npt = len_trim(ligne)/6 -1
         if (nlmax == 2) then
            stop 303
         else                  ! ligne de cotes (2ème)
            read(ligne(8:),'(12f6.0)') (datageo(npro)%z(i),i=datageo(npro)%nbval+1,datageo(npro)%nbval+npt)
         endif
         datageo(npro)%nbval = datageo(npro)%nbval+npt
         nl = 0
         !write(*,*) 'nbval = ',datageo(npro)%nbval, npro
      endif
   enddo
   close(lu)
   nbpro = npro
end subroutine LireTAL


subroutine ecrire_ST(bief_st)
! écriture d'un fichier ST à partir d'une collection de profils Y-Z
   use i_Parametres, only: rdp, fnsize, sp
   use i_geometrie, only : datageo, nbpro
   implicit none
   ! Prototype
   character(len=fnsize+4),intent(in)  :: bief_st
   ! Variables locales
   integer, parameter :: zero = 0
   integer :: n, k, lu
   logical :: rg, rd
   real(kind=sp), parameter :: end_mark = 999.999
   real(kind=rdp) :: pente

   write(*,'(2a)') ' Écriture du fichier : ',trim(bief_st)
   open(newunit=lu, file=trim(bief_st), form='formatted', status='unknown')

   ! définition des lignes directrices RG et RD
   do n = 1, nbpro
      rg = .true.  ;  rd = .true.
      do k = 1, datageo(n)%nbval
         pente = datageo(n)%z(k) - datageo(n)%z(k+1)
         if (rg .and. datageo(n)%z(k) == datageo(n)%zmoy .and. pente > 0._rdp) then
            datageo(n)%tag(k) = 'RG'
            rg = .false.
         elseif (rg .and. datageo(n)%z(k) > datageo(n)%zmoy .and. datageo(n)%z(k+1) < datageo(n)%zmoy) then
            if (datageo(n)%z(k) - datageo(n)%zmoy < datageo(n)%zmoy - datageo(n)%z(k+1)) then
               datageo(n)%tag(k) = 'RG'
            else
               datageo(n)%tag(k+1) = 'RG'
            endif
            rg = .false.
         elseif (rd .and. datageo(n)%z(k) == datageo(n)%zmoy .and. pente < 0._rdp) then
            datageo(n)%tag(k) = 'RD'
            rd = .false.
         elseif (rd .and. datageo(n)%z(k) < datageo(n)%zmoy .and. datageo(n)%z(k+1) > datageo(n)%zmoy) then
            if (datageo(n)%z(k+1) - datageo(n)%zmoy > datageo(n)%zmoy - datageo(n)%z(k)) then
               datageo(n)%tag(k) = 'RD'
            else
               datageo(n)%tag(k+1) = 'RD'
            endif
            rd = .false.
         else
            !datageo(n)%tag(k) = '  '
         endif
      enddo
   enddo

   ! écriture
   do n = 1, nbpro
      write(lu,'(4i6,f13.4)') n, zero, zero, datageo(n)%nbval, datageo(n)%x
      do k = 1, datageo(n)%nbval
         write(lu,'(3f13.4,1x,a)') datageo(n)%x, datageo(n)%y(k), datageo(n)%z(k), datageo(n)%tag(k)
      enddo
      write(lu,'(3f13.4)') end_mark, end_mark, end_mark
   enddo
   close (lu)
end subroutine ecrire_ST
