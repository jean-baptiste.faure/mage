from mage_extraire import data
from PyQt5.QtWidgets import QWidget, QApplication, QSlider, QHBoxLayout, QStyle, QFileDialog, QPushButton, QVBoxLayout, QLabel
import sys
from PyQt5.QtCore import Qt, QPropertyAnimation, QTimer
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg, NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure

class MplCanvas(FigureCanvasQTAgg):

    def __init__(self, parent=None, width=500, height=400, dpi=100):
        self.fig = Figure(figsize=(width, height), dpi=dpi)
        #spec = matplotlib.gridspec.GridSpec(ncols=1, nrows=1,
                         #width_ratios=[1], wspace=0.5,
                         #hspace=0.5, height_ratios=[1])
        #self.axes1 = self.fig.add_subplot(spec[0])
        self.axes1 = self.fig.add_subplot()
        super(MplCanvas, self).__init__(self.fig)

class Window(QWidget):
    def __init__(self, ifilename):
        super().__init__()

        self.setWindowTitle("Slider tick settings")
        self.setGeometry(350, 100, 720, 700)
        self.paused = True

        self.res = []
        self.lines = []

        for name in ifilename:
            self.res.append(data(name))

        self.nt = min([len(r.values['Z'].time) for r in self.res])
        self.t  = self.res[0].values['Z'].time[0:self.nt]

        self.init_ui()

        self.show()

    def init_ui(self):
        self.slider = QSlider(Qt.Horizontal)
        self.slider.setMinimum(1)
        self.slider.setMaximum(self.nt)
        self.slider.setValue(1)
        #self.slider.setTickPosition(QSlider.TicksBothSides)
        #self.slider.setTickPosition(QSlider.TicksBelow)
        self.slider.setTickInterval(1)
        self.playBtn = QPushButton("Play")
        self.nextBtn = QPushButton("Next")
        self.prevBtn = QPushButton("Prev")
        self.label = QLabel("Timestep 0")
        self.sc = MplCanvas()
        self.init_fig()
        self.qhbox = QHBoxLayout()
        self.qhbox2 = QHBoxLayout()
        self.qhbox3 = QHBoxLayout()
        self.qvbox = QVBoxLayout()
        self.qhbox.addWidget(self.sc)
        self.qhbox3.addWidget(self.slider)
        self.qhbox2.addWidget(self.playBtn)
        self.qhbox2.addWidget(self.prevBtn)
        self.qhbox2.addWidget(self.nextBtn)
        self.qhbox2.addWidget(self.label)
        self.qvbox.addLayout(self.qhbox)
        self.qvbox.addLayout(self.qhbox3)
        self.qvbox.addLayout(self.qhbox2)
        self.setLayout(self.qvbox)
        self.slider.valueChanged.connect(self.update_label)
        self.slider.valueChanged.connect(self.update_fig)
        self.playBtn.clicked.connect(self.pause)
        self.prevBtn.clicked.connect(self.prev)
        self.nextBtn.clicked.connect(self.next)
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.update_slider)

    def update_label(self):
        self.label.setText("Timestep "+str(self.slider.value()))

    def update_slider(self):
        if self.slider.value() == self.slider.maximum():
            self.slider.setValue(self.slider.minimum())
        else:
            self.slider.setValue(self.slider.value()+1)

    def next(self):
        self.slider.setValue(self.slider.value()+1)

    def prev(self):
        self.slider.setValue(self.slider.value()-1)

    def pause(self):
        if self.paused:
            self.paused = False
            self.playBtn.setText("Pause")
            self.timer.start(50)
        else:
            self.paused = True
            self.playBtn.setText("Play")
            self.timer.stop()

    #def init_figure(self):
        #mm = 0
        #jj = 0
        #for j in range(self.nt):
            ##m = max(max([r.values['Z'].values[j] for r in self.res]))
            #m = max(self.res[0].values['Z'].values[j])
            #if m > mm:
                #jj = j
                #mm = m

        #line, = plt.plot(self.res[0].xgeo, self.res[0].values['Z'].values[jj])
        #line.set_alpha(0.0)
        #for r in self.res:
            #self.lines.append(plt.plot([], []))
        #if self.res[0].mage_version > 80:
            #line3 = plt.plot(self.res[0].xgeo, self.res[0].zfd)
        #else:
            #line3 = plt.plot(self.res[0].xgeo, self.res[0].zfd + self.res[0].zmin)
        #self.sc.tight_layout()

    def init_fig(self):
        mm = 0
        jj = 0
        for j in range(self.nt):
            #m = max(max([r.values['Z'].values[j] for r in self.res]))
            m = max(self.res[0].values['Z'].values[j])
            if m > mm:
                jj = j
                mm = m

        line, = self.sc.axes1.plot(self.res[0].xgeo, self.res[0].values['Z'].values[jj])
        line.set_alpha(0.0)
        for r in self.res:
            self.lines.append(self.sc.axes1.plot([], []))
        if self.res[0].mage_version > 80:
            line3 = self.sc.axes1.plot(self.res[0].xgeo, self.res[0].zfd, '--')
        else:
            line3 = self.sc.axes1.plot(self.res[0].xgeo, self.res[0].zfd + self.res[0].zmin, '--')
        #self.sc.fig.tight_layout()
        self.update_fig()

    def update_fig(self):
        i = self.slider.value()-1
        self.sc.axes1.set_title("Temps " + str(self.t[i]) + " s" )
        for j in range(len(self.lines)):
            self.lines[j][0].set_data(self.res[j].xgeo, self.res[j].values['Z'].values[i])
        self.sc.draw_idle()

if __name__ == "__main__":
    ifilename = []
    if len(sys.argv) <= 1:
        print("ERROR: no input file name given")
        exit()
    else:
        for name in sys.argv[1:]:
            ifilename.append(str(name))

    app = QApplication(sys.argv)
    Window = Window(ifilename)
    sys.exit(app.exec_())
