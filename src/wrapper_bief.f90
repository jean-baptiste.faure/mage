!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - INRAE - 1989-2020                           #
!# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################

module wrapper_bief
  use objet_bief
  use iso_c_binding, only: c_int, c_char, C_NULL_CHAR, c_double, c_bool

  implicit none

  type(bief), target :: mon_bief
  type(bief), dimension(:), allocatable, target, private :: saved_bief
  integer, private :: saved_bief_current_index

  !======!
  contains
  !======!

  subroutine char_to_string(fname, cname)
  !====================================================!
  ! converts a string to an array of size 1 characters !
  !====================================================!
    ! prototype
    character(len=*), intent(in) :: fname
    character(kind=c_char), dimension(len(fname)+1) ,intent(out) :: cname
    ! variables locales
    integer :: l, i

    l=0
    cname = ''
    do while (l < len(fname))
       l = l + 1
       cname(l)=fname(l:l)
    end do
    cname(l+1) = C_NULL_CHAR
  end subroutine char_to_string

  subroutine string_to_char(cname, fname)
  !====================================================!
  ! converts an array of size 1 characters to a string !
  !====================================================!
    ! prototype
    character(kind=c_char), dimension(*) ,intent(in) :: cname
    character(len=*), intent(inout) :: fname
    ! variables locales
    integer :: l, i

    l=0
    fname = ''
    do while (cname(l+1) .ne. C_NULL_CHAR .and. l < len(fname))
       l = l + 1
       fname(l:l)=cname(l)
    end do
  end subroutine string_to_char

  subroutine c_get_bief_name(name) bind(c)
    ! prototype
    character(kind=c_char), dimension(*) ,intent(out) :: name
    ! variables locales
    character(len=lbief) :: fname

    call mon_bief%get_name(fname)

    call char_to_string(fname, name)
  end subroutine c_get_bief_name

  subroutine c_set_bief_name(name) bind(c)
    ! prototype
    character(kind=c_char), dimension(*) ,intent(in) :: name
    ! variables locales
    character(len=lbief) :: fname

    call string_to_char(name, fname)

    call mon_bief%set_name(fname)
  end subroutine c_set_bief_name

  subroutine c_init_bief_from_geo_file(fichier_geo, with_charriage, with_water) bind(c)
    ! prototype
    character(kind=c_char), dimension(*), intent(in) :: fichier_geo
    integer(kind=c_int), intent(in), optional :: with_charriage
    integer(kind=c_int), intent(in), optional :: with_water
    ! variables locales
    character(len=lname) :: fname

    call string_to_char(fichier_geo, fname)

    call mon_bief%init(fname, with_charriage, with_water)
    saved_bief = [mon_bief]
    saved_bief_current_index = 1
  end subroutine c_init_bief_from_geo_file

  subroutine reset_bief() bind(c)
    if(allocated(saved_bief))then
      mon_bief = saved_bief(1)
      mon_bief%sections_ptr(1:) => mon_bief%sections(1:)
    endif
  end subroutine reset_bief

  subroutine save_bief() bind(c)
    saved_bief = [saved_bief(:saved_bief_current_index),mon_bief]
    saved_bief_current_index = saved_bief_current_index + 1
  end subroutine save_bief

  subroutine undo_bief() bind(c)
    if(saved_bief_current_index > 1)then
      mon_bief = saved_bief(saved_bief_current_index-1)
      saved_bief_current_index = saved_bief_current_index - 1
      mon_bief%sections_ptr(1:) => mon_bief%sections(1:)
    endif
  end subroutine undo_bief

  subroutine c_get_line_tag(i, tag) bind(c)
    ! prototype
    integer(kind=c_int), intent(in) :: i
    character(kind=c_char), dimension(*), intent(out) :: tag
    ! variables locales
    character(len=3) :: fname

    call mon_bief%get_line_tag(i, fname)

    call char_to_string(trim(fname), tag)
  end subroutine c_get_line_tag

  subroutine c_get_nb_lines(nb_lines) bind(c)
    integer(kind=c_int), intent(out) :: nb_lines

    call mon_bief%get_nb_lines(nb_lines)
  end subroutine c_get_nb_lines

  subroutine c_get_nb_sections(nb_sections) bind(c)
    integer(kind=c_int), intent(out) :: nb_sections

    nb_sections = mon_bief%nprof
  end subroutine c_get_nb_sections

  subroutine c_get_pk_section(section, pk) bind(c)
    integer(kind=c_int), intent(in) :: section
    real(kind=c_double), intent(out) :: pk

    if (section < 0 .or. section > mon_bief%nprof)then
      write(error_unit,*) '>>>> Erreur get_pk_section : numero de section hors limites'
      return
    else
      pk = mon_bief%sections_ptr(section)%pk
    endif
  end subroutine c_get_pk_section

  subroutine c_get_nb_points_section(section, nb_points) bind(c)
    integer(kind=c_int), intent(in) :: section
    integer(kind=c_int), intent(out) :: nb_points

    if (section < 0 .or. section > mon_bief%nprof)then
      nb_points = 0
    else
      nb_points = mon_bief%sections_ptr(section)%np
    endif
  end subroutine c_get_nb_points_section

  subroutine c_get_max_3D_length(length) bind(c)
    real(kind=c_double), intent(out) :: length

    length = mon_bief%get_max_3D_length()
  end subroutine c_get_max_3D_length

  subroutine c_get_min_3D_length(length) bind(c)
    real(kind=c_double), intent(out) :: length

    length = mon_bief%get_min_3D_length()
  end subroutine c_get_min_3D_length

  subroutine c_get_mean_3D_length(length) bind(c)
    real(kind=c_double), intent(out) :: length

    length = mon_bief%get_mean_3D_length()
  end subroutine c_get_mean_3D_length

  subroutine c_get_max_2D_length(length) bind(c)
    real(kind=c_double), intent(out) :: length

    length = mon_bief%get_max_2D_length()
  end subroutine c_get_max_2D_length

  subroutine c_get_min_2D_length(length) bind(c)
    real(kind=c_double), intent(out) :: length

    length = mon_bief%get_min_2D_length()
  end subroutine c_get_min_2D_length

  subroutine c_get_mean_2D_length(length) bind(c)
    real(kind=c_double), intent(out) :: length

    length = mon_bief%get_mean_2D_length()
  end subroutine c_get_mean_2D_length

  subroutine c_interpolate_profils_pas_transversal(limite1, limite2, directrice1, directrice2, pas, lplan, lm, lineaire) bind(c)
    ! prototype
    integer(kind=c_int), intent(in) :: limite1, limite2
    real(kind=c_double), intent(in) :: pas
    integer(kind=c_int), intent(in), optional :: lm
    logical(kind=c_bool), intent(in), optional :: lplan, lineaire
    character(kind=c_char), dimension(*), intent(in) :: directrice1, directrice2
    ! variables locales
    character(len=3), dimension(1) :: fdirectrice1, fdirectrice2
    integer, dimension(2) :: limites
    logical :: flplan, flineaire
    integer, dimension(1) :: flm

    if(limite1 > limite2)then
      limites(1) = limite2
      limites(2) = limite1
    else
      limites(1) = limite1
      limites(2) = limite2
    endif

    flm(1)=3 ! moyenne des deux
    if(present(lm)) flm(1)=lm

    call string_to_char(directrice1, fdirectrice1(1))
    call string_to_char(directrice2, fdirectrice2(1))
    if(trim(fdirectrice1(1)) .eq. '' .or. trim(fdirectrice2(1)) .eq. '')then
      write(error_unit,*) '>>>> Erreur interpolate_profils_pas_transversal : nom de directrice absent'
      return
    endif
    if(limites(1) < 1)then
      limites(1) = 1
    else if(limites(1) > mon_bief%nprof)then
      limites(1) = mon_bief%nprof
    endif
    if(limites(2) < 1)then
      limites(2) = 1
    else if(limites(2) > mon_bief%nprof)then
      limites(2) = mon_bief%nprof
    endif
    if(present(lplan))then
      flplan = lplan
    else
      flplan = .false.
    endif
    if(present(lineaire))then
      flineaire = lineaire
    else
      flineaire = .false.
    endif

    call mon_bief%interpolate_profils_pas_transversal(limites, fdirectrice2, fdirectrice1, pas, flplan, flm, flineaire)
  end subroutine c_interpolate_profils_pas_transversal

  subroutine c_st_to_m_compl(npoints, tag1, tag2) bind(c)
    ! prototype
    integer(kind=c_int), intent(in) :: npoints
    character(kind=c_char), dimension(*), intent(in), optional :: tag1, tag2
    ! variables locales
    character(len=3) :: ftag1, ftag2
    type(bief) :: bief_tmp
    if(present(tag1))then
      call string_to_char(tag1, ftag1)
    else
      ftag1 = '   '
    endif
    if(present(tag2))then
      call string_to_char(tag2, ftag2)
    else
      ftag2 = '   '
    endif

    if(present(tag1) .or. present(tag2)) then
      call mon_bief%extract(bief_tmp, ftag1, ftag2)
      call bief_tmp%st_to_m_compl(npoints)
      call mon_bief%patch(bief_tmp, ftag1, ftag2)
    else
      call mon_bief%st_to_m_compl(npoints)
    endif
  end subroutine c_st_to_m_compl

  subroutine c_st_to_m_pasm(pasm, l, lplan, tag1, tag2) bind(c)
    ! prototype
    real(kind=c_double), intent(in) :: pasm, l
    logical(kind=c_bool), intent(in) :: lplan
    character(kind=c_char), dimension(*), intent(in), optional :: tag1, tag2
    ! variables locales
    logical :: flplan
    character(len=3) :: ftag1, ftag2
    type(bief) :: bief_tmp
    flplan = lplan
    if(present(tag1))then
      call string_to_char(tag1, ftag1)
    else
      ftag1 = '   '
    endif
    if(present(tag2))then
      call string_to_char(tag2, ftag2)
    else
      ftag2 = '   '
    endif

    if(present(tag1) .or. present(tag2)) then
      call mon_bief%extract(bief_tmp, ftag1, ftag2)
      call bief_tmp%st_to_m_pasm(pasm, l, flplan)
      call mon_bief%patch(bief_tmp, ftag1, ftag2)
    else
      call mon_bief%st_to_m_pasm(pasm, l, flplan)
    endif
  end subroutine c_st_to_m_pasm

  subroutine c_st_to_m_nmailles(nmailles, lplan, tag1, tag2) bind(c)
    ! prototype
    integer(kind=c_int), intent(in) :: nmailles
    logical(kind=c_bool), intent(in) :: lplan
    character(kind=c_char), dimension(*), intent(in), optional :: tag1, tag2
    ! variables locales
    logical :: flplan
    character(len=3) :: ftag1, ftag2
    type(bief) :: bief_tmp
    flplan = lplan
    if(present(tag1))then
      call string_to_char(tag1, ftag1)
    else
      ftag1 = '   '
    endif
    if(present(tag2))then
      call string_to_char(tag2, ftag2)
    else
      ftag2 = '   '
    endif

    if(present(tag1) .or. present(tag2)) then
      call mon_bief%extract(bief_tmp, ftag1, ftag2)
      call bief_tmp%st_to_m_nmailles(nmailles, flplan)
      call mon_bief%patch(bief_tmp, ftag1, ftag2)
    else
      call mon_bief%st_to_m_nmailles(nmailles, flplan)
    endif
  end subroutine c_st_to_m_nmailles

  subroutine c_purge() bind(c)
    call mon_bief%purge()
  end subroutine c_purge

  subroutine c_update_pk(directrice1, directrice2, origine) bind(c)
    ! prototype
    character(kind=c_char), dimension(*), intent(in) :: directrice1, directrice2
    integer(kind=c_int), intent(in) :: origine
    ! variables locales
    character(len=3) :: fdirectrice1, fdirectrice2

    call string_to_char(directrice1, fdirectrice1)
    call string_to_char(directrice2, fdirectrice2)

    call mon_bief%update_pk(fdirectrice1, fdirectrice2, origine)
  end subroutine c_update_pk

  subroutine c_output_bief(file_name) bind(c)
    ! prototype
    character(kind=c_char), dimension(*), intent(in) :: file_name
    ! variables locales
    character(len=lname) :: fname

    call string_to_char(file_name, fname)

    call mon_bief%output_bief(fname)
  end subroutine c_output_bief

  subroutine c_output_bief_mascaret(file_name) bind(c)
    ! prototype
    character(kind=c_char), dimension(*), intent(in) :: file_name
    ! variables locales
    character(len=lname) :: fname

    call string_to_char(file_name, fname)

    call mon_bief%output_bief_mascaret(fname)
  end subroutine c_output_bief_mascaret

  subroutine c_adjust_banks(file_name_left, file_name_right) bind(c)
    ! prototype
    character(kind=c_char), dimension(*), intent(in) :: file_name_left, file_name_right
    ! variables locales
    character(len=lname) :: fname1, fname2

    call string_to_char(file_name_left, fname1)
    call string_to_char(file_name_right, fname2)

    call mon_bief%adjust_banks(fname1, fname2)
  end subroutine c_adjust_banks


end module wrapper_bief
