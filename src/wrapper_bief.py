##############################################################################
#                                                                            #
#                           PROGRAMME MAGE                                   #
#                                                                            #
# Auteur : Jean-Baptiste FAURE - INRAE - 1989-2020                           #
# Dépôt APP : IDDN.FR.001.180002.000.S.P.2013.000.30200                      #
# Licence : lGPL                                                             #
#                                                                            #
#                                                                            #
# This file is part of Mage, a fortran solver for 1D shallow water equations #
# in compex networks.                                                        #
#                                                                            #
# Copyright (C) 2023 INRAE                                                   #
#                                                                            #
# Mage is free software; you can redistribute it and/or                      #
# modify it under the terms of the GNU Lesser General Public                 #
# License as published by the Free Software Foundation; either               #
# version 3 of the License, or (at your option) any later version.           #
#                                                                            #
# Alternatively, you can redistribute it and/or                              #
# modify it under the terms of the GNU General Public License as             #
# published by the Free Software Foundation; either version 2 of             #
# the License, or (at your option) any later version.                        #
#                                                                            #
# Mage is distributed in the hope that it will be useful, but                #
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
# License or the GNU General Public License for more details.                #
#                                                                            #
# You should have received a copy of the GNU Lesser General Public           #
# License and a copy of the GNU General Public License along with            #
# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
##############################################################################

from ctypes import cdll, byref, create_string_buffer, c_char_p, c_wchar_p, c_int, POINTER, Structure, c_void_p, c_double, c_bool
import os
#import numpy as np

bief_lib = cdll.LoadLibrary('../install/lib/libbief.so')

c_init_bief_from_geo_file=getattr(bief_lib,'c_init_bief_from_geo_file')
c_init_bief_from_geo_file.argtypes = [c_char_p, POINTER(c_int), POINTER(c_int)]
c_init_bief_from_geo_file.restype = None

reset_bief=getattr(bief_lib,'reset_bief')
reset_bief.argtypes = None
reset_bief.restype = None

save_bief=getattr(bief_lib,'save_bief')
save_bief.argtypes = None
save_bief.restype = None

undo_bief=getattr(bief_lib,'undo_bief')
undo_bief.argtypes = None
undo_bief.restype = None

c_get_line_tag=getattr(bief_lib,'c_get_line_tag')
c_get_line_tag.argtypes = [POINTER(c_int), c_char_p]
c_get_line_tag.restype = None

c_get_nb_lines=getattr(bief_lib,'c_get_nb_lines')
c_get_nb_lines.argtypes = [POINTER(c_int)]
c_get_nb_lines.restype = None

c_get_bief_name=getattr(bief_lib,'c_get_bief_name')
c_get_bief_name.argtypes = [c_char_p]
c_get_bief_name.restype = None

c_set_bief_name=getattr(bief_lib,'c_set_bief_name')
c_set_bief_name.argtypes = [c_char_p]
c_set_bief_name.restype = None

c_get_nb_sections=getattr(bief_lib,'c_get_nb_sections')
c_get_nb_sections.argtypes = [POINTER(c_int)]
c_get_nb_sections.restype = None

c_get_pk_section=getattr(bief_lib,'c_get_pk_section')
c_get_pk_section.argtypes = [POINTER(c_int), POINTER(c_double)]
c_get_pk_section.restype = None

c_get_nb_points_section=getattr(bief_lib,'c_get_nb_points_section')
c_get_nb_points_section.argtypes = [POINTER(c_int), POINTER(c_int)]
c_get_nb_points_section.restype = None

c_get_mean_3D_length=getattr(bief_lib,'c_get_mean_3d_length')
c_get_mean_3D_length.argtypes = [POINTER(c_double)]
c_get_mean_3D_length.restype = None

c_get_min_3D_length=getattr(bief_lib,'c_get_min_3d_length')
c_get_min_3D_length.argtypes = [POINTER(c_double)]
c_get_min_3D_length.restype = None

c_get_max_3D_length=getattr(bief_lib,'c_get_max_3d_length')
c_get_max_3D_length.argtypes = [POINTER(c_double)]
c_get_max_3D_length.restype = None

c_get_mean_2D_length=getattr(bief_lib,'c_get_mean_2d_length')
c_get_mean_2D_length.argtypes = [POINTER(c_double)]
c_get_mean_2D_length.restype = None

c_get_min_2D_length=getattr(bief_lib,'c_get_min_2d_length')
c_get_min_2D_length.argtypes = [POINTER(c_double)]
c_get_min_2D_length.restype = None

c_get_max_2D_length=getattr(bief_lib,'c_get_max_2d_length')
c_get_max_2D_length.argtypes = [POINTER(c_double)]
c_get_max_2D_length.restype = None

c_st_to_m_compl=getattr(bief_lib,'c_st_to_m_compl')
c_st_to_m_compl.argtypes = [POINTER(c_int), c_char_p, c_char_p]
c_st_to_m_compl.restype = None

c_interpolate_profils_pas_transversal=getattr(bief_lib,'c_interpolate_profils_pas_transversal')
c_interpolate_profils_pas_transversal.argtypes = [POINTER(c_int), POINTER(c_int), c_char_p, c_char_p, POINTER(c_double), POINTER(c_bool), POINTER(c_int), POINTER(c_bool)]
c_interpolate_profils_pas_transversal.restype = None

c_st_to_m_pasm=getattr(bief_lib,'c_st_to_m_pasm')
c_st_to_m_pasm.argtypes = [POINTER(c_double), POINTER(c_double), POINTER(c_bool), c_char_p, c_char_p]
c_st_to_m_pasm.restype = None

c_st_to_m_nmailles=getattr(bief_lib,'c_st_to_m_nmailles')
c_st_to_m_nmailles.argtypes = [POINTER(c_int), POINTER(c_bool), c_char_p, c_char_p]
c_st_to_m_nmailles.restype = None

c_purge=getattr(bief_lib,'c_purge')
c_purge.argtypes = None
c_purge.restype = None

c_update_pk=getattr(bief_lib,'c_update_pk')
c_update_pk.argtypes = [c_char_p, c_char_p, POINTER(c_int)]
c_update_pk.restype = None

c_output_bief=getattr(bief_lib,'c_output_bief')
c_output_bief.argtypes = [c_char_p]
c_output_bief.restype = None

c_output_bief_mascaret=getattr(bief_lib,'c_output_bief_mascaret')
c_output_bief_mascaret.argtypes = [c_char_p]
c_output_bief_mascaret.restype = None

c_adjust_banks=getattr(bief_lib,'c_adjust_banks')
c_adjust_banks.argtypes = [c_char_p, c_char_p]
c_adjust_banks.restype = None

def get_nb_lines():
  nb_lines = c_int(0)
  c_get_nb_lines(byref(nb_lines))
  return nb_lines.value

def get_line_tags():
  nb_lines = c_int(0)
  c_get_nb_lines(byref(nb_lines))
  line = create_string_buffer(b'   ')
  line_tags = []
  for i in range(nb_lines.value):
    c_get_line_tag(byref(c_int(i+1)), line)
    line_tags.append(line.value.decode())
  return line_tags

def set_bief_name(name):
  cname = create_string_buffer(name.encode())
  c_set_bief_name(cname)

def get_bief_name():
  cname = create_string_buffer(16)
  c_get_bief_name(cname)
  return cname.value.decode()

def init_bief_from_geo_file(name, with_charriage, with_water):
  cname = create_string_buffer(name.encode())
  c_init_bief_from_geo_file(cname, byref(c_int(with_charriage)), byref(c_int(with_water)))

def get_nb_sections():
  nb_sections = c_int(0)
  c_get_nb_sections(byref(nb_sections))
  return nb_sections.value

def get_pk_section(section):
  pk = c_double(0)
  c_get_pk_section(byref(c_int(section)), byref(pk))
  return pk.value

def get_nb_points_section(section):
  nb_points = c_int(0)
  c_get_nb_points_section(byref(c_int(section)), byref(nb_points))
  return nb_points.value

def get_mean_3D_length():
  length = c_double(0)
  c_get_mean_3D_length(byref(length))
  return length.value

def get_min_3D_length():
  length = c_double(0)
  c_get_min_3D_length(byref(length))
  return length.value

def get_max_3D_length():
  length = c_double(0)
  c_get_max_3D_length(byref(length))
  return length.value

def get_mean_2D_length():
  length = c_double(0)
  c_get_mean_2D_length(byref(length))
  return length.value

def get_min_2D_length():
  length = c_double(0)
  c_get_min_2D_length(byref(length))
  return length.value

def get_max_2D_length():
  length = c_double(0)
  c_get_max_2D_length(byref(length))
  return length.value

def interpolate_profils_pas_transversal(limite1, limite2, directrice1, directrice2, pas, lplan=False, lm=3, lineaire=False):
  climite1   = c_int(limite1)
  climite2   = c_int(limite2)
  cpas       = c_double(pas)
  clplan     = c_bool(lplan)
  clm        = c_int(lm)
  clineaire  = c_bool(lineaire)
  cdirectrice1 = create_string_buffer(directrice1.encode())
  cdirectrice2 = create_string_buffer(directrice2.encode())
  c_interpolate_profils_pas_transversal(byref(climite1), byref(climite2), cdirectrice1, cdirectrice2, byref(cpas), byref(clplan), byref(clm), byref(clineaire))

def st_to_m_compl(npoints, tag1='   ', tag2='   '):
  cnpoints=c_int(npoints)
  ctag1 = create_string_buffer(tag1.encode())
  ctag2 = create_string_buffer(tag2.encode())
  c_st_to_m_compl(byref(cnpoints), ctag1, ctag2)

def st_to_m_pasm(pasm, l, lplan, tag1='   ', tag2='   '):
  cpasm=c_double(pasm)
  cl = c_double(l)
  clplan = c_bool(lplan)
  ctag1 = create_string_buffer(tag1.encode())
  ctag2 = create_string_buffer(tag2.encode())
  c_st_to_m_pasm(byref(cpasm), byref(cl), byref(clplan), ctag1, ctag2)

def st_to_m_nmailles(nmailles, lplan, tag1='   ', tag2='   '):
  cnmailles = c_int(nmailles)
  clplan = c_bool(lplan)
  ctag1 = create_string_buffer(tag1.encode())
  ctag2 = create_string_buffer(tag2.encode())
  c_st_to_m_nmailles(byref(cnmailles), byref(clplan), ctag1, ctag2)

def purge():
  c_purge()

def update_pk(directrice1, directrice2, origine):
  cdirectrice1 = create_string_buffer(directrice1.encode())
  cdirectrice2 = create_string_buffer(directrice2.encode())
  corigine     = c_int(origine)
  c_update_pk(cdirectrice1, cdirectrice2, byref(corigine))

def output_bief(name):
  cname = create_string_buffer(name.encode())
  c_output_bief(cname)

def output_bief_mascaret(name):
  cname = create_string_buffer(name.encode())
  c_output_bief_mascaret(cname)

def adjust_banks(file_name_left, file_name_right):
  cname_left = create_string_buffer(file_name_left.encode())
  cname_right = create_string_buffer(file_name_right.encode())
  c_adjust_banks(cname_left, cname_right)

#extract_bief=getattr(bief_lib,'__objet_bief_MOD_extract_bief')
#extract_bief.argtypes = [CustomStructP, c_char_p, c_char_p]
#extract_bief.restype = CustomStructP

#if __name__ == "__main__":
  #os.chdir('/home/theophile.terraz/Documents/cas_mage/cas_TS3/MailleurPF')
  #if os.path.isfile("aaab"):
    #os.remove("aaab")
  #if os.path.isfile("aaac"):
    #os.remove("aaac")

  #init_bief_from_geo_file('Bief_1.ST', 0, 0)
  #name = 'Mon_Bief'
  #set_bief_name(name)
  #print("bief name:",get_bief_name())
  #print("nombre de sections (init):",get_nb_sections())
  #for i in range(get_nb_sections()):
    #print("nombre de points section",i+1,":",get_nb_points_section(i+1))
  #print("longueur 3D max :",get_max_3D_length())
  #print("longueur 3D min :",get_min_3D_length())
  #print("longueur 3D moy :",get_mean_3D_length())
  #print("longueur 2D max :",get_max_2D_length())
  #print("longueur 2D min :",get_min_2D_length())
  #print("longueur 2D moy :",get_mean_2D_length())
  #lines=get_line_tags()
  #print("nombre de lignes directrices :",len(lines))
  #print ("tags des lignes:", lines)
  #output_bief("aaab")
  #st_to_m_compl(75, 'FG','FD')
  #print("nombre de sections (compl):",get_nb_sections())
  #for i in range(get_nb_sections()):
    #print("nombre de points section",i+1,":",get_nb_points_section(i+1))
  #output_bief("aaac")
  #interpolate_profils_pas_transversal(1, get_nb_sections(), 'un', 'np', 50)
  #print("nombre de sections (interpolate_profils):",get_nb_sections())
  #update_pk("un", "np", 1)
  #for i in range(get_nb_sections()):
    #print("pk section",i+1,":",get_pk_section(i+1))
  #reset_bief()
  #print("nombre de sections (reset):",get_nb_sections())
  #for i in range(get_nb_sections()):
    #print("nombre de points section",i+1,":",get_nb_points_section(i+1))


  #print("fin du programme")
