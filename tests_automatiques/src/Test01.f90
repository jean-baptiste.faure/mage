!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - Irstea - 1989-2013                          #
!# Dépôt APP : en cours                                                       #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################
!********************* programme principal
! on vérifie que la ligne d'eau finale est bien identique à la ligne d'eau initiale
! après passage de la crue.
program test_1
   use tests_util
   implicit none

   open(1,file='../../src/Test01.log')
   write(1,'(a)') '============================================='
   write(1,'(a)') 'Test n°1 : Retour au régime permanent initial'
   call printDate(1)
   write(1,'(a)') '============================================='
   call Test1
   write(1,'(a)') ' '
   write(1,'(a)') ' '
   close(1)
end program test_1

subroutine test1
   use tests_util
   implicit none

   real(kind=dp)  :: eps_z, eps_v
   character :: nombase*18
   real(kind=dp)  :: temps ,vol1, vol2, Pk1, Pk2
   real(kind=dp),allocatable :: z1(:), z2(:), q1(:), q2(:)
   real(kind=dp)   :: ez, eq, ev
   integer :: i, isa ,isb, ib

   integer :: lu
   real(kind=dp),allocatable :: x(:)
   real(kind=dp),allocatable :: zf(:)

   lu = 10
   nombase = 'Test01'
   ib = 1
   isa = 1   ;  Pk1 = 0._dp
   isb = 51  ;  Pk2 = 10000._dp
!test d'arret
   eps_Z = 0.000001_dp
   write(1,'(a,es14.6)') ' epsilon pour les lignes d''eau : ', eps_Z
   eps_V = 0.000001_dp
   write(1,'(a,es14.6)') ' epsilon pour les volumes : ', eps_V

!********* récupération des lignes d'eau
   temps = 0._dp
   call extraire_fdx(nombase,'ZdX',ib,'h',temps,x,z1)
   deallocate (x)

   temps = 24._dp
   call extraire_fdx(nombase,'ZdX',ib,'h',temps,x,z2)
   deallocate (x)

   !transformation des cotes en tirants d'eau
   allocate (zf(isb-isa+1))
   zf(isa) = 20._dp
   do i = isa+1, isa+10
	  zf(i) = zf(i-1) - 0.5_dp
   enddo
   do i = isa+11, isa+40
	  zf(i) = zf(i-1) - 0.333333_dp
   enddo
   do i = isa+41, isb
	  zf(i) = zf(i-1) - 0.5_dp
   enddo

   z1 = z1 - zf
   z2 = z2 - zf

!*********** recuperation des débits
   temps = 0._dp
   call extraire_fdx(nombase,'QdX',ib,'h',temps,x,q1)
   deallocate (x)

   temps = 24._dp
   call extraire_fdx(nombase,'QdX',ib,'h',temps,x,q2)
   deallocate (x)

   open(2,file='Q.log')
   do i = isa,isb
      write(2,*) i,'',Q1(i),'' , Q2(i)
   end do
   close (2)

!!********* calcul des erreurs  en norme L1
   write(1,'(a)') ' '
   Ez = Erreur1(Z2,Z1)
   write(1,'(a,es14.6)') ' Erreur pour les lignes d''eau en norme L1 : ', Ez
   Eq = Erreur1(Q2,Q1)
   write(1,'(a,es14.6)') ' Erreur pour les débits en norme L1 : ', Eq
   if (   (Ez .LT. eps_Z) .AND. (Eq .LT. eps_Z)  )    then
      write(1,'(a)') ' >>>>>>>>>> Le test n°1 est valide en norme L1'
   else
      write(1,'(a)') ' ########## Le test n°1 n''est pas valide en norme L1'
   end if

!*********************    NORME INFINIE
   write(1,'(a)') ' '
   Ez = E_infinie(Z2,Z1)
   write(1,'(a,es14.6)') ' Erreur pour les lignes d''eau en norme L_infinie : ', Ez
   Eq = E_infinie(Q2,Q1)
   write(1,'(a,es14.6)') ' Erreur pour les débits en norme L_infinie : ', Eq
   if (   (Ez .LT. eps_Z) .AND. (Eq .LT. eps_Z) )    then
      write(1,'(a)') ' >>>>>>>>>> Le test n°1 est valide en norme L_infinie'
   else
      write(1,'(a)') ' ########## Le test n°1 n''est pas valide en norme L_infinie'
   end if

!!****** les Volumes
   vol1 = extraire_volume(nombase,'QdT',ib,Pk1)
   vol2 = extraire_volume(nombase,'QdT',ib,Pk2)

!*********************    Volume E/S
   write(1,'(a)') ' '
   Ev = abs(Vol1 - Vol2 ) / abs(Vol1 )
   write(1,'(a,es16.8)') ' Le volume entré est : ', vol1
   write(1,'(a,es16.8)') ' Le volume sorti est : ', vol2
   write(1,'(a,es16.8)') ' Erreur pour les volumes : ', Ev
   if (  (Ev .LT. eps_V) )    then
      write(1,'(a)') ' >>>>>>>>>> Le test n°1 est valide pour le bilan de volume E/S'
   else
      write(1,'(a)') ' ########## Le test n°1 n''est pas valide pour le bilan de volume E/S'
   end if

end subroutine Test1
