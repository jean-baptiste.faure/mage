!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - Irstea - 1989-2013                          #
!# Dépôt APP : en cours                                                       #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################
!********************* programme principal
PROGRAM test_2
   use tests_util
   implicit none

   open(1,file='../../src/Test02.log')
   write(1,'(a)') '========================================================================='
   write(1,'(a)') 'Test n°2 : Remplissage d''un canal uniforme par un apport latéral constant'
   call printDate(1)
   write(1,'(a)') '========================================================================='

   call Test2
   write(1,'(a)')
   write(1,'(a)')
   close(1)
END PROGRAM test_2


subroutine Test2
   use tests_util
   implicit none

   real(kind=dp)  :: eps
   character :: nombase*18
   real(kind=dp)  :: temps
   real(kind=dp), allocatable :: ze1(:), ze2(:), ze3(:), za1(:), za2(:), za3(:), x(:)
   real(kind=dp)   :: ez1, ez2, ez3, ez
   integer :: ib, n


   nombase='Test02'

   ib = 1
   eps = 0.0001_dp

   write(1,'(a,es14.6)') ' epsilon : ',eps

!********  les lignes d'eau à t = 5 minutes
   temps = 300._dp
   call extraire_fdx(nombase,'ZdX',ib,'s',temps,x,za1)
   deallocate (x)

!********  les lignes d'eau à t = 10 minutes
   temps = 600._dp
   call extraire_fdx(nombase,'ZdX',ib,'s',temps,x,za2)
   deallocate (x)

!********  les lignes d'eau à t = 15 minutes
   temps = 900._dp
   call extraire_fdx(nombase,'ZdX',ib,'s',temps,x,za3)
   n = size(x) !on récupère la taille de x pour allouer ze1, ze2 et ze3
   deallocate (x)

!******* solution exacte à t = 5 minutes
   allocate (ze1(n))
   Ze1 = 2.3_dp
!******* solution exacte à t = 10 minutes
   allocate (ze2(n))
   Ze2 = 2.6_dp
!******* solution exacte à t = 15 minutes
   allocate (ze3(n))
   Ze3 = 2.9_dp

!!********* calcul des erreurs  en norme L1
   write(1,'(a)') ' '
   ez1 = Erreur1 (Za1,Ze1)
   ez2 = Erreur1 (Za2,Ze2)
   ez3 = Erreur1 (Za3,Ze3)

   Ez = (Ez1 + Ez2 + Ez3)/3._dp
   write(1,'(a,es14.6)') ' Erreur en norme L1 pour les lignes d''eau : ',Ez
   IF (   Ez .LT. eps )    THEN
      write(1,'(a)') ' >>>>>>>>>> Le test n°2 est valide  en norme L1'
   ELSE
      write(1,'(a)') ' ########## le test n°2 n''est pas valide en norme L1'
   END IF

!!********* calcul des erreurs  en norme L_infinie
   write(1,'(a)') ' '
   Ez1 = E_infinie (Za1,Ze1)
   Ez2 = E_infinie (Za2,Ze2)
   Ez3 = E_infinie (Za3,Ze3)
   Ez = (Ez1 + Ez2 + Ez3)/3._dp
   write(1,'(a,es14.6)') ' Erreur en norme L_infinie pour les lignes d''eau : ',Ez
   IF (  Ez .LT. eps  )    THEN
      write(1,'(a)') ' >>>>>>>>>> Le test n°2 est valide en norme L_infinie'
   ELSE
      write(1,'(a)') ' ########## le test n°2 n''est pas valide en norme L_infinie'
   END IF

 end subroutine Test2
