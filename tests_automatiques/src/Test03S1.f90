!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - Irstea - 1989-2013                          #
!# Dépôt APP : en cours                                                       #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################
!***************** test3
! On compare la solution calculée à une solution quasi-analytique
!
PROGRAM test_3
   use tests_util
   IMPLICIT NONE

   open(1,file='../../src/Test03S1.log')
   write(1,'(a)') '================================================================================='
   write(1,'(a)') 'Test n°3-1 : Réponse à un limnigramme linéaire - simulation 1 : bief court (10 m)'
   call printDate(1)
   write(1,'(a)') '================================================================================='
   CALL Test3S1
   write(1,'(a)')
   write(1,'(a)')
   close(1)
END PROGRAM test_3

!!!!!!!!!!!*****
subroutine Test3S1
   use tests_util
   implicit none

   real(kind=dp)  :: epsz, epsq
   integer :: ndes,lu
   character :: nombase*18
   real(kind=dp)  :: temps
   real(kind=dp), allocatable :: ze1(:),ze2(:),ze3(:),ze4(:),ze5(:),za1(:),za2(:),za3(:),za4(:),za5(:),qe(:),qa(:),x(:)
   real(kind=dp)  :: ez1, ez2, ez3, ez4, ez5, ezmoyenne, t, eq, pk1, pk2
   integer :: i, isa , isb, ib

   lu=10

   nombase='Test03S1'

   ib = 1
   isa = 1   ;  pk1 = 0._dp
   isb = 21  ;  pk2 = 10._dp
   epsZ = 1.E-06_dp
   write(1,'(a,es14.6)') ' epsilon sur les cotes : ', epsZ
   epsQ = 0.001_dp
   write(1,'(a,es14.6)') ' epsilon sur les débits : ', epsQ

!********  les lignes d'eau à t = 20 minutes
   temps = 1200._dp
   call extraire_fdx(nombase,'ZdX',ib,'s',temps,x,za1)
   deallocate (x)

!********  les lignes d'eau à t = 50 minutes
   temps = 3000._dp
   call extraire_fdx(nombase,'ZdX',ib,'s',temps,x,za2)
   deallocate (x)

!********  les lignes d'eau à t = 80 minutes
   temps = 4800._dp
   call extraire_fdx(nombase,'ZdX',ib,'s',temps,x,za3)
   deallocate (x)

!********  les lignes d'eau à t = 110 minutes
   temps = 6600._dp
   call extraire_fdx(nombase,'ZdX',ib,'s',temps,x,za4)
   deallocate (x)

!********  les lignes d'eau à t = 160 minutes
   temps = 9600._dp
   call extraire_fdx(nombase,'ZdX',ib,'s',temps,x,za5)
   ndes = size(x) !on conserve la taille de x pour allouer les tableaux Ze
   deallocate (x)

!******** Débit aval
   call extraire_fdt(nombase,'QdT',ib,pk2,x,qa)
   deallocate (x)

!******* solution exacte en cote
   allocate (ze1(ndes),ze2(ndes),ze3(ndes),ze4(ndes),ze5(ndes))
   Ze1 = 1.0_dp   ! solution exacte à t = 20 minutes
   Ze2 = 1.5_dp   ! solution exacte à t = 50 minutes
   Ze3 = 2.0_dp   ! solution exacte à t = 80 minutes
   Ze4 = 3.0_dp   ! solution exacte à t = 110 minutes
   Ze5 = 4.0_dp   ! solution exacte à t = 160 minutes

!******* solution exacte en débit
   if (size(qa) /= 161) stop 1
   allocate(qe(161))
   do i = 1, 161
      t = i*1._dp
      if (t < 20._dp) then
         Qe(i) = 0._dp
      else if (t < 80._dp) then
         Qe(i) = -0.00556_dp
      else if (t < 140._dp) then
         Qe(i) = -0.01111_dp
      else
         Qe(i) = 0._dp
      endif
   enddo
! on ignore les voisinages des discontinuités : on cherche seulement les paliers
   Qa(19:34) = Qe(19:34)
   Qa(79:94) = Qe(79:94)
   Qa(139:144) = Qe(139:144)

!!********* calcul des erreurs en norme L1
   write(1,'(a)') ' '
   ez1 = erreur1 (Za1,Ze1)
   ez2 = erreur1 (Za2,Ze2)
   ez3 = erreur1 (Za3,Ze3)
   ez4 = erreur1 (Za4,Ze4)
   ez5 = erreur1 (Za5,Ze5)
   Ezmoyenne = (Ez1+Ez2+Ez3+Ez4+Ez5)/5._dp
   write(1,'(a,es14.6)') ' Erreur sur les cotes en norme L1 : ',Ezmoyenne

   eq = erreur1(Qa,Qe)
   write(1,'(a,es14.6)') ' Erreur sur les débits aval en norme L1 : '   ,EQ
   write(1,'(a)') ' ATTENTION : on néglige les points au voisinage des sauts de débit, on vérifie seulement les paliers.'

   IF (Ezmoyenne < epsZ .AND. EQ < epsQ)    THEN
      write(1,'(a)') ' >>>>>>>>>> Le test n°3-simulation 1 est valide en norme L1'
   ELSE
      write(1,'(a)') ' ########## le test n°3-simulation 1 n''est pas valide en norme L1'
   END IF

!!********* calcul des erreurs  en norme L_infinie
   write(1,'(a)') ' '
   ez1 = E_infinie(Za1,Ze1)
   ez2 = E_infinie(Za2,Ze2)
   ez3 = E_infinie(Za3,Ze3)
   ez4 = E_infinie(Za4,Ze4)
   ez5 = E_infinie(Za5,Ze5)
   Ezmoyenne = (Ez1+Ez2+Ez3+Ez4+Ez5)/5._dp
   write(1,'(a,es14.6)') ' Erreur sur les cotes en norme L_infinie : '  ,Ezmoyenne

   eq = E_infinie(Qa,Qe)
   write(1,'(a,es14.6)') ' Erreur sur les débits aval en norme L_infinie : '  ,EQ
   write(1,'(a)') ' ATTENTION : on néglige les points au voisinage des sauts de débit, on vérifie seulement les paliers.'

   IF (Ezmoyenne < epsZ .AND. EQ < epsQ)    THEN
      write(1,'(a)') ' >>>>>>>>>> Le test n°3-simulation 1 est valide en norme L_infinie'
   ELSE
      write(1,'(a)') ' ########## le test n°3-simulation 1 n''est pas valide en norme L_infinie'
   END IF

end subroutine Test3S1
