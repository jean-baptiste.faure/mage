!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - Irstea - 1989-2013                          #
!# Dépôt APP : en cours                                                       #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################
!********************* programme principal
! on vérifie que la ligne d'eau finale calculée correspond bien au régime uniforme
! c'est à dire à une ligne d'eau de profondeur constante (profil uniforme)

PROGRAM test_4
   use tests_util
   IMPLICIT NONE

   open(1,file='../../src/Test04S1.log')
   write(1,'(a)') '==============================================================================='
   write(1,'(a)') 'Test n°4-1 : Calcul d''un écoulement uniforme - simulation 1 : C.L. aval = Z(t)'
   call printDate(1)
   write(1,'(a)') '==============================================================================='
   CALL Test4S1
   write(1,'(a)')
   write(1,'(a)')
   close(1)
END PROGRAM test_4


SUBROUTINE   Test4S1
   use tests_util
   IMPLICIT NONE

   real(kind=dp)  :: temps, ez, ev, eps, pk1, pk2, qmin, qmax
   real(kind=dp), allocatable :: znum(:), qnum(:), ze(:), x(:)
   integer :: isa, isb, ib, ndes
   character :: nombase*18

   nombase='Test04S1'
   ib = 1
   isa = 1   ;  pk1 = 0._dp
   isb = 21  ;  pk2 = 2000._dp
   ndes = isb-isa+1

   !test d'arrêt
   eps = 0.00001_dp
   write(1,'(a,es14.6)') ' epsilon : ', eps

   !********* récupération de la ligne d'eau à t = 4 heures
   temps = 14400._dp
   call extraire_fdx(nombase,'ZdX',ib,'s',temps,x,znum)
   deallocate (x)

   !*********** récupération des débits
   call extraire_fdx(nombase,'QdX',ib,'s',temps,x,qnum)
   ndes = size(x)
   deallocate (x)
   qmin = minval(qnum)
   qmax = maxval(qnum)

   !******** calcul de la solution exacte
   allocate(ze(21))
   Ze(1)=7.0
   Ze(2)=6.75
   Ze(3)=6.5
   Ze(4)=6.25
   Ze(5)=6.0
   Ze(6)=5.75
   Ze(7)=5.5
   Ze(8)=5.25
   Ze(9)=5.0
   Ze(10)=4.75
   Ze(11)=4.5
   Ze(12)=4.25
   Ze(13)=4.0
   Ze(14)=3.75
   Ze(15)=3.5
   Ze(16)=3.25
   Ze(17)=3.0
   Ze(18)=2.75
   Ze(19)=2.50
   Ze(20)=2.25
   Ze(21)=2.0

   !********* calcul des erreurs  en norme L1
   write(1,'(a)') ' '
   ez = Erreur1(Znum,Ze)
   write(1,'(a,es14.6)') ' Erreur pour les lignes d''eau en norme L1 : ', Ez
   IF (Ez .LT. eps)    THEN
      write(1,'(a)') ' >>>>>>>>>> Le test n°4-simulation 1 est valide en norme L1'
   ELSE
      write(1,'(a)') ' ########## le test n°4-simulation 1 n''est pas valide en norme L1'
   END IF

   !*********************    NORME INFINIE
   write(1,'(a)') ' '
   ez = E_infinie(Znum,Ze)
   write(1,'(a,es14.6)') ' Erreur pour les lignes d''eau en norme L_infinie : ', Ez
   IF (Ez .LT. eps)    THEN
      write(1,'(a)') ' >>>>>>>>>> Le test n°4-simulation 1 est valide en norme L_infinie'
   ELSE
      write(1,'(a)') ' ########## le test n°4-simulation 1 n''est pas valide en norme L_infinie'
   END IF

   !*********************    Débits
   write(1,'(a)') ' '
   Ev = abs(qmax - qmin ) / abs(qmax)
   write(1,'(a,3(es14.6,a))') ' Erreur pour les volumes en norme L1 : ', Ev,' (',qmin,' ; ',qmax,')'
   IF (Ev .LT. eps)    THEN
      write(1,'(a)')   ' >>>>>>>>>> Le test n°4-simulation 1 est valide pour la constance du débit'
   ELSE
      write(1,'(a)')   ' ########## le test n°4-simulation 1 n''est pas valide pour la constance du débit'
   END IF


END SUBROUTINE Test4S1
