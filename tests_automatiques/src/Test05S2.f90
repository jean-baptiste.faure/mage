!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - Irstea - 1989-2013                          #
!# Dépôt APP : en cours                                                       #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################
!********************* programme principal
program test_5
   use tests_util
   implicit none

   open(1,file='../../src/Test05S2.log')
   write(1,'(a)') '======================================================================================='
   write(1,'(a)') 'Test n°5-2 : Calcul d''un écoulement permanent non uniforme - simulation 2 (durée 2h30)'
   call printDate(1)
   write(1,'(a)') '======================================================================================='
   call Test5S2
   write(1,'(a)')
   write(1,'(a)')
   close(1)
end program test_5


subroutine   Test5S2
   use tests_util
   implicit none

   character :: nombase*18, tra*18
   real(kind=dp), allocatable :: znum(:), ye(:), ze(:), x(:)
   real(kind=dp) :: eps_z, eps_v, dz, ez, ev, vol1, vol2, pk1, pk2, v_ini, v_fin
   integer :: i, ib, ndes, lu
   interface
      subroutine rk4(ye,imax)
         use tests_util, only: dp
         ! -- prototype --
         real(kind=dp), allocatable, intent(out) :: ye(:)
         integer, intent(in) :: imax
      end subroutine rk4
   end interface

   nombase = 'Test05S2'
   tra = 'Test5S2.TRA'
   ib = 1
   Pk1 = 0._dp
   Pk2 = 5000._dp

   !test d'arret
   eps_z = 0.00001_dp
   eps_v = 0.0001_dp
   write(1,'(a,es14.6)') ' epsilon pour les lignes d''eau : ', eps_Z
   write(1,'(a,es14.6)') ' epsilon pour les volumes : ', eps_V

   !********* récupération de la ligne d'eau finale
   call extraire_fdx(nombase,'ZdX',ib,'s',9000._dp,x,znum)
   ndes = size(x)
   deallocate(x)

   !***** calcul de la solution exacte Ze
   call RK4(ye,ndes)
   allocate (ze(ndes))
   dz = 4.5_dp/5000._dp*20._dp
   do i = ndes, 1, -1
!      Ze(i) = Ye(i) + real(ndes-i,kind=dp)*dz
      Ze(i) = Ye(i)
   enddo
   do i = ndes, 1, -1
      Znum(i) = Znum(i) - real(ndes-i,kind=dp)*dz
   enddo
   ! sortie de contrôle du calcul de la solution exacte
   open(newunit=lu,file='RK4.log',form='formatted',status='unknown')
   do i = 1, ndes
      write(lu,*) 20*(i-1), Ze(i), Znum(i)
   end do
   close(lu)

   !****** les Volumes
   vol1 = extraire_volume(nombase,'QdT',ib,pk1)
   vol2 = extraire_volume(nombase,'QdT',ib,pk2)

   Ev = abs(Vol1 - Vol2 ) / abs(Vol2 )

   !********* calcul des erreurs  en norme L1
   write(1,'(a)') ' '
   ez = Erreur1(Znum,Ze)
   write(1,'(a,es14.6)') ' Erreur pour les lignes d''eau en norme L1 : ', Ez
   IF (Ez .LT. eps_Z)    THEN
      write(1,'(a)') ' >>>>>>>>>> Le test n°5-simulation 2 est valide en norme L1'
   ELSE
      write(1,'(a)') ' ########## le test n°5-simulation 2 n''est pas valide en norme L1'
   END IF

   !*********************    NORME INFINIE
   write(1,'(a)') ' '
   ez = E_infinie(Znum,Ze)
   write(1,'(a,es14.6)') ' Erreur pour les lignes d''eau en norme L_infinie : ', Ez
   IF (Ez .LT. eps_Z)    THEN
      write(1,'(a)') ' >>>>>>>>>> Le test n°5-simulation 2 est valide en norme L_infinie'
   ELSE
      write(1,'(a)') ' ########## Le test n°5-simulation 2 n''est pas valide en norme L_infinie'
   END IF

   !*********************    Volumes
   write(1,'(a)') ' '
   !recherche des volumes totaux du réseau initial et final dans le fichier TRA
   call volumes_totaux(tra,v_ini,v_fin)

   Ev = abs(V_fin - (V_ini + Vol1 - Vol2 )) / abs(Vol1 )
   write(1,'(5(a,es14.6),a)') ' Erreur pour les volumes en norme L1 : ', Ev,' (',vol1,' ; ',vol2,' ; ',v_ini,' ; ',v_fin,')'
   IF (Ev .LT. eps_V)    THEN
      write(1,'(a)') ' >>>>>>>>>> Le test n°5-simulation 2 est valide pour le bilan de volume E/S'
   ELSE
      write(1,'(a)') ' ########## Le test n°5-simulation 2 n''est pas valide pour le bilan de volume E/S'
   END IF

end subroutine Test5S2

!*********************  Méthode de Runge Kutta  **************************
!      pour un canal uniforme de 5000m de long, 200m de large,
!              cote du fond amont 4,5m cote du fond aval 0m (pente 0.9%)
!      débit 240 m3/s, cote aval 2m, strickler 40.

function F (Y)
   use tests_util, only: dp
   implicit none
   ! -- prototype --
   real(kind=dp), intent(in) :: y
   real(kind=dp) :: F
   ! -- variables locales --
   real(kind=dp) :: a, b, c
   real(kind=dp), parameter :: alpha     = 4._dp / 3._dp
   real(kind=dp), parameter :: beta      = 10._dp / 3._dp
   real(kind=dp), parameter :: largeur   = 200._dp
   real(kind=dp), parameter :: debit     = 240._dp
   real(kind=dp), parameter :: strickler = 40._dp
   real(kind=dp), parameter :: gravite   = 9.81_dp
   real(kind=dp), parameter :: pente     = 4.5_dp / (20._dp*250._dp)

   a = debit*debit * (largeur+2._dp*Y)**alpha
   b = strickler*strickler * (largeur*y)**beta
   c = debit*debit / (gravite*largeur*largeur*y*y*y)
   F = (pente-a/b) / (1._dp-c)
end function F

subroutine rk4(ye,imax)
   use tests_util, only: dp
   implicit none
   ! -- prototype --
   real(kind=dp), allocatable, intent(out) :: ye(:)
   integer, intent(in) :: imax
   ! -- variables locales --
   real(kind=dp) :: h, y, f1, f2, f3, f4
   integer :: i

   interface
      function f(y)
         use tests_util, only: dp
         real(kind=dp) :: f
         real(kind=dp), intent(in) :: y
      end function f
   end interface

   allocate (ye(imax))
   h = -20._dp
   ye(imax) = 2._dp

   do  i = imax-1, 1, -1
      y = ye(i+1)
      f1 = h*f(y)
      f2 = h*f(y + 0.5_dp*f1)
      f3 = h*f(y + 0.5_dp*f2)
      f4 = h*f(y + f3)
      y = y + (f1 + 2._dp* f2  + 2._dp* f3 + f4)/6.0_dp

      ye(i) = y
   end do
end  subroutine rk4
