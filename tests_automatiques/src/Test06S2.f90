!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - Irstea - 1989-2013                          #
!# Dépôt APP : en cours                                                       #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################
program test_6
   use tests_util
   implicit none

   open(1,file='../../src/Test06S2.log')
   write(1,'(a)') '==============================================================='
   write(1,'(a)') 'Test n°6-2 : Canal horizontal avec seuil de fond - simulation 2'
   write(1,'(a)') '             Pas des profils : 0,5 ; discrétisation : 0,25     '
   call printdate(1)
   write(1,'(a)') '==============================================================='
   call test6s2
   write(1,'(a)')
   write(1,'(a)')
   close(1)
end program test_6





subroutine   test6s2
   use tests_util
   implicit none

   character :: nombase*18
   real(kind=dp),allocatable :: znum(:), ze(:), zn(:), zf(:), x(:)
   real(kind=dp) :: eps, temps, ez
   integer :: ndes, i, ib

   nombase = 'Test06S2'
   ib = 1

   eps = 0.001_dp
   write(1,'(a,es14.6)') ' epsilon pour les lignes d''eau : ', eps


   !********* récupération de la ligne d'eau à 5 minutes
   temps = 300._dp
   call extraire_fdx(nombase,'ZdX',ib,'s',temps,x,Znum)
   ndes = size(x)
   deallocate (x)

   allocate(Zf(ndes),Zn(ndes))
   call Zff(Zf,ndes)
   do i = 1, ndes
      Zn(i) = Znum(i) - Zf(i)
   end do
   close(11)

   !********* calcul de la de la solution exacte
   allocate(Ze(ndes))
   call sol(Ze,ndes)

   !********* calcul des erreurs  en norme L1
   write(1,'(a)') ' '
   ez = Erreur1(Zn, Ze)
   write(1,'(a,es14.6)') ' Erreur pour les lignes d''eau en norme L1 : ', Ez
   if (ez .lt. eps)    then
      write(1,'(a)') ' >>>>>>>>>> Le test n°6-simulation 2 est valide en norme L1'
   else
      write(1,'(a)') ' ########## Le test n°6-simulation 2 n''est pas valide en norme L1'
   end if

   !********* calcul des erreurs en norme L_Infinie
   write(1,'(a)') ' '
   ez = E_infinie(Zn, Ze)
   write(1,'(a,es14.6)') ' Erreur pour les lignes d''eau en norme L_infinie : ', Ez
   if (ez .lt. eps)    then
      write(1,'(a)') ' >>>>>>>>>> Le test n°6-simulation 2 est valide en norme L_infinie'
   else
      write(1,'(a)') ' ########## Le test n°6-simulation 2 n''est pas valide en norme L_infinie'
   end if

end subroutine Test6S2


! cote du fond avec seuil parabolique
subroutine Zff(Zf,Ndes)
   use tests_util
   implicit none
   ! -- prototype --
   integer, intent(in) :: ndes
   real(kind=dp), intent(out) :: Zf(*)
   ! -- variables locales --
   real(kind=dp) :: Dx, x
   integer :: i

   Dx = 40._dp/real(Ndes-1,kind=dp)

   do  i = 1, ndes
      x = real(i-1,kind=dp)*Dx
      if ((x > 16._dp) .AND. (x < 24._dp)) then
         Zf(i) = 0.48_dp - 0.03_dp*(x - 20._dp)**2
      else
         Zf(i) = 0._dp
      end if
   end do
end subroutine Zff

! solution exacte
subroutine sol(Ze, Ndes)
   use tests_util
   implicit none
   ! -- prototype --
   integer, intent(in) :: Ndes
   real(kind=dp),intent(out) :: Ze(*)
   ! -- variables locales --
   real(kind=dp),dimension(ndes) :: Zf
   integer  :: i
   real(kind=dp) :: alpha, gama, theta, A, B

   A = (10._dp*10._dp) /(2._dp*9.81_dp*3._dp*3._dp)
   B = 2._dp + A/(2._dp*2._dp)
   call Zff(Zf,ndes)

   do i= 1,Ndes
      alpha = (Zf(i) - B)/3._dp ; alpha = alpha*alpha
      gama  = (2._dp*9._dp*alpha*(Zf(i)-B) + 27._dp*A) / 54._dp
      theta =  acos(gama/sqrt(alpha**3))
      Ze(i) = -2._dp*sqrt(alpha)*cos((theta+2._dp*Pi)/3._dp)-(Zf(i)-B)/3._dp
   end do
end subroutine sol
