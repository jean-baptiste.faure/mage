!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - Irstea - 1989-2013                          #
!# Dépôt APP : en cours                                                       #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################
program test_7
   use tests_util
   implicit none

   open(1,file='../../src/Test07.log')
   write(1,'(a)') '========================================================'
   write(1,'(a)') 'Test n°7 : oscillation d''une marée dans un estuaire'
   write(1,'(a)') '           Attention la solution approchée de Proudman'
   write(1,'(a)') '           ne prévoit pas le décalage temporel à l''amont'
   call printdate(1)
   write(1,'(a)') '========================================================'
   call test7
   write(1,'(a)')
   write(1,'(a)')
   close(1)
end program test_7


subroutine   Test7
   use tests_util
   implicit none

   real(kind=dp)  :: eps
   integer :: ndes,lu
   character :: nombase*18
   real(kind=dp)  :: pk1, pk2, pk3
   real(kind=dp), allocatable :: Znum(:), Qt2(:), Ze(:), V1(:), V2(:), Ve1(:), Ve2(:), Qe(:), Ve(:), Ze0(:), x(:)
   real(kind=dp)   :: tmin, tmax, Ez, Ev1, Ev2, Eq
   integer :: i, ib, dt
   interface
      subroutine sol7(X,Ze,Ve,Qe,tmin,tmax)
         use tests_util
         real(kind=dp),intent(in) :: X, Tmin, Tmax
         real(kind=dp),intent(out), allocatable :: Ze(:),Ve(:),Qe(:)
      end subroutine sol7
   end interface


   nombase='Test07'
   tmin = 0._dp
   tmax = real(2*24*3600,kind=dp)
   ib = 1
   dt = int(300._dp/60._dp)
   pk1 = 0._dp
   pk2 = 5000._dp
   pk3 = 10000._dp

   eps = 0.01_dp
   write(1,'(a,es14.6)') ' epsilon pour les niveaux, vitesses et débits : ', eps

   !********* récupération des niveaux à l'amont (x=0)
   call sol7(pk1,Ze0,Ve,Qe,tmin,tmax)
   call extraire_fdt(nombase,'ZdT',ib,pk1,x,Znum)
   ndes = size(x)
   deallocate (x)

   open (newunit=lu,file='resZ7.log', form='formatted', status='unknown')
   write(lu,'(2a,2(1x,a))') '" " ', ' " T "', '"Solution Mage"', '"Sol. Proudman"'
   do i = 1, ndes
      write(lu,'(i5,2g14.6)') (i-1)*dt, Znum(i), Ze0(i)
   end do
   close(lu)

   !********* récupération des vitesses au milieu (x=5000)
   call sol7(pk2,Ze,Ve1,Qe,tmin,tmax)
   call extraire_fdt(nombase,'VdT',ib,pk2,x,V1)
   deallocate (x)

   open (newunit=lu,file='resV17.log', form='formatted', status='unknown')
   write(lu,'(2a,2(1x,a))') '" " ',' "T "', '"Solution Mage"', '"Sol. Proudman"'
   do i = 1, ndes
      write(lu,'(i5,2g14.6)') (i-1)*dt, V1(i),Ve1(i)
   end do
   close(lu)

   !********* récupération des vitesses et débits à l'aval (x=10000)
   call sol7(pk3,Ze,Ve2,Qe,tmin,tmax)
   call extraire_fdt(nombase,'VdT',ib,pk3,x,V2)
   deallocate (x)
   call extraire_fdt(nombase,'QdT',ib,pk3,x,Qt2)
   deallocate (x)

   open (newunit=lu,file='resV27.log', form='formatted', status='unknown')
   write(lu,'(2a,4(1x,a))') '" " ',' " T "', '"Solution Mage"', '"Sol. Proudman"', '"Solution Mage"', '"Sol. Proudman"'
   do i=1,ndes
      write(lu,'(i5,4g14.6)') (i-1)*dt, V2(i),Ve2(i),Qt2(i),Qe(i)
   end do
   close(lu)


   !!********* calcul des erreurs en norme L1
   ez = Erreur1(Ze0,Znum)
   write(1,'(a)') ' '
   write(1,'(a,es14.6)') ' Erreur pour les niveaux amont en norme L1 : ', Ez

   ev1 = Erreur1(V1,Ve1)
   write(1,'(a,es14.6)') ' Erreur pour les vitesses en x=5000 en norme L1 : ', Ev1

   ev2 = Erreur1 (V2,Ve2)
   write(1,'(a,es14.6)') ' Erreur pour les vitesses à l''aval en norme L1 : ', Ev2

   eq = Erreur1(Qt2,Qe)
   write(1,'(a,es14.6)') ' Erreur pour les débits à l''aval en norme L1 : ', Eq

   if (   (Ez .LT. eps) .AND. (Eq .LT. eps) .AND. (Ev1 .LT. eps) .AND. (Ev2 .LT. eps)  ) THEN
      write(1,'(a)') ' >>>>>>>>>> Le test n°7 est valide en norme L1'
   else
      write(1,'(a)') ' ########## Le test n°7 n''est pas valide en norme L1'
   end if

   !!********* calcul des erreurs en norme L_infinie
   ez = e_infinie(Znum,Ze0)
   write(1,'(a)') ' '
   write(1,'(a,es14.6)') ' Erreur pour les niveaux amont en norme L_infinie : ', Ez

   ev1 = e_infinie(V1,Ve1)
   write(1,'(a,es14.6)') ' Erreur pour les vitesses en x=5000 en norme L_infinie : ', Ev1

   ev2 = e_infinie(V2,Ve2)
   write(1,'(a,es14.6)') ' Erreur pour les vitesses à l''aval en norme L_infinie : ', Ev2

   eq = e_infinie(Qt2,Qe)
   write(1,'(a,es14.6)') ' Erreur pour les débits à l''aval en norme L_infinie : ', Eq

   if (   (ez .LT. eps) .AND. (Eq .LT. eps) .AND. (Ev1 .LT. eps) .AND. (Ev2 .LT. eps)  ) THEN
      write(1,'(a)') ' >>>>>>>>>> Le test n°7 est valide en norme Infinie'
   else
      write(1,'(a)') ' ########## Le test n°7 n''est pas valide en norme Infinie'
   end if

end subroutine Test7


subroutine sol7(X,Ze,Ve,Qe,tmin,tmax)
 !!! solution exacte du test 7 (marée sinusoïdale dans un estuaire)
 ! entrée  : X  = Pk où on veut la solution
 ! sorties : Ze = niveau d'eau (cote)
 !           Ve = vitesse moyenne
 !           Qe = débit
   use tests_util
   implicit none
   ! -- prototype --
   real(kind=dp),intent(in) :: X, Tmin, Tmax
   real(kind=dp),intent(out), allocatable :: Ze(:),Ve(:),Qe(:)
   ! -- variables locales --
   integer       :: i
   real(kind=dp) :: A, B, c, w, k, Ampli
   real(kind=dp) :: H, dt, t, Dzeta
   real(kind=dp) :: D, E, F, G, P, R, S, U, Y


   B = 10000._dp  !longueur de l'estuaire
   H = 7.5_dp     !hauteur d'eau moyenne
   Ampli = 1._dp  !amplitude de la marée
   k = 0.0025_dp

   A = Ampli/(2._dp*H)  !amplitude de la marée = 1.
   c = sqrt(9.81_dp*H)  !célérité moyenne
   w = (2._dp*Pi)/(12._dp*3600._dp) !période de 12 heures

   !dt = 720._dp
   dt = 300._dp
   i = int((tmax - tmin)/dt) + 1
   allocate (ze(i),ve(i),qe(i))

   D = A*A * w*w / (c*c)
   E = A*A*h * (-2._dp*w*B/c + 4._dp*(w/c)**3 *B*(B*B/3._dp+X*X)) * sin(2._dp*w*x)
   F = h * D * (B*B - X*X )
   G = 4._dp/3._dp*k*D * (B*B*B-x*x*x)

   P = 2._dp*A*h*cos(w*X/c)
   R = h * D * (B*B+3._dp*X*X)

   S = 2._dp*A*c*sin(w*X/c)
   U = A*A * (-2._dp*w*x + 2._dp*w*w*w/(c*c)*x*(B*B+5._dp*x*x/3._dp))
   Y = 4._dp*D*B*x*c

   t = Tmin
   i = 1
   do while (t < Tmax+1._dp) !attention à l'arrondi sur les réels
      Dzeta = P*cos(w*t) + E + F + R*cos(2._dp*w*t) + G*sin(w*t)*abs(sin(w*t))
      Ze(i) = H + Dzeta
      Ve(i) = S*sin(w*t) + U*sin(2._dp*w*t) + Y*cos(2._dp*w*t)
      Qe(i) = 1000._dp*Ze(i)*Ve(i)
      t = t+dt
      i = i+1
   end do

end subroutine sol7
