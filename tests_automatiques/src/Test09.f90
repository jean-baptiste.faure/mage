!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - Irstea - 1989-2013                          #
!# Dépôt APP : en cours                                                       #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################
program test_9
   use tests_util
   implicit none

   open(1,file='../../src/Test09.log')
   write(1,'(a)') '============================================================'
   write(1,'(a)') 'Test n°9 : évolution d''une ligne d''eau initiale gaussienne'
   write(1,'(a)') '           vérification de la symétrie des z(t) et'
   write(1,'(a)') '           de l''antisymétrie des v(t)'
   call printdate(1)
   write(1,'(a)') '============================================================'
   call test9
   write(1,'(a)')
   write(1,'(a)')
   close(1)
end program test_9

subroutine Test9
   use tests_util
   implicit none
   ! -- variables locales --
   integer :: isa, isb, i, ib
   character :: nombase*18
   real(kind=dp), allocatable :: Z1(:), Z2(:), x(:)
   real(kind=dp) :: eps, erreur_norme1=0., erreur_infinie=0., pk, dx

   nombase='Test09'
   ib = 1
   isa = 1
   isb = 101
   dx = 2._dp
   eps = 1.e-07_dp
   write(1,'(a,es14.6)') ' epsilon pour les limnigrammes et vitesses : ', eps

   ! extraction de liminigrammes symétrique par rapport au milieu du bief (is = 51)
   ! on vérifie la symétrie des Z(t)
   do i = isa, (isb+1)/2-1
      pk = real(i-1,kind=dp) * dx
      call extraire_fdt(nombase,'ZdT',ib,pk,x,z1)
      pk = real(isb-i,kind=dp) * dx
      call extraire_fdt(nombase,'ZdT',ib,pk,x,z2)
      erreur_norme1 = erreur_norme1 + erreur1(z1,z2)
      erreur_infinie = erreur_infinie + e_infinie(Z1,Z2)
   enddo
   Erreur_norme1 = Erreur_norme1 / ((isb+1)/2-isa)
   Erreur_infinie = Erreur_infinie / ((isb+1)/2-isa)

   write(1,'(a)') ' '
   write(1,'(a,es14.6)') ' Erreur pour les niveaux en norme L1 : ', Erreur_norme1
   IF (Erreur_norme1 .LT. eps)  THEN
      write(1,'(a)') ' >>>>>>>>>> Le test n°9 est valide en norme L1'
   ELSE
      write(1,'(a)') ' ########## Le test n°9 n''est pas valide en norme L1'
   END IF

   write(1,'(a)') ' '
   write(1,'(a,es14.6)') ' Erreur pour les niveaux en norme L_infinie : ', Erreur_infinie
   IF (Erreur_infinie .LT. eps) THEN
      write(1,'(a)') ' >>>>>>>>>> Le test n°9 est valide en norme L_infinie'
   ELSE
      write(1,'(a)') ' ########## Le test n°9 n''est pas valide en norme L_infinie'
   END IF

   ! extraction de V(t) symétrique par rapport au milieu du bief (is = 51)
   ! on vérifie l'antisymétrie des V(t)
   erreur_norme1 = 0._dp
   erreur_infinie = 0._dp
   do i = isa, (isb+1)/2-1
      pk = real(i-1,kind=dp) * dx
      call extraire_fdt(nombase,'VdT',ib,pk,x,z1)
      pk = real(isb-i,kind=dp) * dx
      call extraire_fdt(nombase,'VdT',ib,pk,x,z2)
      Z2 = -Z2
      erreur_norme1 = erreur_norme1 + erreur1(z1,z2)
      erreur_infinie = erreur_infinie + e_infinie(Z1,Z2)
   enddo
   Erreur_norme1 = Erreur_norme1 / ((isb+1)/2-isa)
   Erreur_infinie = Erreur_infinie / ((isb+1)/2-isa)

   write(1,'(a)') ' '
   write(1,'(a,es14.6)') ' Erreur pour les vitesses en norme L1 : ', Erreur_norme1
   IF (Erreur_norme1 .LT. eps)  THEN
      write(1,'(a)') ' >>>>>>>>>> Le test n°9 est valide en norme L1'
   ELSE
      write(1,'(a)') ' ########## Le test n°9 n''est pas valide en norme L1'
   END IF

   write(1,'(a)') ' '
   write(1,'(a,es14.6)') ' Erreur pour les vitesses en norme L_infinie : ', Erreur_infinie
   IF (Erreur_infinie .LT. eps) THEN
      write(1,'(a)') ' >>>>>>>>>> Le test n°9 est valide en norme L_infinie'
   ELSE
      write(1,'(a)') ' ########## Le test n°9 n''est pas valide en norme L_infinie'
   END IF

END subroutine Test9
