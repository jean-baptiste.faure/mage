!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - Irstea - 1989-2019                          #
!# Dépôt APP : en cours                                                       #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################
program test_11
   use tests_util
   implicit none
   character :: nombase*18, tra*32
   integer :: ibe, ibs
   real(kind=dp) :: eva, evr, eps, pk1, pk2, v_in, v_out, v_ini, v_fin

   open(1,file='../../src/Test111a.log')
   write(1,'(a)') '=========================================================================='
   write(1,'(a)') 'Test n°11-1a : Noeud à surface non-nulle cylindrique avec stockage initial'
   call printDate(1)
   write(1,'(a)') '=========================================================================='

   nombase = 'Test111a'
   tra = 'Test111a.TRA'
   ibe = 1  ;  pk1 = 0._dp
   ibs = 2  ;  pk2 = 20000._dp
   eps = 1.0d-3

   v_in = extraire_volume(nombase,'QdT',ibe,pk1)   !volume entrant
   v_out = extraire_volume(nombase,'QdT',ibs,pk2)  !volule sortant
   call volumes_totaux(tra,v_ini,v_fin)

   eva = v_out - (v_in - (v_fin - v_ini))
   evr = eva / v_in

   write(1,'(a,es14.6)') ' epsilon pour l''erreur relative : ', eps
   write(1,'(a,es14.6)') ' Erreur relative sur le bilan de volume : ', evr
   write(1,'(a,es14.6)') ' Erreur absolue sur le bilan de volume : ', eva
   IF (abs(evr) < eps)  then
      write(1,'(a)') ' >>>>>>>>>> Le test n°11-1a est valide'
   else
      write(1,'(a)') ' ########## Le test n°11-1a n''est pas valide'
   end if
   write(1,'(a)')
   write(1,'(a)') ' '
   close(1)
end program test_11
