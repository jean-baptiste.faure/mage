!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - Irstea - 1989-2019                          #
!# Dépôt APP : en cours                                                       #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################
program test_12b
   use tests_util
   implicit none
   character :: nombase*18, tra*32
   real(kind=dp), allocatable :: erreurs_relatives(:)
   real(kind=dp) :: eps, v_in, v_out, bilan

   open(1,file='../../src/Test12b.log')
   write(1,'(a)') '=========================================================================================================='
   write(1,'(a)') 'Test n°12-b : Vérification des lois d''ouvrage pour seuils, buses, orifices voute et déversoir trapézoïdal'
   call printDate(1)
   write(1,'(a)') '=========================================================================================================='

   nombase = 'Test12b'
   tra = 'Test12.TRA'
   eps = 1.0d-5

   call extract_relative_errors_from_TRA(tra,erreurs_relatives)


   write(1,'(a,es14.6)') ' epsilon pour l''erreur relative : ', eps
   write(1,'(a,es14.6)') ' Erreur relative sur les lois d''ouvrage : ', maxval(abs(erreurs_relatives))
   if (maxval(abs(erreurs_relatives)) < eps)  then
      write(1,'(a)') ' >>>>>>>>>> Le test n°12-b est valide pour le fonctionnement des ouvrages'
   else
      write(1,'(a)') ' ########## Le test n°12-b n''est pas valide pour le fonctionnement des ouvrages'
   end if
   deallocate (erreurs_relatives)

   ! vérification du bilan de masse global
   V_in  = extraire_volume(nombase,'qdt',1,0._dp)     !volume entrant
   V_out = extraire_volume(nombase,'qdt',1,5000._dp)  !volume sortant

   bilan = abs(V_in - V_out) / V_in
   write(1,'(a,es14.6)') ' Erreur relative sur la conservation de la masse : ', bilan
   if (bilan < eps)  then
      write(1,'(a)') ' >>>>>>>>>> Le test n°12-c est valide pour la conservation de la masse'
   else
      write(1,'(a)') ' ########## Le test n°12-c n''est pas valide pour la conservation de la masse'
   end if

   write(1,'(a)')
   write(1,'(a)') ' '
   close(1)
end program test_12b
