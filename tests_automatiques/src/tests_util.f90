!##############################################################################
!#                                                                            #
!#                           PROGRAMME MAGE                                   #
!#                                                                            #
!# Auteur : Jean-Baptiste FAURE - Irstea - 1989-2013                          #
!# Dépôt APP : en cours                                                       #
!# Licence : lGPL                                                             #
!#                                                                            #
!#                                                                            #
!# This file is part of Mage, a fortran solver for 1D shallow water equations #
!# in compex networks.                                                        #
!#                                                                            #
!# Copyright (C) 2023 INRAE                                                   #
!#                                                                            #
!# Mage is free software; you can redistribute it and/or                      #
!# modify it under the terms of the GNU Lesser General Public                 #
!# License as published by the Free Software Foundation; either               #
!# version 3 of the License, or (at your option) any later version.           #
!#                                                                            #
!# Alternatively, you can redistribute it and/or                              #
!# modify it under the terms of the GNU General Public License as             #
!# published by the Free Software Foundation; either version 2 of             #
!# the License, or (at your option) any later version.                        #
!#                                                                            #
!# Mage is distributed in the hope that it will be useful, but                #
!# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
!# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public     #
!# License or the GNU General Public License for more details.                #
!#                                                                            #
!# You should have received a copy of the GNU Lesser General Public           #
!# License and a copy of the GNU General Public License along with            #
!# Mage. If not, see <http://www.gnu.org/licenses/>.                          #
!##############################################################################
module tests_util
integer, parameter :: dp=kind(1.0d0)
!character (len=60) :: solveur = '../../../executables/linux64/mage_Extraire'
include 'solveur.fi'
real(kind=dp), parameter :: pi = 3.14159265358979_dp

contains

subroutine printDate(lu)
   implicit none
   integer,intent(in) :: lu
   integer :: Values(8)
   integer :: Annee,Mois,Jour,Heure,Minute,Seconde
   character(len=8) :: DDate
   character(len=10) :: Time
   character(len=5) :: Zone

   call date_and_time(ddate,time,zone,values)
   annee=values(1)
   mois=values(2)
   jour=values(3)
   heure=values(5)
   minute=values(6)
   seconde=values(7)

   write(lu,1) Jour,Mois,Annee,Heure,Minute,Seconde
!------------------------------------------------------------------------
1  format('Date début simulation : ',I2.2,'/',I2.2,'/',I4,' (',I2.2,':',I2.2,':',I2.2,')')
end subroutine printDate


subroutine extraire_fdx(nombase,type_courbe,iBief,unite,temps, x, y)
! extraction d'une courbe y(x) du fichier BIN d'une simulation
   implicit none
   ! -- prototype --
   character(len=*),intent(in)  :: nombase      !nom sans extension du fichier REP
   character(len=3), intent(in) :: type_courbe  !ZdX, QdX, etc.
   integer, intent(in)          :: iBief        !numéro du bief à extraire
   character(len=1),intent(in)  :: unite        !unité de temps : h, m, s
   real(kind=dp), intent(in)    :: temps        !date en heures de la courbe à extraire
   real(kind=dp), allocatable, intent(out)   :: x(:), y(:)

   ! -- variables locales --
   character(len=1000) :: commande
   character(len=120) :: ligne
   integer :: lu, k, ios
   character(len=20) :: tmp

   if (allocated(x)) deallocate(x)
   if (allocated(y)) deallocate(y)
   write(tmp,'(f13.3)') temps
   write(commande,'(3(a,1x),i0,1x,a1,a)') trim(solveur),trim(nombase),type_courbe,iBief,unite,adjustl(trim(tmp))
   !print*,' Commande = ',trim(commande)
   print*,''
   call execute_command_line (trim(commande))
   !les résultats sont dans «nombase».res
   open(newunit=lu,file=trim(nombase)//'.res',form='formatted',status='old')
   !1ère lecture pour déterminer le nombre de points de la courbe et allouer les 2 tableaux
   k = 0
   do
      read(lu,iostat=ios,fmt='(a)') ligne
      if (ios /= 0) then
         exit
      elseif (ligne(1:1) == '*') then
         cycle
      else
         k = k+1
      endif
   enddo
   allocate (x(k),y(k))
   rewind (lu)
   k = 0
   !2e lecture : lecture des données
   do
      read(lu,iostat=ios,fmt='(a)') ligne
      if (ios /= 0) then
         exit
      elseif (ligne(1:1) == '*') then
         cycle
      else
         k = k+1
         read(ligne,*) x(k), y(k)
      endif
   enddo
   close (lu)
end subroutine extraire_fdx


subroutine extraire_fdt(nombase,type_courbe,iBief,Pk, x, y)
! extraction d'une courbe y(t) du fichier BIN d'une simulation
   implicit none
   ! -- prototype --
   character(len=*),intent(in)  :: nombase      !nom sans extension du fichier REP
   character(len=3), intent(in) :: type_courbe  !ZdX, QdX, etc.
   integer, intent(in)          :: iBief        !numéro du bief à extraire
   real(kind=dp), intent(in)    :: Pk           !Pk de la section où la courbe doit être extraite
   real(kind=dp), allocatable, intent(out)   :: x(:), y(:)

   ! -- variables locales --
   character(len=1000) :: commande
   character(len=120) :: ligne
   integer :: lu, k, ios

   if (allocated(x)) deallocate(x)
   if (allocated(y)) deallocate(y)
   write(commande,'(3(a,1x),i0,1x,f10.3)') trim(solveur),trim(nombase),type_courbe,iBief,Pk
   print*,''
   call execute_command_line (trim(commande))
   !les résultats sont dans «nombase».res
   open(newunit=lu,file=trim(nombase)//'.res',form='formatted',status='old')
   !1ère lecture pour déterminer le nombre de points de la courbe et allouer les 2 tableaux
   k = 0
   do
      read(lu,iostat=ios,fmt='(a)') ligne
      if (ios /= 0) then
         exit
      elseif (ligne(1:1) == '*') then
         cycle
      else
         k = k+1
      endif
   enddo
   allocate (x(k),y(k))
   rewind (lu)
   k = 0
   !2e lecture : lecture des données
   do
      read(lu,iostat=ios,fmt='(a)') ligne
      if (ios /= 0) then
         exit
      elseif (ligne(1:1) == '*') then
         cycle
      else
         k = k+1
         read(ligne,*) x(k), y(k)
      endif
   enddo
   close (lu)
end subroutine extraire_fdt


function extraire_volume(nombase,type_courbe,iBief,Pk)
! extraction du volume d'une courbe y(t) du fichier BIN d'une simulation
   implicit none
   ! -- prototype --
   character(len=*),intent(in)  :: nombase      !nom sans extension du fichier REP
   character(len=3), intent(in) :: type_courbe  !ZdX, QdX, etc.
   integer, intent(in)          :: iBief        !numéro du bief à extraire
   real(kind=dp), intent(in)    :: Pk           !Pk de la section où la courbe doit être extraite
   real(kind=dp)                :: extraire_volume

   ! -- variables locales --
   character(len=1000) :: commande
   character(len=120) :: ligne
   integer :: lu, ios

   write(commande,'(3(a,1x),i0,1x,f10.3)') trim(solveur),trim(nombase),type_courbe,iBief,Pk
   print*,''
   call execute_command_line (trim(commande))
   !les résultats sont dans «nombase».res
   open(newunit=lu,file=trim(nombase)//'.res',form='formatted',status='old')

   do
      read(lu,iostat=ios,fmt='(a)') ligne
      if (ios /= 0) then
         exit
      elseif (ligne(1:22) == '* Volume hydrogramme :') then
         read(ligne(23:),*) extraire_volume
         exit
      endif
   enddo
   close (lu)
end function extraire_volume


subroutine volume ( vol, xdes, ydes , ndes )
   implicit none
   integer, intent (in) :: ndes
   real(kind=dp), dimension( ndes),intent(in) :: xdes,ydes
   real(kind=dp),intent(out) :: vol

   integer :: i

   vol = 0._dp
   do i = 1, ndes -1
      vol = vol  + 0.5_dp*(ydes(i)+ ydes(i+1))*(xdes(i+1)-xdes(i))
   end do
end subroutine volume


function  norme1(x,nmax)
   implicit none
   ! -- prototype --
   real(kind=dp) :: norme1
   integer, intent(in) :: nmax
   real(kind=dp), dimension(*), intent(in) :: x
   ! -- variables locales --
   integer :: i

   norme1 = 0._dp
   do i = 1, nmax
      norme1 = norme1 + abs(x(i))
   end do
end function norme1


function erreur1(e1, e2)
   implicit none
   ! -- prototype --
   real(kind=dp), dimension(:),intent(in) :: e1, e2
   real(kind=dp) ::  erreur1
   ! -- variables locales --
   real(kind=dp) ::  res1, res2, res3
   real(kind=dp), allocatable :: e3(:)
   integer :: nmax

   if (size(e1) /= size(e2)) then
      write(*,*) '>>>> Erreur dans erreur1 : les 2 vecteurs sont de tailles différentes : ',size(e1),size(e2)
      stop 1
   else
      nmax = size(e1)
      allocate (e3(nmax))
   endif

   res1 = sum(abs(e1)) !norme1(e1, nmax)
   res2 = sum(abs(e2)) !norme1(e2, nmax)
   e3 = e1-e2
   res3 = sum(abs(e3)) !norme1(e3, nmax)
   if (res1+res2 .ne. 0._dp) then
      erreur1 = 2._dp*res3/(res1+res2)
   else
      erreur1 = res3
      !stop 'erreur relative en norme L1 impossible'
   end if
   deallocate (e3)
end function erreur1


function norme_inf(x,nmax)
! cette routine calcule la norme L-infini d'un vecteur
   implicit none
   ! -- prototype --
   real(kind=dp) :: norme_inf
   integer,intent(in) :: nmax
   real(kind=dp), dimension(*),intent(in) :: x
   ! -- variables locales --
   integer :: i

   norme_inf = abs(x(nmax))

   do i = 1, nmax
      if(abs(x(i)) .gt. norme_inf)  then
         norme_inf = abs(x(i))
      end if
   end do
end function norme_inf


function e_infinie(e1, e2)
!  routine qui calcule l'erreur L_infinie
   implicit none
   ! -- prototype --
   real(kind=dp), dimension(:), intent(in)   :: e1, e2
   real(kind=dp)                             :: e_infinie
   ! -- variables locales --
   real(kind=dp), allocatable :: e3(:)
   real(kind=dp) :: res1, res2, res3
   integer  :: nmax

   if (size(e1) /= size(e2)) then
      write(*,*) '>>>> Erreur dans e_infinie : les 2 vecteurs sont de tailles différentes !'
      stop 1
   else
      nmax = size(e1)
      allocate (e3(nmax))
   endif

   res1 = maxval(abs(e1(1:nmax))) !norme_inf(e1, nmax)
   res2 = maxval(abs(e2(1:nmax))) !norme_inf(e2, nmax)
   e3 = e1 - e2
   res3 = maxval(abs(e3(1:nmax))) !norme_inf(e3, nmax)

   if (res1+res2 .ne. 0._dp) then
      e_infinie = 2._dp*res3/(res1+res2)
   else
      e_infinie = res3
      !stop 'erreur relative en norme L_infini impossible'
   end if
   deallocate (e3)
end function e_infinie


subroutine volumes_totaux(tra,v_ini,v_fin)
!recherche des volumes totaux du réseau initial et final dans le fichier TRA
   implicit none
   ! -- proptotype --
   character(len=*), intent(in) :: tra !nom du fichier TRA
   real(kind=dp), intent(out) :: v_ini, v_fin ! volumes initial et final
   ! -- variables locales --
   character(len=120) :: ligne, ligne1, ligne2
   integer :: lu, ios

   open(newunit=lu,file=trim(tra),form='formatted',status='old')
   ligne1 = '' ; ligne2 = ''
   do
      read(lu,'(a)',iostat=ios) ligne
      if (ios < 0) then
         exit
      elseif (ligne(2:18) == 'Volume total du r') then
         if (ligne1 == '') then
            ligne1 = ligne
         else
            ligne2 = ligne
         endif
      endif
   enddo
   ! décodage de ligne1
   v_ini = extraire_num(ligne1)
   v_fin = extraire_num(ligne2)
end subroutine volumes_totaux


function extraire_num(ligne)
   implicit none
   integer,parameter :: dp=kind(1.d0)
   ! prototype
   real(kind=dp) :: extraire_num
   character :: ligne*80
   !variables locales
   integer :: i, j, k, n, iv
   character :: numval*12, fmt*4

   k = index(ligne,'10**6 m3')
   if (k == 0) then
      k = index(ligne,'m3') - 1
   else
      k = k-1
   endif
   i = scan(ligne,'1234567890')  ! la valeur cherchée est dans ligne1(i:k)
   n = 0
   do j = i, k  !élimination des séparateurs de millier
      if (ligne(j:j) /= ' ') then
         n = n+1
         numval(n:n) = ligne(j:j)
      endif
   enddo
   !write(*,*) ligne
   if (n > 9) stop 101
   if (n < 1) stop 102
   write(fmt,'(a,i1,a)') '(i',n,')'  !format de lecture
   read(numval(1:n),fmt) iv
   extraire_num = real(iv,dp)
end function extraire_num


subroutine extract_relative_errors_from_TRA(tra,errors_sing)
! extrait les erreurs relatives sur les lois d'ouvrage telles que affichées dans TRA
   implicit none
   ! -- prototype --
   character(len=*), intent(in) :: tra !nom du fichier TRA
   real(kind=dp),allocatable, intent(out) :: errors_sing(:)
   ! -- variables locales --
   integer :: lu, ns, k, ios
   character(len=120) :: ligne

   open(newunit=lu,file=trim(tra),form='formatted',status='old')
   k = 0
   do
      read(lu,'(a)',iostat=ios) ligne
      if (ios < 0) then
         close (lu)
         exit
      elseif (ligne(2:33) == 'Nombre total de sections singuli') then
         ! lecture du nombre de sections singulières pour allouer errors_sing
         read(ligne(41:),*) ns
         allocate (errors_sing(ns))
         print*,' Nombre de sections singulières : ',ns
      elseif (ligne(24:44) == ': Erreur rel. maxi. =') then
         k = k+1
         read(ligne(45:55),*) errors_sing(k)
         print*, k, errors_sing(k)
      endif
   enddo
end subroutine extract_relative_errors_from_TRA

end module tests_util
